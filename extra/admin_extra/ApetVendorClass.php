<?php

namespace ApetVendor;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class ApetVendorClass
{
    public static function firebaseUpdate(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');

        $authPhp = file_get_contents('Auth.php', FILE_USE_INCLUDE_PATH);
        $apiClientPhp = file_get_contents('ApiClient.php', FILE_USE_INCLUDE_PATH);

        $standartAuthPhp = $vendorDir . "/kreait/firebase-php/src/Firebase/Auth.php";
        $standartApiClientPhp = $vendorDir . "/kreait/firebase-php/src/Firebase/Auth/ApiClient.php";

        file_put_contents($standartAuthPhp, $authPhp);
        file_put_contents($standartApiClientPhp, $apiClientPhp);
    }
}
