
import { STORE_MY_REQUESTS, RESET_MY_REQUESTS, REMOVE_REQUESTS, SET_DELETE_REQUEST_PROCESS, SET_MY_REQUEST_FILTER, SET_MY_REQUESTS_RESET_LOADER} from './actionTypes';
import {setMessage} from './indexAction';
import http from '../../utils/http';
export const get_my_requests = (status, from, to, reset) => {
    return (dispatch, getState) => {
        if (reset) {
            dispatch(set_reset_loader(true));
        }
        http({
            "action": "apet_get_app_user_requests",
            "status" : status,
            "from": from,
            "to": to
        }, reset).then((resp) => {
            dispatch(reset_my_requests());
            if (reset) {
                dispatch(set_reset_loader(false));
            }
            if (resp.success === "true") {
                dispatch(store_my_requests(resp.data))
            } else {
                dispatch(store_my_requests([]))
            }
        
        }).catch(() => {
            if (reset) {
                dispatch(set_reset_loader(false));
            }
        })
    }
}

export const delete_request = (id, from, to) => {
    return (dispatch, getState) => {
        dispatch(setDeleteRequestProcess(false));
        http({
            "action": "apet_cancel_app_user_request",
            "request-id": id,
            "from": from,
            "to": to
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setMessage(true, 'requestcanceled-success'))
                dispatch(removeRequest(id));
                dispatch(setDeleteRequestProcess(true));
            } else {
                dispatch(setMessage(true, 'requestcancel-error'))
            }
        })
    }
}

export const reset_my_requests = (data) => {
    return {
        type: RESET_MY_REQUESTS
        
    }
}

export const removeRequest = (id) => {
    return {
        type: REMOVE_REQUESTS,
        id: id
    }
}

const store_my_requests = (data) => {
    return {
        type: STORE_MY_REQUESTS,
        data: data
    }
}

const setDeleteRequestProcess = (process) => {
    return {
        type: SET_DELETE_REQUEST_PROCESS,
        process: process
    }
}

const set_reset_loader = (isLoading) => {
    return {
        type: SET_MY_REQUESTS_RESET_LOADER,
        isLoading: isLoading
    }
}

export const setRequestFilter = (filter) => {
    return {
        type: SET_MY_REQUEST_FILTER,
        filter: filter
    }
}