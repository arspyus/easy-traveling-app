import { AsyncStorage } from "react-native";
import firebase from 'react-native-firebase'

import http from '../../utils/http';
import { STORE_FCMTOKEN, SET_UNREAD_NOTIFICATION, CLEAR_UNREAD_NOTIFICATION } from './actionTypes';


export const get_token = () => {
    return dispatch => {
        firebase.messaging().hasPermission().then((enabled) => {
            if (enabled) {
                dispatch(getFcmToken())
            } else {
                dispatch(requestPermission());
            }
        }).catch(() => {
            dispatch(requestPermission());
        })
        
    }
}

const requestPermission = () => {
    return dispatch => {
        try {
            firebase.messaging().requestPermission().then(() => {

                dispatch(getFcmToken())
            }).catch(err => {});
        } catch (error) {
        }
    }
}

let onTokenRefreshListener;

const getFcmToken = () => {
    return dispatch => {
        
        firebase.messaging().getToken().then(fcmToken => {
            dispatch(updateFcmToken(fcmToken))
        }).catch(err => {});
    }
}

const updateFcmToken = (token) => {
    return (dispatch, getState) => {
        http({
            'action': 'apet_update_push_token',
            'push_token_id': token
        }, true, true).then((resp) => {
            if (resp.success === "true") {
                dispatch(setFcmToken(token));
            }
        })
    }
    
}

export const setFcmToken = (token) => {
    return dispatch => {
        AsyncStorage.setItem('easytraveling:fcmToken', token).then(() => {
            dispatch(store_fcmToken(token))
        }).catch((er) => {
            dispatch(store_fcmToken(null));
        })
    } 
}

export const store_fcmToken = (token) => {
    return {
        type: STORE_FCMTOKEN,
        token: token
    }
}

export const remove_fcmToken = (token) => {
    return dispatch => {
        AsyncStorage.removeItem('easytraveling:fcmToken').then(() => {
            dispatch(store_fcmToken(null));
        }).catch((er) => {
        })
    } 
}

export const set_unread_notification = (notificationType) => {
    return {
        type: SET_UNREAD_NOTIFICATION,
        notificationType: notificationType
    }
}

export const clear_unread_notification = (notificationType) => {
    return {
        type: CLEAR_UNREAD_NOTIFICATION,
        notificationType: notificationType
    }
}