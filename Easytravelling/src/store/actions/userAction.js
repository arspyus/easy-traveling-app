import { AsyncStorage } from "react-native";
import firebase from 'react-native-firebase';

import {
    STORE_USER, 
    STORE_UID, 
    SET_SIGNIN_PROCESS, 
    SET_LOGOUT_PROCESS, 
    UPDATE_USER_PROCESS, 
    CHANGE_PASSWORD_PROCESS, 
    RESET_PASSWORD_PROCESS, 
    DEACTIVATE_PROCESS,
    SET_GUEST,
    SET_USER_DATA_IS_LOADING
} from './actionTypes';
import {setLanguage, setMessage, set_avatar, set_loader, show_password_popup} from './indexAction';
import {remove_fcmToken} from './pushAction';
import { signInWithEmailAndPassword, signInWithCustomToken } from './firebaseAction';

import http from '../../utils/http';
import getAvatar from '../../utils/getAvatar';

export const setUser = (user, updateAvatar) => {
    return (dispatch, getState) => {
        if(user){
            if(user.firebase_uid === user.email){
                 user.email =  ""
            }
            user.phone = user.phone || "";
            AsyncStorage.setItem('easytraveling:user', JSON.stringify(user)).then(() => {
                let old_email = "";
                if(getState().user.user && getState().user.user.email ) old_email = getState().user.user.email;

                dispatch(store_user(user));
                dispatch(setUserDataIsLoading(false));
                if(user.firebase_uid){
                    dispatch(setUID(user.firebase_uid))
                }
                if(updateAvatar){
                    getAvatar(user.email)
                }
            }).catch(() => {
                dispatch(store_user(user));
                if(user.firebase_uid){
                    dispatch(setUID(user.firebase_uid))
                }
            })
        }
    }
}

const setUID = (uid) => {
    return dispatch => {
            AsyncStorage.setItem('easytraveling:uid', uid).then(() => {
                dispatch(store_uid(uid))
            }).catch(() => {
                dispatch(store_uid(uid))
            })
    }
}

export const removeUID = (uid) => {
    return dispatch => {
        dispatch(store_uid(null));
        AsyncStorage.removeItem('easytraveling:uid');
    }
}

export const store_user = (user) => {
    return {
        type: STORE_USER,
        user: user
    }
}

export const store_uid = (uid) => {
    return {
        type: STORE_UID,
        uid: uid
    }
}

export const signUp = (data) => {
    return (dispatch, getState) => {
        dispatch(set_loader(true));
        http({
            "action": "apet_app_user_registration",
            ...data
        }, true).then((resp) => {
            if (resp.success === "true") {
                dispatch(signInWithEmailAndPassword({...resp, email: data.email, password: data.password, registration: true}));
            } else {
                dispatch(set_loader(false));
                if(resp.errorMessage == 'user_exist'){
                    dispatch(setMessage(true, 'user-exist'));
                }else {
                    dispatch(setMessage(true, 'failed-to-register'));
                }
            }
        })
    }
}

export const signIn = (data, link) => {
    return (dispatch, getState) => {
        dispatch(setSigninProcess(false));
        dispatch(set_loader(true));
        http({
            "action": "apet_app_user_login",
            ...data
        }, true).then((resp) => {
            if (resp.success === "true" && resp.customToken) {
                dispatch(signInWithCustomToken(resp, link));
            } else {
                dispatch(setMessage(true, 'failed-to-login'));
                dispatch(set_loader(false));
            }
        })
    }
}

export const firebaseSignIn = (token) => {
    return (dispatch, getState) => {
        http({
            "action": "apet_firebase_login",
            "token": token,
            "language": getState().index.lang
        }).then((resp) => {
            dispatch(set_loader(false));
            if(resp.user_changed === "true"){
                dispatch(show_password_popup(true, resp.data.email));
            } else {
                dispatch(signInToApp(resp))
            }
        })
    }
}

export const signInToApp = (data) => {
    return (dispatch, getState) => {
        if (data.success === "true") {
            dispatch(setSigninProcess(false));
            dispatch(setUser(data.data, true));
            dispatch(store_uid(data.data.firebase_uid));
            if(data.registration){
                dispatch(setMessage(true, "registration-success"))
            }
            dispatch(setGuestUser(false));
            dispatch(setSigninProcess(true));
        } else {
            dispatch(setMessage(true, 'failed-to-login'));
        }
    }
}

export const getUserData = () => {
    return (dispatch, getState) => {
        http({
            "action": "apet_get_app_user_data"
        }, true, true).then((resp) => {
            if (resp.success === "true") {
                dispatch(setUser(resp.data));
                if(resp.data.firebase_uid)
                    dispatch(store_uid(resp.data.firebase_uid));
            } else {
                dispatch(setMessage(true, 'something-went-wrong'));
            }
        })
    }
}

export const resetPassword = (email) => {
    return (dispatch, getState) => {

        dispatch(setResetPasswordProcess(false));

        http({
            "action": "apet_app_reset_password",
            email: email
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setResetPasswordProcess(true));
                dispatch(setMessage(true, 'reset-password-success'));
            } else {
                dispatch(setMessage(true, 'reset-password-error'));
            }
        })
    }
}

export const updateUser = (user) => {
    return (dispatch, getState) => {
        if(Object.keys(user).length > 1){
            dispatch(setUpdateUserProcess(false));
        }

        http({
            "action": "apet_change_app_user_settings",
            ...user
        }).then((resp) => {
            if (resp.success === "true") {
                let prevData = getState().user.user;
                let newUser = {
                    ...prevData
                }
                if(user.lastname){
                    newUser.lastname = user.lastname;
                }
                if(user.firstname){
                    newUser.firstname = user.firstname;
                }
                if(user.phone || user.phone === ""){
                    newUser.phone = user.phone;
                }
                if(user.email){
                    newUser.pending_email = user.email;
                    newUser.email = user.email;
                }
                const email_changed = user.email ? true : false;
                const only_email = prevData.lastname === user.lastname && prevData.firstname === user.firstname && prevData.phone === user.phone && user.language === getState().index.lang ? true : false;
                dispatch(setUser(newUser));
                if(Object.keys(user).length > 1){
                    dispatch(setUpdateUserProcess(true));
                }
                dispatch(setLanguage(user.language));
                if(email_changed && only_email){
                    dispatch(setMessage(true, 'settings-saved-only-email'));
                }else if(email_changed){
                    dispatch(setMessage(true, 'settings-saved-with-email-change'));
                }else {
                    dispatch(setMessage(true, 'settings-saved'));
                }
            } else {
                if(resp.errorMessage == 'user_exist'){
                    dispatch(setMessage(true, 'user-exist'));
                }else{
                    dispatch(setMessage(true, 'faild-tosave-settings'));
                }
            }
        })
    }
}

export const changePassword = (data) => {
    return (dispatch, getState) => {
 
        dispatch(setChangePasswordProcess(false));

        http({
            "action": "apet_app_change_password",
            ...data
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setChangePasswordProcess(true));
                dispatch(setUID(resp.data.firebase_uid));
                dispatch(setMessage(true, 'changepassword-success'));
            } else {
                if(resp.errorMessage == 'incorrect_password'){
                    dispatch(setMessage(true, 'wrong-password'));
                }else {
                    dispatch(setMessage(true, 'changepassword-error'));
                }
            }
        })
    }
}

export const deactivate = (data) => {
    return (dispatch, getState) => {

        dispatch(setDeactivateProcess(false));

        http({
            "action": "apet_deactivate_app_user",
            ...data
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setDeactivateProcess(true));
                dispatch(dispatch(logoutFromApp()));
            } else {
                if(resp.errorMessage == 'wrong_password'){
                    dispatch(setMessage(true, 'wrong-password'));
                }else {
                    dispatch(setMessage(true, 'failed-to-deactivate'));
                }
            }
        })
    }
}

export const logout = () => {
    return (dispatch, getState) => {

        http({
            "action": "apet_app_user_log_out"
        }).then((resp) => {
            dispatch(logoutFromApp())
        })
    }
}

export const logoutFromApp = () => {
    return (dispatch, getState) => {
        firebase.auth().signOut();
        dispatch(setLogoutProcess(false));
        dispatch(set_avatar(null));
        dispatch(removeUID());
        dispatch(remove_fcmToken());
        dispatch(setLogoutProcess(true));
    }
} 

export const verifyAccount = (uid) => {
    return (dispatch, getState) => {
        http({
            "action": "apet_send_email_to_user",
            "uid" : uid
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setMessage(true, "registration-success"))
            } else {
                if(resp.errorMessage == 'user_exist'){
                    dispatch(setMessage(true, 'user-exist'));
                }else {
                    dispatch(setMessage(true, "something-went-wrong"));
                }
                
            }
        })
    }
}

const setSigninProcess = (process) => {
    return {
        type: SET_SIGNIN_PROCESS,
        process: process
    }
}

export const setLogoutProcess = (process) => {
    return {
        type: SET_LOGOUT_PROCESS,
        process: process
    }
}

const setUpdateUserProcess = (process) => {
    return {
        type: UPDATE_USER_PROCESS,
        process: process
    }
}

const setResetPasswordProcess = (process) => {
    return {
        type: RESET_PASSWORD_PROCESS,
        process: process
    }
}

const setChangePasswordProcess = (process) => {
    return {
        type: CHANGE_PASSWORD_PROCESS,
        process: process
    }
}

const setDeactivateProcess = (process) => {
    return {
        type: DEACTIVATE_PROCESS,
        process: process
    }
}

export const setGuestUser = (isGuest) => {
    return {
        type: SET_GUEST,
        isGuest: isGuest
    }
}

export const setUserDataIsLoading = (isLoading) => {
    return {
        type: SET_USER_DATA_IS_LOADING,
        isLoading: isLoading
    }
}