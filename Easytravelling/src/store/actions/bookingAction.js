
import {SET_BOOKING_PROCESS} from './actionTypes';
import {setMessage, set_loader} from './indexAction';
import { setUser } from './userAction';

import http from '../../utils/http';
import upload from '../../utils/upload';

export const book = (data) => {
    return (dispatch, getState) => {
        let connection = getState().index.connection;
        
        if(!connection){
            dispatch(setMessage(true, "went-offline"));
            return;
        }

        dispatch(setBookingProcess(false));
        dispatch(set_loader(true));
        let promises = [];
        for(let o in data.form){
            if(data.form[o].photo){
                ((i) => {
                    promises.push(new Promise((resolve, reject) => {
                        upload({
                            "action": "apet_upload_book_imgs",
                            "offer-id" : data.offer_id,
                            "photo_index": i,
                            "file" : data.form[i].photo
                        }).then((resp) => {
                            if (resp.success === "true") {
                                data.form[i].photo = resp.data.photo;
                                resolve(resp.data.photo)
                            } else {
                                reject()
                            }
                        }).catch((err) => {
                            reject()
                        })
                    }));
                })(o)
                
            }
        }
        if(promises.length > 0) {
            Promise.all(promises).then((images) => {
                dispatch(sendBookData(data, true));
            }).catch(() => {
                dispatch(set_loader(false));
                dispatch(setMessage(true, 'file-upload-error'));
            })
        } else {
            dispatch(sendBookData(data, false));
        }
        
    }
}

const sendBookData = (data, imgBook) => {
    return (dispatch, getState) => {
        http({
            "action": "apet_booking",
            travelers: JSON.stringify(data.form),
            offer_id: data.offer_id,
            img_book: imgBook,
            request_id: data.request_id,
            offer_author: data.offer_author
        }).then((resp) => {
            if (resp.success === "true") {
                
                let prevData = getState().user.user;
                let userData = {
                    ...prevData,
                    ...resp.data
                }
                dispatch(setUser(userData));
                dispatch(setMessage(true, 'book-success'));
                dispatch(setBookingProcess(true));
            } else {
                dispatch(setMessage(true, 'book-error'));
            }
        })
    }
    
}

const setBookingProcess = (isBooking) => {
    return {
        type: SET_BOOKING_PROCESS,
        isBooking: isBooking
    }
}
