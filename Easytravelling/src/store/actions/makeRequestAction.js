import {SELECT_AGENCY, SELECT_LAST_AGENCY, SELECT_TYPE, MAKE_REQUEST, SET_MAKE_REQUEST_PROCESS} from './actionTypes';
import {setMessage} from './indexAction';

import http from '../../utils/http';

export const select_agency = (agency) => {
    return {
        type: SELECT_AGENCY,
        agency: agency
    }
}

export const select_last_agency = (agency) => {
    return {
        type: SELECT_LAST_AGENCY,
        agency: agency
    }
}

export const select_type = (type) => {
    return {
        type: SELECT_TYPE,
        request_type: type
    }
}

export const make_request = (data) => {
    return (dispatch, getState) => {
        dispatch(setMakeRequestProcess(false))
        http({
            "action": "apet_make_request",
            ...data
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setMessage(true, 'make-request-success'));
                dispatch(setMakeRequestProcess(true))
            } else {
                dispatch(setMessage(true, 'make-request-error'));
            }
        })
    }
}

const setMakeRequestProcess = (process) => {
    return {
        type: SET_MAKE_REQUEST_PROCESS,
        process: process
    }
}

