
import {STORE_SPECIAL_OFFERS, STORE_SPECIAL_OFFERS_PAGE, SET_OFFERS_LOADER, SET_OFFERS_RESET_LOADER, STORE_SPECIAL_OFFERS_TOURID} from './actionTypes';
import {setMessage} from './indexAction';

import http from '../../utils/http';

import config from '../../config/config';

export const get_special_offers = (tour_id, agency_name, reset) => {
    return (dispatch, getState) => {
        if(!getState().index.connection){
            return;
        }
        let offset = getState().offers.page;
        offset = reset ? 0 : offset;
        if(offset !== 0){
            dispatch(set_offers_loader(true));
        } else if (reset) {
            dispatch(set_offers_reset_loader(true));
        }

        const data = {
            "action": "apet_get_special_offers",
            "limit" : config.PERPAGE,
            "offset": offset
        }

        if(tour_id && agency_name){
            data["tour-id"] = tour_id;
            data["agency-name"] = agency_name
        }

        if(tour_id){
            dispatch(store_special_offers_tourId(tour_id))
        } else {
            dispatch(store_special_offers_tourId(null))
        }

        http(data, offset !== 0 || tour_id || reset).then((resp) => {
            if(offset !== 0){
                dispatch(set_offers_loader(false));
            } else if (reset) {
                dispatch(set_offers_reset_loader(false));
            }
            if (resp.success === "true") {
                if(resp.data){
                    dispatch(store_special_offers(resp.data, reset))
                    dispatch(store_special_offers_page(offset  + config.PERPAGE))
                }
            } else {
                dispatch(store_special_offers([]))
            }
        }).catch(() => {
            if(offset !== 0){
                dispatch(set_offers_loader(false));
            } else if (reset) {
                dispatch(set_offers_reset_loader(false));
            }
        })
    }
}

export const reset_special_offers = (offset) => {
    return dispatch => {
        dispatch(store_special_offers(null))
        dispatch(store_special_offers_page(0))
    }
}

const set_offers_loader = (loading) => {
    return {
        type: SET_OFFERS_LOADER,
        loading: loading
    }
}

const set_offers_reset_loader = (loading) => {
    return {
        type: SET_OFFERS_RESET_LOADER,
        loading: loading
    }
}

const store_special_offers = (offers, reset) => {
    return {
        type: STORE_SPECIAL_OFFERS,
        offers: offers,
        reset: reset
    }
}

const store_special_offers_page = (page) => {
    return {
        type: STORE_SPECIAL_OFFERS_PAGE,
        page: page
    }
}

const store_special_offers_tourId = (tour_id) => {
    return {
        type: STORE_SPECIAL_OFFERS_TOURID,
        tour_id: tour_id
    }
}

