import firebase from 'react-native-firebase';
import http from '../../utils/http';
import {AsyncStorage} from 'react-native'

import {setMessage, set_loader, show_password_popup, show_google_popup} from './indexAction';
import {firebaseSignIn, signIn, signInToApp} from './userAction';

import {SET_PENDING_CREDINTIAL, SET_FIREBASE_USER} from './actionTypes';

export const signInWithEmailAndPassword = (userData) => {
    return (dispatch, getState) => {
        dispatch(set_loader(true));
        firebase.auth().signInAndRetrieveDataWithEmailAndPassword(userData.email, userData.password).then((response) => {
            if(response && response.user){
                dispatch(set_loader(false));
                dispatch(signInToApp(userData));
            } else {
                dispatch(set_loader(false));
                dispatch(setMessage(true, 'failed-to-login'));
            }
        }).catch((err) => {
            dispatch(set_loader(false));
            dispatch(setMessage(true, 'failed-to-login'));
        })
    }
}

export const signInWithCustomToken = (data, link) => {
    return (dispatch, getState) => {
        firebase.auth().signInAndRetrieveDataWithCustomToken(data.customToken).then((response) => {
            if(response && response.user){
                if(link){
                    const credential = getState().firebase.pendingCredential;
                    if(credential){
                        response.user.linkAndRetrieveDataWithCredential(credential).then((resp) => {
                            dispatch(getIdToken(resp.user));
                        }).catch(err => {
                            dispatch(set_loader(false));
                            dispatch(setMessage(true, 'failed-to-login'));
                        })
                    } else {
                        dispatch(set_loader(false));
                        dispatch(setMessage(true, 'failed-to-login'));
                    }
                } else {
                    dispatch(set_loader(false));
                    dispatch(signInToApp(data));
                }
            } else {
                dispatch(set_loader(false));
                dispatch(setMessage(true, 'failed-to-login'));
            }
        }).catch((err) => {
            dispatch(set_loader(false));
            dispatch(setMessage(true, 'failed-to-login'));
        })
    }
}

export const signInWithGoogle = (idToken, accessToken, link) => {
    return (dispatch, getState) => {
        dispatch(set_loader(true));
        const credential = firebase.auth.GoogleAuthProvider.credential(idToken, accessToken);
        firebase.auth().signInAndRetrieveDataWithCredential(credential).then(response => {
            if(response && response.user){
                if(link){
                    const pendingCredential = getState().firebase.pendingCredential;
                    response.user.linkAndRetrieveDataWithCredential(pendingCredential).then((resp) => {
                        dispatch(getIdToken(resp.user));
                    }).catch(err => {
                        dispatch(set_loader(false));
                        dispatch(setMessage(true, 'failed-to-login'));
                    })
                } else {
                    dispatch(getIdToken(response.user));
                }
            } else {
                dispatch(set_loader(false));
                dispatch(setMessage(true, 'failed-to-login'));
            }
        }).catch(err => {
            dispatch(set_loader(false));
            dispatch(setMessage(true, 'failed-to-login'));
        })
    }
    
}

export const signInWithFacebook = (accessToken) => {
    return (dispatch, getState) => {
        dispatch(set_loader(true));
        const credential = firebase.auth.FacebookAuthProvider.credential(accessToken);
        dispatch(setPendingCredintial(credential));
        
        firebase.auth().signInAndRetrieveDataWithCredential(credential).then(response => {
            if(response && response.user){
                dispatch(getIdToken(response.user));
            }
        }).catch((err) => {
            if (err.code === 'auth/account-exists-with-different-credential') {
                
                fetch('https://graph.facebook.com/v2.5/me?fields=email&access_token=' + accessToken)
                    .then((response) => response.json())
                    .then((fbUser) => {
                        const email = fbUser.email;
                        firebase.auth().fetchSignInMethodsForEmail(email).then((providers) => {
                            dispatch(set_loader(false));
                            if(providers){
                                if(providers.includes("google.com")){
                                    dispatch(show_google_popup(true, email));
                                } else {
                                    dispatch(show_password_popup(true, email));
                                }
                            } else {
                                dispatch(setMessage(true, 'failed-to-login'));
                            }
                        }).catch(err => {
                            dispatch(set_loader(false));
                            dispatch(setMessage(true, 'failed-to-login'));
                        })
                    }).catch(() => {
                        dispatch(set_loader(false));
                        dispatch(setMessage(true, 'failed-to-login'));
                    })
            } else {
                dispatch(set_loader(false));
                dispatch(setMessage(true, 'failed-to-login'));
            }
        })
    }
}

const getIdToken = (user) => {
    return (dispatch, getState) => {
        user.getIdToken().then((token) => {
            dispatch(firebaseSignIn(token));
        }).catch(err => {
            dispatch(set_loader(false));
            dispatch(setMessage(true, 'failed-to-login'));
        })
    }
}

export const setPendingCredintial = (credintial) => {
    return {
        type: SET_PENDING_CREDINTIAL,
        credintial: credintial
    }
}

export const setFirebaseUser = (user) => {
    return {
        type: SET_FIREBASE_USER,
        user: user
    }
}