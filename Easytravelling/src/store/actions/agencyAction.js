
import {STORE_AGENCIES, STORE_AGENCIES_PAGE, SET_AGENCY_LOADER, SET_AGENCIES_RESET_LOADER, CHANGE_AGENCY_RATE, STORE_AGENCY} from './actionTypes';
import {setMessage} from './indexAction';

import http from '../../utils/http';

import config from '../../config/config';

export const get_agencies = (reset) => {
    return (dispatch, getState) => {        
        if(!getState().index.connection){
            return;
        }

        let offset = getState().agencies.page;
        offset = reset ? 0 : offset;

        if(offset !== 0){
            dispatch(set_agencies_loader(true));
        } else if (reset) {
            dispatch(set_agencies_reset_loader(true));
        }
        
        http({
            "action": "apet_get_tour_agencies",
            "limit" : config.PERPAGE,
            "offset": offset
        }, offset !== 0 || reset).then((resp) => {
            if(offset !== 0){
                dispatch(set_agencies_loader(false));
            } else if (reset) {
                dispatch(set_agencies_reset_loader(false));
            }
            if (resp.success === "true") {
                if(resp.data){
                    dispatch(store_agencies(resp.data, reset));
                    dispatch(store_agencies_page(offset  + config.PERPAGE));
                }
            } else {
                dispatch(store_agencies([]))
            }
        }).catch(() => {
            if(offset !== 0){
                
                dispatch(set_agencies_loader(false));
            } else if (reset) {
                dispatch(set_agencies_reset_loader(false));
            }
        })
    }
}

export const reset_agencies = (offset) => {
    return dispatch => {
        dispatch(store_agencies(null))
        dispatch(store_agencies_page(0))
    }
}

export const get_agency = (agency_id) => {
    return (dispatch, getState) => {
        http({
            "action": "apet_get_single_tour_agency",
            'tour-id': agency_id
        }, true, true).then((resp) => {
            if (resp.success === "true") {
                dispatch(store_agency(resp.data))
            } 
        })
    }
}

export const reset_agency = () => {
    return dispatch => {
        dispatch(store_agency(null));
    }
}

export const rate = (agency_id, rate) => {
    return (dispatch, getState) => {
        http({
            "action": "apet_rate_tour_agency",
            'tour-id': agency_id,
            'rate': rate
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(setMessage(true, 'thank-you-for-rating'));
                dispatch(change_agency_rate(agency_id, rate))
            } else {
                dispatch(setMessage(true, 'failed-to-rate'));
            }
        })
    }
}

const change_agency_rate = (agency_id, rate) => {
    return {
        type: CHANGE_AGENCY_RATE,
        agency_id: agency_id,
        rate: rate
    }
}

const set_agencies_loader = (loading) => {
    return {
        type: SET_AGENCY_LOADER,
        loading: loading
    }
}

const set_agencies_reset_loader = (loading) => {
    return {
        type: SET_AGENCIES_RESET_LOADER,
        loading: loading
    }
}

const store_agencies = (agencies, reset) => {
    return {
        type: STORE_AGENCIES,
        agencies: agencies,
        reset: reset
    }
}

const store_agency = (agency) => {
    return {
        type: STORE_AGENCY,
        agency: agency
    }
}

const store_agencies_page = (page) => {
    return {
        type: STORE_AGENCIES_PAGE,
        page: page
    }
}

