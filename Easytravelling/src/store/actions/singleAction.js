import { STORE_SINGLE_OFFER, STORE_SINGLE_REQUEST } from '../actions/actionTypes';
import {setMessage} from './indexAction';

import http from '../../utils/http';

export const get_offer = (id) => {
    return (dispatch, getState) => {
        if(!getState().index.connection){
            return;
        }

        dispatch(store_offer(null));
        http({
            "action": "apet_get_agency_offer",
            "offer-id": id
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(store_offer(resp.data));
            } else {
                dispatch(setMessage(true, 'something-went-wrong'));
            }
        })
    }
}

export const set_offer_seen = (id) => {
    return (dispatch, getState) => {
        http({
            "action": "apet_update_offers_seen_status",
            "offer_id": id,
            'seen_status': 'seen',
        }, true, true)
    }
}

export const get_request = (id) => {
    return (dispatch, getState) => {
        if(!getState().index.connection){
            return;
        }

        dispatch(store_request(null));
        http({
            "action": "apet_get_single_request",
            "request-id": id
        }).then((resp) => {
            if (resp.success === "true") {
                dispatch(store_request(resp.data));
                console.log(resp)
            } else {
                dispatch(setMessage(true, 'something-went-wrong'));
            }
        })
    }
}
const store_offer = (offer) => {
    return {
        type: STORE_SINGLE_OFFER,
        data: offer
    }
}

const store_request = (request) => {
    return {
        type: STORE_SINGLE_REQUEST,
        data: request
    }
}
