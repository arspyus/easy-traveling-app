
import {show_bonus_popup, setMessage} from './indexAction';
import { setUser } from './userAction';

import http from '../../utils/http';

export const getBonus = () => {

    return (dispatch, getState ) => {
        dispatch(show_bonus_popup(false));
        http({
            "action": "apet_get_bonus"
        }).then((resp) => {
            if (resp.success === "true") {
                
                let prevData = getState().user.user;
                let userData = {
                    ...prevData,
                    ...resp.data
                }
                dispatch(setUser(userData));
                dispatch(show_bonus_popup(true));
            } else {
                dispatch(setMessage(true, 'failed-toget-bonus'));
            }
        })
    }
}
