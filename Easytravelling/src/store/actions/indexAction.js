import {
    SET_LANGUAGE,
    SET_LOADING,
    SET_MESSAGE,
    SET_TOAST_SWIPING,
    SET_CURRENT_SCREEN,
    SET_NAVIGATION,
    SET_PREV_PAGE,
    SET_INTRO,
    SET_CONNECTION,
    SET_CONNECTION_CHANGED,
    SET_AVATAR,
    SET_CAN_SHOW_INTRO,
    SET_APP_SHOULD_UPGRADE,
    SET_IOS_TOOLBAR_HEIGHT,
    SHOW_BONUS_POPUP,
    SHOW_AUTH_POPUP,
    SHOW_CONFIRMATION_POPUP,
    SHOW_EMAIL_POPUP,
    SHOW_PASSWORD_POPUP,
    SHOW_GOOGLE_POPUP
} from './actionTypes';

import { AsyncStorage } from "react-native";
import http from '../../utils/http';
import checkForMajorUpdate from '../../utils/checkForMajorUpdate';

/* lang actions */
export const set_loader = loading => {
    return {
        type: SET_LOADING,
        loading: loading
    }
}

/* lang actions */
export const store_language = lang => {
    return {
        type: SET_LANGUAGE,
        lang: lang
    }
}

export const setLanguage = (lang) => {
    return dispatch => {
        if (lang) {
            AsyncStorage.setItem('easytraveling:lang', lang).then(() => {
                dispatch(store_language(lang))
            }).catch(() => {
                dispatch(store_language(lang));
            })
        }
    }
}

export const setMessage = (visibility, message) => {
    return {
        type: SET_MESSAGE,
        visibility: visibility,
        message: message
    }
}

export const setToastSwiping = (swiping) => {
    return {
        type: SET_TOAST_SWIPING,
        swiping: swiping
    }
}


export const setNavigation = (nav) => {
    return {
        type: SET_NAVIGATION,
        nav: nav
    }
}

export const setPrevPage = (page) => {
    return {
        type: SET_PREV_PAGE,
        page: page
    }
}

export const setIntro = () => {
    return dispatch => {
        AsyncStorage.setItem('easytraveling:intro', "true").then(() => {
            dispatch(store_intro(false))
        }).catch((er) => {
            dispatch(store_intro(true));
        })
    }
}

export const checkExtraData = () => {
    
    return (dispatch, getState) => {
        http({
            'action': 'apet_get_extra_data'
        }, true, true).then((resp) => {
            if (resp.success === "true") {
                if (checkForMajorUpdate(resp.data.app_min_version)) {
                    dispatch(app_should_upgrade(true));
                } 
            }
        })
    }
}

export const set_ios_toolbar_height = (height, space) => {
    return {
        type: SET_IOS_TOOLBAR_HEIGHT,
        height: height,
        space: space
    }
}


export const store_intro = (intro) => {
    return {
        type: SET_INTRO,
        intro: intro
    }
}

export const store_connection = (connection) => {
    return {
        type: SET_CONNECTION,
        connection: connection
    }
}

export const store_connection_changed = () => {
    return {
        type: SET_CONNECTION_CHANGED,
        changed: true
    }
}

export const set_current_screen = (screen) => {
    return {
        type: SET_CURRENT_SCREEN,
        currentScreen: screen
    }
}

export const set_avatar = (avatar) => {
    return {
        type: SET_AVATAR,
        avatar: avatar
    }
}

export const can_show_intro = () => {
    return {
        type: SET_CAN_SHOW_INTRO
    }
}

export const app_should_upgrade = (upgrade) => {
    return {
        type: SET_APP_SHOULD_UPGRADE,
        upgrade: upgrade
    }
}

export const show_bonus_popup = (show) => {
    return {
        type: SHOW_BONUS_POPUP,
        show: show
    }
}

export const show_auth_popup = (show, pendingToRedirectData) => {
    return {
        type: SHOW_AUTH_POPUP,
        show: show,
        pendingToRedirectData: pendingToRedirectData
    }
}

export const show_confirmation_popup = (show, message) => {
    return {
        type: SHOW_CONFIRMATION_POPUP,
        show: show,
        verificationMessage: message
    }
}

export const show_email_popup = (show) => {
    return {
        type: SHOW_EMAIL_POPUP,
        show: show
    }
}

export const show_password_popup = (show, email) => {
    return {
        type: SHOW_PASSWORD_POPUP,
        show: show,
        email: email
    }
}

export const show_google_popup = (show, email) => {
    return {
        type: SHOW_GOOGLE_POPUP,
        show: show,
        email: email
    }
}





