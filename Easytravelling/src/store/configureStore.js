import {createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

import indexReducer from './reducers/index';
import userReducer from './reducers/user';
import offersReducer from './reducers/offer';
import agenciesReducer from './reducers/agency';
import makeRequestReducer from './reducers/makeRequest';
import myRequestReducer from './reducers/myRequest';
import singleReducer from './reducers/single';
import bookingReducer from './reducers/booking';
import firebaseReducer from './reducers/firebase';

const rootReducer = combineReducers({
    index: indexReducer,
    user: userReducer,
    offers: offersReducer,
    agencies: agenciesReducer,
    makeRequest: makeRequestReducer,
    myRequest: myRequestReducer,
    single: singleReducer,
    booking: bookingReducer,
    firebase: firebaseReducer
});

const configureStore = () => {
    return createStore(rootReducer, applyMiddleware(thunk))
}

const store = configureStore();

export default store;