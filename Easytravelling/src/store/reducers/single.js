import { STORE_SINGLE_OFFER, STORE_SINGLE_REQUEST } from '../actions/actionTypes';

const initialState = {
    offers: null,
    request: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_SINGLE_OFFER:
            return {
                ...state,
                offers: action.data
            }
        case STORE_SINGLE_REQUEST:
            return {
                ...state,
                request: action.data
            }
        default:
            return state;
    }
}

export default reducer;