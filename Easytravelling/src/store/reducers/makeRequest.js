import {SELECT_AGENCY, SELECT_LAST_AGENCY, SET_MAKE_REQUEST_PROCESS, SELECT_TYPE} from '../actions/actionTypes';

const initialState = {
    selectedAgency: null,
    lastSelectedAgency: null,
    makeRequestProcess: false,
    type: null
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case SELECT_AGENCY:
            return {
                ...state,
                selectedAgency: action.agency
            }
        case SELECT_LAST_AGENCY:
            return {
                ...state,
                lastSelectedAgency: action.agency
            }
        case SELECT_TYPE:
            return {
                ...state,
                type: action.request_type
            }
        case SET_MAKE_REQUEST_PROCESS: 
            return {
                ...state,
                makeRequestProcess: action.process
            }
        default:
            return state;
    }
}

export default reducer;