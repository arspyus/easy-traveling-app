import {
    SET_LANGUAGE, 
    SET_LOADING, 
    SET_MESSAGE, 
    SET_TOAST_SWIPING, 
    SET_NAVIGATION, 
    SET_INTRO, 
    SET_CONNECTION, 
    SET_CONNECTION_CHANGED, 
    SET_CURRENT_SCREEN, 
    STORE_FCMTOKEN, 
    SET_UNREAD_NOTIFICATION,
    CLEAR_UNREAD_NOTIFICATION,
    SET_PREV_PAGE,
    SET_AVATAR,
    SET_CAN_SHOW_INTRO,
    SET_APP_SHOULD_UPGRADE,
    SET_IOS_TOOLBAR_HEIGHT,
    SHOW_BONUS_POPUP,
    SHOW_AUTH_POPUP,
    SHOW_CONFIRMATION_POPUP ,
    SHOW_EMAIL_POPUP ,
    SHOW_PASSWORD_POPUP,
    SHOW_GOOGLE_POPUP
} from '../actions/actionTypes';

import isLandscape from '../../utils/isLandscape';
import isIphoneX from '../../utils/iphoneX';

const initialState = {
    lang: "en",
    isLoading: false,
    isError: false,
    message: "",
    toastSwiping: false,
    activeNav: null,
    prevPage: null,
    intro: true,
    connection: "unknown",
    connectionChanged: false,
    fcm_token: null,
    currenScreen: null,
    unreadOffer: false,
    unreadSpecialOffer: false,
    avatar: null,
    canShowIntro: false,
    iosDatepicker: null,
    appShouldUpgrade: false,
    ios_toolbar_height: !isLandscape() ? isIphoneX() ? 24 : 20 : 0,
    iphoneXSpace: {
        bottom: isIphoneX() ? !isLandscape() ? 34 : 0 : 0,
        left: isIphoneX() ? !isLandscape() ? 0 : 44 : 0,
        right: isIphoneX() ? !isLandscape() ? 0 : 34 : 0
    },
    showBonusPopup: false,
    showAuthPopup: false,
    showConfirmationPopup: false,
    showConfirmationMessage: "",
    pendingToRedirectData: null,
    showEmailPopup: false,
    showPasswordPopup: false,
    showGooglePopup: false,
    passwordPromptEmail: ""
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case SET_LANGUAGE :
            const lang = action.lang === "English" || action.lang === "en"? "en" : action.lang === "Русский" || action.lang === "ru" ? "ru" : "hy"
            return {
                ...state,
                lang: lang
            }
        case SET_MESSAGE:
            return {
                ...state,
                isError: action.visibility,
                message: action.message
            }
        case SET_TOAST_SWIPING:
            return {
                ...state,
                toastSwiping: action.swiping
            }
        case SET_LOADING:
            return {
                ...state,
                isLoading: action.loading
            }
        case SET_NAVIGATION:
            return {
                ...state,
                activeNav: action.nav
            }
        case SET_PREV_PAGE:
            return {
                ...state,
                prevPage: action.page
            }
        case SET_INTRO:
            return {
                ...state,
                intro: action.intro
            }
        case SET_CONNECTION:
            return {
                ...state,
                connection: action.connection
            }
        case SET_CONNECTION_CHANGED:
            return {
                ...state,
                connectionChanged: action.changed
            }
        case STORE_FCMTOKEN:
            return {
                ...state,
                fcm_token: action.token
            }
        case SET_CURRENT_SCREEN:
            return {
                ...state,
                currentScreen: action.currentScreen
            }
        case SET_UNREAD_NOTIFICATION:
            return {
                ...state,
                unreadOffer: action.notificationType === "offer" ? true : state.unreadOffer,
                unreadSpecialOffer: action.notificationType === "special-offer" ? true : state.unreadSpecialOffer
            }
        case CLEAR_UNREAD_NOTIFICATION:
            return {
                ...state,
                unreadOffer: action.notificationType === "offer" ? false : state.unreadOffer,
                unreadSpecialOffer: action.notificationType === "special-offer" ? false : state.unreadSpecialOffer
            }
        case SET_AVATAR:
            return {
                ...state,
                avatar: action.avatar
            }
        case SET_CAN_SHOW_INTRO:
            return {
                ...state,
                canShowIntro: true
            }
        case SET_APP_SHOULD_UPGRADE: 
            return {
                ...state,
                appShouldUpgrade: action.upgrade
            }
        case SET_IOS_TOOLBAR_HEIGHT:
            return {
                ...state,
                ios_toolbar_height: action.height,
                iphoneXSpace: action.space === null ? state.iphoneXSpace : action.space
            }
        case SHOW_BONUS_POPUP:
            return {
                ...state,
                showBonusPopup: action.show,
            }
        case SHOW_AUTH_POPUP:
            return {
                ...state,
                showAuthPopup: action.show,
                pendingToRedirectData: action.pendingToRedirectData ? action.pendingToRedirectData : action.pendingToRedirectData === false ? state.pendingToRedirectData : null
            }
        case SHOW_CONFIRMATION_POPUP:
            return {
                ...state,
                showConfirmationPopup: action.show,
                showConfirmationMessage: action.verificationMessage
            }
        case SHOW_EMAIL_POPUP:
            return {
                ...state,
                showEmailPopup: action.show
            }
        case SHOW_PASSWORD_POPUP:
            return {
                ...state,
                showPasswordPopup: action.show,
                passwordPromptEmail: action.email
            }
        case SHOW_GOOGLE_POPUP:
            return {
                ...state,
                showGooglePopup: action.show,
                passwordPromptEmail: action.email
            }
        default:
            return state;
    }
}

export default reducer;