import {SET_BOOKING_PROCESS } from '../actions/actionTypes';

const initialState = {
    isBooking: false
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case SET_BOOKING_PROCESS :
            return {
                ...state,
                isBooking: action.isBooking
            }
        default:
            return state;
    }
}

export default reducer;