import {STORE_MY_REQUESTS, RESET_MY_REQUESTS, REMOVE_REQUESTS, SET_DELETE_REQUEST_PROCESS, SET_MY_REQUEST_FILTER, SET_MY_REQUESTS_RESET_LOADER} from '../actions/actionTypes';

const initialState = {
    requests: null,
    deleteRequestProcess: false,
    filter: "all",
    reset_loader: false
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case STORE_MY_REQUESTS:
            return {
                ...state,
                requests: action.data
            }
        case RESET_MY_REQUESTS:
            return {
                ...state,
                requests: null
            }
        case REMOVE_REQUESTS: {
            return {
                ...state,
                requests: state.requests ? state.requests.filter(el => { return (el.request_id != action.id && action.id !== "all") || el.status === "booked"}): null
            }
        }
        case SET_DELETE_REQUEST_PROCESS: {
            return {
                ...state,
                deleteRequestProcess: action.process
            }
        }
        case SET_MY_REQUEST_FILTER: {
            return {
                ...state,
                filter: action.filter
            }
        }
        case SET_MY_REQUESTS_RESET_LOADER: {
            return {
                ...state,
                reset_loader: action.isLoading
            }
        }
        default:
            return state;
    }
}

export default reducer;