import { SET_PENDING_CREDINTIAL, SET_FIREBASE_USER } from '../actions/actionTypes';

const initialState = {
    firebase_user: null,
    pendingCredential: null
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case SET_FIREBASE_USER:
            return {
                ...state,
                firebase_user: action.user
            }
        case SET_PENDING_CREDINTIAL:
            return {
                ...state,
                pendingCredential: action.credintial
            }
        default:
            return state;
    }
}

export default reducer;