import {STORE_SPECIAL_OFFERS, SET_OFFERS_LOADER, STORE_SPECIAL_OFFERS_PAGE, SET_OFFERS_RESET_LOADER, STORE_SPECIAL_OFFERS_TOURID} from '../actions/actionTypes';
import removeDublicates from '../../utils/removeDublicates';

const initialState = {
    offers: null,
    special_count: 0,
    offers_loading : false,
    offers_reset_loading: false,
    page: 0,
    tour_id: null
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case STORE_SPECIAL_OFFERS: 
            return {
                ...state,
                offers: action.offers === null ? null : state.offers && !action.reset ? removeDublicates(state.offers.concat(action.offers), "offer_id") : [].concat(action.offers)
            }
        case SET_OFFERS_LOADER:
            return {
                ...state,
                offers_loading: action.loading
            }
        case SET_OFFERS_RESET_LOADER:
            return {
                ...state,
                offers_reset_loading: action.loading
            }
        case STORE_SPECIAL_OFFERS_PAGE:
            return {
                ...state,
                page: action.page
            }
        case STORE_SPECIAL_OFFERS_TOURID:
            return {
                ...state,
                tour_id: action.tour_id
            }
        default:
            return state;
    }
}

export default reducer;