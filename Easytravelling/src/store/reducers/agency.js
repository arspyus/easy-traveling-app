import {STORE_AGENCIES, SET_AGENCY_LOADER, SET_AGENCIES_RESET_LOADER, STORE_AGENCIES_PAGE, CHANGE_AGENCY_RATE, STORE_AGENCY} from '../actions/actionTypes';
import removeDublicates from '../../utils/removeDublicates';

const initialState = {
    agencies: null,
    page: 0,
    agencies_loading : false,
    agencies_reset_loading: false,
    agency: null
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case STORE_AGENCIES: 
            return {
                ...state,
                agencies: action.agencies === null ? null : state.agencies && !action.reset ? removeDublicates(state.agencies.concat(action.agencies), "id") : [].concat(action.agencies)
            }
        case STORE_AGENCIES_PAGE:
            return {
                ...state,
                page: action.page
            }
        case SET_AGENCY_LOADER:
            return {
                ...state,
                agencies_loading: action.loading
            }
        case SET_AGENCIES_RESET_LOADER:
            return {
                ...state,
                agencies_reset_loading: action.loading
            }
        case CHANGE_AGENCY_RATE:
            return {
                ...state,
                agencies: state.agencies === null ? null : state.agencies.map(ag => {return ag.id !== action.agency_id ? ag : {...ag, is_rated: action.rate}})
            }
        case STORE_AGENCY:
            return {
                ...state,
                agency: action.agency
            }
        default:
            return state;
    }
}

export default reducer;