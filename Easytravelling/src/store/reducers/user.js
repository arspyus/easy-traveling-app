import {
    STORE_UID, 
    STORE_USER, 
    SET_SIGNIN_PROCESS, 
    SET_LOGOUT_PROCESS, 
    CHANGE_PASSWORD_PROCESS, 
    RESET_PASSWORD_PROCESS, 
    UPDATE_USER_PROCESS, 
    DEACTIVATE_PROCESS, 
    SET_GUEST,
    SET_USER_DATA_IS_LOADING
} from '../actions/actionTypes';
import { AsyncStorage } from "react-native"

const initialState = {
    user: null,
    uid: null,
    isGuest: false,
    userDataIsLoading: false,
    signInProcess: false,
    logoutProcess: false,
    resetPasswordProcess: false,
    updateUserProcess: false,
    changePasswordProcess: false,
    deactivateProcess: false
}

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case STORE_UID :
            return {
                ...state,
                uid: action.uid
            }
        case STORE_USER :
            return {
                ...state,
                user: action.user
            }
        case SET_SIGNIN_PROCESS :
            return {
                ...state,
                signInProcess: action.process
            }
        case SET_LOGOUT_PROCESS:
            return {
                ...state,
                logoutProcess: action.process
            }
        case RESET_PASSWORD_PROCESS:
            return {
                ...state,
                resetPasswordProcess: action.process
            }
        case UPDATE_USER_PROCESS :
            return {
                ...state,
                updateUserProcess: action.process
            }
        case CHANGE_PASSWORD_PROCESS :
            return {
                ...state,
                changePasswordProcess: action.process
            }
        case DEACTIVATE_PROCESS :
            return {
                ...state,
                deactivateProcess: action.process
            }
        case SET_GUEST :
            return {
                ...state,
                isGuest: action.isGuest
            }
        case SET_USER_DATA_IS_LOADING:
            return {
                ...state,
                userDataIsLoading: action.isLoading
            }
        default:
            return state;
    }
}

export default reducer;