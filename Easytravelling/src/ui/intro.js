import React, { Component } from 'react';
import { View, Text, Dimensions, Animated, TouchableWithoutFeedback } from 'react-native';
import translate from '../utils/translate';
import globalStyles from '../utils/style';
import isLandscape from '../utils/isLandscape';

class APIntro extends Component {
  state = {
    waveAnimation: new Animated.Value(0),
    mainAnimation: new Animated.Value(0),
    opened: false,
    width: isLandscape() ? (Dimensions.get('window').height - 10) * 2 : (Dimensions.get('window').width - 10) * 2
  };

  componentDidMount() {
    Dimensions.addEventListener('change', this.changeDimensions);
    this.startWaveAnimation();
    this.open()
  }

  componentWillUnmount() {
      Dimensions.removeEventListener('change', this.changeDimensions)
  }

  changeDimensions = () => {
      this.setState({
        width: isLandscape() ? (Dimensions.get('window').height - 10) * 2 : (Dimensions.get('window').width - 10) * 2
      });
  }

  startWaveAnimation() {
    Animated.loop(
      Animated.timing(this.state.waveAnimation, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      })).start()
  }

  animate(toValue, cb) {
    Animated.timing(this.state.mainAnimation, {
      toValue: toValue,
      duration: 300,
      useNativeDriver: true
    }).start(cb)
    this.setState({
      ...this.state,
      opened: toValue === 1 ? true : false
    })
  }

  open() {
    this.animate(1)
  }

  close() {
    this.animate(0, () => {
      this.props.onClose()
    })
  }

  render() {
    const waveOpacity = this.state.waveAnimation.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [1, 0, 0],
    });

    const waveScale = this.state.waveAnimation.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [1, 1.5, 1.5],
    });

    const mainOpacity = this.state.mainAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0.95],
    });

    const mainScale = this.state.mainAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });

    const orientation = isLandscape() ? "landscape" : "portrait";

    return (
      
      <TouchableWithoutFeedback onPress={() => this.close()}>
        <View style={style.wrapper}>
          <Animated.View style={[style.container, {
            opacity: mainOpacity,
            width: this.state.width,
            height: this.state.width,
            bottom: -(this.state.width / 2),
            borderRadius: this.state.width,
            transform: [
              { scale: mainScale }
            ]
          },
          orientation === "portrait" ? {bottom: -(this.state.width / 2), left: 10} : {right: -(this.state.width / 2), top: 10}
          ]}>
            <View style={[style.content, { paddingBottom: this.state.width / 2 + 112 }]}>
              <Text style={[style.heading, globalStyles().fontBold]}>{translate["feature-intro-title"][this.props.lang]}</Text>
              <Text style={[style.text, globalStyles().fontBold]}>{translate["feature-intro-content"][this.props.lang]}</Text>
            </View>
          </Animated.View>
          <Animated.View style={[style.whiteCircle, {
            opacity: mainOpacity,
            transform: [
              { scale: mainScale }
            ]
          }]}></Animated.View>
          <Animated.View
            style={[style.whiteCircle, {
              opacity: this.state.opened ? waveOpacity : mainOpacity,
              transform: [
                { scale: this.state.opened ? waveScale : mainScale }
              ]
            }]} >
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const style = {
  wrapper: {
    position: "absolute",
    backgroundColor: "transparent",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    zIndex: 1000
  },
  container: {
    position: 'absolute',
    backgroundColor: '#7cb342',
    zIndex: 1000,
    justifyContent: 'flex-end',
  },

  whiteCircle: {
    width: 112,
    height: 112,
    position: 'absolute',
    backgroundColor: '#fff',
    borderRadius: 56,
    bottom: -4,
    right: -4,
    zIndex: 1001,
  },

  wave: {
    width: '100%',
    height: '100%',
  },

  content: {
    paddingLeft: 76,
    width: "50%"
  },

  heading: {
    fontSize: 24,
    color: "#fff",
    lineHeight: 27,
    paddingTop: 16
  },

  text: {
    fontSize: 15,
    color: "#fff",
    lineHeight: 24,
    //paddingVertical: 15
  }
};


export default APIntro;
