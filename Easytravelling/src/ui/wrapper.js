import React from 'react';
import {View, StyleSheet} from 'react-native';
const APWrapper = (props) => {
    return (
        <View style={[styles.container, props.containerStyles]}>
             <View style={[styles.inner, props.style]}>{props.children}</View>
        </View>
    )
}

const styles ={
    container: {
        width: "100%",
        alignItems: "center",
        paddingBottom: 10,
        paddingTop: 10,
        paddingHorizontal: 10
    },

    inner: {
        width: "100%"
    }
}

export default APWrapper;