import React, { Component } from 'react';
import { View, StyleSheet, Animated, TouchableWithoutFeedback, Text } from 'react-native';
import APIcon from './icon';

import translate from '../utils/translate';
import globalStyles from '../utils/style';
import throttle from '../utils/throttle';

class APPlus extends Component {
    state = {
        opened: false,
        animation: new Animated.Value(0)
    }

    animate(to) {
        Animated.spring(this.state.animation, {
            toValue: to,
            duration: 400,
            useNativeDriver: true
        }).start()
    }

    toggle = () => {
        const isOpened = this.state.opened;
        this.animate(isOpened ? 0 : 1);
        this.setState({
            ...this.state,
            opened: !isOpened
        })

        if (this.props.onClick) {
            this.props.onClick();
        }
    }

    render() {
        const rotateAnimation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "135deg"],
        });

        const scaleXAnimation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0.4, 1],
        });

        const scaleYAnimation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0.4, 1],
        });

        const translateXAnimation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0],
        });

        const translateYAnimation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [40, 0],
        });

        const opacityAnimation = this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
        });


        return (
            <View style={style.wrapper} pointerEvents="box-none">
                <TouchableWithoutFeedback onPress={this.toggle}>
                    <View style={[style.plus, globalStyles().shadow]} >
                        <Animated.View style={[style.icon, { transform: [{ rotate: rotateAnimation }] }]} >
                            <View style={style.iconVertical}></View>
                            <View style={style.iconHorizontal}></View>
                        </Animated.View>
                    </View>
                </TouchableWithoutFeedback>
                <View style={style.floatingContainer} pointerEvents="box-none">
                    <TouchableWithoutFeedback onPress={throttle(() => { this.props.onNavigate("outgoing-tour") })}>
                        <Animated.View style={[style.floating, {
                            opacity: opacityAnimation,
                            transform: [
                                { translateY: translateYAnimation },
                                { translateX: translateXAnimation },
                                { scaleX: scaleXAnimation },
                                { scaleY: scaleYAnimation }
                            ]
                        }]} pointerEvents={this.state.opened ? "auto" : "none"}>
                            <View style={[style.floatingTextView, globalStyles().shadow]}>
                                <Text>{translate["outgoing-tours-btn"][this.props.lang]}</Text>
                            </View>
                            <View style={[style.smallIcon, globalStyles().shadow]}>
                                <APIcon name="world" color="#fff" size={22}  />
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={throttle(() => { this.props.onNavigate("domestic-tour") })}>
                        <Animated.View style={[style.floating, {
                            opacity: opacityAnimation,
                            transform: [
                                { translateY: translateYAnimation },
                                { translateX: translateXAnimation },
                                { scaleX: scaleXAnimation },
                                { scaleY: scaleYAnimation }
                            ]
                        }]} pointerEvents={this.state.opened ? "auto" : "none"}>
                            <View style={[style.floatingTextView, globalStyles().shadow]}>
                                <Text>{translate["domestic-tours-btn"][this.props.lang]}</Text>
                            </View>
                            <View style={[style.smallIcon, globalStyles().shadow]}>
                                <APIcon name="armenia" color="#fff" size={22} />
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    wrapper: {
        backgroundColor: "transparent",
        position: 'absolute',
        width: "100%",
        height: "100%",
        bottom: 0,
        right: 0,
        zIndex: 2000
    },
    plus: {
        width: 56,
        height: 56,
        borderRadius: 28,
        backgroundColor: '#7cb342',
        position: 'absolute',
        bottom: 24,
        right: 24,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1010,
    },

    icon: {
        width: 16,
        height: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },

    iconVertical: {
        backgroundColor: "#fff",
        height: "100%",
        position: "absolute",
        top: 0,
        width: 2
    },

    iconHorizontal: {
        backgroundColor: "#fff",
        width: "100%",
        position: "absolute",
        left: 0,
        height: 2
    },

    floatingContainer: {
        position: "absolute",
        bottom: 85,
        right: 26
    },

    floating: {
        flexDirection: "row",
        marginBottom: 10,
        alignItems: "center",
        justifyContent: "flex-end",
        paddingBottom: 5,
        paddingHorizontal: 5
    },
    floatingTextView: {
        backgroundColor: '#fff',
        borderRadius: 2,
        paddingHorizontal: 8,
        paddingVertical: 0,
        marginRight: 10,
        justifyContent: "center",
        height: 24
    },

    smallIcon: {
        width: 40,
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#7cb342'
    }
})

export default APPlus;