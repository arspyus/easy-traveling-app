import React , {Component} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, Keyboard, Platform} from 'react-native';

import APInput from './input';
import APDropdown from './dropdown';
import APIcon from './icon';

import globalStyles from '../utils/style';

class APSelect extends Component {

    state = {
        value: this.props.value
    }

    dropdown = null;

    showDropdown = () => {
        Keyboard.dismiss()
        this.dropdown.show()
    }

    select = (value) => {
        this.dropdown.selectByValue(value);
        this.setState({
            value: value
        })
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.value != nextProps.value){
            this.setState({
                ...this.state,
                value: nextProps.value
            },() => {
                this.select(nextProps.value)
            })
        }
    }

    onDropdownChange = (index, value) =>{
        if(this.props.disabledOptions && this.props.disabledOptions.indexOf(parseInt(index)) > -1){
            return false;
        }
        this.setState({
            ...this.state,
            value: value
        }, () => {
            if(this.props.onChangeText){
                this.props.onChangeText(value);
            }
        })
       
    }

    render() {
        return (
            
                <View style={styles.container}>
                    <TouchableWithoutFeedback onPress={this.showDropdown}>
                        <View>
                            <APInput
                                {...this.props}
                                editable={false} 
                                selectTextOnFocus={false}
                                value={this.props.options.filter(op => {return op.value === this.state.value})[0].text}
                                inputMargin={-25}
                            />
                            {Platform.OS === "ios" ? <View style={styles.overlay}></View> : null}
                            <View style={styles.icon}>
                                <APIcon size={16} color="#7cb342" name="keyboard_arrow_down"/>
                            </View>
                            <APDropdown 
                                options={this.props.options} 
                                defaultValue={this.props.value}
                                defaultIndex={this.props.options.map(op => op.value).indexOf(this.props.value)}  
                                ref={(dropdown) => { this.dropdown = dropdown; }}
                                dropdownStyle={{width: "90%", marginTop: -30}}
                                dropdownTextStyle={{color: "#7cb342"}}
                                onSelect={this.onDropdownChange}
                                disabledOptions={this.props.disabledOptions} 
                                topOffset={6}
                                bottomOffset={-26}
                            ><View style={globalStyles().hidden}></View></APDropdown> 
                        </View>
                    </TouchableWithoutFeedback>
                </View>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        marginBottom: 48,
        overflow: "hidden"
    },

    overlay: {
        width: "100%",
        height: "100%",
        position: "absolute",
        left: 0,
        top: 0
    },

    icon: {
        position: "absolute",
        right: 4,
        top: 20
    }
})

export default APSelect;