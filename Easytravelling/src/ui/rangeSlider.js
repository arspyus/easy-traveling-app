import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, TouchableHighlight, Text, PanResponder, Platform } from 'react-native';

import globalStyles from '../utils/style';

class APRangeSlider extends Component {

    state = {
        leftPressed: false,
        rightPressed: false,
        valueLeft: this.props.values[0],
        valueRight: this.props.values[1],
        positionLeft: 0,
        positionRight: 0,
        sliderLength: null,
        leftTooltipWidth: 0,
        rightTooltipWidth: 0,
        leftZIndex:10,
        rightZIndex: 20,
        stepsLabelsWidth: [],
        widthIsMeasured: false
    };

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const customPanResponder = (start, move, end) => {
            return PanResponder.create({
                onStartShouldSetPanResponder: (evt, gestureState) => true,
                onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
                onMoveShouldSetPanResponder: (evt, gestureState) => true,
                onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
                onPanResponderGrant: (evt, gestureState) => start(),
                onPanResponderMove: (evt, gestureState) => move(gestureState),
                onPanResponderTerminationRequest: (evt, gestureState) => false,
                onPanResponderRelease: (evt, gestureState) => end(gestureState),
                onPanResponderTerminate: (evt, gestureState) => end(gestureState),
                onShouldBlockNativeResponder: (evt, gestureState) => true,
            });
        };

        this._panResponderLeft = customPanResponder(
            this.startLeft,
            this.moveLeft,
            this.endLeft
        );
        this._panResponderRight = customPanResponder(
            this.startRight,
            this.moveRight,
            this.endRight,
        );
    }

    componentDidMount() {
        Dimensions.addEventListener('change', this.changeDimensions);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.changeDimensions)
    }

    changeDimensions = () => {
        this.setState({
            sliderLength: 0
        })
    }

    startLeft = () => {
        this.props.onValuesChangeStart && this.props.onValuesChangeStart();
        this.setState({
            leftPressed: !this.state.leftPressed,
        });
    };

    startRight = () => {
        this.props.onValuesChangeStart && this.props.onValuesChangeStart();
        this.setState({
            rightPressed: !this.state.rightPressed,
        });
    };

    moveLeft = gestureState => {
        const accumDistance = gestureState.dx;
        const unconfined = accumDistance + this.state.pastLeft;
        let bottom = 0;
        if(this.props.min){
            bottom = this.valueToPosition(this.props.min, this.optionsArray, this.state.sliderLength)
        }
        let trueTop = this.state.positionRight;
        let top = this.state.sliderLength;
        
        let confined = unconfined < bottom ? bottom : unconfined > top ? top : unconfined;

        let outOfRight = confined > this.state.positionRight;
      
        let value = this.positionToValue(
            confined,
            this.optionsArray,
            this.state.sliderLength,
        );

        this.setState({
            positionLeft: confined,
            positionRight : outOfRight ? confined : this.state.positionRight
        });

        if (value !== this.state.valueLeft) {
            this.setState(
                {
                    valueLeft: value,
                    valueRight: outOfRight ? value : this.state.valueRight,
                },
                () => {
                    let change = [this.state.valueLeft];
                    if (this.state.valueRight) {
                        change.push(this.state.valueRight);
                    }
                    this.props.onValuesChange && this.props.onValuesChange(change);
                },
            );
        }
    };

    moveRight = gestureState => {
        const accumDistance = gestureState.dx;
        const unconfined = accumDistance + this.state.pastRight;
        let bottom = 0 ;
        let top = this.state.sliderLength;
        if(this.props.max){
            top = this.valueToPosition(this.props.max, this.optionsArray, this.state.sliderLength)
        }

        let confined = unconfined < bottom ? bottom : unconfined > top ? top : unconfined;
        let outOfLeft = confined < this.state.positionLeft;
        let value = this.positionToValue(
            confined,
            this.optionsArray,
            this.state.sliderLength
        );

        this.setState({
            positionRight: confined,
            positionLeft: outOfLeft ? confined : this.state.positionLeft
        });
        if (value !== this.state.valueRight) {
            this.setState(
                {
                    valueRight: value,
                    valueLeft: outOfLeft ? value : this.state.valueLeft,
                },
                () => {
                    this.props.onValuesChange && this.props.onValuesChange([
                        this.state.valueLeft,
                        this.state.valueRight,
                    ]);
                },
            );
        }
    };

    endLeft = gestureState => {
        if (gestureState.moveX === 0 && this.props.onToggleLeft) {
            this.props.onToggleLeft();
            return;
        }

        this.calculateZIndex("left");

        let positionLeft = this.state.positionLeft;
        let positionRight = this.state.positionRight;
        if(this.props.showSteps){
            positionLeft = this.valueToPosition(this.state.valueLeft, this.optionsArray, this.state.sliderLength);
            positionRight = this.valueToPosition(this.state.valueRight, this.optionsArray, this.state.sliderLength)
        }

        this.setState(
            {
                pastLeft: positionLeft,
                pastRight: positionRight,
                leftPressed: !this.state.leftPressed,
                positionLeft: positionLeft,
                positionRight: positionRight
            },
            () => {
                var change = [this.valueFormatter(this.state.valueLeft)];
                if (this.state.valueRight) {
                    change.push(this.valueFormatter(this.state.valueRight));
                }
                this.props.onChange && this.props.onChange(change);
            },
        );
    };

    endRight = gestureState => {
        if (gestureState.moveX === 0 && this.props.onToggleRight) {
            this.props.onToggleRight();
            return;
        }
        this.calculateZIndex("right");
        let positionRight = this.state.positionRight;
        let positionLeft = this.state.positionLeft;
        if(this.props.showSteps){
            positionRight = this.valueToPosition(this.state.valueRight, this.optionsArray, this.state.sliderLength);
            positionLeft = this.valueToPosition(this.state.valueLeft, this.optionsArray, this.state.sliderLength)
        }

        this.setState(
            {
                rightPressed: !this.state.rightPressed,
                positionRight: positionRight,
                positionLeft: positionLeft,
                pastRight: positionRight,
                pastLeft: positionLeft
            },
            () => {
                if(!this.props.single){
                    this.props.onChange && this.props.onChange([
                        this.valueFormatter(this.state.valueLeft),
                        this.valueFormatter(this.state.valueRight),
                    ]);
                } else {
                    this.props.onChange && this.props.onChange(this.valueFormatter(this.state.valueRight));
                }
                
            },
        );
    };

    calculateZIndex = (dir) => {
        if(dir === "left"){
            this.setState({
                leftZIndex: 20,
                rightZIndex: 10
            })
        } else {
            this.setState({
                leftZIndex: 10,
                rightZIndex: 20
            })
        }
    }

    createArray = (start, end, step) => {
        let i, length, direction = start - end > 0 ? -1 : 1;
        var result = [];
        if (!step) {
            return result;
        } else {
            length = Math.abs((start - end) / step) + 1;
            for (i = 0; i < length; i++) {
                result.push(start + i * Math.abs(step) * direction);
            }
            return result;
        }
    }

    valueToPosition = (value, valuesArray, sliderLength) => {
        const index = this.closest(valuesArray, value);

        const arrLength = valuesArray.length - 1;
        const validIndex = index === -1 ? arrLength : index;
        return (sliderLength * validIndex) / arrLength;
    }

    positionToValue = (position, valuesArray, sliderLength) => {
        var arrLength;
        var index;

        if (position < 0 || sliderLength < position) {
            return null;
        } else {
            arrLength = valuesArray.length - 1;
            index = (arrLength * position) / sliderLength;
            return valuesArray[Math.round(index)];
        }
    }

    closest = (array, n) => {
        let minI = 0;
        let maxI = array.length - 1;

        if (array[minI] > n) {
            return minI;
        } else if (array[maxI] < n) {
            return maxI;
        } else if (array[minI] <= n && n <= array[maxI]) {
            let closestIndex = null;

            while (closestIndex === null) {
                const midI = Math.round((minI + maxI) / 2);
                const midVal = array[midI];

                if (midVal === n) {
                    closestIndex = midI;
                } else if (maxI === minI + 1) {
                    const minValue = array[minI];
                    const maxValue = array[maxI];
                    const deltaMin = Math.abs(minValue - n);
                    const deltaMax = Math.abs(maxValue - n);

                    closestIndex = deltaMax <= deltaMin ? maxI : minI;
                } else if (midVal < n) {
                    minI = midI;
                } else if (midVal > n) {
                    maxI = midI;
                } else {
                    closestIndex = -1;
                }
            }

            return closestIndex;
        }

        return -1;
    };

    valueFormatter = (value) => {
        return this.props.valueFormatter ? this.props.valueFormatter(value) : value;
    }

    measureWidth = (event) => {
        if (!this.state.sliderLength) {
            const sliderWidth = event.nativeEvent.layout.width ;

            this.optionsArray = this.props.optionsArray || this.createArray(this.props.from, this.props.to, this.props.step);
            this.stepLength = sliderWidth / this.optionsArray.length;
            let initialValues = null;
            if(!this.state.widthIsMeasured){
                initialValues = this.props.values.map(value =>
                    this.valueToPosition(value, this.optionsArray, sliderWidth),
                )
            } else {
                initialValues = [this.state.valueLeft, this.state.valueRight].map(value =>
                    this.valueToPosition(value, this.optionsArray, sliderWidth),
                )
            }

            this.setState({
                sliderLength: sliderWidth,
                widthIsMeasured: true,
                pastLeft: initialValues[0],
                pastRight: initialValues[1],
                positionLeft: initialValues[0],
                positionRight: initialValues[1]
            });
        }
    }

    measureStepLayoutWidth = (event, index) => {
        const stepWidth = event.nativeEvent.layout.width ;
        let stepsLabelsWidth = [...this.state.stepsLabelsWidth];
        stepsLabelsWidth[index] = Math.round(stepWidth);
        this.setState({
            stepsLabelsWidth: stepsLabelsWidth
        });
    }

    setTooltipWidth = (width, marker) => {
        if(marker === "left"){
            this.setState({leftTooltipWidth: width})
        } else {
            this.setState({rightTooltipWidth: width})
        }
    }

    render() {
        const trackOneLength = this.state.positionLeft;
        const trackThreeLength = this.state.sliderLength - this.state.positionRight;
        const trackTwoLength = this.state.sliderLength - trackOneLength - trackThreeLength;

        const markerContainerLeft = {
            left: this.state.positionLeft,
            top: this.props.hideTooltip ? 9 : 45
        };

        const markerContainerRight = {
            left: this.state.positionRight,
            top: this.props.hideTooltip ? 9 : 45
        };
    
        let leftTooltipPosition = this.state.positionLeft < this.state.sliderLength / 2 ? this.state.positionLeft - 5 : this.state.positionLeft + (!this.state.leftPressed ? 17 : 22) - this.state.leftTooltipWidth;
        let rightTooltipPosition = this.state.positionRight < this.state.sliderLength / 2 ? this.state.positionRight - 5 : this.state.positionRight + (!this.state.rightPressed ? 17 : 22) - this.state.rightTooltipWidth;
        let steps = null, lastStep = null;
        let stepLabels = null;
        if(this.props.showSteps && this.optionsArray){
            let stepTop = this.props.hideTooltip ? 9: 45 ;
            steps = this.optionsArray.map((el, index) => {
                let stepPosition = this.valueToPosition(el, this.optionsArray, this.state.sliderLength) ;
                
                let selectedStep = stepPosition >= trackOneLength && stepPosition <= trackOneLength + trackTwoLength ? style.selectedStep : null;
                return (
                    <View style={[style.markerContainer, style.stepContainer, {left: stepPosition, top: stepTop}]} key={index}>
                        <View style={[style.touch]}>
                            <Marker
                                markerStyle={StyleSheet.flatten([style.step, selectedStep])}
                            />
                        </View>
                    </View>
                )
            });

            if(Platform.OS === "android"){
                lastStep = (
                    <View style={[style.markerContainer, style.stepContainer, {left: this.state.sliderLength - 6, top: stepTop}]} >
                        <View style={[style.touch]}>
                            <Marker
                                markerStyle={StyleSheet.flatten([style.step])}
                            />
                        </View>
                    </View>
                )
            }
            

            stepLabels = this.optionsArray.map((el, index) => {
                let stepPosition = this.valueToPosition(el, this.optionsArray, this.state.sliderLength) ;
                let label = this.props.stepsTextFormatter ? this.props.stepsTextFormatter(this.valueFormatter(el)) : el;
                stepPosition = this.state.stepsLabelsWidth[index] ? stepPosition - this.state.stepsLabelsWidth[index]/2 + 3 : stepPosition;
                return (
                    <View style={[style.stepLabel, {left: stepPosition}]} onLayout={(event) => this.measureStepLayoutWidth(event, index)} key={index}>
                        <Text style={[style.stepLabelText, globalStyles().fontBold]}>{label}</Text>
                    </View>
                )
            })
        }

        let containterHeight = 72;
        if(this.props.showSteps) {
            containterHeight = 82;
        }
        if(this.props.hideTooltip) {
            containterHeight = containterHeight - 36;
        }
        return (
            <View style={[style.container, this.props.containerStyle, {height: containterHeight}]}>
                <View style={[style.fullTrack, {height: this.props.hideTooltip ? 36 : 72}]} onLayout={(event) => this.measureWidth(event)}>
                    <View style={[style.track, this.props.trackStyle, { width: trackOneLength }]} />
                    <View style={[style.track, this.props.trackStyle, style.selectedTrack, { width: trackTwoLength }]} />
                    <View style={[style.track, this.props.trackStyle, { width: trackThreeLength }]} />
                    
                   
                    {steps}
                    {lastStep}
                </View>
                {
                    !this.props.single ?
                    <View style={[style.markerContainer, this.state.leftPressed ? style.pressedMarkerContainer : null, markerContainerLeft, this.props.markerContainerStyle]} {...this._panResponderLeft.panHandlers}>
                        <View style={[style.touch]} ref={component => (this._markerLeft = component)}  >
                            <Marker
                                pressed={this.state.leftPressed}
                                markerStyle={[style.marker, this.props.markerStyle]}
                                _panResponder={{...this._panResponderLeft.panHandlers}}
                            />
                        </View>
                    </View> :
                    null
                }
                    
                <View style={[style.markerContainer, this.state.rightPressed ? style.pressedMarkerContainer : null, markerContainerRight, this.props.markerContainerStyle]} {...this._panResponderRight.panHandlers}>
                    <View style={[style.touch]} ref={component => (this._markerRight = component)}  >
                        <Marker
                            pressed={this.state.rightPressed}
                            markerStyle={this.props.markerStyle}
                            _panResponder={{...this._panResponderRight.panHandlers}}
                        />
                    </View>
                </View>
                {
                    !this.props.hideTooltip && !this.props.single ?
                    <Tooltip 
                        left={leftTooltipPosition}
                        position={this.state.positionLeft < this.state.sliderLength / 2 ? "left" : "right"}
                        currentValue={this.valueFormatter(this.state.valueLeft)}
                        valuePrefix={this.props.valuePrefix}
                        valueSuffix={this.props.valueSuffix}
                        onLayoutCallback={(width) => {this.setTooltipWidth(width, "left")}}
                        _panResponder={{...this._panResponderLeft.panHandlers}}
                        pressed={this.state.leftPressed}
                        zIndex={this.state.leftZIndex}
                    /> : 
                    null
                }
                {
                    !this.props.hideTooltip ?
                    <Tooltip 
                        left={ rightTooltipPosition }
                        position={this.state.positionRight < this.state.sliderLength / 2 ? "left" : "right"}
                        currentValue={this.valueFormatter(this.state.valueRight)}
                        valuePrefix={this.props.valuePrefix}
                        valueSuffix={this.props.valueSuffix}
                        onLayoutCallback={(width) => {this.setTooltipWidth(width, "right")}}
                        _panResponder={{...this._panResponderRight.panHandlers}}
                        pressed={this.state.rightPressed}
                        zIndex={this.state.rightZIndex}
                    /> : 
                    null
                }
                
                {this.props.showSteps ?
                    <View style={style.stepLabels}>
                        {stepLabels}
                    </View> : null 
                }
            </View>
        )
    }
}

class Marker extends React.Component {
    render() {
        return (
            <TouchableHighlight {...this.props._panResponder}>
                <View style={style.markerWrapper} >
                    <View style={[style.markerStyle, this.props.pressed ? style.pressedMarkerStyle: null, this.props.markerStyle]} />
                </View>
            </TouchableHighlight>
        );
    }
}

class Tooltip extends React.Component {

    onLayout = (event) => {
        const ev = event.nativeEvent;
        if(ev && ev.layout){
            const width = ev.layout.width;
            this.props.onLayoutCallback && this.props.onLayoutCallback(width); 
        }
           
    }

    render() {
        const prefix = this.props.valuePrefix ? this.props.valuePrefix + " " : "";
        const value = this.props.currentValue;
        const suffix = this.props.valueSuffix(value) ? " " + this.props.valueSuffix(value) : "";
        const text = prefix + value + suffix;

        return (
            <View 
                style={[style.markerTooltipWrapper, this.props.pressed ? style.pressedTooltipWrapper: null, this.props.left || this.props.left === 0? { left : this.props.left} : null, {zIndex: this.props.zIndex}]}
                onLayout={this.onLayout}
                {...this.props._panResponder}
            >
                <View style={[style.markerTooltip, this.props.pressed ? style.pressedTooltip: null]}>
                    <Text style={[style.markerTooltipText, this.props.pressed ? style.pressedTooltipText: null, globalStyles().fontBold]}>{text}</Text>
                </View>
                <View style={[style.triangle, this.props.pressed ? style.pressedTriangle: null, this.props.position === "left" ? style.leftTriangle: style.rightTriangle, this.props.pressed &&  this.props.position !== "left" ? style.rightTrianglePressed: null]}/>
            </View>
        );
    }
}


const style = StyleSheet.create({
    container: {
        width: "100%",
        position: 'relative',
        justifyContent: 'center',
        alignItems: "center",
        paddingHorizontal: 5
    },

    fullTrack: {
        flexDirection: 'row',
        width: "100%",
        flex: 1,
        alignItems: "flex-end",
        paddingBottom: 16
    },

    track: {
        height: 2,
        backgroundColor: '#bcbcbc',
    },
    selectedTrack: {
        backgroundColor: '#7cb342',
    },

    markerContainer: {
        position: 'absolute',
        width: 30,
        height: 20,
        justifyContent: 'center',
        alignItems: 'flex-start',
        zIndex: 5
    },
    pressedMarkerContainer: {
        width: 20,
        height: 20
    },
    topMarkerContainer: {
        zIndex: 1,
    },

    stepContainer: {
        zIndex: 4, 
        width: 6
    },

    touch: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },

    step: {
        height: 6,
        width: 6,
        borderRadius: 6,
        backgroundColor: '#BBBBBB'
    },

    selectedStep: {
        backgroundColor: '#7cb342'
    },

    markerStyle: {
        height: 12,
        width: 12,
        borderRadius: 12,
        backgroundColor: '#7cb342'
    },

    pressedMarkerStyle: {
        height: 20,
        width: 20,
        borderRadius: 20
    },

    markerWrapper: {
        height: "100%",
        alignItems: "center",
        justifyContent: "center"
    },

    markerTooltipWrapper: {
        position: "absolute",
        top: 16
    },

    pressedTooltipWrapper: {
        transform: [{translateY: -16}]
    },

    markerTooltip: {
        paddingHorizontal: 8,
        paddingTop: 2,
        paddingBottom: 5,
        borderRadius: 4,
        backgroundColor: "#7cb342",
    },

    pressedTooltip: {
        paddingHorizontal: 12,
        paddingVertical: 9
    },

    markerTooltipText: {
        color: "#fff",
        fontSize: 14,
        lineHeight: 19
    },

    pressedTooltipText: {
        fontSize: 18,
        lineHeight: 24
    },

    triangle: {
        transform: [{ rotate: '180deg' }],
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 4,
        borderRightWidth: 4,
        borderBottomWidth: 4,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: "#7cb342"
    },

    pressedTriangle: {
        borderLeftWidth: 8,
        borderRightWidth: 8,
        borderBottomWidth: 8,
    },

    leftTriangle: {
        marginLeft: 7
    },

    rightTriangle: {
        left: "100%",
        marginLeft: -15
    },
    rightTrianglePressed: {
        marginLeft: -21
    },

    stepLabels: {
       paddingBottom: 10,
       width: "100%"
    },
    stepLabel: {
        position: "absolute",
        top: 0
    },
    stepLabelText: {
        fontSize: 10,
        color: "#bcbcbc",
        lineHeight: 14
    }
})

export default APRangeSlider;