import React from 'react';
import { View, StyleSheet } from 'react-native';

import globalStyles from '../utils/style';

const APNotificationCircle = (props) => {
    return (
        <View style={[style.circle, props.style]}></View>
    )
}

const style = StyleSheet.create({
    circle: {
        backgroundColor: "#ef5350",
        width: 7,
        height: 7,
        borderRadius: 7
    }
})

export default APNotificationCircle;