import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import TouchableNativeFeedback from '../ui/touchableNativeFeedback';

import globalStyles from '../utils/style';

import APIcon from './icon';
const APButton = (props) => {
    let content = (
        <View style={[styles.container, !props.removeShadow ? globalStyles().shadow : null, { backgroundColor: props.backgroundColor }, props.containerStyle]}>
            <Text style={[styles.text, globalStyles().fontBold, { color: props.color }, props.style]}>{props.text.toUpperCase()}</Text>
            {props.icon ? <View style={{ marginLeft: 15 }}><APIcon color={props.color} size={18} name={props.icon} /></View> : null}
        </View>
    );

    if(props.hoverEffect){
        return (
            <TouchableOpacity onPress={props.onPress}>
                {content}
            </TouchableOpacity>
        )
    } else {
        return (
            <TouchableNativeFeedback onPress={props.onPress}>
                {content}
            </TouchableNativeFeedback>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#7cb342",
        paddingHorizontal: 30,
        paddingVertical: 0,
        alignSelf: 'flex-start',
        borderRadius: 2,
        height: 36,
        flexDirection: "row",
        alignItems: "center"
    },

    text: {
        color: "#fff",
        fontSize: 14,
        lineHeight: 36,
        textAlign: 'center'
    }

})

export default APButton;