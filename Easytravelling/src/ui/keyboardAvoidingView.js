import React from 'react';
import { KeyboardAvoidingView, Platform, View } from 'react-native';

const KAView = (props) => {
    return Platform.OS === "ios" ? 
    <KeyboardAvoidingView behavior='padding' style={{flex: 1}} >
        {props.children}
    </KeyboardAvoidingView> :
    <View {...props} style={{flex: 1}}>{props.children}</View>
}

export default KAView;