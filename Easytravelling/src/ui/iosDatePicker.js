import React from "react";
import { DatePickerIOS, Text, TouchableHighlight, View, StyleSheet, Dimensions, Platform } from "react-native";
import ReactNativeModal from "react-native-modal";

export default class CustomDatePickerIOS extends React.PureComponent {

  static defaultProps = {
    neverDisableConfirmIOS: false,
    cancelTextIOS: "Cancel",
    confirmTextIOS: "Confirm",
    date: new Date(),
    mode: "date",
    titleIOS: "Pick a date",
    isVisible: false,
    onHideAfterConfirm: () => {},
    reactNativeModalPropsIOS: {},
    onDateChange: () => {}
  };

  state = {
    date: this.props.date,
    userIsInteractingWithPicker: false,
    minuteInterval: this.props.minuteInterval || 1
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.date !== nextProps.date) {
      this.setState({
        date: nextProps.date
      });
    }
  }

  _handleCancel = () => {
    this.confirmed = false;
    this.props.onCancel();
    this._resetDate();
  };

  _handleConfirm = () => {
    this.confirmed = true;
    this.props.onConfirm(this.state.date);
    this._resetDate();
  };

  _resetDate = () => {
    // this.setState({
    //   date: new Date()
    // });
  };

  _handleOnModalHide = () => {
    if (this.confirmed) {
      this.props.onHideAfterConfirm(this.state.date);
    }
  };

  _handleDateChange = date => {
    this.setState({
      date,
      userIsInteractingWithPicker: false
    });
    this.props.onDateChange(date);
  };

  _handleUserTouchInit = () => {
    // custom date picker shouldn't change this param
    if (!this.props.customDatePickerIOS) {
      this.setState({
        userIsInteractingWithPicker: true
      });
    }
    return false;
  };

  render() {
    const {
      isVisible,
      mode,
      titleIOS,
      confirmTextIOS,
      cancelTextIOS,
      customCancelButtonIOS,
      customConfirmButtonIOS,
      neverDisableConfirmIOS,
      customConfirmButtonWhileInteractingIOS,
      customDatePickerIOS,
      contentContainerStyleIOS,
      customTitleContainerIOS,
      datePickerContainerStyleIOS,
      reactNativeModalPropsIOS,
      titleStyle,
      confirmTextStyle,
      cancelTextStyle,
      pickerRefCb,
      minuteInterval,
      ...otherProps
    } = this.props;

    const titleContainer = (
      <View style={styles.titleContainer}>
        <Text style={[styles.title, titleStyle]}>{titleIOS}</Text>
      </View>
    );
    let confirmButton;

    if (customConfirmButtonIOS) {
      if (
        customConfirmButtonWhileInteractingIOS &&
        this.state.userIsInteractingWithPicker
      ) {
        confirmButton = customConfirmButtonWhileInteractingIOS;
      } else {
        confirmButton = customConfirmButtonIOS;
      }
    } else {
      confirmButton = (
        <Text style={[styles.confirmText, confirmTextStyle]}>
          {confirmTextIOS}
        </Text>
      );
    }
    const cancelButton = (
      <Text style={[styles.cancelText, cancelTextStyle]}>{cancelTextIOS}</Text>
    );
    const DatePickerComponent = customDatePickerIOS || DatePickerIOS;
    return (
      <ReactNativeModal
        isVisible={isVisible}
        style={[styles.contentContainer, contentContainerStyleIOS]}
        onModalHide={this._handleOnModalHide}
        onModalShow={() => {
          this.setState({
            minuteInterval
          });
        }}
        backdropOpacity={0.4}
        {...reactNativeModalPropsIOS}
      >
        <View style={[styles.datepickerContainer, datePickerContainerStyleIOS]}>
          {customTitleContainerIOS || titleContainer}
          <View
            onStartShouldSetResponderCapture={
              neverDisableConfirmIOS !== true ? this._handleUserTouchInit : null
            }
          >
            <DatePickerComponent
              ref={pickerRefCb}
              mode={mode}
              minuteInterval={this.state.minuteInterval}
              minimumDate={this.props.minimumDate}
              maximumDate={this.props.maximumDate}
              date={this.state.date}
              onDateChange={this._handleDateChange}
            />
          </View>
          <TouchableHighlight
            style={styles.confirmButton}
            underlayColor="#ebebeb"
            onPress={this._handleConfirm}
            disabled={
              !neverDisableConfirmIOS && this.state.userIsInteractingWithPicker
            }
          >
            {confirmButton}
          </TouchableHighlight>
        </View>

        <TouchableHighlight
          style={styles.cancelButton}
          underlayColor="#ebebeb"
          onPress={this._handleCancel}
        >
          {customCancelButtonIOS || cancelButton}
        </TouchableHighlight>
      </ReactNativeModal>
    );
  }
}

const isIphoneX = () => {
    const { height, width } = Dimensions.get("window");
  
    return (
      Platform.OS === "ios" &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      (height === 812 || width === 812)
    );
  };
  
  const BORDER_RADIUS = 13;
  const BACKGROUND_COLOR = "#fff";
  const BORDER_COLOR = "#d5d5d5";
  const TITLE_FONT_SIZE = 13;
  const TITLE_COLOR = "#8f8f8f";
  const BUTTON_FONT_WEIGHT = "normal";
  const BUTTON_FONT_COLOR = "#007ff9";
  const BUTTON_FONT_SIZE = 20;
  
  const styles = StyleSheet.create({
    contentContainer: {
      justifyContent: "flex-end",
      margin: 10
    },
    datepickerContainer: {
      backgroundColor: BACKGROUND_COLOR,
      borderRadius: BORDER_RADIUS,
      marginBottom: 8,
      overflow: "hidden"
    },
    titleContainer: {
      borderBottomColor: BORDER_COLOR,
      borderBottomWidth: StyleSheet.hairlineWidth,
      padding: 14,
      backgroundColor: "transparent"
    },
    title: {
      textAlign: "center",
      color: TITLE_COLOR,
      fontSize: TITLE_FONT_SIZE
    },
    confirmButton: {
      borderColor: BORDER_COLOR,
      borderTopWidth: StyleSheet.hairlineWidth,
      backgroundColor: "transparent",
      height: 57,
      justifyContent: "center"
    },
    confirmText: {
      textAlign: "center",
      color: BUTTON_FONT_COLOR,
      fontSize: BUTTON_FONT_SIZE,
      fontWeight: BUTTON_FONT_WEIGHT,
      backgroundColor: "transparent"
    },
    cancelButton: {
      backgroundColor: BACKGROUND_COLOR,
      borderRadius: BORDER_RADIUS,
      height: 57,
      marginBottom: isIphoneX() ? 20 : 0,
      justifyContent: "center"
    },
    cancelText: {
      padding: 10,
      textAlign: "center",
      color: BUTTON_FONT_COLOR,
      fontSize: BUTTON_FONT_SIZE,
      fontWeight: "600",
      backgroundColor: "transparent"
    }
  });