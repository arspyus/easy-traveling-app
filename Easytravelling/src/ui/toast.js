import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Animated,
    Dimensions,
    PanResponder,
} from 'react-native';

import globalStyles from '../utils/style';

class APToast extends Component {
    constructor() {
        super();
        this.animatedValue = new Animated.Value(0);
    }

    state = {
        dragging: false,
        offsetLeft: 0,
        opacity: 0
    };

    componentWillMount() {
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
            onMoveShouldSetPanResponder: (evt, gestureState) => false,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => false,
            onPanResponderGrant: this.handlePanResponderGrant,
            onPanResponderMove: this.handlePanResponderMove,
            onPanResponderRelease: this.handlePanResponderEnd,
            onPanResponderTerminate: this.handlePanResponderEnd,
        });
    }

    componentDidMount() {
        this.show()
    }

    show() {
        this.animatedValue.setValue(0);
        this.setState({
            opacity: 1
        }, () => {
            Animated.timing(this.animatedValue, {
                toValue: 1,
                duration: 300,
                useNativeDriver: true
            }).start();
        });
        
    }

    hide() {
        this.setState({
            dragging: false,
            offsetLeft: 0,
            opacity: 0
        } , () => {
            if (this.props.onHide) {
                this.props.onHide();
                this.props.onSwiping(false);
            }
        });
        
    }

    handleStartShouldSetPanResponder = () => {
        return true;
    };

    handlePanResponderGrant = () => {
        this.setState({ 
            dragging: true,
            draggingStartTime: Date.now() 
        });
        this.props.onSwiping(true);
    };

    handlePanResponderMove = (e, gestureState) => {
        let w = Dimensions.get('window').width;
        let diff = w - Math.abs(gestureState.dx);
        let unit = 1 / w;
        let opacity = diff * unit;
        if(this.state.dragging){
            this.setState({
                offsetLeft: gestureState.dx,
                opacity: opacity
            });
        }
    };

    handlePanResponderEnd = (e, gestureState) => {
        let diff = Dimensions.get('window').width - Math.abs(gestureState.dx);
        let draggingTime = Date.now() - this.state.draggingStartTime;
        if (diff < 80 || (draggingTime < 200 && Math.abs(gestureState.dx) > 50)) {
            this.hide();
        } else {
            if(this.state.dragging){
                this.setState({
                    dragging: false,
                    offsetLeft: 0,
                    opacity: 1
                });
                this.props.onSwiping(false);
            }
        }
    };

    render() {
        let bottom = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-10, 0],
        });

        const opacity = this.state.opacity;
        return (
            <Animated.View
                style={[
                    styles.toast,
                    {
                        transform: [
                            {
                                translateY: bottom
                            },
                            {
                                translateX: this.state.offsetLeft
                            }
                        ],
                        opacity: opacity
                    },
                ]}
                {...this.panResponder.panHandlers}>
                <Text style={[styles.text, globalStyles().fontLight]}>
                    {this.props.message}
                </Text>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    toast: {
        width: '100%',
        position: 'absolute',
        zIndex: 1000000000,
        bottom: 0,
        left: 0,
        marginTop: 10,
        backgroundColor: "#323232",
        paddingVertical: 15,
        paddingHorizontal: 25,
        elevation: 20
    },
    text: {
        color: "#fff",
        fontSize: 16
    }
});

export default APToast;
