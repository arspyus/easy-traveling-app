import React, { Component } from 'react';
import { View, StyleSheet, Text, Animated } from 'react-native';

import APIcon from './icon';
import Collapsible from './collapsible';
import globalStyles from '../utils/style';

class APExpendingView extends Component {

    hide() {
        this._collapsible && this._collapsible.changeLayout();
    }

    renderTop = () => {
        return <View style={[styles.button]}>
                    <View style={styles.icon}>
                        <APIcon size={22} color={"#424242"} name={this.props.icon} />
                    </View>
                    <Text style={[styles.buttonText, globalStyles().fontLight]}>{this.props.buttonText}</Text>
                </View >
    }

    render() {
        return (
            <View>
                <View style={styles.container}>
                    <Collapsible renderTop={this.renderTop} ref={(ref)=> this._collapsible = ref}>
                        <View style={[styles.expending]}>
                            <View style={{ paddingVertical: 28 }}>
                                {this.props.children}
                            </View>
                        </View>
                    </Collapsible>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#ddd",
        marginTop: 20,
        marginBottom: 20,
        overflow: "hidden"
    },
    button: {
        padding: 14,
        flexDirection: "row",
        borderBottomWidth: 1,
        borderColor: "#ddd"
    },
    buttonText: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 21
    },
    icon: {
        marginRight: 14
    },
    expending: {
        paddingHorizontal: 28,
    },

    hr: {
        width: "100%",
        height: 1,
        backgroundColor: "#ddd",
        marginBottom: 28
    }
})

export default APExpendingView;