import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text, Animated, Platform } from 'react-native';
import globalStyles from '../utils/style';

const Tab = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.onClick}>
            <View style={[tabStyle.container, props.containerStyle]}>
                <Text style={[tabStyle.text, globalStyles().fontBold, props.textStyle]}>{props.lowerCase ?  props.text : props.text.toUpperCase()}</Text>
            </View>
        </TouchableWithoutFeedback>
    )
}

class Line extends React.Component {
    state = {
        animation: new Animated.Value(0),  
        width: 0
    }

    componentWillMount () {
        const index = this.props.activeTabIndex;
        let toValue = this.state.width/2 * index;
        this.startAnimation(toValue);
    }

    componentDidUpdate () {
        const index = this.props.activeTabIndex;
        let toValue = this.state.width/2 * index;
        this.startAnimation(toValue)
    }

    startAnimation(toValue) {
        Animated.timing(                  
            this.state.animation,            
            {
              toValue: toValue,                   
              duration: 300,
              useNativeDriver: Platform.OS === "ios" && this.props.disableNativeDriver ? false : true
            }
          ).start();
    }

    render() {
        return (
            <Animated.View onLayout={(event) => {this.setState({...this.state, width: this.props.count * event.nativeEvent.layout.width})}} style={[lineStyle.container, this.props.lineStyle, {transform: [{translateX: this.state.animation}]}]}>
            </Animated.View>
        )
    }
}

const tabStyle = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        backgroundColor: "transparent",
        flex: 1
    },
    text: {
        color: "#fff",
        fontSize: 14
    }
})


const lineStyle = StyleSheet.create({
    container: {
        backgroundColor: "#7cb342",
        width: "50%",
        height: 2
    }
})


class APTabs extends React.Component {
    state = {
        activeTabIndex: 0
    }

    componentWillMount() {
        this.setState({
            activeTabIndex: this.props.initialIndex
        })
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.selectedTab !== undefined && this.props.selectedTab !== undefined && nextProps.selectedTab != this.props.selectedTab){
            this.setState({
                activeTabIndex: nextProps.selectedTab
            })
        }
    }


    onTabClick(index) {
        this.setState({
            activeTabIndex: index
        })
        this.props.onChange(index)
    }

    render() {
        let content = "";
        content = this.props.items.map((element, index) => {
            return <Tab text={element} key={index} textStyle={this.props.textStyle} onClick={() => this.onTabClick(index)} lowerCase={this.props.lowerCase}  containerStyle={this.props.containerStyle}/>
        })


        return (
            <View ref={(ref) => this._ref = ref}>
                <View style={containerStyle.container}>
                    {content}
                </View>
                <Line container={this._ref} activeTabIndex={this.state.activeTabIndex} lineStyle={this.props.lineStyle} count={this.props.items.length} disableNativeDriver={this.props.disableNativeDriver}/>
            </View>
        );
    }
    
}

const containerStyle = StyleSheet.create({
    container: {
        height: 48,
        flexDirection: "row"
    }
})

export default APTabs;