import React, {Component} from 'react';
import {View, StyleSheet, Image} from 'react-native';

const img = require('../../assets/images/no_internet.png');


const NoInternet = (props) => {
    return (
        <View style={style.container}>
            <Image style={style.image} source={img} />
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },

    image: {
        width: "30%",
        resizeMode: "contain"
    }
})

export default NoInternet;