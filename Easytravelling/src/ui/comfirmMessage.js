import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import globalStyles from '../utils/style';

const APComfirmMessage = (props) => {
    return (
            <View style={styles.container}>
                <Text style={globalStyles().fontLight}>{props.text}</Text>
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#E3F2FD",
        borderRadius: 10,
        padding: 14,
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        marginVertical: 10  
    },
    text: {
        color: "#424242",
        fontSize: 14
    }
})

export default APComfirmMessage;