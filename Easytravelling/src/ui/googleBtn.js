import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';

import { GoogleSignin, statusCodes } from 'react-native-google-signin';

import APIcon from './icon';
import TouchableNativeFeedback from '../ui/touchableNativeFeedback';
import globalStyles from '../utils/style';
import translate from '../utils/translate';
import { signInWithGoogle } from '../store/actions/firebaseAction';
import { set_loader, setMessage, show_google_popup } from '../store/actions/indexAction';

class APGoogleBtn extends Component {

    signIn = async () => {
        
        if(!this.props.connection){
            this.props.showMessage("went-offline")
            return;
        }
        try {
            GoogleSignin.configure({
                //webClientId: config.googleClientID, 
                offlineAccess: false, 
                forceConsentPrompt: true, 
                accountName: this.props.email ? this.props.email : ""
            });
            
            if(!this.props.email){
                await GoogleSignin.signOut();
            }

            const data = await GoogleSignin.signIn();
            
            this.props.signInWithGoogle(data.idToken, data.accessToken, this.props.email ? true : false);
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
        this.props.hideGooglePopup();
    };
    render() {
        return (
                <TouchableNativeFeedback onPress={this.signIn}>
                    <View style={[style.container, globalStyles().shadow]}>
                        <APIcon name="google" size={20} color="#FEFEFE" />
                        <View style={style.buttonText}>
                            <Text style={[style.text, globalStyles().fontBold]}>{translate[this.props.email ? "google-signin" : "google-btn-text"][this.props.lang]}</Text>
                        </View>
                    </View>
                </TouchableNativeFeedback> 
        )
    }
}

const style = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: "#2E75F1",
        width: "100%",
        flexDirection: "row",
        paddingLeft: 14,
        borderRadius: 2
    },
    buttonText: {
        flex: 1
    },
    text: {
        color: "#FBFBFC",
        lineHeight: 38,
        fontSize: 14,
        textAlign: "center"
    }
})

const mapStateToProps = (state) => {
    return {
        connection: state.index.connection
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signInWithGoogle: (idToken, accessToken, link) => { dispatch(signInWithGoogle(idToken, accessToken, link)) },
        setLoader: (isLoading) => {dispatch(set_loader(isLoading))},
        showMessage: (message) => {dispatch(setMessage(true, message))},
        hideGooglePopup: () => {dispatch(show_google_popup(false, ""))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APGoogleBtn)
