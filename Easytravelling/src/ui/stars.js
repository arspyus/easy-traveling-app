import React from 'react';
import {View, StyleSheet} from 'react-native';

import APIcon from './icon';

const Stars = (props) => {
    let stars = null;
    let rate = parseInt(props.rate || 5);
    stars = [0, 1, 2, 3, 4].map((el, index) => {
        return <View style={{marginRight: props.distance || 4}} key={el}><APIcon  size={props.size || 24} name={index < rate ? "star" :  "star_border"} color={index < rate ? props.color || "#7cb342" : "#aaa"}  /></View>
    })
    return (
        <View style={[style.stars, props.style]}>{stars}</View>
    )
}

const style = StyleSheet.create({
    stars: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginVertical: 0
    }
})

export default Stars;