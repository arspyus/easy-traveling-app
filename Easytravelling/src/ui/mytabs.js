import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text, Animated, Dimensions } from 'react-native';

import MaterialTabs from 'react-native-material-tabs';

const Tab = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.onClick}>
            <View style={tabStyle.container}>
                <Text style={tabStyle.text}>{props.text.toUpperCase()}</Text>
            </View>
        </TouchableWithoutFeedback>
    )
}

class Line extends React.Component {
    state = {
        animation: new Animated.Value(0),  
    }

    componentDidUpdate () {
        const index = this.props.activeTabIndex;
        const width = Dimensions.get("window").width;
        let toValue = width/2 * index;
        this.startAnimation(toValue)
    }

    startAnimation(toValue) {
        Animated.timing(                  
            this.state.animation,            
            {
              toValue: toValue,                   
              duration: 200,
              useNativeDriver: true
            }
          ).start();
    }

    render() {
        return (
            <Animated.View style={[lineStyle.container, {transform: [{translateX: this.state.animation}]}]}>
            </Animated.View>
        )
    }
}

const tabStyle = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        backgroundColor: "transparent",
        flex: 1
    },
    text: {
        color: "#fff",
        fontSize: 14
    }
})


const lineStyle = StyleSheet.create({
    container: {
        backgroundColor: "#7cb342",
        width: "50%",
        height: 2
    }
})


class APMyTabs extends React.Component {
    state = {
        activeTabIndex: 0
    }

    onTabClick(index) {
        this.setState({
            activeTabIndex: index
        })
        this.props.onChange(index)
    }

    render() {
        let content = "";
        content = this.props.items.map((element, index) => {
            return <Tab text={element} key={index} onClick={() => this.onTabClick(index)}/>
        })


        return (
            <View>
                <View style={containerStyle.container}>
                    {content}
                </View>
                <Line activeTabIndex={this.state.activeTabIndex}/>
            </View>
        );
    }
    
}

const containerStyle = StyleSheet.create({
    container: {
        height: 48,
        flexDirection: "row"
    }
})
export default APMyTabs;