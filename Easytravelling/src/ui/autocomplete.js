import React, { Component } from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text, FlatList, TextInput, Image, Platform } from 'react-native';
import Modal from "react-native-modal";
import { connect } from 'react-redux';

import APIcon from './icon';

import globalStyles from '../utils/style';
const google_logo = require('../../assets/images/powered-by-google.png');

class APAutocomplete extends Component {

    state = {
        searchVal: ""
    }

    onBack = () => {
        this._searchElem.blur();
        this.props.onClose && this.props.onClose();
    }

    onClear = (text) => {
        this._searchElem.setNativeProps({text: ''});
        this.state.searchVal && this.onChange("")
    }

    onChange = (text) => {
        this.setState({
            searchVal: text
        })

        this.props.onChange && this.props.onChange(text)
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableWithoutFeedback onPress={() => this.props.onSelect && this.props.onSelect(item.value)}>
                <View style={style.listItem}>
                    <View style={style.icon}>
                        <APIcon name={item.icon} color="#757575" size={24} paddingVertical={10} />
                    </View>
                    <View style={style.listContent}>
                        <Text style={[style.listTitle, globalStyles().fontBold]} numberOfLines={1}>{item.title}</Text>
                        {
                            item.description ?
                                <Text style={[style.listDescription, globalStyles().fontLight]} numberOfLines={1}>{item.description}</Text> :
                                null
                        }
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    render() {

        return (
            <View style={style.container}>
                <Modal
                    isVisible={this.props.isVisible}
                    style={[style.modal, {paddingTop: Platform.OS === "android" ? 10 : 10 + this.props.ios_toolbar_height}]}
                    backdropOpacity={0.7}
                    onModalShow={() => this._searchElem.focus()}
                    onBackdropPress	={this.onBack}
                    onBackButtonPress ={this.onBack}
                    animationIn="fadeIn"
                    animationOut="fadeOut"
                >
                    <View style={{  maxHeight: "100%"}} >
                        <View style={[style.modalInput, this.props.data.length !== 0 ? {borderBottomLeftRadius: 0, borderBottomRightRadius: 0} : null]}>
                            <TouchableWithoutFeedback onPress={this.onBack}>
                                <View style={style.icon}>
                                    <APIcon name="keyboard_backspace" color="#757575" size={24} paddingVertical={10} />
                                </View>
                            </TouchableWithoutFeedback>
                            <View style={style.inputWrapper}>
                                <TextInput
                                    placeholder={this.props.placeholder}
                                    style={[style.input,  globalStyles().fontLight]}
                                    disableFullscreenUI={true}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="#7C7C7C"
                                    selectionColor="#7cb342"
                                    onChangeText={this.onChange}
                                    ref={(ref) => { this._searchElem = ref }}
                                    returnKeyType="go"
                                    onSubmitEditing={!this.props.restrictWithData && this.props.onSelect ? () => this.props.onSelect(this.state.searchVal) : null}
                                />
                            </View>
                            <TouchableWithoutFeedback onPress={this.onClear}>
                                <View style={style.icon}>
                                    {
                                        this.state.searchVal ?
                                            <APIcon name="clear" color="#757575" size={24} paddingVertical={10} /> :
                                            <View style={{ width: 24 }}></View>
                                    }
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        {
                            this.props.data && this.props.data.length > 0 && this.props.google_logo?
                            <View style={style.google_logo_wrapper}>
                                <Image style={style.google_logo} source={google_logo} />
                            </View> : 
                            null
                        }
                        
                        <FlatList
                            data={this.props.data}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index}
                            showsVerticalScrollIndicator={false}
                            removeClippedSubviews={true}
                            keyboardShouldPersistTaps="handled"
                        />
                    </View>
                </Modal>
            </View>
        )
    }

}

const style = StyleSheet.create({
    container: {
        width: "100%",
        zIndex: 20
    },
    modal: {
        justifyContent: "flex-start",
        margin: 0,
        paddingHorizontal: 10,
        paddingVertical: 10
    },

    modalInput: {
        backgroundColor: "#fff",
        flexDirection: "row",
        borderBottomWidth: 1,
        borderBottomColor: "rgba(0,0,0,0.14)",
        borderRadius: 5,
    },
    list: {
        borderBottomLeftRadius: 5, 
        borderBottomRightRadius: 5,
        backgroundColor: "#fff"
    },

    listItem: {
        backgroundColor: "#fff",
        height: 58,
        flexDirection: "row"
    },

    listContent: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: "rgba(0,0,0,0.14)",
        justifyContent: "center",
        paddingLeft: 10,
        paddingRight: 32
    },

    listTitle: {
        fontSize: 13,
        color: "#212121"
    },

    listDescription: {
        fontSize: 13,
        color: "#7C7C7C"
    },

    icon: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 22
    },

    inputWrapper: {
        flex: 1
    },

    input: {
        fontSize: 14,
        borderRadius: 5,
        color: "#222222",
        paddingHorizontal: 10,
        paddingVertical: 12,
        lineHeight: 19
    },

    google_logo_wrapper: {
        position: "absolute",
        right:15,
        top: 60,
        zIndex: 10
    },

    google_logo: {
        width: 50,
        height: 10,
        resizeMode: "contain"
    }
})

const mapStateToProps = (state) => {
    return {
        ios_toolbar_height: state.index.ios_toolbar_height
    }
}

export default connect(mapStateToProps, null)(APAutocomplete)