import React from 'react';
import { View, StyleSheet, Text, Platform, TouchableHighlight } from 'react-native';

import TouchableNativeFeedback from './touchableNativeFeedback';
import Modal from "react-native-modal";

import globalStyles from '../utils/style';

const APModal = (props) => {

    return (
        <Modal 
            isVisible={props.isVisible} 
            style={props.bottomModal ? {justifyContent: "flex-end", margin: 0} : null} 
            onBackdropPress={props.onBackdropPress}
            backdropOpacity={0.5}
            >
            <View style={[style.modalWrapper, globalStyles().shadow]}>
                <View style={style.modalTop}>
                    <Text style={[style.modalHead, globalStyles().fontLight]}>{props.head}</Text>
                    <View style={style.modalContent}>
                        {props.children}
                    </View>
                </View>
                {   props.btnText ? 
                        <View style={style.modalBottom}>
                            { Platform.OS === "android" ? 
                                <TouchableNativeFeedback onPress={props.onClose}>
                                    <View style={style.modalButtonCont}>
                                        <Text style={[style.modalButton, globalStyles().fontLight]}>{props.btnText}</Text>
                                    </View>
                                </TouchableNativeFeedback> : 
                                <TouchableHighlight onPress={props.onClose} underlayColor={"#ebebeb"}>
                                    <View style={style.modalButtonCont}>
                                        <Text style={[style.modalButton, globalStyles().fontLight]}>{props.btnText}</Text>
                                    </View>
                                </TouchableHighlight> 
                            }
                            </View> : null
                }
            </View>
        </Modal>
    )
}


const style = StyleSheet.create({
    modalWrapper: {
        backgroundColor: '#fafafa',
        padding: 0,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        borderRadius: 2,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },

    modalTop: {
        padding: 24,
        justifyContent: "flex-start",
        width: "100%"        
    },

    modalHead: {
        fontSize: 32,
        lineHeight: 35,
        color: "#424242"
    },

    modalContent: {
        marginTop: 12,
        justifyContent: "flex-start"
    },

    modalNote: {
        fontSize: 10,
        color: "#424242",
        lineHeight: 15,
        paddingTop: 16
    },

    modalBottom: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        width: "100%",
        paddingVertical: 8,
        height: 56
    },

    modalButtonCont: {
        paddingHorizontal: 6,
        height: 40,
        marginRight: 8
    },

    modalButton: {
        paddingHorizontal: 14,
        fontSize: 14,
        lineHeight: 40,
        color: "#343434",
        textAlign: "right"
    }
})

export default APModal;
