import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableWithoutFeedback} from 'react-native';

import APIcon from './icon';

import globalStyles from '../utils/style';

class APTags extends Component {

    renderContent = () => {
        let tags = this.props.tags.map((tag, index) => {
            return (
                <View style={style.tag} key={index}>
                    <Text style={[style.tagText, globalStyles().fontBold]}>{tag}</Text>
                    <TouchableWithoutFeedback onPress={() => this.props.onRemove(tag)}>
                        <View style={style.clear_wrapper}>
                            <View style={style.clear}>
                                <APIcon name="clear" color="#DEDEDE" size={15} />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>    
                </View>
            )
        })

        return <TouchableWithoutFeedback onPress={this.props.onClick}>
                    <View style={this.props.tags.length === 0  ? style.empty : style.row}>
                        {this.props.tags.length > 0 ? tags: <Text style={[style.placeholder, globalStyles().fontLight]}>{this.props.placeholder || ""}</Text>}
                    </View>
                </TouchableWithoutFeedback>    
    }

    
    render() {
        return this.props.tags.length > 0 ?
            <View style={style.container}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} overScrollMode="never" contentContainerStyle={{flexGrow: 1}}>  
                    {this.renderContent()}
                </ScrollView> 
            </View>:
            this.renderContent()
    }
}

const style = StyleSheet.create({

    container: {
        flexDirection: "row",
        paddingBottom: 5,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#DEDEDE",
        width: "100%"
    },

    empty: {
        paddingBottom: 9,
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: "#DEDEDE",
        width: "100%"
    },  

    row: {
        flexDirection: "row",
        width: "100%"
    },

    tag: {
        paddingLeft: 16,
        paddingRight: 8,
        paddingVertical: 0,
        backgroundColor: "#DEDEDE",
        borderRadius: 20,
        marginRight: 8,
        flexDirection: "row"
    },

    tagText: {
        fontSize: 14,
        color: "#515151",
        lineHeight: 34
    },

    clear_wrapper: {
        width: 26,
        justifyContent: "center",
        alignItems: "center",
        height: 34,
        backgroundColor: "transparent"
    },

    clear: {
        width: 18,
        height: 18,
        borderRadius: 18,
        backgroundColor: "#6D6D6D",
        justifyContent: "center",
        alignItems: "center"
    },

    placeholder: {
        fontSize: 14,
        lineHeight: 19,
        color: "#bcbcbc"
    }
})

export default APTags;