import React from 'react';
import {TouchableNativeFeedback, TouchableWithoutFeedback, Platform} from 'react-native';

const TouchableNativeFeedbackWrapper = (props) => {
    if(Platform.OS === "ios"){
        return <TouchableWithoutFeedback onPress={props.onPress}>{props.children}</TouchableWithoutFeedback>
    } else {
        return <TouchableNativeFeedback onPress={props.onPress}>{props.children}</TouchableNativeFeedback>
    }
}

export default TouchableNativeFeedbackWrapper;