import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import globalStyles from '../utils/style';

const NothingFound = (props) => {
    return (
        <View style={[style.container, props.style]}>
            <Text style={[style.text, globalStyles().fontLight]}>{props.text}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        marginVertical: 40,
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        color: "#b1a8a8",
        fontSize: 24,
        lineHeight: 26
    }
})

export default NothingFound;