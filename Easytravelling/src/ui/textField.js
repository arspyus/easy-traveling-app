import React, { Component } from 'react';
import { View, TextInput, StyleSheet, findNodeHandle, UIManager, Dimensions, Platform } from 'react-native';

import globalStyles from '../utils/style';
class APTextField extends Component {

    focus() {
        this._inputElem && this._inputElem.focus();
    }

    _scrollToInput = () => {
        if (typeof this.props.onFocus != "undefined") {
            this.props.onFocus();
        }
        if (!this.props._scrollView) return;
        const inputHandle = findNodeHandle(this._inputElem);

        UIManager.measure(
            inputHandle,
            (x, y, w, h, pageX, pageY) => {
                const wH = Dimensions.get("window").height;
                if (wH - 280 - h < pageY || pageY < 0) {
                    const scrollResponder = this.props._scrollView.getScrollResponder();
                    scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                        inputHandle,
                        90,
                        true
                    );
                }
            });

    }

    render() {
        return (
            <View style={style.container}>

                <TextInput
                    style={[style.input, this.props.multiline ? {textAlignVertical: 'top', borderColor: "rgba(188,188,188, .5)"} : null, this.props.error ? style.error : null, globalStyles().fontLight]}
                    returnKeyType={"next"}
                    blurOnSubmit={this.props.returnKeyType === "go"}
                    ref={(ref) => { this._inputElem = ref }}
                    disableFullscreenUI={true}
                    underlineColorAndroid="transparent"
                    placeholderTextColor="#515151" 
                    selectionColor="#7cb342"
                    ref={(ref) => { this._inputElem = ref }}
                    onSubmitEditing={this.props.onSubmit}
                    {...this.props}
                    onFocus={this._scrollToInput}

                />
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%"
    },

    input: {
        borderWidth: 1,
        borderColor: "#bcbcbc",
        fontSize: 14,
        lineHeight: 19,
        borderRadius: 5,
        color: "#515151",
        paddingHorizontal: 15,
        paddingVertical: 10,
        paddingBottom: 10
    },

    error: {
        borderColor: "#f44336",
        color: "#f44336"
    }

})


export default APTextField;