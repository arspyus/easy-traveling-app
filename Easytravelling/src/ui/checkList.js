import React, { Component } from 'react';
import { View, StyleSheet, Animated, Text } from 'react-native';

import APDropdown from './dropdown';
import APTags from './tags';

import globalStyles from '../utils/style';

class APCheckList extends Component {

    state = {
        value: this.props.options[0].text,
        options: this.props.options.map(op => {
            return { ...op, animation: new Animated.Value(0), checked: false }
        }),
        containerWidth: null
    }

    componentWillReceiveProps(nextProps){
        if(this.props.options && this.props.options !== nextProps.options){
            this.setState({
                ...this.state,
                options: nextProps.options.map(op => {
                    return { ...op, animation: new Animated.Value(0), checked: false, initial: false }
                }),
                value: nextProps.options[0].text
            })
        }

    }

    dropdown = null;

    showDropdown = () => {
        this.dropdown.show()
    }

    startAnimation(animation, to) {
        Animated.timing(animation, {
            toValue: to,
            duration: 250,
            useNativeDriver: true
        }).start();
    }

    onCheck = (index, value) => {
        if(this.props.disabledOptions && this.props.disabledOptions.indexOf(parseInt(index)) > -1){
            return false;
        }
        let option = this.state.options[index];
        this.startAnimation(this.state.options[index].animation, !option.initial ? option.checked ? 0 : 1 : option.checked ? 1: 0);
        let options = [...this.state.options];
        options[index].checked = !options[index].checked;

        let val = "";
        options.forEach((el) => {
            if (el.checked)
                val = val + (val === "" ? el.text : "| " + el.text)
        })
        this.setState({
            ...this.state,
            options: options,
            value: val === "" ? this.state.options[0].text : val
        }, () => {
            if(this.props.onChangeText){
                this.props.onChangeText(this.state.options.filter(el => el.checked).map(el => el.value).join(","));
            }
        })
    }


    renderRow = (value, index) => {
        const checkboxOpacity = this.state.options[index].animation.interpolate({
            inputRange: [0, 1],
            outputRange: this.state.options[index].initial ? [0 , 1] : [1, 0]
        });

        const signOpacity = this.state.options[index].animation.interpolate({
            inputRange: [0, 1],
            outputRange: this.state.options[index].initial ? [1 , 0] : [0, 1]
        });

        const checkboxRotate = this.state.options[index].animation.interpolate({
            inputRange: [0, 1],
            outputRange: this.state.options[index].initial ? ['90deg', '0deg'] : ['0deg', '90deg']
        });

        const signRotate = this.state.options[index].animation.interpolate({
            inputRange: [0, 1],
            outputRange: this.state.options[index].initial ? ['40deg', '0deg'] : ['0deg', '40deg'],
        });

        return (
            <View style={styles.rowWrapper}>
                <View style={styles.row}>
                    <Animated.View style={[styles.checkbox, 
                    this.props.disabledOptions && this.props.disabledOptions.indexOf(parseInt(index)) > -1 ? styles.disabled : null,
                    {
                        opacity: checkboxOpacity,
                        transform: [{ rotate: checkboxRotate }]
                    }]}></Animated.View>
                    <Animated.View style={[styles.sign, {
                        opacity: signOpacity,
                        transform: [{ rotate: signRotate }]
                    }]}></Animated.View>
                    <Text style={[styles.rowText, globalStyles().fontLight]}>{value}</Text>
                </View>
                {
                    this.state.options[index].desc ?
                        <View style={{width: "100%"}}>
                            <Text style={[styles.rowText, globalStyles().fontLight, {color: "grey", fontSize: 12, paddingLeft:38}]}>{this.state.options[index].desc}</Text>
                        </View>:
                        null
                }
            </View>
        )
    }

    getTags = () => {
        let tags = [];
        let values = this.props.options.map(el => el.text);
        tags = this.state.value.split("|").filter(tag => {
            return !this.props.disabledOptions || this.props.disabledOptions.indexOf(values.indexOf(tag)) === -1
        }).map(tag => tag.trim());
        return tags;
    }

    onDropdownWillHide  = (action) =>{
        if(action === "onRowPress") return false;
        this.setState({
            ...this.state,
            options: this.state.options.map((op) => {
                return {
                    ...op,
                    initial: op.checked
                }
            })
        })

        return true;
    }

    onTagRemove = (tag) => {
        let index, value;
        this.props.options.forEach((option, ind ) => {
            if(option.text === tag){
                index = ind;
                value = option.value;
                return false;
            }
        })
        if(typeof index !== "undefined"){
            this.onCheck(index, value);
        }
    }

    render() {
        return (
                <View style={styles.container}>
                    {                        
                        <View onLayout={(event) => {this.setState({containerWidth: event.nativeEvent.layout.width})}}>
                            <APTags 
                                tags={this.getTags()}
                                onRemove={this.onTagRemove}
                                onClick={this.showDropdown}
                                placeholder={this.props.options[0].text}
                            />
                        </View>
                    }
                    
                    <APDropdown
                        options={this.state.options}
                        ref={(dropdown) => { this.dropdown = dropdown; }}
                        dropdownStyle={{ width: this.state.containerWidth, marginTop: -30 }}
                        dropdownTextStyle={{ color: "#7cb342" }}
                        dropdownTextHighlightStyle={{ backgroundColor: "#fff" }}
                        renderRow={this.renderRow}
                        onDropdownWillHide={(action) => { return this.onDropdownWillHide(action) }}
                        onSelect={this.onCheck}
                        disabledOptions={this.props.disabledOptions} 
                        maxItems={5}
                        topOffset={6}
                        bottomOffset={-26}
                        ><View style={globalStyles().hidden}></View></APDropdown>
                </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        overflow: "hidden"
    }, 

    overlay: {
        width: "100%",
        height: "100%",
        backgroundColor: "transparent",
        position: "absolute",
        left: 0,
        top: 0
    },

    icon: {
        position: "absolute",
        right: 4,
        top: 20
    },

    rowWrapper: {
        backgroundColor: "#fff",
        paddingVertical: 14,
        paddingHorizontal: 16,
    },
  
    row: {
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center"
    },
  
    rowText: {
      fontSize: 16,
      lineHeight: 22,
      color: '#7cb342'
    },
  
    checkbox: {
      width: 18,
      height: 18,
      borderWidth: 2,
      borderColor: "#5a5a5a",
      marginRight: 18
    },

    disabled: {
        backgroundColor: "rgba(0,0,0,0.42)",
        borderWidth: 0
    },
  
    sign: {
      position: "absolute",
      height: 18,
      width: 10,
      borderBottomWidth: 2,
      borderRightWidth: 2,
      borderColor: "#7cb342",
      left: 4,
      top: 0
    }
});

export default APCheckList;