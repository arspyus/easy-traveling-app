import React, {Component} from 'react';

import APInput from './input';

class APTextarea extends Component {
    focus(){
        this._inputElem && this._inputElem.focus();
    }

    render() {
        return (
            <APInput 
                ref={ref => {this._inputElem = ref}} 
                minHeight={76} 
                multiline={true} 
                {...this.props}
            />
        )
    }
    
}

export default APTextarea;