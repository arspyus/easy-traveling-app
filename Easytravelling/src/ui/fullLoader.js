import React from 'react';
import {View, StyleSheet} from 'react-native';

import APLoader from './loader'

const APFullLoader= (props) => {
    return (
        <View style={style.container}>
            <APLoader />
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        backgroundColor: "rgba(255,255,255,0.8)",
        position: "absolute",
        flex: 1,
        zIndex: 20000,
        justifyContent: "center",
        alignItems: "center",
        elevation: 5
    }
})

export default APFullLoader
