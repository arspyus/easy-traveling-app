import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import globalStyles from '../utils/style';

const APHeading = (props) => {
    return (
        <View style={style.container}>
            <Text style={[style.text, globalStyles().fontLight]}>{props.text}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        alignItems: "center",
        marginVertical: 24
    },

    text: {
        color: "#424242",
        fontSize: 28,
        lineHeight:31,
        textAlign: "center"
    }
})

export default APHeading;