import React, { Component } from 'react';
import { Text, View, StyleSheet, Animated, Easing, TouchableWithoutFeedback } from 'react-native';

import APIcon from './icon';
import globalStyles from '../utils/style';

const ANIMATED_EASING_PREFIXES = ['easeInOut', 'easeOut', 'easeIn'];

class Collapsible extends Component {
    constructor(props) {
        super(props);
        this.state = {
            measuring: false,
            measured: false,
            height: new Animated.Value(props.collapsedHeight),
            contentHeight: 0,
            animating: false,
            collapsed: true
        };
    }

    static defaultProps = {
        align: 'top',
        collapsedHeight: 0,
        enablePointerEvents: false,
        duration: 300,
        easing: 'easeIn',
        onAnimationEnd: () => null,
        onAnimationStart: () => null,
        title: ""
      };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.collapsed !== this.state.collapsed) {
            this.setState({ measured: false }, () =>
                this._componentDidUpdate(prevProps, prevState)
            );
        } else {
            this._componentDidUpdate(prevProps, prevState);
        }
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    _componentDidUpdate(prevProps, prevState) {
        if (prevState.collapsed !== this.state.collapsed) {
            this._toggleCollapsed(this.state.collapsed);
        } else if (

            this.state.collapsed &&
            prevProps.collapsedHeight !== this.props.collapsedHeight
        ) {
            this.state.height.setValue(this.props.collapsedHeight);
        }
    }

    contentHandle = null;

    _handleRef = ref => {
        this.contentHandle = ref;
    };

    _measureContent(callback) {
        this.setState(
            {
                measuring: true,
            },
            () => {
                requestAnimationFrame(() => {
                    if (!this.contentHandle) {
                        this.setState(
                            {
                                measuring: false,
                            },
                            () => callback(this.props.collapsedHeight)
                        );
                    } else {
                        this.contentHandle.getNode().measure((x, y, width, height) => {
                            this.setState(
                                {
                                    measuring: false,
                                    measured: true,
                                    contentHeight: height,
                                },
                                () => callback(height)
                            );
                        });
                    }
                });
            }
        );
    }

    _toggleCollapsed(collapsed) {
        if (collapsed) {
            this._transitionToHeight(this.props.collapsedHeight);
        } else if (!this.contentHandle) {
            if (this.state.measured) {
                this._transitionToHeight(this.state.contentHeight);
            }

            return;
        } else {
            this._measureContent(contentHeight => {
                this._transitionToHeight(contentHeight);
                this.props.onAnimationStart();
            });
        }
    }

    _transitionToHeight(height) {
        const { duration } = this.props;
        let easing = this.props.easing;
        if (typeof easing === 'string') {
            let prefix;
            let found = false;
            for (let i = 0; i < ANIMATED_EASING_PREFIXES.length; i++) {
                prefix = ANIMATED_EASING_PREFIXES[i];
                if (easing.substr(0, prefix.length) === prefix) {
                    easing =
                        easing.substr(prefix.length, 1).toLowerCase() +
                        easing.substr(prefix.length + 1);
                    prefix = prefix.substr(4, 1).toLowerCase() + prefix.substr(5);
                    easing = Easing[prefix](Easing[easing || 'ease']);
                    found = true;
                    break;
                }
            }
            if (!found) {
                easing = Easing[easing];
            }
            if (!easing) {
                throw new Error('Invalid easing type "' + this.props.easing + '"');
            }
        }

        if (this._animation) {
            this._animation.stop();
        }
        this.setState({ animating: true });
        this._animation = Animated.timing(this.state.height, {
            toValue: height,
            duration,
            easing,
        }).start(() => {
            if (this.unmounted) {
                return;
            }
            this.setState({ animating: false }, () => {
                if (this.unmounted) {
                    return;
                }
                this.props.onAnimationEnd();
            });
        });
    }

    _handleLayoutChange = event => {
        const contentHeight = event.nativeEvent.layout.height;

        if (
            this.state.animating ||
            this.state.collapsed ||
            this.state.measuring ||
            this.state.contentHeight === contentHeight
        ) {
            return;
        }
        this.state.height.setValue(contentHeight);
        this.setState({ contentHeight });
    };

    renderTop = () => {
        if (this.props.renderTop) {
            return this.props.renderTop();
        } else {
            return <View style={style.collapsible_top}>
                <View style={style.row}>
                    <Text style={[style.title, globalStyles().fontBold]}>{this.props.title}</Text>
                    <APIcon size={22} color={"#888888"} name="keyboard_arrow_down" name={this.state.collapsed ? "keyboard_arrow_down" : "keyboard_arrow_left"} />
                </View>
            </View >
        }
    }

    changeLayout = (evn) => {
        this.setState({
            collapsed: !this.state.collapsed
        })
        console.log("clicked",evn.nativeEvent.locationX)
    }

    render() {
        const { enablePointerEvents } = this.props;
        const { height, contentHeight, measuring, measured, collapsed } = this.state;
        const hasKnownHeight = !measuring && (measured || collapsed);
        const style = hasKnownHeight && {
            overflow: 'hidden',
            height: height,
        };
        const contentStyle = {};
        if (measuring) {
            contentStyle.position = 'absolute';
            contentStyle.opacity = 0;
        } else if (this.props.align === 'center') {
            contentStyle.transform = [
                {
                    translateY: height.interpolate({
                        inputRange: [0, contentHeight],
                        outputRange: [contentHeight / -2, 0],
                    }),
                },
            ];
        } else if (this.props.align === 'bottom') {
            contentStyle.transform = [
                {
                    translateY: height.interpolate({
                        inputRange: [0, contentHeight],
                        outputRange: [-contentHeight, 0],
                    }),
                },
            ];
        }

        return (
            <View style={style.container}>
                <View>
                    <TouchableWithoutFeedback onPress={(evn) => this.changeLayout(evn)}>
                        <View>
                            {this.renderTop()}
                        </View>
                    </TouchableWithoutFeedback>

                    <Animated.View
                        style={style}
                        pointerEvents={!enablePointerEvents && collapsed ? 'none' : 'auto'}
                    >
                        <Animated.View
                            ref={this._handleRef}
                            style={[this.props.style, contentStyle]}
                            onLayout={this.state.animating ? undefined : this._handleLayoutChange}
                        >
                            {this.props.children}
                        </Animated.View>
                    </Animated.View>
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create(
    {
        container:
        {
            //justifyContent: 'center',
        },

        collapsible_top: {
            paddingVertical: 20,
            paddingHorizontal: 15,
            backgroundColor: "#fff"
        },
        row: {
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
        },

        title: {
            color: "#515151",
            fontSize: 16,
            lineHeight: 22
        }
    });

export default Collapsible;
