import React, { PureComponent } from 'react';
import { View, Platform, UIManager, Dimensions } from 'react-native';
import TextInput from './input/Input';

import globalStyles from '../utils/style';

class APInput extends PureComponent {

    focus() {
        this._inputElem && this._inputElem.focus();
    }

    _scrollToInput = () => {
        if (typeof this.props.onFocus != "undefined") {
            this.props.onFocus();
        }
        if (!this.props._scrollView) return;
        const inputHandle = this._inputElem.inputHandle();

        UIManager.measure(
            inputHandle,
            (x, y, w, h, pageX, pageY) => {
                const wH = Dimensions.get("window").height;
                if (wH - 280 - h < pageY || pageY < 0) {
                    const scrollResponder = this.props._scrollView.getScrollResponder();
                    scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                        inputHandle,
                        Platform.OS === "android" ? 90 : 90,
                        true
                    );
                }
            });

    }

    onChange = (text) => {
        if(Platform.OS === "ios"){
            clearTimeout(this.changeTimer);
            this.changeTimer = setTimeout( () => {
                this.props.onChangeText && this.props.onChangeText(text);
            }, 200)
        } else {
            this.props.onChangeText && this.props.onChangeText(text);
        }
        
    }

    _onBlurHandler = (event) => {
        this.props.onChangeText(event.nativeEvent.text);
        this.props.onBlur && this.props.onBlur(event)
    }

    render() {
        return (
            <View style={[styles.container, this.props.inputMargin ? {marginBottom: this.props.inputMargin} : null]}>

                <TextInput
                    label={this.props.label}
                    labelColor={!this.props.disabled ? this.props.labelColor || "#7cb342" : "#bcbcbc"}
                    labelActiveColor="#7cb342"
                    underlineColor={this.props.isValid ? "#bcbcbc" : "#F44336"}
                    underlineActiveColor={this.props.isValid ? "#7cb342" : "#F44336"}
                    underlineHeight={this.props.isValid ? 1 : 2}
                    labelStyle={{ ...globalStyles(true).fontLight }}
                    color={!this.props.disabled ? "#424242" : "#cccccc"}
                    fontFamily={Platform.OS === "android" ? "NotoSans-Light-font" : "NotoSans-Light"}
                    fontSize={14}
                    errorFontSize={11}
                    errorColor={"#F44336"}
                    style={[{ placeholderTextColor: "#7cb342" }]}
                    labelActiveTop={-24}
                    returnKeyType={"next"}
                    errorPaddingTop={0}
                    blurOnSubmit={this.props.returnKeyType === "go"}
                    ref={(ref) => { this._inputElem = ref }}
                    disableFullscreenUI={true}
                    selectionColor="#7cb342"
                    {...this.props}
                    onChangeText={this.onChange}
                    onFocus={this._scrollToInput}
                />
            </View>
        )
    }

}

const styles = {
    container: {
        width: "100%",
        marginBottom: 20
    }
}


export default APInput;