import React, {Component} from 'react';
import { View, StyleSheet, Animated, Easing, Dimensions } from 'react-native';

class APSpinner extends Component {

    constructor() {
        super();
        this.animatedValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.start();
    }

    start() {
        this.animatedValue.setValue(0);
        Animated.timing(this.animatedValue, {
            toValue: 1,
            duration: 800,
            useNativeDriver: true
        }).start(() => {
            this.start();
        });
    }

    render() {
        const w = Dimensions.get("window").width;
        let translate = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-100, w + 100],
        });

        return (
            <View style={[styles.container]}>
                <Animated.View style={[styles.inner, {
                    transform: [
                        {translateX : translate}
                    ]
                }]}></Animated.View>
            </View>
        )
    }

    
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 4,
        backgroundColor: "#e8f5e9",
        marginBottom: 0,
        position: "absolute",
        bottom: 0
    },

    inner: {
        width: 100,
        height: 4,
        backgroundColor: "#7cb342",
    }
})

export default APSpinner;