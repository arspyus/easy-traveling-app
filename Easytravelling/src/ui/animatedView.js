import React, { Component } from 'react';
import { View, Text, Animated, Platform, StyleSheet, Dimensions, ScrollView, TouchableWithoutFeedback } from 'react-native';

import { connect } from 'react-redux';

import APContainer from './container';
import Header from './header';
import APBackButton from './backButton'
import APIcon from './icon';

import globalStyles from '../utils/style';

let HEADER_MAX_HEIGHT = 236;

class AnimatedView extends Component {
    state = {
        scrollY: new Animated.Value(0),
        logoVisible: true,
        titleWidth: 0,
        descWidth: null,
        layoutDone: false,
        photoLoaded: false,
        coverImageOpened:false
    }

    componentDidMount() {
        this.setState({
            agencyTitleWidth: null,
            layoutDone: false
        });

        Dimensions.addEventListener('change', this.changeDimensions);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.changeDimensions)
    }

    changeDimensions = () => {
        this.setState({
            descWidth: null,
            layoutDone: false
        });
    }

    onCoverImageClick = () =>{
        this.props.onClick && this.props.onClick();
    }

    onDescLayout = (event) => {
        if (this.state.layoutDone) return;
        const width = event.nativeEvent.layout.width;

        const containerW = Dimensions.get("window").width - 40 - this.props.badgeWidth;
        if (width > containerW && !this.state.descWidth) {
            this.setState({
                descWidth: containerW,
                layoutDone: true
            })
        } else {
            this.setState({
                descWidth: null,
                layoutDone: true
            })
        }
    }

    _handleScroll = (event) => {

        let HEADER_MIN_HEIGHT = Platform.OS === "android" ? 56 : 56 + this.props.ios_toolbar_height;
        if (event.nativeEvent.contentOffset.y >= HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT && this.state.logoVisible) {
            this.setState({
                ...this.state,
                logoVisible: false
            }, () => {
                this.props.onAnimatinChange && this.props.onAnimatinChange(false);
            })
        } else if (event.nativeEvent.contentOffset.y < HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT && !this.state.logoVisible) {
            this.setState({
                ...this.state,
                logoVisible: true
            }, () => {
                this.props.onAnimatinChange && this.props.onAnimatinChange(true);
            })
        }
    }

    onIconClick = () => {
        if(this.state.logoVisible){
            this.props.onIconClick()
        }
    }

    render() {
        let HEADER_MIN_HEIGHT = Platform.OS === "android" ? 56 : 56 + this.props.ios_toolbar_height;
        let HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
        let tableRowPaddingHorizontal = this.props.iphoneXSpace.left > 0 ? 44 : 20;

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });

        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const imageTranslate = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });

        const TextTransformY = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, 66],
            extrapolate: 'clamp',
        });

        const TextScale = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.712],
            extrapolate: 'clamp',
        });
        const TextTransform = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -(0.288 * this.state.titleWidth) / 2 + 18 + (false ? 2 : 26)],
            extrapolate: 'clamp',
        });

        const circularTop = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [208, 0],
            extrapolate: 'clamp',
        });

        const circularOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE * 19 / 20, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const iconOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE * 19 / 20, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp',
        });

        const headerBottomTextOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 0, 0],
            extrapolate: 'clamp',
        });

        return (
            <APContainer
                style={style.container}
                header={false}
                screenId={this.props.testID}
                screenComponent={this.props.screenComponent}
            >
                <ScrollView
                    alwaysBounceVertical={false}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    scrollEventThrottle={1}
                    onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }], { listener: this._handleScroll })}
                >
                    <View style={style.wrapper}>
                        <View>
                            <View style={[style.empty]}></View>
                            <View style={[style.greenBox, {paddingHorizontal: tableRowPaddingHorizontal + 4}]}>
                                <Text style={[style.boxTitle, globalStyles().fontBold]}>{this.props.boxTitle}</Text>
                                <Text style={[globalStyles().text, globalStyles().fontLight, {color: "#fff"}]}>{this.props.boxDescription}</Text>
                            </View>
                            {this.props.children}
                        </View>
                    </View>
                </ScrollView>
                {
                    !this.props.hideHeader ?
                    <Animated.View style={[style.header, !this.state.logoVisible ? globalStyles().shadow : null, { height: headerHeight }, this.state.logoVisible ? { overflow: "hidden" } : null]} pointerEvents="box-none">
                        <TouchableWithoutFeedback onPress={this.onCoverImageClick}>
                            <Animated.Image
                                style={[
                                    style.backgroundImage,
                                    { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },
                                ]}
                                source={this.props.photo ? { uri: this.props.photo } : this.props.defaultSource ? this.props.defaultSource : null}
                                pointerEvents="none"
                                defaultSource={require('../../assets/images/agency_bg.png')}
                            />
                        </TouchableWithoutFeedback>
                        
                        <Animated.View style={[style.overlay, { opacity: imageOpacity }]} pointerEvents="none">
                        </Animated.View>
                        <View style={{ zIndex: 10003 }}>
                            <Header
                                back={false}
                                style={{ backgroundColor: "transparent", flexDirection: "row-reverse" }}
                                noLogo={true}
                                noMenu={true}
                            />
                            <APBackButton onBack={this.props.onBack} />
                            {
                                this.props.showIconOnHeader ?
                                <TouchableWithoutFeedback onPress={this.props.onIconClick}>
                                    <Animated.View style={[style.icon, { opacity: iconOpacity, top: Platform.OS === "android" ? 6 : 6 + this.props.ios_toolbar_height }]} pointerEvents={this.state.logoVisible ? "none" : "auto"}>
                                        <APIcon name={this.props.icon} color="#fff" size={24} paddingVertical={10} />
                                    </Animated.View>
                                </TouchableWithoutFeedback>:
                                null
                            }
                            
                        </View>
                        <View style={style.headerText} pointerEvents="box-none">
                            <Animated.View style={{ transform: [{ scale: TextScale }, { translateX: TextTransform }, { translateY: TextTransformY }] }} pointerEvents="box-none" onLayout={(event) => this.setState({ titleWidth: event.nativeEvent.layout.width })}>
                                <Text style={[style.headerTextTitle, { paddingHorizontal: tableRowPaddingHorizontal }, globalStyles().fontBold]} numberOfLines={!this.state.logoVisible ? 1 : null} pointerEvents="none" >{this.props.title}</Text>
                            </Animated.View>
                            <Animated.View style={[style.headerBottomText, { opacity: headerBottomTextOpacity, paddingHorizontal: tableRowPaddingHorizontal }]} pointerEvents="box-none">
                                <View style={{ width: this.state.descWidth ? this.state.descWidth : "auto" }}>
                                    <Text numberOfLines={1} style={[style.headerTextDesc, globalStyles().fontBold]} pointerEvents="none" onLayout={this.onDescLayout} >{this.props.description}</Text>
                                </View>
                                {this.props.renderBadge()}
                            </Animated.View>
                        </View>

                    </Animated.View>
                    :null
                    }
                {
                    this.props.icon ?
                    <TouchableWithoutFeedback onPress={this.onIconClick}>
                        <Animated.View style={[style.circular, globalStyles().shadow, { top: circularTop, transform: [{ scale: TextScale }], opacity: circularOpacity }] }>
                            <APIcon size={24} color="#7cb342" name={this.props.icon} />
                        </Animated.View>
                    </TouchableWithoutFeedback> : null
                }
                
                {
                    this.props.fixedContent ? 
                    this.props.fixedContent.map(el => el) : 
                    null
                }
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "#F2F2F2",
        flex: 1
    },
    
    wrapper: {
        padding: 0,
        backgroundColor: "#fff"
    },

    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#7cb342',
        zIndex: 10001
    },

    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    overlay: {
        position: "absolute",
        backgroundColor: "rgba(0,0,0,.3)",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%"
    },
    icon: {
        right: 0,
        paddingHorizontal: 18,
        position: "absolute",
        zIndex: 10002
    },

    headerText: {
        position: "absolute",
        bottom: 0,
        justifyContent: "center",
        width: "100%"
    },

    headerTextTitle: {
        color: "#fff",
        paddingHorizontal: 20,
        paddingVertical: 16,
        fontSize: 28
    },
    headerBottomText: {
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingBottom: 16,
        alignItems: "center"
    },

    headerTextDesc: {
        fontSize: 16,
        color: "#fff"
    },
    empty: {
        height: HEADER_MAX_HEIGHT,
        backgroundColor: "#7cb342"
    },
    greenBox: {
        backgroundColor: "#7cb342",
        paddingVertical: 24,
        width: "100%"
    },
    boxTitle: {
        color: "#fff",
        paddingBottom: 8,
        fontSize: 24,
        lineHeight: 32
    },

    circular: {
        width: 56,
        height: 56,
        borderRadius: 28,
        backgroundColor: '#fff',
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        right: 16,
        top: 208,
        zIndex: 10002
    }
})

const mapStateToProps = (state) => {
    return {
        ios_toolbar_height: state.index.ios_toolbar_height,
        iphoneXSpace: state.index.iphoneXSpace,
        prevPage: state.index.prevPage
    }
}

export default connect(mapStateToProps, null)(AnimatedView)

var stateOfButton;
export const buttonState = (currentState,checkBackPress) =>{
  stateOfButton = currentState;
  if(checkBackPress){
    stateOfButton = !currentState
  }

}