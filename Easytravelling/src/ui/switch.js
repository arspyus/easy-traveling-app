import React from 'react';
import {Switch, View, StyleSheet, Text, Platform} from 'react-native';

import globalStyles from '../utils/style';

const APSwitch = (props) => {
    return (
        <View style={style.container}>
            <Text style={[globalStyles().text, globalStyles().fontBold]}>{props.label}</Text>
            <View >
                <Switch {...props} thumbTintColor={Platform.OS === "android" ? props.value ? "#7cb342": "#ffffff" : null} onTintColor="#B1C49C" tintColor={Platform.OS === "android" ? "#bcbcbc" : null} />
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    }
})

export default APSwitch