import React, { Component } from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Animated, Text } from 'react-native';

import globalStyles from '../utils/style';

class APRadioList extends Component {

    state = {
        value: this.props.value,
        options: this.props.options.map(op => {
            return { ...op, animation: new Animated.Value(0), checked: false }
        })
    }

    componentWillReceiveProps(nextProps){
        if(this.props.options && this.props.options !== nextProps.options){
            this.setState({
                ...this.state,
                options: nextProps.options.map(op => {
                    return { ...op, animation: new Animated.Value(0), checked: false, initial: false }
                })
            })
        }
    }

    startAnimation(animation, to) {
        Animated.timing(animation, {
            toValue: to,
            duration: 250,
            useNativeDriver: true
        }).start();
    }

    onCheck = (index, value) => {

        let option = this.state.options[index];
        if(option.checked) return;
        let options = this.state.options.map((el, ind) => {
            return ind === index ? {...el, checked: true} : {...el, checked: false}
        })

        let val = value;
        this.setState({
            ...this.state,
            options: options,
            value: val
        }, () => {
            this.state.options.forEach((el, ind) => {
                this.startAnimation(el.animation, el.checked ? 1 : 0);
            });
            if(this.props.onChangeText){
                this.props.onChangeText(val);
            }
        })
    }

    render() {
        let radios = null;

        radios = this.state.options.map((el, index) => {

            return (
                <TouchableWithoutFeedback onPress={() => this.onCheck(index, el.value)} key={index}>
                    <View style={style.row}>
                        <View style={[style.radio, {borderColor: this.props.isValid ? el.checked ? "#7cb342" : "#5a5a5a" : "#F44336"}]}>
                            <Animated.View style={[style.inner, {opacity: el.animation}]}></Animated.View>
                        </View>
                        <Text style={[style.text, globalStyles().fontLight]}>{el.text}</Text>
                    </View>
                </TouchableWithoutFeedback>
            )
        })

        return (
                <View style={style.container}>
                    {radios}
                </View>
        );
    }

}

const style = StyleSheet.create({
    container: {
        width: "100%"
    },
    row: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginBottom: 10
    },
    radio: {
        borderRadius: 8,
        width: 16,
        height: 16,
        borderWidth: 2,
        borderColor: "#5a5a5a",
        marginVertical: 4,
        marginLeft: 4,
        marginRight: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        lineHeight: 25,
        color: "#9e9e9e",
        fontSize: 14
    },
    inner: {
        width: 8,
        height:8,
        borderRadius: 4,
        backgroundColor: "#7cb342"
    }
});

export default APRadioList;