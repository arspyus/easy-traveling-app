import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableWithoutFeedback} from 'react-native';

import APIcon from './icon';

import globalStyles from '../utils/style';
import generateArray from '../utils/generateArray';

class APStarSelect extends Component {

    state={
        stars: generateArray(5, false)
    }

    selectStar = (index) => {
        this.setState({
            stars: this.state.stars.map((el, ind) => {return ind === index ? !el : el})
        }, () => {
            let value = this.state.stars.map((el, index) => el ? index + 1 : null).filter((el, index) => el);
            this.props.onChange && this.props.onChange(value)
        })
    }

    render() {
        let stars = this.state.stars.map((el, index) => {
            return (
                <TouchableWithoutFeedback key={index} onPress={() => this.selectStar(index)}>
                    <View style={{marginRight: 10}}>
                        <APIcon name="star" color={el ? "#E8C231" : "#BBBBBB"} size={40} />
                        <View style={style.number}>
                            <Text style={[style.text, globalStyles().fontBold]}>{index + 1}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            )
        })

        return (
            <View style={style.container}>
                {stars}
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        flexDirection: "row",
        marginBottom: 20,
        marginTop: 10
    },

    number: {
        width: "100%",
        height: "100%",
        left: 0,
        top: 0,
        position: "absolute",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center"
    },

    text: {
        color: "#ffffff",
        fontSize: 10
    }
})

export default APStarSelect;