import React from 'react';
import {ActivityIndicator, Platform} from 'react-native';


const APLoader= (props) => {
    return (
        <ActivityIndicator size={Platform.OS === "android" ? 64: "large"} color="#7cb342" />
    )
}

export default APLoader
