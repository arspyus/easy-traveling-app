import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Platform, DatePickerAndroid } from 'react-native';
import CustomDatePickerIOS from './iosDatePicker';

import translate from '../utils/translate';
import globalStyles from '../utils/style';
import formatDate from '../utils/formatDate';

class APDateRangePicker extends Component {

    constructor() {
        super();

        this.state = {
            valueFrom: null,
            valueTo: null,
            show: false
        }
    }

    componentWillReceiveProps = (nextProps) => {
        if(!this.props.isEmpty && nextProps.isEmpty){
            this.setState({
                ...this.state,
                valueFrom: "",
                valueTo: ""
            })
        }
    }

    getDate = (date) => {
        if(!date) return null;
        return date.getFullYear() + "-" + (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate())
    }

    showDatePicker = (type) => {
        if (Platform.OS === "android") {
            let minDate = new Date();
            let start = new Date();
            if(type === "to"){
                if(this.state.valueFrom){
                    start = new Date(this.state.valueFrom);
                }
                minDate = start.setDate(start.getDate() + this.props.range);
            }

            let options = {
                minDate: minDate,
                date: type === "from" ? this.state.valueFrom || minDate : this.state.valueTo || minDate,
                mode: "default"
            }
            if (type === "from" && this.state.valueTo) {
                let fromMax = new Date(this.state.valueTo);
                fromMax.setDate(fromMax.getDate() - this.props.range);
                options.maxDate = fromMax
            }

            DatePickerAndroid.open(options).then(data => {
                if (data.action != DatePickerAndroid.dismissedAction) {
                    const d = data.year + "-" + (data.month + 1 < 10 ? "0" + (data.month + 1) : (data.month + 1)) + "-" + (data.day < 10 ? "0" + data.day : data.day);
                    this.setState({
                        ...this.state,
                        valueFrom: type === "from" ? new Date(d) : this.state.valueFrom,
                        valueTo: type === "to" ? new Date(d) : this.state.valueTo
                    }, () => {
                        if (this.props.onChange) {
                            this.props.onChange({
                                from: this.getDate(this.state.valueFrom),
                                to: this.getDate(this.state.valueTo),
                                type: type
                            })
                        }
                    });

                }

            })
        } else {
            this.setState({
                ...this.state,
                show: type
            })
        }
    }

    onIOSDatePickerChange = (d) => {
        const date = new Date(d);
        const formated = date.getFullYear() + "-" + (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate())
        this.setState({
            ...this.state,
            valueFrom: this.state.show === "from" ?  date : this.state.valueFrom,
            valueTo: this.state.show === "to" ? date : this.state.valueTo,
            show: false
        }, () => {
            if (this.props.onChange) {
                this.props.onChange({
                    from: this.getDate(this.state.valueFrom),
                    to: this.getDate(this.state.valueTo),
                    type: this.state.show
                })
            }
        })
    }

    render() {
        let minDate = new Date();
        let maxDate = null;
        let start = new Date();
        if(this.state.show === "to"){
            if(this.state.valueFrom){
                start = new Date(this.state.valueFrom);
            }
            minDate = start.setDate(start.getDate() + this.props.range);
        }

        if (this.state.show === "from" && this.state.valueTo) {
            let fromMax = new Date(this.state.valueTo);
            fromMax.setDate(fromMax.getDate() - this.props.range);
            maxDate = fromMax
        }

        return (
            <View style={style.container}>
                <TouchableWithoutFeedback onPress={() => this.showDatePicker("from")}>
                    <View style={[style.fieldset, { marginRight: 10 }]}>
                        <View style={style.label}>
                            <Text style={[style.labelText, globalStyles().fontBold, !this.props.isValid.from ? style.errorText: null]}>{this.props.labelFrom}</Text>
                        </View>
                        <View style={[style.field, !this.props.isValid.from ? style.errorField: null]}>
                            <Text style={[style.fieldsetText, globalStyles().fontLight, !this.props.isValid.from ? style.errorText: null]}>{formatDate(this.state.valueFrom) || translate["select_date"][this.props.lang]}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.showDatePicker("to")}>
                    <View style={[style.fieldset]}>
                        <View style={style.label}>
                            <Text style={[style.labelText, globalStyles().fontBold,  !this.props.isValid.to ? style.errorText: null]}>{this.props.labelTo}</Text>
                        </View>
                        <View style={[style.field, !this.props.isValid.to ? style.errorField: null]}>
                            <Text style={[style.fieldsetText, globalStyles().fontLight, !this.props.isValid.to ? style.errorText: null]}>{formatDate(this.state.valueTo) || translate["select_date"][this.props.lang]}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                {Platform.OS === "ios" ?
                    <View style={style.iosDatePicker}>
                        <CustomDatePickerIOS
                            date={this.state.show === "from" ? this.state.valueFrom || new Date() : this.state.valueTo || new Date()}
                            minimumDate={this.state.show === "to" && this.state.valueFrom ? new Date(minDate): new Date()}
                            maximumDate={this.state.show === "from" && this.state.valueTo ? new Date(maxDate) : null}
                            onConfirm={this.onIOSDatePickerChange}
                            mode="date"
                            isVisible={this.state.show !== false}
                            onCancel={() => this.setState({ ...this.state, show: false })}
                            cancelTextIOS={translate["cancel"][this.props.lang]}
                            confirmTextIOS={translate["confirm"][this.props.lang]}
                            titleIOS={translate["date"][this.props.lang]}
                        />
                    </View> : null}
            </View>
        )
    }
}

const style = StyleSheet.create({
    iosDatePicker: {
    },

    container: {
        width: "100%",
        overflow: "hidden",
        flexDirection: "row",
        marginTop: 5
    },

    fieldset: {
        flex: 1
    },

    field: {
        borderWidth: 1,
        borderColor: "#bcbcbc",
        borderRadius: 4,
        paddingHorizontal: 15,
        paddingVertical: 9,
        marginTop: 7
    },

    fieldsetText: {
        fontSize: 14,
        color: "#515151",
        lineHeight: 19
    },

    label: {
        position: "absolute",
        top: 0,
        left: 14,
        backgroundColor: "#fff",
        zIndex: 10,
        paddingHorizontal: 3,
        alignItems: "center",
        justifyContent: "center"
    },

    labelText: {
        color: "#bcbcbc",
        fontSize: 10,
        lineHeight: 14
    },

    errorField: {
        borderColor: "#f44336"
    },

    errorText: {
        color: "#f44336"
    }

})

export default APDateRangePicker;