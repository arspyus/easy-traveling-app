import React from 'react';
import {View, StyleSheet, Text, TouchableWithoutFeedback} from 'react-native';

import globalStyles from '../utils/style';
const APLink = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.onClick}>
            <View style={styles.container}>
                <Text style={[styles.text,globalStyles().fontLight, props.style]}>{props.children}</Text>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center"
    },
    text: {
        fontSize: 14,
        color: "#7cb342"
    }
})

export default APLink;