import React from 'react';
import {Text} from 'react-native';

const mapping = {
    "offers_response_5_place" : "&#xe904;",
    "offers_response_4_place" : "&#xe905;",
    "offers_response_3_place" : "&#xe906;",
    "offers_response_2_place" : "&#xe908;",
    "offers_response_1_place" : "&#xe909;",
    "TROPHY": "&#xe907;",
    "facebook": "&#xe903;",
    "google": "&#xea88",
    "instagram": "&#xe902;",
    "world":  "&#xe900;",
    "armenia": "&#xe901;",
    "logo_horizontal": "&#xe90a;",
    "logo_final": "&#xe90b;"

}

const APExtraIcon = (props) => {
    if(props.name === "offers_response_1_place"){
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe904;</Text>
    } else if (props.name === "offers_response_2_place") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe905;</Text>
    } else if (props.name === "offers_response_3_place") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe906;</Text>
    } else if (props.name === "offers_response_4_place") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe908;</Text>
    } else if (props.name === "offers_response_5_place") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe909;</Text>
    } else if (props.name === "TROPHY") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe907;</Text>
    } else if (props.name === "facebook") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe903;</Text>
    } else if (props.name === "google") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xea88;</Text>
    } else if (props.name === "instagram") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe902;</Text>
    } else if (props.name === "world") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe900;</Text>
    } else if (props.name === "armenia") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe901;</Text>
    } else if (props.name === "logo_horizontal") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe90a;</Text>
    } else if (props.name === "logo_final") {
        return <Text style={{fontFamily: "icomoon", color: props.color, fontSize: props.size}}>&#xe90b;</Text>
    }
    
}

export default APExtraIcon;

