import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text, Platform } from 'react-native';

import APIcon from './icon';
import APNotificationCircle from './notificationCircle';
import TourTypeMenu from '../components/tourTypeMenu';

import globalStyles from '../utils/style';

const Header = (props) => {
    let content =  <View style={style.logo}><APIcon name="logo_horizontal" size={194} color="#fff"  /></View>

    if (props.noLogo) {
        content = null;
    }

    if (props.title) {
        content = <View style={{ width: props.back || props.filter || props.tourTypeMenu ? "65%" : "80%" }}><Text numberOfLines={1} style={[style.title, globalStyles().fontBold]}>{props.title}</Text></View>
    }

    return (
        <View style={[style.container, { height: Platform.OS === "android" ? 56 : 56 +props.ios_toolbar_height, paddingTop: Platform.OS === "android" ? 0 : props.ios_toolbar_height}, globalStyles().shadow, {elevation: 7}, props.style , props.reverse ? {flexDirection: "row-reverse"} : null]}>
            {!props.noMenu ?
                <TouchableWithoutFeedback onPress={props.onMenu}>
                    <View style={style.menu}>
                        <APIcon name="menu" color="#fff" size={24} paddingVertical={10} />
                        {props.notificationCircle ? <APNotificationCircle style={{position: "absolute", top: 13, right: 18}}/> : null}
                    </View>
                </TouchableWithoutFeedback> : 
                 <View style={style.icon}><View style={style.back_hidden}></View></View>
            }
            {content}
            <View>
                {props.back ? 
                    <TouchableWithoutFeedback onPress={props.onBack}>
                        <View style={style.icon}>
                            <APIcon name="keyboard_backspace" color="#fff" size={24} paddingVertical={10}/>
                        </View>
                    </TouchableWithoutFeedback> :
                    props.filter ? <View> 
                                        <TouchableWithoutFeedback onPress={props.onFilter}>
                                            <View style={style.icon}>
                                                {
                                                    props.filterIcon ?
                                                    props.filterIcon : 
                                                    <APIcon name="filter_list" color="#fff" size={24} paddingVertical={10}/>
                                                }
                                                
                                            </View>
                                        </TouchableWithoutFeedback> 
                                        {props.filterContent}
                                    </View> :
                                    props.tourTypeMenu ? 
                                    <View style={style.icon}>
                                        <TourTypeMenu 
                                            value={props.tourTypeValue}
                                            lang={props.lang}
                                            onSelect={props.ontourTypeChange}
                                        />
                                    </View> :
                                    <View style={style.icon}><View style={style.back_hidden}/></View> 
                }
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "#7cb342",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        zIndex: 10001,
        width: "100%"
    },

    menu: {
        paddingHorizontal: 18
    },

    back_hidden: {
        width: 24
    },

    title: {
        color: "#fff",
        fontSize: 20
    },

    logo: {
        marginTop: Platform.OS === "android" ? 0 : -68
    },

    icon: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 18
    }
});

export default Header;