import React from 'react';
import {View, StyleSheet} from 'react-native';

const APDivider = (props) => {
    return (
            <View style={styles.container}>
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        width: "100%",
        height: 1,
        marginVertical: 2,
        backgroundColor: "#e0e0e0"
    }
})

export default APDivider;