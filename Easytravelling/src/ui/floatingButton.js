import React, {Component} from 'react';
import {View, StyleSheet, Text, Animated, TouchableWithoutFeedback} from 'react-native';

import globalStyles from '../utils/style';
class APFloatingButton extends Component {
    constructor() {
        super();
        this.animatedValue = new Animated.Value(0);
    }

    show() {
        this.animatedValue.setValue(0);
        Animated.timing(this.animatedValue, {
            toValue: 1,
            duration: 300,
            useNativeDriver: true
        }).start();
        
    }

    hide() {
        this.animatedValue.setValue(1);
        Animated.timing(this.animatedValue, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true
        }).start();
    }

    componentDidUpdate(prevProps){
        if(prevProps.isVisible !== this.props.isVisible){
            if(this.props.isVisible){
                this.show();
            } else {
                this.hide()
            }
        }
    }

    render() {

        let bottom = this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [70, 0],
        });

        return(
            <Animated.View 
                style={[
                    style.container,
                    this.props.containerStyle,
                    !this.props.static ? {
                        transform: [
                            {
                                translateY: bottom
                            }
                        ]
                    } : null,
                ]}
            >
                <View style={style.wrapper}>
                    <View style={[style.row, {alignItems: "flex-start"}]}>
                        <Text style={[style.title, globalStyles().fontBold]} numberOfLines={1}>{this.props.title || ""}</Text>
                        <Text style={[style.desc, globalStyles().fontLight]} numberOfLines={1}>{this.props.description || ""}</Text>
                    </View>
                    <View style={[style.row, {alignItems: "flex-end"}]}>
                        <TouchableWithoutFeedback onPress={this.props.onClick}>
                            <View style={style.button}>
                                <Text style={[style.buttonText, globalStyles().fontBold]} numberOfLines={1}>{this.props.buttonText || ""}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </Animated.View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        position: "absolute",
        zIndex: 1000000,
        bottom: 0,
        left: 0,
        width: '100%',
        height: 70,
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 8.4,
        },
        shadowOpacity: 0.58,
        shadowRadius: 11.2,
        elevation: 16
    },
    wrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 15
    },
    row: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },
    title: {
        fontSize: 16,
        lineHeight: 22,
        textAlign: "left"
    },

    desc: {
        fontSize: 12,
        lineHeight: 17,
        textAlign: "left"
    },

    button: {
        borderRadius: 10,
        backgroundColor: "#7cb342",
        paddingHorizontal: 8,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 7,
        minWidth: 150
    },
    buttonText: {
        fontSize: 16,
        lineHeight: 24,
        letterSpacing: 0.18,
        color: "#fff"
    }
})

export default APFloatingButton;