import React, { Component } from 'react'
import { Text } from 'react-native'
import PropTypes from 'prop-types'

import globalStyles from './../../utils/style';

export default class extends Component {
  static propTypes = {
    error: PropTypes.string,
    errorPaddingTop: PropTypes.number,
    errorColor: PropTypes.string,
    errorFontSize: PropTypes.number,
    tintMessage: PropTypes.string,
    tintMessageColor: PropTypes.string,
  }

  static defaultProps = {
    errorPaddingTop: 8,
    errorColor: '#fc1f4a',
    errorFontSize: 12
  }

  render() {
    let { error, errorColor, errorPaddingTop, errorFontSize, tintMessage, tintMessageColor } = this.props
    return (
      <Text
        style={[{
          paddingTop: errorPaddingTop,
          color: tintMessageColor || errorColor,
          fontSize: errorFontSize,
          
        }, globalStyles().fontLight]}>
        {error || tintMessage}
      </Text>
    )
  }
}
