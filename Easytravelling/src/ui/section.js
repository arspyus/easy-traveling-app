import React from 'react';
import {View, StyleSheet} from 'react-native';
import globalStyles from '../utils/style';

const APSection = (props) => {
    return (
        <View style={[style.container, globalStyles().shadow, props.style]}>
            {props.children}
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        marginBottom: 10,
        borderRadius: 10,
        width: "100%",
        alignSelf: "center",
        overflow: "hidden" 
    }
})

export default APSection;