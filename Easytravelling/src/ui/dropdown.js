import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  ListView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
  Animated,
  Platform
} from 'react-native';
import APIcon from './icon';

import globalStyles from '../utils/style';

const TOUCHABLE_ELEMENTS = [
  'TouchableHighlight',
  'TouchableOpacity',
  'TouchableWithoutFeedback'
];

class APDropdown extends Component {
  //dropdownTextHighlightStyle - highlighted text style
  //dropdownTextStyle - row text style
  //dropdownRowStyle - row style
  //dropdownStyle - dropdown style(should have height for better calculation)
  //textStyle - button style
  //renderRow, renderSeparator, adjustFrame
  //defaultValue - button text
  //defaultIndex - selected option
  // options - options
  // icon - icon
  constructor(props) {
    super(props);

    this._button = null;
    this._buttonFrame = null;
    this._nextValue = null;
    this._nextText = null;
    this._nextIndex = null;

    let selected = props.options.filter(op => { return op.value === props.defaultValue })[0];
    this.state = {
      showDropdown: false,
      buttonText: selected ? selected.text : this.props.renderButtonText ? this.props.renderButtonText() : "",
      selectedIndex: props.defaultIndex,
      anim: new Animated.Value(0)
    };
  }

  componentWillReceiveProps(nextProps) {
    let { buttonText, selectedIndex } = this.state;
    const { defaultIndex, defaultValue, options } = nextProps;
    buttonText = this._nextText == null ? buttonText : this._nextText;
    selectedIndex = this._nextIndex == null ? selectedIndex : this._nextIndex;
    if (selectedIndex < 0) {
      selectedIndex = defaultIndex;
      if (selectedIndex < 0) {
        buttonText = defaultValue;
      }
    }
    this._nextValue = null;
    this._nextText = null;
    this._nextIndex = null;

    this.setState({
      buttonText,
      selectedIndex
    });
  }

  render() {
    return (
      <View {...this.props}>
        {this._renderButton()}
        {this._renderModal()}
      </View>
    );
  }

  _updatePosition(callback) {
    if (this._button && this._button.measure) {
      this._button.measure((fx, fy, width, height, px, py) => {
        this._buttonFrame = { x: px, y: py, w: width, h: height };
        callback && callback();
      });
    }
  }

  show() {
    this._updatePosition(() => {
      this.setState({
        showDropdown: true
      }, () => {
        const windowHeight = Dimensions.get("window").height;
        const bottomSpace = windowHeight - this._buttonFrame.y - this._buttonFrame.h;
        const dropdownHeight = (this.props.dropdownStyle && StyleSheet.flatten(this.props.dropdownStyle).height);
        const showInBottom = bottomSpace >= dropdownHeight || bottomSpace >= this._buttonFrame.y;

        let max = showInBottom ? bottomSpace : this._buttonFrame.y;

        const h = Math.min((50 + StyleSheet.hairlineWidth) * (this.props.maxItems || this.props.options.length), max);
        const height = (this.props.dropdownStyle && StyleSheet.flatten(this.props.dropdownStyle).height) || h;
        Animated.timing(this.state.anim, {
          duration: 400,
          toValue: height
        }).start();
      });
    });
  }

  hide() {
    this.setState({
      showDropdown: false
    }, () => {
      Animated.timing(this.state.anim, {
        duration: 200,
        toValue: 0
      }).start();
    });
  }

  select(idx) {
    const { defaultValue, options, defaultIndex, renderButtonText } = this.props;

    let value = defaultValue;
    let text = options.filter((op) => { return op.value === defaultValue })[0].text;
    if (idx == null || !options || idx >= options.length) {
      idx = defaultIndex;
    }

    if (idx >= 0) {
      value = options[idx].value.toString();
      text = renderButtonText ? renderButtonText(options[idx]) : options[idx].text.toString();
    }

    this._nextValue = value;
    this._nextText = text;
    this._nextIndex = idx;

    this.setState({
      buttonText: text,
      selectedIndex: idx
    });
  }

  selectByValue(value) {
    let idx = this.props.options.map((el, index) => el.value).indexOf(value);
    this.select(idx)
  }

  _renderButton() {
    const { disabled, children, textStyle, icon, iconColor, iconSize } = this.props;
    const { buttonText } = this.state;

    return (
      <TouchableOpacity ref={button => this._button = button} disabled={disabled} onPress={this._onButtonPress}>
        {
          children ||
          (
            <View style={styles.button}>
              <Text style={[styles.buttonText, textStyle]}
                numberOfLines={1}
              >
                {buttonText}
              </Text>
              {icon ?

                (<View style={styles.icon}>
                  <APIcon size={iconSize || 16} color={iconColor} name={icon === true ? "keyboard_arrow_down" : icon} />
                </View>) :
                null
              }
            </View>
          )
        }
      </TouchableOpacity>
    );
  }

  _onButtonPress = () => {
    const { onDropdownWillShow } = this.props;
    if (!onDropdownWillShow || onDropdownWillShow() !== false) {
      this.show();
    }
  };

  _renderModal() {
    const { animated, dropdownStyle } = this.props;
    const { showDropdown } = this.state;
    if (showDropdown && this._buttonFrame) {
      const frameStyle = this._calcPosition();
      const animationType = animated ? 'fade' : 'none';
      return (
        <Modal animationType={animationType}
          visible={true}
          transparent={true}
          onRequestClose={this._onRequestClose}
          supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
        >
          <TouchableWithoutFeedback disabled={!showDropdown} onPress={this._onModalPress}
          >
            <View style={styles.modal} >
              <Animated.View style={[styles.dropdown, globalStyles().shadow, dropdownStyle, frameStyle, { height: this.state.anim }]}>
                {this._renderDropdown()}
              </Animated.View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      );
    }
  }

  _calcPosition() {
    const { dropdownStyle, style, adjustFrame } = this.props;

    const dimensions = Dimensions.get('window');
    const windowWidth = dimensions.width;
    const windowHeight = dimensions.height;

    const h = Math.min((50 + StyleSheet.hairlineWidth) * (this.props.maxItems || this.props.options.length), windowHeight);
    let dropdownHeight = (dropdownStyle && StyleSheet.flatten(dropdownStyle).height) || h

    const bottomSpace = windowHeight - this._buttonFrame.y - this._buttonFrame.h;
    const rightSpace = windowWidth - this._buttonFrame.x;
    const showInBottom = bottomSpace >= dropdownHeight || bottomSpace >= this._buttonFrame.y;
    const showInLeft = rightSpace >= this._buttonFrame.x;
    const topOffset = this.props.topOffset || 0;
    const bottomOffset = this.props.bottomOffset || 0;
    dropdownHeight = showInBottom ? Math.min(bottomSpace, dropdownHeight) : Math.min(dropdownHeight, this._buttonFrame.y);
    const positionStyle = {
      height: dropdownHeight,
      top: showInBottom ? this._buttonFrame.y + this._buttonFrame.h - topOffset: Math.max(Platform.OS === "android" ? 36 : 22, this._buttonFrame.y - dropdownHeight - bottomOffset)
    };
    if (showInLeft) {
      positionStyle.left = this._buttonFrame.x;
    } else {
      const dropdownWidth = (dropdownStyle && StyleSheet.flatten(dropdownStyle).width) ||
        (style && StyleSheet.flatten(style).width) || -1;
      if (dropdownWidth !== -1) {
        positionStyle.width = dropdownWidth;
      }
      positionStyle.right = rightSpace - this._buttonFrame.w;
    }

    return adjustFrame ? adjustFrame(positionStyle) : positionStyle;
  }

  _onRequestClose = () => {
    const { onDropdownWillHide } = this.props;
    if (!onDropdownWillHide ||
      onDropdownWillHide("onRequestClose") !== false) {
      this.hide();
    }
  };

  _onModalPress = () => {
    const { onDropdownWillHide } = this.props;
    if (!onDropdownWillHide ||
      onDropdownWillHide("onModalPress") !== false) {
      this.hide();
    }
  };

  _renderDropdown() {
    const { scrollEnabled, renderSeparator } = this.props;
    return (
      <ListView scrollEnabled={scrollEnabled}
        dataSource={this._dataSource}
        renderRow={this._renderRow}
        renderSeparator={renderSeparator || this._renderSeparator}
        automaticallyAdjustContentInsets={false}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={"always"}
      />
    );
  }

  get _dataSource() {
    const { options } = this.props;
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    return ds.cloneWithRows(options.map(op => op.text));
  }

  _renderRow = (rowData, sectionID, rowID, highlightRow) => {

    const { renderRow, dropdownTextStyle, dropdownTextHighlightStyle, dropdownRowStyle } = this.props;
    const { selectedIndex } = this.state;
    const key = `row_${rowID}`;
    const highlighted = rowID == selectedIndex;
    let textHighlightStyle = { backgroundColor: "#F1F1F1" };
    if (dropdownTextHighlightStyle) {
      textHighlightStyle = { ...textHighlightStyle, ...dropdownTextHighlightStyle };
    }
    const row = !renderRow ?
      (<View style={[styles.dropdownStyle, dropdownRowStyle, highlighted ? textHighlightStyle : ""]}>
        <Text style={[styles.rowText, globalStyles().fontLight, dropdownTextStyle]}>{rowData}</Text>
      </View>) :
      renderRow(rowData, rowID, highlighted);
    const preservedProps = {
      key,
      onPress: () => this._onRowPress(rowData, sectionID, rowID, highlightRow),
    };
    if (TOUCHABLE_ELEMENTS.find(name => name == row.type.displayName)) {
      const props = { ...row.props };
      props.key = preservedProps.key;
      props.onPress = preservedProps.onPress;
      const { children } = row.props;
      return (
        <TouchableWithoutFeedback {...props}>
          {children}
        </TouchableWithoutFeedback>
      );
    }
    return (
      <TouchableWithoutFeedback {...preservedProps}>
        {row}
      </TouchableWithoutFeedback>
    );
  };

  _onRowPress(rowData, sectionID, rowID, highlightRow) {
    const { onSelect, renderButtonText, onDropdownWillHide, options } = this.props;
    const op = options.filter(op => op.text === rowData.toString())[0];
    if (!onSelect || onSelect(rowID, op.value) !== false) {
      highlightRow(sectionID, rowID);
      const value = op.value;
      const text = renderButtonText && renderButtonText(rowData) || rowData.toString();
      this._nextValue = value;
      this._nextText = text;
      this._nextIndex = rowID;
      this.setState({
        buttonText: text,
        selectedIndex: rowID
      });
    }
    if (!onDropdownWillHide || onDropdownWillHide("onRowPress") !== false) {
      this.hide()
    }
  }

  _renderSeparator = (sectionID, rowID, adjacentRowHighlighted) => {
    const key = `spr_${rowID}`;
    return (
      <View style={styles.separator} key={key} />
    );
  };
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    flexDirection: "row"
  },
  buttonText: {
    fontSize: 12
  },
  icon: {
    marginLeft: 10,
    justifyContent: "center"
  },
  modal: {
    flexGrow: 1
  },
  dropdown: {
    position: 'absolute',
    height: (47 + StyleSheet.hairlineWidth) * 6,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'lightgray',
    borderRadius: 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    zIndex: 8500

  },
  dropdownStyle: {
    backgroundColor: "#fff",
    paddingVertical: 14,
    paddingHorizontal: 16
  },

  rowText: {
    fontSize: 16,
    color: '#424242',
    lineHeight: 22
  },

  separator: {
    height: 0,
    backgroundColor: 'transparent'
  }
});

export default APDropdown;