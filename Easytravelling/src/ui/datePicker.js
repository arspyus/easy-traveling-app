import React , {Component} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, Platform, DatePickerAndroid, DatePickerIOS} from 'react-native';
import CustomDatePickerIOS from './iosDatePicker';

import APInput from './input';

import translate from '../utils/translate';

class APDatePicker extends Component {

    state = {
        value: this.props.value,
        show: false,
        iosChoosenDate: new Date()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.value != nextProps.value) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    showDatePicker = () => {
        if(Platform.OS === "android"){
            let options = {
                minDate: this.props.min ? new Date(this.props.min) : new Date(),
                date: this.state.value ? new Date(this.state.value) : new Date(),
                mode: "default"
            }
            if(this.props.max){
                options.maxDate = new Date(this.props.max)
            }
            DatePickerAndroid.open(options).then(data => {
                if(data.action != DatePickerAndroid.dismissedAction){
                    const d = data.year + "-" + (data.month + 1 < 10 ? "0" + (data.month + 1) : (data.month + 1)) + "-" + (data.day < 10 ? "0" + data.day : data.day)
                    this.setState({
                        ...this.state,
                        value: d
                    });
                    if(this.props.onChangeText){
                        this.props.onChangeText(d)
                    }
                }
                
            })
        } else {

            this.setState({
                ...this.state,
                show: true
            })
            
        }
    }

    onIOSDatePickerChange =(d) =>{
        const date = new Date(d);
        const formated = date.getFullYear() + "-" + (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate())
        this.setState({
            ...this.state,
            value: formated,
            iosChoosenDate: d,
            show: false
        })
        if(this.props.onChangeText){
            this.props.onChangeText(formated)
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.showDatePicker}>
                <View style={styles.container}>
                    <APInput
                        {...this.props}
                        editable={false} 
                        selectTextOnFocus={false}
                        value={this.state.value}
                        isValid={this.props.isValid}
                        inputMargin={-25}
                    />
                    {Platform.OS === "ios"  ?
                        <View style={styles.iosDatePicker}>
                            <CustomDatePickerIOS
                                date={this.state.iosChoosenDate}
                                minimumDate={this.props.min ? new Date(this.props.min) : new Date()}
                                maximumDate={this.props.max ? new Date(this.props.max) : null }
                                onConfirm={this.onIOSDatePickerChange}
                                mode="date"
                                isVisible={this.state.show}
                                onCancel={() => this.setState({...this.state, show:false})}
                                cancelTextIOS={translate["cancel"][this.props.lang]}
                                confirmTextIOS={translate["confirm"][this.props.lang]}
                                titleIOS={translate["date"][this.props.lang]}
                            /> 
                        </View>: null }
                </View>
            </TouchableWithoutFeedback>
        )
    }
    
}

const styles = StyleSheet.create({
    iosDatePicker: {
        position: "absolute",
        width: "100%",
        height: "100%",
        left: 0,
        top: 0,
        backgroundColor: "transparent"
    },

    container: {
        width: "100%",
        marginBottom: 48,
        overflow: "hidden"
    },

    icon: {
        position: "absolute",
        right: 4,
        top: 20
    }

})

export default APDatePicker;