import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';

import { AccessToken, LoginManager } from 'react-native-fbsdk';
import APIcon from './icon';
import TouchableNativeFeedback from '../ui/touchableNativeFeedback';

import globalStyles from '../utils/style';
import translate from '../utils/translate';

import { signInWithFacebook } from '../store/actions/firebaseAction';
import { setMessage, set_loader } from '../store/actions/indexAction';

class APFacebookBtn extends Component {

    facebookLogin = async () => {
        if(!this.props.connection){
            this.props.showMessage("went-offline")
            return;
        }
        let credential;

        try {
            this.props.setLoader(true);
            LoginManager.logOut();
            
            const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
            if (result.isCancelled) {
                throw new Error('User cancelled request'); // Handle this however fits the flow of your app
            }


            // get the access token
            const data = await AccessToken.getCurrentAccessToken();

            if (!data) {
                throw new Error('Something went wrong obtaining the users access token'); // Handle this however fits the flow of your app
            }
            this.props.setLoader(false);
            this.props.signInWithFacebook(data.accessToken)

        } catch (error) {
            this.props.setLoader(false);
        }
    }

    render() {
        return (
            <TouchableNativeFeedback onPress={this.facebookLogin}>
                <View style={[style.container, globalStyles().shadow]}>
                    <APIcon name="facebook" size={20} color="#FEFEFE" />
                    <View style={style.buttonText}>
                        <Text style={[style.text, globalStyles().fontBold]}>{translate["facebook-btn-text"][this.props.lang]}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        )
    }
}

const style = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: "#3B5997",
        width: "100%",
        flexDirection: "row",
        paddingLeft: 14,
        marginBottom: 8,
        borderRadius: 2
    },
    buttonText: {
        flex: 1
    },
    text: {
        color: "#FBFBFC",
        lineHeight: 38,
        fontSize: 14,
        textAlign: "center"
    }
})

const mapStateToProps = (state) => {
    return {
        connection: state.index.connection
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signInWithFacebook: (token) => { dispatch(signInWithFacebook(token)) },
        showMessage: (message) => {dispatch(setMessage(true, message))},
        setLoader: (loader) => {dispatch(set_loader(loader))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APFacebookBtn)
