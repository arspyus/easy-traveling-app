import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import globalStyles from '../utils/style';
import translate from '../utils/translate';
const APStatusLoader= (props) => {
    let loader_width = 0;
    let loader_color = "#7cb342";
    switch (props.status) {
        case "pending":
            loader_width = (100 / 6 * 1) + "%"
            break;
        case "offered":
            loader_width = (100 / 6 * 2) + "%"
            break;
        case "pre-booked":
            loader_width = (100 / 6 * 3) + "%"
            break;
        case "processing":
            loader_width = (100 / 6 * 4) + "%"
            break;
        case "confirmed":
            loader_width = (100 / 6 * 5) + "%"
            break;
        case "booked":
            loader_width = (100 / 6 * 6) + "%"
            break;
        case "declined":
        case "canceled":
            loader_width = (75) + "%";
            loader_color = "#f44336"
            break;
    }
    return (
        
        <View style={style.container}>
            <View style={style.loader}>
                <View style={[style.loader_inner, { width: loader_width, backgroundColor: loader_color }]}>
                </View>
            </View>
            <Text style={[style.loader_text, style.small_text, globalStyles().fontLight]}>{translate[props.status][props.lang]}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        alignItems: "flex-end"
    },
    loader: {
        width: "100%",
        backgroundColor: "#bcbcbc",
        borderRadius: 10,
        height: 5
    },
    loader_inner: {
        height: "100%",
        borderRadius: 10
    },
    loader_text: {
        color: "#bcbcbc",
        paddingTop: 2
    },
})

export default APStatusLoader