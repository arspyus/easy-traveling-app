import React from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, Platform} from 'react-native';
import { connect } from 'react-redux';

import APIcon from './icon';

const APBackButton = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.onBack}>
            <View style={[style.back, { top: Platform.OS === "android" ? 6 : 6 + props.ios_toolbar_height }]} >
                <APIcon name="keyboard_backspace" color="#fff" size={24} paddingVertical={10} />
            </View>
        </TouchableWithoutFeedback>
    )
}

const style = StyleSheet.create({
    back: {
        left: 0,
        paddingLeft: 18,
        position: "absolute",
        zIndex: 10002
    }
})

const mapStateToProps = (state) => {
    return {
        ios_toolbar_height: state.index.ios_toolbar_height
    }
}

export default connect(mapStateToProps, null)(APBackButton)

