import React, { Component } from 'react';
import { View, Text, Platform} from 'react-native';
import { connect } from 'react-redux';
import APFullLoader from './fullLoader';
import APToast from './toast';
import APPlus from './plus';
import APIntro from './intro';
import APNoInternet from './noInternet';
import APBonusPopup from '../popups/bonusPopup';
import APAuthPopup from '../popups/authPopup';
import APConfirmationPopup from '../popups/confirmationPopup';
import APUpgradePopup from '../popups/upgradePopup';
import APEmailPopup from '../popups/emailPopup';

import Header from './header';

import { setMessage, setIntro, setToastSwiping } from '../store/actions/indexAction';

import translate from '../utils/translate';
import globalStyles from '../utils/style';
import iphoneX from '../utils/iphoneX';
import navigate from '../utils/navigate';

let toastTimer = null;
let _toast = null;

class APContainer extends Component {

    state = {
        showToast: this.props.isError && this.props.message
    }

    onHide = () => {
        clearTimeout(toastTimer);
        this.props.hideError();
        this.setState({
            showToast: false
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.message && nextProps.isError) {
            if ((!this.props.isError || (this.props.message != nextProps.message))) {
                this.setState({
                    showToast: true
                })
                clearTimeout(toastTimer);

                toastTimer = setTimeout(() => {

                    if (_toast) _toast.hide();
                    if (this._toast !== _toast) {

                        this._toast.hide();
                    }
                }, 5000)

            }
        }

        if (!this.props.toastSwiping && nextProps.toastSwiping && nextProps.message) {
            clearTimeout(toastTimer);
        }

        if (this.props.toastSwiping && !nextProps.toastSwiping && nextProps.message) {
            clearTimeout(toastTimer);
            toastTimer = setTimeout(() => {
                if (_toast) _toast.hide();
                if (this._toast !== _toast) {
                    this._toast.hide();
                }
            }, 5000)
        }


        if (this.props.message && !nextProps.message) {
            if (_toast) _toast.hide();
        }
    }

    onSignIn = () => {
        navigate(this.props.screenComponent, 'AuthScreen', 'push', {
            selectedTabIndex: 0
        });
    }

    render() {
        const isOnline = this.props.connection === true || this.props.connectionChanged || this.props.showOffline;
        return (
            <View style={[style.container, this.props.style]}>
                {
                    this.props.header !== false ?
                        <Header
                            {...this.props}
                            notificationCircle={this.props.unreadOffer || this.props.unreadSpecialOffer}

                        /> :
                        null
                }
                    {this.props.plus ? <APPlus lang={this.props.lang} onClick={() => { return this._intro && this.props.intro ? this._intro.close() : null }} onNavigate={this.props.onNavigate} /> : null}
                    {this.props.plus && this.props.intro && !this.props.isLoading && this.props.canShowIntro ? <APIntro lang={this.props.lang} onClose={this.props.hideIntro} ref={(ref) => this._intro = ref} /> : null}
                    {isOnline ? this.props.children : this.props.connection === false ? <APNoInternet /> : null}
                    {this.state.showToast && this.props.message ? <APToast
                        message={this.props.message ? translate[this.props.message][this.props.lang] : ""}
                        onHide={this.onHide}
                        ref={toast => { if(toast) {_toast = toast; this._toast = toast }}}
                        onSwiping={this.props.setToastSwiping}
                    /> : null}
                    {this.props.user ? <APBonusPopup isVisible={this.props.showBonusPopup} /> : null}
                    <APUpgradePopup isVisible={this.props.appShouldUpgrade} />
                    <APAuthPopup isVisible={this.props.showAuthPopup} onSignIn={this.onSignIn} />
                    <APConfirmationPopup isVisible={this.props.showConfirmationPopup} />
                    <APEmailPopup isVisible={this.props.showEmailPopup} />
                    {this.props.isLoading ? <APFullLoader /> : null}
            </View>
        )
    }

}

const style = {
    container: {
        flex: 1,
        backgroundColor: "#F2F2F2"
    },
    modalText: {
        fontSize: 14,
        lineHeight: 22,
        color: "#424242",
        textAlign: "left"
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.index.isLoading,
        lang: state.index.lang,
        user: state.user.user,
        isError: state.index.isError,
        message: state.index.message,
        toastSwiping: state.index.toastSwiping,
        intro: state.index.intro,
        connection: state.index.connection,
        connectionChanged: state.index.connectionChanged,
        unreadOffer: state.index.unreadOffer,
        unreadSpecialOffer: state.index.unreadSpecialOffer,
        canShowIntro: state.index.canShowIntro,
        appShouldUpgrade: state.index.appShouldUpgrade,
        prevPage: state.index.prevPage,
        ios_toolbar_height: state.index.ios_toolbar_height,
        showBonusPopup: state.index.showBonusPopup,
        showAuthPopup: state.index.showAuthPopup,
        showConfirmationPopup: state.index.showConfirmationPopup,
        showEmailPopup: state.index.showEmailPopup
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideError: () => { dispatch(setMessage(false, "")) },
        showError: (message) => { dispatch(setMessage(true, message)) },
        hideIntro: () => { dispatch(setIntro()) },
        setToastSwiping: (swiping) => { dispatch(setToastSwiping(swiping)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APContainer)