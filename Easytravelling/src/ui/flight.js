import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import APIcon from './icon';

import globalStyles from '../utils/style';
import translate from '../utils/translate';

const Flight = (props) => {
    const monthNames = {
        "en": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        "ru": ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        "hy": ["Հունվ", "Փետ", "Մար", "Ապր", "Մայ", "Հուն", "Հուլ", "Օգոստ", "Սեպտ", "Հոկտ", "Նոյ", "Դեկտ"]
    }
    return (
        <View style={style.container}>
            <View>
                <View style={[style.airport, props.type === "air" ? {justifyContent: "flex-start" } : null]}>
                    {
                        props.type === "air" ?
                            <Text style={[style.airport_name, globalStyles().fontBold]}>{props.data.departure_airport}</Text> :
                            null
                    }
                    <Text style={[style.small_text, globalStyles().fontLight]}>{props.data.departure_date.split('-')[2] + " " + monthNames[props.lang][+props.data.departure_date.split('-')[1] - 1] + " " + props.data.departure_date.split('-')[0]}</Text>
                    <Text style={[globalStyles().text, globalStyles().fontLight, style.departure_time]}>{props.data.departure_time}</Text>

                </View>
                <View style={style.city}>
                    <Text style={[style.city_text, globalStyles().fontBold]}>{props.data.departure_city}</Text>
                </View>
            </View>
            <View style={style.flight_info}>
                {
                    props.type === "air" ?
                        <Text style={[style.small_text, { color: "#7cb342" }, globalStyles().fontLight]}>{props.data && props.data.type ? translate[props.data.type][props.lang] : ""}</Text> :
                        null
                }
                <View style={style.icons}>
                    <APIcon name="remove" color="#707070" size={36} />
                    <APIcon name={props.type === "air" ? "flight" : "directions_bus"} color="#515151" size={32} />
                    <APIcon name="remove" color="#707070" size={36} />
                </View>
                {
                    props.type === "air" ?
                        <Image source={{ uri: props.data.air_company_logo }} style={style.logo} /> :
                        null
                }
            </View>
            <View>
                <View style={[style.airport, props.type === "air" ? {justifyContent: "flex-start" } : null]}>
                    {
                        props.type === "air" ?
                            <Text style={[style.airport_name, globalStyles().fontBold]}>{props.data.arrival_airport}</Text> :
                            null
                    }
                    <Text style={[style.small_text, globalStyles().fontLight]}>{props.data.arrival_date.split('-')[2] + " " + monthNames[props.lang][+props.data.arrival_date.split('-')[1] - 1] + " " + props.data.arrival_date.split('-')[0]}</Text>
                    <Text style={[globalStyles().text, globalStyles().fontLight, style.arrival_time]}>{props.data.arrival_time}</Text>

                </View>
                <View style={style.city}>
                    <Text style={[style.city_text, globalStyles().fontBold]}>{props.data.arrival_city}</Text>
                </View>
            </View>

        </View>
    )
}

const style = StyleSheet.create({
    container: {
        paddingTop: 15,
        paddingBottom: 20,
        flexDirection: "row",
        width: "100%",
        justifyContent: "space-between",
        alignItems: "flex-start"
    },
    airport: {
        width: 80,
        height: 76,
        borderRadius: 5,
        backgroundColor: '#F8F8F8',
        flexDirection: "column",
        justifyContent: "center"
    },
    flight_info: {
        alignItems: "center",
        paddingTop: 5,
        flex: 1
    },
    airport_name: {
        fontSize: 28,
        lineHeight: 38,
        color: "#7cb342",
        letterSpacing: 0.49,
        marginLeft:10
    },
    small_text: {
        fontSize: 8,
        lineHeight: 11,
        color: "#bcbcbc",
        letterSpacing: 0.14,
        marginLeft:11
    },
    icons: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        width: 60,
        height: 60,
        resizeMode: "contain"
    },
    city: {
        paddingTop: 5,
        alignItems: "center",
        width: "100%"
    },
    city_text: {
        fontSize: 10,
        lineHeight: 14,
        color: "#bcbcbc",
        letterSpacing: 0.17,
        textAlign: "center"
    },
    arrival_time: {
        fontSize: 14,
        color: '#515151',
        marginLeft: 11
    },
    departure_time: {
        marginLeft: 11
    }
})

export default Flight;