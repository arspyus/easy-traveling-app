import React from 'react';
import {View, StyleSheet} from 'react-native';
import globalStyles from '../utils/style';

const APCard = (props) => {
    return (
        <View style={[globalStyles().shadow, props.containerStyle]}>
            <View style={[style.container, globalStyles().shadow, props.style]}>
                {props.children}
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        marginTop: 7,
        marginBottom: 14,
        borderRadius: 5,
        width: "90%",
        alignSelf: "center",
        overflow: "hidden" 
    }
})

export default APCard;