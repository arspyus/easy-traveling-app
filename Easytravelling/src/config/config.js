const isDevelopment = false;
const config = {
    API_URL: isDevelopment ? "https://dev.easytraveling.am/wp-admin/admin-ajax.php" : "https://easytraveling.am/wp-admin/admin-ajax.php",
    PERPAGE: 20,
    market_url: {
        'ios': 'https://itunes.apple.com/us/app/easytraveling/id1368470794',
        'android': 'https://play.google.com/store/apps/details?id=am.easytraveling.easytraveling'
    },
    google_places_api_key: "AIzaSyDBN0dmaRZeTml3chn2f8B7tDRX22NtiUU",
    phone_number: "+37496886655"
 };

 
 export default config;
