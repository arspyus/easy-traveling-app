import React from 'react';
import { Animated, Dimensions, StyleSheet, Platform } from 'react-native';

class AuthAnim extends React.Component {
    state = {
        animation: new Animated.Value(- (Dimensions.get("window").width * this.props.tabIndex)),
        containerWidth: Dimensions.get("window").width * 2
    }

    componentDidMount() {
        Dimensions.addEventListener('change', this.changeDimensions);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.changeDimensions)
    }

    changeDimensions = () => {
        this.setState({
            containerWidth: Dimensions.get("window").width * 2
        }, () => {
            let toValue = - (Dimensions.get("window").width * this.props.tabIndex);
            this.startAnimation(toValue, 0)
        });
    }

    startAnimation(toValue, duration) {
        Animated.timing(
            this.state.animation,
            {
                toValue: toValue,                  
                duration: duration,
                useNativeDriver: Platform.OS === "android"
            }
        ).start();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.tabIndex != this.props.tabIndex) {
            let toValue = - (Dimensions.get("window").width * this.props.tabIndex);
            this.startAnimation(toValue, 200)
        }
    }

    render() {
        let { animation } = this.state;

        return (
            <Animated.View
                style={[style.container, {
                    width: this.state.containerWidth,
                    ...this.props.style,
                    transform: [{ translateX: animation }]
                }]}
                ref={(ref) => {this._view = ref}}
            >
                {this.props.children}
            </Animated.View>
        );
    }
}


const style = StyleSheet.create({

    container: {
        alignItems: "flex-start",
        flexDirection: "row",
    }
})

export default AuthAnim;