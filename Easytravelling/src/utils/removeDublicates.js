export default (arr, key) => {
    let result = [], keys = [];
    for(let i = 0; i < arr.length; i++){
        if(keys.indexOf(arr[i][key]) === -1){
            result.push(arr[i]);
            keys.push(arr[i][key])
        }
    }
    return result;
}