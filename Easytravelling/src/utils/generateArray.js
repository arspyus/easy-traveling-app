export default (count, value) => {
    const arr = [];
    for(let i = 0; i < count; i++){
        arr.push(typeof value !== "undefined" ? value : i)
    }
    return arr;
}