import {Dimensions} from 'react-native';

export default () => {
    return Dimensions.get("window").width > Dimensions.get("window").height
}