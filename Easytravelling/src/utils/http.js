const DeviceInfo = require('react-native-device-info');

import { Platform } from 'react-native';

import config from '../config/config';

import store from '../store/configureStore';

import {setMessage, set_loader} from '../store/actions/indexAction';
import { logoutFromApp } from '../store/actions/userAction';
const dataToURl = (d) => {
    let str = "";
    let prefix = "";
    for(let key in d){
        prefix = str ? "&" : "";
        if(d.hasOwnProperty(key)){
            str += prefix + key + "=" + (d[key] && typeof d[key] === "string" ? d[key].trim() : d[key]);
        }
    }
    return str;
}

const http = (data, disableLoader, skipError) => {
    //return sendHttp(data, disableLoader, skipError, null)
    let token = null;
    const currentUser = store.getState().firebase.firebase_user;
    if(currentUser){
        return currentUser.getIdToken()
                .then((token) => {
                    return sendHttp(data, disableLoader, skipError, token );
                }).catch(err => {
                    if(!disableLoader) store.dispatch(set_loader(false));
                    if(!skipError) store.dispatch(setMessage(true, 'something-went-wrong'));
                })
        
    } else {
        return sendHttp(data, disableLoader, skipError, null)
    }
}

const sendHttp = (data, disableLoader, skipError, token ) => {
    let user_id = store.getState().user && store.getState().user.user ? store.getState().user.user.user_id : null;
    let d = {
        device_id: DeviceInfo.getUniqueID(),
        platform: Platform.OS,
        app_version: DeviceInfo.getVersion(),
        'user-type': "app-user",
        ...data
    };
    if(token){
        d.token = token;
    }
    if(user_id){
        d.user_id = user_id;
    }
    let urlString = dataToURl(d);


    let connection = store.getState().index.connection;
        
    if(!connection){
        store.dispatch(setMessage(true, "went-offline"));
        return new Promise((resolve, reject) => resolve());
    }

    if(!disableLoader) store.dispatch(set_loader(true));
    return fetch(config.API_URL, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: urlString
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if(!disableLoader) store.dispatch(set_loader(false));
        if(res.success === 'false' && (res.errorMessage  == 'invalid_token' || res.errorMessage == 'unauthorize_access')){
            store.dispatch(setMessage(true, "expired-session"));
            store.dispatch(logoutFromApp());
        } else {
            return res;
        }
    }).catch((ex) => {
        store.dispatch(set_loader(false));
        if(!skipError) store.dispatch(setMessage(true, 'something-went-wrong'));
    });
}

export default http;