const capitalize = (str) => {
    if(!str) return "";
    let arr = str.split(" ").map((el) => {
        return el.charAt(0).toUpperCase() + el.slice(1);
    })
    return arr.join(" ");
}


export default capitalize;