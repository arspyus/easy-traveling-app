import { StyleSheet, Platform } from "react-native";

export default (objectMode) => {
    const obj = {
        shadow: {
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 2,
            shadowOpacity: 0.4,
            elevation: 2
        },
        fontLight: {
            fontFamily: Platform.OS === "android" ? "NotoSans-Light-font" : "NotoSans-Light",
            //fontFamily: Platform.OS === "android" ? "NotoSans-Regular-font" : "NotoSans-Regular",
            //fontWeight: Platform.OS === "android" ? "200" : "200",
        },
        fontBold: {
            //fontFamily: Platform.OS === "android" ? "NotoSans-Medium-font" : "NotoSans-Medium",
            fontFamily: Platform.OS === "android" ? "NotoSans-SemiBold-font" : "NotoSans-SemiBold",
            fontWeight: "400"
        },
        hidden: {
            width: 0,
            height: 0,
            overflow: "hidden"
        },
        
        section: {
            paddingTop: 15,
            paddingHorizontal: 15,
            paddingBottom: 20,
            borderBottomWidth: 1,
            borderBottomColor: "rgba(188,188,188, .5)",
            backgroundColor: "#fff",
            width: "100%"
        },

        text: {
            color: "#515151",
            fontSize: 14,
            lineHeight: 19,
            letterSpacing: 0.24
        },

        title: {
            color: "#515151",
            fontSize: 16,
            lineHeight: 22,
            letterSpacing: 0.28
        },

        big_title: {
            color: "#515151",
            fontSize: 18,
            lineHeight: 24,
            letterSpacing: 0.31
        },
    };



    return objectMode ? obj : StyleSheet.create(obj)

}
