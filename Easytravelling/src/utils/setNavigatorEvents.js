
import { BackHandler, NetInfo, Platform } from 'react-native';
import { setRootViewBackgroundColor } from 'react-native-root-view-background';
import Orientation from 'react-native-orientation-locker';

import {setNavigation, setPrevPage, setMessage, store_connection, store_connection_changed, set_current_screen, checkExtraData} from '../store/actions/indexAction';

import { clear_unread_notification } from '../store/actions/pushAction';
import navigate from './navigate';

import store from '../store/configureStore';

export default (component) => {
    let backHandler = null;

    const handleBackPress = (event) => {
        if(store.getState().index.isLoading){
            return;
        }
        if(typeof component.handleBackPress !== "undefined" && component.handleBackPress()){
            return;
        }
        if(component.props.testID === "easytraveling.LandingScreen" || component.props.testID === "easytraveling.OffersScreen"){
            BackHandler.exitApp()
        } else if(
            component.props.testID === "easytraveling.PrebookScreen" || 
            component.props.testID === "easytraveling.SelectAgencyScreen" || 
            component.props.testID === "easytraveling.SingleOfferScreen" || 
            component.props.testID === "easytraveling.SingleRequestScreen" ||
            component.props.testID === "easytraveling.AuthScreen" || 
            (component.props.testID === "easytraveling.OfferScreen" && component.comesFromPage !== "easytraveling.AuthScreen") || 
            (component.props.testID === "easytraveling.AgencyScreen" && component.comesFromPage !== "easytraveling.AuthScreen") || 
            (component.props.testID === "easytraveling.MakeRequestScreen" && component.comesFromPage === "easytraveling.AgencyScreen")) {
                navigate(component, null, "pop")
        } else {
            navigate(component, "OffersScreen", "resetTo", null)
            return true;
        }
        
    }

    const connectionHandler = (connection) => {

        if(store.getState().index.connection !== true && connection.type !== "none" && connection.type !== "unknown"){
            store.dispatch(store_connection(true));
            component.willAppear && component.willAppear();
            store.dispatch(checkExtraData())
        }
        if(store.getState().index.connection === false && connection.type !== "none" && connection.type !== "unknown" ){
            store.dispatch(setMessage(true, "went-online" ));
            store.dispatch(store_connection(true));
            store.dispatch(store_connection_changed(true));
            store.dispatch(checkExtraData())
        } else if (connection.type === "none") {
            store.dispatch(setMessage(true, "went-offline" ));
            if(store.getState().index.connection === true){
                store.dispatch(store_connection(false));
                store.dispatch(store_connection_changed(true));
            }
            
        }
    }

    component.props.navigator.setOnNavigatorEvent(
        (event) => {
            if (event.type == 'DeepLink' ) {
                const parts = event.link.split('/'); 
                const payload = event.payload; 
                if (parts[0] && store.getState().index.prevPage === component.props.testID) {
                   navigate(component, parts[0], parts[1], payload);
                   return;
                }
            }
            switch (event.id) {
                case "willAppear":
                    if(Platform.OS === "android"){
                        let rootBackgroundColor = "#FFFFFF";
                        if(component.props.testID === "easytraveling.OffersScreen" || component.props.testID === "easytraveling.AgenciesScreen" || component.props.testID === "easytraveling.MakeRequestScreen"){
                            rootBackgroundColor = "#F2F2F2";
                        }
                        if(component.props.testID !== "easytraveling.SidebarScreen"){
                            setRootViewBackgroundColor(rootBackgroundColor);
                        }
                    }
                    
                    component.willAppear && component.willAppear();
                    NetInfo.addEventListener( 'connectionChange', connectionHandler);
                    store.dispatch(setPrevPage(component.props ? component.props.testID : null));
                    store.dispatch(set_current_screen(component));

                    if(component.props.testID === "easytraveling.MyRequestsScreen" || component.props.testID === "easytraveling.SingleOfferScreen"){
                        store.dispatch(clear_unread_notification("offer"));
                    } else if(component.props.testID === "easytraveling.OffersScreen"){
                        store.dispatch(clear_unread_notification("special-offer"));
                    }
                    
                    if(component.props.testID === "easytraveling.LandingScreen" || component.props.testID === "easytraveling.AuthScreen" ){
                        Orientation.lockToPortrait();
                    } else {
                        Orientation.unlockAllOrientations();
                    }
                    component.props.navigator.setDrawerEnabled({
                        side: "left",
                        enabled: component.props.testID !== "easytraveling.LandingScreen" && component.props.testID !== "easytraveling.AuthScreen"
                    });
                    break;
                case "didAppear":
                    component.screenName && store.dispatch(setNavigation(component.screenName));
                    component.didAppear && component.didAppear();
                    break;
                case "willDisappear":
                    component.willDisappear && component.willDisappear();
                    
                    NetInfo.removeEventListener('connectionChange', connectionHandler);
                    setTimeout(() => {
                        component._scrollView && component._scrollView.scrollTo && component._scrollView.scrollTo({x: 0, y: 0});
                    }, 500)
                    break;
                case "didDisappear":
                    component.didDisappear && component.didDisappear();
                    break;
                case 'backPress':
                    handleBackPress(event)
                break
            }
        }
    );

    
}