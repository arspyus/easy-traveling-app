import store from '../store/configureStore';
import { set_avatar } from '../store/actions/indexAction';

export default (email) => {
    let photoURl = store.getState().user.user && store.getState().user.user.photo_url ? store.getState().user.user.photo_url : null;
    if(photoURl){
        store.dispatch(set_avatar(photoURl));
        return;
    }

    let user_email = email || store.getState().user.user ? store.getState().user.user.email : "";

    if(!user_email){
	    store.dispatch(set_avatar(null));
	    return;
    }
    fetch("https://picasaweb.google.com/data/entry/api/user/" + user_email + "?alt=json", {
        method: "GET"
    }).then(res => res.json()).then(resp => {

        if (resp && resp.entry && resp.entry.gphoto$thumbnail) {
            store.dispatch(set_avatar(resp.entry.gphoto$thumbnail.$t))
        } else {
            store.dispatch(set_avatar(null));
        }
    }).catch(err => {
        store.dispatch(set_avatar(null));
    })
}