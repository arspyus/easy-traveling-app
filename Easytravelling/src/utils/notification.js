import { Platform } from 'react-native';

import firebase from 'react-native-firebase';
import navigate from './navigate';

import store from '../store/configureStore';

import { set_unread_notification } from '../store/actions/pushAction'

let notificationSeen = false;
const notification = async (component) => {
    if (notification.notificationInstalled) return;

        // app is in foreground mode
        notification.notificationListener = firebase.notifications().onNotification((notification) => {
            if (notification && notification.data) {

                firebase.notifications().setBadge(0);

                if (store.getState().index.prevPage === "easytraveling.MyRequestsScreen" && notification.data.action === "offer") {
                    store.getState().index.currentScreen && store.getState().index.currentScreen.willAppear();
                } else if (store.getState().index.prevPage === "easytraveling.OffersScreen" && notification.data.action === "special-offer") {
                    store.dispatch(set_unread_notification(notification.data.action));
                    store.getState().index.currentScreen && store.getState().index.currentScreen.willAppear();
                } else {
                    store.dispatch(set_unread_notification(notification.data.action));
                }
            }
        });

        // app is in background mode
        notification.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            let notification = notificationOpen.notification;
            if (notification && notification.data) {
                firebase.notifications().setBadge(0);
                store.dispatch(set_unread_notification(notification.data.action));

                if (notification.data.action === "special-offer") {
                    if (store.getState().index.prevPage === "easytraveling.OffersScreen") {
                        store.getState().index.currentScreen && store.getState().index.currentScreen.willAppear();
                    } else {
                        navigate(component, "OffersScreen", "resetTo", null)
                    }
                } else if (notification.data.action === "offer") {
                    navigate(component, "SingleOfferScreen", "push", {
                        offer_id: notification.data.offer_id
                    })
                }
            }
        });

        // app is killed
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            counter ++;
            let notification = notificationOpen.notification;
            if (notification && notification.data && !notificationSeen) {
                firebase.notifications().setBadge(0);
                store.dispatch(set_unread_notification(notification.data.action));
                if (notification.data.action == 'offer') {
                    notificationSeen = true;
                    navigate(component, "SingleOfferScreen", "push", {
                        offer_id: notification.data.offer_id
                    })
                }

            }
        }

    notification.notificationInstalled = true;
}

export default notification;