import {AsyncStorage, Platform} from 'react-native';
const RNFS = require('react-native-fs');
import config from '../config/config';
class APImageStore {

    constructor(place_id){
        this.place_id = place_id;
    }

    getThumbnail = () => {
        let promise = new Promise((resolve, reject) => {
            const PATH_TO_LIST = RNFS.DocumentDirectoryPath;
            let thumbnail = null;
            RNFS.readDir(PATH_TO_LIST).then(files => {
                files.forEach(image => {
                    if(image.name === this.place_id + "_0.jpeg"){
                        thumbnail = this.makeFileStoreUrl(image.name);
                        return false;
                    } else {
                        return true
                    }
                });

                if(thumbnail){
                    resolve(thumbnail);
                } else {
                    this.loadThumbnail().then(path => {
                        resolve(path);
                    }).catch(errorMessage => {
                        console.log(errorMessage)
                    })
                }
            })
        });
        
        return promise;
    }

    loadThumbnail = () => {
        const url = 'https://maps.googleapis.com/maps/api/place/details/json?fields=photo' + '&placeid=' + this.place_id + '&key=' + config.google_places_api_key;

        return new Promise((resolve, reject) => {
            fetch(url, {
                method: "GET"
            }).then(res => res.json()).then(resp => {
                const photos = resp.result.photos;
                if(photos && photos[0]){
                    this.getImageFromNetwork(this.makePhotoURL(photos[0].photo_reference) , 0).then((path) => {
                        resolve(path)
                    })
                } else {
                    resolve(null)
                }
                
            }).catch(errorMessage => {
                reject(errorMessage)
            })
        })
        
    }

    makeFileStoreUrl = (image) => {
        return Platform.OS === "android" ? "file:///" + RNFS.DocumentDirectoryPath + "/" + image : RNFS.DocumentDirectoryPath + "/" + image;
    }

    getImages = (onImageAdded) => {
        AsyncStorage.getItem('easytraveling:imagesLoadedOffers').then((offers) => {
            let offersArray = [];
            if(offers){
                offersArray = JSON.parse(offers);
            }
            if(offersArray.indexOf(this.place_id) > -1){
                this.loadImagesFromFileStore(onImageAdded)
            } else {
                this.loadImagesFromServer(onImageAdded)
            }
        }).catch((err) => {
        })

        
    }

    loadImagesFromFileStore = (onImageAdded) => {
        const PATH_TO_LIST = RNFS.DocumentDirectoryPath;
        for(let i = 1; i < 6; i++){
            let path = PATH_TO_LIST + "/" + this.place_id + "_" + i + ".jpeg";
            RNFS.exists(path)
            .then((exist) => {
                if(exist){
                    onImageAdded(this.makeFileStoreUrl(this.place_id + "_" + i + ".jpeg"))
                }
            })
            .catch(() => {  })
        }
    }

    loadImagesFromServer = (onImageAdded) => {
        const url = 'https://maps.googleapis.com/maps/api/place/details/json?fields=photo' + '&placeid=' + this.place_id + '&key=' + config.google_places_api_key;
        
        fetch(url, {
            method: "GET"
        }).then(res => res.json()).then(resp => {
            const photos = resp.result.photos;
            if(!photos) return;
            const photosCount = Math.min(6, photos.length);
            let loadedPhotosCounter = 0;
            for(let i = 1; i < photosCount; i++){
                this.getImageFromNetwork(this.makePhotoURL(resp.result.photos[i].photo_reference) , i).then((path) => {
                    loadedPhotosCounter ++;
                    onImageAdded(path);
                    if(loadedPhotosCounter === photosCount - 1){
                        this.markPlaceAsLoaded();
                    }
                })
            }
        }).catch(errorMessage => {
            console.log(errorMessage)
        })
    }

    getImageFromNetwork(imageURL, i) {
        return new Promise((resolve, reject) => {
            RNFS.downloadFile({
                fromUrl: encodeURI(imageURL),
                toFile: RNFS.DocumentDirectoryPath  + '/' + this.place_id + "_" + i + ".jpeg"
            }).promise
            .then(res => {
                let path = Platform.OS === "android" ? "file:///" + RNFS.DocumentDirectoryPath + '/' + this.place_id + "_" + i + ".jpeg" : RNFS.DocumentDirectoryPath + '/' + this.place_id + "_" + i + ".jpeg";
                resolve(path);
            })
            .catch((errorMessage, statusCode) => {
                reject(errorMessage)
            })
        })
        
    }

    makePhotoURL(photo_referance, size){
        const url = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=1200&photoreference=' + photo_referance + '&key=' + config.google_places_api_key;
        
        return url;
    }

    markPlaceAsLoaded = () => {
        AsyncStorage.getItem('easytraveling:imagesLoadedOffers').then((offers) => {
            let offersArray = [];
            if(offers){
                offersArray = JSON.parse(offers);
            }
            offersArray.push(this.place_id);
            AsyncStorage.setItem('easytraveling:imagesLoadedOffers', JSON.stringify(offersArray));
        }).catch((err) => {
        })
    }

    getMap = () => {
        let promise = new Promise((resolve, reject) => {
            const PATH_TO_LIST = RNFS.DocumentDirectoryPath;
            let map = null;
            RNFS.readDir(PATH_TO_LIST).then(files => {
                files.forEach(image => {
                    if(image.name === this.place_id + "_map.jpeg"){
                        map = this.makeFileStoreUrl(image.name);
                        return false;
                    } else {
                        return true
                    }
                });
                if(map){
                    resolve(map);
                } else {
                    this.loadMap().then(path => {
                        resolve(path);
                    }).catch(errorMessage => {
                        console.log(errorMessage)
                    })
                }
            })

        });
        
        return promise;
    }

    loadMap = () => {
        const url = 'https://maps.googleapis.com/maps/api/place/details/json?fields=geometry' + '&placeid=' + this.place_id + '&key=' + config.google_places_api_key;

        return new Promise((resolve, reject) => {
            fetch(url, {
                method: "GET"
            }).then(res => res.json()).then(resp => {
                const geometry = resp.result.geometry;
                if(geometry && geometry.location){
                    const coordinates = geometry.location.lat + "," + geometry.location.lng;
                    const url = "https://maps.googleapis.com/maps/api/staticmap?center=" + coordinates + "&zoom=16&size=600x300&maptype=roadmap&key=" + config.google_places_api_key + "&markers=|" + coordinates;
                    this.getImageFromNetwork(url , "map").then((path) => {
                        resolve(path)
                    })
                } else {
                    resolve(null)
                }
                
            }).catch(errorMessage => {
                reject(errorMessage)
            })
        })
        
    }
}

export default APImageStore;