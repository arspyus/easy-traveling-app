const throttle = (fn) => {
    if(!throttle.lastCall)
        throttle.lastCall = 0;
    return (...args) => {
        const now = (new Date).getTime();
        if (now - throttle.lastCall < 1000) {
            return;
        }

        throttle.lastCall = now;
        return fn(...args);
    }
}

export default throttle;