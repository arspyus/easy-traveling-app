import store from '../store/configureStore';

import {setMessage} from '../store/actions/indexAction';

const navigate = (component, screen, action, props, noAnimation) => {
    let connection = store.getState().index.connection;
    if(!connection && screen !== "AboutScreen" && screen !== "AuthScreen" ){
        store.dispatch(setMessage(true, "went-offline"));
        return;
    }
    let screenBackgroundColor = "#FFFFFF";
    let statusBarColor = "#000000"
    if(screen === "OffersScreen" || screen === "AgenciesScreen" || screen === "MakeRequestScreen"){
        screenBackgroundColor = "#F2F2F2"
    }
    if(screen !== "LandingScreen"){
        statusBarColor = "#8bc34a"
    }


    if(action === "resetTo"){
        component.props.navigator.resetTo({
            screen: 'easytraveling.' + screen,
            navigatorStyle: {
                navBarHidden: true,
                disabledBackGesture: true,
                screenBackgroundColor: screenBackgroundColor,
                statusBarColor: statusBarColor,
                statusBarHidden: screen === "LandingScreen"
            },
            animated: true,
            animationType: noAnimation ? 'none' : 'slide-horizontal',
            overrideBackPress: screen !== "LandingScreen" && screen !== "OffersScreen",
            passProps: props
        })
    } else if (action === "push"){
        component.props.navigator.push({
            screen: 'easytraveling.' + screen,
            navigatorStyle: {
                navBarHidden: true,
                disabledBackGesture: true,
                screenBackgroundColor: screenBackgroundColor,
                statusBarColor: statusBarColor,
                statusBarHidden: screen === "LandingScreen"
            },
            animated: true,
            animationType: noAnimation ? 'none' : 'slide-horizontal',
            overrideBackPress: screen !== "LandingScreen" && screen !== "OffersScreen",
            passProps: props
        })
    } else if (action === "pop"){
        component.props.navigator.pop({
            animated: true,
            animationType: noAnimation ? 'none' : 'slide-horizontal'
        })
    }
}

export default navigate;