const DeviceInfo = require('react-native-device-info');

import { Platform } from 'react-native';

import config from '../config/config';

import store from '../store/configureStore';

import {setMessage, set_loader} from '../store/actions/indexAction';

const upload = (data) => {
    let token = null;
    const currentUser = store.getState().firebase.firebase_user;
    if(currentUser){
        return currentUser.getIdToken()
                .then((token) => {
                    return sendHttp(data, token);
                }).catch(err => {
                    if(!disableLoader) store.dispatch(set_loader(false));
                    if(!skipError) store.dispatch(setMessage(true, 'something-went-wrong'));
                })
        
    } else {
        return sendHttp(data, null)
    }
}

const sendHttp = (data, token) => {
    let user_id = store.getState().user && store.getState().user.user ? store.getState().user.user.user_id : null;
    let d = {
        device_id: DeviceInfo.getUniqueID(),
        platform: Platform.OS,
        app_version: "2.0.0",
        'user-type': "app-user",
        ...data
    };
    if(token){
        d.token = token;
    }
    if(user_id){
        d.user_id = user_id;
    }

    const formData  = new FormData();
    for(let name in d) {
        if(name === "file"){
            formData.append(name, URLtoFile(d[name]));
        } else {
            formData.append(name, d[name]);
        }
    }

    return fetch(config.API_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        body: formData
    }).then(res=>res.json());
}



const URLtoFile = (dataurl) => {
    return {
        uri: dataurl,
        name: "photo",
        type: 'image/png'
    };
}

export default upload;