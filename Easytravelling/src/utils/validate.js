import countries from './countries';

const validate = (rules, value) => {
    let isValid = true;
    for(let i = 0; i < rules.length; i++){
        if(!isValid) break;
        if(typeof value === "string") value = value.trim();
        switch(rules[i].type){
            case "email": 
                isValid = isEmail(value);
                break;
            case "password":
                isValid = isPassword(value);
                break;
            case "phone":
                isValid = isPhone(value);
                break;
            case "date":
                isValid = isDate(value);
                break;
            case "name":
                isValid = isName(value);
                break;
            case "required":
                isValid = isRequired(value);
                break;
            case "number":
                isValid = isNumber(value);
                break;
            case "country":
                isValid = isCountry(value);
                break;
            case "compareDates":
                isValid = compareDates(value, rules[i]['compareDate'])
                break;
            default:
                break;
        }
    }
    return isValid;
}

const isEmail = value => {
    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(value);
}

const isPassword = value => {
    const pattern = /^(?=.*[A-Za-z])(?=.*[0-9])(?=.{6,})/;
    return pattern.test(value);
}

const isPhone = value => {
    const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return pattern.test(value) || !value;
}

const isDate = value => {
    const pattern = /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
    return value && pattern.test(value);
}

const isName = value => {
    return value.length > 1;
}

const isRequired = value => {
    return value.length > 0 ;
}

const isNumber = value => {
    const pattern = /^\+?(0|[1-9]\d*)$/;
    return pattern.test(value) && Number(value) > 0;
}

const isCountry = value => {
    let countriesList = countries.map(el => el.text.toLowerCase());
    return value && countriesList.indexOf(value.toLowerCase()) > -1
}

const compareDates = (date1, date2) => {
    const newDate1 = new Date(date1);
    const newDate2 = new Date(date2);
    return (date1 >= date2);
}

export default validate;