export default (elem) => {
    if(!elem) return false;
    return elem.layoutMeasurement.height + elem.contentOffset.y >= elem.contentSize.height - 10;
}