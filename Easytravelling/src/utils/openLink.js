import { Linking } from 'react-native';


export default (url) => {
    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
        } else {
            return Linking.openURL(url)
        }
    }).catch(err => { })
}