export default {
    languages : ['English', 'Русский', 'Հայերեն'],
    languagesMap : {
        'en': 'English',
        'ru': 'Русский',
        'hy': 'Հայերեն'
    },
    languagesObj : [
        {value: "en", text: "English"},
        {value: "ru", text: "Русский"},
        {value: "hy", text: "Հայերեն"}
    ]
}