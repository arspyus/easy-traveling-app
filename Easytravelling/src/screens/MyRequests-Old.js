import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableWithoutFeedback, RefreshControl, Platform } from 'react-native';
import { connect } from 'react-redux';

import { get_my_requests, reset_my_requests, delete_request, setRequestFilter } from '../store/actions/myRequest';
import { select_type } from '../store/actions/makeRequestAction';
import { show_confirmation_popup } from '../store/actions/indexAction';

import APDropdown from '../ui/dropdown';
import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APTabs from '../ui/tabs';
import APCard from '../ui/card';
import APButton from '../ui/button';
import APNothingFound from '../ui/nothingFound';

import SingleRequest from '../components/singleRequest';
import APAlertPopup from '../popups/alertPopup';

import translate from '../utils/translate';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents'; 
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';

class MyRequestsScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this)
    }

    screenName = "myrequests";

    willAppear() {
        this.getMyRequests();
    }

    willDisappear() {
        this.props.resetMyRequests();
    }

    singleRequestArray = [];

    getMyRequests(filter, reset) {
        filter = filter || this.props.filter;
        let from = "";
        let to = "";
        let status = "all";
        let currentDate = new Date();
        let calculatedDate = new Date();

        if(filter == 'last_week'){
            calculatedDate.setDate(currentDate.getDate() - 7);
        }else if(filter == 'last_month'){
            calculatedDate.setDate(currentDate.getDate() - 30);
        }else if(filter == 'last_six'){
            calculatedDate.setDate(currentDate.getDate() - 180);
        }else if(filter == 'last_year'){
            calculatedDate.setDate(currentDate.getDate() - 365);
        } else if (filter === "archive") {
            status = "archive";
        }

        if(filter !== "all" && filter !== "archive"){
            to = currentDate.getFullYear() + '-' + ((currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : currentDate.getMonth() + 1) + '-' + (currentDate.getDate()  < 10 ? '0' + currentDate.getDate() : currentDate.getDate() );
            from = calculatedDate.getFullYear() + '-' + ((calculatedDate.getMonth() + 1) < 10 ? '0' + (calculatedDate.getMonth() + 1) : calculatedDate.getMonth() + 1) + '-' + (calculatedDate.getDate()  < 10 ? '0' + calculatedDate.getDate() : calculatedDate.getDate());
        }
        if(!reset) this.props.resetMyRequests();

        this.props.getMyRequests(status, from, to, reset)
    }

    state = {
        tabItems: [translate["active"][this.props.lang], translate["booked"][this.props.lang]],
        dropdownItems: [
            { text: translate["last-week"][this.props.lang], value: "last_week" },
            { text: translate["last-month"][this.props.lang], value: "last_month" },
            { text: translate["last-six-months"][this.props.lang], value: "last_six" },
            { text: translate["last-year"][this.props.lang], value: "last_year" },
            { text: translate["all"][this.props.lang], value: "all" },
            { text: translate["archive"][this.props.lang], value: "archive" }
        ],
        selectedTab: 0,
        showAlert: false,
        deletingElementIndex: null
    }

    dropdown = null;

    showDropdown = () => {
        this.dropdown.show()
    }

    onTabChange = selectedTab => {
        this.setState({
            ...this.state,
            selectedTab: selectedTab
        });
    };

    navigateTo = (id, page) => {
        let props = null;
        let screen = null;
        if (page === "offer") {
            screen = 'SingleOfferScreen';
            props = { offer_id: id }
        } else if (page === "request") {
            screen = 'SingleRequestScreen';
            props = { request_id: id }
        }
        navigate(this, screen, "push", props);
    }

    onDelete = (id) => {
        this.setState({
            deletingElementIndex: id,
            showAlert: true
        })
    }

    deleteRequest = () => {
        this.props.deleteRequest(this.state.deletingElementIndex, "", "");
    }

    hidePopup = () => {
        this.setState({
            deletingElementIndex: null,
            showAlert: false
        })
    }

    onFilter = (index, value) => {
        this.props.setFilter(value);
        this.getMyRequests(value)
    }

    onRefresh = () => {
        this.getMyRequests(null, true);
    }

    _renderRow = (rowData, rowID, highlighted) => {
        return (
            <View style={[style.dropdownStyle, highlighted ? { backgroundColor: "#F1F1F1" } : null, rowID == this.state.dropdownItems.length - 1 ? { borderTopColor: "#e0e0e0", borderTopWidth: 1 } : null]}>
                <Text style={[style.rowText, globalStyles().fontLight, rowID == this.state.dropdownItems.length - 1 ? { color: "#EF5350" } : null]}>{rowData}</Text>
            </View>
        )
    }

    navigateToMakeRequest = (type) => {
        if(this.props.user && this.props.user.user_status === "unconfirmed"){
            this.props.showConfirmationPopup();
            return;
        }

        this.props.selectType(type)
        navigate(this, "MakeRequestScreen", "resetTo", {
            type: type
        });
    }

    render() {

        let requestsList = null;
        let tabs = null;
        let filtered = [];
        this.singleRequestArray = [];
        if (this.props.requests) {
            filtered = this.props.requests.filter(req => {
                return this.state.selectedTab === 0 && req.status !== "booked" || this.state.selectedTab === 1 && req.status === "booked" || this.props.filter === "archive"
            })
            if (filtered.length > 0) {
                requestsList = filtered.map((request, ind) => {
                    return  <SingleRequest
                                ref={(ref) => ref ? this.singleRequestArray[ind] = ref._collapsible: false}
                                request={request}
                                lang={this.props.lang}
                                key={request.request_id + "_" + ind}
                                navigateTo={this.navigateTo}
                                onDelete={this.onDelete}
                                booked={this.state.selectedTab === 1}
                                singleRequestArray = {this.singleRequestArray}
                                isArchive={this.props.filter === "archive"}
                            />
                    
                })
            }
        }

        let dropdown = <View style={style.dropdown}>
                            <APDropdown
                                options={this.state.dropdownItems}
                                defaultValue={this.props.filter}
                                defaultIndex={this.state.dropdownItems.map(el => el.value).indexOf(this.props.filter)}
                                onSelect={this.onFilter}
                                textStyle={{ fontSize: 0 }}
                                renderRow={this._renderRow}
                                ref={(dropdown) => { this.dropdown = dropdown; }}
                            ><View style={globalStyles().hidden}></View></APDropdown>
                        </View>

        return (
            <APContainer 
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })} 
                title={translate["my-requests"][this.props.lang]}
                filter={true}
                filterContent={dropdown}
                onFilter={throttle(() => this.showDropdown())}
                plus={true}
                onNavigate={this.navigateToMakeRequest}
                screenId={this.props.testID}
                screenComponent={this}
                >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    alwaysBounceVertical={Platform.OS !== "android"}
                    bounces={ Platform.OS !== "android" }
                    ref= {(ref) => {this._scrollView = ref}} 
                    horizontal={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.reset_loader}
                            onRefresh={this.onRefresh}
                            colors={["#7cb342"]}
                            tintColor="#7cb342"
                        />
                      }
                >
                    <View style={style.container}>
                        <View style={style.tabsWrapper}>
                            {this.props.filter !== "archive" ?
                                    <APTabs
                                        initialIndex={this.state.selectedTab}
                                        items={this.state.tabItems}
                                        onChange={this.onTabChange}
                                        textStyle={{ color: "#000", ...globalStyles(true).fontLight }}
                                    /> :
                                <View>
                                    <APTabs
                                        initialIndex={0}
                                        items={[translate["archive"][this.props.lang]]}
                                        onChange={() => false}
                                        textStyle={{ color: "#000", ...globalStyles(true).fontLight }}
                                        lineStyle={{backgroundColor: "#EF5350", width: "100%"}}
                                    /> 
                                </View> 
                            }
                        </View>
                        {this.props.requests && filtered.length > 0 ? (<View style={{width: "100%"}}>
                            <View style={style.table}>
                                <APCard>
                                    {requestsList}
                                </APCard>
                            </View>
                            {this.props.filter !== "archive" && this.state.selectedTab === 0 ?
                                ( <View style={style.buttonContainer}>
                                    <APButton text={translate["cancel-all"][this.props.lang]} color="#fff" backgroundColor="#EF5350" icon="delete_forever" onPress={throttle(() => this.onDelete("all"))} />
                                </View> ) : null }
                        </View>) 
                        : this.props.requests ? 
                            <APNothingFound text={translate[this.state.selectedTab === 0 ? "no-requests-yet" : "not-booked-yet"][this.props.lang]} /> 
                            : null
                        }
                    </View>
                    <APAlertPopup
                        isVisible={this.state.showAlert} 
                        title={translate["cancel-question"][this.props.lang]}
                        text={translate["request-cancel-question"][this.props.lang]}
                        onSubmit={throttle(this.deleteRequest)}
                        hidePopup={this.hidePopup}
                        cancelable={false}
                    />
                </ScrollView>
            </APContainer>

        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        paddingBottom: 38,
        paddingTop: 24,
        alignItems: "center",
    },
    tabsWrapper: {
        width: "90%"
    },
    table: {
        marginTop: 14,
        width: "100%"
    },
    buttonContainer: {
        marginVertical: 22,
        flexDirection: "row",
        justifyContent: "flex-start",
        paddingLeft: "5%"
    },

    dropdown: {
        top: 0,
        position: "absolute",
        right: 0
    },

    dropdownStyle: {
        backgroundColor: "#fff",
        paddingVertical: 14,
        paddingHorizontal: 16
    },

    rowText: {
        fontSize: 16,
        color: 'black',
        lineHeight: 22
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        requests: state.myRequest.requests,
        filter: state.myRequest.filter,
        reset_loader: state.myRequest.reset_loader
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMyRequests: (status, from, to, reset) => { dispatch(get_my_requests(status, from, to, reset)) },
        resetMyRequests: () => { dispatch(reset_my_requests()) },
        deleteRequest: (id, from, to) => { dispatch(delete_request(id, from, to)) },
        setFilter: (filter) => { dispatch(setRequestFilter(filter)) },
        selectType: (type) => {dispatch(select_type(type))},
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "activate"))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyRequestsScreen)
