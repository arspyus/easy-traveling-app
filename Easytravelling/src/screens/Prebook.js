import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, Keyboard, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APTextField from '../ui/textField';
import APIcon from '../ui/icon';
import APFloatingButton from '../ui/floatingButton';
import APKeyboardAvoidingView from '../ui/keyboardAvoidingView';
import APSection from '../ui/section';
import APSwitch from '../ui/switch';
import APInput from '../ui/input';
import APAutocomplete from '../ui/autocomplete';

import translate from '../utils/translate';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';
import countriesList from '../utils/countries';

const countries = countriesList.map(country => {
    return country.value === "armenia" ? {...country, text: "Armenia"} : country;
})
const ImagePicker = require('react-native-image-picker');

import { setMessage } from '../store/actions/indexAction';
import { book } from '../store/actions/bookingAction';
class PrebookScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
        this.state = {
            form: {
                people: this.makeEmptyArray(this.props.adults + this.props.children + this.props.infant)
            },
            _scrollView: null,
            showAutocomplete: false,
            autocompleteData: [],
            autocompleteInputIndex: null,
            isScrollExist: false,
            showButton: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.isBooking && nextProps.isBooking) {
            navigate(this, "OffersScreen", "resetTo", null);
        }
    }

    onScrollViewLayout = (contentwidth, contentHeight) => {
        this.setState({
            isScrollExist: contentHeight > Dimensions.get('window').height
        })
    }

    _inputElements = [];

    makeEmptyArray(count) {
        if (count === 0) return [];
        return Array(count).join(".").split(".").map((el, index) => {
            return {
                first: {
                    isValid: true,
                    value: ""
                },
                last: {
                    isValid: true,
                    value: ""
                },
                picture: "",
                type: index < this.props.adults ? "adult" : "child",
                "visa-support": {
                    isValid: true,
                    value: false
                },
                nationality: {
                    isValid: true,
                    value: ""
                }
            }
        });
    }

    onBack = () => {
        navigate(this, null, "pop");
    }

    onChange = (text, field, index) => {
        this.setState({
            ...this.state,
            form: {
                people: this.state.form.people.map((el, ind) => {
                    return ind === index ? { ...el, [field]: { ...el[field], value: text } } : el;
                })
            }
        })
    }

    onScroll = (event) => {
        const vH = Dimensions.get("window").height;
        const sH = event.nativeEvent.contentSize.height * 4/5;
        const diff = sH - vH;
        const offset = event.nativeEvent.contentOffset.y;
        if(offset > diff && !this.state.showButton || offset <= diff && this.state.showButton){
            this.setState({
                showButton: !this.state.showButton
            })
        }
    }

    openCamera = (index) => {
        const options = {
            quality: 0.75,
            maxWidth: 800,
            maxHeight: 800,
            mediaType: "photo",
            chooseFromLibraryButtonTitle: translate["gallery"][this.props.lang],
            takePhotoButtonTitle: translate["camera"][this.props.lang],
            cancelButtonTitle: translate["close"][this.props.lang].toUpperCase(),
            title: translate["camera-popup-msg"][this.props.lang]
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (!response.didCancel && !response.error) {
                this.setPicture(index, response.uri)
            }
        });
    }

    setPicture = (index, uri) => {
        this.setState({
            ...this.state,
            form: {
                people: this.state.form.people.map((el, ind) => {
                    return ind === index ? { ...el, picture: uri } : el;
                })
            }
        })
    }

    photoCancel = (index) => {
        this.setPicture(index, "")
    }

    book = () => {
        Keyboard.dismiss();
        this.updateValidAtionState(() => {
            if (this.isFormValid()) {
                let obj = {};
                for (let i = 0; i < this.state.form.people.length; i++) {
                    let field = i < this.props.adults ? "adults" : i < this.props.adults + this.props.children ? "children" : "infant"
                    obj[field + "_" + (i + 1)] = {
                        "photo": this.state.form.people[i].picture,
                        "firstname": this.state.form.people[i].first.value,
                        "lastname": this.state.form.people[i].last.value,
                        "visa-support": this.state.form.people[i]['visa-support'].value,
                        "nationality": this.state.form.people[i].nationality.value || ""
                    }
                }
                const data = {
                    offer_id: this.props.offer_id,
                    request_id: this.props.request_id,
                    offer_author: this.props.offer_author,
                    form: obj
                };
                this.props.startBooking(data);
            } else {
                this.props.showError("fill-form")
            }
        });
    }

    updateValidAtionState = (cb) => {
        let people = [...this.state.form.people ];
        for (let i = 0; i < people.length; i++) {
            people[i].first.isValid = people[i].first.value.trim() != "" || people[i].picture != "";
            people[i].last.isValid = people[i].last.value.trim() != "" || people[i].picture != "";
        }
        this.setState({
            ...this.state,
            form: {
                people: people
            }
        }, cb)
    }

    isFormValid = () => {
        let isValid = true;
        for (let i = 0; i < this.state.form.people.length; i++) {
            if (!this.state.form.people[i].first.isValid || !this.state.form.people[i].last.isValid) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    closeAutocomplete = () => {
        this.setState({
            showAutocomplete: false
        })
    }

    onAutocompleteChange = (text) => {
        this.getCountries(text);
    }

    getCountries = (text) => {
        text = text.trim().toLowerCase();
        let data = [];
        data = countries.filter(country => {
            return country.text.toLowerCase().indexOf(text) > -1 ;
        }).map(country => {
            return {
                icon: "language",
                title: country.text,
                description: "",
                value: country.text
            }
        });

        this.setState({
            autocompleteData: data
        })
    }

    onAutocompleteSelect = (value) => {
        Keyboard.dismiss();
        if(this.state.autocompleteInputIndex === null) return;

        this.setState({
            ...this.state,
            form: {
                people: this.state.form.people.map((el, ind) => {
                    return ind === this.state.autocompleteInputIndex ? { ...el, ['nationality']: { ...el['nationality'], value: value } } : el;
                })
            },
            showAutocomplete: false
        })
    }

    openAutocomplete = (index) => {
        this.setState({
            showAutocomplete: true,
            autocompleteInputIndex: index,
            autocompleteData: []
        })
    }

    render() {
        let people = null;
        this._inputElements = [];
        if (this.state.form.people && this.state.form.people.length > 0) {
            people = this.state.form.people.map((person, index) => {
                return (
                    <APSection key={index} style={style.section}>
                        <View style={globalStyles().section}>
                            <View style={[style.row, { justifyContent: "space-between", alignItems: "flex-start" }]}>
                                <Text style={[globalStyles().title, globalStyles().fontBold]}>{translate['traveler'][this.props.lang]}</Text>
                                <Text style={[globalStyles().text, globalStyles().fontLight, style.green]}>{translate[person.type][this.props.lang]}</Text>
                            </View>
                            <View style={style.form}>
                                <View style={style.input}>
                                    <APTextField
                                        placeholder={translate["first-name"][this.props.lang]}
                                        value={person.first.value}
                                        error={!person.first.isValid}
                                        ref={(ref) => ref && this._inputElements.push(ref)}
                                        onSubmitEditing={()=>{this._inputElements[2*index + 1] ? this._inputElements[2*index + 1].focus() : null}}
                                        onChangeText={(text) => { this.onChange(text, "first", index) }}
                                        _scrollView={this.state._scrollView}
                                        placeholderTextColor="#bcbcbc"
                                        returnKeyType="next"
                                    />
                                </View>
                                <View style={style.input}>
                                    <APTextField
                                        placeholder={translate["last-name"][this.props.lang]}
                                        value={person.last.value}
                                        error={!person.last.isValid}
                                        ref={(ref) => ref && this._inputElements.push(ref)}
                                        onSubmitEditing={()=>{index === this.state.form.people.length - 1 ? this.book() : this._inputElements[2*index + 2] ? this._inputElements[2*index + 2].focus() : null}}
                                        onChangeText={(text) => { this.onChange(text, "last", index) }}
                                        _scrollView={this.state._scrollView}
                                        placeholderTextColor="#bcbcbc"
                                        returnKeyType={index === this.state.form.people.length - 1 ? "go" : "next"}
                                    />
                                </View>
                            </View>
                            <View style={[style.row, { justifyContent: "space-between", alignItems: "flex-start" }]}>
                                <Text style={[globalStyles().title, globalStyles().fontBold, style.grey]}>{translate['or'][this.props.lang].toUpperCase()}</Text>
                                {
                                    !person.picture ?
                                        <TouchableWithoutFeedback onPress={() => this.openCamera(index)}>
                                            <View style={style.row}>
                                                <APIcon name="note_add" size={18} color="#7cb342" />
                                                <Text style={[globalStyles().text, globalStyles().fontLight, style.green]}>{translate['upload_passport'][this.props.lang]}</Text>
                                            </View>
                                        </TouchableWithoutFeedback> :
                                        <TouchableWithoutFeedback onPress={() => this.photoCancel(index)}>
                                            <View style={style.row}>
                                                <APIcon name="delete_forever" size={18} color="#f44336" />
                                                <Text style={[globalStyles().text, globalStyles().fontLight, { color: "#f44336" }]}>{translate['delete_passport'][this.props.lang]}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                }
                            </View>
                        </View>
                        {
                            this.props.country.toLowerCase() !== "armenia" ?
                                <View style={globalStyles().section}>
                                    <View>
                                        <APSwitch
                                            label={translate["visa-support"][this.props.lang]}
                                            onValueChange={(text) => { this.onChange(text, "visa-support", index) }}
                                            value={person["visa-support"].value}
                                        />
                                    </View>
                                    {
                                        person["visa-support"].value ?
                                            <TouchableWithoutFeedback onPress={() => this.openAutocomplete(index)}>
                                                <View style={{ marginTop: 5 }}>
                                                    <APInput
                                                        label={translate["nationality-form"][this.props.lang]}
                                                        isValid={person.nationality.isValid}
                                                        value={person.nationality.value}
                                                        onChangeText={(text) => { this.onChange(text, "nationality", index) }}
                                                        labelColor="#bcbcbc"
                                                        inputMargin={-25}
                                                        _scrollView={this.state._scrollView}
                                                        returnKeyType="go"
                                                        editable={false}
                                                    />
                                                </View>
                                            </TouchableWithoutFeedback> : null
                                    }

                                </View> :
                                null
                        }
                    </APSection>
                )
            })
        }
        return (
            <APContainer
                back={true}
                onBack={this.onBack}
                title={translate["pre-book-long"][this.props.lang]}
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })}
                screenId={this.props.testID}
                reverse={true}
                noMenu={true}
                screenComponent={this}
            >
                <APKeyboardAvoidingView>
                    <ScrollView
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}
                        alwaysBounceVertical={false}
                        bounces={false}
                        ref={(ref) => { !this.state._scrollView && this.setState({ ...this.state, _scrollView: ref }) }}
                        onScroll={this.onScroll}
                        onContentSizeChange={this.onScrollViewLayout}
                    >
                        <APWrapper>
                            <View>
                                {people}
                            </View>
                            <View>
                                <View style={style.notes}>
                                    <Text style={[style.notesText, globalStyles().fontLight, style.grey]}>{translate["book-notes"][this.props.lang]}</Text>
                                    <Text style={[style.notesText, globalStyles().fontLight, style.grey]}>{translate["pre-book-notes"][this.props.lang]}</Text>
                                </View>

                            </View>
                        </APWrapper>
                        <APAutocomplete
                            isVisible={this.state.showAutocomplete}
                            onClose={this.closeAutocomplete}
                            placeholder={translate["search"][this.props.lang] + " " + translate["country"][this.props.lang].toLowerCase() }
                            data={this.state.autocompleteData}
                            onChange={this.onAutocompleteChange}
                            onSelect={this.onAutocompleteSelect}
                            restrictWithData={true}
                            google_logo={false}
                        />
                    </ScrollView>
                </APKeyboardAvoidingView>

                <APFloatingButton
                    description={this.props.city ? this.props.city : this.props.country ? this.props.country : ""}
                    title={this.props.price + " " + translate['amd'][this.props.lang]}
                    buttonText={translate["pre-book"][this.props.lang]}
                    isVisible={this.state.showButton}
                    onClick={throttle(this.book)}
                    containerStyle={!this.state.isScrollExist ? { position: "relative" } : null}
                    static={!this.state.isScrollExist}
                />
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    section: {
        marginBottom: 20
    },
    form: {
        marginTop: 10,
        marginBottom: 16
    },

    row: {
        flexDirection: "row"
    },

    input: {
        marginTop: 10
    },

    grey: {
        color: "#bcbcbc"
    },
    green: {
        color: "#7cb342"
    },

    notes: {
        paddingHorizontal: 25,
        paddingBottom: 60
    },

    notesText: {
        lineHeight: 24,
        fontSize: 12,
        letterSpacing: 0.21
    },

});

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        isBooking: state.booking.isBooking
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        showError: (message) => { dispatch(setMessage(true, message)) },
        startBooking: (data) => { dispatch(book(data)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrebookScreen)
