import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';
import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';

import translate from '../utils/translate';
import languages from '../utils/languages';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import globalStyles from '../utils/style';

class AboutScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    screenName = "about";

    render() {
        let str = translate["about-content"][this.props.lang];
        let strArr = str.split("<<<EasyTraveling>>>");
        strArr = strArr.map(el => {
            return <Text>{el}</Text>
        });

        let strArrFull = [];
        for (let i = 0; i < strArr.length; i++) {
            strArrFull.push(strArr[i]);
            if (i != strArr.length - 1) strArrFull.push(<Text style={[globalStyles().fontBold]}>EasyTraveling</Text>)
        }

        let result = strArrFull.map((el, index) => {
            return <Text key={index}>{el}</Text>;
        })

        return (
            <APContainer 
                onMenu={() =>this.props.navigator.toggleDrawer({ side: 'left',  animated: true})} 
                title={translate["about"][this.props.lang]} 
                screenId={this.props.testID}
                showOffline={true}
                screenComponent={this}
            >
                <ScrollView 
                    keyboardShouldPersistTaps="always" 
                    showsVerticalScrollIndicator={false} 
                    style={{backgroundColor: "#fff"}}
                    alwaysBounceVertical={false}
                    bounces={false}
                >
                    <APWrapper>
                        <View style={style.mainContainer}>
                            <View style={style.container}>
                                <Text style={[style.text, globalStyles().fontLight]}>
                                    {result}
                                </Text>
                            </View>
                        </View>
                    </APWrapper>
                </ScrollView>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    mainContainer: {
        width: "100%",
        alignItems: "center",
        height: "100%",
        flex: 1,
        backgroundColor: "#fff"
    },

    container: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        backgroundColor: "#fff",
        marginBottom: 12
    },

    text: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 21
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

export default connect(mapStateToProps, null)(AboutScreen)
