import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, Image, TouchableWithoutFeedback, Animated, Dimensions, Platform, FlatList } from 'react-native';
import { connect } from 'react-redux';

import { rate } from '../store/actions/agencyAction';
import APContainer from '../ui/container';
import APIcon from '../ui/icon';
import APTabs from '../ui/tabs';
import APStars from '../ui/stars';
import APSpinner from '../ui/spinner';
import APNothingFound from '../ui/nothingFound';
import APBackButton from '../ui/backButton';

import Header from '../ui/header';
import Rate from '../components/rate';
import SingleOffer from '../components/singleOffer';

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import capitalize from '../utils/capitalize';
import openLink from '../utils/openLink';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import isReachedToBottom from '../utils/isReachedToBottom';

import { get_special_offers, reset_special_offers } from '../store/actions/offerAction';
import { get_agency, reset_agency } from '../store/actions/agencyAction';
import { show_confirmation_popup } from '../store/actions/indexAction';

const bgImage = require('../../assets/images/agency_bg.png');

const HEADER_MAX_HEIGHT = 200;

class AgencyScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
        this.state = {
            scrollY: new Animated.Value(0),
            items: [translate["details"][this.props.lang], translate["special-offers-agency"][this.props.lang]],
            selectedTab: 0,
            winWidth: Dimensions.get("window").width,
            logoVisible: true,
            agency: this.getAgencyFromProps()
        }
    }

    willAppear(){
        this.props.resetSpecialOffers();
        this.props.resetAgency();
        this.props.getSpecialOffers(this.state.agency.id, this.state.agency.tour_agent);
        this.comesFromPage = this.props.prevPage;
        
        // came from single offer page, where we don't have is_rated, need to get agency
        if(typeof this.props.is_rated === "undefined") {
            this.props.getAgency(this.props.id)
        }

    }

    getAgencyFromProps = () => {
        let agency = {
            country: this.props.country,
            city: this.props.city,
            tour_agent: this.props.tour_agent,
            phone: this.props.phone,
            email: this.props.email,
            website: this.props.website,
            photo: this.props.photo,
            is_rated: this.props.is_rated,
            id: this.props.id,
            coordinates: this.props.coordinates,
            average_rate: this.props.average_rate,
            badges: this.props.badges,
            address: this.props.address
        } 
        return agency
    }

    componentDidMount() {
        Dimensions.addEventListener('change', this.changeDimensions);
    }
    
    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.changeDimensions)
    }

    changeDimensions = () => {
        this.setState({
            winWidth: Dimensions.get("window").width
        });
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(!prevProps.agency && this.props.agency){
            this.setState({
                agency: {... this.props.agency}
            })
        }
    }

    openLink = (action) => {
        let url = "";
        if (action === "phone") {
            url = "tel:" + this.state.agency.phone
        } else if (action === "email") {
            url = "mailto:" + this.state.agency.email
        } else if (action === "website") {
            url = this.state.agency.website
        }
        openLink(url);
    }

    onBack = () => {
        if(this.comesFromPage !== "easytraveling.AuthScreen"){
            navigate(this, null, "pop");
        } else {
            navigate(this, "OffersScreen", 'resetTo', {})
        }
        
    }

    onTabChange = selectedTab => {
        this.setState({
            ...this.state,
            selectedTab: selectedTab
        });
    };

    onScroll = ($event) => {
        Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }], { listener: this._handleScroll })($event);
        if (isReachedToBottom($event.nativeEvent) && !this.props.offers_loading && this.state.selectedTab === 1 && this.props.offers && this.props.offers.length) {
            this.props.getSpecialOffers(this.state.agency.id, this.state.agency.tour_agent);
        }
    }

    onOfferClick = (offer) => {
        navigate(this, "OfferScreen", "push", {
            offer: offer,
            lang: this.props.lang
        });
    }

    openRoute = () => {
        if(!this.state.agency.coordinates) return;
        const coordinates = this.state.agency.coordinates.split(', ');
        const lat = parseFloat(coordinates[0]);
        const long = parseFloat(coordinates[1]);
        const link = "https://www.google.com/maps/dir/?api=1&language=" + this.props.lang + "&destination=" + lat + "," + long + "&z=14";
        openLink(link);
    }

    _handleScroll = (event) => {
        let HEADER_MIN_HEIGHT = Platform.OS === "android" ? 56: 56 + this.props.ios_toolbar_height;
        if(event.nativeEvent.contentOffset.y >= HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT && this.state.logoVisible){
            this.setState({
                ...this.state,
                logoVisible: false
            })
        } else if (event.nativeEvent.contentOffset.y < HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT && !this.state.logoVisible){
            this.setState({
                ...this.state,
                logoVisible: true
            })
        }
    }

    render() {
        let table = null;
        let HEADER_MIN_HEIGHT = Platform.OS === "android" ? 56: 56 + this.props.ios_toolbar_height;
        let HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
        let tableRowPaddingHorizontal = this.props.iphoneXSpace.left > 0 ? 44 : 20;

        if (this.state.selectedTab === 0) {
            table = (<View>
                <View style={[style.tableRow, {paddingHorizontal: tableRowPaddingHorizontal}]}>
                    <View><APIcon size={24} color={"#7cb342"} name="home" /></View>
                    <Text style={[style.tableRowValue, globalStyles().fontLight]}>{capitalize(this.state.agency.address) + ", " + capitalize(this.state.agency.city) + ", " + capitalize(this.state.agency.country)}</Text>
                </View>
                <View style={[style.tableRow, {paddingHorizontal: tableRowPaddingHorizontal}]}>
                    <View><APIcon size={24} color={"#7cb342"} name="phone" /></View>
                    <TouchableWithoutFeedback onPress={() => this.openLink("phone")}>
                        <View>
                            <Text style={[style.tableRowValue, globalStyles().fontLight, style.green_text]}>{this.state.agency.phone}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={[style.tableRow, {paddingHorizontal: tableRowPaddingHorizontal}]}>
                    <View><APIcon size={24} color={"#7cb342"} name="email" /></View>
                    <TouchableWithoutFeedback onPress={() => this.openLink("email")}>
                        <View>
                            <Text style={[style.tableRowValue, globalStyles().fontLight, style.green_text]}>{this.state.agency.email}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                {this.props.website ?
                    <View style={[style.tableRow, { borderBottomWidth: 0 }, {paddingHorizontal: tableRowPaddingHorizontal}]}>
                        <View><APIcon size={24} color={"#7cb342"} name="language" /></View>
                        <TouchableWithoutFeedback onPress={() => this.openLink("website")}>
                            <View>
                                <Text style={[style.tableRowValue, globalStyles().fontLight, style.green_text]}>{this.state.agency.website}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View> : null}

                <Rate lang={this.props.lang} is_rated={this.state.agency.is_rated} agency_id={this.state.agency.id} 
                    onRate={this.props.rate} 
                    avatar={this.props.avatar} 
                    user_firstname={this.props.user ? this.props.user.firstname : ""} />
            </View>)
        } else {
            let offers = null;

            if (this.props.offers) {
                if (this.props.offers.length > 0) {
                    offers = <FlatList 
                        data={this.props.offers}
                        renderItem={({item, index}) => <SingleOffer offer={item} lang={this.props.lang} key={item.id + "_" + index} onSelect={() => this.onOfferClick(item)} rate={false}/> }
                        keyboardShouldPersistTaps="always" 
                        showsVerticalScrollIndicator={false} 
                        alwaysBounceVertical={false}
                        bounces={false}
                        removeClippedSubviews={true}
                        contentContainerStyle={{paddingVertical: 20}}
                        keyExtractor={(item, index) => item.id + "_" + index}
                    />
                } else {
                    offers = <FlatList 
                        data={[1]}
                        renderItem={({item, index}) => <APNothingFound text={translate["no-offers-found"][this.props.lang]} /> }
                        showsVerticalScrollIndicator={false} 
                        alwaysBounceVertical={false}
                        bounces={false}
                        removeClippedSubviews={true}
                        contentContainerStyle={{paddingVertical: 70}}
                        keyExtractor={(item, index) => index}
                    />
                }
            }

            table = <View style={style.offers}>
                        <View style={style.offers_container}>
                            {offers}
                        </View>
                        {this.props.offers_loading ? <APSpinner /> : null}
                    </View>;
        }

        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });

        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const logoOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp',
        });

        const imageTranslate = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });

        const logoTranslateY = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [140, Platform.OS === "android" ? -32 : -32 + this.props.ios_toolbar_height],
            extrapolate: 'clamp',
        });

        const logoTranslateX = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [-60, -(0.75 * 120) / 2 + 48 - this.state.winWidth/2],
            extrapolate: 'clamp',
        });

        const logoScale = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 0.25],
            extrapolate: 'clamp',
        });

        return (
            <APContainer 
                style={style.container} 
                header={false} 
                screenComponent={this}
            >
                <ScrollView 
                    showsVerticalScrollIndicator={false} 
                    scrollEventThrottle={16}
                    alwaysBounceVertical={false}
                    bounces={false}
                    onScroll={this.onScroll}
                    screenId={this.props.testID}
                >
                    <View style={style.wrapper}>
                        <View style={style.contentTop}>
                            <Text style={[style.title, globalStyles().fontBold]}>{this.state.agency.tour_agent}</Text>
                            <APStars rate={this.state.agency.average_rate} />
                            {this.state.agency.badges && this.state.agency.badges[0] ? <View style={style.cup}><APIcon name={this.state.agency.badges[0]} size={24} color="#fff" /></View> : null}
                        </View>
                        <APTabs
                            initialIndex={0}
                            items={this.state.items}
                            onChange={this.onTabChange}
                            textStyle={{ color: "#424242", fontSize: 16 }}
                            containerStyle={{ backgroundColor: "#F6F6F6" }}
                            lowerCase={true}
                        />
                        <View>
                            {table}
                        </View>
                    </View>
                </ScrollView>
                <Animated.View style={[style.header, !this.state.logoVisible ? globalStyles().shadow: null, { height: headerHeight }, this.state.logoVisible ? {overflow: "hidden"} : null]} pointerEvents="box-none">
                    
                    <Animated.Image
                        style={[
                            style.backgroundImage,
                            { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },
                        ]}
                        source={bgImage}
                        pointerEvents="none"
                    />
                    <Animated.View>
                        <Header
                            back={false}
                            style={{ backgroundColor: "transparent", flexDirection: "row-reverse" }}
                            noLogo={true}
                            noMenu={true}
                        />
                        <APBackButton onBack={this.onBack}/>
                        <TouchableWithoutFeedback onPress={this.openRoute}>
                            <View style={[style.location, { top: Platform.OS === "android" ? 6 : 6 + this.props.ios_toolbar_height }]} >
                                <APIcon name="location_on" color="#fff" size={24} paddingVertical={10} />
                            </View>
                        </TouchableWithoutFeedback>
                    </Animated.View>
                    <View style={[style.headerTextTitleContainer, {top: Platform.OS === "android" ? 0 : this.props.ios_toolbar_height}]} pointerEvents="none">
                        <Animated.Text style={[style.headerTextTitle, globalStyles().fontBold, { opacity: logoOpacity }]} numberOfLines={1} >{this.state.agency.tour_agent}</Animated.Text>
                    </View>
                </Animated.View>
                <Animated.View style={[style.logoInner, { transform: [{ translateY: logoTranslateY }, {translateX: logoTranslateX}, {scale: logoScale}], opacity: 1 }, globalStyles().shadow]} pointerEvents="none">
                    <Image style={style.logo} source={{ uri: this.state.agency.photo }} resizeMode='cover'></Image>
                </Animated.View>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: "center"
    },

    wrapper: {
        padding: 0,
        backgroundColor: "#fff",
        marginTop: HEADER_MAX_HEIGHT,
        paddingTop: 80
    },

    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#7cb342',
        zIndex: 10001
    },

    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover'
    },

    logoInner: {
        width: 120,
        height: 120,
        borderRadius: 60,
        backgroundColor: "#fff",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        top: 0,
        left: "50%",
        zIndex: 10002
    },

    logo: {
        width: "100%",
        height: "100%",
        borderRadius: 60
    },

    contentTop: {
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        marginBottom: 20
    },

    title: {
        paddingVertical: 12,
        fontSize: 28,
        color: "#000",
        textAlign: "center"
    },

    cup: {
        backgroundColor: "#FEA62A",
        width: 44,
        height: 44,
        borderRadius: 22,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 16
    },

    btnSpace: {
        width: "5%"
    },

    tableRow: {
        flexDirection: "row",
        paddingVertical: 15,
        paddingHorizontal: 20,
        justifyContent: "flex-start",
        borderBottomWidth: 1,
        borderBottomColor: "#d0d0d0"
    },
    tableRowValue: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 24,
        paddingLeft: 20
    },

    headerTextTitleContainer: {
        left: 36,
        
        position: "absolute",
        width: "80%"
    },

    headerTextTitle: {
        color: "#fff",
        paddingHorizontal: 20,
        fontSize: 20,
        lineHeight: 56,
        paddingLeft: 52
    },

    offers: {
        paddingTop: 10,
        paddingBottom: 20
    },

    green_text: {
        color: "#7cb342"
    },

    offers_container: {
        paddingTop: 0
    },

    location: {
        right: 0,
        paddingHorizontal: 18,
        position: "absolute",
        zIndex: 10002
    },

    back: {
        left: 0,
        paddingLeft: 18,
        position: "absolute",
        zIndex: 10002
    }


})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        offers_loading: state.offers.offers_loading,
        offers: state.offers.offers,
        prevPage: state.index.prevPage,
        avatar: state.index.avatar,
        ios_toolbar_height: state.index.ios_toolbar_height,
        iphoneXSpace: state.index.iphoneXSpace,
        agency: state.agencies.agency
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        rate: (agency_id, rating) => { dispatch(rate(agency_id, rating)) },
        getSpecialOffers: (tour_id, agency_name) => { dispatch(get_special_offers(tour_id, agency_name)) },
        resetSpecialOffers: () => { dispatch(reset_special_offers()) },
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "activate"))},
        getAgency: (id) => {dispatch(get_agency(id))},
        resetAgency: () => {dispatch(reset_agency())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AgencyScreen)
