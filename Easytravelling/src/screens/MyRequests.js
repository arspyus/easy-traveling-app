import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, RefreshControl, Platform, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import { get_my_requests, reset_my_requests, delete_request, setRequestFilter } from '../store/actions/myRequest';
import { select_type } from '../store/actions/makeRequestAction';
import { show_confirmation_popup } from '../store/actions/indexAction';

import APDropdown from '../ui/dropdown';
import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APIcon from '../ui/icon';

import SingleRequest from '../components/singleRequest';

import translate from '../utils/translate';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';

class MyRequestsScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this)
    }

    screenName = "myrequests";

    willAppear() {
        this.getMyRequests();
    }

    willDisappear() {
        this.props.resetMyRequests();
    }

    getMyRequests(filter, reset) {
        filter = filter || this.props.filter;
        let from = "";
        let to = "";
        let status = "all";
        let currentDate = new Date();
        let calculatedDate = new Date();

        if(filter == 'last_week'){
            calculatedDate.setDate(currentDate.getDate() - 7);
        }else if(filter == 'last_month'){
            calculatedDate.setDate(currentDate.getDate() - 30);
        }else if(filter == 'last_six'){
            calculatedDate.setDate(currentDate.getDate() - 180);
        }else if(filter == 'last_year'){
            calculatedDate.setDate(currentDate.getDate() - 365);
        } else if (filter === "archive") {
            status = "archive";
        } else if (filter === "booked") {
            status = "booked";
        }

        if(filter !== "all" && filter !== "archive" &&  filter !== "booked"){
            to = currentDate.getFullYear() + '-' + ((currentDate.getMonth() + 1) < 10 ? '0' + (currentDate.getMonth() + 1) : currentDate.getMonth() + 1) + '-' + (currentDate.getDate()  < 10 ? '0' + currentDate.getDate() : currentDate.getDate() );
            from = calculatedDate.getFullYear() + '-' + ((calculatedDate.getMonth() + 1) < 10 ? '0' + (calculatedDate.getMonth() + 1) : calculatedDate.getMonth() + 1) + '-' + (calculatedDate.getDate()  < 10 ? '0' + calculatedDate.getDate() : calculatedDate.getDate());
        }
        if(!reset) this.props.resetMyRequests();

        this.setState({
            positions: {}
        })
        this.props.getMyRequests(status, from, to, reset)
    }

    state = {
        dropdownItems: [
            { text: translate["booked"][this.props.lang], value: "booked" },
            { text: translate["last-week"][this.props.lang], value: "last_week" },
            { text: translate["last-month"][this.props.lang], value: "last_month" },
            { text: translate["last-six-months"][this.props.lang], value: "last_six" },
            { text: translate["last-year"][this.props.lang], value: "last_year" },
            { text: translate["all"][this.props.lang], value: "all" },
            { text: translate["archive"][this.props.lang], value: "archive" }
        ],
        positions: {}
    }

    dropdown = null;

    showDropdown = () => {
        this.dropdown.show()
    }

    navigateTo = (id, page, params) => {
        let props = {};
        let screen = null;
        if(params){
            props = {...params}
        }
        if (page === "offer") {
            screen = 'SingleOfferScreen';
            props.offer_id = id;
            props.isArchive = this.props.filter === "archive"
        } else if (page === "request") {
            screen = 'SingleRequestScreen';
            props.request_id = id;
            props.isArchive = this.props.filter === "archive"
        }
        navigate(this, screen, "push", props);
    }

    onFilter = (index, value) => {
        this.props.setFilter(value);
        this.getMyRequests(value)
    }

    onRefresh = () => {
        this.getMyRequests(null, true);
    }

    _renderRow = (rowData, rowID, highlighted) => {
        return (
            <View style={[style.dropdownStyle, highlighted ? { backgroundColor: "#F1F1F1" } : null, rowID == this.state.dropdownItems.length - 1 ? { borderTopColor: "#e0e0e0", borderTopWidth: 1 } : null]}>
                <Text style={[style.rowText, globalStyles().fontLight, rowID == this.state.dropdownItems.length - 1 ? { color: "#EF5350" } : null]}>{rowData}</Text>
            </View>
        )
    }

    navigateToMakeRequest = () => {
        if(this.props.user && this.props.user.user_status === "unconfirmed"){
            this.props.showConfirmationPopup();
            return;
        }

        navigate(this, "MakeRequestScreen", "resetTo", null);
    }

    setPosition = (y, h, ind) => {
        let positions = {...this.state.positions};
        positions[ind] = {y: y, h: h}
        positions[ind].isVisible = y <= Dimensions.get("window").height - 90;
        this.setState({
            positions: positions
        })
    }

    onScroll = ($event) => {
        let ev = $event.nativeEvent;
        let offset = ev.contentOffset.y;
        let result = {...this.state.positions};
        if(this.state.positions){
            let positions = Object.keys(this.state.positions);
            positions.forEach(num => {
                if(result[num] && !result[num].isVisible){
                    result[num].isVisible = offset + Dimensions.get("window").height - 90 > result[num].y;
                }
            })
        }

        this.setState({
            positions: result
        })

    }

    render() {
        let requestsList = null;
        if (this.props.requests && this.props.requests.length > 0) {
            requestsList = this.props.requests.map((request, ind) => {
                return  <View onLayout={(event) => {this.setPosition(event.nativeEvent.layout.y, event.nativeEvent.layout.height, ind)}} key={request.request_id + "_" + ind} style={{paddingTop: 2}}>
                            <SingleRequest
                                request={request}
                                lang={this.props.lang}
                                navigateTo={this.navigateTo}
                                isVisible={this.state.positions[ind] && this.state.positions[ind].isVisible}
                            />
                        </View>

            })
        }

        let dropdown = <View style={style.dropdown}>
                            <APDropdown
                                options={this.state.dropdownItems}
                                defaultValue={this.props.filter}
                                defaultIndex={this.state.dropdownItems.map(el => el.value).indexOf(this.props.filter)}
                                onSelect={this.onFilter}
                                textStyle={{ fontSize: 0 }}
                                renderRow={this._renderRow}
                                ref={(dropdown) => { this.dropdown = dropdown; }}
                            ><View style={globalStyles().hidden}></View></APDropdown>
                        </View>

        return (
            <APContainer
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })}
                title={translate["my-requests"][this.props.lang]}
                filter={true}
                filterContent={dropdown}
                onFilter={throttle(() => this.showDropdown())}
                screenId={this.props.testID}
                screenComponent={this}
                >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    alwaysBounceVertical={Platform.OS !== "android"}
                    bounces={ Platform.OS !== "android" }
                    horizontal={false}
                    onScroll={this.onScroll}
                    style={{backgroundColor: "#f6f6f6"}}
                    removeClippedSubviews={true}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.reset_loader}
                            onRefresh={this.onRefresh}
                            colors={["#7cb342"]}
                            tintColor="#7cb342"
                        />
                      }
                >
                    <APWrapper containerStyles={{paddingHorizontal: 0}}>
                        {
                            this.props.requests && this.props.requests.length > 0 ?
                            <View>
                                {requestsList}
                            </View>
                        : this.props.requests ?
                            <View style={style.no_request_container}>
                                <View style={style.no_request}>
                                    <TouchableWithoutFeedback onPress={throttle(this.navigateToMakeRequest)}>
                                        <View>
                                            <APIcon size={36} color={"#7cb342"} name={"add"} />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    <Text style={[style.no_request_text, globalStyles().fontLight]}>{translate['make_request_now'][this.props.lang]}</Text>
                                </View>
                            </View>
                            : null
                        }
                    </APWrapper>
                </ScrollView>
            </APContainer>

        )
    }
}

const style = StyleSheet.create({
    no_request_container: {
        width: "100%",
        alignItems: "center",
        justifyContent: "flex-end",
        height: 160,
        borderRadius: 10
    },

    no_request: {
        backgroundColor: "rgba(188, 188, 188, 0.3)",
        width: "94%",
        height: 160,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "flex-end"
    },

    no_request_text: {
        color: "rgba(81, 81, 81, 0.5)",
        fontSize: 12,
        lineHeight: 17,
        letterSpacing: 0.21,
        paddingBottom: 29,
        paddingTop: 22
    },

    dropdown: {
        top: 34,
        position: "absolute",
        right: 18
    },

    dropdownStyle: {
        backgroundColor: "#fff",
        paddingVertical: 14,
        paddingHorizontal: 16
    },

    rowText: {
        fontSize: 16,
        color: 'black',
        lineHeight: 22
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        requests: state.myRequest.requests,
        filter: state.myRequest.filter,
        reset_loader: state.myRequest.reset_loader
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMyRequests: (status, from, to, reset) => { dispatch(get_my_requests(status, from, to, reset)) },
        resetMyRequests: () => { dispatch(reset_my_requests()) },
        deleteRequest: (id, from, to) => { dispatch(delete_request(id, from, to)) },
        setFilter: (filter) => { dispatch(setRequestFilter(filter)) },
        selectType: (type) => {dispatch(select_type(type))},
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "activate"))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyRequestsScreen)
