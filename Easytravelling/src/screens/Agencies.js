import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text, RefreshControl, Platform } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APSpinner from '../ui/spinner';
import APNothingFound from '../ui/nothingFound';

import SingleAgency from '../components/singleAgency';

import translate from '../utils/translate';
import isReachedToBottom from '../utils/isReachedToBottom';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';

import { get_agencies, reset_agencies } from '../store/actions/agencyAction';
import { select_type } from '../store/actions/makeRequestAction';
import { show_auth_popup, show_confirmation_popup } from  '../store/actions/indexAction';

class AgenciesScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    screenName = "agencies";

    willAppear() {
        if(this.props.prevPage !== "easytraveling.AgencyScreen" ){
            this.props.resetAgencies();
            this.props.getAgencies();
        } else if (!this.props.agencies || this.props.agencies.length === 0){
            this.props.getAgencies();
        }
    }

    willDisappear() {
        if(!this.movingToSingleAgency)
            this.props.resetAgencies();
    }

    onSelect = (agency) => {
        let params = {
            country: agency.country,
            city: agency.city,
            tour_agent: agency.tour_agent,
            phone: agency.phone,
            email: agency.email,
            website: agency.website,
            photo: agency.photo,
            is_rated: agency.is_rated,
            id: agency.id,
            coordinates: agency.coordinates,
            lang: this.props.lang,
            average_rate: agency.average_rate,
            badges: agency.badges,
            address: agency.address
        }
        if(this.props.isGuest){
            this.props.showAuthPopup({
                screen: "AgencyScreen",
                params: params
            });
            return;
        }
        this.movingToSingleAgency = true;
        navigate(this, "AgencyScreen", "push", params);
    }

    scrollTimer = null;

    onScroll = ($event) => {
        clearTimeout(this.scrollTimer)
        let ev = $event.nativeEvent;
        this.scrollTimer = setTimeout(() => {
            if(isReachedToBottom(ev) && !this.props.agencies_loading){
                this.props.getAgencies();
            }
        }, 100)
        
    }

    onRefresh = () => {
        this.props.getAgencies(true);
    }

    navigateToMakeRequest = (type) => {
        
        this.props.selectType(type)
        if(this.props.isGuest){
            this.props.showAuthPopup({
                screen: "MakeRequestScreen",
                params: {
                    type: type
                }
            });
            return;
        }

        if(this.props.user && this.props.user.user_status === "unconfirmed"){
            this.props.showConfirmationPopup();
            return;
        }
        navigate(this, "MakeRequestScreen", "resetTo", {
            type: type
        });
    }

    render() {
        let agencies = null;

        let refresh_control = <RefreshControl
                                    refreshing={this.props.agencies_reset_loading}
                                    onRefresh={this.onRefresh}
                                    colors={["#7cb342"]}
                                    tintColor="#7cb342"
                                />

        const bounces = Platform.OS !== "android" ;
        if (this.props.agencies) {

            if (this.props.agencies.length > 0) {
                agencies = <FlatList 
                    data={this.props.agencies}
                    renderItem={({item, index}) => <SingleAgency isLast={this.props.agencies.length - 1 === index} agency={item} lang={this.props.lang} key={item.id} onSelect={() => this.onSelect(item)} ios_toolbar_height={this.props.ios_toolbar_height} /> }
                    keyExtractor={(item, index) => item.id}
                    keyboardShouldPersistTaps="always" 
                    onScroll={this.onScroll} 
                    showsVerticalScrollIndicator={false} 
                    alwaysBounceVertical={bounces}
                    bounces={bounces}
                    removeClippedSubviews={true}
                    contentContainerStyle={{paddingVertical: 20}}
                    refreshControl={
                        refresh_control
                    }
                />
            } else {
                agencies = <FlatList 
                    data={[1]}
                    renderItem={({item, index}) => <APNothingFound text={translate["no-agencies-found"][this.props.lang]} /> }
                    keyExtractor={(item, index) => index}
                    showsVerticalScrollIndicator={false} 
                    alwaysBounceVertical={bounces}
                    bounces={bounces}
                    removeClippedSubviews={true}
                    contentContainerStyle={{paddingVertical: 20, paddingTop:70}}
                    refreshControl={
                        refresh_control
                    }
                />
            }
        }

        return (
            <APContainer 
                onMenu={() =>this.props.navigator.toggleDrawer({ side: 'left',  animated: true})} 
                title={translate["tour-agencies"][this.props.lang]} plus={true} 
                onNavigate={this.navigateToMakeRequest} 
                screenId={this.props.testID}
                screenComponent={this}
            >
                <View style={[style.container]}>
                    {agencies}
                    {this.props.agencies_loading ? <APSpinner /> : null}
                </View>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    container:{
        paddingTop: 0,
        flex: 1
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        isGuest: state.user.isGuest,
        agencies: state.agencies.agencies,
        agencies_loading: state.agencies.agencies_loading,
        agencies_reset_loading: state.agencies.agencies_reset_loading,
        prevPage: state.index.prevPage,
        ios_toolbar_height: state.index.ios_toolbar_height
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAgencies: (reset) => { dispatch(get_agencies(reset)) },
        resetAgencies: () => {dispatch(reset_agencies())},
        selectType: (type) => {dispatch(select_type(type))},
        showAuthPopup: (pending) => {dispatch(show_auth_popup(true, pending))},
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "activate"))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AgenciesScreen)
