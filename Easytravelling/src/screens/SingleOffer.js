import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';

import AnimatedView from '../ui/animatedView';
import Flight from '../ui/flight';
import APSection from '../ui/section';
import APWrapper from '../ui/wrapper';
import APIcon from '../ui/icon';
import APStars from '../ui/stars';
import APFloatingButton from '../ui/floatingButton';
import APFullLoader from '../ui/fullLoader';

import translate from '../utils/translate';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import throttle from '../utils/throttle';
import globalStyles from '../utils/style';
import urlify from '../utils/urlify';
import capitalize from '../utils/capitalize';
import APImageStore from '../utils/imageStore';

import { get_offer, set_offer_seen } from '../store/actions/singleAction';
import generateArray from '../utils/generateArray';


class SingleOfferScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    state = {
        offers: this.props.offers ? this.props.offers.map(offer => {
            return { ...offer, hotel_images: offer.thumbnail ? [offer.thumbnail]: [] }
        }) : null,
        showButton: false,
        showGallery: false,
        galleryInitialImage: 0,
        handledOffers: []
    }

    willAppear() {
        if (!this.state.offers) {
            this.props.getOffer(this.props.offer_id);
        } else {
            this.handleOffer(this.state.offers.filter(el => el.offer_id === this.props.offer_id)[0])
        }

    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.storeOffers != this.props.storeOffers && this.props.storeOffers) {
            this.setState({
                offers: this.props.storeOffers.map(offer => {
                    return { ...offer, hotel_images: [] }
                })
            }, () => {
                this.handleOffer(this.state.offers.filter(el => el.offer_id === this.props.offer_id)[0])
            })
        }
    }

    handleOffer = (offer) => {
        if (this.state.handledOffers.indexOf(offer.offer_id) > -1) return;
        this.setState({
            handledOffers: [...this.state.handledOffers].concat([offer.offer_id])
        }, () => {
            if (offer.seen_status === "new") {
                this.props.setOfferSeen(offer.offer_id)
            }

            if (offer.place_id) {
                this.loadImages(offer);
            }
        })
    }

    loadImages = (offer) => {
        const IS = new APImageStore(offer.place_id);
        IS.getThumbnail().then(thumbnail => {
            if (!offer.thumbnail) {
                this.setState({
                    offers: this.state.offers.map(offerObj => {
                        return offerObj.offer_id === offer.offer_id ? { ...offerObj, thumbnail: thumbnail } : offerObj
                    })
                })
            }
        });

        IS.getMap().then(map => {
            if (!offer.map) {
                this.setState({
                    offers: this.state.offers.map(offerObj => {
                        return offerObj.offer_id === offer.offer_id ? { ...offerObj, map: map } : offerObj
                    })
                })
            }
        });

        IS.getImages(thumbnail => {
            this.setState({
                offers: this.state.offers.map(offerObj => {
                    return offerObj.offer_id === offer.offer_id ? { ...offerObj, hotel_images: [...offerObj.hotel_images].concat([thumbnail]) } : offerObj
                })
            })
        })
    }

    handleBackPress = () => {
        if (this.state.showGallery) {
            this.setState({ showGallery: false });
            return true;
        } else {
            return false;
        }
    }

    onBack = () => {
        navigate(this, null, "pop");
    }

    preBook = (offer) => {
        navigate(this, "PrebookScreen", "push", {
            offer_id: offer.offer_id,
            request_id: offer.request_id,
            offer_author: offer.offer_author,
            adults: parseInt(offer.adults),
            children: parseInt(offer.children),
            infant: parseInt(offer.infant),
            country: offer.country,
            city: offer.country,
            price: offer.price
        });

    }

    generateBoardType = (board_type) => {
        if (!board_type) return "";
        let splitted = board_type.split(",");
        let result = [];
        splitted.forEach(element => {
            result.push(translate[element.trim()][this.props.lang] ? translate[element.trim()][this.props.lang] : element.trim())
        });
        return result.join(", ")
    }

    navigateToAgencySingle = (agency) => {
        let params = {
            ...agency,
            lang: this.props.lang,
        }

        navigate(this, "AgencyScreen", "push", params);
    }

    onAnimatinChange = (change) => {
        if (this.state.showButton === change) {
            this.setState({
                showButton: !change
            })
        }
    }

    openGallery = (index) => {
        this.setState({
            showGallery: true,
            galleryInitialImage: index
        })
    }

    closeGallery = (index) => {
        this.setState({
            showGallery: false,
            galleryInitialImage: index
        })
    }

    renderFixedContent = (offer) => {
        const fixedContents = [];
        
        let floatingButton = <APFloatingButton
            title={offer.city ? capitalize(offer.city) : offer.country ? capitalize(offer.country) : ""}
            description={offer.price + " " + translate["amd"][this.props.lang]}
            buttonText={translate["pre-book"][this.props.lang]}
            isVisible={this.state.showButton}
            onClick={throttle(() => this.preBook(offer))}
        />;
        if (offer.days_left != -1 && offer.offer_status !== 'booked' && offer.offer_status !== 'rejected' && !this.props.isArchive) {
            fixedContents.push(floatingButton);
        }
        let imagesArray = null;
        if (offer.hotel_images) {
            imagesArray = offer.hotel_images.map((image, index) => {
                return (
                    <View style={{width:'100%',height:'50%'}}>
                        <Image source={{ uri: image }} style={style.image} key={index}/>
                    </View>
                )
            })
        }
        let gallery = (
            <View style={style.gallery}>
              <Swiper height={200} horizontal={true} showsPagination={false} index={this.state.galleryInitialImage}>
                {imagesArray}
              </Swiper>
            </View>)

        let galleryControls = (
            <View style={style.galleryControls}>
                <View style={[style.row, { justifyContent: "space-between" }]}>
                    <TouchableWithoutFeedback onPress={this.closeGallery}>
                        <View>
                            <APIcon name="clear" size={24} color="#ffffff" />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );

        if (this.state.showGallery) {
            fixedContents.push(gallery);
            fixedContents.push(galleryControls)
        }
        return fixedContents;
    }

    renderOffer = (offer) => {
        let personsText = offer.adults + " " + translate['adult'][this.props.lang];
        let childrenCount = parseInt(offer.children) + parseInt(offer.infant);
        if (childrenCount > 0) {
            personsText += " " + childrenCount + " " + translate['child'][this.props.lang];
        }
        let valid_until = null;
        if (offer.days_left == -1 && offer.offer_status !== 'booked' && offer.offer_status !== 'rejected') {
            valid_until = translate["expired"][this.props.lang];
        } else if (offer.offer_status !== 'booked' && offer.offer_status !== 'rejected') {
            valid_until = offer.days_left === 0 ? translate["expiring-today"][this.props.lang] : translate["valid-for"][this.props.lang].replace('%days%', offer.days_left);
        } else {
            valid_until = ""
        }
        let hotel_stars = generateArray(parseInt(offer.hotel_rating)).map(el => {
            return <APIcon size={15} color="#e9c32b" name="star" marginRight={5.6}  />
        })

        let images = null;
        if (offer.hotel_images) {
            images = offer.hotel_images.map((image, index) => {
                return (
                    <TouchableOpacity onPress={() => this.openGallery(index)} key={index}>
                        <View style={style.hotel_image_wrap}>
                            <Image source={{ uri: image }} style={style.hotel_image} imageStyle={{ borderRadius: 5 }} />
                        </View>
                    </TouchableOpacity>
                )
            })
        }
        let nights = Math.floor(((new Date(offer.hotel_checkout.split(" ")[0]) - new Date(offer.hotel_checkin.split(" ")[0])) / 3600 / 24 / 1000));
        return (
            <AnimatedView
                testID={this.props.testID}
                screenComponent={this}
                onBack={this.onBack}
                photo={offer.thumbnail ? offer.thumbnail : null}
                icon={offer.days_left != -1 && offer.offer_status !== 'booked' && offer.offer_status !== 'rejected' && !this.props.isArchive && !this.state.showGallery ? "chevron_right" : null }
                onIconClick={throttle(() => this.preBook(offer))}
                title={offer.city ? capitalize(offer.city) + ", " + capitalize(offer.country) : capitalize(offer.country)}
                description={personsText}
                badgeWidth={0}
                renderBadge={() => null}
                boxTitle={offer.price + " " + translate["amd"][this.props.lang]}
                boxDescription={valid_until}
                onAnimatinChange={this.onAnimatinChange}
                fixedContent={this.renderFixedContent(offer)}
                hideHeader={this.state.showGallery}
                onClick={throttle(() => this.openGallery(0))}
                defaultSource={require('./../../assets/images/hotel.png')}
            >
                {
                    this.state.offers ?
                        <View style={style.content}>
                            <APWrapper>
                                {
                                    offer.transportation ?
                                        <APSection>
                                            <View style={[globalStyles().section, { paddingBottom: 0 }]}>
                                                <View style={style.sectionTitle}>
                                                    <Text style={[globalStyles().big_title, globalStyles().fontBold]}>{translate['transportation'][this.props.lang]}</Text>
                                                </View>
                                                <View>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.grey, style.subTitle]}>{offer.transportation.type ? translate[offer.transportation.type][this.props.lang] : ""}</Text>
                                                </View>
                                                <View style={{ borderBottomWidth: 0.5, borderBottomColor: "#bcbcbc" }} >
                                                    <Flight type={offer.transportation.type} data={offer.transportation.departure} lang={this.props.lang} />
                                                </View>
                                                <Flight type={offer.transportation.type} data={offer.transportation.arrival} lang={this.props.lang} />
                                            </View>
                                        </APSection> : null
                                }

                                <APSection>
                                    <View style={[globalStyles().section, { paddingBottom: 10 }]}>
                                        <View style={style.sectionTitle}>
                                            <Text style={[globalStyles().big_title, globalStyles().fontBold]}>{translate['hotel'][this.props.lang]}</Text>
                                        </View>
                                        <View>
                                            <Text style={[style.hotel_name, globalStyles().fontBold]}>{capitalize(offer.hotel)}</Text>
                                        </View>
                                        <View style={[style.row]}>
                                            {hotel_stars}
                                        </View>
                                        <TouchableWithoutFeedback onPress={() => Linking.openURL("https://www.google.com/maps/search/" + offer.hotel)}>
                                            <View style={style.mapContainer}>
                                                {
                                                    offer.map ?
                                                        <Image style={style.map} source={{ uri: offer.map }} imageStyle={{ borderRadius: 10 }} /> :
                                                        null
                                                }
                                            </View>
                                        </TouchableWithoutFeedback>
                                        {
                                            offer.board_type ?
                                                <View style={style.table_row}>
                                                    <View style={style.table_row_icon}>
                                                        <APIcon name="restaurant" size={22} color="#7cb342" />
                                                    </View>
                                                    <View style={style.table_row_info}>
                                                        <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{this.generateBoardType(offer.board_type)}</Text>
                                                    </View>
                                                </View> :
                                                null
                                        }
                                        {
                                            offer.rooms ?
                                                <View style={style.table_row}>
                                                    <View style={style.table_row_icon}>
                                                        <APIcon name="hotel" size={22} color="#7cb342" />
                                                    </View>
                                                    <View style={style.table_row_info}>
                                                        <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{offer.rooms + " " + translate["rooms_offer"][this.props.lang]}</Text>
                                                    </View>
                                                </View> :
                                                null
                                        }
                                        <View style={style.table_row}>
                                            <View style={style.table_row_icon}>
                                                <APIcon name="date_range" size={22} color="#7cb342" />
                                            </View>
                                            <View style={style.table_row_info}>
                                                <View style={style.row}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{nights + " " + translate["night"][this.props.lang] + " "}</Text>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontLight, style.grey]}>{"  " + (parseInt(nights) + 1) + " " + translate["days"][this.props.lang]}</Text>
                                                </View>
                                                <View style={style.row}>
                                                    <View style={{ marginRight: 20 }}>
                                                        <Text style={[globalStyles().text, globalStyles().fontBold, style.grey, { paddingBottom: 5, paddingTop: 9 }]}>{translate["hotel-check-in"][this.props.lang]}</Text>
                                                        <Text style={[globalStyles().text, globalStyles().fontBold, style.green]}>{offer.hotel_checkin.split(" ")[0].split("-").reverse().join(".")}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={[globalStyles().text, globalStyles().fontBold, style.grey, { paddingBottom: 5, paddingTop: 9 }]}>{translate["hotel-check-out"][this.props.lang]}</Text>
                                                        <Text style={[globalStyles().text, globalStyles().fontBold, style.green]}>{offer.hotel_checkout.split(" ")[0].split("-").reverse().join(".")}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        {
                                            offer.hotel_images && offer.hotel_images.length > 0 ?
                                                <View>
                                                  <View style={[style.table_row, { marginBottom: 0 ,justifyContent:"flex-start"}]}>
                                                      <View style={style.table_row_icon}>
                                                        <APIcon name="image" size={22} color="#7cb342" />
                                                      </View>
                                                        <Text style={[globalStyles().title, globalStyles().fontBold , {marginBottom:10,marginLeft:0}]}>{translate["images"][this.props.lang]}</Text>
                                                  </View>
                                                  <View style={style.table_row_info}>
                                                      <View style={style.hotel_images}>
                                                          {images}
                                                      </View>
                                                  </View>
                                                </View> :
                                                null
                                        }
                                    </View>
                                </APSection>
                                {
                                    offer.notes ?
                                        <APSection>
                                            <View style={globalStyles().section}>
                                                <View style={style.sectionTitle}>
                                                    <Text style={[globalStyles().big_title, globalStyles().fontBold]}>{translate['notes'][this.props.lang]}</Text>
                                                </View>
                                                <View>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.grey, { lineHeight: 24 }]}>{urlify(offer.notes)}</Text>
                                                </View>
                                            </View>
                                        </APSection> : null
                                }

                                <APSection>
                                    <TouchableWithoutFeedback onPress={throttle(() => this.navigateToAgencySingle(offer.agency))}>
                                        <View style={globalStyles().section}>
                                            <View style={style.row}>
                                                <View style={[style.tour_agent_logo_wrapper, globalStyles().shadow]}>
                                                    <Image
                                                        style={[
                                                            style.tour_agent_logo
                                                        ]}
                                                        source={{ uri: offer.agency.photo }}
                                                    />
                                                </View>
                                                <View style={{ paddingLeft: 10, flex: 1 }}>
                                                    <View style={[style.row, { flex: 1, alignItems: "center" }]}>
                                                        <View style={{ maxWidth: "80%" }}>
                                                            <Text style={[globalStyles().big_title, globalStyles().fontBold]} numberOfLines={1}>{offer.agency.tour_agent}</Text>
                                                        </View>
                                                        {offer.agency.badges && offer.agency.badges[0] ? <View style={style.cup}><APIcon name={offer.agency.badges[0]} size={11} color="#fff" /></View> : null}
                                                    </View>
                                                    <View style={[style.row, { paddingTop: 10 }]}>
                                                        <Text style={[globalStyles().text, globalStyles().fontBold, style.green, { paddingRight: 8 }]}>{offer.agency.average_rate}</Text>
                                                        <APStars rate={offer.agency.average_rate} color="#7cb342" size={14} distance={2} style={{ width: "auto", marginLeft: 0, justifyContent: "flex-start" }} />
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </APSection>
                                <View style={style.notes}>
                                    <Text style={[globalStyles().fontLight, style.notesText, style.grey]}>{translate['single_offer_notes'][this.props.lang]}</Text>
                                </View>
                            </APWrapper>
                        </View> :
                        null
                }

            </AnimatedView >
        )
    }

    render() {
        let offers = this.state.offers ? this.state.offers.map((offer, index) => {
            return (
                <View key={offer.offer_id} style={{ flex: 1 }}>
                    {this.renderOffer(offer)}
                </View>
            )
        }) : null;
        let initialIndex = 0;
        if(this.state.offers){
            this.state.offers.forEach((el, index) => {
                if (el.offer_id === this.props.offer_id) {
                    initialIndex = index;
                    return false;
                }
            })
        }
        

        return this.state.offers ? <Swiper
            showsButtons={false}
            showsPagination={false}
            onIndexChanged={(index) => this.handleOffer(this.state.offers[index])}
            loop={false}
            index={initialIndex}
            scrollEnabled={!this.state.showGallery}>
            {offers}
        </Swiper> : <View style={{flex: 1}}><APFullLoader /></View>
    }
}

const style = StyleSheet.create({
    content: {
        backgroundColor: "#f6f6f6"
    },
    sectionTitle: {
        paddingBottom: 5
    },

    grey: {
        color: "#bcbcbc"
    },
    green: {
        color: "#7cb342"
    },
    hotel_name: {
        color: "#515151",
        paddingTop: 15,
        paddingBottom: 5,
        fontSize: 24,
        lineHeight: 33,
        letterSpacing: 0.42
    },
    row: {
        flexDirection: "row"
    },
    mapContainer: {
        borderRadius: 10,
        width: "100%",
        marginTop: 20,
        marginBottom: 30
    },
    map: {
        borderRadius: 10,
        height: 152,
        width: "100%"
    },
    table_row: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 20
    },
    table_row_icon: {
        width: 43,
        justifyContent: "flex-start",
        alignItems: "flex-start",
        paddingLeft: 3
    },
    table_row_info: {
        flex: 1,
    },
    table_row_info_text: {
        paddingTop: 5,
        color: "#bcbcbc"
    },

    hotel_images: {
        flexDirection: "row",
        flexWrap: "wrap",
        width:"100%",
        paddingLeft:0
    },

    hotel_image_wrap: {
        marginRight: 5,
        marginBottom: 5,
        borderRadius: 5
    },

    hotel_image: {
        width: 94,
        height: 94,
        borderRadius: 5,
        flexGrow:1,
        flexBasis:"25%"
    },
    tour_agent_logo_wrapper: {
        height: 50,
        width: 50,
        borderRadius: 50
    },
    tour_agent_logo: {
        height: 50,
        resizeMode: "cover",
        width: 50,
        borderRadius: 50
    },
    cup: {
        backgroundColor: "#ffcd2c",
        width: 20,
        height: 20,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 13
    },

    notes: {
        paddingHorizontal: 25,
        paddingBottom: 80,
        paddingTop: 20
    },

    notesText: {
        lineHeight: 24,
        fontSize: 12,
        letterSpacing: 0.21
    },
    gallery: {
        flex: 1,
        backgroundColor: 'black',
        position: "absolute",
        left: 0,
        top: 0,
        zIndex: 1000001,
        height: "100%",
        width: "100%",
        elevation: 30
    },
    galleryControls: {
        position: "absolute",
        left: 0,
        top: 0,
        zIndex: 1000002,
        width: "100%",
        padding: 15,
        elevation: 31
    },
    subTitle:{
        fontSize: 14,
        textAlign: 'left'
    },
    image:{
        width:"100%",
        flex:1,
        height:'100%',
        position: "absolute",
        left: 0,
        top: '50%',
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        storeOffers: state.single.offers,
        prevPage: state.index.prevPage,
        isLoading: state.index.isLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getOffer: (id) => { dispatch(get_offer(id)) },
        setOfferSeen: (id) => { dispatch(set_offer_seen(id)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleOfferScreen)
