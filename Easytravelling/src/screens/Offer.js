import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback } from 'react-native';

import { connect } from 'react-redux';

import APIcon from '../ui/icon';
import APStars from '../ui/stars';
import AnimatedView from '../ui/animatedView';

import translate from '../utils/translate';
import openLink from '../utils/openLink';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';
import urlify from '../utils/urlify';
import throttle from '../utils/throttle';

import config from '../config/config'
class OfferScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    call = () => {
        let url = "tel:" + config.phone_number
        openLink(url);
    }

    willAppear() {
        this.comesFromPage = this.props.prevPage;
    }

    onBack = () => {
        if(this.comesFromPage !== "easytraveling.AuthScreen"){
            navigate(this, null, "pop");
        } else {
            navigate(this, "OffersScreen", 'resetTo', {})
        }
    }

    navigateToAgencySingle = () => {
        let params = {
            ...this.props.offer.agency,
            lang: this.props.lang,
        }

        navigate(this, "AgencyScreen", "push", params);
    }

    renderRate = () => {
        const rate = parseInt(this.props.offer.agency.average_rate);
        if(rate === 0) return null;
        return (
            <View style={style.rate} pointerEvents="box-none">
                <View style={{ marginRight: 8 }} pointerEvents="none"><APIcon size={16} name="star" color={"#fff"} /></View>
                <Text style={[style.headerTextDesc, globalStyles().fontBold]} pointerEvents="none" >{this.props.offer.agency.average_rate}</Text>
            </View> 
        )
    }

    render() {
        let tableRowPaddingHorizontal = this.props.iphoneXSpace.left > 0 ? 44 : 20;
        let table = (<View>
            <TouchableWithoutFeedback onPress={throttle(this.navigateToAgencySingle)}>
                <View style={[style.tableRow, { justifyContent: "space-between", paddingVertical: 24, paddingHorizontal: tableRowPaddingHorizontal }]}>
                    <View>
                        <Text style={style.tour_agent}>{this.props.offer.agency.tour_agent}</Text>
                        <APStars rate={this.props.offer.agency.average_rate} color="#7cb342" size={16} distance={2} style={{ width: "auto", marginLeft: 0, justifyContent: "flex-start" }} />
                    </View>
                    <Image
                        style={[
                            style.tour_agent_logo,
                        ]}
                        source={{ uri: this.props.offer.agency.photo }}
                    />
                </View>
            </TouchableWithoutFeedback>
            <View style={[style.tableRow, {paddingHorizontal: tableRowPaddingHorizontal}]}>
                <View><APIcon size={24} color={"#7cb342"} name="date_range" /></View>
                <Text style={[style.tableRowValue, globalStyles().fontLight]}>{this.props.offer.trip_start + " - " + this.props.offer.trip_end}</Text>
            </View>
            {this.props.offer.description ?
                <View style={[style.tableRow, { borderBottomWidth: 0 , paddingHorizontal: tableRowPaddingHorizontal}]}>
                    <View><APIcon size={24} color={"#7cb342"} name="insert_drive_file" /></View>
                    <View style={{ flex: 1 }}>
                        <Text style={[style.tableRowLabel, globalStyles().fontBold]}>{translate["description"][this.props.lang]}</Text>
                        <Text style={[style.tableRowValue, globalStyles().fontLight]}>{urlify(this.props.offer.description)}</Text>
                    </View>
                </View> : null}
        </View>)

        const rate = parseInt(this.props.offer.agency.average_rate);

        return (
            <AnimatedView
                testID={this.props.testID}
                screenComponent={this}
                onBack={this.onBack}
                photo={this.props.offer.photo}
                icon="phone"
                onIconClick={this.call}
                showIconOnHeader={true}
                title={this.props.offer.title}
                description={this.props.offer.agency.tour_agent}
                badgeWidth={rate ? 60 : 0}
                renderBadge={this.renderRate}
                boxTitle={this.props.offer.price + " " + translate['amd'][this.props.lang]}
                boxDescription={this.props.offer.days_left == 0 ? translate['expiring-today'][this.props.lang] : translate['valid-for'][this.props.lang].replace('%days%', this.props.offer.days_left)}
            >
                <View style={style.content}>
                    {table}
                </View>
            </AnimatedView>
        )
    }
}

const style = StyleSheet.create({

    content: {
        paddingBottom: 24
    },

    tour_agent: {
        color: "#424242",
        fontSize: 18,
        paddingBottom: 16
    },

    tour_agent_logo: {
        height: 60,
        resizeMode: "cover",
        width: 60,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: "#d0d0d0"
    },

    tableRow: {
        flexDirection: "row",
        paddingVertical: 15,
        paddingHorizontal: 20,
        justifyContent: "flex-start",
        borderBottomWidth: 1,
        borderBottomColor: "#d0d0d0",
        width: "100%"
    },

    tableRowLabel: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 24,
        paddingLeft: 20,
        paddingBottom: 8
    },

    tableRowValue: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 24,
        paddingLeft: 20,
        width: "100%"
    },

    green_text: {
        color: "#7cb342"
    },

    rate: {
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 12
    },

    headerTextDesc: {
        fontSize: 16,
        color: "#fff"
    }
})

const mapStateToProps = (state) => {
    return {
        iphoneXSpace: state.index.iphoneXSpace,
        prevPage: state.index.prevPage
    }
}

export default connect(mapStateToProps, null)(OfferScreen)
