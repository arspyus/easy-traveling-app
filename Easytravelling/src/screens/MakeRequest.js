import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, Dimensions, Keyboard, TouchableWithoutFeedback, UIManager, findNodeHandle, Platform } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APKeyboardAvoidingView from '../ui/keyboardAvoidingView';
import APSection from '../ui/section';
import APTextField from '../ui/textField';
import APAutocomplete from '../ui/autocomplete';
import APRangeSlider from '../ui/rangeSlider';
import APDateRangePicker from '../ui/dateRangePicker';
import APStarSelect from '../ui/starSelect';
import APTags from '../ui/tags';
import Collapsible from '../ui/collapsible';
import APCheckList from '../ui/checkList';
import APSwitch from '../ui/switch';
import APFloatingButton from '../ui/floatingButton';

import Travelers from '../components/travelers';


import translate from '../utils/translate';
import validate from '../utils/validate';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import globalStyles from '../utils/style';
import countries from '../utils/countries';
import formatDate from '../utils/formatDate';
import navigate from '../utils/navigate';
import capitalize from '../utils/capitalize'
import config from '../config/config';

import { setMessage } from '../store/actions/indexAction';
import { make_request, select_type } from '../store/actions/makeRequestAction';

class MakeRequestScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    state = {
        _scrollView: null,
        previouseLoaded:false,
        form: {
            country: {
                value: this.props.tour_type === "domestic-tour" ? "Armenia and Artsakh" : "",
                isValid: true,
                validationRules: [
                    { type: "required" },
                    { type: "country" }
                ],
            },
            city: {
                value: "",
                isValid: true
            },
            from: {
                value: "",
                validationRules: [
                    { type: "required" }
                ],
                isValid: true,
                isEmpty: false
            },
            to: {
                value: "",
                validationRules: [
                    { type: "required" }
                ],
                isValid: true,
                isEmpty: false
            },
            nights: {
                value: [1, 5],
                isValid: true
            },
            adults: {
                isValid: true,
                validationRules: [
                    { type: "number" }
                ],
                value: 1
            },
            children: {
                isValid: true,
                value: ""
            },
            infant: {
                isValid: true,
                value: ""
            },
            childrenAges: {
                value: [],
                isValid: true
            },
            price: {
                value: [200000, 2000000],
                validationRules: [
                    { type: "required" }
                ],
                isValid: true
            },
            "hotel-category": {
                isValid: true,
                value: []
            },
            desired_hotels: {
                value: [],
                isValid: true
            },
            rooms: {
                isValid: true,
                value: ""
            },
            "board-type": {
                isValid: true,
                value: "",
                options: [
                    { text: translate["meals-included"][this.props.lang], value: "" },
                    { text: translate["room-only"][this.props.lang], value: "room-only" },
                    { text: translate["breakfast"][this.props.lang], value: "breakfast" },
                    { text: translate["half-board"][this.props.lang], value: "half-board", desc: translate["half-board-desc"][this.props.lang] },
                    { text: translate["full-board"][this.props.lang], value: "full-board", desc: translate["full-board-desc"][this.props.lang] },
                    { text: translate["all-inclusive"][this.props.lang], value: "all-inclusive", desc: translate["all-inclusive-desc"][this.props.lang] }
                ]
            },
            "extra": {
                isValid: true,
                value: "",
                options: [
                    { text: translate["choose-extra-options"][this.props.lang], value: "" },
                    { text: translate["city-holiday"][this.props.lang], value: "city-holiday" },
                    { text: translate["beach-holidays"][this.props.lang], value: "beach-holidays" },
                    { text: translate["transfer"][this.props.lang], value: "transfer" },
                    { text: translate["pool"][this.props.lang], value: "pool" },
                    { text: translate["1st-line"][this.props.lang], value: "1st-line" },
                    { text: translate["2st-line"][this.props.lang], value: "2st-line" },
                    { text: translate["internet"][this.props.lang], value: "internet" },
                    { text: translate["parking"][this.props.lang], value: "parking" },
                    { text: translate["gym"][this.props.lang], value: "gym" },
                    { text: translate["childrens-room"][this.props.lang], value: "childrens-room" },
                    { text: translate["gid-service"][this.props.lang], value: "gid-service" }
                ]
            },
            "social_package": {
                isValid: true,
                value: false
            },
            "notes": {
                value: "",
                isValid: true
            }
        },
        showAutocomplete: false,
        autocompleteType: "",
        autocompleteData: [],
        tour_type: this.props.tour_type ? this.props.tour_type : "outgoing-tour",
        showButton: false
    }

    screenName = "makerequest";

    componentWillReceiveProps(nextProps) {

        if (!this.props.makeRequestProcess && nextProps.makeRequestProcess) {
            navigate(this, "MyRequestsScreen", "resetTo", null);
        }
    }


    calculateDateRange = () => {
        if (!this.state.form.to.value) return null;
        let d1 = this.state.form.from.value ? new Date(this.state.form.from.value) : new Date();
        let d2 = new Date(this.state.form.to.value);
        let diff = d2 - d1;
        let days = Math.ceil(diff / 3600 / 1000 / 24);
        return days;
    }

    onChange = (value, field) => {
        let form;
        if (field === "dateRange") {
            form = {
                ... this.state.form,
                from: {
                    ...this.state.form.from,
                    value: value.from || "",
                    isEmpty: false
                },
                to: {
                    ...this.state.form.to,
                    value: value.to || "",
                    isEmpty: false
                }
            }
        }  else if (field === "nights") {
            form = {
                ... this.state.form,
                nights: {
                    ...this.state.form.nights,
                    value: value
                },
                from: {
                    ...this.state.form.from,
                    value: "",
                    isEmpty: true
                },
                to: {
                    ...this.state.form.to,
                    value: "",
                    isEmpty: true
                }
            }
        } else {
            form = {
                ... this.state.form,
                [field]: {
                    ... this.state.form[field],
                    value: value
                }
            }
        }

        this.setState({
            form: form
        }, () => {
            if (field === "dateRange") {
                this.updateValidationState(() => { }, false, value.type)
            }
        })
    }

    valueToPrice = (value) => {
        const values = [0, 200000, 500000, 1000000, 1500000, 2000000, ">2000000"];
        return values[value]
    }

    openAutocomplete = (type) => {
        if (type === "country" && this.state.tour_type === "domestic-tour") return;
        this.setState({
            showAutocomplete: true,
            autocompleteType: type,
            autocompleteData: []
        })
    }

    closeAutocomplete = () => {
        this.setState({
            showAutocomplete: false
        })
    }

    onAutocompleteChange = (text) => {
        this.getAutocompleteData(text);
    }

    onAutocompleteSelect = (value) => {
        Keyboard.dismiss();
        let selectedData = {};
        if (this.state.autocompleteType === "country") {
            selectedData = {
                country: {
                    ...this.state.form.country,
                    value: value
                },
                city: {
                    ...this.state.form.city,
                    value: ""
                },
                desired_hotels: {
                    ...this.state.form.desired_hotels,
                    value: []
                }
            }

        } else if (this.state.autocompleteType === "city") {
            selectedData = {
                city: {
                    ...this.state.form.city,
                    value: value
                },
                desired_hotels: {
                    ...this.state.form.desired_hotels,
                    value: []
                }
            }
        } else if (this.state.autocompleteType === "hotel") {
            selectedData = {
                desired_hotels: {
                    ...this.state.form.desired_hotels,
                    value: [value, ...this.state.form.desired_hotels.value].filter((value, index, self) => {
                        return self.indexOf(value) === index;
                    })
                }
            }
        }

        this.setState({
            ...this.state,
            showAutocomplete: false,
            form: {
                ...this.state.form,
                ...selectedData
            }
        }, () => {
            if (this.state.autocompleteType === "country") {
                this.updateValidationState(() => { }, false, "country")
            }

        })
    }

    setCountry = (country, cb) => {
        let selectedData = {
            country: {
                ...this.state.form.country,
                value: country
            },
            city: {
                ...this.state.form.city,
                value: ""
            },
            desired_hotels: {
                ...this.state.form.desired_hotels,
                value: []
            }
        };

        this.setState({
            ...this.state,
            showAutocomplete: false,
            form: {
                ...this.state.form,
                ...selectedData
            }
        }, () => {
            cb()
        })
    }

    getAutocompleteData = (text) => {
        let data = [];
        text = text.trim().toLowerCase();
        if (text) {
            if (this.state.autocompleteType === "country") {
                this.getCountries(text);
            } else if (this.state.autocompleteType === "city") {
                this.getPlaces(text, "city");
            } else if (this.state.autocompleteType === "hotel") {
                this.getPlaces(text, "hotel");
            }
        } else {
            this.setState({
                autocompleteData: data
            })
        }
    }

    getCountries = (text) => {
        let data = [];
        data = countries.filter(country => {
            return country.text.toLowerCase().indexOf(text) > -1 && (this.state.tour_type !== "outgoing-tour" || country.code !== "am");
        }).map(country => {
            return {
                icon: "language",
                title: country.text,
                description: "",
                value: country.text
            }
        });

        this.setState({
            autocompleteData: data
        })
    }

    getPlaces = (text, type) => {
        let country_code = this.getCountryCode()
        let components = '';
        if (country_code) {
            components = '&components=country:' + country_code;
        }
        const searchType = type === "city" ? "(cities)" : "establishment"

        const url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + text.toLowerCase() + '&types=' + searchType + components + '&key=' + config.google_places_api_key;

        fetch(url, {
            method: "GET"
        }).then(res => res.json()).then(resp => {
            let data = []
            if (resp.predictions) {
                data = resp.predictions.map(pred => {
                    return {
                        icon: type === "city" ? "location_on" : "domain",
                        title: pred.structured_formatting.main_text,
                        value: pred.structured_formatting.main_text,
                        description: "",
                    }
                })
            }
            this.setState({
                autocompleteData: data
            });
        }).catch(err => {
        })
    }

    getCountryCode = () => {
        let country = this.state.form.country.value;
        let country_code = null;
        if (this.state.tour_type === "domestic-tour") {
            country_code = "am";
        } else {
            let countryObj = countries.filter(el => {
                return el.text === country
            })[0];
            country_code = countryObj ? countryObj.code : null;
        }

        return country_code;
    }

    removeDesiredHotel = (hotel) => {
        this.setState({
            form: {
                ...this.state.form,
                desired_hotels: {
                    ...this.state.form.desired_hotels,
                    value: this.state.form.desired_hotels.value.filter(el => el !== hotel)
                }
            }
        });
    }

    ontourTypeChange = (index, tour_type) => {
        this.setState({
            tour_type: tour_type
        }, () => {
            this.setCountry(this.state.tour_type === "outgoing-tour" ? "" : "Armenia and Artsakh", () => {
                this.updateValidationState(() => { }, true)
            });
            this.props.selectType(tour_type);

        })
    }

    onTravelersChange = (travelers) => {
        let childrenCount = 0;
        let infantCount = 0;
        travelers.children_ages.forEach(age => {
            if (age < 3) infantCount++;
            else childrenCount++;
        })
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                adults: {
                    ...this.state.form.adults,
                    value: travelers.adults
                },
                children: {
                    ...this.state.form.children,
                    value: childrenCount
                },
                infant: {
                    ...this.state.form.infant,
                    value: infantCount
                },
                childrenAges: {
                    ...this.state.form.childrenAges,
                    value: travelers.children_ages
                }
            }
        })
    }

    onScroll = (event) => {
        const vH = Dimensions.get("window").height;
        const sH = event.nativeEvent.contentSize.height * 4 / 5;
        const diff = sH - vH;
        const offset = event.nativeEvent.contentOffset.y;
        if (offset > diff && !this.state.showButton || offset <= diff && this.state.showButton) {
            this.setState({
                showButton: !this.state.showButton
            })
        }
    }

    makeRequest = () => {
        Keyboard.dismiss();

        this.updateValidationState(() => {
            if (this.isFormValid()) {
                let nights = this.state.form.nights.value instanceof Array ? this.state.form.nights.value[1] : this.state.form.nights.value;
                const data = {
                    country: this.state.tour_type === "domestic-tour" ? "armenia" : this.state.form.country.value.toLowerCase(),
                    country_code: this.getCountryCode(),
                    city: this.state.form.city.value || "",
                    'period-from': this.state.form.from.value,
                    'period-to': this.state.form.to.value,
                    nights: [Math.max(1, nights - 3), Math.min(nights + 3, 30)].toString(),
                    adults: this.state.form.adults.value,
                    children: this.state.form.children.value,
                    children_ages: this.state.form.childrenAges.value ? JSON.stringify(this.state.form.childrenAges.value) : JSON.stringify([]),
                    infant: this.state.form.infant.value,
                    rooms: this.state.form.rooms.value,
                    "hotel-category": this.state.form["hotel-category"].value.toString(),
                    "board-type": this.state.form["board-type"].value ? JSON.stringify(this.state.form["board-type"].value.split(",")) : this.state.form["board-type"].value,
                    extra: this.state.form.extra.value ? JSON.stringify(this.state.form.extra.value.split(",")) : this.state.form.extra.value,
                    notes: this.state.form.notes.value,
                    desired_hotels: this.state.form.desired_hotels.value ? JSON.stringify(this.state.form.desired_hotels.value) : this.state.form.desired_hotels.value,
                    price: this.state.form.price.value.map(el => el === ">2000000" ? -1 : el).toString(),
                    social_package: false
                }

                if (this.state.tour_type === "domestic-tour") {
                    data.social_package = this.state.form.social_package.value
                }
                this.props.makeRequest(data);
            } else {
                this.props.showError("fill-form");
            }
        });
    }

    updateValidationState = (cb, valid, s_field) => {
        let obj = { ...this.state.form };
        for (let field in this.state.form) {
            if (this.state.form[field].validationRules && (!s_field || field === s_field)) {
                obj[field].isValid = valid ? true : validate(this.state.form[field].validationRules, this.state.form[field].value);
            }
        }
        if (!s_field) {
            let isChildrenAgesValid = true;
            for (let i = 0; i < this.state.form.childrenAges.value.length; i++) {
                if (typeof this.state.form.childrenAges.value[i] === "undefined" || this.state.form.childrenAges.value[i] === "-" || parseInt(this.state.form.childrenAges.value[i]) > 12) {
                    isChildrenAgesValid = false;
                    break;
                }
            }
            obj['childrenAges'].isValid = isChildrenAgesValid;
        }


        this.setState({
            ...this.state,
            form: obj
        }, cb)
    }

    isFormValid = () => {
        let isValid = true;
        for (let field in this.state.form) {
            if (!this.state.form[field].isValid) {
              console.log(field)
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    scrollTo = (type) => {
      if(type === "notes"){
        this.setState({previouseLoaded:!this.state.previouseLoaded})
        this.state._scrollView.scrollToEnd()
      }else if(!this.state.previouseLoaded){
        let elem = type === "extra" ? findNodeHandle(this._extra) : findNodeHandle(this._notes);
        let headerH = Platform.OS === "android" ? 70 : 70 + this.props.ios_toolbar_height
        UIManager.measure(
            elem,
            (x, y, w, h, pageX, pageY) => {
                let pos = type === "extra" ? pageY + Dimensions.get("window").height + headerH : pageY + Dimensions.get("window").height + headerH;
                console.log("position",pos)
                this.state._scrollView.scrollTo({ x: 0, y: pos, animated: true })
            });
          }else if(this.state.previouseLoaded && type === "extra"){

            let elem = findNodeHandle(this._extra)
            UIManager.measure(
                elem,
                (x, y, w, h, pageX, pageY) => {
                    let pos =pageY + Dimensions.get("window").height;
                    this.state._scrollView.scrollTo({ x: 0, y: pos, animated: true })
                });
          }
    }

    render() {

        return (
            <APContainer
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })}
                title={translate["make-request"][this.props.lang]}
                screenId={this.props.testID}
                screenComponent={this}
                tourTypeMenu={true}
                tourTypeValue={this.state.tour_type}
                ontourTypeChange={this.ontourTypeChange}
            >
                <APKeyboardAvoidingView>
                    <ScrollView
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}
                        alwaysBounceVertical={false}
                        bounces={false}
                        ref={(ref) => { !this.state._scrollView && this.setState({ _scrollView: ref }) }}
                        onScroll={this.onScroll}
                    >

                        <APWrapper>
                            <APSection >
                                <View style={globalStyles().section}>
                                    <View>
                                        <Text style={[style.sectionTitle, globalStyles().title, globalStyles().fontBold]}>{translate["destination"][this.props.lang]}</Text>
                                    </View>
                                    {
                                        this.state.tour_type === "domestic-tour" ?
                                            <View style={{ marginBottom: 18 }}>
                                                <Text style={[globalStyles().text, { paddingBottom: 0 }, globalStyles().fontLight]}>{translate["armenia-and-karabakh"][this.props.lang]}</Text>
                                            </View> :
                                            null
                                    }
                                    <View style={style.row}>
                                        {
                                            this.state.tour_type === "outgoing-tour" ?

                                                <TouchableWithoutFeedback onPress={() => this.openAutocomplete("country")}>
                                                    <View style={[style.destination, { marginRight: 10 }]}>
                                                        <View style={[style.textField, !this.state.form.country.isValid ? style.textFieldError : null]}>
                                                            <Text style={[globalStyles().text, globalStyles().fontLight, !this.state.form.country.isValid ? style.textFieldErrorText : null]} numberOfLines={1}>{this.state.form.country.value || translate["country"][this.props.lang]}</Text>
                                                        </View>
                                                    </View>
                                                </TouchableWithoutFeedback> :
                                                null
                                        }
                                        <TouchableWithoutFeedback onPress={() => this.openAutocomplete("city")}>
                                            <View style={style.destination}>
                                                <View style={[style.textField, !this.state.form.city.isValid ? style.textFieldError : null]}>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, !this.state.form.city.isValid ? style.textFieldErrorText : null]} numberOfLines={1}>{this.state.form.city.value || translate["city"][this.props.lang]}</Text>
                                                </View>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                                <View style={globalStyles().section}>
                                    <View>
                                        <Text style={[style.sectionTitle, globalStyles().title, globalStyles().fontBold]}>{translate["how_long_to_stay"][this.props.lang]}</Text>
                                    </View>
                                    <View>
                                        <Text style={[globalStyles().text, { paddingBottom: 0 }, globalStyles().fontLight]}>{translate["nights"][this.props.lang]}</Text>
                                    </View>
                                    <APRangeSlider
                                        step={1}
                                        from={1}
                                        to={30}
                                        valueSuffix={value => value === 1 ? capitalize(translate["single-night"][this.props.lang]) : capitalize(translate["night"][this.props.lang])}
                                        values={this.state.form.nights.value}
                                        onChange={(value) => { this.onChange(value, "nights") }}
                                        single={true}
                                        valueFormatter={this.valueToNights}
                                    />
                                    <View>
                                        <Text style={[globalStyles().text, globalStyles().fontLight]}>{translate["date_range"][this.props.lang]}</Text>
                                    </View>
                                    <View>
                                        <APDateRangePicker
                                            labelFrom={translate["from"][this.props.lang]}
                                            labelTo={translate["to"][this.props.lang]}
                                            isValid={{ from: this.state.form.from.isValid, to: this.state.form.to.isValid }}
                                            onChange={(value) => { this.onChange(value, "dateRange") }}
                                            range={this.state.form.nights.value instanceof Array ? Math.max(1, this.state.form.nights.value[1] - 3) : Math.max(1, this.state.form.nights.value - 3)}
                                            lang={this.props.lang}
                                            isEmpty={this.state.form.from.isEmpty}
                                        />
                                    </View>
                                </View>
                                <View style={[globalStyles().section, { paddingBottom: 8 }]}>
                                    <Travelers
                                        onChange={this.onTravelersChange}
                                    />
                                </View>
                                <View style={globalStyles().section}>
                                    <View>
                                        <Text style={[style.sectionTitle, globalStyles().title, globalStyles().fontBold]}>{translate["total_price"][this.props.lang]}</Text>
                                    </View>
                                    <View>
                                        <Text style={[globalStyles().text, { paddingBottom: 0 }, globalStyles().fontLight]}>{translate["total_price_info"][this.props.lang]}</Text>
                                    </View>
                                    <APRangeSlider
                                        step={1} from={0} to={6} valueSuffix={() => translate['amd'][this.props.lang]}
                                        values={[1, 5]}
                                        onChange={(value) => { this.onChange(value, "price") }}
                                        valueFormatter={this.valueToPrice}
                                        showSteps={true}
                                        stepsTextFormatter={(value) => value === ">2000000" ? "∞" : value === 0 ? value : value / 1000 + "K"}
                                    />
                                </View>
                                <View style={globalStyles().section}>
                                    <View>
                                        <Text style={[style.sectionTitle, globalStyles().title, globalStyles().fontBold]}>{translate["hotel-rating"][this.props.lang]}</Text>
                                    </View>
                                    <View>
                                        <Text style={[globalStyles().text, { paddingBottom: 0 }, globalStyles().fontLight]}>{translate["hotel-rating-info"][this.props.lang]}</Text>
                                    </View>
                                    <APStarSelect onChange={(value) => { this.onChange(value, "hotel-category") }} />
                                    <View>
                                        <Text style={[globalStyles().title, globalStyles().fontBold]}>{translate["desired_hotels"][this.props.lang]}</Text>
                                    </View>
                                    <APTags
                                        tags={this.state.form.desired_hotels.value}
                                        onRemove={(tag) => { this.removeDesiredHotel(tag) }}
                                        onClick={() => this.openAutocomplete("hotel")}
                                        placeholder={translate["hotel_name"][this.props.lang]}
                                    />
                                </View>
                                <APAutocomplete
                                    isVisible={this.state.showAutocomplete}
                                    onClose={this.closeAutocomplete}
                                    placeholder={translate["search"][this.props.lang] + " " + (this.state.autocompleteType === "country" ? translate["country-form"][this.props.lang].toLowerCase() : this.state.autocompleteType === "city" ? translate["city-form"][this.props.lang].toLowerCase() : translate["hotel"][this.props.lang].toLowerCase())}
                                    data={this.state.autocompleteData}
                                    onChange={this.onAutocompleteChange}
                                    onSelect={this.onAutocompleteSelect}
                                    restrictWithData={this.state.autocompleteType === "country"}
                                    google_logo={this.state.autocompleteType !== "country"}
                                />
                            </APSection>
                            <View style={[style.collapsible, globalStyles().shadow]} ref={(ref) => { this._extra = ref }}>
                                <Collapsible
                                    title={translate["request-general"][this.props.lang]}
                                 onAnimationEnd={() => { this.scrollTo("extra")}}
                                >
                                    <View style={style.collapsible_content}>
                                        <View style={style.collapsible_row}>
                                            <Text style={[style.collapsible_title, globalStyles().text, globalStyles().fontBold]}>{translate["rooms"][this.props.lang]}</Text>
                                            <APRangeSlider
                                                step={1} from={1} to={7}
                                                values={[1, 1]}
                                                onChange={(value) => { this.onChange(value, "rooms") }}
                                                showSteps={true}
                                                stepsTextFormatter={value => value === 7 ? "7+" : value}
                                                single={true}
                                                valueSuffix={value => value === 1 ? capitalize(translate["room_offer"][this.props.lang]) : capitalize(translate["rooms_offer"][this.props.lang])}
                                                type="rooms"
                                            />
                                        </View>
                                        <View style={style.collapsible_row}>
                                            <Text style={[style.collapsible_title, globalStyles().text, globalStyles().fontBold]}>{translate["board-type"][this.props.lang]}</Text>
                                            <APCheckList
                                                options={this.state.form["board-type"].options}
                                                onChangeText={(text) => { this.onChange(text, "board-type") }}
                                                disabledOptions={[0]}
                                            />
                                        </View>
                                        <View style={style.collapsible_row}>
                                            <Text style={[style.collapsible_title, globalStyles().text, globalStyles().fontBold]}>{translate["additional_options"][this.props.lang]}</Text>
                                            <APCheckList
                                                options={this.state.form["extra"].options}
                                                onChangeText={(text) => { this.onChange(text, "extra") }}
                                                disabledOptions={[0]}
                                            />
                                        </View>
                                        {
                                            this.state.tour_type === "domestic-tour" ?

                                                <View style={[style.collapsible_row, { marginBottom: 20 }]}>
                                                    <APSwitch
                                                        label={translate["social_package"][this.props.lang]}
                                                        onValueChange={(text) => { this.onChange(text, "social_package") }}
                                                        value={this.state.form["social_package"].value}
                                                    />
                                                </View> :
                                                null
                                        }
                                    </View>
                                </Collapsible>
                            </View>
                            <View style={[style.collapsible, globalStyles().shadow, { marginBottom: 74 }]} ref={(ref) => { this._notes = ref }}>
                                <Collapsible
                                    title={translate["notes"][this.props.lang]}
                                    onAnimationEnd={() => { this.scrollTo("notes") }}
                                >
                                    <View style={style.collapsible_content}>
                                        <View style={style.collapsible_row}>
                                            <APTextField
                                                placeholder={translate["notes_placeholder"][this.props.lang]}
                                                value={this.state.form.notes.value}
                                                minHeight={122}
                                                multiline={true}
                                                onChangeText={(value) => { this.onChange(value, "notes") }}
                                                _scrollView={this.state._scrollView}
                                            />
                                        </View>
                                    </View>
                                </Collapsible>
                            </View>
                        </APWrapper>
                    </ScrollView>
                </APKeyboardAvoidingView>
                <APFloatingButton
                    title={this.state.form.city.value ? this.state.form.city.value : this.state.form.country.value ? this.state.form.country.value : ""}
                    description={this.state.form.from.value && this.state.form.to.value ? formatDate(this.state.form.from.value) + " - " + formatDate(this.state.form.to.value) : ""}
                    buttonText={translate["make-request"][this.props.lang]}
                    isVisible={this.state.showButton}
                    onClick={this.makeRequest}
                />
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5"
    },

    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    destination: {
        flex: 1
    },

    sectionTitle: {
        paddingBottom: 5
    },

    collapsible: {
        borderWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#ddd",
        marginBottom: 10,
        borderRadius: 10,
        overflow: "hidden"
    },

    collapsible_content: {
        paddingHorizontal: 18,
        backgroundColor: "#fff",
        borderTopWidth: 1,
        borderTopColor: "rgba(188,188,188, .5)"
    },

    collapsible_title: {
        paddingBottom: 5
    },

    collapsible_row: {
        paddingVertical: 15
    },

    textField: {
        borderWidth: 1,
        borderColor: "#bcbcbc",
        borderRadius: 5,
        paddingHorizontal: 15,
        paddingVertical: 10,
        paddingBottom: 10
    },

    textFieldError: {
        borderColor: "#f44336"
    },
    textFieldErrorText: {
        color: "#f44336"
    },
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        tour_type: state.makeRequest.type,
        makeRequestProcess: state.makeRequest.makeRequestProcess,
        ios_toolbar_height: state.index.ios_toolbar_height
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        showError: (message) => { dispatch(setMessage(true, message)) },
        makeRequest: (data) => { dispatch(make_request(data)) },
        selectType: (type) => { dispatch(select_type(type)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MakeRequestScreen)
