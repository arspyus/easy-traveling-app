import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APButton from '../ui/button';

import TableRow from '../components/tableRow';

import translate from '../utils/translate';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';
import urlify from '../utils/urlify';
import capitalize from '../utils/capitalize';

import {get_offer, set_offer_seen} from '../store/actions/singleAction';

class SingleOfferScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    willAppear() {
        if(this.props.prevPage !== "easytraveling.PrebookScreen"){
            this.props.getOffer(this.props.offer_id);
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.offer != nextProps.offer && nextProps.offer){
            if(nextProps.offer.seen_status === "new"){
               this.props.setOfferSeen(nextProps.offer.offer_id)
            }
        }
    }

    onBack = () => {
        navigate(this, null, "pop");
    }

    preBook = () => {
        navigate(this, "PrebookScreen", "push", {
            offer_id: this.props.offer.offer_id,
            request_id: this.props.offer.request_id,
            offer_author: this.props.offer.offer_author,
            adults: parseInt(this.props.offer.adults),
            children: parseInt(this.props.offer.children),
            infant: parseInt(this.props.offer.infant)
        });

    }

    render() {
        let valid_until = null;
        let expiredOffer = false;
        if(this.props.offer){
            if(this.props.offer.days_left == -1){
                expiredOffer = true;
            }
            if(expiredOffer && this.props.offer.offer_status !== 'booked'){
                valid_until = <Text style={[style.text, globalStyles().fontLight, {color: "#EF5350"}]}>{translate["expired"][this.props.lang]}</Text>;
            } else if(this.props.offer.offer_status !== 'booked'){
                valid_until = <Text style={[style.text, globalStyles().fontLight, {color: "#7cb342"}]}>{this.props.offer.days_left === 0 ? translate["expiring-today"][this.props.lang] : translate["valid-for"][this.props.lang].replace('%days%', this.props.offer.days_left)}</Text>;
            } else {
                valid_until = <Text></Text> 
            }
        }
        
        return (
            <APContainer 
                back={true} 
                onBack={this.onBack} 
                title={this.props.offer ? translate["offer"][this.props.lang] + " #" + this.props.offer.offer_id : " "}
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })}
                screenId={this.props.testID}
                reverse={true}
                noMenu={true}
                screenComponent={this}
            >
                <ScrollView 
                    showsVerticalScrollIndicator={false}
                    ref= {(ref) => {this._scrollView = ref}}  
                    alwaysBounceVertical={false}
                    bounces={false}
                >
                    {this.props.offer ? (
                        <View >
                            <APWrapper>
                                <View>
                                    <View style={style.header}>
                                        {valid_until}
                                        {this.props.offer.badges && this.props.offer.badges[0] ?
                                            <View style={style.badge}>
                                                <APIcon name={this.props.offer.badges[0]} size={23} color="#fff" />
                                            </View> : null}
                                    </View>
                                    <TableRow
                                        label={translate["tour-agent"][this.props.lang]}
                                        text={capitalize(this.props.offer.tour_agent)}
                                    />
                                    <TableRow
                                        label={translate["price"][this.props.lang]}
                                        text={this.props.offer.price + " " + translate["amd"][this.props.lang]}
                                    />
                                    <TableRow
                                        label={translate["nights"][this.props.lang]}
                                        text={this.props.offer.nights + "(" + translate["days"][this.props.lang] + " - " + (parseInt(this.props.offer.nights) + 1) + ")"}
                                    />
                                    <TableRow
                                        label={translate["board-type"][this.props.lang]}
                                        text={this.props.offer.board_type ? translate[this.props.offer.board_type][this.props.lang] : translate['not-specified'][this.props.lang]}
                                    />
                                    {this.props.offer.flight_type ? (
                                        <View>
                                            <TableRow
                                                label={translate["flight-type"][this.props.lang]}
                                                text={translate[this.props.offer.flight_type][this.props.lang]}
                                            />
                                            <TableRow
                                                label={translate["departure"][this.props.lang]}
                                                text={this.props.offer.departure}
                                            />
                                            <TableRow
                                                label={translate["arrival"][this.props.lang]}
                                                text={this.props.offer.arrival}
                                            />
                                        </View>
                                    ) : null}
                                    <TableRow
                                        label={translate["hotel"][this.props.lang]}
                                        text={this.props.offer.hotel}
                                    />
                                    <TableRow
                                        label={translate["hotel-check-in"][this.props.lang]}
                                        text={this.props.offer.hotel_checkin}
                                    />
                                    <TableRow
                                        label={translate["hotel-check-out"][this.props.lang]}
                                        text={this.props.offer.hotel_checkout}
                                    />
                                    {this.props.offer.doc_list ? 
                                        <TableRow
                                            label={translate["doc-list"][this.props.lang]}
                                            text={this.props.offer.doc_list.join("\n")}
                                        /> : null }
                                    <TableRow
                                        label={translate["doc-deadline"][this.props.lang]}
                                        text={this.props.offer.doc_deadline}
                                    />
                                    {this.props.offer.notes ? (
                                        <View>
                                            <TableRow
                                                label={translate["notes"][this.props.lang]}
                                                text={urlify(this.props.offer.notes)}
                                            />
                                        </View>
                                    ) : null}
                                </View>
                                {!expiredOffer && this.props.offer.offer_status !== 'booked'? 
                                (<View>
                                    <View style={style.notes}>
                                        <Text style={[style.notesText, globalStyles().fontLight]}>{translate["pre-book-notes"][this.props.lang]}</Text>
                                    </View>
                                    <View style={style.buttonContainer}>
                                        <APButton text={translate["pre-book"][this.props.lang]} color="#fff" backgroundColor="#7cb342" icon="send" onPress={this.preBook} />
                                    </View>
                                </View>) : null}
                                
                            </APWrapper>
                        </View>) : null
                    }
                </ScrollView>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    text: {
        fontSize: 14,
        lineHeight: 22,
        paddingLeft: 5
    },
    notes: {
        marginTop: 16
    },

    notesText: {
        fontSize: 10,
        color: "#424242",
        lineHeight: 15
    },

    buttonContainer: {
        marginTop: 20,
        marginBottom: 14,
        flexDirection: "row",
        justifyContent: "flex-start"
    },

    badge: {
        width: 35,
        height: 35,
        borderRadius: 17.5,
        backgroundColor: "#ffa726",
        alignItems: "center",
        justifyContent: "center"
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        offer: state.single.offer,
        prevPage: state.index.prevPage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
       getOffer : (id) => {dispatch(get_offer(id))},
       setOfferSeen: (id) => {dispatch(set_offer_seen(id))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleOfferScreen)
