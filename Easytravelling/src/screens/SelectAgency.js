import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text, Image,TouchableWithoutFeedback, Platform } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APSpinner from '../ui/spinner';
import APCard from '../ui/card';
import APNothingFound from '../ui/nothingFound';
import APIcon from '../ui/icon';

import translate from '../utils/translate';
import isReachedToBottom from '../utils/isReachedToBottom';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';

import { get_agencies, reset_agencies } from '../store/actions/agencyAction';
import { select_agency, select_last_agency } from '../store/actions/makeRequestAction';

class SelectAgencyScreen extends Component {


    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    willAppear() {
        this.props.resetAgencies();
        this.props.getAgencies();
    }

    onSelect = (agency) => {
        this.props.selectAgency({
            id: agency.id,
            title: agency.tour_agent
        })
        this.props.selectLastAgency({
            id: agency.id,
            title: agency.tour_agent
        })
        navigate(this, null, "pop");
    }

    scrollTimer = null;

    onScroll = ($event) => {
        clearTimeout(this.scrollTimer)
        let ev = $event.nativeEvent;
        this.scrollTimer = setTimeout(() => {
            if(isReachedToBottom(ev) && !this.props.agencies_loading){
                this.props.getAgencies();
            }
        }, 100)
        
    }


    renderCard = (agency, index) => {
        let rate = parseInt(agency.average_rate);
        return <APCard key={agency.id} style={this.props.agencies.length - 1 === index ? {marginBottom : Platform.OS === "android" ? 88 : 88 + this.props.ios_toolbar_height} : null}>
            <TouchableWithoutFeedback onPress={() => this.onSelect(agency)}>
                <View>
                    <View style={style.content}>
                        <Image style={style.image} source={{ uri: agency.photo }} >
                        </Image>
                        <View style={style.contentRight}>
                            <View>
                                <View style={style.titleWrap}>
                                    <Text style={[style.title, globalStyles().fontLight]}>{agency.tour_agent}</Text>
                                </View>
                                <View style={style.bottom}>
                                    {
                                        rate > 0 ?
                                            <View style={style.starContainer}>
                                                <APIcon size={24} name={rate > 0 ? "star" : "star_border"} color={"#7cb342"} />
                                            </View> : null
                                    }
                                    {
                                        rate > 0 ?
                                            <View style={style.rateContainer}>
                                                <Text style={[style.rate, globalStyles().fontLight]}>{agency.average_rate}</Text>
                                            </View> : null
                                    }
                                    {agency.badges && agency.badges[0] ? <View style={style.cup}><APIcon name={agency.badges[0]} size={14} color="#fff" /></View> : null}
                                </View>
                            </View>
                            <View>
                                <APIcon color="#aaa" size={24} name="chevron_right" />
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>

        </APCard>
    }

    render() {
        let agencies = null;
        if (this.props.agencies) {

            if (this.props.agencies.length > 0) {
                agencies = <FlatList 
                    data={this.props.agencies}
                    renderItem={({item, index}) => this.renderCard(item, index) }
                    keyboardShouldPersistTaps="always" 
                    onScroll={this.onScroll} 
                    showsVerticalScrollIndicator={false} 
                    alwaysBounceVertical={false}
                    bounces={false}
                    removeClippedSubviews={true}
                    contentContainerStyle={{paddingVertical: 20}}
                    keyExtractor={(item, index) => item.id }
                />
            } else {
                agencies = <FlatList 
                    data={[1]}
                    renderItem={({item, index}) => <APNothingFound text={translate["no-agencies-found"][this.props.lang]} /> }
                    showsVerticalScrollIndicator={false} 
                    alwaysBounceVertical={false}
                    bounces={false}
                    removeClippedSubviews={true}
                    contentContainerStyle={{paddingVertical: 20, paddingTop:70}}
                    keyExtractor={(item, index) => index}

                />
            }
        }

        return (
            <APContainer 
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })} 
                title={translate["tour-agencies"][this.props.lang]} 
                screenId={this.props.testID}
                back={true} 
                onBack={() => {navigate(this, null, "pop")}} 
                noMenu={true}
                reverse={true}
                screenComponent={this}
            >
                <View style={style.container}>
                    {agencies}
                </View>
                {this.props.agencies_loading ? <APSpinner /> : null}
            </APContainer>
        )
    }
}

const style = StyleSheet.create({

    container:{
        paddingTop: 0
    },
    content: {
        padding: 24,
        flexDirection: "row"
    },

    image: {
        height: 55,
        width: 55,
        borderRadius: 27.5,
        borderWidth: 1,
        borderColor: "#d0d0d0"
    },

    contentRight: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingLeft: 24
    },

    titleWrap: {
        marginBottom: 8
    },

    title: {
        lineHeight: 20,
        fontSize: 16,
        color: "#424242"
    },

    bottom: {
        flexDirection: "row",
        alignItems: "center"
    },

    cup: {
        backgroundColor: "#FEA62A",
        width: 20,
        height: 20,
        marginRight: 12,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    },

    rateContainer: {
        alignItems: "center",
        flexDirection: "row",
        marginRight: 12
    },

    rate: {
        fontSize: 22,
        lineHeight: 28
    },

    starContainer: {
        flexDirection: "row",
        justifyContent: "flex-start"
    }

})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        token: state.user.token,
        agencies: state.agencies.agencies,
        agencies_loading: state.agencies.agencies_loading,
        ios_toolbar_height: state.index.ios_toolbar_height
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAgencies: () => { dispatch(get_agencies()) },
        resetAgencies: () => { dispatch(reset_agencies()) },
        selectAgency: (agency) => { dispatch(select_agency(agency)) },
        selectLastAgency: (agency) => {dispatch(select_last_agency(agency))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectAgencyScreen)
