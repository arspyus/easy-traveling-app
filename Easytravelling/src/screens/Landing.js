import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, Image, AsyncStorage, NetInfo, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import { setLanguage, store_connection, store_connection_changed, setMessage, checkExtraData} from '../store/actions/indexAction';
import { setGuestUser } from '../store/actions/userAction';

import APButton from '../ui/button';
import APDropdown from '../ui/dropdown';
import APContainer from '../ui/container';
import APIcon from '../ui/icon';
import APSocialButtons from '../components/socialButtons';

const bgImage = require('./../../assets/images/landing-bg.jpg');

import translate from '../utils/translate';
import languages from '../utils/languages';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';
import throttle from '../utils/throttle';

class LandingScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
        if(props.uid){
            navigate(this, 'OffersScreen', 'resetTo', {}, true);
        }
    }

    state = {
        top: Dimensions.get("window").height*0.18,
        dropdowntop: Dimensions.get("window").height*0.045,
        bottom: Dimensions.get("window").height*0.12
    }

    willAppear() {
        this.props.connection === false && this.props.showError("went-offline");
        if(this.props.connection === "unknown"){
            NetInfo.getConnectionInfo().then((connectionInfo) => {
                if(connectionInfo.type === "none"){
                    this.props.setConnection(false);
                } else if(connectionInfo.type !== "unknown"){
                    this.props.setConnection(true);
                    this.props.setConnectionChanged();
                    this.props.checkExtraData()
                }
            })
        } 
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.signInDone && nextProps.signInDone) {
            if(this.props.prevPage !== "easytraveling.AuthScreen"){
                navigate(this, 'OffersScreen', 'resetTo', {})
            }
        }
    }

    willDisappear() {
        this.props.navigator.setDrawerEnabled({
            side: "left",
            enabled: true
        });
    }


    onSignIn = () => {
        this.navigateToAuth(0)
    }

    onSignUp = () => {
        this.navigateToAuth(1)
    }

    navigateToAuth(index) {
        navigate(this, 'AuthScreen', 'push', {
            selectedTabIndex: index
        });
    }

    onLangChange = (index, value) => {
        this.props.setLanguage(value);
    }

    onSkip = () => {
        this.props.setGuestUser();
        navigate(this, 'OffersScreen', 'resetTo', {})
    }

    render() {

        return (
            <APContainer 
                onMenu={() =>this.props.navigator.toggleDrawer({ side: 'left',  animated: true})} 
                header={false} 
                showOffline={true} 
                screenId={this.props.testID}
                screenComponent={this}
            >
                <View style={style.container}>
                    <Image source={bgImage} style={style.bgImage} resizeMode="cover" />
                    <APDropdown 
                            options={languages.languagesObj} 
                            defaultValue={this.props.lang}
                            defaultIndex={languages.languages.indexOf(languages.languagesMap[this.props.lang])} 
                            onSelect={this.onLangChange}
                            style={[style.dropdown, {top: this.state.dropdowntop}]}
                            textStyle={{ fontSize: 15, color: "#fff", ...globalStyles(true).fontLight}} 
                            dropdownTextStyle={{color: "#7cb342"}}
                            icon={true}
                            iconColor="#fff"
                        />
                    <View style={[style.logo, {top: this.state.top}]}>
                        <View style={[style.logoContainer]}>
                            <APIcon name="logo_final" size={216} color="#fff"  />
                        </View>
                    </View>
                    <View style={[style.btnContainer, {bottom: this.state.bottom}]}>
                        <View style={style.btnContainerRow}>
                            <APButton text={translate["login"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.onSignIn)} />
                            <View style={style.btnSpace}></View>
                            <APButton text={translate["registration"][this.props.lang]} color="#7cb342" backgroundColor="#fff" onPress={throttle(this.onSignUp)} />
                        </View>
                        <APSocialButtons lang={this.props.lang} page="landing"/>
                    </View>
                    <TouchableOpacity style={style.skip} onPress={this.onSkip}>
                        <View>
                            <Text style={[style.skipText, globalStyles().fontLight]}>{translate["skip"][this.props.lang]}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </APContainer>

        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center"
    },
    bgImage: {
        width: "100%",
        height: "100%",
        position: 'absolute', 
        top: 0, 
        left: 0
    },
    logoContainer: {
        position: "absolute",
        flexDirection: "row",
        width: "100%",
        justifyContent: "center",
        zIndex: 8000,
        transform: [{translateY : -71.5}]
    },
    logo: {
        position: "absolute",
        zIndex: 8000,
        flexDirection: "row",
        width: "100%",
        justifyContent: "center",
        left: 0,
        height: 73,
        overflow: "hidden"
    },
    btnContainer: {
        position: "absolute",
        justifyContent: "space-between",
        zIndex: 1
    },
    btnContainerRow: {
        justifyContent: "space-between",
        flexDirection: "row",
        paddingVertical: 5
    },
    btnSpace: {
        width: "5%"
    },
    dropdown: {
        position: "absolute",
        alignItems: "center",
        left: 0,
        right: 0,
        zIndex: 2
    },
    skip: {
        zIndex: 2,
        position: "absolute",
        alignItems: "center",
        bottom: 14,
        paddingVertical: 5,
        paddingHorizontal: 20
    },
    skipText: {
        color: "#fff",
        fontSize: 16
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        connection: state.index.connection,
        connectionChanged : state.index.connectionChanged,
        uid: state.user.uid,
        signInDone: state.user.signInProcess,
        prevPage: state.index.prevPage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setLanguage: (lang) => { dispatch(setLanguage(lang)) },
        setConnection: (connection) => {dispatch(store_connection(connection))},
        setConnectionChanged: () => dispatch(store_connection_changed(true)),
        showError: (message) => {dispatch(setMessage(true, message))},
        checkExtraData: () => {dispatch(checkExtraData())},
        setGuestUser: () => {dispatch(setGuestUser(true))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingScreen)
