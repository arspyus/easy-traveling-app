import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, Image, Keyboard, Platform } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APInput from '../ui/input';
import APSelect from '../ui/select';
import APAutocomplete from '../ui/autocomplete';
import APSwitch from '../ui/switch';
import APDatePicker from '../ui/datePicker';
import APCheckList from '../ui/checkList';
import APRadioList from '../ui/radioList';
import APTextarea from '../ui/textarea';
import APButton from '../ui/button';
import APKeyboardAvoidingView from '../ui/keyboardAvoidingView';

import translate from '../utils/translate';
import countries from '../utils/countries';
import isNumber from '../utils/isNumber';
import validate from '../utils/validate';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';

import { setMessage } from '../store/actions/indexAction';
import { make_request, select_type, select_agency, select_last_agency } from '../store/actions/makeRequestAction';
import { reset_agencies } from '../store/actions/agencyAction';
const google_logo = require('../../assets/images/powered-by-google.png');

class MakeRequestScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    state = {
        enableScrollViewScroll: true
    }

    willAppear() {
        this.props.resetAgencies();
        if(this.props.prevPage !== "easytraveling.SelectAgencyScreen" && this.props.lastSelectedAgency === null){
            this.props.selectAgency(null)
        }

        this.comesFromPage = this.props.prevPage;

        this.setState({...this.initialState()}, () => {
            if(this.props.lastSelectedAgency){
                this._requestTo && this._requestTo.select(this.props.lastSelectedAgency.id)
            } else {
                this._requestTo && this._requestTo.select(this.state.form.requestTo.value)
            }
            this.props.selectLastAgency(null);
        });
    }


    childrenAgesRefs = [];

    initialState() {
        return {
            cities: [],
            _scrollView: null,
            enableScrollViewScroll: true,
            form: {
                type: {
                    isValid: true,
                    options: [
                        { text: translate["outgoing-tours"][this.props.lang], value: "outgoing-tour" },
                        { text: translate["domestic-tours"][this.props.lang], value: "domestic-tour" },
                    ],
                    value: this.props.type ? this.props.type : "outgoing-tour"
                },

                requestTo: {
                    isValid: true,
                    options: this.props.selectedAgency ? [
                        { text: this.props.selectedAgency.title, value: this.props.selectedAgency.id },
                        { text: translate["to-all"][this.props.lang], value: "to-all" },
                        { text: translate["to-agency"][this.props.lang], value: "to-agency" },
                    ] : [
                            { text: translate["to-all"][this.props.lang], value: "to-all" },
                            { text: translate["to-agency"][this.props.lang], value: "to-agency" }
                        ],
                    value: this.props.lastSelectedAgency ? this.props.lastSelectedAgency.id : this.state.form && this.state.form.requestTo && this.state.form.requestTo.value ? this.state.form.requestTo.value : "to-all"
                },

                country: {
                    isValid: true,
                    validationRules: [
                        { type: "required" },
                        { type: "country" }
                    ],
                    value: this.props.type === "domestic-tour" ? "armenia" : ""
                },

                city: {
                    isValid: true,
                    validationRules: [
                        { type: "required" }
                    ],
                    value: ""
                },

                nationality: {
                    isValid: true,
                    validationRules: [
                        { type: "required" }
                    ],
                    value: this.props.type === "domestic-tour" ? "armenia" : ""
                },

                "visa-support": {
                    isValid: true,
                    value: false
                },

                from: {
                    isValid: true,
                    value: "",
                    validationRules: [
                        { type: "required" }
                    ]
                },

                to: {
                    isValid: true,
                    value: "",
                    validationRules: [
                        { type: "required" }
                    ]
                },

                nights: {
                    isValid: true,
                    validationRules: [
                        { type: "required" },
                        { type: "number" }
                    ],
                    value: ""
                },
                adults: {
                    isValid: true,
                    validationRules: [
                        { type: "required" },
                        { type: "number" }
                    ],
                    value: ""
                },
                children: {
                    isValid: true,
                    value: ""
                },
                infant: {
                    isValid: true,
                    value: ""
                },
                airticket: {
                    isValid: true,
                    validationRules: [
                        { type: "required" }
                    ],
                    value: this.props.type === "domestic-tour" ? "no" : "",
                    options: [
                        { text: translate["request-no-ticket"][this.props.lang], value: "no" },
                        { text: translate["request-armenia"][this.props.lang], value: "armenia" },
                        { text: translate["request-georgia"][this.props.lang], value: "georgia" }
                    ]
                },
                payment: {
                    isValid: true,
                    value: "",
                    options: [
                        { text: translate["choose-payment-method"][this.props.lang], value: "" },
                        { text: translate["cash"][this.props.lang], value: "cash" },
                        { text: translate["banking-transfer"][this.props.lang], value: "banking-transfer" },
                        { text: translate["bank-cart"][this.props.lang], value: "bank-cart" },
                        { text: translate["express"][this.props.lang], value: "express" },
                    ]
                },
                "hotel-category": {
                    isValid: true,
                    value: "",
                    options: [
                        { text: translate["choose-hotel-category"][this.props.lang], value: "" },
                        { text: translate["1-star"][this.props.lang], value: "1" },
                        { text: translate["2-star"][this.props.lang], value: "2" },
                        { text: translate["3-star"][this.props.lang], value: "3" },
                        { text: translate["4-star"][this.props.lang], value: "4" },
                        { text: translate["5-star"][this.props.lang], value: "5" },
                    ]
                },
                rooms: {
                    isValid: true,
                    value: ""
                },
                "board-type": {
                    isValid: true,
                    value: "",
                    options: [
                        { text: translate["choose-board-type"][this.props.lang], value: "" },
                        { text: translate["room-only"][this.props.lang], value: "room-only" },
                        { text: translate["breakfast"][this.props.lang], value: "breakfast" },
                        { text: translate["half-board"][this.props.lang], value: "half-board" },
                        { text: translate["full-board"][this.props.lang], value: "full-board" },
                        { text: translate["all-inclusive"][this.props.lang], value: "all-inclusive" }
                    ]
                },
                "extra": {
                    isValid: true,
                    value: "",
                    options: [
                        { text: translate["choose-extra-options"][this.props.lang], value: "" },
                        { text: translate["city-holiday"][this.props.lang], value: "city-holiday" },
                        { text: translate["beach-holidays"][this.props.lang], value: "beach-holidays" },
                        { text: translate["transfer"][this.props.lang], value: "transfer" },
                        { text: translate["pool"][this.props.lang], value: "pool" },
                        { text: translate["1st-line"][this.props.lang], value: "1st-line" },
                        { text: translate["2st-line"][this.props.lang], value: "2st-line" },
                        { text: translate["internet"][this.props.lang], value: "internet" },
                        { text: translate["parking"][this.props.lang], value: "parking" },
                        { text: translate["gym"][this.props.lang], value: "gym" },
                        { text: translate["childrens-room"][this.props.lang], value: "childrens-room" },
                        { text: translate["gid-service"][this.props.lang], value: "gid-service"}
                    ]
                },

                "request-notes": {
                    isValid: true,
                    value: ""
                },

                childrenAges: {
                    value: [],
                    isValid: true
                }
            },
        }
    }

    screenName = "makerequest";


    getCities(text) {
        let country = this.state.form.country.value;
        let country_code = null;
        if (this.state.form.type.value === "domestic-tour") {
            country_code = "am";
        } else {
            let countryObj = countries.filter(el => {
                return el.text === country
            })[0];
            country_code = countryObj ? countryObj.code : null;
        }
        let components = '';
        if (country_code) {
            components = '&components=country:' + country_code;
        }
        const url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + text.toLowerCase() + '&types=(cities)' + components + '&key=AIzaSyDBN0dmaRZeTml3chn2f8B7tDRX22NtiUU';
        fetch(url, {
            method: "GET"
        }).then(res => res.json()).then(resp => {
            if (resp.predictions) {
                this.setState({
                    ...this.state,
                    cities: resp.predictions.map(pred => {
                        return {
                            text: pred.structured_formatting.main_text,
                            value: pred.structured_formatting.main_text
                        }

                    })
                })
            }
        }).catch(err => {
        })
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.lang != nextProps.lang) {
            this.resetOptionsText(nextProps.lang);
        }

        if (!this.props.makeRequestProcess && nextProps.makeRequestProcess) {
            this.navigateToOffers();
        }
    }

    navigateToOffers() {
        navigate(this, "OffersScreen", "resetTo", null);
    }

    resetOptionsText(lang) {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                type: {
                    ...this.state.form.type,
                    options: [
                        { text: translate["outgoing-tours"][lang], value: "outgoing-tour" },
                        { text: translate["domestic-tours"][lang], value: "domestic-tour" },
                    ]
                },
                requestTo: {
                    ...this.state.form.requestTo,
                    options: [
                        { text: translate["to-all"][lang], value: "to-all" },
                        { text: translate["to-agency"][lang], value: "to-agency" }
                    ]
                },
                payment: {
                    ...this.state.form.payment,
                    options: [
                        { text: translate["choose-payment-method"][lang], value: "" },
                        { text: translate["cash"][lang], value: "cash" },
                        { text: translate["banking-transfer"][lang], value: "banking-transfer" },
                        { text: translate["bank-cart"][lang], value: "bank-cart" },
                        { text: translate["express"][lang], value: "express" },
                    ]
                },
                airticket: {
                    ...this.state.form.airticket,
                    options: [
                        { text: translate["request-no-ticket"][lang], value: "no" },
                        { text: translate["request-armenia"][lang], value: "armenia" },
                        { text: translate["request-georgia"][lang], value: "georgia" }
                    ]
                },
                "hotel-category": {
                    ...this.state.form["hotel-category"],
                    options: [
                        { text: translate["choose-hotel-category"][lang], value: "" },
                        { text: translate["1-star"][lang], value: "1" },
                        { text: translate["2-star"][lang], value: "2" },
                        { text: translate["3-star"][lang], value: "3" },
                        { text: translate["4-star"][lang], value: "4" },
                        { text: translate["5-star"][lang], value: "5" },
                    ]
                },
                "board-type": {
                    ...this.state.form["board-type"],
                    options: [
                        { text: translate["choose-board-type"][lang], value: "" },
                        { text: translate["room-only"][lang], value: "room-only" },
                        { text: translate["breakfast"][lang], value: "breakfast" },
                        { text: translate["half-board"][lang], value: "half-board" },
                        { text: translate["full-board"][lang], value: "full-board" },
                        { text: translate["all-inclusive"][lang], value: "all-inclusive" }
                    ]
                },
                "extra": {
                    ...this.state.form["extra"],
                    options: [
                        { text: translate["choose-extra-options"][lang], value: "" },
                        { text: translate["city-holiday"][lang], value: "city-holiday" },
                        { text: translate["beach-holidays"][lang], value: "beach-holidays" },
                        { text: translate["transfer"][lang], value: "transfer" },
                        { text: translate["pool"][lang], value: "pool" },
                        { text: translate["1st-line"][lang], value: "1st-line" },
                        { text: translate["2st-line"][lang], value: "2st-line" },
                        { text: translate["internet"][lang], value: "internet" },
                        { text: translate["parking"][lang], value: "parking" },
                        { text: translate["gym"][lang], value: "gym" },
                        { text: translate["childrens-room"][lang], value: "childrens-room" },
                        { text: translate["gid-service"][lang], value: "gid-service"}
                    ]
                }
            }
        })
    }

    onChange = (text, field, index) => {
        if (field === "childrenAges") {
            let ages = [...this.state.form.childrenAges.value];
            ages[index] = {
                value: text,
                isValid: true
            };
            this.updateField(field, ages);
        } else if (field === "requestTo") {
            if (text === "to-agency") {
                this._requestTo && this._requestTo.select(this.state.form.requestTo.value)
                
                navigate(this, "SelectAgencyScreen", "push", null);

            } else {
                this.updateField(field, text);
            }
        } else if (field === "type") {
            let form = null;
            this.props.selectType(text)
            if (text === "domestic-tour") {
                form = {
                    type: {
                        ...this.state.form.type,
                        value: "domestic-tour"
                    },
                    country: {
                        ...this.state.form.country,
                        value: "armenia"
                    },
                    nationality: {
                        ...this.state.form.nationality,
                        value: "armenia"
                    },
                    airticket: {
                        ...this.state.form.airticket,
                        value: "no"
                    },
                    "visa-support": {
                        ...this.state.form["visa-support"],
                        value: false
                    },
                }
            } else {
                form = {
                    type: {
                        ...this.state.form.type,
                        value: "outgoing-tour"
                    },
                    country: {
                        ...this.state.form.country,
                        value: ""
                    },
                    nationality: {
                        ...this.state.form.nationality,
                        value: ""
                    },
                    airticket: {
                        ...this.state.form.airticket,
                        value: ""
                    },
                    "visa-support": {
                        ...this.state.form["visa-support"],
                        value: false
                    }
                }
            }
            this.setState({...this.initialState()}, () => {
                this.setState({
                    ...this.state,
                    form: {
                        ...this.state.form,
                        ...form
                    }
                })
            });
            
        } else if (field === "country") {
            let form = null;
            form = {
                city: {
                    ...this.state.form.city,
                    value: ""
                },
                country: {
                    ...this.state.form.country,
                    value: text
                }
            }
            this.setState({...this.initialState()}, () => {
                this.setState({
                    ...this.state,
                    form: {
                        ...this.state.form,
                        ...form
                    }
                })
            });
        } else {
            this.updateField(field, text);
        }

    }

    onChildrenBlur = () => {
        let text = this.state.form.children.value;
        const count = text === "" || !isNumber(text) ? 0 : parseInt(text);
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                childrenAges: {
                    ...this.state.form.childrenAges,
                    value: count === 0 ? [] : Array(count).join(".").split(".").map(el => {
                        return {
                            isValid: true,
                            value: ""
                        }
                    })
                }
            }
        })
    }

    onStartShouldSetResponderCapture = (enableScrollViewScroll) => {
        this.setState({
            ...this.state,
            enableScrollViewScroll: enableScrollViewScroll
        });
    }

    updateField(field, value) {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [field]: {
                    ...this.state.form[field],
                    value: value
                }
            }
        });
    }

    updateValidationState = (cb) => {
        let obj = { ...this.state.form };
        for (let field in this.state.form) {
            if (this.state.form[field].validationRules) {
                obj[field].isValid = validate(this.state.form[field].validationRules, this.state.form[field].value);
            }
        }
        if (this.state.form.childrenAges.value) {
            for (let i = 0; i < this.state.form.childrenAges.value.length; i++) {

                obj['childrenAges'].value[i].isValid = this.state.form.childrenAges.value[i].value && parseInt(this.state.form.childrenAges.value[i].value) > 1 && parseInt(this.state.form.childrenAges.value[i].value) <= 12 
            }
        }

        this.setState({
            ...this.state,
            form: obj
        }, cb)
    }

    isFormValid = () => {
        let isValid = true;
        for (let field in this.state.form) {
            if (!this.state.form[field].isValid) {
                isValid = false;
                break;
            }

            if(field === "childrenAges" && this.state.form.childrenAges.value && this.state.form.childrenAges.value.length){
                for (let i = 0; i < this.state.form.childrenAges.value.length; i++) {
                    if(!this.state.form.childrenAges.value[i].isValid){
                        isValid = false;
                        break;
                    }
                }
            }
        }
        return isValid;
    }

    getExtraValue() {
        let str = this.state.form.extra.value;
        if (!str) return [];
        let splitted = this.state.form.extra.value.split(",").map(el => el.trim());
        let options = this.state.form.extra.options;
        let result = [];
        let temp = {};
        options.forEach(el => {
            temp[el.value] = el.text;
        });
        const values = Object.keys(temp);
        const texts = Object.values(temp);
        splitted.forEach(el => {
            let index = texts.indexOf(el);
            if (index > -1) {
                result.push(values[index]);
            }
        })
        return result;
    }

    makeRequest = () => {
        if (this.clicked) return;
        this.clicked = true;
        Keyboard.dismiss();
        this.updateValidationState(() => {
            if (this.isFormValid()) {
                const data = {
                    country: this.state.form.country.value,
                    city: this.state.form.city.value,
                    nationality: this.state.form.nationality.value,
                    'period-from': this.state.form.from.value,
                    'period-to': this.state.form.to.value,
                    nights: this.state.form.nights.value,
                    adults: this.state.form.adults.value,
                    children: this.state.form.children.value,
                    children_ages: this.state.form.childrenAges.value ? JSON.stringify(this.state.form.childrenAges.value.map(el => el.value)) : JSON.stringify([]),
                    infant: this.state.form.infant.value,
                    airticket: this.state.form.airticket.value,
                    rooms: this.state.form.rooms.value,
                    "chosen-tour": this.state.form.requestTo.value != "to-all" && this.state.form.requestTo.value != "to-agency" ? this.state.form.requestTo.value : "",
                    payment: this.state.form.payment.value,
                    "hotel-category": this.state.form["hotel-category"].value,
                    "board-type": this.state.form["board-type"].value,
                    "visa-support": this.state.form["visa-support"].value ? "yes" : "no",
                    extra: JSON.stringify(this.getExtraValue()),
                    notes: this.state.form["request-notes"].value
                }
                this.props.makeRequest(data);
                this.clicked = false;
            } else {
                this.props.showError("fill-form");
                this.clicked = false;
            }
        });
    }

    render() {
        let childrenAges = null;
        this.childrenAgesRefs = [];
        if (this.state.form && this.state.form.childrenAges && this.state.form.childrenAges.value.length > 0) {
            childrenAges = this.state.form.childrenAges.value.map((el, index) => {
                return (
                    <View style={style.ageBox} key={index.toString()}>
                        <APInput
                            label={translate["age"][this.props.lang]}
                            value={this.state.form.childrenAges.value[index].value}
                            isValid={this.state.form.childrenAges.value[index].isValid}
                            keyboardType="numeric"
                            onChangeText={(text) => { this.onChange(text, "childrenAges", index) }}
                            ref={(ref) => ref && this.childrenAgesRefs.push(ref)}
                            onSubmitEditing={()=>{this.childrenAgesRefs[index + 1] ? this.childrenAgesRefs[index + 1].focus() : this._infantTextInput.focus()}}
                            _scrollView={this.state._scrollView}
                        />
                    </View>
                )
            })
        }

        let nightsLabel = translate["nights"][this.props.lang] + " *";
        if (this.state.form && this.state.form.nights.value && isNumber(this.state.form.nights.value)) {
            nightsLabel = <Text><Text>{translate["nights"][this.props.lang] + " *"}</Text><Text style={{ color: "#424242" }}> ({translate["days"][this.props.lang] + " - " + (parseInt(this.state.form.nights.value) + 1)})</Text></Text>;
        }
        return (
            <APContainer 
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })} 
                title={translate["make-request"][this.props.lang]} 
                screenId={this.props.testID}
                style={{backgroundColor: "#fff"}}
                screenComponent={this}
            >
                    <APKeyboardAvoidingView onStartShouldSetResponderCapture={() => { Platform.OS === "android" && this.setState({ enableScrollViewScroll: true }) }}>
                        <ScrollView 
                            keyboardShouldPersistTaps="handled" 
                            showsVerticalScrollIndicator={false}
                            alwaysBounceVertical={false}
                            scrollEnabled={this.state.enableScrollViewScroll} 
                            bounces={false}
                            ref={(ref)=>{!this.state._scrollView && this.setState({...this.state, _scrollView: ref})}} 
                    >
                        {this.state.form ? <APWrapper>
                            {this.state.form.type.value === "domestic-tour" ?
                                <View style={[style.section, { alignItems: "center", marginTop: 0 }]}>
                                    <Text style={[style.sectionText, globalStyles().fontLight]}>{translate["armenia-and-karabakh"][this.props.lang]}</Text>
                                </View> : null}
                            <View>
                                <APSelect
                                    label={translate["tour-type"][this.props.lang]}
                                    isValid={this.state.form.type.isValid}
                                    value={this.state.form.type.value}
                                    options={this.state.form.type.options}
                                    onChangeText={(text) => { this.onChange(text, "type") }}
                                />
                                <APSelect
                                    label={translate["request"][this.props.lang]}
                                    isValid={this.state.form.requestTo.isValid}
                                    value={this.state.form.requestTo.value}
                                    options={this.state.form.requestTo.options}
                                    onChangeText={(text) => { this.onChange(text, "requestTo") }}
                                    ref={(ref) => this._requestTo = ref}
                                />
                                {this.state.form.type.value !== "domestic-tour" ?
                                    <APAutocomplete
                                        label={translate["country-form"][this.props.lang] + " *"}
                                        isValid={this.state.form.country.isValid}
                                        value={this.state.form.country.value}
                                        options={countries}
                                        onChangeText={(text) => { this.onChange(text, "country") }}
                                        ref={(ref) => this._countryTextInput = ref}
                                        onSubmitEditing={()=>this._cityTextInput.focus()}
                                        _scrollView={this.state._scrollView}
                                        responderHandler={Platform.OS === "android" ? this.onStartShouldSetResponderCapture: null}
                                    /> : null}
                                <APAutocomplete
                                    label={translate["city-form"][this.props.lang] + " *"}
                                    isValid={this.state.form.city.isValid}
                                    value={this.state.form.city.value}
                                    options={this.state.cities}
                                    onChangeText={(text) => { this.onChange(text, "city"), this.getCities(text) }}
                                    ref={(ref) => this._cityTextInput = ref}
                                    onSubmitEditing={()=>{this._nationalityTextInput ? this._nationalityTextInput.focus() : this._nightsTextInput.focus()}}
                                    _scrollView={this.state._scrollView}
                                    responderHandler={Platform.OS === "android" ? this.onStartShouldSetResponderCapture: null}
                                />
                                <View style={style.google_logo_wrapper}>
                                    <Image style={style.google_logo} source={google_logo} />
                                </View>
                                {this.state.form.type.value !== "domestic-tour" ?
                                    <View>
                                        <APAutocomplete
                                            label={translate["nationality-form"][this.props.lang] + " *"}
                                            isValid={this.state.form.nationality.isValid}
                                            value={this.state.form.nationality.value}
                                            options={countries}
                                            onChangeText={(text) => { this.onChange(text, "nationality") }}
                                            ref={(ref) => this._nationalityTextInput = ref}
                                            onSubmitEditing={()=> this._nightsTextInput.focus()}
                                            _scrollView={this.state._scrollView}
                                            responderHandler={Platform.OS === "android" ? this.onStartShouldSetResponderCapture: null}
                                        />
                                        <View style={style.section}>
                                            <Text style={[style.sectionText, globalStyles().fontLight]}>{translate["visa-support"][this.props.lang]}</Text>
                                        </View>
                                        <APSwitch
                                            yes={translate["yes"][this.props.lang]}
                                            no={translate["no"][this.props.lang]}
                                            onValueChange={(text) => { this.onChange(text, "visa-support") }}
                                            value={this.state.form["visa-support"].value}
                                        />
                                    </View>
                                    : null}
                                <View style={style.section}>
                                    <Text style={[style.sectionText, globalStyles().fontLight]}>{translate["request-period"][this.props.lang]}</Text>
                                </View>
                                <APDatePicker
                                    label={translate["from"][this.props.lang] + " *"}
                                    isValid={this.state.form.from.isValid}
                                    value={this.state.form.from.value}
                                    onChangeText={(text) => { this.onChange(text, "from") }}
                                    max={this.state.form.to.value ? this.state.form.to.value : null}
                                    lang={this.props.lang}
                                />
                                <APDatePicker
                                    label={translate["to"][this.props.lang] + " *"}
                                    isValid={this.state.form.to.isValid}
                                    value={this.state.form.to.value}
                                    onChangeText={(text) => { this.onChange(text, "to") }}
                                    min={this.state.form.from.value ? this.state.form.from.value : null}
                                    lang={this.props.lang}
                                />
                                <APInput
                                    label={nightsLabel}
                                    isValid={this.state.form.nights.isValid}
                                    value={this.state.form.nights.value}
                                    keyboardType="numeric"
                                    onChangeText={(text) => { this.onChange(text, "nights") }}
                                    ref={(ref) => this._nightsTextInput = ref}
                                    onSubmitEditing={()=> this._adultsTextInput.focus()}
                                    _scrollView={this.state._scrollView}
                                />
                                <View style={style.section}>
                                    <Text style={[style.sectionText, globalStyles().fontLight]}>{translate["total-pax"][this.props.lang]}</Text>
                                </View>
                                <APInput
                                    label={translate["adults"][this.props.lang] + " *"}
                                    isValid={this.state.form.adults.isValid}
                                    value={this.state.form.adults.value}
                                    keyboardType="numeric"
                                    onChangeText={(text) => { this.onChange(text, "adults") }}
                                    ref={(ref) => this._adultsTextInput = ref}
                                    onSubmitEditing={()=> this._childrenTextInput.focus()}
                                    _scrollView={this.state._scrollView}
                                />
                                <View style={[childrenAges ? style.agesStyle : null, {borderRadius: 4} ]}>
                                    <APInput
                                        label={translate["children"][this.props.lang]}
                                        isValid={this.state.form.children.isValid}
                                        value={this.state.form.children.value}
                                        keyboardType="numeric"
                                        onChangeText={(text) => { this.onChange(text, "children") }}
                                        onBlur={this.onChildrenBlur}
                                        ref={(ref) => this._childrenTextInput = ref}
                                        onSubmitEditing={()=> {this.childrenAgesRefs[0] && this.state.form.children.value ? this.childrenAgesRefs[0].focus(): this._infantTextInput.focus()}}
                                        _scrollView={this.state._scrollView}
                                        maxLength={1}
                                    />
                                    <View style={[style.agesContainer, childrenAges ? {marginBottom: -34} : null]}>
                                        {childrenAges}
                                    </View>
                                </View>
                                <APInput
                                    label={translate["infant"][this.props.lang]}
                                    isValid={this.state.form.infant.isValid}
                                    value={this.state.form.infant.value}
                                    keyboardType="numeric"
                                    onChangeText={(text) => { this.onChange(text, "infant") }}
                                    ref={(ref) => this._infantTextInput = ref}
                                    onSubmitEditing={()=> this._roomsTextInput.focus()}
                                    _scrollView={this.state._scrollView}
                                />
                                {this.state.form.type.value !== "domestic-tour" ?
                                    <View>
                                        <View style={style.section}>
                                            <Text style={[style.sectionText, globalStyles().fontLight]}>{translate["airticket"][this.props.lang] + " *"}</Text>
                                        </View>
                                        <APRadioList
                                            options={this.state.form["airticket"].options}
                                            onChangeText={(text) => { this.onChange(text, "airticket") }}
                                            isValid={this.state.form['airticket'].isValid}
                                        />
                                    </View> : null
                                }
                                <View style={style.section}>
                                    <Text style={[style.sectionText, globalStyles().fontLight]}>{translate["extra"][this.props.lang]}</Text>
                                </View>
                                <APSelect
                                    label={translate["payment-method"][this.props.lang]}
                                    isValid={this.state.form.payment.isValid}
                                    value={this.state.form.payment.value}
                                    options={this.state.form.payment.options}
                                    onChangeText={(text) => { this.onChange(text, "payment") }}
                                    disabledOptions={[0]}
                                />
                                <APSelect
                                    label={translate["hotel-category"][this.props.lang]}
                                    isValid={this.state.form["hotel-category"].isValid}
                                    value={this.state.form["hotel-category"].value}
                                    options={this.state.form["hotel-category"].options}
                                    onChangeText={(text) => { this.onChange(text, "hotel-category") }}
                                    disabledOptions={[0]}
                                />
                                <APInput
                                    label={translate["rooms"][this.props.lang]}
                                    isValid={this.state.form.rooms.isValid}
                                    value={this.state.form.rooms.value}
                                    keyboardType="numeric"
                                    onChangeText={(text) => { this.onChange(text, "rooms") }}
                                    ref={(ref) => this._roomsTextInput = ref}
                                    onSubmitEditing={()=> this._notesTextInput.focus()}
                                    _scrollView={this.state._scrollView}
                                />
                                <APSelect
                                    label={translate["board-type"][this.props.lang]}
                                    isValid={this.state.form["board-type"].isValid}
                                    value={this.state.form["board-type"].value}
                                    options={this.state.form["board-type"].options}
                                    onChangeText={(text) => { this.onChange(text, "board-type") }}
                                    disabledOptions={[0]}
                                />
                                <APCheckList
                                    label={translate["extra-options"][this.props.lang]}
                                    isValid={this.state.form["extra"].isValid}
                                    options={this.state.form["extra"].options}
                                    onChangeText={(text) => { this.onChange(text, "extra") }}
                                    disabledOptions={[0]}
                                />
                                <APTextarea
                                    label={translate["notes"][this.props.lang]}
                                    isValid={this.state.form["request-notes"].isValid}
                                    value={this.state.form["request-notes"].value}
                                    onChangeText={(text) => { this.onChange(text, "request-notes") }}
                                    ref={(ref) => this._notesTextInput = ref}
                                    _scrollView={this.state._scrollView}
                                />
                            </View>
                            <View style={style.buttonContainer}>
                                <APButton text={translate["make-request"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.makeRequest)} style={{ ...globalStyles(true).fontLight }} />
                            </View>
                        </APWrapper> : null}
                    </ScrollView>
                    </APKeyboardAvoidingView>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },

    section: {
        paddingBottom: 10,
        marginVertical: 10
    },

    sectionText: {
        fontSize: 21,
        lineHeight: 24,
        color: "#424242"
    },

    buttonContainer: {
        marginVertical: 14,
        width: "100%"
    },

    agesStyle: {
        borderStyle: "solid", 
        borderWidth: 2, 
        borderColor: "#9e9e9e", 
        paddingTop: 10, 
        paddingHorizontal: 10, 
        marginBottom: 20
    },

    agesContainer: {
        flexDirection: "row",
        width: "100%",
        flexWrap: "wrap"
    },

    ageBox: {
        width: "33.33%",
        paddingHorizontal: 10,

    },

    google_logo_wrapper: {
        flexDirection: "row",
        justifyContent: "flex-end",
        marginTop: -40,
        marginBottom: 30
    },

    google_logo: {
        width: 50,
        height: 10,
        resizeMode: "contain"
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        prevPage: state.index.prevPage,
        form: state.makeRequest.form,
        selectedAgency: state.makeRequest.selectedAgency,
        lastSelectedAgency: state.makeRequest.lastSelectedAgency,
        type: state.makeRequest.type,
        makeRequestProcess: state.makeRequest.makeRequestProcess
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        showError: (message) => { dispatch(setMessage(true, message)) },
        makeRequest: (data) => { dispatch(make_request(data)) },
        selectType: (type) => { dispatch(select_type(type)) },
        selectAgency: (agency) => { dispatch(select_agency(agency)) },
        selectLastAgency: (agency) => { dispatch(select_last_agency(agency)) },
        resetAgencies: () => {dispatch(reset_agencies())},
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MakeRequestScreen)