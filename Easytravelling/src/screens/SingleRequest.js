import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';

import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APAlertPopup from '../popups/alertPopup';
import APSection from '../ui/section';
import APStatusLoader from '../ui/statusLoader';
import APIcon from '../ui/icon';

import translate from '../utils/translate';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';
import capitalize from '../utils/capitalize';
import urlify from '../utils/urlify'

import APImageStore from '../utils/imageStore';

import { get_request } from '../store/actions/singleAction';
import { delete_request } from '../store/actions/myRequest';

const hotelImage = require('./../../assets/images/hotel.png');

class SingleRequestScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    state = {
        showAlert: false,
        thumbnails: {}
    }

    willAppear() {
        this.props.getRequest(this.props.request_id);
        console.log(this.props.request)
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.deleteRequestProcess && nextProps.deleteRequestProcess) {
            navigate(this, null, "pop");
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(!prevProps.request && this.props.request){
            this.loadThumbnails()
        }
    }

    loadThumbnails = () => {
        this.props.request.offers.forEach((offer) => {
            if(offer.place_id){
                let IS = new APImageStore(offer.place_id);
                IS.getThumbnail().then(thumbnail => {
                    this.setState({
                        thumbnails: {
                            ...this.state.thumbnails,
                            [offer.offer_id]: thumbnail

                        }
                    })
                });
            }
        })
    }

    onBack = () => {
        navigate(this, null, "pop");
    }

    onDelete = () => {
        this.setState({
            showAlert: true
        })
    }

    hidePopup = () => {
        this.setState({
            showAlert: false
        })
    }

    newOffersCount() {
        let offers = this.props.request.offers;
        if (!offers) return 0;
        let newOffersCount = 0;
        for (let i = 0; i < this.props.request.offers.length; i++) {
            if (this.props.request.offers[i].seen_status === "new") {
                ++newOffersCount;
            }
        }
        return newOffersCount;
    }

    generateExtra = (value) => {
        if(!value) return "";
        let result = [];
        value.forEach(element => {
            if(translate[element.trim()]){
                result.push(translate[element.trim()][this.props.lang] ? translate[element.trim()][this.props.lang]: element.trim())
            }
        });
        return result.join(", ")
    }

    navigateToSingleOffer = (offer) => {
        let offers = this.props.request.offers.map(offer => {
            return {...offer, city: this.props.request.city, country: this.props.request.country, adults: this.props.request.adults, children: this.props.request.children, infant: this.props.request.infant}
        })
        navigate(this, "SingleOfferScreen", "push",  { offer_id: offer.offer_id, offers: offers, isArchive: this.props.isArchive })
    }

    renderOffers = () => {
      console.log(this.props.request)
        let request_offers = this.props.request.offers;
        offers = request_offers.map(offer => {
            let nights = Math.floor(((new Date(offer.hotel_checkout.split(" ")[0]) - new Date(offer.hotel_checkin.split(" ")[0])) / 3600 / 24 / 1000));
            let offerContent = (
                <View>
                    {
                        offer.days_left !== -1 && offer.offer_status !== 'rejected' ?
                            <View style={style.overlay}></View> :
                            null
                    }

                    {
                        offer.days_left === -1 || offer.offer_status === 'rejected' ?
                            <View style={[style.overlay, { backgroundColor: "rgba(255,255,255,.6)" }]}></View> :
                            null
                    }
                    <TouchableWithoutFeedback onPress={throttle(() => this.navigateToSingleOffer(offer))}>

                        <View style={style.offer}>
                            <View style={[style.row, style.offer_top]}>
                                <View>
                                    {
                                        offer.seen_status !== "seen" ?
                                            <View style={style.unseen} /> :
                                            null
                                    }
                                </View>
                                <View style={style.row}>
                                    {
                                        offer.hotel_rating ?
                                            <View style={style.row}>
                                                <APIcon size={18} color={"#ffffff"} name={"star"} />
                                                <Text style={[globalStyles().text, globalStyles().fontBold, { paddingLeft: 5, color: "#ffffff" }]}>{offer.hotel_rating}</Text>
                                            </View> :
                                            null
                                    }
                                    <View style={[style.row,  {marginLeft: 18}]}>
                                        <APIcon size={18} color={"#ffffff"} name={"date_range"} />
                                        <Text style={[globalStyles().text, globalStyles().fontBold, { paddingLeft: 5,  color: "#ffffff" }]}>{nights}</Text>
                                    </View>


                                </View>
                            </View>
                            <View style={[style.offer_bottom]}>
                                <View style={style.row}>
                                    <Text style={[style.offer_text, globalStyles().fontLight]} numberOfLines={2} >{offer.hotel}</Text>
                                </View>
                                <View style={[style.row, { marginTop: 6 }]}>
                                    <Text style={[style.offer_title, globalStyles().fontBold, style.textShadow]}>{offer.price + " " + translate["amd"][this.props.lang]}</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
            return (
                <View style={{paddingVertical: 5}}  key={offer.offer_id}>
                    {
                        <ImageBackground style={[style.imageBackground, globalStyles().shadow]} source={this.state.thumbnails[offer.offer_id] ? { uri: this.state.thumbnails[offer.offer_id] } : hotelImage} imageStyle={{ borderRadius: 10 }}>
                            {offerContent}
                        </ImageBackground>
                    }
                </View>
            )
        })


        return (
            <View>
                {offers}
            </View>
        )
    }

    render() {
        let travelersText = "";
        if(this.props.request){
            travelersText = this.props.request.adults + " " + translate["adults"][this.props.lang];
            const children = parseInt(this.props.request.children) + parseInt(this.props.request.infant);
            if(children > 0){
                travelersText += ", " + children + " " + translate["children"][this.props.lang];
            }
            if(this.props.request.children_ages){
                travelersText += " (" + this.props.request.children_ages.join(", ") + " " + translate["years_old"][this.props.lang] +")"
            }
        }
        return (
            <APContainer
                back={true}
                onBack={this.onBack}
                title={translate["my-request"][this.props.lang]}
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })}
                screenId={this.props.testID}
                reverse={true}
                noMenu={true}
                screenComponent={this}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    alwaysBounceVertical={false}
                    bounces={false}
                >
                    <APWrapper>
                        {this.props.request ? (
                            <View>
                                <APSection>
                                    <View style={globalStyles().section}>
                                        <View style={[style.row, style.top]}>
                                            <View style={style.top_left}>
                                                <View style={style.top_top}>
                                                    <Text style={[style.title, globalStyles().fontBold]} numberOfLines={1}>{translate["request"][this.props.lang] + " #" + this.props.request.request_id}</Text>
                                                </View>
                                                <View style={[style.row, { flexWrap: "wrap" }]}>

                                                    <Text style={[style.text, { paddingRight: 10 }, globalStyles().fontLight, style.green]}>{this.props.request.offers.length + " " + translate['offer'][this.props.lang]}</Text>
                                                    {
                                                        this.newOffersCount() > 0 ?
                                                            <View style={style.new}>
                                                                <Text style={[style.newText, globalStyles().fontLight]}>{this.newOffersCount() + " New"}</Text>
                                                            </View> :
                                                            null
                                                    }
                                                </View>
                                            </View>
                                            <View style={style.top_right}>
                                                <View style={style.top_top}>
                                                    <View style={[style.row, { justifyContent: "space-between" }]}>
                                                        <View style={[style.row]}>
                                                            <Text style={[globalStyles().text, globalStyles().fontLight, style.grey]}>{this.props.request.request_date}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                <APStatusLoader
                                                    status={this.props.request.status}
                                                    lang={this.props.lang}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={[globalStyles().section]}>
                                        <View style={style.table_row}>
                                            <View style={style.table_row_icon}>
                                                <APIcon name="location_on" size={26} color="#7cb342"/>
                                            </View>
                                            <View style={style.table_row_info}>
                                                <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["destination"][this.props.lang]}</Text>
                                                <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.props.request.city ? capitalize(this.props.request.city) + ", " + capitalize(this.props.request.country) : capitalize(this.props.request.country)}</Text>
                                            </View>
                                        </View>
                                        <View style={style.table_row}>
                                            <View style={style.table_row_icon}>
                                                <APIcon name="date_range" size={26} color="#7cb342"/>
                                            </View>
                                            <View style={style.table_row_info}>
                                                <View style={style.row}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["date_range"][this.props.lang] }</Text>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontLight]}>{" (" + this.props.request.nights + " " + translate["night"][this.props.lang] + " " + ")"}</Text>
                                                </View>
                                                <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.props.request.period_from + " — " + this.props.request.period_to}</Text>
                                            </View>
                                        </View>
                                        <View style={style.table_row}>
                                            <View style={style.table_row_icon}>
                                                <APIcon name="person" size={26} color="#7cb342"/>
                                            </View>
                                            <View style={style.table_row_info}>
                                                <View style={style.row}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["travelers"][this.props.lang] }</Text>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontLight]}>{" (" + this.props.request.number_of_people + ")"}</Text>
                                                </View>
                                                <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{travelersText}</Text>
                                            </View>
                                        </View>
                                        {
                                            this.props.request.price ?
                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="monetization_on" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["price"][this.props.lang]}</Text>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.props.request.price.split(",").map(el => el === "-1" ? "∞" : el).join(" - ") }</Text>
                                                </View>
                                            </View>:
                                            null
                                        }
                                        {
                                            this.props.request.desired_hotels ?
                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="hotel" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <View style={style.row}>
                                                        <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["hotels"][this.props.lang] }</Text>
                                                        {
                                                            this.props.request.hotel_category && parseInt(this.props.request.hotel_category) > 0 ?
                                                            <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontLight]}>{" (" + this.props.request.hotel_category + " " + translate["star"][this.props.lang] + ")"}</Text> :
                                                            null
                                                        }
                                                    </View>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.props.request.desired_hotels.join(", ") || ""}</Text>
                                                </View>
                                            </View> : null
                                        }
                                        {
                                            this.props.request.rooms && parseInt(this.props.request.rooms) > 0 ?
                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="meeting_room" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <View style={style.row}>
                                                        <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["rooms"][this.props.lang] }</Text>
                                                    </View>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.props.request.rooms + " " + translate["rooms_offer"][this.props.lang]}</Text>
                                                </View>
                                            </View> : null
                                        }
                                        {
                                            this.props.request.board_type ?
                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="restaurant" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["board-type"][this.props.lang]}</Text>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.generateExtra(this.props.request.board_type)}</Text>
                                                </View>
                                            </View>:
                                            null
                                        }
                                        {
                                            this.props.request.extra ?
                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="list" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["additional_options"][this.props.lang]}</Text>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{this.generateExtra(this.props.request.extra)}</Text>
                                                </View>
                                            </View>:
                                            null
                                        }
                                        {
                                            this.props.request.social_package === "true" ?

                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="business_center" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["social_package"][this.props.lang]}</Text>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{translate["included"][this.props.lang]}</Text>
                                                </View>
                                            </View>:
                                            null
                                        }
                                        {
                                            this.props.request.notes ?
                                            <View style={style.table_row}>
                                                <View style={style.table_row_icon}>
                                                    <APIcon name="insert_drive_file" size={26} color="#7cb342"/>
                                                </View>
                                                <View style={style.table_row_info}>
                                                    <Text numberOfLines={1} style={[globalStyles().title, globalStyles().fontBold]}>{translate["notes"][this.props.lang]}</Text>
                                                    <Text style={[globalStyles().text, globalStyles().fontLight, style.table_row_info_text]}>{urlify(this.props.request.notes)}</Text>
                                                </View>
                                            </View>:
                                            null
                                        }
                                        {
                                            !this.props.isArchive && this.props.request.status !== "booked" ?
                                            <View style={[style.row, style.delete]}>
                                                <TouchableWithoutFeedback onPress={this.onDelete}>
                                                    <View style={style.row}>
                                                        <APIcon name="delete" size={24} color="#bcbcbc"/>
                                                        <Text style={[globalStyles().title, globalStyles().fontBold, style.grey]}>{translate["delete"][this.props.lang]}</Text>
                                                    </View>
                                                </TouchableWithoutFeedback>
                                            </View> :
                                            null
                                        }
                                    </View>
                                </APSection>
                                {
                                    this.props.request.offers && this.props.request.offers.length > 0 ?
                                    <View>
                                        <View style={[style.row, style.offers]}>
                                            <APIcon name="work" size={26} color="#7cb342"/>
                                            <Text style={[style.title, globalStyles().fontBold, {paddingLeft: 7, paddingRight: 10}]}>{translate["offers"][this.props.lang]}</Text>
                                            <Text style={[style.title, globalStyles().fontBold, style.green]}>{this.props.request.offers.length}</Text>
                                        </View>
                                        <View style={{marginVertical: 5}}>
                                            {this.renderOffers()}
                                        </View>
                                    </View> : null
                                }

                            </View>
                        ) : null}
                    </APWrapper>
                    <APAlertPopup
                        isVisible={this.state.showAlert}
                        title={translate["cancel-question"][this.props.lang]}
                        text={translate["request-cancel-question"][this.props.lang]}
                        onSubmit={throttle(() => this.props.deleteRequest(this.props.request.request_id))}
                        hidePopup={this.hidePopup}
                        cancelable={false}
                    />
                </ScrollView>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    title: {
        fontSize: 20,
        lineHeight: 27,
        letterSpacing: 0.35,
        color: "#515151"
    },

    row: {
        flexDirection: "row",
        alignItems: "center",
    },

    top: {
        width: "100%",
        alignItems: "flex-start",
    },
    top_left: {
        flex: 1,
        paddingRight: 10
    },
    top_right: {
        width: 80
    },
    top_top: {
        marginBottom: 5,
        height: 28
    },
    green: {
        color: "#7cb342"
    },
    new: {
        paddingHorizontal: 7,
        paddingVertical: 1,
        backgroundColor: "#e9c32b",
        borderRadius: 5,
        height: 13
    },
    newText: {
        fontSize: 8,
        lineHeight: 11,
        color: "#ffffff",
        letterSpacing: 0.14
    },
    table_row: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 20
    },
    table_row_icon: {
        width: 54,
        justifyContent: "flex-start",
        alignItems: "flex-start",
        paddingLeft: 6.5
    },
    table_row_info: {
        flex: 1,
    },
    table_row_info_text: {
        paddingTop: 5,
        color: "#bcbcbc"
    },
    grey: {
        color: "#bcbcbc"
    },
    delete: {
        justifyContent: "flex-end",
        paddingRight: 5,
        paddingBottom: 5
    },
    offers: {
        paddingTop: 20,
        paddingBottom: 10,
        paddingLeft: 5
    },
    imageBackground: {
        width: "100%",
        height: 200,
        backgroundColor: "rgba(188, 188, 188, 0.4)",
        borderRadius: 10
    },

    offer: {
        width: "100%",
        height: 200,
        borderRadius: 10,
        padding: 20
    },

    unseen: {
        width: 12,
        height: 12,
        borderRadius: 12,
        backgroundColor: "#e9c32b"
    },
    offer_text: {
        color: "#fff",
        fontSize: 20,
        lineHeight: 27,
        letterSpacing: 0.35
    },
    offer_title: {
        color: "#fff",
        fontSize: 28,
        lineHeight: 38,
        letterSpacing: 0.49
    },
    offer_top: {
        justifyContent: "space-between",
        alignItems: "flex-start",
        flex: 1
    },
    offer_bottom: {
        justifyContent: "flex-end",
        flex: 1
    },
    overlay: {
        position: "absolute",
        backgroundColor: "rgba(0,0,0,.3)",
        top: 0,
        left: 0,
        width: "100%",
        height: 200,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    textShadow: {
        textShadowColor: 'rgba(0, 0, 0, 0.56)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 5
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        request: state.single.request,
        deleteRequestProcess: state.myRequest.deleteRequestProcess,
        prevPage: state.index.prevPage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRequest: (id) => { dispatch(get_request(id)) },
        deleteRequest: (id) => { dispatch(delete_request(id, "", "")) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleRequestScreen)
