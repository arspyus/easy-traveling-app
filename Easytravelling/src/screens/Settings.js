import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Keyboard } from 'react-native';
import { connect } from 'react-redux';

import { changePassword, updateUser, deactivate } from '../store/actions/userAction';
import { setMessage, show_confirmation_popup } from '../store/actions/indexAction';

import APButton from '../ui/button';
import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APExpendingView from '../ui/expendingView';
import APKeyboardAvoidingView from '../ui/keyboardAvoidingView';

import SettingsForm from '../components/settingsForm';
import ChangePasswordForm from '../components/changePasswordForm';
import DeactivateForm from '../components/deactivateForm';
import APAlertPopup from '../popups/alertPopup';

import translate from '../utils/translate';
import validate from '../utils/validate';
import languages from '../utils/languages';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';
import globalStyles from '../utils/style';

class SettingsScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    screenName = "settings";

    state = {
        form: {
            settings: {
                first: {
                    value: this.props.user.firstname,
                    isValid: true,
                    validationRules: [
                        { type: "name" }
                    ]
                },
                last: {
                    value: this.props.user.lastname,
                    isValid: true,
                    validationRules: [
                        { type: "name" }
                    ]
                },
                phone: {
                    value: this.props.user.phone ? this.props.user.phone.startsWith("+") ? this.props.user.phone : "+" + this.props.user.phone : "",
                    isValid: true,
                    validationRules: [
                        { type: "phone" }
                    ]
                },
                email: {
                    start_value: this.props.user.email || this.props.user.pending_email || "",
                    value: this.props.user.email || this.props.user.pending_email || "",
                    isValid: true,
                    validationRules: [
                        { type: "email" }
                    ]
                },
                language: {
                    value: this.props.lang,
                    isValid: true,
                    validationRules: [
                    ],
                    languages: languages.languagesObj,
                    defaultValue: this.props.lang
                }
            },

            changePassword: {
                current: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ]
                },
                new: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ],
                    error: false
                },
                retype: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ]
                },
                isFormActive: false
            },
            deactivate: {
                password: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ]
                },
                isFormActive: false
            }
        },
        _scrollView: null,
        showAlert: false,
        canShowVerifyLink: true
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.updateUserDone && nextProps.updateUserDone) {
            this.setState({
                ...this.state,
                form: {
                    ...this.state.form,
                    settings: {
                        ...this.state.form["settings"],
                        email: {
                            ...this.state.form["settings"]["email"],
                            start_value: this.state.form.settings.email.value
                        }
                    }
                },
                canShowVerifyLink: true
            })
        }

        if (!this.props.changePasswordDone && nextProps.changePasswordDone) {
            this.resetChangePasswordForm();
            this._changePasswordView.hide();
        }

        if (!this.props.deactivateDone && nextProps.deactivateDone) {
            this._deactivateView.hide();
        }
    }


    onChangeText = (text, mode, field) => {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [mode]: {
                    ...this.state.form[mode],
                    [field]: {
                        ...this.state.form[mode][field],
                        value: text
                    },
                    isFormActive: true
                }
            },
            canShowVerifyLink: field === "email" && mode === "settings" ? false : true
        })
    }

    updateValidAtionState = (mode, cb) => {
        let obj = { ...this.state.form[mode] };
        for (let field in this.state.form[mode]) {
            if (this.state.form[mode][field].validationRules)
                obj[field].isValid = validate(this.state.form[mode][field].validationRules, this.state.form[mode][field].value);

            if (mode === "settings" && field === "email") {
                obj[field].isValid = obj[field].isValid || (!this.props.user.email && !this.state.form[mode][field].value)
            }

            if (mode === "changePassword" && field === "retype") {
                obj[field].isValid = obj[field].isValid && this.state.form[mode][field].value === this.state.form[mode]["new"].value
            }
            if (mode === "changePassword" && field === "new") {
                if (!obj[field].isValid) {
                    obj[field].error = translate["password-hint"][this.props.lang]
                } else {
                    obj[field].error = false
                }
            }
        }
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [mode]: obj
            }
        }, cb)
    }

    isFormValid = (mode) => {
        let isValid = true;
        for (let field in this.state.form[mode]) {
            if (this.state.form[mode][field].validationRules && !this.state.form[mode][field].isValid) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    saveSettings = () => {
        Keyboard.dismiss();

        this.updateValidAtionState("settings", () => {
            if (this.isFormValid("settings")) {
                let data = {};
                data.lastname = this.state.form.settings.last.value.trim();
                data.firstname = this.state.form.settings.first.value.trim();
                data.phone = this.state.form.settings.phone.value.trim();
                data.language = this.state.form.settings.language.value;
                if (this.state.form.settings.email.value !== this.state.form.settings.email.start_value) {
                    data.email = this.state.form.settings.email.value.trim();
                }
                this.props.updateUser(data);
            } else {
                let data = {};
                this.props.showError("fill-form");
                data.language = this.state.form.settings.language.value;
                this.props.updateUser(data);
            }
        });
    }

    changePassword = () => {
        if (!this.state.form.changePassword.isFormActive) return;
        Keyboard.dismiss();
        this.updateValidAtionState("changePassword", () => {

            if (this.isFormValid("changePassword")) {
                const data = {
                    'current-password': this.state.form.changePassword.current.value,
                    'new-password': this.state.form.changePassword.new.value
                };
                this.props.changePassword(data);
            } else {
                this.props.showError("fill-form");
            }
        });
    }

    resetChangePasswordForm() {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                changePassword: {
                    ...this.state.form.changePassword,
                    current: {
                        ...this.state.form.changePassword.current,
                        value: ""
                    },
                    new: {
                        ...this.state.form.changePassword.new,
                        value: ""
                    },
                    retype: {
                        ...this.state.form.changePassword.retype,
                        value: ""
                    }
                }
            }
        })
    }

    deactivate = () => {
        if (!this.state.form.deactivate.isFormActive && !this.isSocialUser()) return;
        Keyboard.dismiss();
        this.updateValidAtionState("deactivate", () => {
            if (this.isFormValid("deactivate") || this.isSocialUser()) {
                this.setState({
                    showAlert: true
                })
            } else {
                this.props.showError("fill-form");
            }
        });
    }

    hidePopup = () => {
        this.setState({
            showAlert: false
        })
    }

    deactivateAccount = () => {
        let data = {};
        if(!this.isSocialUser()){
            data = { password: this.state.form.deactivate.password.value };
        } else {
            data = { is_social: true };
        }
        this.props.deactivate(data)
    }

    isSocialUser = () => {
        const user = this.props.firebase_user;
        if (!user) return false;
        let providerData = this.props.firebase_user.providerData;
        if (!providerData) return false;
        let providers = providerData.map((provider, ind) => {
            return provider.providerId;
        });
        return providers.indexOf("password") === -1;
    }

    render() {
        return (
            <APContainer
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })}
                title={translate["settings"][this.props.lang]}
                screenId={this.props.testID}
                screenComponent={this}
            >
                <APKeyboardAvoidingView>
                    <ScrollView
                        style={style.container}
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}
                        alwaysBounceVertical={false}
                        bounces={false}
                        ref={(ref) => { !this.state._scrollView && this.setState({ ...this.state, _scrollView: ref }) }}
                    >
                        <APWrapper containerStyles={{ paddingBottom: 30, paddingTop: 24, paddingHorizontal: 0 }} style={{width: "90%"}}>
                            <View style={style.formContainerWrapper} >
                                <SettingsForm
                                    form={this.state.form.settings}
                                    lang={this.props.lang}
                                    onChange={this.onChangeText}
                                    user={this.props.user}
                                    onSubmit={throttle(this.saveSettings)}
                                    _scrollView={this.state._scrollView}
                                    userDataIsLoading={this.props.userDataIsLoading}
                                    onVerify={this.props.showConfirmationPopup}
                                    showVerifyLink={this.props.user.user_status === "unconfirmed" && !this.props.userDataIsLoading && (this.props.user.email || this.props.user.pending_email) && this.state.canShowVerifyLink}
                                />
                                <View style={style.buttonContainer}>
                                    <APButton text={translate["save-changes"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.saveSettings)} style={{ ...globalStyles(true).fontLight }} />
                                </View>
                            </View>
                            {
                                !this.isSocialUser() ?
                                    <View>
                                        <APExpendingView icon="vpn_key" buttonText={translate["change-password"][this.props.lang]} ref={changePasswordView => this._changePasswordView = changePasswordView}>
                                            <ChangePasswordForm
                                                form={this.state.form.changePassword}
                                                lang={this.props.lang}
                                                onChange={this.onChangeText}
                                                onSubmit={throttle(this.changePassword)}
                                                _scrollView={this.state._scrollView}
                                            />
                                            <View style={style.buttonContainer}>
                                                <APButton
                                                    text={translate["save-changes"][this.props.lang]}
                                                    color={this.state.form.changePassword.isFormActive ? "#fff" : "#9F9F9F"}
                                                    backgroundColor={this.state.form.changePassword.isFormActive ? "#7cb342" : "#DFDFDF"}
                                                    onPress={throttle(this.changePassword)}
                                                    style={{ ...globalStyles(true).fontLight }}
                                                    removeShadow={!this.state.form.changePassword.isFormActive}
                                                />
                                            </View>
                                        </APExpendingView>
                                    </View>:
                                    null
                            }

                            <View>
                                <APExpendingView icon="warning" buttonText={translate["deactivate-account"][this.props.lang]} ref={deactivateView => this._deactivateView = deactivateView}>
                                    {
                                        !this.isSocialUser() ?
                                        <DeactivateForm
                                            form={this.state.form.deactivate}
                                            lang={this.props.lang}
                                            onChange={this.onChangeText}
                                            onSubmit={throttle(this.deactivate)}
                                            _scrollView={this.state._scrollView}
                                        /> : null
                                    }
                                    
                                    <View style={style.buttonContainer}>
                                        <APButton
                                            text={translate["deactivate"][this.props.lang]}
                                            color={this.state.form.deactivate.isFormActive || this.isSocialUser() ? "#fff" : "#9F9F9F"}
                                            backgroundColor={this.state.form.deactivate.isFormActive || this.isSocialUser() ? "#EF5350" : "#DFDFDF"}
                                            onPress={throttle(this.deactivate)}
                                            style={{ ...globalStyles(true).fontLight }}
                                            removeShadow={!this.state.form.deactivate.isFormActive}
                                        />
                                    </View>
                                </APExpendingView>
                            </View>
                        </APWrapper>
                        <APAlertPopup
                            isVisible={this.state.showAlert} 
                            title={translate["deactivate-question"][this.props.lang]}
                            text={translate["deactivate-account-question"][this.props.lang]}
                            onSubmit={throttle(() => this.deactivateAccount())}
                            hidePopup={this.hidePopup}
                            cancelable={true}
                        />
                    </ScrollView>
                </APKeyboardAvoidingView>
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "#fff"
    },
    formContainerWrapper: {
        height: "100%",
        flex: 1,
        marginBottom: 20
    },
    buttonContainer: {
        marginVertical: 14,
        width: "100%"
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        uid: state.user.uid,
        firebase_user: state.firebase.firebase_user,
        updateUserDone: state.user.updateUserProcess,
        changePasswordDone: state.user.changePasswordProcess,
        deactivateDone: state.user.deactivateProcess,
        userDataIsLoading: state.user.userDataIsLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateUser: (user) => { dispatch(updateUser(user)) },
        changePassword: (data) => { dispatch(changePassword(data)) },
        deactivate: (data) => { dispatch(deactivate(data)) },
        showError: (message) => { dispatch(setMessage(true, message)) },
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "verify"))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen)
