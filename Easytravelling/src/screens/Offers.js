import React, { Component } from 'react';
import { View, StyleSheet, Text, NetInfo, FlatList, RefreshControl, Platform } from 'react-native';
import { connect } from 'react-redux';

import APHeading from '../ui/heading';
import APContainer from '../ui/container';
import APFullLoader from '../ui/fullLoader';
import APSpinner from '../ui/spinner';
import APNothingFound from '../ui/nothingFound';

import SingleOffer from '../components/singleOffer';

import translate from '../utils/translate';
import isReachedToBottom from '../utils/isReachedToBottom';
import setNavigatorEvents from '../utils/setNavigatorEvents'; 
import navigate from '../utils/navigate';
import notification from '../utils/notification';

import {store_connection, store_connection_changed, setMessage, can_show_intro, checkExtraData, show_auth_popup, show_confirmation_popup} from '../store/actions/indexAction';
import { get_special_offers, reset_special_offers } from '../store/actions/offerAction';
import { select_type } from '../store/actions/makeRequestAction';
import { get_token } from '../store/actions/pushAction';
import { getUserData } from '../store/actions/userAction';

class OffersScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this);

        if(!props.uid && !props.isGuest){
            navigate(this, 'LandingScreen', 'resetTo', {}, true);
        }
    }

    screenName = "offers";

    willAppear() {
        this.props.connection === false && this.props.showError("went-offline");
        this.movingToSingleOffer = false;
        if(this.props.connection === "unknown"){
            NetInfo.getConnectionInfo().then((connectionInfo) => {
                if(connectionInfo.type === "none"){
                    this.props.setConnection(false);
                } else if(connectionInfo.type !== "unknown"){
                    this.props.setConnection(true);
                    this.props.setConnectionChanged();
                    this.start();
                }
            }).catch((ex)=> { 
                this.start()
            });
        } else {
           this.start();
        }
        
    }

    willDisappear() {
        if(!this.movingToSingleOffer)
            this.props.resetSpecialOffers();
    }

    componentWillUnmount() {
        this.willDisappear()
    }

    didAppear() {
        this.props.canShowIntro()
    }

    start() {
        if(this.props.prevPage !== "easytraveling.OfferScreen" || this.props.offers === null || this.props.offers_stored_tour_id){
            this.props.resetSpecialOffers();
            this.props.getSpecialOffers(this.props.tour_id, this.props.agency_name);
        }
        if(!this.props.isGuest){
            if(!this.props.fcm_token){
                this.getFcmToken();
            } else {
                notification(this);
            }
        }
        
        if(!this.props.prevPage || this.props.prevPage ===  "easytraveling.OffersScreen"){
            if(!this.props.isGuest){
                this.props.getUserData();
            }
            this.props.checkExtraData()
        }
    }

    getFcmToken() {
        this.props.getFCMToken();
    }

    componentWillReceiveProps(nextProps){
        if(!this.props.fcm_token && nextProps.fcm_token){
            notification(this);
        }
    }

    navigateToMakeRequest = (type) => {
        this.props.selectType(type)
        if(this.props.isGuest){
            this.props.showAuthPopup({
                screen: "MakeRequestScreen",
                params: {
                    type: type
                }
            });
            return;
        }

        if(this.props.user && this.props.user.user_status === "unconfirmed"){
            this.props.showConfirmationPopup();
            return;
        }

        navigate(this, "MakeRequestScreen", "resetTo", {
            type: type
        });
    }

    onSelect = (offer) => {
        let params = {
            offer: offer,
            lang: this.props.lang
        };

        if(this.props.isGuest){
            this.props.showAuthPopup({
                screen: "OfferScreen",
                params: params
            });
            return;
        }
        this.movingToSingleOffer = true;
        navigate(this, "OfferScreen", "push", params);
    }

    scrollTimer = null;

    onScroll = ($event) => {
        clearTimeout(this.scrollTimer)
        let ev = $event.nativeEvent;
        this.scrollTimer = setTimeout(() => {
            if(isReachedToBottom(ev) && !this.props.offers_loading){
                this.props.getSpecialOffers(this.props.tour_id, this.props.agency_name);
            }
        }, 100)
        
    }

    onRefresh = () => {
        this.props.getSpecialOffers(this.props.tour_id, this.props.agency_name, true);
    }

    render() {
        let offers = null;
        const bounces = Platform.OS !== "android" ;

        let refresh_control = <RefreshControl
                                    refreshing={this.props.offers_reset_loading}
                                    onRefresh={this.onRefresh}
                                    colors={["#7cb342"]}
                                    tintColor="#7cb342"
                                />
        if (this.props.offers) {
            if (this.props.offers.length > 0) {
                offers = <FlatList 
                    data={this.props.offers}
                    renderItem={({item, index}) => <SingleOffer isLast={this.props.offers.length - 1 === index} offer={item} lang={this.props.lang} key={item.id + "_" + index} onSelect={() => this.onSelect(item)} rate={true}/> }
                    keyboardShouldPersistTaps="always" 
                    keyExtractor={(item, index) => item.id + "_" + index}
                    onScroll={this.onScroll} 
                    showsVerticalScrollIndicator={false} 
                    alwaysBounceVertical={bounces}
                    bounces={bounces}
                    removeClippedSubviews={true}
                    contentContainerStyle={{paddingVertical: 20}}
                    refreshControl={
                        refresh_control
                    }
                />
            } else {
                offers = <FlatList 
                    data={[1]}
                    renderItem={({item, index}) => <APNothingFound text={translate["no-offers-found"][this.props.lang]} /> }
                    keyExtractor={(item, index) => index}
                    showsVerticalScrollIndicator={false} 
                    alwaysBounceVertical={bounces}
                    bounces={bounces}
                    removeClippedSubviews={true}
                    contentContainerStyle={{paddingVertical: 20, paddingTop:70}}
                    refreshControl={
                        refresh_control
                    }
                />
            }
        }


        return (
            <APContainer 
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true})} 
                plus={true} 
                onNavigate={this.navigateToMakeRequest} 
                screenId={this.props.testID}
                screenComponent={this}
            >
                <View style={[style.container]}>
                    {offers}
                    {this.props.offers_loading ? <APSpinner /> : null}
                </View>
                
            </APContainer>
        )
    }
}

const style = StyleSheet.create({
    container: {
        paddingTop: 0,
        flex: 1
    },
    agency_name: {
        textAlign: "center",
        paddingVertical: 12,
        color: "#424242",
        fontSize: 21,
        lineHeight: 23
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        uid: state.user.uid,
        isGuest: state.user.isGuest,
        prevPage: state.index.prevPage,
        offers: state.offers.offers,
        offers_loading: state.offers.offers_loading,
        offers_reset_loading: state.offers.offers_reset_loading,
        offers_stored_tour_id: state.offers.tour_id,
        fcm_token: state.index.fcm_token,
        connection: state.index.connection,
        connectionChanged : state.index.connectionChanged
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        getSpecialOffers: (tour_id, agency_name, reset) => { dispatch(get_special_offers( tour_id, agency_name, reset)) },
        resetSpecialOffers: () => {dispatch(reset_special_offers())},
        selectType: (type) => {dispatch(select_type(type))},
        getFCMToken: () => {dispatch(get_token())},
        setConnection: (connection) => {dispatch(store_connection(connection))},
        setConnectionChanged: () => dispatch(store_connection_changed(true)),
        showError: (message) => {dispatch(setMessage(true, message))},
        canShowIntro: () => {dispatch(can_show_intro())},
        checkExtraData: () => {dispatch(checkExtraData())},
        getUserData: () => {dispatch(getUserData())},
        showAuthPopup: (pending) => {dispatch(show_auth_popup(true, pending))},
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "activate"))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OffersScreen)
