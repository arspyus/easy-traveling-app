import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground, ScrollView, Dimensions, Keyboard, TouchableWithoutFeedback, Platform} from 'react-native';
import { connect } from 'react-redux';

import APHeading from '../ui/heading';
import APTabs from '../ui/tabs';
import APButton from '../ui/button';
import APLink from '../ui/link';
import APContainer from '../ui/container';
import APWrapper from '../ui/wrapper';
import APComfirmMessage from '../ui/comfirmMessage';
import APKeyboardAvoidingView from '../ui/keyboardAvoidingView';
import AuthAnim from '../animations/authAnimation';
import APSocialButtons from '../components/socialButtons';
import APIcon from '../ui/icon';
import APBackButton from '../ui/backButton';

const bgImage = require('./../../assets/images/login-register-bg.jpg');

import translate from '../utils/translate';
import validate from '../utils/validate';
import throttle from '../utils/throttle';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import navigate from '../utils/navigate';

import SignupForm from '../components/signupForm';
import SigninForm from '../components/signinForm';
import ResetPasswordForm from '../components/resetPasswordForm';

import { signIn, signUp, resetPassword } from '../store/actions/userAction';
import { setMessage, show_auth_popup } from '../store/actions/indexAction';

class AuthScreen extends Component {
    constructor(props) {
        super(props);
        setNavigatorEvents(this);
    }

    state = {
        tabItems: [translate["login"][this.props.lang], translate["registration"][this.props.lang]],
        selectedTab: this.props.selectedTabIndex ? this.props.selectedTabIndex : 0,
        isForgetPasswordMode: false,
        comfirmMessage: false,
        bgImageHeight: Dimensions.get('window').height * 3 / 10,
        _scrollView: null,
        form: {
            signup: {
                first: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "name" }
                    ]
                },
                last: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "name" }
                    ]
                },
                phone: {
                    value: "+374",
                    isValid: true,
                    validationRules: [
                        { type: "phone" }
                    ]
                },
                email: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "email" }
                    ]
                },
                password: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ],
                    error: false
                },
                retype: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ]
                }
            },
            signin: {
                email: {
                    value: this.props.user && this.props.user.email ? this.props.user.email : "",
                    isValid: true,
                    validationRules: [
                        { type: "email" }
                    ]
                },
                password: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "password" }
                    ]
                }
            },
            resetPassword: {
                email: {
                    value: "",
                    isValid: true,
                    validationRules: [
                        { type: "email" }
                    ]
                }
            }
        }
    }

    componentDidMount() {
        Dimensions.addEventListener('change', this.changeDimensions);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.changeDimensions)
    }

    changeDimensions = () => {
        this.setState({
            bgImageHeight: Dimensions.get('window').height * 3 / 10
        });
    }

    willDisappear() {
        this.props.resetAuthPopup();
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.signInDone && nextProps.signInDone) {
            if(this.props.prevPage !== "easytraveling.LandingScreen"){
                this.navigateToApp();
            }
            
        }

        if (!this.props.resetPasswordDone && nextProps.resetPasswordDone) {
            this.toggleForgetPassword(false)
        }
    }


    onTabChange = selectedTab => {
        this.setState({
            ...this.state,
            selectedTab: selectedTab,
            isForgetPasswordMode: false
        });
        Keyboard.dismiss();
    };

    updateValidAtionState = (mode, cb) => {
        let obj = { ...this.state.form[mode] };
        for (let field in this.state.form[mode]) {
            obj[field].isValid = validate(this.state.form[mode][field].validationRules, this.state.form[mode][field].value);
            if (mode === "signup" && field === "password") {
                if(!obj[field].isValid){
                    obj[field].error = translate["password-hint"][this.props.lang]
                } else {
                    obj[field].error = false
                }
                
            }

            if (mode === "signup" && field === "retype") {
                obj[field].isValid = obj[field].isValid && this.state.form[mode][field].value === this.state.form[mode]["password"].value
            }
        }
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [mode]: obj
            }
        }, cb)
    }

    onCreateAccount = () => {
        Keyboard.dismiss();
        this.updateValidAtionState("signup", () => {
            if (this.isFormValid("signup")) {
                let d = {
                    "email": this.state.form.signup.email.value,
                    "password": this.state.form.signup.password.value,
                    "firstname": this.state.form.signup.first.value,
                    "lastname": this.state.form.signup.last.value,
                    "phone": this.state.form.signup.phone.value,
                    "language": this.props.lang
                }
                this.props.signUp(d)
            } else {
                this.props.showError("fill-form")
            }
        });
    }

    signIn = () => {
        Keyboard.dismiss();
        this.updateValidAtionState("signin", () => {
            if (this.isFormValid("signin")) {
                let d = {
                    "email": this.state.form.signin.email.value,
                    "password": this.state.form.signin.password.value,
                    "language": this.props.lang
                }
                this.props.signIn(d)
            } else {
                this.props.showError("fill-form");
            }
        });

    }

    onResetPassword = () => {
        Keyboard.dismiss();
        this.updateValidAtionState("resetPassword", () => {
            if (this.isFormValid("resetPassword")) {
                this.props.resetPassword(this.state.form.resetPassword.email.value)
            } else {
                this.props.showError("fill-form");
            }
        })
    }

    navigateToApp() {
        if(!this.props.pendingToRedirectData){
            navigate(this, 'OffersScreen', 'resetTo', {})
        } else {
            navigate(this, this.props.pendingToRedirectData.screen, 'resetTo', this.props.pendingToRedirectData.params)
        }
        
    }

    isFormValid = (mode) => {
        let isValid = true;
        for (let field in this.state.form[mode]) {
            if (!this.state.form[mode][field].isValid) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    onChangeText = (text, mode, field) => {
        this.setState({
            form: {
                ...this.state.form,
                [mode]: {
                    ...this.state.form[mode],
                    [field]: {
                        ...this.state.form[mode][field],
                        value: text
                    }
                }
            }

        })
    }

    resetRegisterForm() {
        let newEmail = this.state.form.signup.email.value;
        this.setState({
            ...this.state,
            selectedTab: 0,
            comfirmMessage: false,
            form: {
                ...this.state.form,
                signup: {
                    ...this.state.form.signup,
                    email: {
                        ...this.state.form.signup.email,
                        value: "",
                        isValid: true
                    },
                    phone: {
                        ...this.state.form.signup.phone,
                        value: "+374",
                        isValid: true
                    },
                    first: {
                        ...this.state.form.signup.first,
                        value: "",
                        isValid: true
                    },
                    last: {
                        ...this.state.form.signup.last,
                        value: "",
                        isValid: true
                    },
                    password: {
                        ...this.state.form.signup.password,
                        value: "",
                        isValid: true
                    },
                    retype: {
                        ...this.state.form.signup.retype,
                        value: "",
                        isValid: true
                    }
                },
                resetPassword: {
                    ...this.state.form.resetPassword,
                    email: {
                        ...this.state.form.resetPassword.email,
                        value: newEmail,
                        isValid: true
                    }
                },
                signin: {
                    ...this.state.form.signin,
                    email: {
                        ...this.state.form.signin.email,
                        value: newEmail,
                        isValid: true
                    },
                    password: {
                        ...this.state.form.signin.password,
                        value: "",
                        isValid: true
                    }
                }
            }

        })
    }

    toggleForgetPassword = (mode) => {
        this.setState({
            ...this.state,
            isForgetPasswordMode: mode
        })
    }

    onBack = () => {
        navigate(this, null, "pop");
    }

    render() {
        let height = 0;
        if (this.state.selectedTab === 1) {
            height = "auto"
        }
        let formContent = (
            <AuthAnim tabIndex={this.state.selectedTab}>
                <View style={style.formContainerWrapper} >
                    <APWrapper containerStyles={{ paddingBottom: 38, paddingTop: 24, paddingHorizontal: 0 }} style={{width: "90%"}}>
                        <APHeading text={translate["login"][this.props.lang]} />
                        {this.state.comfirmMessage ? <APComfirmMessage text={translate["registration-success"][this.props.lang]} /> : null}
                        <SigninForm 
                            form={this.state.form.signin} 
                            lang={this.props.lang} 
                            onChange={this.onChangeText} 
                            onSubmit={throttle(this.signIn)}
                            _scrollView={this.state._scrollView}

                        />
                        <View style={style.buttonContainer}>
                            <APButton text={translate["login"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.signIn)} />
                        </View>
                        <APLink onClick={() => { this.toggleForgetPassword(true) }}>{translate["forgot-password"][this.props.lang]}</APLink>
                        <View style={style.social}>
                            <View style={style.socialContainer}>
                                <APSocialButtons lang={this.props.lang} page="auth"/>
                            </View>
                        </View>
                    </APWrapper>

                </View>
                <View style={[style.formContainerWrapper, { backgroundColor: "#fff", height: height }]} >
                    <APWrapper>

                        <APHeading text={translate["registration"][this.props.lang]} />
                        <SignupForm 
                            form={this.state.form.signup} 
                            lang={this.props.lang} 
                            onChange={this.onChangeText} 
                            onSubmit={throttle(this.onCreateAccount, true)}
                            _scrollView={this.state._scrollView}
                        />
                        <View style={style.buttonContainer}>
                            <APButton text={translate["create-account"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.onCreateAccount)} />
                        </View>
                        <View style={style.social}>
                            <View style={style.socialContainer}>
                                <APSocialButtons lang={this.props.lang} page="auth"/>
                            </View>
                        </View>
                    </APWrapper>

                </View>
            </AuthAnim>
        )

        if (this.state.isForgetPasswordMode) {
            formContent = (
                <APWrapper>
                    <APHeading text={translate["reset-password"][this.props.lang]} />
                    <ResetPasswordForm 
                        form={this.state.form.resetPassword} 
                        lang={this.props.lang} 
                        onChange={this.onChangeText} 
                        onSubmit={throttle(this.onResetPassword, true)}
                        _scrollView={this.state._scrollView}
                    />
                    <View style={style.buttonContainer}>
                        <APButton text={translate["reset"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.onResetPassword)} />
                    </View>
                    <APLink onClick={() => { this.toggleForgetPassword(false) }}>{translate["cancel"][this.props.lang]} </APLink>
                </APWrapper>
            )
        }
        return (
            <APContainer 
                onMenu={() => this.props.navigator.toggleDrawer({ side: 'left', animated: true })} 
                header={false} 
                showOffline={true} 
                screenId={this.props.testID}
                style={{backgroundColor: "#fff"}}
                screenComponent={this}
            >
                <APKeyboardAvoidingView>
                    <ScrollView
                        style={[style.container]}
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}
                        alwaysBounceVertical={false}
                        bounces={false}
                        ref={(ref)=>{!this.state._scrollView && this.setState({...this.state, _scrollView: ref})}}
                    >
                
                    <ImageBackground source={bgImage} style={[style.bgImage, { height: this.state.bgImageHeight }]}>
                        <APBackButton onBack={this.onBack}/>
                        <View >
                            <APTabs
                                initialIndex={this.props.selectedTabIndex}
                                items={this.state.tabItems}
                                onChange={this.onTabChange}
                                selectedTab={this.state.selectedTab}
                                disableNativeDriver={true}
                            />
                        </View>
                    </ImageBackground>
                    
                    {formContent}

                </ScrollView>                   
                </APKeyboardAvoidingView>

            </APContainer>
        )
    }
}

const style = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    formContainerWrapper: {
        height: "100%",
        flex: 1,
        marginBottom: 14,
        overflow: "hidden"
    },
    bgImage: {
        width: "100%",
        justifyContent: "flex-end"
    },
    buttonContainer: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        marginBottom: 10,
        marginTop: -12
    },
    social: {
        alignItems: "center"
    },
    socialContainer: {
        width: "80%"
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        prevPage: state.index.prevPage,
        signInDone: state.user.signInProcess,
        resetPasswordDone: state.user.resetPasswordProcess,
        fcm_token: state.index.fcm_token,
        ios_toolbar_height: state.index.ios_toolbar_height,
        pendingToRedirectData: state.index.pendingToRedirectData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (data) => { dispatch(signIn(data, false)) },
        signUp: (data) => { dispatch(signUp(data)) },
        resetPassword: (email) => { dispatch(resetPassword(email)) },
        showError: (message) => { dispatch(setMessage(true, message)) },
        resetAuthPopup: () => {dispatch(show_auth_popup(false, null))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen)
