import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Dimensions, ImageBackground, TouchableWithoutFeedback, Text, Image , Platform} from 'react-native';
import { connect } from 'react-redux';

import APDivider from '../ui/divider';
import APIcon from '../ui/icon';
import MenuItem from '../components/menuItem';
import APButton from '../ui/button';

import translate from '../utils/translate';
import openLink from '../utils/openLink';
import throttle from '../utils/throttle';
import navigate from '../utils/navigate';
import setNavigatorEvents from '../utils/setNavigatorEvents';
import globalStyles from '../utils/style';
import getAvatar from '../utils/getAvatar';
import isLandscape from '../utils/isLandscape';

import { show_bonus_popup, show_auth_popup, show_confirmation_popup } from '../store/actions/indexAction';
import { logout } from '../store/actions/userAction';
import { getBonus } from '../store/actions/bonusAction';
import { select_type } from '../store/actions/makeRequestAction';
import { clear_unread_notification } from '../store/actions/pushAction';

const bgImage = require('../../assets/images/menu_bg.png');

class SidebarScreen extends Component {

    constructor(props) {
        super(props);
        setNavigatorEvents(this)
    }

    componentDidMount() {
        getAvatar()
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.logoutDone && nextProps.logoutDone) {
            this.navigateTo("Landing", true);
            this.props.clearUnreadNotifications();
        }
    }

    navigateTo(screen, dontToggleDrawer) {
        if (screen === "MakeRequest") {
            this.props.selectType(null);

            if(this.props.user && this.props.user.user_status === "unconfirmed"){
                this.props.showConfirmationPopup();
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'close'
                })
                return;
            }
        }
        if(!dontToggleDrawer){
            this.props.navigator.toggleDrawer({
                side: 'left',
                animated: true,
                to: 'close'
            })
        }
        
        if (this.props.activeNav && screen.toLowerCase() === this.props.activeNav.toLowerCase()) return;

        if(Platform.OS === "android"){
            navigate(this, screen + "Screen", "resetTo", null);
        } else {
            this.props.navigator.handleDeepLink({
                link: screen + 'Screen/resetTo',
                payload: null
            });
        }
    }

    openTerms = () => {
        const url = this.props.lang === "en" ? "https://easytraveling.am/terms" : this.props.lang === "ru" ? "https://easytraveling.am/ru/условия/" : "https://easytraveling.am/hy/պայմաններ/";
        openLink(url);
    }

    signOut = () => {
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'close'
        })
        this.props.logout();
    }

    signIn = () => {
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'close'
        })
        if(Platform.OS === "android"){
            navigate(this, 'AuthScreen', "push", {
                selectedTabIndex: 0
            });
        } else {
            this.props.navigator.handleDeepLink({
                link: "AuthScreen/push",
                payload: {
                    selectedTabIndex: 0
                }
            });
        }
    }

    getBonus = () => {
        this.props.getBonus();
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'close'
        })
    }

    showBonus = () => {
        this.props.showBonus();
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'close'
        })
    }

    showAuthPopup = (screen) => {
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'close'
        })
        this.props.showAuthPopup({
            screen: screen,
            params: null
        });
    }

    render() {
        let bonusButton = null;
        if (this.props.user && this.props.user.bonus > 0) {
            bonusButton = (<TouchableWithoutFeedback onPress={throttle(this.getBonus)}>
                <View>
                    <Text style={[style.getbonusBtn, globalStyles().fontBold]}>{translate["get-bonus"][this.props.lang]}</Text>
                </View>
            </TouchableWithoutFeedback>)
        } else {
            bonusButton = <Text style={[style.getbonusBtn, globalStyles().fontBold, style.disabled]}>{translate["get-bonus"][this.props.lang]}</Text>
        }

        if (this.props.user && this.props.user.bonus_id) {
            bonusButton = (<TouchableWithoutFeedback onPress={throttle(this.showBonus)}>
                <View>
                    <Text style={[style.getbonusBtn, globalStyles().fontBold]}>{translate["view-bonus-id"][this.props.lang]}</Text>
                </View>
            </TouchableWithoutFeedback>)
        }

        let drawerLeftPadding = 16 + this.props.iphoneXSpace.left;
        return (
            <View style={[style.container]}>
                <ScrollView 
                    showsVerticalScrollIndicator={false}
                    alwaysBounceVertical={false}
                    bounces={false}
                    scrollEnabled={true}
                >
                    <ImageBackground source={bgImage} style={[style.bgImage, {height: Platform.OS === "android" ? 165 : 165 + this.props.ios_toolbar_height, paddingTop: Platform.OS === "android" ? 0 : this.props.ios_toolbar_height}]}>
                        {!this.props.isGuest ?
                            <View style={[style.avatarWrapper, {paddingLeft: drawerLeftPadding}]}>
                                <View>
                                    <TouchableWithoutFeedback onPress={() => this.navigateTo("Settings")}>
                                        <View style={style.avatar}>
                                            {
                                                this.props.avatar ?
                                                    <Image source={{ uri: this.props.avatar }} style={style.avatarThumbnail}></Image> :
                                                    <Text style={[style.avatarFirstLetter, globalStyles().fontBold]}>{this.props.user && this.props.user.firstname ? this.props.user.firstname.charAt(0).toUpperCase() : ""}</Text>
                                            }
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <TouchableWithoutFeedback onPress={() => this.navigateTo("Settings")}>
                                    <View style={style.avatarName}>
                                        <Text style={[style.avatarNameText, globalStyles().fontBold]}>
                                            { this.props.user ? this.props.user.firstname + " " + this.props.user.lastname : ""}
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                
                                <View style={style.bonusContainer}>
                                    <View>
                                        <Text style={[style.bonusText, globalStyles().fontLight]}>{this.props.user ? translate["my-bonus"][this.props.lang] + this.props.user['bonus'] : ''} </Text>
                                    </View>
                                    <View>
                                        <TouchableWithoutFeedback>
                                            <View style={style.getbonusBtnWrap}>
                                                {bonusButton}
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View> 
                            </View> :
                            <View style={style.guestAvatar}>
                                <APButton 
                                    text={translate["login"][this.props.lang]} 
                                    color="#fff" 
                                    backgroundColor="transparent" 
                                    onPress={throttle(this.signIn)} 
                                    containerStyle={{borderWidth: 1, borderColor: "#fff"}}
                                    hoverEffect={true}
                                />
                            </View>
                        }
                    </ImageBackground>
                    <View style={[style.listContainer]}>
                        <MenuItem 
                            onClick={() => { this.navigateTo("MakeRequest") }} 
                            activeItem={this.props.activeNav} 
                            label="makerequest"
                            paddingLeft={drawerLeftPadding}
                            text={translate["make-request"][this.props.lang]}
                            disabled={this.props.isGuest} 
                            onDisabled={() => { this.showAuthPopup("MakeRequestScreen") }}
                        />
                        <MenuItem 
                            onClick={() => { this.navigateTo("MyRequests") }} 
                            activeItem={this.props.activeNav} 
                            label="myrequests"
                            paddingLeft={drawerLeftPadding}
                            text={translate["my-requests"][this.props.lang]}
                            notification={this.props.unreadOffer} 
                            disabled={this.props.isGuest}
                            onDisabled={() => { this.showAuthPopup("MyRequestsScreen") }}
                        /> 
                        <APDivider />
                        <MenuItem 
                            onClick={() => { this.navigateTo("Offers") }} 
                            activeItem={this.props.activeNav} 
                            label="offers"
                            paddingLeft={drawerLeftPadding}
                            text={translate["special-offers-menu"][this.props.lang]}
                            notification={this.props.unreadSpecialOffer}
                        />
                        <APDivider />
                        <MenuItem 
                            onClick={() => { this.navigateTo("Settings") }} 
                            activeItem={this.props.activeNav} 
                            label="settings"
                            paddingLeft={drawerLeftPadding}
                            text={translate["settings"][this.props.lang]}
                            disabled={this.props.isGuest}
                            onDisabled={() => { this.showAuthPopup("SettingsScreen") }} 
                        />
                        <MenuItem 
                            onClick={() => { this.navigateTo("About") }} 
                            activeItem={this.props.activeNav} 
                            label="about"
                            paddingLeft={drawerLeftPadding}
                            text={translate["menu-about"][this.props.lang]} 
                        />
                        <MenuItem 
                            onClick={this.openTerms} 
                            activeItem={this.props.activeNav} 
                            label="terms"
                            paddingLeft={drawerLeftPadding}
                            text={translate["terms"][this.props.lang]} 
                        />
                        <APDivider />
                        <View style={[style.iconsWrapper, {paddingLeft: drawerLeftPadding}]}>
                            <TouchableWithoutFeedback onPress={() => { openLink("tel:+37496886655") }}>
                                <View style={style.icon}>
                                    <APIcon name="phone" size={24} color="#7cb342" />
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => { openLink("mailto:info@easytraveling.am") }}>
                                <View style={style.icon}>
                                    <APIcon name="email" size={24} color="#7cb342" />
                                </View>

                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => { openLink("https://www.facebook.com/EasyTraveling-214959275757931/") }}>
                                <View style={style.icon} >
                                    <APIcon name="facebook" size={24} color="#3b5999" />
                                </View>

                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => { openLink("https://www.instagram.com/easytraveling_app/") }}>
                                <View style={style.icon}>
                                    <APIcon name="instagram" size={24} color="#e4405f" />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        <APDivider />
                        { 
                            !this.props.isGuest ? 
                            <TouchableWithoutFeedback onPress={throttle(this.signOut)}>
                                <View style={[style.iconsWrapper, {paddingLeft: drawerLeftPadding}]}>

                                    <View style={style.icon}>
                                        <APIcon name="exit_to_app" size={24} color="rgba(0,0,0,0.54)" />
                                    </View>
                                    <View style={style.icon}>
                                        <Text style={[style.listItemText, globalStyles().fontBold]}>{translate["log-out"][this.props.lang]}</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>: 
                            <TouchableWithoutFeedback onPress={throttle(this.signIn)}>
                                <View style={[style.iconsWrapper, {paddingLeft: drawerLeftPadding}]}>
                                    <View style={style.icon}>
                                        <APIcon name="exit_to_app" size={24} color="rgba(0,0,0,0.54)" />
                                    </View>
                                    <View style={style.icon}>
                                        <Text style={[style.listItemText, globalStyles().fontBold]}>{translate["login"][this.props.lang]}</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        }

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: Platform.OS === "android" ? !isLandscape() ? Dimensions.get("window").width * 8 / 10 : Dimensions.get("window").height * 8 / 10 : "100%",
        backgroundColor: "#fff",
        flex: 1
    },

    bgImage: {
        backgroundColor: "#444444",
        marginBottom: 8

    },
    avatarWrapper: {
        paddingRight: 16,
        paddingTop: 16
    },

    avatar: {
        backgroundColor: "#7cb342",
        width: 64,
        height: 64,
        borderRadius: 32,
        justifyContent: "center",
        alignItems: "center"
    },

    guestAvatar: {
        justifyContent: "flex-end",
        paddingHorizontal: 16,
        height: "100%",
        paddingBottom: 32
    },

    avatarThumbnail: {
        width: 64,
        height: 64,
        borderRadius: 32
    },

    avatarFirstLetter: {
        color: "#fff",
        fontSize: 42,
        lineHeight: 64
    },

    avatarName: {
        marginTop: 16,
        flexDirection: "row",
        alignItems: "center"
    },

    avatarNameText: {
        lineHeight: 24,
        fontSize: 14,
        color: "#fff"
    },

    bonusContainer: {
        paddingBottom: 16,
        flexDirection: "row"
    },

    bonusText: {
        lineHeight: 24,
        fontSize: 14,
        color: "#fff"
    },

    getbonusBtn: {
        color: "#7cb342",
        lineHeight: 24,
        color: "#7cb342"
    },

    disabled: {
        color: "#9e9e9e"
    },

    getbonusBtnWrap: {
        marginLeft: 10
    },

    listContainer: {
        flex: 1,
        backgroundColor: "#fff"
    },

    listItem: {
        height: 48,
        paddingRight: 16,
        justifyContent: "flex-start",
        flexDirection: "row"
    },

    listItemText: {
        lineHeight: 48,
        fontSize: 14,
        color: "rgba(0,0,0,0.87)"
    },

    iconsWrapper: {
        height: 54,
        paddingRight: 16,
        alignItems: "center",
        flexDirection: "row"
    },
    icon: {
        marginRight: 20
    },

    avatarSignInWrap: {
        marginLeft: 20
    },

    avatarSignIn: {
        textDecorationLine: "underline"
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user,
        isGuest: state.user.isGuest,
        logoutDone: state.user.logoutProcess,
        activeNav: state.index.activeNav,
        unreadOffer: state.index.unreadOffer,
        unreadSpecialOffer: state.index.unreadSpecialOffer,
        avatar: state.index.avatar,
        ios_toolbar_height: state.index.ios_toolbar_height,
        iphoneXSpace: state.index.iphoneXSpace
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => { dispatch(logout()) },
        getBonus: () => { dispatch(getBonus()) },
        showBonus: () => { dispatch(show_bonus_popup(true)) },
        selectType: (type) => { dispatch(select_type(null)) },
        clearUnreadNotifications: () => {dispatch(clear_unread_notification("all"))},
        showAuthPopup: (pending) => {dispatch(show_auth_popup(true, pending))},
        showConfirmationPopup: () => {dispatch(show_confirmation_popup(true, "activate"))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarScreen)
