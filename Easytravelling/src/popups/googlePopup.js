import React, { Component } from 'react';
import { View, Text, Keyboard, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import APGoogleBtn from '../ui/googleBtn';

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import validate from '../utils/validate';
import popupStyles from './popupStyles';
import throttle from '../utils/throttle';

import { show_google_popup } from '../store/actions/indexAction';

class APGooglePopup extends Component {

    makePassWordHintText = () => {
        const text = translate["google-hint-text"][this.props.lang];
        const regex = /\{email\}/g;
        const splitted = text.split(regex);

        return (
            <Text>
                <Text>{splitted[0]}</Text>
                <Text style={[style.modalText, globalStyles().fontBold]}>{this.props.email}</Text>
                <Text>{splitted[1]}</Text>
            </Text>
        );

    }

    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                backdropOpacity={0.5}
                style={{ justifyContent: "flex-end", margin: 0 }}
                onBackdropPress={this.props.hideModal}
                onBackButtonPress={this.props.hideModal}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["google-signin-head"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View style={style.container}>
                                <Text style={[style.modalText, globalStyles().fontLight]}>{this.makePassWordHintText()}</Text>
                                <View style={style.bnContainer}>
                                    <APGoogleBtn email={this.props.email} lang={this.props.lang} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        paddingBottom: 10
    },
    modalText: {
        fontSize: 14,
        lineHeight: 22,
        color: "#464653",
        textAlign: "left",
        paddingTop: 10,
        paddingBottom: 26
    },
    btnContainer: {
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
        flex: 1,
        flexDirection: "row",
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideModal: () => {dispatch(show_google_popup(false, ""))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APGooglePopup)
