import React, { Component } from 'react';
import { View, Text, Platform } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import TouchableNativeFeedback from '../ui/touchableNativeFeedback'

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import openLink from '../utils/openLink';

import popupStyles from './popupStyles';

import { app_should_upgrade } from '../store/actions/indexAction';
import config from '../config/config';

class APUpgradePopup extends Component {
    onSubmit = () => {
        openLink(Platform.OS === "android" ? config.market_url.android : config.market_url.ios);
    }

    render() {
        return (
            <Modal 
                isVisible={this.props.isVisible} 
                backdropOpacity={0.5}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["major-update-title"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View>
                                <Text style={[popupStyles.modalText, globalStyles().fontLight]}>{translate["major-update-content"][this.props.lang]}</Text>
                            </View>
                        </View>
                    </View>
                        <View style={popupStyles.modalBottom}>
                            <TouchableNativeFeedback onPress={this.onSubmit}>
                                <View style={popupStyles.modalButtonCont}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["major-update-update-btn"][this.props.lang]}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                        </View>
                </View>
            </Modal>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

export default connect(mapStateToProps, null)(APUpgradePopup)
