import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import TouchableNativeFeedback from '../ui/touchableNativeFeedback'

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import popupStyles from './popupStyles';

class APAlertPopup extends Component {
    onClose = () => {
        this.props.hidePopup();
    }

    onSubmit = () => {
        this.props.onSubmit();
        this.props.hidePopup();
    }

    render() {
        return (
            <Modal 
                isVisible={this.props.isVisible} 
                backdropOpacity={0.5}
                onBackdropPress={() => this.props.cancelable ? this.onClose() : null }
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{this.props.title}</Text>
                        <View style={popupStyles.modalContent}>
                            <View>
                                <Text style={[popupStyles.modalText, globalStyles().fontLight]}>{this.props.text}</Text>
                            </View>
                        </View>
                    </View>
                        <View style={popupStyles.modalBottom}>
                            <TouchableNativeFeedback onPress={this.onClose}>
                                <View style={[popupStyles.modalButtonCont, popupStyles.modalLeftButtonCont]}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["no"][this.props.lang].toUpperCase()}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                            <TouchableNativeFeedback onPress={this.onSubmit}>
                                <View style={popupStyles.modalButtonCont}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["yes"][this.props.lang].toUpperCase()}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                        </View>
                </View>
            </Modal>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

export default connect(mapStateToProps, null)(APAlertPopup)
