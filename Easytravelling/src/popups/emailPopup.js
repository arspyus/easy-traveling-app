import React, { Component } from 'react';
import { View, Text, Keyboard, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import APInput from '../ui/input';
import APButton from '../ui/button';

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import validate from '../utils/validate';
import popupStyles from './popupStyles';
import throttle from '../utils/throttle';


import { show_email_popup } from '../store/actions/indexAction';
import { updateUser } from '../store/actions/userAction';
class APEmailPopup extends Component {
    state = {
        isValid: true,
        value: "",
        error: false
    }

    onSubmit = () => {
        Keyboard.dismiss();
        const isValid = validate([{type: "email"}], this.state.value);

        this.setState({
            ...this.state,
            isValid : isValid
        }, () => {
            if(isValid){
                this.props.updateUser({
                    email: this.state.value,
                    firstname: this.props.user.firstname,
                    lastname: this.props.user.lastname,
                    phone: this.props.user.phone,
                    language: this.props.lang
                });
                this.hidePopup(); 
            };
        })
        
    }

    hidePopup = () => {
        this.setState({
            isValid: true,
            value: "",
            error: false
        });
        this.props.hidePopup();
    }

    onChange = (text) => {
        this.setState({
            ...this.state,
            value: text
        })
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                backdropOpacity={0.5}
                style={{ justifyContent: "flex-end", margin: 0 }}
                onBackdropPress={this.hidePopup}
                onBackButtonPress={this.hidePopup}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["email-popup-head"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View style={style.container}>
                                <Text style={[style.modalText, globalStyles().fontLight]}>{translate["email-popup-text"][this.props.lang]}</Text>
                                <APInput
                                    label={translate["email"][this.props.lang]}
                                    isValid={this.state.isValid}
                                    value={this.state.value}
                                    onChangeText={this.onChange}
                                    onSubmitEditing={this.onSubmit}
                                    returnKeyType={"go"}
                                />
                                <View style={style.btnContainer}>
                                    <APButton text={translate["confirmation-popup-button"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.onSubmit)} />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%"
    },
    modalText: {
        fontSize: 14,
        lineHeight: 22,
        color: "#464653",
        textAlign: "left",
        paddingBottom: 10
    },
    btnContainer: {
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
        flex: 1,
        flexDirection: "row",
        marginBottom: 40,
        marginTop: -20
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        user: state.user.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hidePopup: (close) => { dispatch(show_email_popup(false)) },
        updateUser: (user) => { dispatch(updateUser(user))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APEmailPopup)
