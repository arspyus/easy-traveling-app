import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import TouchableNativeFeedback from '../ui/touchableNativeFeedback'

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import popupStyles from './popupStyles';


import { show_auth_popup } from '../store/actions/indexAction';
class APAuthPopup extends Component {
    onClose = () => {
        this.props.hidePopup(true);
    }

    onSubmit = () => {
        this.props.onSignIn();
        this.props.hidePopup(false);
    }

    render() {
        return (
            <Modal 
                isVisible={this.props.isVisible} 
                backdropOpacity={0.5}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["signup-popup-head"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View>
                                <Text style={[popupStyles.modalText, globalStyles().fontLight]}>{translate["signup-popup-text"][this.props.lang]}</Text>
                            </View>
                        </View>
                    </View>
                        <View style={popupStyles.modalBottom}>
                            <TouchableNativeFeedback onPress={this.onClose}>
                                <View style={[popupStyles.modalButtonCont, popupStyles.modalLeftButtonCont]}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["cancel"][this.props.lang]}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                            <TouchableNativeFeedback onPress={this.onSubmit}>
                                <View style={popupStyles.modalButtonCont}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["signup-popup-button"][this.props.lang]}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                        </View>
                </View>
            </Modal>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hidePopup: (close) => {dispatch(show_auth_popup(false, close ? null: false))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APAuthPopup)
