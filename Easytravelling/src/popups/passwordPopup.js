import React, { Component } from 'react';
import { View, Text, Keyboard, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import APInput from '../ui/input';
import APButton from '../ui/button';
import APLink from '../ui/link';

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import validate from '../utils/validate';
import popupStyles from './popupStyles';
import throttle from '../utils/throttle';

import { show_password_popup } from '../store/actions/indexAction';
import { signIn, resetPassword } from '../store/actions/userAction';

class APPasswordPopup extends Component {
    state = {
        isValid: true,
        value: "",
        error: false
    }

    onSubmit = () => {
        Keyboard.dismiss();
        const isValid = validate([{type: "password"}], this.state.value);

        this.setState({
            ...this.state,
            isValid : isValid,
            error: isValid ? false : translate["password-hint"][this.props.lang]
        }, () => {
            if(isValid){
                this.hideModal();
                this.props.signIn({email: this.props.email, password: this.state.value});
            }
        })
    }

    onReset = () => {
        this.props.resetPassword(this.props.email);
        this.props.hideModal();
    }

    onChange = (text) => {
        this.setState({
            ...this.state,
            value: text
        })
    }

    hideModal = () =>{
        this.setState({
            isValid: true,
            value: "",
            error: false
        });
        this.props.hideModal();
    }

    makePassWordHintText = () => {
        const text = translate["type-password-text"][this.props.lang];  
        const regex = /\{email\}/g;
        const splitted = text.split(regex);

        return (
            <Text>
                <Text>{splitted[0]}</Text>
                <Text style={[style.modalText, globalStyles().fontBold]}>{this.props.email}</Text>
                <Text>{splitted[1]}</Text>
            </Text>
        );

    }

    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                backdropOpacity={0.5}
                style={{ justifyContent: "flex-end", margin: 0 }}
                onBackdropPress={this.hideModal}
                onBackButtonPress={this.hideModal}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["type-password"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View style={style.container}>
                                <Text style={[style.modalText, globalStyles().fontLight]}>{this.makePassWordHintText()}</Text>
                                <APInput
                                    label={translate["password"][this.props.lang]}
                                    isValid={this.state.isValid}
                                    value={this.state.value}
                                    error={this.state.error}
                                    onChangeText={this.onChange}
                                    secureTextEntry={true}
                                    onSubmitEditing={this.onSubmit}
                                    returnKeyType={"go"}
                                />
                                <View style={style.btnContainer}>
                                    <APButton text={translate["login"][this.props.lang]} color="#fff" backgroundColor="#7cb342" onPress={throttle(this.onSubmit)} />
                                </View>
                                <APLink onClick={throttle(this.onReset)}>{translate["reset-password"][this.props.lang] }</APLink>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        marginBottom: 10
    },
    modalText: {
        fontSize: 14,
        lineHeight: 22,
        color: "#464653",
        textAlign: "left",
        paddingBottom: 10
    },
    btnContainer: {
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
        flexDirection: "row",
        marginBottom: 10,
        marginTop: -20
    }
})


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (data) => { dispatch(signIn(data, true)) },
        hideModal: () => {dispatch(show_password_popup(false, ""))},
        resetPassword: (email) => { dispatch(resetPassword(email)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APPasswordPopup)
