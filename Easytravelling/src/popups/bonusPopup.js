import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import TouchableNativeFeedback from '../ui/touchableNativeFeedback'

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import popupStyles from './popupStyles';

import { show_bonus_popup } from '../store/actions/indexAction';

class APBonusPopup extends Component {
    onClose = () => {
        this.props.hidePopup();
    }

    render() {
        return (
            <Modal 
                isVisible={this.props.isVisible} 
                backdropOpacity={0.5}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["bonus-details"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View>
                                <Text style={[popupStyles.modalText, globalStyles().fontLight]}>{translate["your-bonus"][this.props.lang] + " " + this.props.user.withdrawn_bonus}</Text>
                                <Text style={[popupStyles.modalText, globalStyles().fontLight]}>{translate["bonus-id"][this.props.lang] + " " + this.props.user.bonus_id}</Text>
                            </View>
                        </View>
                        <Text style={[popupStyles.modalNote, globalStyles().fontLight]}>{translate["bonus-notes"][this.props.lang].replace("%price%", this.props.user.bonus_unit_price)}</Text>
                    </View>
                        <View style={popupStyles.modalBottom}>
                            <TouchableNativeFeedback onPress={this.onClose}>
                                <View style={popupStyles.modalButtonCont}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{"OK"}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                        </View>
                </View>
            </Modal>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        uid: state.user.uid,
        user: state.user.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hidePopup: () => {dispatch(show_bonus_popup(false))},
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APBonusPopup)
