import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import Modal from "react-native-modal";
import TouchableNativeFeedback from '../ui/touchableNativeFeedback'

import globalStyles from '../utils/style';
import translate from '../utils/translate';
import popupStyles from './popupStyles';


import { show_confirmation_popup, show_email_popup } from '../store/actions/indexAction';
import { verifyAccount } from '../store/actions/userAction';

class APConfirmationPopup extends Component {
    onClose = () => {
        this.props.hidePopup();
    }

    onSubmit = () => {
        this.props.hidePopup();
        if(this.props.user.email || this.props.user.pending_email){
            this.props.verifyAccount(this.props.uid);
        } else {
            this.props.showEmailPopup()
        }
        
    }

    render() {
        return (
            <Modal 
                isVisible={this.props.isVisible} 
                backdropOpacity={0.5}
            >
                <View style={[popupStyles.modalWrapper, globalStyles().shadow]}>
                    <View style={popupStyles.modalTop}>
                        <Text style={[popupStyles.modalHead, globalStyles().fontLight]}>{translate["confirmation-popup-head"][this.props.lang]}</Text>
                        <View style={popupStyles.modalContent}>
                            <View>
                                <Text style={[popupStyles.modalText, globalStyles().fontLight]}>{this.props.showConfirmationMessage === "activate" && this.props.user && this.props.user.email ? translate["confirmation-popup-text"][this.props.lang] : translate["verify-popup-text"][this.props.lang]}</Text>
                            </View>
                        </View>
                    </View>
                        <View style={popupStyles.modalBottom}>
                            <TouchableNativeFeedback onPress={this.onClose}>
                                <View style={[popupStyles.modalButtonCont, popupStyles.modalLeftButtonCont]}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["cancel"][this.props.lang]}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                            <TouchableNativeFeedback onPress={this.onSubmit}>
                                <View style={popupStyles.modalButtonCont}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontLight]}>{translate["confirmation-popup-button"][this.props.lang]}</Text>
                                </View>
                            </TouchableNativeFeedback> 
                        </View>
                </View>
            </Modal>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        uid: state.user.uid,
        user: state.user.user,
        showConfirmationMessage: state.index.showConfirmationMessage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hidePopup: () => {dispatch(show_confirmation_popup(false, ""))},
        verifyAccount: (uid) => {dispatch(verifyAccount(uid))},
        showEmailPopup: () => {dispatch(show_email_popup(true))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(APConfirmationPopup)
