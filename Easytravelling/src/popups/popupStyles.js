import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    modalWrapper: {
        backgroundColor: '#FEFEFE',
        padding: 0,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },

    modalTop: {
        paddingVertical: 16,
        paddingHorizontal: 18,
        justifyContent: "flex-start",
        width: "100%"        
    },

    modalHead: {
        fontSize: 16,
        lineHeight: 35,
        color: "#424242"
    },

    modalContent: {
        marginTop: 12,
        justifyContent: "flex-start"
    },

    modalNote: {
        fontSize: 10,
        color: "#424242",
        lineHeight: 15,
        paddingTop: 16
    },

    modalBottom: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        width: "100%",
        paddingVertical: 8,
        height: 56
    },

    modalButtonCont: {
        paddingHorizontal: 6,
        height: 40,
        marginRight: 8
    },

    modalLeftButtonCont: {
        marginRight: 0,
        paddingRight: 0
    },

    modalButton: {
        paddingHorizontal: 14,
        fontSize: 14,
        lineHeight: 46,
        color: "#343434",
        textAlign: "center"
    },

    modalText: {
        fontSize: 14,
        lineHeight: 22,
        color: "#424242",
        textAlign: "left"
    }
})

export default style;