import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableWithoutFeedback} from 'react-native';

import APDropdown from '../ui/dropdown';
import APIcon from '../ui/icon';

import translate from '../utils/translate';
import globalStyles from '../utils/style';
import throttle from '../utils/throttle';

class APTourTypeMenu extends Component {

    dropdown = null;
    

    state = {
        tourTypes: [
            { text: translate["outgoing-tours"][this.props.lang], value: "outgoing-tour", icon: "world" },
            { text: translate["domestic-tours"][this.props.lang], value: "domestic-tour", icon: "armenia" }
        ]
    }

    _renderRow = (rowData, rowID, highlighted) => {
        return (
            <View style={[style.dropdownStyle, highlighted ? { backgroundColor: "#F1F1F1" } : null, rowID == this.state.tourTypes.length - 1 ? { borderTopColor: "#e0e0e0", borderTopWidth: 1 } : null]}>
                <View style={{marginRight: 8, justifyContent: "center"}}>
                    <APIcon name={this.state.tourTypes[rowID].icon} color="#515151" size={20} />
                </View>
                <Text style={[style.rowText, globalStyles().fontLight]}>{this.state.tourTypes[rowID].text}</Text>
            </View>
        )
    }

    showDropdown = () => {
        this.dropdown.show();
    }

    render() {
        let dropdown = <View style={style.dropdown}>
                            <APDropdown
                                options={this.state.tourTypes}
                                defaultValue={this.props.value}
                                defaultIndex={this.state.tourTypes.map(el => el.value).indexOf(this.props.value)}
                                onSelect={this.props.onSelect}
                                textStyle={{ fontSize: 0 }}
                                renderRow={this._renderRow}
                                ref={(dropdown) => { this.dropdown = dropdown; }}
                            >
                                <View style={globalStyles().hidden}></View>
                            </APDropdown>
                    </View>

        return (
            <View> 
                <TouchableWithoutFeedback onPress={throttle(() => this.showDropdown())}>
                    <View >
                        <APIcon name={this.props.value === "domestic-tour" ? "armenia" : "world"} color="#fff" size={24} paddingVertical={10}/>
                    </View>
                </TouchableWithoutFeedback> 
                {dropdown}
            </View>
        )
    }
}

const style = StyleSheet.create({
    dropdown: {
        top: 0,
        position: "absolute",
        right: 0
    },

    dropdownStyle: {
        backgroundColor: "#fff",
        paddingVertical: 14,
        paddingHorizontal: 16,
        flexDirection: "row"
    },

    rowText: {
        fontSize: 16,
        color: '#515151',
        lineHeight: 22
    }
})

export default APTourTypeMenu;
