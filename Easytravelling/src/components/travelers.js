import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableWithoutFeedback, ScrollView } from 'react-native';

import { connect } from 'react-redux';
import Modal from "react-native-modal";

import APIcon from '../ui/icon';
import TouchableNativeFeedback from '../ui/touchableNativeFeedback';

import globalStyles from '../utils/style';
import popupStyles from '../popups/popupStyles';
import translate from '../utils/translate';
import generateArray from '../utils/generateArray';

class Travelers extends Component {

    state = {
        adults: 1,
        children: 0,
        prev_children: 0,
        showModal: false,
        temp: {
            adults: 1,
            children: 0,
            children_ages: []
        },
        children_ages: [],
        prev_children_ages: [],
        children_ages_errors: []
    }

    onChange = (type, count) => {
        let children_ages = [...this.state.children_ages];
        let shouldOpenModal = false;
        if(type === "children"){
            if(count >= this.state.children){
                for(let i = 0; i < count - this.state.children; i++){
                    children_ages.push("-");
                }
            } else {
                children_ages = children_ages.slice(0, count)
            }
            if(count === this.state.children &&  count === 1){
                count = 0;
                children_ages = [];
            } else {
                shouldOpenModal = true;
            }
        }


        this.setState({
            ... this.state,
            [type]: Math.min(Math.max(0, count), 9),
            children_ages: children_ages,
            prev_children: shouldOpenModal ? this.state.children : this.state.prev_children,
            prev_children_ages: shouldOpenModal ? this.state.children_ages : this.state.prev_children_ages
        }, () => {
            shouldOpenModal && this.openModal();
            this.props.onChange && this.props.onChange({
                adults: this.state.adults,
                children: this.state.children,
                children_ages: this.state.children_ages
            })
        })
    }

    onChangeModal = (type, count) => {
        let children_ages = [...this.state.temp.children_ages]
        if(type === "children"){
            if(count >= this.state.temp.children){
                for(let i = 0; i < count - this.state.temp.children; i++){
                    children_ages.push("-");
                }
            } else {
                children_ages = children_ages.slice(0, count)
            }
        }


        this.setState({
            ... this.state,
            temp: {
                ...this.state.temp,
                [type]: Math.min(Math.max(type === "adults" ? 1 : 0, count), 9),
                children_ages: children_ages,
            }
            
        })
    }

    applyChanges = () => {
        let inValidAges = [];
        this.state.temp.children_ages.forEach((el, index) => {
            if(el === "-"){
                inValidAges.push(index)
            }
        });
        if(inValidAges.length > 0){
            this.setState({
                ... this.state,
                children_ages_errors: inValidAges
            });
            return;
        }
        this.setState({
            ... this.state,
            children_ages: this.state.temp.children_ages,
            adults: this.state.temp.adults,
            children: this.state.temp.children,
            prev_children: this.state.temp.children,
            prev_children_ages: this.state.temp.children_ages,
            children_ages_errors: []
        }, () => {
            this.closeModal(false);
            this.props.onChange && this.props.onChange({
                adults: this.state.adults,
                children: this.state.children,
                children_ages: this.state.children_ages
            })
        })
    }


    setAge = (index, plusOrMinus) => {
        let children_ages = [...this.state.temp.children_ages];
        if(plusOrMinus === "plus"){
            if(children_ages[index] === "-"){
                children_ages[index] = 0;
            } else if(children_ages[index]  < 12){
                children_ages[index] ++;
            }
        } else {
            if(children_ages[index] !== "-" && children_ages[index] !== 0){
                children_ages[index] --;
            }
        }
        this.setState({
            ... this.state,
            temp: {
                ... this.state.temp,
                children_ages: children_ages
            }
        })
    }

    openModal = () => {
        this.setState({
            showModal: true,
            temp: {children: this.state.children, adults: this.state.adults, children_ages: [...this.state.children_ages]}
        })
    }

    closeModal = (cancel) => {
        this.setState({
            showModal: false,
            temp: {children: 0, adults: 1, children_ages: []},
            children: cancel ? this.state.prev_children : this.state.children,
            children_ages: cancel ? this.state.prev_children_ages : this.state.children_ages,
            children_ages_errors: []
        })
    }

    renderCounter = (field) => {
        return (
            <View style={[style.row]}>
                <Text style={[style.desc, globalStyles().text, globalStyles().fontLight]}>{translate[field][this.props.lang]} </Text>
                <TouchableWithoutFeedback onPress={() => { this.onChangeModal(field, this.state.temp[field] - 1) }}>
                    <View style={style.increase}>
                        <APIcon size={16} color={"#515151"} name={"remove"} />
                    </View>
                </TouchableWithoutFeedback>
                <Text style={[globalStyles().title, globalStyles().fontBold, style.green, { textAlign: "center" }]}>{this.state.temp[field]}</Text>
                <TouchableWithoutFeedback onPress={() => { this.onChangeModal(field, this.state.temp[field] + 1) }}>
                    <View style={style.increase}>
                        <Text style={[globalStyles().title, globalStyles().fontBold]}>+</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }

    render() {
        let adults = [0, 1, 2, 3, 4].map((el, index) => {
            return (
                <TouchableWithoutFeedback onPress={() => { this.onChange("adults", el + 1) }} key={el}>
                    <View>
                        <APIcon size={36} color={this.state.adults > el ? "#7cb342" : "#BBBBBB"} name={this.state.adults <= 5 || index != 4 ?  "person": "people"} />
                    </View>
                </TouchableWithoutFeedback>
            )
        });

        let children = [0, 1, 2].map((el, index) => {
            return (
                <TouchableWithoutFeedback onPress={() => { this.onChange("children", el + 1) }} key={el}>
                    <View>
                        <APIcon size={30} color={this.state.children > el ? "#7cb342" : "#BBBBBB"} name={this.state.children <= 3 || index != 2 ?  "person": "people"} />
                        {typeof this.state.children_ages[index] !== "undefined" && this.state.children_ages[index] !== "-" && (this.state.children <= 3 || index != 2) ? <Text style={[style.ageText, globalStyles().fontBold]} >{this.state.children_ages[index]} {translate["yr"][this.props.lang]}</Text> : null}
                    </View>
                </TouchableWithoutFeedback>
            )
        });

        let adults_count = generateArray(this.state.temp.adults).map((el, index) => {
            return (
                <View key={index}>
                    <APIcon size={38} color={"#7cb342"} name={"person"} />
                </View>
            )
        });

        let children_ages = generateArray(this.state.temp.children).map((el, index) => {
            return (
                <View style={[style.row, style.children_age]} key={index}>
                    <View style={style.ageRowIcon}>
                        <APIcon size={24} color={"#7cb342"} name={"person"} />
                    </View>
                    <View style={[style.row]}>
                        <Text style={[style.desc, globalStyles().text, globalStyles().fontLight, this.state.children_ages_errors.indexOf(index) > -1 ? style.error : null]}>{translate["age"][this.props.lang]} </Text>
                        <TouchableWithoutFeedback onPress = {() => this.setAge(index, "minus")}>
                            <View style={[style.increase, style.increase_age]}>
                                <APIcon size={16} color={"#515151"} name={"remove"} />
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{alignItems: "center", justifyContent: "center", width: 24}}>
                            {
                                this.state.temp.children_ages[index] !== "-" ?
                                <Text style={[globalStyles().title, globalStyles().fontBold, { textAlign: "center" }, this.state.children_ages_errors.indexOf(index) > -1 ? style.error : null]}>{this.state.temp.children_ages[index]}</Text>:
                                <APIcon size={10} color={"#515151"} name={"remove"} />
                            }
                        </View>
                        <TouchableWithoutFeedback onPress = {() => this.setAge(index, "plus")}>
                            <View style={[style.increase, style.increase_age]}>
                                <Text style={[globalStyles().title, globalStyles().fontBold]}>+</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )
        })

        return (
            <View style={style.container}>
                <View style={style.topBar}>
                    <View>
                        <Text style={[globalStyles().title, globalStyles().fontBold]} >{translate["travelers"][this.props.lang]}</Text>
                    </View>
                    <TouchableWithoutFeedback onPress={this.openModal}>
                        <View style={style.moreBtn}>
                            <APIcon size={14} color={"#7cb342"} name={"person_add"} />
                            <Text style={[style.moreBtnText, globalStyles().fontLight]} >{translate["more_people"][this.props.lang]}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={style.persons}>
                    <View>
                        <View style={style.row}>
                            <Text style={[style.desc, globalStyles().text, globalStyles().fontLight]}>{translate["adults"][this.props.lang]} </Text>
                            <Text style={[style.desc, globalStyles().text, globalStyles().fontLight, style.green]}>{this.state.adults}</Text>
                        </View>
                        <View style={style.adults}>
                            {adults}
                        </View>
                    </View>
                    <View>
                        <View style={[style.row, { justifyContent: "flex-end" }]}>
                            <Text style={[style.desc, globalStyles().text, globalStyles().fontLight]}>{translate["children"][this.props.lang]} </Text>
                            <Text style={[style.desc, globalStyles().text, globalStyles().fontLight, style.green]}>{this.state.children}</Text>
                        </View>
                        <View style={style.children}>
                            {children}
                        </View>
                    </View>
                </View>
                <Modal
                    isVisible={this.state.showModal}
                    backdropOpacity={0.5}
                    animationIn="fadeIn"
                    animationOut="fadeOut"
                    style={{ justifyContent: "center", alignItems: "center" }}
                >
                    <View style={[popupStyles.modalWrapper, globalStyles().shadow, style.modal]}>
                        <ScrollView>
                        <View style={popupStyles.modalTop}>
                            <Text style={[popupStyles.modalHead, globalStyles().fontBold]}>{translate["travelers"][this.props.lang]}</Text>
                            <View style={popupStyles.modalContent}>
                                <View style={style.travelers_count_adults}>
                                    {this.renderCounter("adults")}
                                    <View style={[style.row]}>
                                        <View style={style.modalCont}>
                                            {adults_count}
                                        </View>
                                    </View>
                                </View>
                                <View style={style.travelers_count_children}>
                                    {this.renderCounter("children")}
                                </View>
                                <View style={[style.row]}>
                                    <View style={style.modalCont}>
                                        {children_ages}
                                    </View>
                                </View>
                                {
                                    this.state.children_ages_errors.length > 0 ?
                                    <View style={style.row}>
                                        <Text style={[style.errorMsg, globalStyles().fontLight]}>{translate["children_ages_warning"][this.props.lang]}</Text>
                                    </View> : null
                                }
                                
                            </View>
                        </View>
                        <View style={popupStyles.modalBottom}>
                            <TouchableNativeFeedback onPress={() => this.closeModal(true)}>
                                <View style={[popupStyles.modalButtonCont, popupStyles.modalLeftButtonCont]}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontBold, {paddingHorizontal: 10}]}>{translate["cancel"][this.props.lang].toUpperCase()}</Text>
                                </View>
                            </TouchableNativeFeedback>
                            <TouchableNativeFeedback onPress={this.applyChanges}>
                                <View style={popupStyles.modalButtonCont}>
                                    <Text style={[popupStyles.modalButton, globalStyles().fontBold, style.green, {paddingHorizontal: 9}]}>{translate["done"][this.props.lang].toUpperCase()}</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%"
    },

    desc: {
        paddingBottom: 4
    },

    green: {
        color: "#7cb342"
    },

    topBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    moreBtn: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },

    moreBtnText: {
        color: "#7cb342",
        fontSize: 10,
        lineHeight: 14,
        paddingLeft: 3
    },
    ageText: {
        color: "#7cb342",
        fontSize: 7,
        lineHeight: 9,
        textAlign: "center"
    },

    persons: {
        marginTop: 16,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    adults: {
        flexDirection: "row",
        paddingTop: 7
    },

    children: {
        flexDirection: "row",
        justifyContent: "flex-end",
        paddingTop: 11
    },

    modal: {
        width: "85%"
    },

    row: {
        flexDirection: "row"
    },

    green: {
        color: "#7cb342"
    },

    travelers_count_adults: {
        borderBottomWidth: 1,
        borderBottomColor: "#F5F5F5",
    },

    travelers_count_children: {
        paddingVertical: 14
    },


    increase: {
        backgroundColor: "rgba(188, 188, 188, 0.5)",
        width: 24,
        height: 24,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        marginHorizontal: 10
    },

    increase_age: {
        marginHorizontal: 8
    },

    modalCont: {
        paddingVertical: 14,
        flexDirection: "row",
        flexWrap: "wrap"
    },

    ageRowIcon: {
        marginRight: 36
    },
    children_age: {
        marginBottom: 8
    },

    error: {
        color: "#f44336"
    },

    errorMsg: {
        fontSize: 10,
        lineHeight: 12,
        color: "#f44336"
    }
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang
    }
}

export default connect(mapStateToProps, null)(Travelers)

