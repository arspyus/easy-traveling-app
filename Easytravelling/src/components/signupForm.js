import React from 'react';
import { View, Platform} from 'react-native';

import APInput from '../ui/input';

import translate from '../utils/translate';


const SignupForm = (props) => {
    let lastTextInput, phoneTextInput, emailTextInput, passwordTextInput, retypeTextInput;
    return (
        <View>
            <APInput 
                label={translate["first-name"][props.lang]} 
                isValid = {props.form.first.isValid}
                value={props.form.first.value}
                onChangeText={(text) => { props.onChange(text, "signup", "first")}}  
                onSubmitEditing={()=>lastTextInput.focus()}
                _scrollView={props._scrollView}
            />
            <APInput 
                label={translate["last-name"][props.lang]} 
                isValid = {props.form.last.isValid}
                value={props.form.last.value}
                onChangeText={(text) => { props.onChange(text, "signup", "last")}}
                ref={(ref) => lastTextInput = ref}
                onSubmitEditing={()=>phoneTextInput.focus()}
                _scrollView={props._scrollView}  
            />
            <APInput 
                label={translate["phone"][props.lang]} 
                isValid = {props.form.phone.isValid}
                value={props.form.phone.value}
                onChangeText={(text) => { props.onChange(text, "signup", "phone")}} 
                keyboardType="phone-pad"
                ref={(ref) => phoneTextInput = ref}
                onSubmitEditing={()=>emailTextInput.focus()}
                _scrollView={props._scrollView}  
            />
            <APInput 
                label={translate["email"][props.lang]} 
                isValid = {props.form.email.isValid}
                value={props.form.email.value}
                onChangeText={(text) => { props.onChange(text, "signup", "email")}} 
                keyboardType="email-address"
                ref={(ref) => emailTextInput = ref} 
                onSubmitEditing={()=>passwordTextInput.focus()}
                _scrollView={props._scrollView} 
            />
            <APInput 
                label={translate["password"][props.lang]} 
                isValid = {props.form.password.isValid}
                value={props.form.password.value}  
                secureTextEntry={true}
                onChangeText={(text) => { props.onChange(text, "signup", "password")}} 
                error={props.form.password.error}
                ref={(ref) => passwordTextInput = ref}
                onSubmitEditing={()=>retypeTextInput.focus()} 
                _scrollView={props._scrollView} 
            />
            <APInput 
                label={translate["retype-password"][props.lang]} 
                isValid = {props.form.retype.isValid}
                value={props.form.retype.value}
                onChangeText={(text) => { props.onChange(text, "signup", "retype")}}
                secureTextEntry={true}
                ref={(ref) => retypeTextInput = ref} 
                onSubmitEditing={props.onSubmit}
                returnKeyType={"go"} 
                _scrollView={props._scrollView}
            />
        </View>
    )
}
export default SignupForm;