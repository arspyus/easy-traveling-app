import React, { Component } from 'react';
import { View} from 'react-native';

import APInput from '../ui/input';

import translate from '../utils/translate';

const DeactivateForm = (props) => {


    return (
        <View >
            <APInput 
                label={translate["password"][props.lang]} 
                isValid = {props.form.password.isValid}
                value={props.form.password.value}
                onChangeText={(text) => { props.onChange(text, "deactivate", "password")}}
                secureTextEntry={true}
                returnKeyType={"go"}
                onSubmitEditing={props.onSubmit}
                _scrollView={props._scrollView}  
            />
        </View>
    )
}


export default DeactivateForm;