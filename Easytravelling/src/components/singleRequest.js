import React, { Component } from 'react';
import { View, StyleSheet, Text, ImageBackground, ScrollView, TouchableWithoutFeedback } from 'react-native';

import APIcon from '../ui/icon';
import APSection from '../ui/section';
import APStatusLoader from '../ui/statusLoader'

import translate from '../utils/translate';
import capitalize from '../utils/capitalize';
import globalStyles from '../utils/style';
import APImageStore from '../utils/imageStore';
import throttle from '../utils/throttle';

const hotelImage = require('./../../assets/images/hotel.png');


class SingleRequest extends Component {

    state = {
        offers : []
    }

    componentWillMount() {
        this.setState({
            offers: this.props.request.offers.map(offer => {
                return {...offer, city: this.props.request.city, country: this.props.request.country, adults: this.props.request.adults, children: this.props.request.children, infant: this.props.request.infant}
            })
        })
    }


    componentDidUpdate(prevProps, prevState){

        if(!prevProps.isVisible && this.props.isVisible){
            this.getThumbnails();
        }
    }

    newOffersCount() {
        let offers = this.state.offers;
        let newOffersCount = 0;
        for (let i = 0; i < this.state.offers.length; i++) {
            if (this.state.offers[i].seen_status === "new") {
                newOffersCount++;
            }
        }


        return newOffersCount;
    }


    renderOffers = () => {
        let request_offers = this.state.offers;

        let offers = null;
        if (request_offers.length > 0) {
            offers = request_offers.map(offer => {
                let nights = Math.floor(((new Date(offer.hotel_checkout.split(" ")[0]) - new Date(offer.hotel_checkin.split(" ")[0])) / 3600 / 24 / 1000));
                let offerContent = (
                    <View>
                        {
                            offer.days_left !== -1  && offer.offer_status !== 'rejected' ?
                                <View style={style.overlay}></View> :
                                null
                        }
                        {
                            offer.days_left === -1 || offer.offer_status === 'rejected' ?
                                <View style={[style.overlay, { backgroundColor: "rgba(255,255,255,.6)" }]}></View> :
                                null
                        }
                        <TouchableWithoutFeedback onPress={throttle(() => this.props.navigateTo(offer.offer_id, "offer", {offers: this.state.offers}))}>

                            <View style={style.offer}>
                                <View style={[style.row, style.offer_top]}>
                                    <View>
                                        {
                                            offer.seen_status !== "seen" ?
                                                <View style={style.unseen} /> :
                                                null
                                        }
                                    </View>
                                    <View>
                                        <View style={[style.row, {marginBottom: 2}]}>
                                            <APIcon size={16} color={"#ffffff"} name={"date_range"} />
                                            <Text style={[style.offer_text, globalStyles().fontBold, { paddingLeft: 5 }]}>{nights}</Text>
                                        </View>
                                        {
                                            offer.hotel_rating ?
                                                <View style={style.row}>
                                                    <APIcon size={16} color={"#ffffff"} name={"star"} />
                                                    <Text style={[style.offer_text, globalStyles().fontBold, { paddingLeft: 5 }]}>{offer.hotel_rating}</Text>
                                                </View> :
                                                null
                                        }

                                    </View>
                                </View>
                                <View style={[style.offer_bottom]}>
                                    <View style={style.row}>
                                        <Text style={[style.offer_text, globalStyles().fontLight]} numberOfLines={2}>{offer.hotel}</Text>
                                    </View>
                                    <View style={[style.row, { marginTop: 2 }]}>
                                        <Text style={[style.offer_text, globalStyles().fontBold]}>{offer.price + " " + translate['amd'][this.props.lang]}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>

                    </View>
                )
                return (<ImageBackground style={style.imageBackground} source={offer.thumbnail ? { uri: offer.thumbnail} : hotelImage } imageStyle={{ borderRadius: 10 }} key={offer.offer_id}>
                        {offerContent}
                    </ImageBackground> )

            })
        } else {
            offers = (
                <View style={[style.offer, style.offer_empty]}>
                    <APIcon size={24} color={"#bcbcbc"} name={"access_time"} />
                    <Text style={[style.small_text, style.offer_empty_text, globalStyles().fontLight]}>{translate['offer_on_way'][this.props.lang]}</Text>
                </View>
            )
        }


        return (
            <View style={style.offer_container}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    overScrollMode="never"
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15 }}
                >
                    {offers}
                </ScrollView>
            </View>
        )
    }

    getThumbnails = () => {
        this.state.offers.forEach((offer) => {
            if(offer.place_id){
                let IS = new APImageStore(offer.place_id);
                IS.getThumbnail().then(thumbnail => {
                    this.setState({
                        offers: this.state.offers.map(offerObj => {
                            return offer.offer_id === offerObj.offer_id ? {...offerObj, thumbnail: thumbnail} : offerObj
                        })
                    })
                });
            }
        })
    }

    render() {
      console.log(this.props.request)
        let nights = this.props.request.nights.replace(",", " - ");
        let nightsArr = nights.split(" - ");
        if(nightsArr[0] && nightsArr[1] && nightsArr[0] === nightsArr[1]){
            nights = nightsArr[0];
        }
        return (
            <APSection style={{width: "94%"}}>
                <View>
                    <TouchableWithoutFeedback onPress={() => this.props.navigateTo(this.props.request.request_id, "request")}>
                        <View style={globalStyles().section}>
                            <View style={[style.row, style.top]}>
                                <View style={style.top_left}>
                                    <View style={style.top_top}>
                                        <Text style={[style.title, globalStyles().fontBold]} numberOfLines={1}>{this.props.request.city && this.props.request.city !== "any" ? capitalize(this.props.request.city) : capitalize(this.props.request.country)}</Text>
                                    </View>
                                    <View style={[style.row, { flexWrap: "wrap" }]}>
                                        {
                                            this.props.request.city && this.props.request.city !== "any" ?
                                                <Text style={[style.text, { paddingRight: 10 }, globalStyles().fontLight]}>{capitalize(this.props.request.country)}</Text> :
                                                null
                                        }
                                        <Text style={[style.text, { paddingRight: 10 }, globalStyles().fontLight, style.green]}>{this.state.offers.length + " " + translate['offer'][this.props.lang]}</Text>
                                        {
                                            this.newOffersCount() > 0 ?
                                                <View style={style.new}>
                                                    <Text style={[style.newText, globalStyles().fontLight]}>{this.newOffersCount() + " New"}</Text>
                                                </View> :
                                                null
                                        }
                                    </View>
                                </View>
                                <View style={style.top_right}>
                                    <View style={style.top_top}>
                                        <View style={[style.row, { justifyContent: "space-between" }]}>
                                            <View style={[style.row, { marginRight: 15 }]}>
                                                <APIcon size={18} color={"#7cb342"} name={"person"} />
                                                <Text style={[globalStyles().title, globalStyles().fontBold, { paddingLeft: 3 }]}>{this.props.request.number_of_people}</Text>
                                            </View>
                                            <View style={[style.row]}>
                                                <APIcon size={18} color={"#7cb342"} name={"date_range"} />
                                                <Text style={[globalStyles().title, globalStyles().fontBold, { paddingLeft: 3 }]}>{nights}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <APStatusLoader
                                        status={this.props.request.status}
                                        lang={this.props.lang}
                                    />
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <View style={[globalStyles().section, {paddingHorizontal: 0}]} >
                        {this.renderOffers()}
                    </View>
                </View>
            </APSection>
        )
    }

}

const style = StyleSheet.create({
    title: {
        fontSize: 20,
        lineHeight: 27,
        letterSpacing: 0.35,
        color: "#515151"
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
    },
    top: {
        width: "100%",
        alignItems: "flex-start",
    },
    top_left: {
        flex: 1,
        paddingRight: 10
    },
    top_right: {
        width: 114
    },
    top_top: {
        marginBottom: 5,
        height: 28
    },
    green: {
        color: "#7cb342"
    },
    new: {
        paddingHorizontal: 7,
        paddingVertical: 1,
        backgroundColor: "#e9c32b",
        borderRadius: 5,
        height: 13
    },
    newText: {
        fontSize: 8,
        lineHeight: 11,
        color: "#ffffff",
        letterSpacing: 0.14
    },
    small_text: {
        fontSize: 10,
        lineHeight: 14,
        letterSpacing: 0.17
    },

    offer_container: {
        flexDirection: "row",
        backgroundColor: "#ffffff",
        width: "100%"
    },

    imageBackground: {
        width: 150,
        height: 120,
        marginRight: 10,
        backgroundColor: "rgba(188, 188, 188, 0.4)",
        borderRadius: 10
    },

    offer: {
        width: 150,
        height: 120,
        borderRadius: 10,
        padding: 9
    },

    offer_empty: {
        backgroundColor: "rgba(188, 188, 188, 0.4)",
        justifyContent: "center",
        alignItems: "center"
    },
    unseen: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: "#e9c32b"
    },
    offer_text: {
        color: "#fff",
        fontSize: 12,
        lineHeight: 20,
        letterSpacing: 0.21
    },
    offer_title: {
        color: "#fff",
        fontSize: 18,
        lineHeight: 24,
        letterSpacing: 0.31
    },
    offer_top: {
        justifyContent: "space-between",
        alignItems: "flex-start",
        flex: 1
    },
    offer_bottom: {
        justifyContent: "flex-end",
        flex: 1
    },
    offer_empty_text: {
        paddingTop: 5,
        color: "#bcbcbc",
        textAlign: "center"
    },
    overlay: {
        position: "absolute",
        backgroundColor: "rgba(0,0,0,.3)",
        top: 0,
        left: 0,
        width: 150,
        height: 120,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center"
    },
})

export default SingleRequest;
