import React, { Component } from 'react';
import { View } from 'react-native';

import APInput from '../ui/input';

import translate from '../utils/translate';
const SigninForm = (props) => {

    let passwordTextInput;
    return (
        <View>
            <APInput 
                label={translate["email"][props.lang]} 
                isValid = {props.form.email.isValid}
                value={props.form.email.value}
                onChangeText={(text) => { props.onChange(text, "signin", "email")}}
                keyboardType="email-address"
                onSubmitEditing={()=>passwordTextInput.focus()}
                _scrollView={props._scrollView}
            />
           <APInput 
                label={translate["password"][props.lang]} 
                isValid = {props.form.password.isValid}
                value={props.form.password.value}  
                secureTextEntry={true}
                onChangeText={(text) => { props.onChange(text, "signin", "password")}}
                ref={(ref)=>passwordTextInput = ref} 
                onSubmitEditing={props.onSubmit}
                returnKeyType={"go"} 
                _scrollView={props._scrollView}
            />
        </View>
    )
}

export default SigninForm;