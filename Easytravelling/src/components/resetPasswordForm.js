import React, { Component } from 'react';
import { View } from 'react-native';

import APInput from '../ui/input';

import translate from '../utils/translate';

const ResetPasswordForm = (props) => {


    return (
        <View>
            <APInput 
                label={translate["email"][props.lang]} 
                isValid = {props.form.email.isValid}
                value={props.form.email.value}
                onChangeText={(text) => { props.onChange(text, "resetPassword", "email")}} 
                onSubmitEditing={props.onSubmit}
                returnKeyType={"go"} 
                _scrollView={props._scrollView}   
            />
        </View>
    )
}

export default ResetPasswordForm;