import React from 'react';
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback, Platform } from 'react-native';

import APIcon from '../ui/icon';
import APCard from '../ui/card';

import capitalize from '../utils/capitalize';
import globalStyles from '../utils/style';
import throttle from '../utils/throttle';

const SingleAgency = (props) => {
    let stars = null;
    let rate = parseInt(props.agency.average_rate);

    return (
        <APCard style={props.isLast ? {marginBottom : 32 } : null}>
            <TouchableWithoutFeedback onPress={throttle(props.onSelect)}>
                <View>
                    <View style={style.content}>
                        <Image style={style.image} source={{ uri: props.agency.photo }} resizeMode='cover'></Image>
                        <View style={style.contentRight}>
                            <View style={{flex: 1}}>
                                <View style={style.titleWrap}><Text style={[style.title, globalStyles().fontLight]}>{props.agency.tour_agent}</Text></View>
                                <Text style={[style.address, globalStyles().fontLight]}>{capitalize(props.agency.address + ", " + props.agency.city)}</Text>
                            </View>
                            {(!props.agency.badges || props.agency.badges[0]) && rate == 0 ?
                                <View >
                                    <APIcon color="#aaa" size={24} name="chevron_right" />
                                </View> : null }

                        </View>
                    </View>
                    {(props.agency.badges && props.agency.badges[0]) || rate > 0 ?
                        <View style={style.bottom}>
                            <View style={style.bottomWrapper}>
                                {
                                    props.agency.badges && props.agency.badges[0] ?
                                        <View style={style.cup}><APIcon name={props.agency.badges[0]} size={22} color="#fff" /></View>
                                        : null
                                }
                                {rate > 0 ?
                                    <View style={style.starContainer}>
                                        <APIcon size={40} name={rate > 0 ? "star" : "star_border"} color={rate > 0 ? "#7cb342" : "#000"} />
                                    </View> : null}
                                {rate > 0 ?
                                    <View style={style.rateContainer}>
                                        <Text style={[style.rate, globalStyles().fontLight]}>{props.agency.average_rate}</Text>
                                    </View> : null
                                }
                            </View>
                            <View >
                                <APIcon color="#aaa" size={24} name="chevron_right" />
                            </View>

                        </View> : null}
                </View>
            </TouchableWithoutFeedback>
        </APCard>
    )

}

const style = StyleSheet.create({
    content: {
        padding: 24,
        flexDirection: "row"
    },
    bottom: {
        paddingBottom: 16,
        paddingTop: 16,
        paddingHorizontal: 24,
        borderTopColor: "rgba(160,160,160,0.2)",
        borderTopWidth: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    bottomWrapper: {
        flexDirection: "row",
        alignItems: "center"
    },
    image: {
        height: 55,
        width: 55,
        borderRadius: 27.5,
        borderWidth: 1,
        borderColor: "#d0d0d0"
    },

    contentRight: {
        paddingLeft: "10%",
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    titleWrap: {
        marginBottom: 8
    },

    title: {
        lineHeight: 32,
        fontSize: 24,
        color: "#424242"
    },
    address: {
        lineHeight: 22,
        fontSize: 14,
        color: "#424242"
    },

    cup: {
        backgroundColor: "#FEA62A",
        width: 32,
        height: 32,
        marginRight: 12,
        borderRadius: 16,
        alignItems: "center",
        justifyContent: "center"
    },

    rateContainer: {
        alignItems: "center",
        flexDirection: "row"
    },

    rate: {
        fontSize: 40,
        lineHeight: 52
    },

    starContainer: {
        flexDirection: "row"
    }
})

export default SingleAgency;