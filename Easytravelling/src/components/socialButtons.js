import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { connect } from 'react-redux';

import APGoogleBtn from '../ui/googleBtn';
import APFacebookBtn from '../ui/facebookBtn';
import APPasswordPopup from '../popups/passwordPopup';
import APGooglePopup from '../popups/googlePopup';

import translate from '../utils/translate';
import globalStyles from '../utils/style';

class SocialButtons extends Component{
    render() {
        return (
            <View style={style.container}>
                <View style={style.orContainer}>
                    <View style={[style.orBorder, this.props.page === "auth" ? {backgroundColor: "#B2B2B2"} : null]}></View>
                    <View style={style.orContent}>
                        <Text style={[style.orText, globalStyles().fontLight, this.props.page === "auth" ? {color: "#B2B2B2"} : null]}>{translate["or"][this.props.lang].toUpperCase()}</Text>
                    </View>
                    <View style={[style.orBorder, this.props.page === "auth" ? {backgroundColor: "#B2B2B2"} : null]}></View>
                </View>
                <View>
                    <APFacebookBtn lang={this.props.lang}/> 
                    <APGoogleBtn lang={this.props.lang}/>
                </View>
                <APPasswordPopup 
                    email={this.props.passwordPromptEmail}
                    isVisible={this.props.showPasswordPopup} 
                /> 
                <APGooglePopup 
                    email={this.props.passwordPromptEmail}
                    isVisible={this.props.showGooglePopup} 
                />
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        paddingBottom: 5
    },

    orContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 20
    },

    orBorder: {
        height: 1,
        flex: 1,
        backgroundColor: "#fff"
    },

    orContent: {
        paddingHorizontal: 14
    },

    orText: {
        color: "#fff",
        fontSize: 16
    },
})

const mapStateToProps = (state) => {
    return {
        lang: state.index.lang,
        passwordPromptEmail: state.index.passwordPromptEmail,
        showPasswordPopup: state.index.showPasswordPopup,
        showGooglePopup: state.index.showGooglePopup
    }
}

export default connect(mapStateToProps, null)(SocialButtons)