import React, { Component } from 'react';
import { View} from 'react-native';

import APInput from '../ui/input';

import translate from '../utils/translate';

const ChangePasswordForm = (props) => {
    let newTextInput, retypeTextInput;
    return (
        <View >
            <APInput 
                label={translate["current-password"][props.lang]} 
                isValid = {props.form.current.isValid}
                value={props.form.current.value}
                onChangeText={(text) => { props.onChange(text, "changePassword", "current")}}
                secureTextEntry={true}
                onSubmitEditing={()=>newTextInput.focus()}
                _scrollView={props._scrollView}  
            />
            <APInput 
                label={translate["new-password"][props.lang]} 
                isValid = {props.form.new.isValid}
                value={props.form.new.value}
                onChangeText={(text) => { props.onChange(text, "changePassword", "new")}}
                secureTextEntry={true}
                error={props.form.new.error}
                ref={(ref)=>newTextInput = ref}
                onSubmitEditing={()=>retypeTextInput.focus()}
                _scrollView={props._scrollView}   
            />
            <APInput 
                label={translate["retype-password"][props.lang]} 
                isValid = {props.form.retype.isValid}
                value={props.form.retype.value}
                onChangeText={(text) => { props.onChange(text, "changePassword", "retype")}}
                secureTextEntry={true}
                ref={(ref)=>retypeTextInput = ref} 
                onSubmitEditing={props.onSubmit}
                returnKeyType={"go"}
                _scrollView={props._scrollView}   
            />
        </View>
    )
}


export default ChangePasswordForm;