import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';

import APInput from '../ui/input';
import APSelect from '../ui/select';

import translate from '../utils/translate';
import globalStyles from '../utils/style';

const SettingsForm = (props) => {
    let lastTextInput, phoneTextInput, emailTextInput;
    return (
        <View >
            <APInput 
                label={translate["first-name"][props.lang]} 
                isValid = {props.form.first.isValid}
                value={props.form.first.value}
                onChangeText={(text) => { props.onChange(text, "settings", "first")}}
                onSubmitEditing={()=>lastTextInput.focus()}
                _scrollView={props._scrollView}     
            />
            <APInput 
                label={translate["last-name"][props.lang]} 
                isValid = {props.form.last.isValid}
                value={props.form.last.value}
                onChangeText={(text) => { props.onChange(text, "settings", "last")}}
                ref={(ref) => lastTextInput=ref}
                onSubmitEditing={()=>phoneTextInput.focus()}
                _scrollView={props._scrollView}  
            />
            <APInput 
                label={translate["phone"][props.lang]} 
                isValid = {props.form.phone.isValid}
                value={props.form.phone.value}
                onChangeText={(text) => { props.onChange(text, "settings", "phone")}}   
                keyboardType="phone-pad"
                ref={(ref) => phoneTextInput=ref} 
                onSubmitEditing={()=>emailTextInput.focus()}
                _scrollView={props._scrollView} 
            />
            <APInput 
                label={translate["email"][props.lang]} 
                isValid = {props.form.email.isValid}
                value={props.form.email.value}
                onChangeText={(text) => { props.onChange(text, "settings", "email")}}
                keyboardType="email-address"
                returnKeyType={"go"}
                onSubmitEditing={props.onSubmit}  
                ref={(ref) => emailTextInput=ref}
                _scrollView={props._scrollView} 
                editable={props.user.user_status === "unconfirmed" && !props.userDataIsLoading}
                disabled={props.user.user_status === "active" || props.userDataIsLoading}
            />
            {
                props.showVerifyLink?
                <TouchableWithoutFeedback onPress={props.onVerify}>
                    <View style={style.verifyLink}>
                        <Text style={[style.verifyLinkText, globalStyles().fontLight]}>{translate["verify-button"][props.lang]}</Text>
                    </View>
                </TouchableWithoutFeedback>:
                null
            }
            <APSelect 
                label={translate["language"][props.lang]} 
                value={props.form.language.value}
                isValid={true}
                options={[{text: translate["choose-app-lang"][props.lang], value: ""}].concat(props.form.language.languages)}
                disabledOptions={[0]}
                onChangeText={(text) => { props.onChange(text, "settings", "language")}}
            />
        </View>
    )
}

const style = StyleSheet.create({
    verifyLink: {
        marginTop: -40,
        marginBottom: 40
    },
    verifyLinkText: {
        color: "#7cb342",
        fontSize: 12,
        textDecorationLine: "underline"
    }
})


export default SettingsForm;