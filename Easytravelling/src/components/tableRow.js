import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import globalStyles from '../utils/style';

const TableRow = (props) => {
    return (
        <View style={style.tableRow}>
            <Text style={[style.tableRowLabel, globalStyles().fontBold]}>{props.label}</Text>
            <Text style={[style.tableRowValue, globalStyles().fontLight]}>{props.text}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    tableRow: {
        flexDirection: "row",
        paddingVertical: 15,
        paddingHorizontal: 5,
        justifyContent: "space-between",
        borderBottomWidth: 1,
        borderBottomColor: "#d0d0d0"
    },

    tableRowLabel: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 22,
        flex: 1
    },

    tableRowValue: {
        color: "#424242",
        fontSize: 14,
        lineHeight: 22,
        paddingLeft: 10,
        flex: 1,
        textAlign: "right"
    }
})

export default TableRow;