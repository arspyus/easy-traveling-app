import React, {Component} from 'react';
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback, Dimensions, Platform} from 'react-native';

import APIcon from '../ui/icon';
import APCard from '../ui/card';

import translate from '../utils/translate';
import globalStyles from '../utils/style';
import throttle from '../utils/throttle';
import { connect } from 'react-redux';

class SingleOffer extends Component {

    state = {
        titleWidth: null,
        layoutDone: false
    }
    onTitleLayout = (event) => {
        if(this.state.layoutDone) return;
        const width = event.nativeEvent.layout.width;
        const rateW = this.props.rate ? 60 : 0;
        
        const containerW = Dimensions.get("window").width * 9/10 - 40 - rateW ;
        
        if(width > containerW && !this.state.titleWidth){
            this.setState({
                titleWidth : containerW,
                layoutDone: true
            })
        } else {
            this.setState({
                titleWidth : null,
                layoutDone: true
            })
        }
    }

    componentDidMount() {
        Dimensions.addEventListener('change', this.changeDimensions);
    }
    
    componentWillUnmount() {
          Dimensions.removeEventListener('change', this.changeDimensions)
    }

    changeDimensions = () => {
        this.setState({
            titleWidth: null,
            layoutDone: false
        });
    }

    render() {
        const rating = this.props.offer.agency ? this.props.offer.agency.average_rate : 0;
        return (
            <APCard style={this.props.isLast ? { marginBottom :32  } : null}>
                <TouchableWithoutFeedback onPress={throttle(this.props.onSelect)}>
                    <View>
                        <View style={style.bgImage}>
                            <Image source={{ uri: this.props.offer.photo }} style={style.image}></Image>
    
                            <View style={style.overlay}></View>
                            {this.props.offer.agency && this.props.offer.agency.badges && this.props.offer.agency.badges[0] ?
                                <View style={style.badge}>
                                    <APIcon name={this.props.offer.agency.badges[0]} size={29} color="#fff" />
                                </View> : null}
                                <Text style={[style.headerTextTitle, globalStyles().fontBold]} numberOfLines={1}>{this.props.offer.title}</Text>
                            {this.props.rate ? <View style={style.headerBottomText}>
                                <View style={{width: this.state.titleWidth ? this.state.titleWidth : "auto"}}>
                                    <Text numberOfLines={1} style={[style.headerTextDesc, globalStyles().fontBold]} onLayout={this.onTitleLayout}>{this.props.offer.agency && this.props.offer.agency.tour_agent}</Text>
                                </View>
                                {
                                    parseInt(rating) > 0 ?
                                        <View style={style.rate} >
                                            <View style={{ marginRight: 8 }} pointerEvents="none">
                                                <APIcon size={16} name={"star"} color={"#fff"} />
                                            </View>
                                            <Text style={[style.headerTextDesc, globalStyles().fontBold]}>{rating}</Text>
                                        </View> : null}
                            </View> : null}
                        </View>
                        <View style={style.bottom}>
                            <View>
                                <Text style={[style.price, globalStyles().fontBold]}>{this.props.offer.price} {translate['amd'][this.props.lang]}</Text>
                                <Text style={[style.left_days, globalStyles().fontLight]}>{this.props.offer.days_left == 0 ? translate['expiring-today'][this.props.lang] : translate['valid-for'][this.props.lang].replace('%days%', this.props.offer.days_left)}</Text>
                            </View>
                            <View >
                                <APIcon color="#aaa" size={24} name="chevron_right" />
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </APCard>
        )
    }
    

}

const style = StyleSheet.create({

    bgImage: {
        height: 210,
        justifyContent: "flex-end",
        width: "100%"
    },

    image: {
        width: "100%",
        height: "100%",
        position: "absolute",
        top: 0,
        left: 0,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5
    },

    badge: {
        width: 44,
        height: 44,
        borderRadius: 22,
        backgroundColor: "#ffa726",
        position: "absolute",
        right: 16,
        top: 16,
        alignItems: "center",
        justifyContent: "center"
    },

    overlay: {
        position: "absolute",
        backgroundColor: "rgba(0,0,0,.3)",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%"
    },

    headerTextTitle: {
        color: "#fff",
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 22
    },

    headerBottomText: {
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingBottom: 16,
        alignItems: "center"
    },

    headerTextDesc: {
        fontSize: 16,
        color: "#fff"
    },

    bottom: {
        paddingVertical: 24,
        paddingHorizontal: 20,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    price: {
        color: "#7cb342",
        paddingBottom: 8,
        lineHeight: 32,
        fontSize: 24
    },

    left_days: {
        fontSize: 14,
        lineHeight: 22,
        color: "#aaa"
    },
    rate: {
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 13
    }
})

const mapStateToProps = (state) => {
    return {
        ios_toolbar_height: state.index.ios_toolbar_height
    }
}

export default connect(mapStateToProps, null)(SingleOffer)
