import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableWithoutFeedback, Image } from 'react-native';

import APWrapper from '../ui/wrapper';
import APIcon from '../ui/icon';
import translate from '../utils/translate';
import globalStyles from '../utils/style';

class Rate extends Component {
    state = {
        selectedStars: this.props.is_rated ? parseInt(this.props.is_rated) : 0
    }

    componentDidUpdate(prevProps) {
        if (prevProps.is_rated != this.props.is_rated) {
            this.setState({
                ...this.state,
                selectedStars: this.props.is_rated ? parseInt(this.props.is_rated) : 0
            })
        }
    }

    onStarClick = (index) => {
        this.setState({
            ...this.state,
            selectedStars: index + 1
        })

        this.props.onRate(this.props.agency_id, index + 1)
    }

    render(){
        let stars = null;
        let rate = this.state.selectedStars;
        stars = [0, 1, 2, 3, 4].map((el, index) => {
            return (
                <TouchableWithoutFeedback key={el} onPress={() => {this.onStarClick(index)}}>
                    <View style={{marginRight: 4}} >
                        <APIcon size={28} name={index < rate ? "star" :  "star_border"} color={index < rate ? "#7cb342" : "#aaa"} />
                    </View>
                </TouchableWithoutFeedback>
            )
        })

        return (
            <View style={style.container}>
                <View style={{justifyContent: "center", width: "100%", alignItems: "center"}}>
                    <View style={{height: 42, backgroundColor: "#fff", width:"100%"}}></View>
                    <View style={{height: 46, backgroundColor: "#F6F6F6", width:"100%"}}></View>
                    <View style={[style.avatar, globalStyles().shadow]}>
                        {
                            this.props.avatar ? 
                            <Image source={{ uri: this.props.avatar }} style={style.avatarThumbnail}></Image> :
                            <Text style={[style.avatarFirstLetter, globalStyles().fontBold]}>{this.props.user_firstname.charAt(0).toUpperCase() }</Text>
                        }
                    </View>
                </View>
                <View style={style.wrapper}>
                    <View style={style.stars}>
                        {stars}
                    </View>
                    <View style={style.rate}>
                        <Text style={[style.rateText, globalStyles().fontLight]}>{translate["rate"][this.props.lang].toUpperCase()}</Text>
                    </View>
                </View>
            </View>    
        )
    }
    
}

const style = StyleSheet.create({
    avatar: {
        width: 84,
        height: 84,
        borderRadius: 42,
        backgroundColor: "#7cb342",
        borderWidth: 2,
        borderColor: "#fff",
        justifyContent: "center",
        alignItems: "center",
        top: 0,
        position: "absolute"
    },

    avatarThumbnail: {
        width: 80,
        height: 80,
        borderRadius: 40
    },

    avatarFirstLetter: {
        color: "#fff",
        fontSize: 42,
        lineHeight: 80
    },

    container: {
        alignItems: "center"
    },

    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F6F6F6",
        width: "100%"
    },

    rateText: {
        lineHeight : 32,
        fontSize: 16,
        color: "#aaa"
    },

    rate: {
        marginVertical: 32
    },

    stars: {
        flexDirection : "row",
        marginTop: 32,
        justifyContent: "center",
        alignItems: "center"
    }
})

export default Rate;