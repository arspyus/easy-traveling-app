import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import APNotificationCircle from '../ui/notificationCircle';

import globalStyles from '../utils/style';

const MenuItem = (props) => {
    return (
        <View>
            <TouchableWithoutFeedback onPress={ props.onClick }>
                <View style={[style.listItem, { backgroundColor: props.activeItem === props.label ? "rgba(0,0,0,0.05)" : "#fff", paddingLeft: props.paddingLeft }]}>
                    <Text style={[style.listItemText, globalStyles().fontBold]}>{props.text}</Text>
                    {props.notification ? <APNotificationCircle style={{ marginTop: 14, marginLeft: 2 }} /> : null}
                </View>
            </TouchableWithoutFeedback>
            { 
                props.disabled ?
                    <TouchableWithoutFeedback onPress={ props.onDisabled }> 
                        <View style={style.disabled}></View> 
                    </TouchableWithoutFeedback> : 
                    null 
            }
        </View>
    )
}

const style = StyleSheet.create({
    listItem: {
        height: 48,
        paddingRight: 16,
        justifyContent: "flex-start",
        flexDirection: "row"
    },

    listItemText: {
        lineHeight: 48,
        fontSize: 14,
        color: "rgba(0,0,0,0.87)"
    },
    disabled: {
        width: "100%",
        height: "100%",
        position: "absolute",
        left: 0,
        top: 0,
        backgroundColor: "rgba(255,255,255,.6)"
    }
})

export default MenuItem;