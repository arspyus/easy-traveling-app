import { Navigation, NativeEventsReceiver } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { Dimensions, AsyncStorage, NetInfo, AppState, Platform } from 'react-native';
import firebase from 'react-native-firebase';

import LandingScreen from './src/screens/Landing';
import AuthScreen from './src/screens/Auth';
import OffersScreen from './src/screens/Offers';
import SettingsScreen from './src/screens/Settings';
import MakeRequestScreen from './src/screens/MakeRequest';
import MyRequestsScreen from './src/screens/MyRequests';
import SidebarScreen from './src/screens/Sidebar';
import OfferScreen from './src/screens/Offer';
import AgencyScreen from './src/screens/Agency';
import AboutScreen from './src/screens/About';
import SingleOfferScreen from './src/screens/SingleOffer';
import SingleRequestScreen from './src/screens/SingleRequest';
import PrebookScreen from './src/screens/Prebook';

import store from './src/store/configureStore';

import { store_language, store_intro, store_connection, checkExtraData, set_ios_toolbar_height } from './src/store/actions/indexAction';
import { store_uid, store_user, getUserData, setUserDataIsLoading } from './src/store/actions/userAction';
import { store_fcmToken } from './src/store/actions/pushAction';
import { setFirebaseUser } from './src/store/actions/firebaseAction'


import isIphoneX from './src/utils/iphoneX';
import isLandscape from './src/utils/isLandscape';

// Register Screens

Navigation.registerComponent(
  "easytraveling.LandingScreen",
  () => LandingScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.AuthScreen",
  () => AuthScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.OffersScreen",
  () => OffersScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.OfferScreen",
  () => OfferScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.AgencyScreen",
  () => AgencyScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.SidebarScreen",
  () => SidebarScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.SettingsScreen",
  () => SettingsScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.MakeRequestScreen",
  () => MakeRequestScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.MyRequestsScreen",
  () => MyRequestsScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.AboutScreen",
  () => AboutScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.SingleOfferScreen",
  () => SingleOfferScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.SingleRequestScreen",
  () => SingleRequestScreen,
  store,
  Provider
);

Navigation.registerComponent(
  "easytraveling.PrebookScreen",
  () => PrebookScreen,
  store,
  Provider
);

// Start an App


const startApp = (screen) => {
    Navigation.startSingleScreenApp({
      screen: {
        screen: screen,
        navigatorStyle: {
          navBarHidden: true,
          screenBackgroundColor: Platform.OS === "ios" ? '#f2f2f2' : "#7cb342",
          statusBarColor: screen === "easytraveling.LandingScreen" ? "#000000" : "#8bc34a",
          statusBarHidden: screen === "easytraveling.LandingScreen"
        }
      },
      drawer: {
        left: {
          screen: 'easytraveling.SidebarScreen',
        },
        disableOpenGesture: false
      },
      animationType: 'none'
    })

  let previosAppState;
  AppState.addEventListener('change', (state) => {

    if (previosAppState === "inactive" && state === "active") {
      previosAppState = "active";
      store.dispatch(checkExtraData());
      if (!store.getState().user.isGuest && store.getState().user.uid && store.getState().user.user && store.getState().user.user.user_status === "unconfirmed") {
        store.dispatch(getUserData());
      }
    } else if (state && state.match(/inactive|background/)) {
      if (!store.getState().user.isGuest && store.getState().user.uid && store.getState().user.user && store.getState().user.user.user_status === "unconfirmed") {
        store.dispatch(setUserDataIsLoading(true));
      }
      previosAppState = "inactive";
    }
  })

  const statusBarHeight = isIphoneX() ? 24 : 20;
  if (Platform.OS === "ios") {
    Dimensions.addEventListener('change', () => {
      store.dispatch(set_ios_toolbar_height(!isLandscape() ? statusBarHeight : 0, isIphoneX() ? !isLandscape() ? { bottom: 34, left: 0, right: 0 } : { bottom: 0, left: 44, right: 34 } : null));
    });
  }

  firebase.notifications().setBadge(0);
}

const userPromise = () => new Promise((resolve, reject) => {
  AsyncStorage.getItem('easytraveling:user').then((user) => {
    resolve(user)
  }).catch((err) => {
    reject(err);
  })
})

const uidPromise = () => new Promise((resolve, reject) => {
  AsyncStorage.getItem('easytraveling:uid').then((uid) => {
    resolve(uid)
  }).catch((err) => {
    reject(err);
  })
})

const langPromise = () => new Promise((resolve, reject) => {
  AsyncStorage.getItem('easytraveling:lang').then((lang) => {
    resolve(lang)
  }).catch((err) => {
    reject(err);
  })
})

const connectionPromise = () => new Promise((resolve, reject) => {
  NetInfo.getConnectionInfo().then((connectionInfo) => {
    if (connectionInfo.type === "none") {
      store.dispatch(store_connection(false));
    } else if (connectionInfo.type !== "unknown") {
      store.dispatch(store_connection(true))
    }
    resolve();
  }).catch((ex) => { resolve() });
})

const introPromise = () => new Promise((resolve, reject) => {
  AsyncStorage.getItem('easytraveling:intro').then((intro) => {
    resolve(intro)
  }).catch((err) => {
    reject(err);
  })
})

const fcmPromise = () => new Promise((resolve, reject) => {
  AsyncStorage.getItem('easytraveling:fcmToken').then((fcmToken) => {
    resolve(fcmToken)
  }).catch((err) => {
    reject(err);
  })
})

const startApplication = () => {
  Promise.all([userPromise(), uidPromise(), langPromise(), introPromise(), fcmPromise(), connectionPromise()]).then((values) => {
    let screen = "easytraveling.LandingScreen";
    if (values && values[1]) {
      screen = "easytraveling.OffersScreen";
    }
    if (values && values[0]) {
      store.dispatch(store_user(JSON.parse(values[0])))
    }

    if (values && values[1]) {
      store.dispatch(store_uid(values[1]))
    }

    if (values && values[2]) {
      store.dispatch(store_language(values[2]))
    }
    if (values && values[3]) {
      store.dispatch(store_intro(false))
    }

    if (values && values[4]) {
      store.dispatch(store_fcmToken(values[4]))
    }
    startApp(screen)
  }).catch(er => {
    startApp("easytraveling.LandingScreen")
  });

}

let applicationHasStarted = false;

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    store.dispatch(setFirebaseUser(user))
  } else {
    store.dispatch(setFirebaseUser(null))
  }

  if (!applicationHasStarted) {
    if (Platform.OS === "ios") {
      startApplication();
    } else {
      Navigation.isAppLaunched()
        .then((appLaunched) => {
          if (appLaunched) {
            startApplication();
          }
          new NativeEventsReceiver().appLaunched(startApplication);
        })
    }
    applicationHasStarted = true;
  }

});