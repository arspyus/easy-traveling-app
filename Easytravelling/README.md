

## Table of Contents

* [Running on android](#running-on-android)
* [Running on ios](#running-on-ios)

## Running on android

You should follow to following steps to run the project on android

1. Clone the project - run «git clone https://YOUR_USERNAME@bitbucket.org/arspyus/easy-traveling-app.git» (if you want to clone develop branch you can run command with «-b develop» argument)
2. Go to «Easytravelling» folder, which is the folder where app is located.
3. Install node modules - run «npm install». This will install all node modules, which are need to run the project.
4. Run on device or emulator - run «npm run android» command in terminal. This command will run the app in development mode. It attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup). Also make sure that you can run adb from your terminal.
   Sometimes you may need to clear gradlew cache. For this go to android folder and run «gradlew clean» command. Make sure that you have gradlew.bat in your android folder.
5. Build app for debug - Run «npm run android-bundle-dev» to create app bundle file. Next go to the android folder and run «gradlew assembleDebug» command. Again make sure that you have gradlew.bat in your android folder. The generated APK can be found in android/app/build/outputs/apk/ folder.
6. Build app for release - go to android folder and run «gradlew assembleRelease» command. Again make sure that you have gradlew.bat in your android folder. The generated APK can be found in android/app/build/outputs/apk/ folder, and is ready to be distributed.

## Running on iOS

You should follow to following steps to run the project on android

1. Clone the project - run «git clone https://YOUR_USERNAME@bitbucket.org/arspyus/easy-traveling-app.git» (if you want to clone develop branch you can run command with «-b develop» argument)
2. Go to «Easytravelling» folder, which is the folder where app is located.
3. Install node modules - run «npm install». This will install all node modules, which are needed to run the project.
4. Install Pods - go to ios folder, there you will file the PodFile of project, run «pod install» command in terminal, this will install all of pod libraries and dependencies, which are needed to run ios project. Make sure you have the CocoaPods gem installed on your machine.
4. Run on device or emulator - Go to ios folder and double click on «EasyTraveling.xcworkspace». This will open your project in Xcode. Choose the device , and click on play button . This will run the app on your device or emulator. Sometimes emulator can’t load bundle from the first attempt, and you will see error message on simulator, just click on «reload JS» button at the bottom.
   You can also run project via nom command. In project folder run «npm run ios» command. This will run project on iPhone 6 emulator. To run project on real device you will need to run project by xCode.
!!! Note, you will need Xcode version 9.2 or upper 
5. Archive project - Select «Generic iOS Device» in devices list on xCode. Navigate to Product->Archive. To generate debug build : Go to Product->Scheme->Edit Scheme. Click to Archive tab, and set «Build Configuration» to Debug.


