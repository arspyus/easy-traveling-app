<?php

function easytraveling_scripts() {

    $theme = wp_get_theme();
    $ver = $theme->get( 'Version' );

    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(),$ver);
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(),$ver);
    wp_enqueue_style( 'linearicons', get_template_directory_uri() . '/css/linearicons.css', array(),$ver);
    wp_enqueue_style( 'ionicons', get_template_directory_uri() . '/css/ionicons.min.css', array(),$ver);
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(),$ver);
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl-carousel.css', array(),$ver);
    wp_enqueue_style( 'preset', get_template_directory_uri() . '/css/preset.css', array(),$ver);
    wp_enqueue_style( 'scroll-animation', get_template_directory_uri() . '/css/scroll-animation.css', array(),$ver);
    wp_enqueue_style( 'style', get_stylesheet_uri(), array(),$ver);
    wp_enqueue_style( 'color3', get_template_directory_uri() . '/css/triggerPlate/color3.css', array(),$ver);

    //Uncomment for gradient
    //wp_enqueue_style( 'color-3', get_template_directory_uri() . '/css/triggerPlate/gradient/color-3.css');


    wp_enqueue_script("jquery");



    wp_add_inline_script( 'theme-js-detection',
        'jQuery(document).ready(function(){$ = jQuery.noConflict(true);});'
    );
    wp_enqueue_script( 'theme-js-detection', null, array(), false, true);



    wp_register_script( 'gmap', get_template_directory_uri() . '/js/gmap.js' ,array(),$ver, true);
    $template_directory_uri = get_template_directory_uri();
    $gmap_data = array(
        'theme_uri' => $template_directory_uri
    );
    wp_localize_script( 'gmap', 'gmap_data', $gmap_data );
    wp_enqueue_script( 'gmap' );
    wp_enqueue_script( 'googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBumN1FxU0vF8HVI_qy9yFlwcZ4Wop_RtY',array(),$ver,true);
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js',array(),$ver,true);
    wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/waypoints.min.js',array(),$ver,true);
    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/magnific-popup.min.js',array(),$ver,true);
    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl-carousel.min.js',array(),$ver,true);
    wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js',array(),$ver,true);
    wp_enqueue_script( 'custom-animations', get_template_directory_uri() . '/js/custom-animations.js',array(),$ver,true);
    wp_enqueue_script( 'theme', get_template_directory_uri() . '/js/theme.js',array(),$ver,true);



}
add_action( 'wp_enqueue_scripts', 'easytraveling_scripts' );


function easytraveling_javascript_detection() {
	echo '<script type="text/javascript">jQuery(document).ready(function(){$ = jQuery.noConflict(true);});</script>';
}
add_action( 'wp_footer', 'easytraveling_javascript_detection');

function easytraveling_setup() {


	load_theme_textdomain( 'twentyseventeen' );

	add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'easytraveling_setup' );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

register_nav_menus( array(
		'landingmain' => __( 'Landing Main',      'easytraveling' )
	) );



