<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118974299-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-118974299-1');
		</script>

        <!-- META -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="author" content="aparg">
               
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/easytraveling/img/Favicon_app.png">
		<?php wp_head(); ?>
    </head>
	<body <?php body_class(); ?> data-spy="scroll" data-target=".navbar" data-offset="50">
	 <div class="loading">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_one"></div>
                <div class="object object_two"></div>
                <div class="object object_three"></div>
            </div>
        </div>
    </div>     
	<?php if ( has_nav_menu( 'landingmain' ) ) : ?>
			<nav class="navbar navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="smoothscroll navbar-brand" href="#home">
                            <img src="/wp-content/themes/easytraveling/img/nav/logo-faded.png" alt="logo">
                            <img class="logo-blue" src="/wp-content/themes/easytraveling/img/nav/logo-faded-blue.png" alt="logo-blue">
                        </a>
                        <a class="smoothscroll btn-getnow" href="#get-app">Ներբեռնել</a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
				<?php
					// Primary navigation menu.
					wp_nav_menu( array(
						'menu_class'     => 'nav navbar-nav navbar-right smoothscroll',
						'theme_location' => 'landingmain',
						'container' => false
					) );
				?>
			</div>
                </div>
            </div>
        </nav>
		<?php endif; ?>
		