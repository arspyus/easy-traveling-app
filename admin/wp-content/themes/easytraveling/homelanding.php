<?php
/*
Template Name: Home Landing
*/
?>
<?php get_header(); ?>

<?php

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		the_content();
		/*
		 * Include the Post-Format-specific template for the content.
		 * If you want to override this in a child theme, then include a file
		 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
		 */

	endwhile;

?>

<?php get_footer(); ?>