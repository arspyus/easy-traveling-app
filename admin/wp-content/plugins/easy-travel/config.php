<?php
defined('ABSPATH') or die('No script kiddies please!');

define("APET_BASE_CHARS", 'HbUlYmGoAd0ScKq6Er5PuZp3OsQCh4RfNMtV8kJiLv9yeI1aWgFj2zT7DnBw');
define("APET_BONUS_UNIT_PRICE", '500');
define("APET_SPEC_OFFER_EDIT_TIME", '60'); // in minutes
define("APET_ERROR_LOG", true);
define("APET_PLUGIN_PATH", plugin_dir_path(__FILE__));
define("APET_PLUGIN_URL", plugin_dir_url(__FILE__));
define("APET_REQUESTS_CHECK_INTERVAL", '10000');// in miliseconds

define("APET_WEB_MIN_VERSION",'1.0.57');// Force to refresh web dashboard if lower then this. Must be change only for major incompatible releases.
define("APET_APP_MIN_VERSION", '2.0.3');// Force to update app if lower then this. Must be change only for major incompatible releases.
define("APET_OLD_APPS_FORCE_UPDATE_FIX", "<script>var apet_fix_majorUpdateText,apet_fix_market_url,apet_fix_appendToBody;setTimeout(function(){apet_fix_majorUpdateText={\"major-update-title\":{en:\"Major Update\",ru:\"Важное Обновление\",hy:\"Կարևոր Թարմացում\"},\"major-update-content\":{en:\"Please update application to continue.\",ru:\"Пожалуйста обновите приложение, чтобы продолжить.\",hy:\"Խնդրում ենք թարմացնել հավելվածը շարունակելու համար:\"},\"major-update-update-btn\":{en:\"Update\",ru:\"Обновить\",hy:\"Թարմացնել\"}};apet_fix_market_url={'ios':'https://itunes.apple.com/us/app/easytraveling/id1368470794','android':'https://play.google.com/store/apps/details?id=am.easytraveling.easytraveling'};apet_fix_appendToBody='<div id=\"major-update-popup\" class=\"modal\"><div class=\"modal-content\"><h4>'+apet_fix_majorUpdateText['major-update-title'][application.language]+'</h4><p >'+apet_fix_majorUpdateText['major-update-content'][application.language]+'</p></div><div class=\"modal-footer\"><a href=\"#!\" id=\"major-update-update\" class=\"waves-effect waves-green btn-flat\" onclick=\"window.open( apet_fix_market_url[application.deviceInfo().platform], \'_system\');\">'+apet_fix_majorUpdateText['major-update-update-btn'][application.language]+'</a></div></div>';jQuery('#main').append(apet_fix_appendToBody);jQuery('#major-update-popup').modal({'dismissible':false,'ready':function(){jQuery('html').addClass('modal-opened')},'complete':function(){jQuery('html').removeClass('modal-opened')}});jQuery('#major-update-popup').modal('open');}, 1000);</script>");// Fix for apps lower then v. 1.0.21 to force update to min major app version.

define("APET_TRAVELPAYOUT_TOKEN",'1d1964a73fbdfefacb0dc6d5b4653899');
define("APET_GOOGLE_MAP_LIB",'<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBN0dmaRZeTml3chn2f8B7tDRX22NtiUU&libraries=places&language=en"></script>');
define("APET_GA", "<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-118974299-1\"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-118974299-1');</script>");
define("APET_FB_CHAT", "<!-- Load Facebook SDK for JavaScript --><div id=\"fb-root\"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><!-- Your customer chat code --><div class=\"fb-customerchat\"  attribution=setup_tool  page_id=\"214959275757931\"  theme_color=\"#7cb342\"  greeting_dialog_display=\"hide\"></div>");
define('APET_MAILGUN_URL', 'https://api.mailgun.net/v3/em.easytraveling.am');
define('APET_MAILGUN_KEY', 'key-29034172a5ab4ed39612b658335e5f57');

define("APET_CONFIG_EMAIL", 'easytraveling@em.easytraveling.am');
define("APET_EMAIL_SENDER_NAME", 'EasyTraveling');

define("APET_PACKAGE_PLANS",array(
    "free_plan"=>array(
        "title"                 =>"Free Plan",
        "special_offers_limit"  =>0,
        "request_offers_limit"  =>5
    ),
    "standard_plan"=>array(
        "title"                 =>"Standard Plan",
        "special_offers_limit"  =>0,
        "request_offers_limit"  =>-1
    ),
    'premium_plan'=>array(
        "title"                 =>"Premium Plan",
        "special_offers_limit"  =>5,
        "request_offers_limit"  =>-1
    ),
    'unlimited_plan'=>array(
        "title"                 =>"Unlimited Plan",
        "special_offers_limit"  =>-1,
        "request_offers_limit"  =>-1
    )
));

define('APET_DEBUG', (get_site_url()  != 'https://easytraveling.am' ) ? true : false);