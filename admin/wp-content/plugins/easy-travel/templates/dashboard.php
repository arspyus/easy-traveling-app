<?php
/**
 * Template Name: dashboard
 *
 * 
 */
$role = apet_get_logged_user_role();
$user_id = get_current_user_id();
$user_data = get_userdata($user_id);
$user_name = $user_data->user_login;
$data = get_plugin_data( APET_PLUGIN_PATH . "easy-traveling.php", false, false );
$plugin_version = $data['Version'];

global $wpdb;
$tour_table = $wpdb->prefix . 'tour_agencies';

$tour_data = $wpdb->get_results("SELECT id,status FROM `$tour_table` WHERE tour_admin='$user_id' ", ARRAY_A);

$tour_id = !empty($tour_data) ? $tour_data[0]['id'] : 'all';
$tour_status = !empty($tour_data) ? $tour_data[0]['status'] : '';

global $apet_file_path;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <?php echo APET_GA; ?>
        <?php echo APET_GOOGLE_MAP_LIB ?>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">

        <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-32x32.png" sizes="32x32" />
        <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-180x180.png" />
        <meta name="msapplication-TileImage" content="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-270x270.png" />

        <title>EasyTraveling</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />

        <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/materialize.min.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/style.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />
        <script>
            var apetWebVersion ="<?php echo $plugin_version?>";
            var apetTempLogoUrl = "<?php echo APET_PLUGIN_URL . 'assets/img/bg-preloader.png' ?>";
            var apetRequestAlertSoundUrl = "<?php echo APET_PLUGIN_URL . 'assets/audio/request-alert.mp3' ?>";
            var apetRequestsCheckInterval = "<?php echo APET_REQUESTS_CHECK_INTERVAL ?>";
        </script>
    </head>
    <body data-active="<?php echo $tour_status !== 'inactive' ? 'dashboard' : 'settings' ?>" id="body">
        <?php echo APET_FB_CHAT; ?>
        <div id="main" data-user-role="<?php echo $role; ?>" data-user-id="<?php echo $user_id ?>" data-tour-id="<?php echo $tour_id ?>" data-tour-status="<?php echo $tour_status; ?>">
            <div id="header">
                <ul id="user-dropdown" class="dropdown-content">
                     <li><a href="javascript:void(0);"  data-destination="settings" class="page-redirect settings-menu-btn   ">Settings</a></li>
                     <li class="divider"></li>
                    <li><a class="log-out" href="javascript:void(0);">Log Out</a></li>
                </ul>
                <div class="navbar-fixed">
                    <nav>
                        <div class="nav-wrapper light-green darken-1">
                            <a  data-destination="requests" href="javascript:void(0);" class="brand-logo header-logo center page-redirect"></a>
                            <div class="menu-slide-wrap">
                                <a  data-activates="menu-slide-out" class="header-menu-button"><i class="material-icons">&#xE5D2;</i></a>
                                <?php if(APET_DEBUG):?>
                                    <span class="btn waves-effect waves-light red">DEBUG MODE</span>
                                <?php endif; ?>
                            </div>
                            <div class="notification-badge-container">

                            </div>
                            <ul class="right ">
                                <li>
                                    <a class="dropdown-button" href="javascript:void(0);"  data-target="user-dropdown">
                                        <span class="agency-name"><?php if($role == 'tour-super-admin'){ ?>Admin<?php } ?></span><i class="material-icons right">arrow_drop_down</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <ul id="menu-slide-out" class="sidenav">
                <li class="user-view-container">
                    <div class="user-view">
                        <a href="javascript:void(0);" class="sidenav-close page-redirect user-photo-wrap settings-menu-btn" data-destination="settings">
                            <span class="user-photo">
                                <img class="circle" src=""/>
                                <span></span>
                            </span>
                        </a>
                        <a href="javascript:void(0);" class="sidenav-close page-redirect settings-menu-btn" data-destination="settings">
                            <span class="nav-name-place white-text name"></span>
                        </a>
                        <a href="javascript:void(0);" class="sidenav-close page-redirect settings-menu-btn" data-destination="settings">
                            <span class="nav-package-plan white-text package-plan"></span>
                        </a>
                    </div>
                </li>

                <li class="sidenav-close">  <a data-destination="requests" class="<?php echo $tour_status !== 'inactive' ? 'page-redirect' : 'side-menu-disabled' ?> request-menu-btn">Requests</a></li>
                <li class="sidenav-close">  <a   data-destination="special-offers" class="<?php echo $tour_status !== 'inactive' ? 'page-redirect' : 'side-menu-disabled' ?> offres-menu-btn ">Special Offers</a></li>
                <li><div class="divider"></div></li>
                <li class="sidenav-close">   <a  data-destination="settings" class="page-redirect settings-menu-btn   ">Settings</a></li>
                <li>
                    <div class="divider"></div>
                </li>
                <li class="menu-footer">
                    <div class="left-align">
                        <div class="contacs-block left-align">
                            <a href="https://www.facebook.com/EasyTraveling-214959275757931/" target="_blank" class="contact-facebook"><i class="extra-icons">&#xea90;</i></a>
                            <a href="https://www.instagram.com/easytraveling_app/" target="_blank" class="contact-instagram"><i class="extra-icons insta">&#xea92;</i></a>
                            <div class="phone-block">
                                <div><i class="material-icons light-green-text text-darken-1">&#xE0CD;</i><span>+(374)96886655</span></div>
                                <div><i class="material-icons light-green-text text-darken-1">&#xE0BE;</i></a><span>info@easytraveling.am</span></div>
                            </div>
                        </div>
                        <div class="web-app-version">
                            V. <?php $data = get_plugin_data( APET_PLUGIN_PATH . "easy-traveling.php", false, false ); echo $data['Version'] ?>
                        </div>
                    </div>
                </li>
            </ul>



            <!--================ Page ===========-->
            <div id="special-offers" class="col s12 page">
                <div class="wrapper">
                    <div class="container">
                         <span class="spec-offers-count-info">Offers left: <span class="spec-offers-count"></span></span>
                             <h4>Monthly Special Offers</h4>
                            <div class="special-offers-filter filter-form-container box-element box-shadow white">
                                <form enctype="multipart/form-data" id="special-offers-filter-form" class="submitable-form" autocomplete="off">
                                    <div class="grey-text text-darken-3">
                                        <div class="row">
                                            <div class="input-field col s3">
                                                <select id="special-offers-filter-status" name="status" class="app-input">
                                                    <option value="active">Active</option>
                                                    <option value="expired">Expired</option>
                                                    <option value="deleted">Deleted</option>
                                                </select>
                                                <label for="special-offers-filter-status">Status</label>
                                            </div>
                                            <div class="input-field col s7">
                                                <input type="text" placeholder="Title" id="special-offers-search" class="autocomplete app-input" name="search">
                                                <label for="app-users-search">Search</label>
                                            </div>
                                            <div class="input-field col s2 filter-button-container">
                                                <button type="submit" form="special-offers-filter-form" data-action="special-offers-filter"
                                                        class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                                <a href="javascript:void(0);" id="reset-special-offers-filter" class="reset-filter-btn">Reset Filter</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="offer-empty-msg nothing-found">
                                No Offers Yet
                            </div>
                            <div class="special-offers-container">

                            </div>
                           <a class="special-offers-load-more load-more"><i class="material-icons">&#xE042;</i></a>


                        <div id="cancel-spec-offer-modal" class="modal">
                            <div class="modal-content">
                                <h4><span>Delete</span>?</h4>
                                <p>Are you sure you want delete Offer</p>
                            </div>
                            <div class="modal-footer">
                                <a href="javascript:void(0);"
                                   class="yes modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
                                <a href="javascript:void(0);"
                                   class="no modal-action modal-close waves-effect waves-green btn-flat">No</a>
                            </div>
                        </div>

                         <div class=" fixed-action-btn">
                                <a class="add-special-offer btn-floating btn-large light-green darken-1 modal-trigger "
                                   href="#popup-modal">
                                    <i class="large material-icons">add</i>
                                </a>

                            </div>
                    </div>
                </div>
            </div>




            <!--================ Page ===========-->
            <div id="requests" class="col s12 page">
                <div class="wrapper">
                    <div class="container">
                        <span class="req-offers-count-info light-green-text text-darken-1">Request Offers left: <span class="req-offers-count"></span></span>
                         <h4>Requests</h4>
                        <div class="requests-filter box-element box-shadow white">
                            <form enctype="multipart/form-data" id="requests-filter-form" class="submitable-form" autocomplete="off">
                            <div class="grey-text text-darken-3">
                                <div class="row">
                                    <div class="input-field col s12 m4 l3">
                                        <select id="filter-status" name="status" class="app-input">
                                            <option value="" selected>All</option>
                                            <option value="pending">Pending</option>
                                            <option value="offered">Offered</option>
                                            <option value="pre-booked">Pre-Booked</option>
                                            <option value="processing">Processing</option>
                                            <option value="confirmed">Confirmed</option>
                                            <option value="booked">Booked</option>
                                        </select>
                                        <label for="filter-status">Status</label>
                                    </div>
                                    <div class="input-field col s12 m4 l1">
                                        <input type="text" id="filter-id" class="app-input validate-positive-int" autocomplete="off" name="id">
                                        <label for="filter-id">ID</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2">
                                        <input type="text" id="filter-country" class="autocomplete app-input" name="country">
                                        <label for="filter-country">Country</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2">
                                        <input type="text" id="filter-from" class="datepicker app-input" name="from" readonly/>
                                        <label for="filter-from">From</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2">
                                        <input type="text" id="filter-to" class="datepicker app-input" name="to" readonly/>
                                        <label for="filter-to">To</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2  center filter-button-container ">
                                        <button type="submit" form="requests-filter-form" data-action="requests-filter"
                                                class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                        <a href="javascript:void(0);" id="reset-filter" class="reset-filter-btn">Reset Filter</a>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>

                        <div class="request-empty-msg nothing-found">
                            No Requests Yet
                        </div>
                        <div class="requests-container">
                        </div>
                        <div id="cancel-req-offer-modal" class="modal">
                            <div class="modal-content">
                                <h4><span>Cancel</span>?</h4>
                                <p>Are you sure you want to cancel your offer</p>
                            </div>
                            <div class="modal-footer">
                                <a href="javascript:void(0);"
                                   class="yes modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
                                <a href="javascript:void(0);"
                                   class="no modal-action modal-close waves-effect waves-green btn-flat">No</a>
                            </div>
                        </div>
                        <a class="requets-load-more load-more"><i class="material-icons">&#xE042;</i></a>
                    </div>
                </div>
                <div id="delete-doc-template" class="modal">
                    <div class="modal-content">
                        <h4><span>Delete</span>?</h4>
                        <p>Are you sure you want delete document template</p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" class="yes modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
                        <a href="javascript:void(0);" class="no modal-action modal-close waves-effect waves-green btn-flat">No</a>
                    </div>
                </div>
            </div>




            <!--================ Page ===========-->


            <div id="settings" class="col s12 page">
                <div class="wrapper">
                    <div class="container">
                          <h4>Settings</h4>
                            <div class="form-container main-settings-container box-shadow white">
                                <form enctype="multipart/form-data" id="settings-form" class="submitable-form"
                                      autocomplete="off">

                                    <div class="row">
                                        <div class="settings-input input-field col s12">
                                            <input placeholder="" id="settings-country" type="text" name="country" class="validate-required validate-country autocomplete app-input">
                                            <label for="settings-country" >Country</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" settings-input input-field col s12">
                                            <input placeholder="" id="settings-city" name="city" type="text"  class="validate-required app-input">
                                            <label for="settings-city" >City</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" settings-input input-field col s12">
                                            <input placeholder="" id="settings-address" name="address" type="text"  class="validate-required app-input">
                                            <label for="settings-address" >Address</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" settings-input input-field col s12">
                                            <input placeholder="" id="settings-agency-name" name="tour_agent" type="text"  class="validate-required app-input">
                                            <label for="settings-agency-name" >Tour Agency Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" settings-input input-field col s12">
                                            <input placeholder="" id="settings-latitude" name="latitude" type="text"  class="validate-required app-input">
                                            <label for="settings-latitude" >Latitude</label>
                                        </div>
                                    </div> <div class="row">
                                        <div class=" settings-input input-field col s12">
                                            <input placeholder="" id="settings-longitude" name="longitude" type="text"  class="validate-required app-input">
                                            <label for="settings-longitude" >Longitude</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="settings-input input-field col s12">
                                            <input placeholder="" id="settings-phone" type="tel" name="phone"
                                                   class="validate-phone app-input" value="+374">
                                            <label for="settings-phone" data-translation-key="phone">Agency Phone</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="settings-input input-field col s12">
                                            <input placeholder="" id="settings-email" type="email" name="email" class="validate-email app-input">
                                            <label for="settings-email">Agency Email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="settings-input input-field col s12">
                                            <input placeholder="" id="settings-website" type="url" name="website" class="app-input">
                                            <label for="settings-website">Agency Website</label>
                                        </div>
                                    </div>
                                    <div class="row tour-agency-img-row">
                                        <div class="settings-input file-field input-field col s12">
                                            <input disabled  class="file-path validate-required" type="text">
                                            <div class="col s12 file-field-container">
                                                <div class="file-filed-button-container ">
                                                    <div class="tour-agency-img-btn light-green darken-1 waves-effect waves-light btn">
                                                        <span>Photo</span>
                                                        <input class="tour-img-file" type="file">
                                                    </div>
                                                </div>
                                                <div class="file-field-image-container right">
                                                    <img class="tour-agency-img materialboxed placeholder-image" src="<?php echo APET_PLUGIN_URL . 'assets/img/bg-preloader.png' ?>"/>
                                                    <span class="extra-notes">For best experience use squared and transparent images.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select class="app-input" name="prefered_countries" multiple
                                                    id="settings-pref-countries">
                                                <option value="" selected disabled>Choose Prefered Countries</option>

                                            </select>
                                            <label>Prefered Countries</label>
                                        </div>
                                        <input class="data-collect" type="hidden" name="chosen-tour" value="">
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" form="settings-form" data-action="settings"
                                                    class="submit save-settings light-green darken-1 waves-effect waves-light btn">
                                                Save Changes
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="form-container change-password-container">
                                <form id="settings-password-form" class="submitable-form">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <ul class="collapsible" data-collapsible="accordion">
                                                <li>
                                                    <div class="collapsible-header"><i
                                                            class="material-icons">&#xE0DA;</i><span>Change Password</span>
                                                    </div>
                                                    <div class="collapsible-body">
                                                        <div class="row">
                                                            <div class="input-field col s12">
                                                                <input id="settings-current-password" type="password"
                                                                       name="current-password"
                                                                       class="validate-password app-input">
                                                                <label for="settings-current-password">Current
                                                                    Password</label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="input-field col s12">
                                                                <input id="settings-new-password" type="password"
                                                                       name="new-password"
                                                                       class="validate-password app-input">
                                                                <label for="registr-password">New Password</label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="input-field col s12">
                                                                <input id="settings-retype-password" type="password"
                                                                       name="retype-password"
                                                                       class="validate-password app-input">
                                                                <label for="settings-retype-password">Retype
                                                                    Password</label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="input-field col s12">
                                                                <button type="submit" form="settings-password-form"
                                                                        data-action="change-password"
                                                                        class="disabled submit change-password light-green darken-1 waves-effect waves-light btn">
                                                                    Save Changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>


                    </div>
                </div>
            </div>

            <div id="popup-modal" class="modal  modal-fixed-footer">

            </div>

            <div id="upgrade-plan-modal" class="modal">
                <div class="modal-content">
                    <h4 ><span>Upgrade</span>?</h4>
                    <p >You have exceeded usage limits in your current plan. To complete this action, please upgrade to a new plan.</p>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" class="modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                    <a href="https://easytraveling.am/#pricing" target="_blank" class="modal-action modal-close waves-effect waves-green btn-flat ">Upgrade</a>
                </div>
            </div>

            <div class="full-page progress">
                <div class="indeterminate"></div>
            </div>
        </div>

        <!------ Loader --------->
        <div class="loader-layer">
            <div class="loader-container">
                <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="white-layer"></div>
        </div>

        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/jquery-2.2.4.min.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/ajax-uploader.min.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/materialize.min.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/countries-list.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/airlines-list.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/index.js' . '?pv=' . $plugin_version ?>"></script>
    </body>
</html>
