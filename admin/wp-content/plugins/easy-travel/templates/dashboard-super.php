<?php
/**
 * Template Name: dashboard
 *
 *
 */
$role = apet_get_logged_user_role();
$user_id = get_current_user_id();
$user_data = get_userdata($user_id);
$user_name = $user_data->user_login;
$data = get_plugin_data( APET_PLUGIN_PATH . "easy-traveling.php", false, false );
$plugin_version = $data['Version'];

global $wpdb;
$tour_table = $wpdb->prefix . 'tour_agencies';

$tour_data = $wpdb->get_results("SELECT id,status FROM `$tour_table` WHERE tour_admin='$user_id' ", ARRAY_A);

$tour_id = !empty($tour_data) ? $tour_data[0]['id'] : 'all';
$tour_status = !empty($tour_data) ? $tour_data[0]['status'] : '';

global $apet_file_path;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php echo APET_GA; ?>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-32x32.png" sizes="32x32"/>
    <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-192x192.png" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-180x180.png"/>
    <meta name="msapplication-TileImage" content="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-270x270.png"/>

    <title>EasyTraveling</title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>

    <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/materialize.min.css' . '?pv=' . $plugin_version ?>"
          type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/style.css' . '?pv=' . $plugin_version ?>" type="text/css"
          media="screen"/>
    <script>
        var apetWebVersion ="<?php echo $plugin_version?>";
        var apetPackagePlans = <?php echo json_encode(APET_PACKAGE_PLANS); ?>;
        var apetTempLogoUrl = "<?php echo APET_PLUGIN_URL . 'assets/img/bg-preloader.png' ?>";
        var apetRequestAlertSoundUrl = "<?php echo APET_PLUGIN_URL . 'assets/audio/request-alert.mp3' ?>";
        var apetRequestsCheckInterval = "<?php echo APET_REQUESTS_CHECK_INTERVAL ?>";
    </script>
</head>
<body data-active="<?php echo $tour_status !== 'inactive' ? 'dashboard' : 'settings' ?>" id="body">
<div id="main" data-user-role="<?php echo $role; ?>" data-user-id="<?php echo $user_id ?>"
     data-tour-id="<?php echo $tour_id ?>" data-tour-status="<?php echo $tour_status; ?>">
    <div id="header">
        <ul id="user-dropdown" class="dropdown-content">
            <li><a class="log-out" href="javascript:void(0);">Log Out</a></li>
        </ul>
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper light-green darken-1">
                    <a data-destination="requests" href="javascript:void(0);"
                    class="brand-logo header-logo center page-redirect"></a>
                    <div class="menu-slide-wrap">
                        <a data-activates="menu-slide-out" class="header-menu-button"><i
                            class="material-icons">&#xE5D2;</i></a>
                        <?php if(APET_DEBUG):?>
                            <span class="btn waves-effect waves-light red">DEBUG MODE</span>
                        <?php endif; ?>
                    </div>
                    <div class="notification-badge-container">

                    </div>
                    <ul class="right ">
                        <li><a class="dropdown-button" href="javascript:void(0);" data-target="user-dropdown"><span
                                        class="agency-name"><?php if ($role == 'tour-super-admin') { ?>Admin<?php } ?></span><i
                                        class="material-icons right">arrow_drop_down</i></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <ul id="menu-slide-out" class="sidenav">
        <li class="user-view-container">
            <div class="user-view">
                <a href="javascript:void(0);"
                   class="user-photo-wrap settings-menu-btn sidenav-close"><span class="user-photo"><img
                                class="circle"
                                src=""/><span>A</span></span></a>
                <a href="javascript:void(0);"
                  settings-menu-btn"
                   data-destination="settings"><span
                            class="nav-name-place white-text name sidenav-close">Super Admin</span></a>
                <a href="javascript:void(0);"
                   class=" settings-menu-btn">
                    <span class="nav-address-place white-text address sidenav-close">Dashboard</span></a>

            </div>
        </li>
        <li><a data-destination="requests" class="page-redirect request-menu-btn sidenav-close">Requests</a></li>
        <li><a data-destination="special-offers" class="page-redirect offres-menu-btn  sidenav-close ">Special Offers</a></li>
        <li>
            <div class="divider"></div>
        </li>
        <li><a data-destination="tour-agencies" class="page-redirect agencies-menu-btn  sidenav-close">Tour Agencies</a></li>
        <li>
            <div class="divider"></div>
        </li>
        <li><a data-destination="app-users" class="page-redirect app-users-menu-btn  sidenav-close">App Users</a></li>
        <li><a data-destination="bonus-details" class="page-redirect bonus-menu-btn  sidenav-close">Bonuses</a></li>
        <li>
            <div class="divider"></div>
        </li>
        <li><a data-destination="stats" class="page-redirect stats-menu-btn sidenav-close">Statistics</a></li>
        <li>
            <div class="divider"></div>
        </li>
        <li class="menu-footer">
            <div class="left-align">
                <div class="contacs-block left-align">
                    <a href="https://www.facebook.com/EasyTraveling-214959275757931/" target="_blank"
                       class="contact-facebook"><i class="extra-icons">&#xea90;</i></a>
                    <a href="https://www.instagram.com/easytraveling_app/" target="_blank" class="contact-instagram"><i
                                class="extra-icons insta">&#xea92;</i></a>
                    <div class="phone-block">
                        <div><i class="material-icons light-green-text text-darken-1">&#xE0CD;</i><span>+(374)96886655</span>
                        </div>
                        <div><i class="material-icons light-green-text text-darken-1">&#xE0BE;</i></a><span>info@easytraveling.am</span>
                        </div>
                    </div>
                </div>
                <div class="web-app-version">
                    V. <?php $data = get_plugin_data(APET_PLUGIN_PATH . "easy-traveling.php", false, false);
                    echo $data['Version'] ?>
                </div>
            </div>
        </li>
    </ul>


    <!--================ Page ===========-->
    <div id="special-offers" class="col s12 page">
        <div class="wrapper">
            <div class="container">
                <h4>Monthly Special Offers</h4>
                <div class="special-offers-filter filter-form-container box-element box-shadow white">
                    <form enctype="multipart/form-data" id="special-offers-filter-form" class="submitable-form" autocomplete="off">
                        <div class="grey-text text-darken-3">
                            <div class="row">
                                <div class="input-field col s3">
                                    <select id="special-offers-filter-status" name="status" class="app-input">
                                        <option value="active">Active</option>
                                        <option value="expired">Expired</option>
                                        <option value="deleted">Deleted</option>
                                    </select>
                                    <label for="special-offers-filter-status">Status</label>
                                </div>
                                <div class="input-field col s7">
                                    <input type="text" placeholder="Title, Agency Name" id="special-offers-search" class="autocomplete app-input" name="search">
                                    <label for="app-users-search">Search</label>
                                </div>
                                <div class="input-field col s2 filter-button-container">
                                    <button type="submit" form="special-offers-filter-form" data-action="special-offers-filter"
                                            class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                    <a href="javascript:void(0);" id="reset-special-offers-filter" class="reset-filter-btn">Reset Filter</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="offer-empty-msg nothing-found">
                    No Offers Yet
                </div>
                <div class="special-offers-container">

                </div>
                <a class="special-offers-load-more load-more"><i class="material-icons">&#xE042;</i></a>
           </div>
        </div>
    </div>


    <!--================ Page ===========-->
    <div id="requests" class="col s12 page">
        <div class="wrapper">
            <div class="container">

                    <h4>Requests</h4>
                    <div class="requests-filter filter-form-container box-element box-shadow white">
                        <form enctype="multipart/form-data" id="requests-filter-form" class="submitable-form" autocomplete="off">
                            <div class="grey-text text-darken-3">
                                <div class="row">
                                    <div class="input-field col s12 m4 l3">
                                        <select id="filter-status" name="status" class="app-input">
                                            <option value="">All</option>
                                            <option value="pending">Pending</option>
                                            <option value="offered">Offered</option>
                                            <option value="pre-booked">Pre-Booked</option>
                                            <option value="processing">Processing</option>
                                            <option value="confirmed">Confirmed</option>
                                            <option value="booked">Booked</option>
                                            <option value="canceled">Canceled</option>
                                        </select>
                                        <label for="filter-status">Status</label>
                                    </div>
                                    <div class="input-field col s12 m4 l1">
                                        <input type="text" id="filter-id" class="app-input validate-positive-int" autocomplete="off" name="id">
                                        <label for="filter-id">ID</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2">
                                        <input type="text" id="filter-country" class="autocomplete app-input" name="country">
                                        <label for="filter-country">Country</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2">
                                        <input type="text" id="filter-from" class="datepicker app-input" name="from"/>
                                        <label for="filter-from">From</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2">
                                        <input type="text" id="filter-to" class="datepicker app-input" name="to"/>
                                        <label for="filter-to">To</label>
                                    </div>
                                    <div class="input-field col s12 m4 l2  center filter-button-container ">
                                        <button type="submit" form="requests-filter-form" data-action="requests-filter"
                                                class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                        <a href="javascript:void(0);" id="reset-filter" class="reset-filter-btn">Reset Filter</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="request-empty-msg nothing-found">
                        No Requests Yet
                    </div>
                    <div class="requests-container">

                    </div>
                    <a class="requets-load-more load-more"><i class="material-icons">&#xE042;</i></a>

            </div>
        </div>
        <div id="delete-doc-template" class="modal">
            <div class="modal-content">
                <h4><span>Delete</span>?</h4>
                <p>Are you sure you want delete document template</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);"
                   class="yes modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
                <a href="javascript:void(0);"
                   class="no modal-action modal-close waves-effect waves-green btn-flat">No</a>
            </div>
        </div>
    </div>

    <!--================ Page ===========-->
    <div id="tour-agencies" class="col s12 page">
        <div class="wrapper">
            <div class="container">

                    <h4>Tour Agencies</h4>
                    <div class="tour-agencies-filter filter-form-container box-element box-shadow white">
                        <form enctype="multipart/form-data" id="tour-agencies-filter-form" class="submitable-form" autocomplete="off">
                            <div class="grey-text text-darken-3">
                                <div class="row">
                                    <div class="input-field col s3">
                                        <select id="tour-agencies-filter-status" name="status" class="app-input">
                                            <option value="active">Active</option>
                                            <option value="pending">Pending</option>
                                            <option value="deactivated">Deactivated</option>
                                        </select>
                                        <label for="tour-agencies-filter-status">Status</label>
                                    </div>
                                    <div class="input-field col s7">
                                        <input type="text" placeholder="Agency Name, Address, Phone" id="tour-agencies-search" class="autocomplete app-input" name="search">
                                        <label for="tour-agencies-search">Search</label>
                                    </div>
                                    <div class="input-field col s2 filter-button-container">
                                        <button type="submit" form="tour-agencies-filter-form" data-action="tour-agencies-filter"
                                                class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                        <a href="javascript:void(0);" id="reset-tour-agencies-filter" class="reset-filter-btn">Reset Filter</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="agencies-empty-msg nothing-found">
                        No Tour Agencies Yet
                    </div>
                    <div class="tour-agencies-container">

                    </div>
                    <a class="tour-agencies-load-more load-more"><i class="material-icons">&#xE042;</i></a>
                    <div class="fixed-action-btn">
                        <a class="add-tour-agency btn-floating btn-large light-green darken-1 modal-trigger "
                           href="#popup-modal">
                            <i class="large material-icons">add</i>
                        </a>

                    </div>
                    <div id="deactivate-agency-modal" class="modal">
                        <div class="modal-content">
                            <h4><span>Deactivate</span>?</h4>
                            <p>Are you sure you want deactivate Tour Agency</p>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:void(0);"
                               class="yes modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
                            <a href="javascript:void(0);"
                               class="no modal-action modal-close waves-effect waves-green btn-flat">No</a>
                        </div>
                    </div>


            </div>
        </div>
    </div>


    <!--================ Page ===========-->
    <div id="bonus-details" class="col s12 page">
        <div class="wrapper">
            <div class="container">

                    <h4>Bonus Details</h4>
                    <div class="bonus-details-container box-shadow white">
                        <form class="submitable-form" id="bonus-details-form">
                            <div class="row">
                                <div class="input-field col s2 push-s10">
                                    <button type="submit" form="bonus-details-form" data-action="bonus-details"
                                            class="submit light-green darken-1 waves-effect waves-light btn right">
                                        Check
                                    </button>
                                </div>
                                <div class="input-field col s10 pull-s2">
                                    <input id="bonus-id" type="text" name="bonus_id"
                                           class="validate-required app-input">
                                    <label for="bonus-id">Bonuns ID</label>
                                </div>
                            </div>
                        </form>
                        <div class="bonus-user-info">

                        </div>
                    </div>

                    <h4>Add Bonus</h4>
                    <div class="add-user-bonus-container box-shadow white">
                        <form class="submitable-form" id="add-user-bonus-form">
                            <div class="row">
                                <div class="input-field col s2 push-s10">
                                    <button type="submit" form="add-user-bonus-form" data-action="add-user-bonus"
                                            class="submit light-green darken-1 waves-effect waves-light btn right">
                                        Add
                                    </button>
                                </div>
                                <div class="input-field col s5 pull-s2">
                                    <input  id="user-email" type="text" name="user_email"
                                           class="validate-required validate-email app-input">
                                    <label for="user-email">User Email</label>
                                </div>
                                <div class="input-field col s5 pull-s2">
                                    <input  id="bonus-amount" type="text" name="bonus_amount"
                                           class="validate-required validate-positive-int app-input">
                                    <label for="bonus-amount">Bonus Amount</label>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
        <div id="add-user-bonus-popup" class="modal">
            <div class="modal-content">
                <h4><span>Add Bonus</span>?</h4>
                <p>Are you sure you want to add <strong class="popup-bonus-amount"></strong> bonus points to user with <strong class="popup-bonus-email"></strong> email?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);"
                   class="yes modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
                <a href="javascript:void(0);"
                   class="no modal-action modal-close waves-effect waves-red btn-flat">No</a>
            </div>
        </div>
    </div>

    <!--================ Page ===========-->
    <div id="app-users" class="col s12 page">
        <div class="wrapper">
            <div class="container">
                <h4>App Users</h4>
                <div class="app-users-filter filter-form-container box-element box-shadow white">
                    <form enctype="multipart/form-data" id="app-users-filter-form" class="submitable-form" autocomplete="off">
                        <div class="grey-text text-darken-3">
                            <div class="row">
                                <div class="input-field col s3">
                                    <select id="app-users-filter-status" name="status" class="app-input">
                                        <option value="">All</option>
                                        <option value="active">Active</option>
                                        <option value="deactivated">Deactivated</option>
                                        <option value="unconfirmed">Unconfirmed</option>
                                    </select>
                                    <label for="app-users-filter-status">Status</label>
                                </div>
                                <div class="input-field col s7">
                                    <input type="text" placeholder="Email, First name, Last name, Phone" id="app-users-search" class="autocomplete app-input" name="search">
                                    <label for="app-users-search">Search</label>
                                </div>
                                <div class="input-field col s2 filter-button-container">
                                    <button type="submit" form="app-users-filter-form" data-action="app-users-filter"
                                            class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                    <a href="javascript:void(0);" id="reset-app-users-filter" class="reset-filter-btn">Reset Filter</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="app-users-empty-msg nothing-found">
                    No Users Yet
                </div>
                <div class="app-users-container">

                </div>
                <a class="users-load-more load-more"><i class="material-icons">&#xE042;</i></a>
            </div>
        </div>
    </div>


    <div id="stats" class="col s12 page">
        <div class="wrapper">
            <div class="container">
                <h4>Statistics</h4>
                <div class="stats-filter filter-form-container box-element box-shadow white">
                    <form enctype="multipart/form-data" id="stats-filter-form" class="submitable-form filter-type-user" autocomplete="off">
                        <div class="grey-text text-darken-3">
                            <div class="row">
                                <div class="input-field col s3">
                                    <select id="stats-filter-type" name="type" class="app-input">
                                        <option value="user">Users</option>
                                        <option value="agency">Tour Agenices</option>
                                        <option value="special_offers">Special Offers</option>
                                        <option value="requests">Requests</option>
                                    </select>
                                    <label for="stats-filter-type">Type</label>
                                </div>

                                <div class="input-field col s3 user-stats-sub-type-container">
                                    <select id="user-stats-sub-type" name="sub_type" class="app-input">
                                        <option value="regs">Registrations</option>
                                        <option value="types">Types</option>
                                    </select>
                                    <label for="user-stats-sub-type">Sub-Type</label>
                                </div>


                                <div class="input-field col s3 agency-stats-sub-type-container">
                                    <select disabled id="agency-stats-sub-type" name="sub_type" class="app-input">
                                        <option value="regs">Registrations</option>
                                        <option value="types">Types</option>
                                    </select>
                                    <label for="agency-stats-sub-type">Sub-Type</label>
                                </div>


                                <div class="input-field col s3 requests-stats-sub-type-container">
                                    <select disabled multiple id="requests-stats-sub-type" name="data_types" class="app-input">
                                        <option value="" disabled>Select Data Type</option>
                                        <option value="requests" selected>Requests</option>
                                        <option value="offers" selected>Offers</option>
                                        <option value="books">Books</option>
                                        <option value="offered_requests">Offered Requests</option>
                                        <option value="booked_offers">Booked Offers</option>
                                        <option value="pending" >Pending </option>
                                        <option value="offered" >Offered </option>
                                        <option value="pre-booked" >Pre-booked </option>
                                        <option value="processing" >Processing </option>
                                        <option value="confirmed" >Confirmed </option>
                                        <option value="canceled" >Canceled </option>
                                        <option value="declined" >Declined </option>
                                    </select>
                                    <label for="requests-stats-sub-type">Data-Typee</label>
                                </div>


                                <div class="input-field col s2 stats-dates">
                                    <input type="text" id="stats-filter-from" class="datepicker app-input" name="from"/>
                                    <label for="stats-filter-from">From</label>
                                </div>
                                <div class="input-field col s2 stats-dates">
                                    <input type="text" id="stats-filter-to" class="datepicker app-input" data-date-compare="stats-filter-from" name="to"/>
                                    <label for="stats-filter-to">To</label>
                                </div>
                                <div class="input-field col s2 filter-button-container right">
                                    <button type="submit" form="stats-filter-form" data-action="stats-filter"
                                            class="submit light-green darken-1 waves-effect waves-light btn">Filter</button>
                                    <a href="javascript:void(0);" id="reset-stats-filter" class="reset-filter-btn">Reset Filter</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="stats-empty-msg nothing-found">
                    No Statistics Yet
                </div>
                <div class="stats-container box-element box-shadow white">
                    <div class="chart-container">
                        <canvas id="stats-canvas" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="popup-modal" class="modal  modal-fixed-footer">

    </div>

    <div id="decline-offer-modal" class="modal">
        <div class="modal-content">
            <h4 class="bonus-popup-header"><span>Declined</span>?</h4>
            <p class="bonus-id">Are you sure you want to decline offer</p>
        </div>
        <div class="modal-footer">
            <a href="javascript:void(0);"
               class="decline-offer modal-action modal-close waves-effect waves-red btn-flat">Yes</a>
            <a href="javascript:void(0);"
               class="no modal-action modal-close waves-effect waves-green btn-flat">No</a>
        </div>
    </div>
    <div class="full-page progress">
        <div class="indeterminate"></div>
    </div>
</div>

<!------ Loader --------->
<div class="loader-layer">
    <div class="loader-container">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-green-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="white-layer"></div>
</div>

<script type="text/javascript"
        src="<?php echo APET_PLUGIN_URL . 'assets/js/jquery-2.2.4.min.js' . '?pv=' . $plugin_version ?>"></script>
<script type="text/javascript"
        src="<?php echo APET_PLUGIN_URL . 'assets/js/ajax-uploader.min.js' . '?pv=' . $plugin_version ?>"></script>
<script type="text/javascript"
        src="<?php echo APET_PLUGIN_URL . 'assets/js/materialize.min.js' . '?pv=' . $plugin_version ?>"></script>
<script type="text/javascript"
        src="<?php echo APET_PLUGIN_URL . 'assets/js/countries-list.js' . '?pv=' . $plugin_version ?>"></script>
<script type="text/javascript"
        src="<?php echo APET_PLUGIN_URL . 'assets/js/chart.bundle.min.js' . '?pv=' . $plugin_version ?>"></script>
<script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/index.js' . '?pv=' . $plugin_version ?>"></script>
</body>
</html>
