<?php
/**
 * Template Name: confirmation
 *
 *
 */
defined('ABSPATH') or die('No script kiddies please!');

global $apet_file_path;
$firebase_email_verification = false;

$data = get_plugin_data(APET_PLUGIN_PATH . "easy-traveling.php", false, false);
$plugin_version = $data['Version'];

if(isset($_GET['mode']) && isset($_GET['apiKey']) && isset($_GET['oobCode']) && isset($_GET['continueUrl']) ){
    $mode = $_GET['mode'];
    $api_key = $_GET['apiKey'];
    $action_code = $_GET['oobCode'];
    $content_url = $_GET['continueUrl'];
    $lang = isset($_GET['lang'])? $_GET['lang'] : 'en';

    if($mode === 'verifyEmail'){
        preg_match('/uid=(.*)&changeEmail=(.*)/', $content_url, $out);

        if(!empty($out)){
            $encrypted_uid = $out[1];
            $change_email = $out[2];
            $user_id = apet_any_to_dec($encrypted_uid);
            $firebase_email_verification = true;
            $success_title_arr = array(
                'hy' => 'Էլ. հասցեի հաստատում',
                'en' => 'Email confirmation',
                'ru' => 'Подтверждение эл. почты'
            );
            $success_message_arr = array(
                'hy' => 'Շնորհակալություն ձեր էլ. հասցեն հաստատելու համար: Այժմ կարող եք փակել այս էջը:',
                'en' => 'Thank you for confirming your email. You can now close this page.',
                'ru' => 'Благодарим вас за подтверждение вашей эл. почты. Вы можете закрыть эту страницу.'
            );

            $error_title_arr = array(
                'hy' => 'Հաստատում',
                'en' => 'Confirmation',
                'ru' => 'Подтверждение'
            );
            $error_message_arr = array(
                'hy' => 'Հաստատման հղումը անվավեր է:',
                'en' => 'Confirmation link is invalid.',
                'ru' => 'Cсылка подтверждения недействительна.'
            );
        }
    }

}

if(!$firebase_email_verification) {
    $user_id = isset($_GET['uid']) ? $_GET['uid'] : false;


    $ln = isset($_GET['ln']) ? $_GET['ln'] : 'en';
    $languages = array('hy', 'en', 'ru');
    $ln = in_array($ln, $languages) ? $ln : 'en';

    $user_id = apet_any_to_dec($user_id);
    $token = isset($_GET['token']) ? $_GET['token'] : false;
    $email_verification = isset($_GET['email-verification']) ? $_GET['email-verification'] : false;

    $password_updated = isset($_REQUEST['password']) && $_REQUEST['password'] == 'changed';
    $login = isset($_GET['login']) ? $_GET['login'] : false;

    if ($login === 'invalidkey' || $login === 'expiredkey') {
        $title_arr = array(
            'hy' => 'Հաստատում',
            'en' => 'Confirmation',
            'ru' => 'Подтверждение'
        );
        $message_arr = array(
            'hy' => 'Գաղտնաբառի վերականգնման հղումը այլևս վավեր չէ:',
            'en' => 'The password reset link you used is not valid anymore.',
            'ru' => 'Cсылка на сброс пароля больше не действительна.'
        );
        $title = $title_arr[$ln];
        $message = $message_arr[$ln];
    } else if ($password_updated) {
        $title_arr = array(
            'hy' => 'Հաստատում',
            'en' => 'Confirmation',
            'ru' => 'Подтверждение'
        );
        $message_arr = array(
            'hy' => 'Ձեր գաղտնաբառը հաջողությամբ փոխվեց:',
            'en' => 'Your password was successfully changed.',
            'ru' => 'Ваш пароль был успешно изменен.'
        );
        $title = $title_arr[$ln];
        $message = $message_arr[$ln];

    } else if ($email_verification === 'true') {
        $pending_email = get_user_meta($user_id, 'pending_email', true);
        if ($pending_email) {
            $check_pending = apet_change_app_user_email($user_id, $pending_email);
            if ($check_pending) {
                $title_arr = array(
                    'hy' => 'Էլ. հասցեի հաստատում',
                    'en' => 'Email confirmation',
                    'ru' => 'Подтверждение эл. почты'
                );
                $message_arr = array(
                    'hy' => 'Շնորհակալություն ձեր էլ. հասցեն հաստատելու համար: Այժմ կարող եք փակել այս էջը:',
                    'en' => 'Thank you for confirming your email. You can now close this page.',
                    'ru' => 'Благодарим вас за подтверждение вашей эл. почты. Вы можете закрыть эту страницу.'
                );
                $title = $title_arr[$ln];
                $message = $message_arr[$ln];

            } else {
                $title_arr = array(
                    'hy' => 'Էլ. հասցեի հաստատում',
                    'en' => 'Email confirmation',
                    'ru' => 'Подтверждение эл. почты'
                );
                $message_arr = array(
                    'hy' => 'Էլ. հասցեն արդեն գոյություն ունի',
                    'en' => 'Email already exist',
                    'ru' => 'Эл. почтa уже существует'
                );
                $title = $title_arr[$ln];
                $message = $message_arr[$ln];
            }
        } else {
            $title_arr = array(
                'hy' => 'Էլ. հասցեի հաստատում',
                'en' => 'Email confirmation',
                'ru' => 'Подтверждение эл. почты'
            );
            $message_arr = array(
                'hy' => 'Հաստատման հղումը անվավեր է:',
                'en' => 'Confirmation link is invalid.',
                'ru' => 'Cсылка подтверждения недействительна.'
            );
            $title = $title_arr[$ln];
            $message = $message_arr[$ln];
        }
    } else {
        $user_status = $user_id ? get_user_meta($user_id, 'user_status', true) : false;
        if ($user_status === "unconfirmed") {
            $user_token = get_user_meta($user_id, 'user_token', true);
            if ($user_token === $token) {
                apet_activate_app_user($user_id);
                $title_arr = array(
                    'hy' => 'Հաստատում',
                    'en' => 'Confirmation',
                    'ru' => 'Подтверждение'
                );
                $message_arr = array(
                    'hy' => 'Շնորհակալություն ձեր հաշիվը հաստատելու համար: Այժմ կարող եք փակել այս էջը:',
                    'en' => 'Thank you for confirming your account. You can now close this page.',
                    'ru' => 'Благодарим вас за подтверждение вашей учетной записи. Вы можете закрыть эту страницу.'
                );
                $title = $title_arr[$ln];
                $message = $message_arr[$ln];

            } else {
                $title_arr = array(
                    'hy' => 'Հաստատում',
                    'en' => 'Confirmation',
                    'ru' => 'Подтверждение'
                );
                $message_arr = array(
                    'hy' => 'Հաստատման հղումը անվավեր է:',
                    'en' => 'Confirmation link is invalid.',
                    'ru' => 'Cсылка подтверждения недействительна.'
                );
                $title = $title_arr[$ln];
                $message = $message_arr[$ln];

            }
        } else if ($user_status === "active") {
            $title_arr = array(
                'hy' => 'Հաստատում',
                'en' => 'Confirmation',
                'ru' => 'Подтверждение'
            );
            $message_arr = array(
                'hy' => 'Ձեր հաշիվն արդեն ակտիվ է:',
                'en' => 'Your account is already active.',
                'ru' => 'Ваша учетная запись уже активна.'
            );
            $title = $title_arr[$ln];
            $message = $message_arr[$ln];
        } else {
            $title_arr = array(
                'hy' => 'Հաստատում',
                'en' => 'Confirmation',
                'ru' => 'Подтверждение'
            );
            $message_arr = array(
                'hy' => 'Հաստատման հղումը անվավեր է:',
                'en' => 'Confirmation link is invalid.',
                'ru' => 'Cсылка подтверждения недействительна.'
            );
            $title = $title_arr[$ln];
            $message = $message_arr[$ln];
        }
    }
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php echo APET_GA; ?>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-32x32.png" sizes="32x32" />
    <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-180x180.png" />
    <meta name="msapplication-TileImage" content="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-270x270.png" />

    <title>EasyTraveling</title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/materialize.min.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/style.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />

</head>
<body id="body" class="white">

<?php if($firebase_email_verification): ?>
    <div class="loader-layer confirmation">
        <div class="loader-container">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-green-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="white-layer"></div>
    </div>
    <div id="main" class="row">
        <div id="confirmation"  class="confirmation-container main-page">
            <div  class="container">
                <div class="row">

                    <div class="col s12 ">
                        <div class="easytravel-logo"></div>
                        <h4 class="center-align title"></h4>
                        <div class="row">
                            <h5 class="center-align message"></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>
<?php if(!$firebase_email_verification):?>
    <div id="main" class="row">
        <div id="confirmation"  class="confirmation-container main-page">
            <div  class="container">
                <div class="row">

                    <div class="col s12 ">
                        <div class="easytravel-logo"></div>
                        <h4 class="center-align"><?php echo $title ?></h4>
                        <div class="row">
                            <h5 class="center-align"><?php echo $message ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php endif;?>
<script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/jquery-2.2.4.min.js' . '?pv=' . $plugin_version ?>"></script>
<script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/materialize.min.js' . '?pv=' . $plugin_version ?>"></script>
<?php if($firebase_email_verification):?>
    <script src="https://www.gstatic.com/firebasejs/5.5.6/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.6/firebase-auth.js"></script>
    <script>
        function showResult(resp,lang){
            var successTitles = <?php echo json_encode($success_title_arr)?>,
                successMessages = <?php echo json_encode($success_message_arr)?>,
                errorTitles = <?php echo json_encode($error_title_arr)?>,
                errorMessages = <?php echo json_encode($error_message_arr)?>;
            $('.loader-layer').remove();
            if(resp === 'success'){
                $('.title').html(successTitles[lang]);
                $('.message').html(successMessages[lang]);
            }else{
                $('.title').html(errorTitles[lang]);
                $('.message').html(errorMessages[lang]);
            }
        }
        function handleVerifyEmail(auth, actionCode, uid, lang) {

            auth.applyActionCode(actionCode).then(function(resp) {
                // Email address has been verified.
                sendAjax(uid,lang);
            }).catch(function(error) {
                showResult('error',lang)
            });
        }

        function sendAjax(uid,lang){
            var ajaxUrl = '/wp-admin/admin-ajax.php';
            var emailChanging = "<?php echo $change_email;?>";
            $.ajax({
                type: "POST",
                url: ajaxUrl,
                dataType: "json",
                data:{
                    action:'apet_activate_user',
                    uid: uid,
                    emailChanging:emailChanging
                },
                success:function(resp){
                    showResult('success',lang)
                },
                error:function(err){
                    showResult('error',lang)
                }
            })
        }

        $(document).ready(function() {
            // Get the action to complete.
            var mode = "<?php echo $mode; ?>";
            var actionCode = "<?php echo $action_code; ?>";
            var apiKey = "<?php echo $api_key; ?>";
            var continueUrl = "<?php echo $content_url; ?>";
            var lang = "<?php echo $lang;?>";
            var uid = <?php echo $user_id?>;


            var config = {
                'apiKey': apiKey  // This key could also be copied from the web
                                  // initialization snippet found in the Firebase console.
            };
            var app = firebase.initializeApp(config);
            var auth = app.auth();

//             Handle the user management action.
            switch (mode) {
                case 'resetPassword':
                    // Display reset password handler and UI.
                    handleResetPassword(auth, actionCode, continueUrl, lang);
                    break;
                case 'recoverEmail':
                    // Display email recovery handler and UI.
                    handleRecoverEmail(auth, actionCode, lang);
                    break;
                case 'verifyEmail':
                    // Display email verification handler and UI.
                    handleVerifyEmail(auth, actionCode, uid, lang);
                    break;
                default:
                // Error: invalid mode.
            }
        });
    </script>
<?php endif;?>
</body>
</html>


