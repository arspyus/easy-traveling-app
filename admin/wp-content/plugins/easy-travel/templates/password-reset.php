<?php
/*
  Template Name: Custom  Password Reset
 */

defined('ABSPATH') or die('No script kiddies please!');
global $apet_file_path;

$data = get_plugin_data( APET_PLUGIN_PATH . "easy-traveling.php", false, false );
$plugin_version = $data['Version'];

$firebase_reset = false;

if(isset($_GET['mode']) && isset($_GET['oobCode']) && isset($_GET['apiKey']) && isset($_GET['continueUrl']) ){
    $mode = $_GET['mode'];
    $api_key = $_GET['apiKey'];
    $action_code = $_GET['oobCode'];
    $lang = isset($_GET['lang']) ? $_GET['lang']: 'en';

    if($mode === 'resetPassword'){
        $content_url = $_GET['continueUrl'];

        $action ='rp';
        preg_match('/key=(.*)&login=(.*)&ln=(.*)/', $content_url, $output_array);

        if(!empty($output_array)){
            $action ='rp';
            $key = $output_array[1];
            $login = urldecode($output_array[2]);
            $ln = $output_array[3] ? $output_array[3]: 'en';
            $firebase_reset = true;
        }
    }
}else{
    $key = isset($_GET['key']) ? $_GET['key'] : '';
    $login = isset($_GET['login']) ? $_GET['login'] : '';
    $ln = isset($_GET['ln']) ? $_GET['ln'] : 'en';
    $languages = array('hy','en','ru');
    $ln = in_array($ln,$languages) ? $ln :'en';
}

$title = array(
    'hy' => 'Գաղտնաբառի վերականգնում',
    'en' => 'Reset Password',
    'ru' => 'Восстановление пароля'
);
$new_password = array(
    'hy' => 'Նոր գաղտնաբառ',
    'en' => 'New password',
    'ru' => 'Новый пароль'
);
$password_hint = array(
    'hy' => 'Օգտագործեք 6 կամ ավելի նիշ, տառերի և թվերի համադրությամբ:',
    'en' => 'Use 6 or more characters with a mix of letters & numbers.',
    'ru' => 'Используйте 6 или более символов с комбинацией букв и цифр.'
);
$button_text = array(
    'hy' => 'Վերականգնել',
    'en' => 'Reset Password',
    'ru' => 'Восстановить'
);
?>

    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>
    <head>
        <?php echo APET_GA; ?>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">

        <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-32x32.png" sizes="32x32" />
        <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-180x180.png" />
        <meta name="msapplication-TileImage" content="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-270x270.png" />

        <title>EasyTraveling</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/materialize.min.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/style.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />

    </head>
    <body id="body" class="white">
    <div id="main" class="row">
        <div id="reset" data-action="reset"  class=" reset-container main-page">
            <div  class="container">
                <div class="row">
                    <div class="col s12 ">
                        <div class="easytravel-logo"></div>
                        <h4 class="center-align"  ><?php echo $title[$ln] ?></h4>
                        <div class="row">
                            <div class="form-container">
                                <form name="resetpassform" id="resetpassform" action="<?php echo site_url('wp-login.php?action=resetpass'); ?>" method="post" autocomplete="off">
                                    <input type="hidden" name="rp_login" value="<?php echo esc_attr($login);  ?>"  />
                                    <input type="hidden" name="rp_key" value="<?php echo esc_attr($key); ?>" />
                                    <input type="hidden" name="language" value="<?php echo esc_attr($ln); ?>" />
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label for="reset-password"><?php echo $new_password[$ln]?></label>
                                            <input type="text" name="pass1" id="reset-password" class="input" value="<?php echo wp_generate_password(12, false); ?>" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <div> <?php echo $password_hint[$ln] ?></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <p class="center-align">
                                                <button type="submit" name="submit" id="resetpass-button" class="btn form-submit waves-effect light-green darken-1 waves-light"><?php echo $button_text[$ln] ?></button>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------ Loader --------->
    <div class="loader-layer">
        <div class="loader-container">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-green-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="white-layer"></div>
    </div>

    <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/jquery-2.2.4.min.js' . '?pv=' . $plugin_version ?>"></script>
    <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/materialize.min.js' . '?pv=' . $plugin_version ?>"></script>
    <?php if($firebase_reset):?>
        <script src="https://www.gstatic.com/firebasejs/5.5.6/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/5.5.6/firebase-auth.js"></script>
        <script>
            (function ($) {

                function handleResetPassword(auth, actionCode, continueUrl, lang) {

                    auth.verifyPasswordResetCode(actionCode).then(function(email) {
                        var accountEmail = email;

                        var newPassword =$('#reset-password').val();

                        auth.confirmPasswordReset(actionCode, newPassword).then(function(resp) {
                            $('#resetpassform').submit();
                        }).catch(function(error) {

                        });
                    }).catch(function(error) {

                    });
                }



                function checkPassword(password) {
                    if (password === '') {
                        return false;
                    }
                    var pattern = /^(?=.*[a-z])(?=.*[0-9])(?=.{6,})/;
                    return pattern.test(password);
                }

                var actionCode = "<?php echo $action_code; ?>";
                var apiKey = "<?php echo $api_key; ?>";
                var lang = "<?php echo $lang;?>";
                var continueUrl;
                var config = {
                    'apiKey': apiKey  // This key could also be copied from the web
                                      // initialization snippet found in the Firebase console.
                };
                var app = firebase.initializeApp(config);
                var auth = app.auth();

                $('#resetpass-button').click(function(){
                    if (!checkPassword($('#reset-password').val())) {
                        $(this).removeClass('valid').addClass('invalid');
                        Materialize.toast('Please fill required field', 3000, 'error-toast');
                        return false;
                    }else{
                        handleResetPassword(auth, actionCode, continueUrl, lang);
                    }
                });

                $('#reset-password').on('input', function () {
                    if (!checkPassword($(this).val())) {
                        $(this).removeClass('valid').addClass('invalid')

                    } else {
                        if ($(this).hasClass('invalid')) {
                            $(this).removeClass('invalid').addClass('valid');
                        }
                    }
                });

            })(jQuery)

        </script>
    <?php else:?>
        <script>
            (function ($) {

                function checkPassword(password) {
                    if (password === '') {
                        return false;
                    }
                    var pattern = /^(?=.*[a-z])(?=.*[0-9])(?=.{6,})/;
                    return pattern.test(password);
                }

                $('#resetpassform').on('submit', function (e) {
                    if (!checkPassword($('#reset-password').val())) {
                        $(this).removeClass('valid').addClass('invalid');
                        Materialize.toast('Please fill required field', 3000, 'error-toast');
                        return false;
                    }
                });

                $('#reset-password').on('input', function () {
                    if (!checkPassword($(this).val())) {
                        $(this).removeClass('valid').addClass('invalid')

                    } else {
                        if ($(this).hasClass('invalid')) {
                            $(this).removeClass('invalid').addClass('valid');
                        }
                    }
                });

            })(jQuery)

        </script>
    <?php endif;?>
    </body>
    </html>
