<?php
/**
 * Template Name: login-page
 *
 *
 */
defined('ABSPATH') or die('No script kiddies please!');
global $apet_file_path;
$data = get_plugin_data( APET_PLUGIN_PATH . "easy-traveling.php", false, false );
$plugin_version = $data['Version'];
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="login-page">
    <head>
        <?php echo APET_GA; ?>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">

        <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-32x32.png" sizes="32x32" />
        <link rel="icon" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-180x180.png" />
        <meta name="msapplication-TileImage" content="<?php echo APET_PLUGIN_URL; ?>assets/img/fav/fav-270x270.png" />

        <title>EasyTraveling</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/materialize.min.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo APET_PLUGIN_URL . 'assets/css/style.css' . '?pv=' . $plugin_version ?>" type="text/css" media="screen" />



    </head>
    <body id="body" class="white" >
        <div id="main">
            <div id="login" class="login-container active-login">
                <div class="wrapper">
                    <div  class="container">
                        <div class="row">
                            <div class="col s12 ">
                                <div class="easytravel-logo">
                                    <?php echo file_get_contents(APET_PLUGIN_PATH . 'assets/img/logo.svg'); ?>
                                </div>
                                <div class="row">

                                    <div class="form-container login-form-container">
                                        <form id="login-form" class="col s12 submitable-form">
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <input id="login-email" type="text" name="email-username" class="validate-email app-input">
                                                    <label for="login-email"  >Email</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <input id="login-password" type="password"  name="password" class="validate-password app-input">
                                                    <label  for="login-password">Password</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12">
                                                    <p class="center-align">
                                                        <button form="login-form"  class="btn form-submit waves-effect light-green darken-1 waves-light"  type="submit"  data-action="login">Login</button>
                                                    </p>
                                                    <p class="center-align">
                                                        <a href="javascript:void(0);" id="reset-password-btn" data-translation-key="forgot-password">Forgot Password?</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="form-container forgot-password-form-container">
                                        <form id="forgot-password-form" class="col s12 submitable-form" >
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <input id="forgot-email" type="email" name="email"  class="validate-email app-input">
                                                    <label for="forgot-email">Email</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12">
                                                    <p class="center-align">
                                                        <button data-action="reset-password" form="forgot-password-form" class="btn form-submit waves-effect light-green darken-1 waves-light" type="submit" >Reset</button>
                                                    </p>
                                                    <p class="center-align">
                                                        <a href="javascript:void(0);" id="reset-password-cancel">Cancel</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!------ Loader --------->
        <div class="loader-layer">
            <div class="loader-container">
                <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="white-layer"></div>
        </div>

        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/jquery-2.2.4.min.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/materialize.min.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/countries-list.js' . '?pv=' . $plugin_version ?>"></script>
        <script type="text/javascript" src="<?php echo APET_PLUGIN_URL . 'assets/js/index.js' . '?pv=' . $plugin_version ?>"></script>
    </body>
</html>
