<?php

/**
Plugin Name: Aparg EasyTraveling
Description: plugin
Version:     1.0.57
Author:      Aparg
Author URI:  https://aparg.com/
Text Domain: aparg-travel
Domain Path: /languages/
License:     GPL2

plugin

You should have received a copy of the GNU General Public License
along with this plugin. If not, see https://wordpress.org/about/gpl/.
 */


defined('ABSPATH') or die('No script kiddies please!');
global $apet_file_path,$serviceAccount,$firebase,$auth;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

$apet_file_path = __FILE__;
include_once 'config.php';
include_once __DIR__.'/../vendor/autoload.php';
include_once 'functions.php';
include_once 'push.php';
include_once 'API/special-offers.php';
include_once 'API/bonus.php';
include_once 'API/book.php';
include_once 'API/agency-offers.php';
include_once 'API/users.php';
include_once 'API/requests.php';
include_once 'API/tour-agencies.php';
include_once 'API/stats.php';


if(APET_DEBUG){
    $account_json = 'dev-service-account.json';
}else{
    $account_json = 'live-service-account.json';
}

$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/' . $account_json);

$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->create();

$auth = $firebase->getAuth();

add_filter("show_admin_bar", "__return_false");
add_filter('send_email_change_email', '__return_false' );

function easy_travel_setup() {
    include_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $wpdb->show_errors();

    /** Create Tour Agencies rate table */
    $table_name = $wpdb->prefix . "tour_agencies_rate";

    $sql = "CREATE TABLE $table_name (
            id bigint(20) NOT NULL AUTO_INCREMENT,
            tour_id bigint(20) NOT NULL,
            user_id bigint(20) NOT NULL,
            notes text,
            rate float(2,1),
            UNIQUE KEY `rate_once` (tour_id, user_id),
            UNIQUE KEY id (id)
          ) $charset_collate;";

    dbDelta($sql);
    /** Create Tour Agencies  table */
    $table_name = $wpdb->prefix . "tour_agencies";

    $sql = "CREATE TABLE $table_name (
            id bigint(20) NOT NULL AUTO_INCREMENT,
            tour_admin bigint(20) NOT NULL,
            country varchar(255),
            city varchar(255),
            address varchar(255),
            tour_agent varchar(255),
            phone varchar(255),
            email varchar(255),
            website varchar(255),
            coordinates varchar(255),
            average_rate  float(2,1) NOT NULL,
            voters_number bigint(20) NOT NULL,
            package_plan_name varchar(64),
            package_plan text,
            doc_list text,
            prefered_countries text,
            photo text,
            tour_creation_date datetime NOT NULL,
            status varchar(64),
            KEY tour_admin (tour_admin),
            UNIQUE KEY id (id)
          ) $charset_collate;";

    dbDelta($sql);
    /** Create Special Offers table */
    $table_name = $wpdb->prefix . "special_offers";

    $sql = "CREATE TABLE $table_name (
            offer_id bigint(20) NOT NULL AUTO_INCREMENT,
            offer_author bigint(20) NOT NULL,
            title varchar(255),
            description text,
            trip_start datetime NOT NULL,
            trip_end datetime NOT NULL,
            offer_creation_date datetime NOT NULL,
            offer_deadline datetime NOT NULL,
            price bigint(20),
            status varchar(255),
            photo text,
            KEY offer_author (offer_author),
            UNIQUE KEY offer_id (offer_id)
          ) $charset_collate;";

    dbDelta($sql);

    /** Create Requests table */
    $table_name = $wpdb->prefix . "requests";

    $sql = "CREATE TABLE $table_name (
            request_id bigint(20) NOT NULL AUTO_INCREMENT,
            request_author bigint(20) NOT NULL,
            country varchar(255),
            country_code varchar(2),
            city varchar(255),
            period_from datetime NOT NULL,
            period_to datetime NOT NULL,
            request_date datetime NOT NULL,
            cancel_date datetime NOT NULL,
            airticket varchar(64),
            nationality varchar(128),
            adults int(3),
            children int(3),
            infant int(3),
            nights varchar(64),
            desired_hotels text,
            rooms varchar(64),
            hotel_category varchar(64),
            price_from int(10),
            price_to int(10),
            payment_method varchar(255),
            board_type varchar(255),
            visa_support varchar(8),
            social_package varchar(64),
            notes text,
            extra text, 
            children_ages text, 
            chosen_tour varchar(255),
            status varchar(64),
            KEY request_author (request_author),
            UNIQUE KEY request_id (request_id)
          ) $charset_collate;";

    dbDelta($sql);

    /** Create Requests Status table */
    $table_name = $wpdb->prefix . "requests_statuses";

    $sql = "CREATE TABLE $table_name (
            status_id bigint(20) NOT NULL AUTO_INCREMENT,
            request_id bigint(20) NOT NULL,
            status varchar(64) NOT NULL,
            set_date datetime NOT NULL,
            UNIQUE KEY status_id (status_id)
          ) $charset_collate;";

    dbDelta($sql);

    /** Create Agency Offers table */
    $agency_offers_table = $wpdb->prefix . "agency_offers";

    $sql = "CREATE TABLE $agency_offers_table (
            offer_id bigint(20) NOT NULL AUTO_INCREMENT,
            offer_author bigint(20) NOT NULL,
            request_id bigint(20) NOT NULL,
            departure datetime,
            arrival datetime,
            place_id varchar(255),
            trans_type varchar(64),
            departure_departure_air_code varchar(64),
            departure_destination_arrival_air_code varchar(64),
            departure_airlines_iata varchar(64),
            departure_flight_type varchar(64),
            departure_departure_city varchar(255),
            departure_departure_airport varchar(255),
            departure_departure_air_company varchar (255),
            departure_departure_date_time datetime ,
            departure_destination_arrival_city varchar(255),
            departure_destination_arrival_airport varchar(255),
            departure_destination_arrival_date_time datetime,
            arrival_departure_air_code varchar(64),
            arrival_destination_arrival_air_code varchar(64),
            arrival_flight_type varchar(64),
            arrival_airlines_iata varchar(64),
            arrival_departure_airport varchar(255),
            arrival_departure_air_company varchar(255),
            arrival_departure_date_time datetime,
            arrival_destination_arrival_date_time datetime,
            arrival_destination_arrival_airport varchar(255),
            arrival_departure_city varchar(255),
            arrival_destination_arrival_city varchar(255),
            offer_creation_date datetime NOT NULL,
            offer_deadline datetime NOT NULL,
            doc_deadline datetime ,
            hotel_rating varchar(64),
            number_of_rooms int(4),
            hotel_checkin datetime NOT NULL,
            hotel_checkout datetime NOT NULL,
            flight_type varchar(255),
            nights int(4),
            board_type text, 
            notes text, 
            price bigint(20),
            hotel varchar(255) NOT NULL, 
            doc_list text,
            offer_status varchar(64),
            seen_status varchar(64) DEFAULT 'seen',
            PRIMARY KEY (offer_id)
           )$charset_collate;";

    dbDelta($sql);
    /** Create Agency Offers table */
    $books_table = $wpdb->prefix . "books";

    $sql = "CREATE TABLE $books_table (
            book_id bigint(20) NOT NULL AUTO_INCREMENT,
            request_id bigint(20) NOT NULL,
            offer_id bigint(20) NOT NULL,
            offer_author bigint(20) NOT NULL,
            user_id bigint(20) NOT NULL,
            book_date datetime NOT NULL,
            travelers_data text NOT NULL,
            book_status varchar(64),
            PRIMARY KEY (book_id)
            )$charset_collate;";

    dbDelta($sql);

    /** Create Airports data table */
    $airports_table = $wpdb->prefix . "airports";

    $sql = "CREATE TABLE $airports_table (
            id bigint(20) NOT NULL AUTO_INCREMENT,
            ident varchar(64),
            name varchar(255),
            latitude_deg varchar(255),
            longitude_deg varchar(255),
            continent varchar(64),
            iso_country varchar(64),
            iso_region varchar(64),
            municipality varchar(64),
            scheduled_service varchar(64),
            gps_code varchar(64),
            iata_code varchar(64),
            local_code varchar(64),
            PRIMARY KEY (id)
            )$charset_collate;";

    dbDelta($sql);

    /* =====add custom roles======== */
    add_role('app-user', 'App User', array('app_login' => true));
    add_role('tour-agent-admin', 'Tour Agent Admin', array('admin_login' => true));
    add_role('tour-super-admin', 'Tour Super Admin', array('create_tour' => true));

    if (get_page_by_title('action') === NULL) {

        $action_page = array();
        $action_page['post_title'] = 'action';
        $action_page['post_status'] = 'publish';
        $action_page['post_content'] = '';
        $action_page['post_type'] = 'page';
        $action_page_id = wp_insert_post($action_page);
    }

    if (get_page_by_title('login') === NULL) {

        $login_page = array();
        $login_page['post_title'] = 'login';
        $login_page['post_status'] = 'publish';
        $login_page['post_content'] = '';
        $login_page['post_type'] = 'page';
        $login_page_id = wp_insert_post($login_page);
    }

    if (get_page_by_title('dashboard') === NULL) {
        $dashboard_page = array();
        $dashboard_page['post_title'] = 'dashboard';
        $dashboard_page['post_status'] = 'publish';
        $dashboard_page['post_content'] = '';
        $dashboard_page['post_type'] = 'page';
        $dashboard_page_id = wp_insert_post($dashboard_page);
    }
    if (get_page_by_title('confirmation') === NULL) {
        $dashboard_page = array();
        $dashboard_page['post_title'] = 'confirmation';
        $dashboard_page['post_status'] = 'publish';
        $dashboard_page['post_content'] = '';
        $dashboard_page['post_type'] = 'page';
        $dashboard_page_id = wp_insert_post($dashboard_page);
    }
    if (get_page_by_title('reset') === NULL) {
        $dashboard_page = array();
        $dashboard_page['post_title'] = 'reset';
        $dashboard_page['post_status'] = 'publish';
        $dashboard_page['post_content'] = '';
        $dashboard_page['post_type'] = 'page';
        $dashboard_page_id = wp_insert_post($dashboard_page);
    }
    if (get_page_by_title('bookpics') === NULL) {
        $dashboard_page = array();
        $dashboard_page['post_title'] = 'bookpics';
        $dashboard_page['post_status'] = 'publish';
        $dashboard_page['post_content'] = '';
        $dashboard_page['post_type'] = 'page';
        $dashboard_page_id = wp_insert_post($dashboard_page);
    }
}

register_activation_hook(__FILE__, 'easy_travel_setup');