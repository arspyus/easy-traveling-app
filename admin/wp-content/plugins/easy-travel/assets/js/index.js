(function ($) {
    var app = {
        bindEvents: function (_self) {
            _self.materialazeMethods(_self);
            _self.loginEvents(_self);
            _self.generalEvents(_self);
            _self.validateEvents();
            if ($('#main').attr('data-user-id')) {
                _self.loggedIn(_self);
            }
        },
        formSubmit: function (data, type, _self) {
            switch (type) {
                case 'login':
                    _self.loginSubmit(data, _self);
                    break;
                case 'special-offers':
                    _self.addSpecOfferSubmit(data, _self);
                    break;
                case 'edit-spec-offer':
                    _self.editSpecOfferSubmit(data, _self);
                    break;
                case 'make-offer':
                    _self.makeOfferSubmit(data, _self);
                    break;
                case 'edit-offer':
                    data['action'] = 'apet_edit_request_offer';
                    _self.makeOfferSubmit(data, _self);
                    break;
                case 'create-agency':
                    _self.createAgencySubmit(data, _self);
                    break;
                case 'settings':
                    _self.settingsSubmit(data, _self);
                    break;
                case 'change-password':
                    _self.changePassword(data, _self);
                    break;
                case 'bonus-details':
                    _self.checkBonusDetails(data);
                    break;
                case 'add-user-bonus':
                    _self.addUserBonus(data);
                    break;
                case 'reset-password':
                    _self.resetPassword(data, _self);
                    break;
                case 'requests-filter':
                    _self.filterRequests(data);
                    break;
                case 'app-users-filter':
                    _self.filterAppUsers(data);
                    break;
                case 'tour-agencies-filter':
                    _self.filterTourAgencies(data);
                    break;
                case 'special-offers-filter':
                    _self.filterSpecOffers(data);
                    break;
                case 'stats-filter':
                    _self.filterStats(data);
                    break;
            }

        },
        generalEvents: function (_self) {

            $(document)
                .on('submit', '.submitable-form', function (e) {
                    e.preventDefault();
                    var inputsValue = {};
                    var scope = $(this);
                    var type = scope.find('[type="submit"]').attr('data-action');
                    scope.find('input.app-input').each(function () {
                        if ($(this).is(':disabled')) {
                            return true;
                        }
                        var inputType = $(this).attr('type');
                        var value = '';
                        if (inputsValue[$(this).attr('name')] !== "") {
                            if (inputType === 'radio') {
                                if ($(this).is(":checked")) {
                                    value = $(this).val();
                                } else {
                                    return true;
                                }
                            } else {
                                if((type === 'make-offer' || type === 'edit-offer') && $(this).hasClass('city-autocomplete')){
                                    value = $(this).attr('data-value');
                                }else{
                                    value = $(this).val();
                                }
                            }
                            inputsValue[$(this).attr('name')] = value;
                        }
                    });
                    scope.find('select.app-input').each(function () {
                        if ($(this).is(':disabled')) {
                            return true;
                        }
                        var value = $(this).val();
                        inputsValue[$(this).attr('name')] = value;
                    });
                    scope.find('textarea.app-input').each(function () {
                        if ($(this).is(':disabled')) {
                            return true;
                        }
                        inputsValue[$(this).attr('name')] = $(this).val();
                    });
                    scope.find('select.app-input[name]').each(function () {
                        if ($(this).is(':disabled')) {
                            return true;
                        }
                        var value = '';
                        if ($(this).attr('multiple')) {
                            value = JSON.stringify($(this).val());
                        } else {
                            value = $(this).val();
                        }
                        inputsValue[$(this).attr('name')] = value;
                    });
                    if (_self.validateInputs(scope)) {
                        _self.formSubmit(inputsValue, type, _self);
                    } else {
                        _self.errorToast('Please fill required fields.');
                    }
                    return false;
                })
                .on('input change', 'form input, form textarea ,form select', function () {
                    _self.validateInputs($(this));
                })
                .on('click','.modal-close',function(){
                    $('.pac-container').remove();
                });

            $('body').on('click', '.page-redirect', function () {
                var pageId = $(this).attr('data-destination');
                _self.goToPage(pageId);
            });

            $(window).on('hashchange', function (e) {
                e.preventDefault();
                var pageId = _self.helpers.locationHashToPageId();
                if (pageId) {
                    _self.goToPage(pageId);
                }
                return false;
            }).on('popstate',function(e){
                var url = new URL(location.href);
                var requestId = url.searchParams.get('request_id');
                var pageId = _self.helpers.locationHashToPageId();
                if (!requestId) {
                    if (pageId === _self.currentPage  &&  pageId === 'requests') {
                        _self.resetFilter();
                    }else{
                        _self.goToPage(pageId);
                    }
                }else{
                    if(pageId === 'requests'){
                        _self.setFilter({id:requestId},false);
                        $('#requests-filter-form').submit();
                    }else{
                        _self.goToPage(pageId);
                    }
                }
            });

            $('.page .wrapper').on('scroll',_self.pageScroll);

        },
        pageScroll: function(e){

            if( $('html').hasClass('loading-start')){
                return true;
            }

            var _self = app;
            clearTimeout(_self.clearScrollTimeOut);
            var elem = $(e.currentTarget);
            _self.clearScrollTimeOut = setTimeout(function () {
                if (elem.scrollTop() + elem.innerHeight() + 100 >= elem[0].scrollHeight) {
                    switch ($('.active-page').attr('id')) {
                        case 'requests':
                            _self.getRequests();
                            break;
                        case 'app-users':
                            _self.getAppUsers();
                            break;
                        case 'tour-agencies':
                            _self.getTourAgencies();
                            break;
                        case 'special-offers':
                            _self.getSpecOffers();
                            break;
                    }
                }
            }, 100);

        },
        addSpecOfferSubmit: function (inputsValue, _self) {
            _self.offerUpload._opts.data = {
                action: 'apet_create_special_offer',
                'tour-id': _self.tourId,
                'user-id': _self.userId
            }
            _self.uploadSpecOfferData.action = 'upload';
            _self.offerUpload.submit();
        },
        uploadSpecOfferOnComplete: function (response) {
            var _self = this;
            if (response.success === "true") {

                var offerData = response.data['offer-data'];
                if (_self.specOffers) {
                    _self.specOffers.push(offerData)
                } else {
                    _self.specOffers = [offerData];
                }

                //$('#make-spec-offer-form .photo-path').removeClass('img-changed');

                if(typeof _self.getSpecOffers.filterOptions == 'undefined' ||  _self.getSpecOffers.filterOptions.status == 'active'){

                    if ($('#special-offers .spec-offers-table').length === 0) {
                        $('.special-offers-container').append('<ul class="collapsible spec-offers-table" data-collapsible="accordion"></ul>');
                    }
                    $('#special-offers .spec-offers-table').prepend(_self.addSpecOfferBlock(offerData, _self));
                    _self.getSpecOffers.offset++;
                }

                $('.collapsible').collapsible();
                $('.special-offers-container .materialboxed').materialbox();
                $('#popup-modal').modal('close');

                _self.packageDetails.spec_offers_left = response.data['offers-left'];

                _self.updateSpecOffersCounter(_self.packageDetails.spec_offers_left);
                if(_self.packageDetails.spec_offers_left !== -1){
                    _self.successToast('Success. Offers left: ' + _self.packageDetails.spec_offers_left);
                }else{
                    _self.successToast('Success');
                }

            } else {

                _self.errorToast('Could not create offer.');
                if (response.errorMessage == 'img_upload_failed') {
                    $('#make-spec-offer-form  .special-offer-img').attr('src', apetTempLogoUrl).addClass('placeholder-image');
                    $('#make-spec-offer-form  .file-path').val('')
                } else if (response.errorMessage === 'unauthorize_access') {
                    _self.logout(_self);
                } else if (response.errorMessage == 'max_limit_reached') {
                    _self.errorToast('You have exceeded the special offers limit.');
                    _self.packageDetails.spec_offers_left = 0;
                    _self.updateSpecOffersCounter(_self.packageDetails.spec_offers_left);
                    return false;
                }
            }

        },
        editSpecOfferOnComplete: function (response) {
            var _self = this;
            if (response.success === "true") {
                //$('#make-spec-offer-form .photo-path').removeClass('img-changed');
                var offerId = response['offer-data']['offer_id'];
                for (var i = 0; i < _self.specOffers.length; i++) {
                    if (_self.specOffers[i]['offer_id'] == offerId) {
                        _self.specOffers[i] = response['offer-data'];
                        var offerData = _self.specOffers[i];
                        break;
                    }
                }

                if( _self.getSpecOffers.filterOptions &&  _self.getSpecOffers.filterOptions.status == 'expired'   && offerData['days_left'] > -1){
                    $('#special-offers .spec-offers-table .single-offer[data-offer-id="' + offerId + '"]').remove();
                    _self.getSpecOffers.offset--;
                }else{
                    var specialOfferHtml = _self.addSpecOfferBlock(offerData, _self);
                    $('#special-offers .spec-offers-table .single-offer[data-offer-id="' + offerId + '"]').replaceWith(specialOfferHtml);
                }
                _self.successToast('Changes saved.');

                $('.collapsible').collapsible();
                $('.special-offers-container .materialboxed').materialbox();
                $('#popup-modal').modal('close');
            } else {
                _self.errorToast('Something went wrong.');
                if (response.errorMessage == 'img_upload_failed') {

                    var offerId = $('#popup-modal').data('offer-id');
                    for (var i = 0; i < _self.specOffers.length; i++) {
                        if (_self.specOffers[i]['offer_id'] == offerId) {
                            var offerData = _self.specOffers[i];
                            break;
                        }
                    }
                    var index = offerData['photo'].lastIndexOf("/") + 1;
                    var photoName = offerData['photo'].substr(index);
                    $('#make-spec-offer-form .photo-path').val(photoName);
                    $('#make-spec-offer-form .special-offer-img').attr('src', offerData['photo']);

                } else if (response.errorMessage === 'unauthorize_access') {
                    _self.logout(_self);
                }
            }
        },
        uploadSpecOfferData: function (_self) {

            _self.offerUpload = new ss.SimpleUpload({
                button: $('.spec-offer-img-btn').get(0), // file upload button
                url: _self.ajaxUrl,
                name: 'file',
                overrideSubmit: false,
                form: 'make-spec-offer-form',
                responseType: 'json',
                allowedExtensions: ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG'],
                maxSize: 2048, // kilobytes
                onChange: function (filename, extension, uploadBtn, fileSize, file) {
                    if (_self.validations.allowedFileSize(fileSize) == false) {
                        _self.errorToast('Max file size is 2MB.');
                        return false;
                    }

                    var allowedType = ['jpg', 'jpeg', 'png'];
                    var imageType = extension.toLowerCase();;
                    if (($.inArray(imageType, allowedType) > -1)) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#make-spec-offer-form .special-offer-img').attr('src', e.target.result);
                            $('#make-spec-offer-form  .special-offer-img.placeholder-image').removeClass('placeholder-image');
                            $('#make-spec-offer-form .photo-path').val(filename).removeClass('invalid');
                            $('#make-spec-offer-form .photo-path').addClass('img-changed');
                        };
                        reader.readAsDataURL(file);
                    } else {
                        _self.errorToast('Allowed types jpeg or png.');
                    }
                },
                onSubmit: function (filename, extension) {
                    $('.loader-layer').show();
                },
                onComplete: function (filename, response) {
                    $('.loader-layer').hide();
                    switch (_self.uploadSpecOfferData.action) {
                        case  'upload':
                            _self.uploadSpecOfferOnComplete(response);
                            break;
                        case  'edit':
                            _self.editSpecOfferOnComplete(response);
                            break;
                    }
                    $('#make-spec-offer-form .photo-path').removeClass('img-changed');

                }, onError: function (response) {
                    $('#make-spec-offer-form .photo-path').removeClass('img-changed');
                    $('.loader-layer').hide();
                    _self.errorToast('Something went wrong.');
                }
            });
        },
        specOfferModal: function (_self) {

            var modalHtml = '<form class="submitable-form" id="make-spec-offer-form" autocomplete="off">\
                                    <div class="modal-content">\
                                        <div>\
                                            <span class="spec-offers-count-info  ' + (_self.packageDetails.spec_offers_left === 0 ? 'red-text text-lighten-1' : 'light-green-text text-darken-1') + '">Offers left: <span class="spec-offers-count">' + _self.packageDetails.spec_offers_left + '</span></span>\
                                            <h5 class="make-offer-heading">Make Special Offer </h5>\
                                            <div class="clear-float"></div>\
                                    <div class="row">\
                                        <div class="input-field col s12">\
                                            <input id="spec-offer-title" name="title" type="text" data-min-length="1" data-max-length="35" class="validate validate-length app-input">\
                                            <label for="spec-offer-title">Title (Max 35 characters)</label>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="input-field col s12">\
                                            <input id="spec-offer-price" name="price" type="text" class="validate-required  validate-positive-int app-input">\
                                            <label for="spec-offer-price">Price (AMD)</label>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="input-field col s12">\
                                            <input name="trip_start" type="text" id="spec-offer-from" class="datepicker validate-required validate-date app-input">\
                                            <label for="spec-offer-from">Departure</label>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="input-field col s12">\
                                            <input name="trip_end" type="text" id="spec-offer-to" data-date-compare="spec-offer-from" class="datepicker validate-required validate-date app-input">\
                                            <label for="spec-offer-to">Arrival</label>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="input-field col s12">\
                                            <input name="offer_deadline" type="text" id="spec-offer-deadline" class="deadline-datepicker validate-required app-input">\
                                            <label for="spec-offer-deadline">Offer Deadline</label>\
                                        </div>\
                                    </div>\
                                    <div class="row">\
                                        <div class="input-field col s12">\
                                            <textarea id="spec-offer-description" name="description" class="materialize-textarea validate validate-required app-input"></textarea>\
                                            <label for="spec-offer-description">Description</label>\
                                        </div>\
                                    </div>\
                                    <div class="row spec-offer-img-row">\
                                         <div class="file-field input-field col s12">\
                                            <input disabled  class="photo-path file-path validate-required" type="text">\
                                            <div class="col s12 file-field-container">\
                                                <div class="file-filed-button-container ">\
                                                    <div class="spec-offer-img-btn darken-1 light-green waves-effect waves-light btn">\
                                                        <span>Photo</span>\
                                                        <input class="tour-img-file" type="file">\
                                                    </div>\
                                                </div>\
                                                <div class="file-field-image-container right">\
                                                    <img class="special-offer-img materialboxed placeholder-image"  src="' + apetTempLogoUrl + '"/>\
                                                    <span class="extra-notes">For best experience use landscape images.</span>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="modal-footer">\
                                <a href="javascript:void(0);" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>\
                                <button class="waves-effect modal-action-btn make-offer-btn waves-light darken-1 light-green btn" type="submit" data-action="special-offers">Make Offer</button>\
                            </div>\
                        </form>';
            _self.createModal(_self, modalHtml, 'special-offers');
            if(_self.packageDetails.spec_offers_left === -1){
                $('#make-spec-offer-form .spec-offers-count-info').hide();
            }else{
                $('#make-spec-offer-form .spec-offers-count-info').show();
            }
            //========== Initilizing MaterializeCSS Method========
            var formatedCountriesList = {};
            $.each(countriesList, function (index, value) {
                formatedCountriesList[index] = null;
            });

            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            // $('select').formSelect();
            $('.collapsible').collapsible();
            $('.datepicker').datepicker({
                // selectMonths: true, // Creates a dropdown to control month
                yearRange: 2, // Creates a dropdown of 15 years to control year,
                container: 'body',
                firstDay: 1,
                showClearBtn:true,
                minDate: new Date(),
                onClose: function () {
                    var _this = $($(this)[0].$el);
                    var _compareEl;

                    var dataCompare = _this.attr('data-date-compare');
                    var id = _this.attr('id');

                    if (dataCompare) {
                        _compareEl = $('#' + dataCompare);
                        if (_self.validations['compareDates'](_compareEl.val(), _this.val())) {
                            _this.removeClass('valid').addClass('invalid');
                            invalid = true;
                        } else {
                            _this.removeClass('invalid').addClass('valid');
                        }
                    } else if ($('[data-date-compare="' + id + '"]').length) {
                        _compareEl = $('[data-date-compare="' + id + '"]');
                        if (_self.validations['compareDates'](_this.val(), _compareEl.val())) {
                            _compareEl.removeClass('valid').addClass('invalid');
                            invalid = true;
                        } else {
                            _compareEl.removeClass('invalid').addClass('valid');
                        }
                    }
                },
                format: 'yyyy-mm-dd',
                closeOnSelect: false // Close upon selecting a date,
            });
            $('.deadline-datepicker').datepicker({
                selectMonths: false, // Creates a dropdown to control month
                yearRange: 0, // Creates a dropdown of 15 years to control year,
                firstDay: 1,
                showDaysInNextAndPreviousMonths:true,
                showClearBtn:true,
                minDate: date,
                maxDate: lastDay,
                container: 'body',
                format: 'yyyy-mm-dd',
                closeOnSelect: false // Close upon selecting a date,
            });
            $('.special-offer-img.materialboxed').materialbox();

        },
        specOffersEvents: function (_self) {


            /* $('#settings-form input,#settings-form select,#settings-form textarea').on('change', function () {
             _self.settingPageEvents.inputChanged = true;
             });*/

            $('#special-offers')
                .on('click', '.spec-offer-edit', function (e) {
                    e.stopPropagation();
                    var offerId = $(this).attr('data-offer-id');
                    for (var i = 0; i < _self.specOffers.length; i++) {
                        if (_self.specOffers[i]['offer_id'] == offerId) {
                            var offerData = _self.specOffers[i];
                            break;
                        }
                    }
                    _self.specOfferModal(_self);
                    _self.editSpecOfferFillds(offerData, _self);
                    M.updateTextFields()
                    $('#popup-modal .make-offer-btn').html('Save Changes').attr('data-action', 'edit-spec-offer');
                    $('#popup-modal').data('offer-id', offerId);
                    $('#popup-modal').modal('open');
                })
                .on('click', '.cancel-spec-offer', function (e) {
                    e.preventDefault();
                    var singleOffer = $(this).closest('.single-offer');
                    var offerId = $(this).attr('data-offer-id');
                    $('#cancel-spec-offer-modal').data({'single-offer': singleOffer, 'offer-id': offerId});
                    $('#cancel-spec-offer-modal').modal('open');
                });
            $('#cancel-spec-offer-modal .yes').on('click', function () {
                var data = {
                    'offer-id': $('#cancel-spec-offer-modal').data('offer-id'),
                    'single-offer': $('#cancel-spec-offer-modal').data('single-offer'),
                    'user-id': _self.userId
                };
                _self.cancelSpecOffer(data);
            });

            $('#special-offers select').formSelect();
            $('#special-offers .special-offers-load-more').on('click',function(){
                if( $('html').hasClass('loading-start')){
                    return true;
                }
                _self.getSpecOffers();
            });
            $('.add-special-offer').on('click', function (e) {
                e.preventDefault();
                var offersLeft = parseInt(_self.packageDetails.spec_offers_left);
                if(offersLeft === 0){
                    $(this).removeClass('modal-trigger');
                    $('#upgrade-plan-modal').modal('open');
                }else{
                    _self.specOfferModal(_self);
                }
            });
            $('#reset-special-offers-filter').on('click',function(){
                _self.resetSpecOffersFilter();
            });
        },
        editSpecOfferFillds: function (offerData, _self) {

            $('#spec-offer-title').val(offerData['title']).attr('data-value', offerData['desc']);
            $('#spec-offer-description').val(offerData['description']).attr('data-value', offerData['description']);
            $('#spec-offer-city').val(offerData['city']).attr('data-value', offerData['city']);
            $('#spec-offer-price').val(offerData['price']).attr('data-value', offerData['price']);
            $('#spec-offer-from').val(offerData['trip_start']).attr('data-value', offerData['trip_start']);
            $('#spec-offer-to').val(offerData['trip_end']).attr('data-value', offerData['trip_end']);
            $('#spec-offer-deadline').val(offerData['offer_deadline']).attr('data-value', offerData['offer_deadline']);
            //$('#make-spec-offer-form .offer-limit').val(_self.getSpecOffers.offerLimit).attr('data-value', _self.getSpecOffers.offerLimit);
            var index = offerData['photo'].lastIndexOf("/") + 1;
            var photoName = offerData['photo'].substr(index);
            $('#make-spec-offer-form .photo-path').val(photoName);
            $('#make-spec-offer-form .special-offer-img').attr('src', offerData['photo']);
        },
        editSpecOfferSubmit: function (inputsData, _self) {
            var editedOffer = {};
            for (var input in inputsData) {
                if (inputsData.hasOwnProperty(input)) {
                    if ($('#popup-modal .app-input[name="' + input + '"]').val() !== $('#popup-modal .app-input[name="' + input + '"]').attr('data-value') && $('#popup-modal .app-input[name="' + input + '"]').val() !== "") {
                        editedOffer[input] = inputsData[input]
                    }

                }
            }
            var userId = _self.userId;
            var offerId = $('#popup-modal').data('offer-id');
            var ajaxData = {
                'edited-data': JSON.stringify(editedOffer),
                'offer-data': JSON.stringify(inputsData),
                'action': 'apet_edit_special_offer',
                'user-id': userId,
                'offer-id': offerId
            };
            if ($('#make-spec-offer-form .photo-path').hasClass('img-changed')) {
                _self.offerUpload._opts.data = ajaxData;
                _self.uploadSpecOfferData.action = 'edit';
                _self.offerUpload.submit();
                return;
            }

            if (!$.isEmptyObject(editedOffer)) {
                _self.sendAjax({
                    data: ajaxData,
                    success: function (response) {
                        _self.editSpecOfferOnComplete(response);
                    },
                    error: function (error) {
                        _self.errorToast('Something went wrong.');
                    },
                    complete: function () {
                        $('#popup-modal').modal('close');
                    }, loader: true
                })

            } else {
                $('#popup-modal').modal('close');
            }
        },
        addSpecOfferBlock: function (inputsValue, _self) {
            $('.offer-empty-msg').hide();
            deleteBtn = "";
            if (_self.tourId !== 'all' && inputsValue["status"] != 'canceled') {
                var deleteBtn = '<tr class="tr-no-border">' +
                    '<th colspan="2" style="text-align:right"><a data-offer-id="' + inputsValue["offer_id"] + '" class="cancel-spec-offer btn-floating waves-effect waves-light  btn red lighten-1" ><i class="material-icons">delete_forever</i></a></th>' +
                    ' </tr>';
            }

            if (inputsValue.editable_time && parseInt(inputsValue.editable_time) > 0) {
                if (_self.specialOffersTimers[inputsValue['offer_id']]) {
                    clearTimeout(_self.specialOffersTimers[inputsValue['offer_id']]);
                }
                _self.specialOffersTimers[inputsValue['offer_id']] = setTimeout(function () {
                    $('.single-offer[data-offer-id="' + inputsValue['offer_id'] + '"] .spec-offer-edit').hide();
                }, (parseInt(inputsValue.editable_time)));
            }

            var tourAgencyRow = (_self.role === 'tour-super-admin') ? '<th>Tour agency</th><td class="capitalize">' + inputsValue["tour_agent"] + '</td></tr>' : '';
            var blockHtml = '<li data-offer-id="' + inputsValue["offer_id"] + '" class="single-offer">\
                                <div class="collapsible-header">\
                                    <table>\
                                        <tbody>\
                                            <tr>\
                                                <th class="capitalize">' + inputsValue["title"] + '</th>\
                                                <th class="capitalize"></th>\
                                                <th>' + inputsValue["price"] + ' AMD</th>'
                + (function (daysLeft) {

                    if (inputsValue["status"] === 'canceled') {
                        return '<th class="red-text text-lighten-1 capitalize"><span class="new badge lighten-1 red left" data-badge-caption="Deleted"></span></th>';
                    }else if (daysLeft == -1) {
                        return '<th class="red-text text-lighten-1 capitalize"><span class="new badge lighten-1 red left" data-badge-caption="Expired"></span></th>';
                    }else if (daysLeft == 0) {
                        daysLeft = 1;
                    }

                    return '<th class="light-green-text text-darken-1 capitalize">Valid Days: ' + daysLeft + '</th>';
                })(inputsValue['days_left']) +
                '<td>'
                + (inputsValue['editable'] === 'true' && (typeof _self.getSpecOffers.filterOptions  == 'undefined' || _self.getSpecOffers.filterOptions.status != 'deleted'  )  ? '<a data-offer-id="' + inputsValue["offer_id"] + '"  class="spec-offer-edit light-green-text text-darken-1" ><i class="material-icons">&#xE254;</i></a>' : '') +
                '<a class="collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a>\
                                    </td>\
                                </tr>\
                            </tbody>\
                        </table>\
                        </div>\
                        <div class="collapsible-body grey lighten-3">\
                            <table class="bordered single-spec-offer">\
                                <tbody>\
                                    <tr>' +
                tourAgencyRow +
                '<th>Title</th>\
                <td class="capitalize">' + inputsValue["title"] + '</td>\
                                        </tr>\
                                        <tr>\
                                            <th>Price</th>\
                                            <td>' + inputsValue["price"] + ' AMD</td>\
                                        </tr>\
                                        <tr>\
                                            <th>Departure</th>\
                                            <td>' + inputsValue["trip_start"] + '</td>\
                                        </tr>\
                                        <tr>\
                                            <th>Arrival</th>\
                                            <td>' + inputsValue["trip_end"] + '</td>\
                                        </tr>\
                                        <tr>\
                                            <th>Valid Until</th>\
                                            <td>' + inputsValue["offer_deadline"] + '</td>\
                                        </tr>\
                                        <tr>\
                                            <th>Description</th>\
                                            <td>' + (inputsValue["description"] ? _self.helpers.nl2br(_self.helpers.urlify(inputsValue["description"])) : 'Not Specified') + '</td>\
                                        </tr>\
                                        <tr>\
                                            <th>Photo</th>\
                                            <td class="table-photo-container">\
                                                <div class="photo-frame">\
                                                    <img src="' + inputsValue["photo"] + '" class="spec-offer-photo materialboxed right" />\
                                                </div>\
                                            </td>\
                                        </tr>'
                + deleteBtn +
                '</tbody>\
                            </table>\
                        </div>\
                    </li>';
            return blockHtml;
        },
        cancelSpecOffer: function (cancelData) {
            var _self = this;
            _self.sendAjax({
                data: {
                    'action': 'apet_cancel_special_offer',
                    'offer-id': cancelData['offer-id'],
                    'user-id': cancelData['user-id']

                },
                success: function (response) {
                    if (response.success === "true") {

                        if(typeof _self.getSpecOffers.filterOptions == 'undefined' || _self.getSpecOffers.filterOptions.status != 'deleted') {
                            cancelData['single-offer'].remove();
                            _self.getSpecOffers.offset--;
                            if ($('#special-offers .spec-offers-table li').length === 0) {
                                $('#special-offers .spec-offers-table').remove();
                                $('.offer-empty-msg').show();
                            }
                        }
                        _self.successToast('Special offer deleted.');
                    } else {
                        _self.errorToast('Something went wrong.');
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }, loader: true
            })


        },
        getSpecOffers: function (firstLoad) {
            var _self = this;
            firstLoad = (typeof firstLoad === 'undefined') ? false : firstLoad;

            if(firstLoad){
                delete  _self.getSpecOffers.filterOptions;
                delete _self.getSpecOffers.offset;
                $('#special-offers-filter-form')[0].reset();
                M.updateTextFields()
            }

            var loader = false;
            if(typeof _self.getSpecOffers.offset === 'undefined'){
                _self.getSpecOffers.offset = 0;
                loader = true;
            }

            $('html').addClass('loading-start');

            var data = {
                'action': 'apet_get_tour_special_offers',
                'user-id': _self.userId,
                'tour-id': _self.tourId,
                'limit':_self.limit,
                'offset':_self.getSpecOffers.offset,
                'filter':JSON.stringify({'status':'active'})
            };

            if(_self.getSpecOffers.filterOptions){
                data['filter'] = JSON.stringify(_self.getSpecOffers.filterOptions);
            }


            _self.sendAjax({
                data: data,
                beforeSend:function(){
                    if( _self.getSpecOffers.offset === 0){
                        $('.special-offers-container').empty();
                        $('.offer-empty-msg').show();
                    }
                },
                complete: function(){
                    $('html').removeClass('loading-start')
                },
                success: function (response) {
                    if (response.success === "true") {
                        _self.specOffers = response.data.specOffers;
                        //_self.getSpecOffers.offerLimit = response.data['offers-limit'];
                        _self.packageDetails.spec_offers_Left = response.data['offers-left'];

                        _self.drawSpecOffers(_self, response.data.specOffers);
                        _self.getSpecOffers.offset += response.data.specOffers.length;
                    } else {
                        if (response.errorMessage === 'failed_to_fetch') {
                            $('.special-offers-container').empty();
                            $('.offer-empty-msg').show();
                        }
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }, loader: loader
            });
        },
        drawSpecOffers: function (_self, specOffers) {

            /* update special offers count and disable if needed for tour agencies */
            var offersLeft = _self.packageDetails.spec_offers_left;

            _self.updateSpecOffersCounter(offersLeft);
            if(specOffers.length == 0){
                return false;
            }

            if(!$('.special-offers-container .spec-offers-table').length){
                $('.special-offers-container').append('<ul class="collapsible spec-offers-table" data-collapsible="accordion"></ul>');
            }

            var specOfferHtml = '';
            $.each(specOffers, function (key, offerData) {
                specOfferHtml += _self.addSpecOfferBlock(offerData, _self);
            });

            $('.special-offers-container .spec-offers-table').append(specOfferHtml);

            $('.collapsible').collapsible();
            $('.special-offers-container .materialboxed').materialbox();
        },
        requestsEvents: function (_self) {
            _self.getRequest.requestLoaded = {};
            _self.requestsStorage = {};
            _self.offersStorage = {};
            var clearScrollTimeOut;
            $('#main').on('keyup', '#make-request-offer-form #request-offer-nights', function () {

                clearTimeout(clearScrollTimeOut);
                var that = this;
                clearScrollTimeOut = setTimeout(function () {
                    var days = parseInt($(that).val()) + 1;
                    var label = !isNaN(days) ? 'Nights <span class="request-days grey-text text-darken-3">(Days - ' + days + ')</span>' : 'Nights';
                    $(that).siblings('label').html(label);
                }, 100);
            });
            $('#requests')
                .on('click', '.single-request', function () {
                    var requestId = $(this).attr('data-requst-id');

                    if ($(this).hasClass('new-single-request')) {

                        $(this).find('.new-request-badge').remove();
                        $(this).removeClass('new-single-request');

                        delete _self.getRequests.newTrackedRequests[requestId];
                        var requestsCount = _self.getNotificationBadgeNumber('requests');
                        requestsCount--;
                        _self.setNotificationBadgeNumber('requests', requestsCount);

                    }

                    if ($(this).hasClass('new-single-pre-booked')) {

                        $(this).find('.new-request-badge').remove();
                        $(this).removeClass('new-single-pre-booked');

                        delete _self.getRequests.newTrackedBookedRequests[requestId];
                        var bookedCount = _self.getNotificationBadgeNumber('pre-booked');
                        bookedCount--;
                        _self.setNotificationBadgeNumber('pre-booked', bookedCount);

                    }
                    var requestData = {
                        'request_id': $(this).attr('data-requst-id'),
                        'request_author': $(this).attr('data-request-author'),
                        'status': $(this).attr('data-status')
                    };
                    _self.getRequest(_self, requestData);
                })
                .on('click', '.make-request-offer-btn', function (e) {
                    e.preventDefault();
                    var reqOfferLeft = parseInt(_self.packageDetails.req_offers_left);
                    if(reqOfferLeft === 0){
                        $('#upgrade-plan-modal').modal('open');
                    }else{
                        var requestId = $(this).attr('data-requst-id');
                        var requestData = _self.requestsStorage[requestId];
                        _self.createRequstOfferModal(_self, requestData);
                    }

                })
                .on('click', '.edit-request-offer-btn', function () {
                    var requestId = $(this).attr('data-requst-id');
                    var requestData = _self.requestsStorage[requestId];

                    var offerId = $(this).attr('data-offer-id');
                    var offerData = _self.offersStorage[offerId];

                    _self.createRequstOfferModal(_self, requestData, offerData);

                })
                .on('change','.change-request-status select',function(e){

                    var saveButton = $(this).parents('.input-field').next().find('button.btn.request-status-btn'),
                        reqStatus = $(this).parents('li.single-request.active').attr('data-status');
                    // Remove Disabled Attribute from Save Button
                    saveButton.removeAttr('disabled');

                    if(e.target.value ==='declined' || e.target.value ==='canceled' ){
                        saveButton.removeClass('light-green').addClass('red');
                    }else{
                        if(reqStatus === e.target.value)
                            saveButton.attr('disabled',true);

                        if(!saveButton.hasClass('light-green'))
                            saveButton.removeClass('red').addClass('light-green');

                    }
                    // Save Selected Value on Button
                    saveButton.attr('data-selected-status',e.target.value)

                })
                .on('click','button.btn.request-status-btn',function(e){

                    // Save Request Status //
                    var requestBlock = $(this).closest('.single-request.active');

                    var requestId = requestBlock.attr('data-requst-id');

                    var selectedStatus = $(this).attr('data-selected-status');

                    if(!selectedStatus) return false;
                    if(selectedStatus === 'declined' ){
                        $('#decline-offer-modal').data({
                            request_id:requestId,
                        })
                        $('#decline-offer-modal').modal('open');
                        return false;
                    }

                    _self.editRequestStatus({requestId:requestId,status:selectedStatus});
                })

            var lastSearch ={};
            var keyupTimer,changeTimer,changeTimeout;
            // preventing enter
            $(window).keydown(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            $('#popup-modal')
                .on('click', '.delete-request-doc', function () {
                    $(this).closest('.custom-doc-row').remove();
                })
                .on('click', '.submit-request-offer-btn', function (e) {

                    $('#doc-sample-name').removeClass('validate-required invalid');
                })
                .on('click', '.save-doc-sample', function (e) {
                    $('#doc-sample-name').addClass('validate-required');
                    //$('.request-offer-doc').addClass('validate-required');
                    var scope = $('#popup-modal .doc-list-collapsible')

                    if (_self.validateInputs(scope)) {
                        //$('#doc-sample-name').removeClass('validate-required');
                        // $('.request-offer-doc').removeClass('validate-required');
                        var templateName = $('#doc-sample-name').val();
                        var docList = [];
                        $('.request-offer-doc-container .app-input').each(function () {
                            docList.push($(this).val())
                        })
                        var templateData = {
                            'doc_list': JSON.stringify(docList),
                            'template_name': templateName
                        }
                        _self.saveDocTemplate(_self, templateData);
                    }
                })
                .on('change', '#doc-template-select', function (e) {
                    $('.delete-doc-list').removeClass('hide');
                    var doc_template = _self.docTemplates[Number($(this).val())]
                    var doc_list = doc_template['documents_list'];
                    $('.request-offer-doc-container').empty();
                    var selectHtml = "";
                    for (var i = 1; i <= doc_list.length; i++) {
                        if (i === 1) {
                            selectHtml += '<div data-doc-index="1" class="row custom-doc-row required">' +
                                '<div class="input-field col s12">' +
                                ' <input value="' + doc_list[0] + '" id="request-offer-doc1" type="text"  name="document1" class="validate-required request-offer-doc app-input">' +
                                ' <label for="request-offer-doc1">Document 1</label>' +
                                ' </div>' +
                                '</div>';
                            continue;
                        }
                        selectHtml += '<div data-doc-index="' + i + '" class="row custom-doc-row">' +
                            '<div class="input-field col s11">' +
                            '<input value="' + doc_list[i - 1] + '" id="request-offer-doc' + i + '" type="text" name="document' + i + '" class="validate-required request-offer-doc app-input">' +
                            '<label for="request-offer-doc1" class="active">Document ' + i + '</label>' +
                            ' </div>' +
                            '<div class="col s1">' +
                            '<a class="btn-floating waves-light waves-effect delete-request-doc red lighten-1"><i class="material-icons">close</i></a>' +
                            ' </div>' +
                            '</div>';
                    }
                    $('.request-offer-doc-container').append(selectHtml);
                    M.updateTextFields()
                })
                .on('click', '.add-offer-request-doc', function (e) {

                    var docContainer = $('.request-offer-doc-container .custom-doc-row').last();
                    var docIndex = Number(docContainer.attr('data-doc-index')) + 1;
                    var docHtml = '<div data-doc-index="' + docIndex + '" class="row custom-doc-row">' +
                        '<div class="input-field col s11">' +
                        '<input id="request-offer-doc' + docIndex + '" type="text" name="document' + docIndex + '" class="validate-required request-offer-doc app-input">' +
                        '<label for="request-offer-doc1" class="active">Document ' + docIndex + '</label>' +
                        ' </div>' +
                        '<div class="col s1">' +
                        '<a class="btn-floating waves-light waves-effect delete-request-doc red lighten-1"><i class="material-icons">close</i></a>' +
                        ' </div>' +
                        '</div>';
                    $('.request-offer-doc-container').append(docHtml);
                    M.updateTextFields()
                })
                .on('click', '#request-offer-trans-air', function (e,edit) {

                    var formatedAirCompanyList = {};
                    $.each(airlinesList, function (index, value) {
                        formatedAirCompanyList[value.name] = 'https://pics.avs.io/40/40/'+value.code+'.png';
                    });

                    $('.air-company-autocomplete').each(function(){

                        var _this = $(this);

                        _this.autocomplete({
                            data: formatedAirCompanyList,
                            limit: 10, // The max amount of results that can be shown at once. Default: Infinity.
                            onAutocomplete: function(val) {
                                airlinesList.forEach(function(element){
                                    if(element.name === val) {
                                        var iata = element.code;
                                        var src = 'https://pics.avs.io/130/65/'+iata+'.png'

                                        if(_this.hasClass('departure')){
                                            $('#airlines-iata-code-departure').val(iata);
                                            $('.aircompany-logo.departure').attr('src',src);
                                        }else if(_this.hasClass('arrival')){
                                            $('.aircompany-logo.arrival').attr('src',src);
                                            $('#airlines-iata-code-arrival').val(iata);
                                        }
                                        return false;
                                    }
                                });
                            }
                        });
                    });



                    $('#popup-modal .air-only input[type=text]').addClass('validate-required app-input');
                    $('#popup-modal .air-only input[type=text].airport-autocomplete').removeClass('validate-required').addClass('validate-autocomplete app-input');
                    $('#popup-modal .air-only input[type=text].air-company-autocomplete').removeClass('validate-required').addClass('validate-company-autocomplete app-input');
                    $('#popup-modal .air-only input[type=radio]').addClass('app-input');

                    if(typeof edit === 'undefined'){
                        $('#popup-modal .air-only .aircompany-logo').attr('src',apetTempLogoUrl);
                        $('#popup-modal [id^=airlines-iata-code-]').val('');
                        $('#popup-modal .city-autocomplete').val('').removeAttr('data-value');
                        M.updateTextFields();
                    }

                    $('#popup-modal .air-only').show();
                    $('#popup-modal .land-only').hide();
                })
                .on('click', '#request-offer-trans-land', function (e,edit) {
                    if(typeof edit === 'undefined'){
                        $('#popup-modal .air-only .aircompany-logo').attr('src',apetTempLogoUrl);
                        $('#popup-modal [id^=airlines-iata-code-]').val('');
                        $('#popup-modal .city-autocomplete').val('').removeAttr('data-value');
                        $('#popup-modal .air-only').hide().find('input').removeClass('validate-required app-input validate-autocomplete validate-company-autocomplete').val("");
                        M.updateTextFields();
                    }
                    $('#popup-modal .land-only').show();
                })
                .on('keyup','.air-company-autocomplete',function(e){
                    if(e.which === 13) return;

                    var _this = this;
                    if($(_this).hasClass('departure')){
                        $('.aircompany-logo.departure').attr('src',apetTempLogoUrl);
                        $('#airlines-iata-code-departure').val("");
                    }else if($(_this).hasClass('arrival')){
                        $('.aircompany-logo.arrival').attr('src',apetTempLogoUrl)
                        $('#airlines-iata-code-arrival').val("");
                    }
                })
                .on('blur','.air-company-autocomplete', function (e){

                    clearTimeout(changeTimeout);
                    var _this = this;
                    var val = $(_this).val().trim();
                    if(val !== ''){
                        changeTimeout = setTimeout(function(){
                            var valid = false;
                            val = $('#'+$(_this).attr('id')).val();
                            airlinesList.forEach(function(e){

                                if(e.name.toLowerCase() === val.toLowerCase()){
                                    valid = true;
                                    var iata = e.code;
                                    var src = 'https://pics.avs.io/135/65/'+iata+'.png';
                                    if($(_this).hasClass('departure')){
                                        $('#airlines-iata-code-departure').val(iata);
                                        $('.aircompany-logo.departure').attr('src',src);
                                    }else if($(_this).hasClass('arrival')){
                                        $('.aircompany-logo.arrival').attr('src',src);
                                        $('#airlines-iata-code-arrival').val(iata);
                                    }
                                    return false;
                                }
                            });
                            if(!valid){
                                $(_this).removeClass('valid').addClass('invalid');
                                _self.errorToast("Please select Air-Company from Autocomplete")
                            }else{
                                $(_this).removeClass('invalid').addClass('valid');
                            }
                        },300)
                    }
                })
                .on('keyup','.airport-autocomplete',function(e){
                    clearTimeout(keyupTimer);
                    if(e.which == 13 || $(this).val().trim() == '') return ;
                    var that = this;
                    var thatId = $(that).attr('id');
                    keyupTimer = setTimeout(function(){
                        var searchValue = $(that).val().trim();
                        if(lastSearch[thatId] === searchValue){
                            return false;
                        }
                        lastSearch[thatId] = searchValue;
                        var id = $(that).attr('id');

                        var res =  id.split('-');

                        var airportInput,airportCityInput;

                        airportInput = $('#request-offer-' + res[2] + '-' + res[3] + '-airport-code');
                        airportInput.val("");
                        var cityScope = $(that).attr('data-country-scope');
                        airportCityInput = $('#request-offer-' + cityScope);
                        airportCityInput.val("").removeAttr('data-value');

                        _self.sendAjax({
                            data: {action:'apet_airports_autocomplate',user_id:_self.userId,search:searchValue},
                            success: function (response) {

                                if(response.success === 'true'){
                                    var airports = response.data;
                                    var newData = {};

                                    $.each(airports,function(index,value){
                                        newData[value.name +' '+ '( '+ value.iata_code+' )' + (value.municipality ? ' ' + value.municipality : "") ] = null;
                                    });
                                    $(that).autocomplete('updateData',newData).autocomplete('open');
                                    // $(that).autocomplete('updateData',newData).autocomplete('open');
                                    var instance = M.Autocomplete.getInstance($(that));
                                    instance.options.onAutocomplete =  function(val) {
                                        lastSearch[thatId] = val;
                                        airports.forEach(function(element){
                                            if(element.name +' '+ '( '+ element.iata_code+' )' + (element.municipality ? ' ' + element.municipality : "")  === val) {
                                                var iata = element.iata_code;
                                                airportInput.val(iata);
                                                var city = element.municipality;
                                                if(city) airportCityInput.val(city).attr('data-value',city);
                                                return false;
                                            }
                                        });

                                    }
                                }else{
                                    _self.errorToast('Something went wrong.');
                                }
                            },
                            error: function (error) {
                                _self.errorToast('Something went wrong.');
                            }
                        })

                    },300);
                })
                .on('blur','.airport-autocomplete',function(){
                    clearTimeout(changeTimer);
                    var _this = this;
                    if($(_this).val() !== ''){
                        changeTimer = setTimeout(function(){
                            var id = $(_this).attr('id');
                            var res =  id.split('-');

                            var airportInput,airportCityInput;
                            var cityScope = $(_this).attr('data-country-scope');

                            airportInput = $('#request-offer-' + res[2] + '-' + res[3] + '-airport-code');
                            airportCityInput = $('#request-offer-' + cityScope);
                            if(airportCityInput.val() === '' || !airportCityInput.attr('data-value') || airportCityInput.attr('data-value') === ''){
                                $(_this).removeClass('valid').addClass('invalid');
                                _self.errorToast("Please select airport from Autocomplete");
                            }else{
                                $(_this).removeClass('invalid');
                            }
                        },300);
                    }
                })
                .on('keyup','.city-autocomplete',function(){
                    $(this).attr('data-value',$(this).val().trim());
                })
                .on('keyup','#request-offer-hotel',function(e){
                    if (e.which <= 90 && e.which >= 48 || e.which === 46 || e.which === 8){
                        $('#hotel-place-id').val('');
                    }
                })
                .on('click','.cancel-offer',function(e){
                    e.preventDefault();
                    $('#cancel-req-offer-modal').modal('open');
                }).on('click','.view-request',function(){
                    var requestId = $(this).parent().siblings('input[name=request_id]').val();
                    _self.createRequestDetailsModal(_self.requestsStorage[requestId]);
                })

            $('#cancel-req-offer-modal .yes.modal-action').on('click',function(){
                $('#popup-modal').modal('close');
            });
            $('#delete-doc-template .yes').on('click', function () {
                var docIndex = $('#delete-doc-template').data('index')
                _self.deleteDocList(_self, docIndex)
            });
            $('#decline-offer-modal .decline-offer').on('click',function(){
                var reqId = $('#decline-offer-modal').data( 'request_id');

                _self.editRequestStatus({requestId:reqId,status:'declined'});
            })
            $('#requests .datepicker').datepicker({
                // selectYears: 4,
                yearRange: 2,
                showClearBtn:true,
                firstDay: 1,
                onOpen: function () {
                    var current_id = this.$el.attr('id'),instance;


                    if(current_id == 'filter-to' ){
                        instance = getPicker($('#filter-to'));
                        if($('#filter-from').val()) {
                            instance.options.minDate = new Date($('#filter-from').val());
                            instance.gotoDate(new Date($('#filter-from').val()));
                        }else{
                            instance.options.minDate = null;
                        }
                    }
                    if(current_id == 'filter-from'){
                        instance = getPicker($('#filter-from'));
                        if($('#filter-to').val()){
                            instance.options.maxDate = new Date($('#filter-to').val());
                            instance.gotoDate(new Date($('#filter-to').val()));
                        }else{
                            instance.options.maxDate = null;
                        }
                    }
                },
                onClose: function(){
                    M.updateTextFields();
                },
                format: 'yyyy-mm-dd',
                closeOnSelect: false // Close upon selecting a date,
            });

            $('#reset-filter').on('click',function(){
                _self.resetFilter();
            });

            $('#requests .requets-load-more').on('click',function(){
                if( $('html').hasClass('loading-start')){
                    return true;
                }
                _self.getRequests();
            });

        },
        editRequestStatus:function(data){

            var _self = this,
                requestBlock = $('#requests .single-request.active[data-requst-id='+data.requestId+']');

            _self.sendAjax({
                data: {
                    'user_id': _self.userId,
                    'tour_id': _self.tourId,
                    'request_id': data.requestId,
                    'status': data.status,
                    'action': 'apet_change_request_status'
                },
                beforeSend: function(){
                    requestBlock.find('.request-status-btn').attr('disabled',true);
                },
                success: function (response) {
                    if (response.success === "true") {
                        var statusBadge;
                        if(response.data.status === 'canceled'){
                            _self.cancelRequest(data.requestId,requestBlock);
                        } else if(data.status === 'declined'){
                            requestBlock.find('.single-book-table').remove();
                            requestBlock.find('.my-offer-table').remove();
                            requestBlock.find('td.request-status').html(_self.getRequestsStatusBadge({'status':response.data.status,'offered':"false"}));
                        }
                        requestBlock.attr('data-status',response.data.status);
                        statusBadge = _self.getRequestsStatusBadge({'status':response.data.status,'offered':"false"});
                        requestBlock.find('.collapsible-header .status-column').html(statusBadge);
                        _self.successToast('Status changed.');
                    }else if(response.errorMessage === 'unauthorize_access') {
                        _self.logout(_self);
                    }else{
                        if(response.errorMessage) {
                            _self.errorToast('Something went wrong.');
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                },
                loader: true
            })
        },
        getRequest: function (_self, requestData, loader) {

            if (typeof _self.getRequest.requestLoaded[requestData['request_id']] == 'undefined') {
                requestData['user_id'] = _self.userId;
                requestData['tour_id'] = _self.tourId;
                requestData['action'] = "apet_get_users_single_request";
                _self.sendAjax({
                    data: requestData,
                    success: function (response) {
                        if (response.success === "true") {
                            _self.drawRequstInnerBlock(_self, response.data);
                            _self.docTemplates = response.data['doc_list'];
                            _self.getRequest.requestLoaded[requestData['request_id']] = 'loaded';
                            _self.requestsStorage[requestData['request_id']] = response.data.request;

                            if(response.data.agency_offer ){
                                response.data.agency_offer.forEach(function (agency_offer){
                                    if(agency_offer.offer_id){
                                        _self.offersStorage[agency_offer.offer_id] = agency_offer;
                                    }
                                })
                            }
                        } else {

                            delete _self.getRequest.requestLoaded[requestData['request_id']];
                            _self.errorToast('Something went wrong.');
                            if (response.errorMessage === 'unauthorize_access') {
                                _self.logout(_self);
                            }
                        }
                    },
                    error: function (error) {
                        //to allow on error reload info
                        delete _self.getRequest.requestLoaded[requestData['request_id']];

                        _self.errorToast('Something went wrong.');
                    },
                    complete: function () {
                        $('#requests li[data-requst-id="' + requestData['request_id'] + '"] .progress').hide();
                        if (loader) {
                            $('.collapsible-single-requst .loader-layer').remove();
                        }
                    },
                    beforeSend: function () {
                        _self.getRequest.requestLoaded[requestData['request_id']] = 'loading-start';
                        if (loader) {
                            $('.collapsible-single-requst').css({position: 'relative'});
                            var loader = ' <div class="loader-layer">\
                       <div class="loader-container">\
                      <div class="preloader-wrapper big active">\
                    <div class="spinner-layer spinner-green-only">\
                        <div class="circle-clipper left">\
                            <div class="circle"></div>\
                        </div><div class="gap-patch">\
                            <div class="circle"></div>\
                        </div><div class="circle-clipper right">\
                            <div class="circle"></div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div class="white-layer"></div>\
           </div>';
                            $('.collapsible-single-requst').append(loader)
                        }
                    }
                })
            }


        },
        cancelRequest: function(requestId,requestBlock){
            var _self = this;
            _self.getRequests.offset--;
            if(typeof _self.getRequests.filterOptions === 'undefined' || _self.getRequests.filterOptions.status !== 'deleted') {
                requestBlock.remove();
                _self.getSpecOffers.offset--;
                if ($('#requests .request-table li').length === 0) {
                    $('#requests .request-table').remove();
                    $('.offer-empty-msg').show();
                }
            }
        },
        createRequestDetailsModal: function (requestsData) {
            var _self = this;
            var socialPackageRow ="",socialPackage = requestsData['social_package'];

            if(requestsData['country'].toLowerCase() ==='armenia' && socialPackage){
                socialPackageRow ='<tr><th>Social Package</th><td class="capitalize">'+ (socialPackage === 'true' ? 'Yes' : 'No')+'</td></tr>';
            }
            var extra = requestsData['extra'];
            var extraRow = "";
            if (extra && extra.length) {
                extraRow += '<tr><th>Additional Options</th><td class="capitalize">';
                for (var i = 0; i < extra.length; i++) {
                    extraRow += _self.labels[extra[i]] + '<br>'
                }
                extraRow += '</td></tr>'

            }
            var desiredHotels = requestsData['desired_hotels'];
            var desiredHotelsRow = "";

            if(desiredHotels && desiredHotels.length){
                desiredHotelsRow += '<tr><th>Desired Hotels</th><td class="capitalize">';
                for (var i = 0; i < desiredHotels.length; i++) {
                    desiredHotelsRow += desiredHotels[i] + '<br>'
                }
                desiredHotelsRow += '</td></tr>'
            }else{
                desiredHotelsRow ='<tr><th>Desired Hotels</th><td class="capitalize">Not Specified</td></tr>';
            }

            var boardType = requestsData['board_type'];
            var boardTypeRow = "";

            if(typeof boardType === 'object' && boardType && boardType.length){

                boardTypeRow += '<tr><th>Board Type</th><td class="capitalize">';
                for (var i = 0; i < boardType.length; i++) {
                    boardTypeRow += _self.labels[boardType[i]] + '<br>'
                }
                boardTypeRow += '</td></tr>'
            }else{
                boardTypeRow = '<tr>' +
                    '              <th>Board Type</th>' +
                    '              <td class="capitalize">' + (requestsData["board_type"] ? _self.labels[requestsData["board_type"]] : 'Not Specified') + '</td>' +
                    '           </tr>'
            }

            var childrenAges = requestsData['children_ages'];
            var childrenAgesRow = "";
            if (childrenAges && childrenAges.length) {

                for (var i = 0; i < childrenAges.length; i++) {
                    childrenAgesRow += childrenAges[i];
                    if(i < childrenAges.length - 1){
                        childrenAgesRow += ', ';
                    }
                }

            }

            var selectReqStatusHtml = _self.getRequestsStatusBadge(requestsData);

            var citizenshipRow = requestsData["nationality"] ? '<tr>' +
                '                            <th>Citizenship</th>' +
                '                            <td class="capitalize">' + requestsData["nationality"] + '</td>' +
                '                        </tr>' : '';

            var visaSupportRow = requestsData['visa_support'] ? '<tr>' +
                '                            <th>Visa Support</th>' +
                '                            <td class="capitalize">' + requestsData["visa_support"] + '</td>' +
                '                        </tr>' : '';

            var oldNightRow = requestsData['nights'] ? '<tr>' +
                '                            <th>Nights</th>' +
                '                            <td>' + requestsData["nights"] + ' (Days - ' + (parseInt(requestsData["nights"]) + 1) + ')' + '</td>' +
                '                        </tr>' : '';

            var newNightRow = !requestsData['nights'] ? '<tr>' +
                '                            <th>Nights Range</th>' +
                '                            <td>(' + requestsData["nights_from"] + ' - ' +  requestsData["nights_to"] + ')Nights</td>' +
                '                        </tr>' : '';

            var requestTable ='<table class="bordered request-details-table">\
                    <tbody>\
                        <tr>\
                            <th>Request Date</th>\
                            <td class="capitalize"> '+ requestsData["request_date"] +'</td>\
                        </tr>\
                        <tr>\
                            <th>Country</th>\
                            <td class="capitalize">' + requestsData["country"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>City</th>\
                            <td class="capitalize">' + (requestsData["city"] ? requestsData["city"] : "Any") + '</td>\
                        </tr>'
                + oldNightRow
                + newNightRow +
                '<tr>\
                   <th>Period From</th>\
                   <td>' + requestsData["period_from"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Period To</th>\
                            <td>' + requestsData["period_to"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Travelers</th>\
                            <td>'
                + (requestsData["adults"] ? '<div>Adults: ' + requestsData["adults"] + '</div>':'') +
                ( +requestsData["children"] !== 0  || +requestsData["infant"] !== 0 ? '<div>Children: ' + (+requestsData["children"] + +requestsData["infant"]) + ' ('+ childrenAgesRow +' year'+ (childrenAgesRow === '1'? "": "s") +' old)</div>': '')+
                '</td>\
             </tr>\
            <tr>\
                <th>Price Range</th>\
                <td>' +  (requestsData["price_from"] && requestsData["price_to"] ? (requestsData["price_from"] +' - '+requestsData["price_to"]+' AMD') : "Not Specified")  + '</td>\
                        </tr>\
                        <tr>\
                            <th>Hotel Category</th>\
                            <td>' + (requestsData["hotel_category"] == 0 ? 'Not Specified' : requestsData["hotel_category"] + ' ' + (requestsData["hotel_category"] > 1 ? ' Stars' : ' Star')) + '</td>\
                        </tr>'
                + desiredHotelsRow +
                '<tr>\
                    <th>Rooms</th>\
                    <td>' + (requestsData["rooms"] == 0 ? 'Not Specified' : requestsData["rooms"]) + '</td>\
                        </tr>'
                + boardTypeRow +
                (requestsData["payment_method"]? '\
                                    <th>Payment Method</th>\
                                    <td class="capitalize">' + (requestsData["payment_method"] ? _self.labels[requestsData["payment_method"]] : 'Not Specified') + '</td>\
                                </tr>': '')
                + extraRow
                + socialPackageRow  +
                '<tr>\
                    <th>Notes</th>\
                    <td>' + (requestsData["notes"]? requestsData["notes"] : "Not Specified") + '</td>\
                        </tr>\
                        <tr>\
                            <th>Status</th>\
                            <td class="capitalize request-status ">' + selectReqStatusHtml + '</td>\
                        </tr>\
                </tbody>\
            </table>';

            var html = '<div id="request-details-modal" class="modal modal-fixed-footer">\
                            <div class="modal-content">\
                                <h4><span>Request: #'+ requestsData["request_id"] + '</span></h4>'+
                                requestTable +
                            '</div>\
                            <div class="modal-footer">\
                                <a href="javascript:void(0);" class="yes modal-action modal-close waves-effect waves-red btn-flat">Close</a>\
                            </div>\
                        </div>';
            $('body').append(html);
            $('#request-details-modal').modal({
                onCloseEnd: function(){
                    $('#request-details-modal').remove();
                }
            }).modal('open');
        },
        filterSpecOffers:function(data){
            var _self = this;

            delete _self.getSpecOffers.offset;
            _self.getSpecOffers.filterOptions = {
                'status': data.status,
                'search': data.search
            };
            _self.getSpecOffers();
        },
        filterTourAgencies:function(data){
            var _self = this;

            delete _self.getTourAgencies.offset;
            _self.getTourAgencies.filterOptions = {
                'status': data.status,
                'search': data.search
            };
            _self.getTourAgencies();
        },
        filterAppUsers:function(data){
            var _self = this;

            delete _self.getAppUsers.offset;
            _self.getAppUsers.filterOptions = {
                'status': data.status,
                'search': data.search
            };
            _self.getAppUsers();

        },
        filterStats: function (data) {
            var _self = this;


            var filteredData = {
                'type': data.type,
                'date': {
                    'from': data.from,
                    'to': data.to
                }
            };
            if (data.sub_type) {
                filteredData.sub_type = data.sub_type
            }
            if (data.data_types) {
                filteredData.data_types = JSON.parse(data.data_types);
            }


            _self.getStats.filterOptions = filteredData;
            _self.getStats();
        },
        resetSpecOffersFilter:function(doRequest){
            var _self = this;
            if(typeof doRequest == 'undefined'){
                doRequest = true;
            }

            if(!_self.getSpecOffers.filterOptions ){
                return;
            }

            delete  _self.getSpecOffers.filterOptions;
            delete _self.getSpecOffers.offset;
            $('#special-offers-filter-form')[0].reset();
            M.updateTextFields()

            if(doRequest) {
                _self.getSpecOffers(true);
            }
        },
        resetTourAgenciesFilter:function(doRequest){

            var _self = this;
            if(typeof doRequest == 'undefined'){
                doRequest = true;
            }

            if(!_self.getTourAgencies.filterOptions ){
                return;
            }

            delete  _self.getTourAgencies.filterOptions;
            delete _self.getTourAgencies.offset;
            $('#tour-agencies-filter-form')[0].reset();
            M.updateTextFields()

            if(doRequest) {
                _self.getTourAgencies(true);
            }
        },
        resetAppUsersFilter:function(doRequest){

            var _self = this;
            if(typeof doRequest == 'undefined'){
                doRequest = true;
            }

            if(!_self.getAppUsers.filterOptions ){
                return;
            }

            delete  _self.getAppUsers.filterOptions;
            delete _self.getAppUsers.offset;
            $('#app-users-filter-form')[0].reset();
            M.updateTextFields()

            if(doRequest) {
                _self.getAppUsers(true);
            }
        },
        filterRequests: function (data) {
            var _self = this;

            if( $('html').hasClass('loading-start')){
                return true;
            }

            var detectResetFilterState = false;
            if(data.status == ''){
                detectResetFilterState = true;
                $.each(data, function(key){
                    if( data[key] != '' && key !='status' ){
                        detectResetFilterState = false;
                    }
                })
            }
            if(detectResetFilterState){
                _self.resetFilter();
            }else{
                delete _self.getRequests.offset;
                /* clear requests dashboard */
                $('#requests .requests-container').empty();

                _self.getRequests.filterOptions = {
                    'status': data.status,
                    'id':data.id,
                    'country': data.country,
                    'date':{
                        'from': data.from,
                        'to': data.to
                    }
                };

                if(data.id) {
                    history.pushState({}, "dashboard#requests-page", location.origin + "/dashboard?request_id="+ data.id +"#requests-page");
                }
                _self.getRequests();
            }

        },
        resetStatsFilter:function(doRequest){
            var _self = this;
            if(typeof doRequest == 'undefined'){
                doRequest = true;
            }

            if(!_self.getStats.filterOptions ){
                return;
            }

            delete  _self.getStats.filterOptions;


            $('#stats-filter-form').removeClass(function (index, css) {
                return (css.match(/\bfilter-type-\S+/g) || []).join(' ');
            });
            $('#stats-filter-form').removeClass(function (index, css) {
                return (css.match(/\bfilter-sub-type-\S+/g) || []).join(' ');
            });
            $('#stats-filter-form').addClass('filter-type-user');


            $('#stats-filter-form')[0].reset();
            $('#stats-filter-form select').formSelect();
            $('#stats-filter-type').change();
            M.updateTextFields()
            if(doRequest) {
                _self.getStats(true);
            }
        },
        /**
         *
         * @param {boolean} doRequest set false to disable request after reseting
         */
        resetFilter:function(doRequest){
            var _self = this;
            if(typeof doRequest == 'undefined'){
                doRequest = true;
            }

            if(!_self.getRequests.filterOptions ){
                return;
            }
            history.pushState({}, "dashboard#requests-page", location.origin + "/dashboard#requests-page");
            $('#requests-filter-form')[0].reset();
            M.updateTextFields()

            if(doRequest) {
                _self.getRequests(true);
            }

        },
        /**
         *
         * @param {object} filterOptions optionsn for filter
         * @param {boolean} doRequest set false to disable request after setting filter;
         */
        setFilter:function(filterOptions,doRequest){
            var _self = this;

            _self.resetFilter(false);
            if(typeof doRequest == 'undefined'){
                doRequest = true;
            }

            if(filterOptions.status && $('#filter-status option[value="'+filterOptions.status+'"]').length){
                $('#filter-status').val(filterOptions.status);
                $('#filter-status').formSelect();
            }
            if(filterOptions.id){
                $('#filter-id').val(filterOptions.id);
            }
            if(filterOptions.country){
                $('#filter-country').val(filterOptions.country);
            }
            if(filterOptions.from){
                $('#filter-from').val(filterOptions.from);
            }
            if(filterOptions.to){
                $('#filter-to').val(filterOptions.to);
            }
            M.updateTextFields();
            if(doRequest) {
                $('#requests-filter-form').submit();
            }
        },
        getRequests: function (firstLoad) {
            var _self = this;

            firstLoad = (typeof firstLoad === 'undefined') ? false : firstLoad;

            if(firstLoad){
                delete  _self.getRequests.filterOptions;
                delete _self.getRequests.offset;
                $('#requests-filter-form')[0].reset();
                M.updateTextFields()
            }

            var loader = false;
            if(typeof _self.getRequests.offset === 'undefined'){
                _self.getRequests.offset = 0;
                loader = true;
            }

            $('html').addClass('loading-start');



            var userId = _self.userId;
            var tourId = _self.tourId;
            var data  = {
                'action': 'apet_get_users_requests',
                'user_id': userId,
                'tour_id': tourId,
                'limit': _self.limit,
                'offset': _self.getRequests.offset
            };
            if(_self.getRequests.filterOptions){
                data['filter'] = JSON.stringify(_self.getRequests.filterOptions);
            }

            _self.sendAjax({
                data: data,
                complete: function(){
                    $('html').removeClass('loading-start');
                },
                beforeSend:function(){
                    if( _self.getRequests.offset == 0){
                        $('#requests .requests-container').empty();
                        _self.getRequest.requestLoaded = {};
                    }
                },
                success: function (response) {
                    if (response.success === "true") {
                        /* get last request id  for further usage*/
                        if(typeof _self.getRequests.lastUpdateTime === 'undefined'){
                            _self.getRequests.lastUpdateTime = response.extra.utc_time;
                        }
                        _self.packageDetails.req_offers_left = response.extra['req_offers_left'];
                        _self.packageDetails.spec_offers_left = response.extra['spec_offers_left'];
                        _self.getRequests.first_id =  response.data[response.data.length-1].request_id;
                        _self.drawRequests(_self, response.data);
                        _self.getRequests.offset  += response.data.length;
                        if(_self.getRequests.filterOptions && _self.getRequests.filterOptions.id){
                            $('#requests .requests-container .request-table').collapsible('open', 0).find('li.single-request[data-requst-id='+ _self.getRequests.first_id+']').click();
                        }
                    } else {
                        if (response.errorMessage === 'no_requests') {

                            if (!$('#requests .request-table li').length) {
                                $('#requests .requests-container').empty();
                                $('.request-empty-msg').show();
                            }
                        }
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                },loader: loader
            })

        },
        drawRequests: function (_self, requests) {
            $('.request-empty-msg').hide();
            if(!$('.requests-container .request-table').length){
                $('.requests-container').append('<ul class="collapsible request-table" data-collapsible="accordion"></ul>');
            }

            var reqOffersLeft = _self.packageDetails.req_offers_left;
            _self.updateReqOffersCounter(reqOffersLeft);

            var requestHtml = '';
            $.each(requests, function (key, requestsData) {
                if(_self.getRequests.newTrackedRequests && _self.getRequests.newTrackedRequests[requestsData['request_id']]){
                    requestsData['new'] = true;
                }

                if(_self.getRequests.newTrackedBookedRequests && _self.getRequests.newTrackedBookedRequests[requestsData['request_id']]){
                    requestsData['pre-booked'] = true;
                }

                requestHtml += _self.addRequstBlock(requestsData, _self);
            })
            $('.requests-container .request-table').append(requestHtml);
            $('.collapsible').collapsible();
        },
        addRequstBlock: function (requestsData, _self) {

            var statusBadge = _self.getRequestsStatusBadge(requestsData);

            var newRequest = requestsData['new'] || requestsData["pre-booked"] ? ' <span class="new badge new-request-badge red lighten-1" data-badge-caption="New"></span>' : "";
            var blockHtml = '<li data-status="' + requestsData["status"] + '" data-requst-id="' + requestsData["request_id"] + '" data-request-author="' + requestsData["request_author"] + '"  class="single-request' + (requestsData["new"] ? ' new-single-request' : '') + (requestsData["pre-booked"] ? ' new-single-pre-booked' : '') + '">\
                                <div class="collapsible-header">\
                                    <table>\
                                        <tbody>\
                                            <tr>\
                                                <th class="capitalize">' + requestsData["country"] + '</th>\
                                                <th class="capitalize">' + requestsData["city"] + '</th>\
                                                <th class="capitalize">Nights: ' + requestsData["nights"]  + '</th>\
                                                <th class="status-column capitalize">'+ newRequest + statusBadge +'</th>\
                                                <td>\
                                                    <a class="view-request collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a>\
                                                </td>\
                                            </tr>\
                                            </tbody>\
                                        </table>\
                                    </div>\
                                    <div class="collapsible-body collapsible-single-requst grey lighten-3">\
                                        <div class = "progress" >\
                                            <div class = "indeterminate" ></div>\
                                        </div>\
                                    </div>\
                                </li>';
            return blockHtml;
        },
        getRequestsStatusBadge: function (requestsData)     {

            var _self = this;

            var badgeColor = '';
            var badgeCaption = _self.helpers.firstToUpperCase(requestsData['status']);


            if(_self.tourId !== 'all' && (requestsData['status'] === 'offered' && requestsData['offered'] && requestsData['offered'] === 'true' ) || (requestsData['status'] === 'pending' && requestsData['offered'] && requestsData['offered'] === 'true')){
                badgeColor = 'light-blue';
                badgeCaption = "Offered";
            }else if( _self.tourId === 'all' && (requestsData['status'] === 'offered' || ( requestsData['offered'] && requestsData['offered'] === 'true'))){
                badgeColor = 'light-blue';
                badgeCaption = "Offered";
            }else if( _self.tourId !== 'all' && (requestsData['status'] === 'offered' && requestsData['offered'] !== 'true')){
                badgeColor='deep-orange';
                badgeCaption = "Pending";
            }else if(requestsData['status'] === 'pending'){
                badgeColor='deep-orange';
            }

            if(requestsData['status'] === 'pre-booked'){
                badgeColor = 'light-green';
                badgeCaption = "Pre-booked";
            }else if(requestsData['status'] === 'processing'){
                badgeColor = 'blue-grey';
                badgeCaption = "Processing";
            }else if(requestsData['status'] === 'declined'){
                badgeColor = 'red darken-4';
                badgeCaption = "Declined";
            }else if(requestsData['status'] === 'confirmed'){
                badgeColor = 'light-green darken-4';
                badgeCaption = "Confirmed";
            }else if(requestsData['status'] === 'booked'){
                badgeColor = 'green darken-4';
                badgeCaption = "Booked";
            }else if(requestsData['status'] === 'canceled'){
                badgeColor = 'red';
                badgeCaption = "Canceled";
            }
            return '<span class="new badge ' + badgeColor + ' lighten-1" data-badge-caption="'+ badgeCaption +'"></span>';
        },
        drawRequstInnerBlock: function (_self, data) {
            var requestsData = data['request'];

            var socialPackageRow ="",socialPackage = requestsData['social_package'];

            if(requestsData['country'].toLowerCase() ==='armenia' && socialPackage){
                socialPackageRow ='<tr><th>Social Package</th><td class="capitalize">'+ (socialPackage === 'true' ? 'Yes' : 'No')+'</td></tr>';
            }
            var extra = requestsData['extra'];
            var extraRow = "";
            if (extra && extra.length) {
                extraRow += '<tr><th>Additional Options</th><td class="capitalize">';
                for (var i = 0; i < extra.length; i++) {
                    extraRow += _self.labels[extra[i]] + '<br>'
                }
                extraRow += '</td></tr>'

            }
            var desiredHotels = requestsData['desired_hotels'];
            var desiredHotelsRow = "";

            if(desiredHotels && desiredHotels.length){
                desiredHotelsRow += '<tr><th>Desired Hotels</th><td class="capitalize">';
                for (var i = 0; i < desiredHotels.length; i++) {
                    desiredHotelsRow += desiredHotels[i] + '<br>'
                }
                desiredHotelsRow += '</td></tr>'
            }else{
                desiredHotelsRow ='<tr><th>Desired Hotels</th><td class="capitalize">Not Specified</td></tr>';
            }

            var boardType = requestsData['board_type'];
            var boardTypeRow = "";

            if(typeof boardType === 'object' && boardType && boardType.length){

                boardTypeRow += '<tr><th>Board Type</th><td class="capitalize">';
                for (var i = 0; i < boardType.length; i++) {
                    boardTypeRow += _self.labels[boardType[i]] + '<br>'
                }
                boardTypeRow += '</td></tr>'
            }else{
                boardTypeRow = '<tr>' +
                    '              <th>Board Type</th>' +
                    '              <td class="capitalize">' + (requestsData["board_type"] ? _self.labels[requestsData["board_type"]] : 'Not Specified') + '</td>' +
                    '           </tr>'
            }

            var childrenAges = requestsData['children_ages'];
            var childrenAgesRow = "";
            if (childrenAges && childrenAges.length) {

                for (var i = 0; i < childrenAges.length; i++) {
                    childrenAgesRow += childrenAges[i];
                    if(i < childrenAges.length - 1){
                        childrenAgesRow += ', ';
                    }
                }

            }
            var userHtml = "";

            if( _self.tourId === 'all' || requestsData['status'] ==='confirmed' || requestsData['status'] ==='booked') {

                if (data['user_data']['email']) {
                    var userData = data['user_data'];
                    userHtml = '<table class="bordered single-user-table box-shadow">\
                                <caption>User Info</caption>\
                                    <tbody>\
                                    <tr>\
                                        <th>First Name</th>\
                                        <td class="capitalize">' + userData["firstname"] + '</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Last Name</th>\
                                    <td class="capitalize">' + userData["lastname"] + '</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Phone</th>\
                                        <td>' + (userData["phone"] ? userData["phone"] : "Not Specified") + '</td>\
                                    </tr>\
                                    <tr>\
                                        <th>Email</th>\
                                        <td>' + userData["email"] + '</td>\
                                    </tr>\
                                </tbody>\
                            </table>';
                }
            }

            var bookHtml = "";
            if (data['agency_offer'][0] && data['agency_offer'][0]['book_id']) {
                var travelersData = data['agency_offer'][0]['travelers_data'];
                var travelersHtml = "";

                if( _self.tourId === 'all' || requestsData['status'] ==='confirmed' || requestsData['status'] ==='booked'){
                    $.each(travelersData, function (traveler, info) {
                        traveler = traveler.replace(/_/g, ' ');
                        var travelerInitials = '';
                        travelerInitials += info['lastname'] ? info['lastname'] + ' ' : '';
                        travelerInitials += info['firstname'] ? info['firstname'] : '';
                        travelerInitials = travelerInitials ? '<span class="traveler-initials">' + travelerInitials + '</span>' : '';

                        var nationality =  info['nationality'] ? '<div class="capitalize  personal-info"> Nationality - ' + info['nationality'] + ' </div>' : '';
                        var visaSupport =  info['visa-support'] ? '<div class="capitalize  personal-info"> Visa-support - ' +(info['visa-support'] === '1' ? 'Yes': 'No') + ' </div>' : '';

                        if (info['photo'] === "") {
                            travelersHtml += '<tr>\
                                                <th class="traveler-label capitalize">' + traveler + '</th>\
                                                <td class="capitalize">\
                                                ' + travelerInitials + '\
                                                '+ nationality +'\
                                                '+ visaSupport +'\
                                                </td>\
                                            </tr>';
                        } else {

                            travelersHtml += '<tr>\
                                                <th class="traveler-label capitalize">' + traveler + '</th>\
                                                <td  class="table-photo-container">'
                                + travelerInitials +
                                '<div class="photo-frame">\
                                                        <img src="/bookpics/?url=' + info['photo'] + '" class="booked-traveler-photo materialboxed" />\
                                                    </div>\
                                                    '+ nationality +'\
                                                    '+ visaSupport +'\
                                                </td>\
                                            </tr>';
                        }
                    })
                }

                bookHtml = '<table class="bordered single-book-table box-shadow">\
                                <caption>Book Info #' + data['agency_offer'][0]['book_id'] + '</caption>\
                                <tbody>\
                                    <tr>\
                                        <th>Book Date</th>\
                                        <td>' + data['agency_offer'][0]['book_date'] + '</td>\
                                    </tr>'
                    + travelersHtml +
                    '</tbody>\
                </table>';
            }
            var agencyOfferHtml ='';
            if(data['agency_offer'].length){
                agencyOfferHtml ='<ul class="collapsible offers-accordion" data-collapsible="accordion">';
                data['agency_offer'].forEach(function (agency_offer){

                    if (agency_offer['offer_id']) {
                        agencyOfferHtml +='<li>' +
                            '<div class="collapsible-header">' +
                            '<table>' +
                            '<tr>' +
                            '<th><span>Offer: <strong>#' + agency_offer["offer_id"] +'</strong></span>'+ (agency_offer["seen_status"] === 'seen' ? '<span class="seen-status">Seen</span>' :'') +'</th>' +
                            '<td>Offer date: '+ agency_offer["offer_creation_date"] +'</td>' +
                            '<td>Offer deadline: '+ agency_offer["offer_deadline"] +'</td>' +
                            '<td>Offer Status: '+ agency_offer["offer_status"] +'</td>' +
                            '<td><a class="view-request collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a></td>' +
                            '</tr>' +
                            '</table>' +
                            '</div>' +
                            '<div class="collapsible-body">';
                        agencyOfferHtml += _self.drawReqOfferBlock(_self, agency_offer);
                        agencyOfferHtml += '</div></li>';
                    }
                });
                agencyOfferHtml  +='</ul>';
            }

            var makeOfferBtn = '';

            if (_self.tourId !== 'all' && (requestsData['status'] ==='pending' || requestsData['status'] ==='offered')) {
                makeOfferBtn = ' </tr>' +
                    '<tr class="tr-no-border make-offer-btn-tr">' +
                    '<th colspan="2" style="text-align:right"><a data-requst-id="' + requestsData["request_id"] + '" class="waves-effect make-request-offer-btn waves-light darken-1 light-green btn "  >Make Offer</a></th>' +
                    ' </tr>';
            }

            var selectReqStatusHtml = requestsData['status'] = (requestsData['status'] === 'pending' && requestsData['offered'] && requestsData['offered'] === 'true')? 'offered': requestsData['status'];

            var allowStatuses = ['pre-booked','processing','canceled','declined','confirmed','booked'];

            if(_self.tourId === 'all' && $.inArray(requestsData['status'],allowStatuses) > -1 && requestsData['status'] !== 'booked'){
                selectReqStatusHtml ='<div class="change-request-status row"><div class="input-field col s12 m7"><select name="status" class="app-input selected-request-status">';
                var disabled ='';
                allowStatuses.forEach(function(element){
                    disabled = element === 'pre-booked' ? 'disabled': '';
                    var selected = requestsData['status'] === element ? "selected" : "";
                    selectReqStatusHtml+='<option value="'+ element +'"  '+ selected +' '+ disabled +'>'+ _self.helpers.firstToUpperCase(element) +'</option>';
                });
                selectReqStatusHtml += '</select></div><div class="input-field col s12 m5"><button class="btn waves-effect waves-light darken-1 light-green request-status-btn" disabled type="submit">Save</button></div>'

            }else{
                selectReqStatusHtml = _self.getRequestsStatusBadge(requestsData);
            }
            var caption = '';

            if(_self.tourId !== 'all' && data['agency_offer'].length){
                caption = '<h5 class="center">My Offers</h5>';
            }
            var citizenshipRow = requestsData["nationality"] ? '<tr>' +
                '                            <th>Citizenship</th>' +
                '                            <td class="capitalize">' + requestsData["nationality"] + '</td>' +
                '                        </tr>' : '';

            var visaSupportRow = requestsData['visa_support'] ? '<tr>' +
                '                            <th>Visa Support</th>' +
                '                            <td class="capitalize">' + requestsData["visa_support"] + '</td>' +
                '                        </tr>' : '';

            var oldNightRow = requestsData['nights'] ? '<tr>' +
                '                            <th>Nights</th>' +
                '                            <td>' + requestsData["nights"] + ' (Days - ' + (parseInt(requestsData["nights"]) + 1) + ')' + '</td>' +
                '                        </tr>' : '';

            var newNightRow = !requestsData['nights'] ? '<tr>' +
                '                            <th>Nights Range</th>' +
                '                            <td>(' + requestsData["nights_from"] + ' - ' +  requestsData["nights_to"] + ')Nights</td>' +
                '                        </tr>' : '';

            var requestAccourdion =
                '<h5 class="center">User Request</h5>' +
                '<ul class="collapsible request-accordion" data-collapsible="accordion">' +
                '<li >' +
                    '<div class="collapsible-header">' +
                        '<table>' +
                            '<tr>' +
                                '<th class="capitalize">Request: <strong>#' + requestsData["request_id"] + '</strong></th>' +
                                '<td class="capitalize right-align">Request Date: ' + requestsData["request_date"] + '</td>' +
                                '<td><a class="view-request collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a></td>'+
                            '</tr>' +
                        '</table>' +
                    '</div>' +
                    '<div class="collapsible-body">';

            requestAccourdion+='<table class="bordered single-request-table box-shadow">\
                    <tbody>\
                        <tr>\
                            <th>Country</th>\
                            <td class="capitalize">' + requestsData["country"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>City</th>\
                            <td class="capitalize">' + (requestsData["city"] ? requestsData["city"] : "Any") + '</td>\
                        </tr>'
                            + oldNightRow
                            + newNightRow +
                        '<tr>\
                           <th>Period From</th>\
                           <td>' + requestsData["period_from"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Period To</th>\
                            <td>' + requestsData["period_to"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Travelers</th>\
                            <td>'
                                + (requestsData["adults"] ? '<div>Adults: ' + requestsData["adults"] + '</div>':'') +
                                 ( +requestsData["children"] !== 0  || +requestsData["infant"] !== 0 ? '<div>Children: ' + (+requestsData["children"] + +requestsData["infant"]) + ' ('+ childrenAgesRow +' year'+ (childrenAgesRow === '1'? "": "s") +' old)</div>': '')+
                            '</td>\
                         </tr>\
                        <tr>\
                            <th>Price Range</th>\
                            <td>' +  (requestsData["price_from"] && requestsData["price_to"] ? (requestsData["price_from"] +' - '+requestsData["price_to"]+' AMD') : "Not Specified")  + '</td>\
                        </tr>\
                        <tr>\
                            <th>Hotel Category</th>\
                            <td>' + (requestsData["hotel_category"] == 0 ? 'Not Specified' : requestsData["hotel_category"] + ' ' + (requestsData["hotel_category"] > 1 ? ' Stars' : ' Star')) + '</td>\
                        </tr>'
                        + desiredHotelsRow +
                        '<tr>\
                            <th>Rooms</th>\
                            <td>' + (requestsData["rooms"] == 0 ? 'Not Specified' : requestsData["rooms"]) + '</td>\
                        </tr>'
                        + boardTypeRow +
                        (requestsData["payment_method"]? '\
                                    <th>Payment Method</th>\
                                    <td class="capitalize">' + (requestsData["payment_method"] ? _self.labels[requestsData["payment_method"]] : 'Not Specified') + '</td>\
                                </tr>': '')
                        + extraRow
                        + socialPackageRow  +
                        '<tr>\
                            <th>Notes</th>\
                            <td>' + (requestsData["notes"]? requestsData["notes"] : "Not Specified") + '</td>\
                        </tr>\
                        <tr>\
                            <th>Status</th>\
                            <td class="capitalize request-status ">' + selectReqStatusHtml + '</td>\
                        </tr>'
                        + makeOfferBtn +
                    '</tbody>\
                </table>';

            requestAccourdion += '</div></li></ul>';


            var blockHtml = userHtml + bookHtml + requestAccourdion + caption + agencyOfferHtml;
            $('#requests li[data-requst-id="' + requestsData["request_id"] + '"] .collapsible-single-requst').append(blockHtml);
            $('#requests .materialboxed').materialbox();
            $('#requests select').formSelect();
            $('#requests li[data-requst-id="' + requestsData["request_id"] + '"] .collapsible-single-requst .collapsible').collapsible();
        },
        createRequstOfferModal: function (_self, requestData, offerData) {

            var airportHtml = "";
            // if (requestData['airticket'] !== 'no') {
            //     airportHtml = '<div class="row">\
            //                         <div class="col s12">\
            //                             <p>\
            //                                 <input checked class="with-gap app-input" name="flight_type" value="transit" type="radio" id="request-offer-flight-tr"/>\
            //                                 <label for="request-offer-flight-tr" >Transit</label>\
            //                             </p>\
            //                             <p>\
            //                                 <input class="with-gap app-input" name="flight_type" type="radio" value="direct" id="request-offer-flight-dir"/>\
            //                                 <label for="request-offer-flight-dir" >Direct</label>\
            //                             </p>\
            //                         </div>\
            //                     </div>\
            //                     <div class="row">\
            //                         <div class="input-field col s12">\
            //                             <input id="request-offer-departure" name="departure" type="text" class="datepicker validate-required validate-date app-input" >\
            //                             <label for="request-offer-departure">Departure</label>\
            //                         </div>\
            //                     </div>\
            //                     <div class="row">\
            //                         <div class="input-field col s12">\
            //                             <input id="request-offer-arrival" name="arrival" type="text"  data-date-compare="request-offer-departure" class="datepicker validate-required validate-date app-input" >\
            //                             <label for="request-offer-arrival">Arrival</label>\
            //                         </div>\
            //                     </div>';
            // }
            var documents = "";
            // if (requestData['visa_support'] === 'yes') {
            //     var docList = "";
            //     if (_self.docTemplates.length) {
            //         for (var i = 0; i < _self.docTemplates.length; i++) {
            //             docList += '<option value="' + i + '">' + _self.docTemplates[i]['template_name'] + '</option>';
            //         }
            //     }
            //     documents = '<div class="row">\
            //                     <div class="input-field col s12">\
            //                         <ul class="collapsible doc-list-collapsible" data-collapsible="accordion">\
            //                             <li>\
            //                                 <div class="collapsible-header"><i class="material-icons">description</i><span>Doc List</span></div>\
            //                                 <div class="collapsible-body">\
            //                                     <ul class="collapsible doc-sample-collapsible" data-collapsible="accordion">\
            //                                         <li>\
            //                                             <div class="collapsible-header">\
            //                                                 <i class="material-icons">folder_open</i><span>Document Sample</span>\
            //                                             </div>\
            //                                             <div class="collapsible-body">\
            //                                                 <div class="row request-offer-doc-sample">\
            //                                                     <div class="input-field col s4">\
            //                                                         <input id="doc-sample-name" type="text"  name="doc-sample-name" >\
            //                                                         <label for="doc-sample-name">Sample Name</label>\
            //                                                     </div>\
            //                                                     <div class="input-field col s2">\
            //                                                         <a class="btn waves-light waves-effect save-doc-sample  lighten-2 green">Save</a>\
            //                                                     </div>\
            //                                                     <div class="input-field col s4">\
            //                                                         <select id="doc-template-select" class="doc-template-select">\
            //                                                         <option value="" disabled selected>Choose Sample</option>'
            //         + docList +
            //         '</select>\
            //                                                     <label>Select Sample</label>\
            //                                                 </div>\
            //                                                   <div class="input-field col s2">\
            //                                                     <a  class="delete-doc-list hide btn-floating waves-effect waves-light  btn red lighten-1"><i class="material-icons">delete_forever</i></a>\
            //                                                 </div>\
            //                                             </div>\
            //                                         </div>\
            //                                     </li>\
            //                                 </ul>\
            //                                 <div class="request-offer-doc-container">\
            //                                     <div data-doc-index="1" class="row custom-doc-row">\
            //                                         <div class="input-field col s12">\
            //                                             <input id="request-offer-doc1" type="text"  name="document1" class="validate-required request-offer-doc app-input">\
            //                                             <label for="request-offer-doc1">Document 1</label>\
            //                                         </div>\
            //                                     </div>\
            //                                 </div>\
            //                                 <div class="center">\
            //                                     <a class="add-offer-request-doc btn-floating  darken-1 light-green">\
            //                                         <i class="large material-icons">add</i>\
            //                                     </a>\
            //                                 </div> \
            //                             </div>\
            //                         </li>\
            //                     </ul>\
            //                 </div>\
            //             </div>';
            // }
            //
            var airOnly = '';
            var landOnly = true;
            if(requestData['country'].toLowerCase() !== 'armenia'){
                landOnly = false;
                airOnly ='<p><label>' +
                    '<input class="with-gap app-input"  checked="checked"  name="trans_type" type="radio" value="air" id="request-offer-trans-air"/>' +
                    '<span>Air</span></label>' +
                    '</p>';
            }
            var modalHtml = '<form class="submitable-form" id="make-request-offer-form">\
                                <input  name="request_id" type="hidden" class="app-input" value="' + requestData['request_id'] + '">\
                                <input  name="request_author" type="hidden" class="app-input" value="' + requestData['request_author'] + '">\
                                <input  name="place_id" id="hotel-place-id" type="hidden" class="app-input">\
                                <input  name="departure_airlines_iata" id="airlines-iata-code-departure" type="hidden" class="app-input">\
                                <input  name="arrival_airlines_iata" id="airlines-iata-code-arrival" type="hidden" class="app-input">\
                                <input  name="departure_departure_air_code" id="request-offer-departure-departure-airport-code" type="hidden" class="app-input">\
                                <input  name="departure_destination_arrival_air_code" id="request-offer-departure-destination-airport-code" type="hidden" class="app-input">\
                                <input  name="arrival_departure_air_code" id="request-offer-arrival-departure-airport-code" type="hidden" class="app-input">\
                                <input  name="arrival_destination_arrival_air_code" id="request-offer-arrival-destination-airport-code" type="hidden" class="app-input">'
                +( (typeof offerData != 'undefined' && offerData['offer_id']) ? '<input  name="offer_id" type="hidden" class="app-input" value="' + offerData['offer_id'] + '">':'')+
                '<div class="modal-content">\
                    <div>\
                        <span class="req-offers-count-info  ' + (_self.packageDetails.req_offers_left === 0 ? 'red-text text-lighten-1' : 'light-green-text text-darken-1') + '">Offers left: <span class="req-offers-count">' + _self.packageDetails.req_offers_left + '</span></span>\
                        <h5 class="make-offer-heading">Make offer </h5>\
                        <div class="divider"></div>\
                        <div class="section section-general">\
                            <h6 class="make-offer-section-header">General</h6>\
                            <div class="row">\
                                <div class="col s12 m8">\
                                    <div class="input-field ">\
                                        <input id="request-offer-price" name="price"  type="text" class="validate-required validate-positive-int app-input">\
                                        <label for="request-offer-price">Price (AMD)</label>\
                                    </div>\
                                    <div class="input-field ">\
                                        <input id="request-offer-deadline" name="offer_deadline" type="text" class="datepicker  validate-required app-input" readonly>\
                                        <label for="request-offer-deadline">Offer Deadline</label>\
                                    </div>\
                                </div>\
                                <div class="col s12 m4">\
                                    <div class="input-field transportation-field">\
                                        <p>Transportation Type</p>'
                                        + airOnly +
                                        '<p>\
                                            <label>\
                                                <input class="with-gap app-input" ' + (landOnly? "checked=\"checked\"": "" ) + '  name="trans_type" type="radio" value="land" id="request-offer-trans-land"/>\
                                            <span>Land</span>\
                                            </label>\
                                        </p>\
                                    </div>\
                                </div>\
                            </div>\
                            <h6 class="make-offer-section-header">Departure</h6>\
                            <div class="row air-only">\
                                <div class="col s12 m4 ">\
                                    <p>Flight Type</p>\
                                    <p>\
                                        <label>\
                                            <input checked class="with-gap" name="departure_flight_type" type="radio" value="direct" id="request-offer-departure-flight-dir"/>\
                                            <span for="request-offer-departure-flight-dir" >Direct</span>\
                                        </label>\
                                    </p>\
                                    <p>\
                                        <label>\
                                            <input  class="with-gap" name="departure_flight_type" value="transit" type="radio" id="request-offer-departure-flight-tr"/>\
                                            <span for="request-offer-departure-flight-tr" >Transit</span>\
                                        </label>\
                                    </p>\
                                </div>\
                                <div class="col s12 m8">\
                                    <div class="input-field">\
                                        <input id="request-offer-departure-departure-air-company" name="departure_departure_air_company" type="text" class="air-company-autocomplete departure app-input" autocomplete="off">\
                                        <label for="request-offer-departure-departure-air-company" >Departure Air Company</label>\
                                    </div>\
                                    <div class="aircompany-logo-container">\
                                        <img class="aircompany-logo departure" src=" ' + apetTempLogoUrl + ' "/>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="row">\
                                <div class="input-field col s12 m4 air-only">\
                                    <input id="request-offer-departure-departure-airport" data-country-scope="departure-departure-city" name="departure_departure_airport" type="text" class="airport-autocomplete app-input" autocomplete="off">\
                                    <label for="request-offer-departure-departure-airport" >Departure Airport</label>\
                                </div>\
                                <div class="input-field col s12 m4 land-only">\
                                    <input id="request-offer-departure-departure-city" name="departure_departure_city" type="text"    class="city-autocomplete validate-city app-input" autocomplete="off">\
                                    <label for="request-offer-departure-departure-city" >Departure City</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-departure-departure-date" name="departure_departure_date" type="text" class="datepicker validate-required validate-date app-input" readonly>\
                                    <label for="request-offer-departure-departure-date">Departure Date</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-departure-departure-time" name="departure_departure_time" type="text"  class="timepicker validate-time   app-input" readonly>\
                                    <label for="request-offer-departure-departure-time">Departure Time</label>\
                                </div>\
                            </div>\
                            <div class="row">\
                                <div class="input-field col s12 m4 air-only">\
                                    <input id="request-offer-departure-destination-airport" data-country-scope="departure-destination-arrival-city" name="departure_destination_arrival_airport" type="text"  class="airport-autocomplete app-input"  autocomplete="off">\
                                    <label for="request-offer-departure-destination-airport" >Destination Arrival Airport</label>\
                                </div>\
                                <div class="input-field col s12 m4 land-only">\
                                    <input id="request-offer-departure-destination-arrival-city" name="departure_destination_arrival_city" type="text"    class="city-autocomplete validate-city  app-input" autocomplete="off">\
                                    <label for="request-offer-departure-destination-arrival-city">Destination Arrival City</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-departure-destination-arrival-date" name="departure_destination_arrival_date" type="text"  data-date-compare="request-offer-departure-departure-date" class="datepicker validate-required validate-date app-input" readonly>\
                                    <label for="request-offer-departure-destination-arrival-date">Destination Arrival Date</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-departure-destination-arrival-time" name="departure_destination_arrival_time" type="text"  class="timepicker validate-time   app-input" data-date-compare="request-offer-departure-departure-time" readonly>\
                                    <label for="request-offer-departure-destination-arrival-time">Destination Arrival Time</label>\
                                </div>\
                            </div>\
                            <h6 class="make-offer-section-header">Arrival</h6>\
                            <div class="row air-only">\
                                <div class="col s12 m4">\
                                    <p>Flight Type</p>\
                                    <p>\
                                        <label>\
                                            <input checked class="with-gap" name="arrival_flight_type" type="radio" value="direct" id="request-offer-arrival-flight-dir"/>\
                                            <span for="request-offer-arrival-flight-dir" >Direct</span>\
                                        </label>\
                                    </p>\
                                    <p>\
                                        <label>\
                                            <input  class="with-gap" name="arrival_flight_type" value="transit" type="radio" id="request-offer-arrival-flight-tr"/>\
                                            <span for="request-offer-arrival-flight-tr" >Transit</span>\
                                        </label>\
                                    </p>\
                                </div>\
                                <div class="col s12 m8">\
                                    <div class="input-field ">\
                                        <input id="request-offer-arrival-departure-air-company" name="arrival_departure_air_company" type="text" class="air-company-autocomplete arrival   app-input" autocomplete="off">\
                                        <label for="request-offer-arrival-departure-air-company" >Departure Air Company</label>\
                                    </div>\
                                    <div class="aircompany-logo-container">\
                                        <img class="aircompany-logo arrival" src="' + apetTempLogoUrl + '"/>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="row ">\
                                <div class="input-field col s12 m4 air-only">\
                                    <input id="request-offer-arrival-departure-airport" data-country-scope="arrival-departure-city" name="arrival_departure_airport" type="text" class="airport-autocomplete  app-input" autocomplete="off">\
                                    <label for="request-offer-arrival-departure-airport" >Departure Airport</label>\
                                </div>\
                                <div class="input-field col s12 m4 land-only">\
                                    <input id="request-offer-arrival-departure-city" name="arrival_departure_city" type="text"     class="city-autocomplete validate-city app-input"  autocomplete="off">\
                                    <label for="request-offer-arrival-departure-city">Departure City</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-arrival-departure-date" name="arrival_departure_date" type="text" class="datepicker validate-required validate-date app-input" readonly>\
                                    <label for="request-offer-arrival-departure-date">Departure Date</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-arrival-departure-time" name="arrival_departure_time" type="text"  class="timepicker validate-time  app-input" readonly>\
                                    <label for="request-offer-arrival-departure-time">Departure Time</label>\
                                </div>\
                            </div>\
                            <div class="row ">\
                                <div class="input-field col s12 m4 air-only">\
                                    <input id="request-offer-arrival-destination-airport" data-country-scope="arrival-destination-city" name="arrival_destination_arrival_airport" type="text"  class="airport-autocomplete  app-input"  autocomplete="off">\
                                    <label for="request-offer-arrival-destination-airport" >Home Arrival Airport</label>\
                                </div>\
                                <div class="input-field col s12 m4 land-only">\
                                    <input id="request-offer-arrival-destination-city" name="arrival_destination_arrival_city" type="text"     class="city-autocomplete validate-city app-input"   autocomplete="off">\
                                    <label for="request-offer-arrival-destination-city">Home Arrival City</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-arrival-destination-arrival-date" name="arrival_destination_arrival_date" type="text"  data-date-compare="request-offer-arrival-departure-date" class="datepicker validate-required validate-date app-input" readonly>\
                                    <label for="request-offer-arrival-destination-arrival-date">Home Arrival Date</label>\
                                </div>\
                                <div class="input-field col s12 m4">\
                                    <input id="request-offer-arrival-destination-arrival-time" name="arrival_destination_arrival_time" type="text"   class="timepicker validate-time  app-input" data-date-compare="request-offer-arrival-departure-time" readonly>\
                                    <label for="request-offer-arrival-destination-arrival-time">Home Arrival Time</label>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="section section-hotel">\
                            <h6 class="make-offer-section-header">Hotel</h6>\
                            <div class="row">\
                                <div class="input-field col s12">\
                                    <input name="hotel" type="text" id="request-offer-hotel"  class="validate-required app-input hotel-autocomplete"  autocomplete="off">\
                                    <label for="request-offer-hotel" >Hotel Name</label>\
                                </div>\
                            </div>\
                        <div class="row">\
                            <div class="input-field col s12 l4">\
                                <select name="board_type" id="request-offer-food" class="app-input validate-select-required" >\
                                    <option value="" selected disabled>Meals included</option>\
                                    <option value="room-only" >Room Only</option>\
                                    <option value="breakfast">Breakfast</option>\
                                    <option value="half-board">Half Board</option>\
                                    <option value="full-board" >Full Board</option>\
                                    <option value="all-inclusive" >All Inclusive</option>\
                                </select>\
                                <label for="request-offer-food" >Board Type</label>\
                            </div>\
                            <div class="input-field col s12 l4">\
                                <select name="hotel_rating" id="request-offer-hotel-rating" class="app-input validate-select-required" >\
                                    <option value="" selected disabled>Choose Hotel Rating</option>\
                                    <option value="0" >n/a</option>\
                                    <option value="1" >1 star</option>\
                                    <option value="2">2 star</option>\
                                    <option value="3">3 star</option>\
                                    <option value="4" >4 star</option>\
                                    <option value="5" >5 star</option>\
                                </select>\
                                <label for="request-offer-hotel-rating" >Hotel Rating</label>\
                            </div>\
                            <div class="input-field col s12 l4">\
                                <input name="number_of_rooms" type="text" id="request-offer-hotel-number-of-rooms" class="validate-required validate-positive-int app-input">\
                                <label for="request-offer-hotel-number-of-rooms" >Number of Rooms</label>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="input-field col s12 m6">\
                                <input name="hotel_checkin" type="text" id="request-offer-hotel-in" class="datepicker validate-required validate-date app-input" autocomplete="off" readonly>\
                                <label for="request-offer-hotel-in" >Hotel Check-in</label>\
                            </div>\
                            <div class="input-field col s12 m6">\
                                <input name="hotel_checkout" type="text" id="request-offer-hotel-out" data-date-compare="request-offer-hotel-in" class="datepicker validate-required validate-date app-input" autocomplete="off" readonly>\
                                <label for="request-offer-hotel-out" >Hotel Check-out</label>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="section section-notes">\
                        <div class="row">\
                            <div class="input-field col s12">\
                                <textarea id="request-offer-notes" name="notes" class="materialize-textarea app-input" autocomplete="off"></textarea >\
                                <label for="request-offer-notes" >Notes</label>\
                            </div>\
                        </div>\
                    </div>\
                    </div>\
                </div>\
                <div class="modal-footer">\
                    <a href="javascript:void(0);" class="modal-action view-request waves-effect waves-green btn-flat left grey lighten-2" >View Request</a>\
                    <a href="javascript:void(0);" class="modal-action cancel-offer waves-effect waves-red btn-flat ">Cancel</a>'
                +( (typeof offerData != 'undefined' && offerData['offer_id']) ? '<button  class="waves-effect modal-action-btn submit-request-offer-btn waves-light darken-1 light-green btn" type="submit" data-action="edit-offer">Save</button>':'<button  class="waves-effect modal-action-btn submit-request-offer-btn waves-light darken-1 light-green btn" type="submit" data-action="make-offer">Make Offer</button>')+
                '</div>\
        </form>';
            _self.createModal(_self, modalHtml, 'make-offer');
            if(_self.packageDetails.req_offers_left === -1){
                $('#make-request-offer-form .req-offers-count-info').hide();
            }else{
                $('#make-request-offer-form .req-offers-count-info').show();
            }



            if(typeof offerData !== 'undefined'){
                $('#make-request-offer-form .req-offers-count-info').hide();
                $.each(offerData, function(key,value){
                    if(key =='doc_list' && value){
                        $('.request-offer-doc-container').empty();
                        var selectHtml = "";
                        doc_list = value;
                        for (var i = 1; i <= doc_list.length; i++) {
                            if (i === 1) {
                                selectHtml += '<div data-doc-index="1" class="row custom-doc-row required">' +
                                    '<div class="input-field col s12">' +
                                    ' <input value="' + doc_list[0] + '" id="request-offer-doc1" type="text"  name="document1" class="validate-required request-offer-doc app-input">' +
                                    ' <label for="request-offer-doc1">Document 1</label>' +
                                    ' </div>' +
                                    '</div>';
                                continue;
                            }
                            selectHtml += '<div data-doc-index="' + i + '" class="row custom-doc-row">' +
                                '<div class="input-field col s11">' +
                                '<input value="' + doc_list[i - 1] + '" id="request-offer-doc' + i + '" type="text" name="document' + i + '" class="validate-required request-offer-doc app-input">' +
                                '<label for="request-offer-doc1" class="active">Document ' + i + '</label>' +
                                ' </div>' +
                                '<div class="col s1">' +
                                '<a class="btn-floating waves-light waves-effect delete-request-doc red lighten-1"><i class="material-icons">close</i></a>' +
                                ' </div>' +
                                '</div>';
                        }
                        $('.request-offer-doc-container').append(selectHtml);
                    }else if(typeof value != 'object'){

                        var inputType =  $('#make-request-offer-form [name="'+key+'"]').attr('type');
                        if(inputType == 'radio'){
                            $('#make-request-offer-form [name="'+key+'"][value="'+value+'"]').prop('checked',true);
                        }else{
                            var input =  $('#make-request-offer-form [name="'+key+'"]');
                            if(input.hasClass('city-autocomplete')){
                                input.val(value).attr('data-value',value);
                            }else{
                                input.val(value);
                            }

                        }

                    }
                });

                if(offerData['trans_type']){

                    if(offerData['trans_type'] ==='air'){
                        var departureSrc,
                            arrivalSrc,
                            departureIata = offerData['departure_airlines_iata'],
                            arrivalIata = offerData['arrival_airlines_iata'];

                        departureSrc = 'https://pics.avs.io/130/65/'+departureIata+'.png';
                        $('.aircompany-logo.departure').attr('src',departureSrc);

                        arrivalSrc = 'https://pics.avs.io/130/65/'+arrivalIata+'.png';
                        $('.aircompany-logo.arrival').attr('src',arrivalSrc);

                        $('#request-offer-trans-air').trigger('click',[ "edit" ]);
                    }else{
                        $('#request-offer-trans-land').trigger('click',[ "edit"]);
                    }
                }
                $('#make-request-offer-form .cancel-offer').removeClass('cancel-offer').addClass('modal-close');
                M.updateTextFields()
            }else{
                if(landOnly){
                    $('#request-offer-trans-land').trigger('click');
                }else{
                    $('#request-offer-trans-air').trigger('click');
                }
            }
            //========== Initilizing MaterializeCSS Method========

            $('select').formSelect();
            $('.collapsible').collapsible();

            var maxDate = new Date(requestData['period_to']+ " 00:00:00");
            var nightsFrom = +requestData['nights'] -3;
            maxDate.setDate(maxDate.getDate() - parseInt(nightsFrom));
            var toDay = new Date();
            var periodFrom  = new Date(requestData['period_from']+ " 00:00:00");
            periodFrom.setDate(periodFrom.getDate() - 5);

            $('.datepicker').datepicker({
                yearRange: 2,
                showClearBtn:true,
                minDate: ( _self.validations['compareDates'](  periodFrom, toDay) ?  periodFrom : toDay  ),
                maxDate: maxDate,
                defaultDate: ( _self.validations['compareDates'](  periodFrom, toDay)  ?  periodFrom : toDay  ),
                autoClose:false,
                container: 'body',
                firstDay: 1,
                onClose: function () {
                    var _this = $($(this)[0].$el);
                    var _compareEl,thisDate,compareDate,thisTimeInput,compareTimeInput,picker,min,max;

                    if(_this.val().trim() === '') return;
                    var dataCompare = _this.attr('data-date-compare');
                    var id = _this.attr('id');

                    if (dataCompare) {
                        _compareEl = $('#' + dataCompare);
                        var res1 = id.split('-');
                        var res2 = _compareEl.attr('id').split('-');
                        if(_compareEl.val() !== ''){
                            if (_self.validations['compareDates'](_this.val(),_compareEl.val())) {
                                _this.removeClass('invalid').addClass('valid');

                                thisTimeInput = $('#request-offer-' + res1[2] + '-' + res1[3] + '-' + res1[4] + '-time');
                                compareTimeInput = $('#request-offer-' + res2[2] + '-' + res2[3] + '-time');

                                if(thisTimeInput.val() !== '' &&  compareTimeInput.val() !== ''){

                                    thisDate = _this.val() + ' ' + thisTimeInput.val() + ':00';
                                    compareDate = _compareEl.val() + ' ' + compareTimeInput.val() + ':00';

                                    if (_self.validations['compareDates'](thisDate, compareDate)) {
                                        thisTimeInput.removeClass('invalid').addClass('valid');
                                    }else{
                                        thisTimeInput.removeClass('valid').addClass('invalid');
                                    }
                                }


                            } else {
                                _this.removeClass('valid').addClass('invalid');
                            }
                        }

                        if(res1[2] !== 'arrival' && id !== 'request-offer-hotel-in' && id !== 'request-offer-hotel-out'){

                            var arrivalDate1 = $('#request-offer-arrival-departure-date');
                            var arrivalDate2 = $('#request-offer-arrival-destination-arrival-date');


                            var pick1 = getPicker(arrivalDate1),
                                 pick2 = getPicker(arrivalDate2);

                            var nightFrom  = +requestData["nights"] - 3;
                            var nightTo  =  +requestData["nights"] + 4;
                            min = new Date(_this.val().trim()+ " 00:00:00");
                            min.setDate(min.getDate() + parseInt(nightFrom));


                            max = new Date(_this.val().trim()+ " 00:00:00");
                            max.setDate(max.getDate() + parseInt(nightTo));

                            if(_self.validations['compareDates'](max,requestData['period_to'])){
                                max = new Date(requestData['period_to']+ " 00:00:00");
                                max.setDate(max.getDate() + 5);
                            }

                            pick1.options.minDate = min;
                            pick1.options.maxDate = max;
                            pick1.gotoDate(min);

                            pick2.options.minDate = min;
                            pick2.options.maxDate = max;
                            pick2.gotoDate(min);
                            if(arrivalDate1.val().trim()!=='' && _self.validations['compareDates'](min,arrivalDate1.val())){
                                arrivalDate1.val('')
                            }
                            if(arrivalDate2.val().trim()!=='' && _self.validations['compareDates'](min,arrivalDate2.val())){
                                arrivalDate2.val('');
                            }
                        }

                        picker = getPicker(_compareEl);
                        picker.options.maxDate = new Date(_this.val()+ " 00:00:00");

                    } else if ($('[data-date-compare="' + id + '"]').length) {
                        _compareEl = $('[data-date-compare="' + id + '"]');
                        if(_compareEl.val().trim() !== '') {

                            if (_self.validations['compareDates'](_compareEl.val(), _this.val())) {
                                _compareEl.removeClass('invalid').addClass('valid');

                                res1 = id.split('-');
                                res2 = _compareEl.attr('id').split('-');

                                thisTimeInput = $('#request-offer-' + res1[2] + '-' + res1[3] + '-time');
                                compareTimeInput = $('#request-offer-' + res2[2] + '-' + res2[3] + '-' + res2[4] + '-time');

                                if(thisTimeInput.val() !== '' &&  compareTimeInput.val() !== ''){

                                    thisDate = _this.val() + ' ' + thisTimeInput.val() + ':00';
                                    compareDate = _compareEl.val() + ' ' + compareTimeInput.val() + ':00';

                                    if (_self.validations['compareDates'](compareDate, thisDate)) {
                                        compareTimeInput.removeClass('invalid').addClass('valid');
                                    }else{
                                        compareTimeInput.removeClass('valid').addClass('invalid');
                                    }
                                }


                            } else {
                                _compareEl.removeClass('valid').addClass('invalid');
                            }
                        }
                        picker = getPicker(_compareEl);
                        picker.options.minDate = new Date(_this.val()+ " 00:00:00");
                        picker.gotoDate(new Date(_this.val()+ " 00:00:00"));
                    }
                    var picker1,picker2;

                    if(id === 'request-offer-departure-destination-arrival-date' ){

                        picker1 = getPicker($('#request-offer-hotel-in').val(''));
                        picker2 = getPicker($('#request-offer-hotel-out').val(''));

                        picker1.options.minDate = new Date(_this.val()+ " 00:00:00");
                        picker1.gotoDate( new Date(_this.val()+ " 00:00:00"));
                        picker2.options.minDate =  new Date(_this.val()+ " 00:00:00");
                        picker1.gotoDate( new Date(_this.val()+ " 00:00:00"));
                    }

                    if(id === 'request-offer-arrival-departure-date'){

                        picker1 = getPicker($('#request-offer-hotel-in').val(''));
                        picker2 = getPicker($('#request-offer-hotel-out').val(''));

                        picker1.options.maxDate =  new Date(_this.val()+ " 00:00:00");
                        picker2.options.maxDate = new Date(_this.val()+ " 00:00:00");
                    }
                    M.updateTextFields()
                },
                format: 'yyyy-mm-dd',
            });

            var deadlinePicker = getPicker($('#request-offer-deadline'));
            deadlinePicker.options.maxDate = new Date(requestData['period_from']+ " 00:00:00");
            deadlinePicker.options.minDate = toDay;
            deadlinePicker.gotoDate(toDay);

            // getPicker($('#request-offer-arrival-departure-date')).options.maxDate = new Date(maxDate.setDate(maxDate.getDate() + parseInt(nightsFrom) +5));
            // getPicker($('#request-offer-arrival-destination-arrival-date')).options.maxDate = new Date( maxDate);
            $('.timepicker').each(function(index,element){
                var _this = $(element);
                _this.timepicker({
                    defaultTime: 'now', // Set default time: 'now', '1:30AM', '16:30'
                    container: 'body',
                    twelveHour: false,
                    showClearBtn:true,
                    onCloseStart: function() {

                        var _compareEl,thisDate,compareDate,_compareElId,
                            dataCompare = _this.attr('data-date-compare'),
                            id = _this.attr('id');

                        if (dataCompare) {
                            _compareEl = $('#' + dataCompare);
                            var res1 =  id.split('-');
                            var res2 =  dataCompare.split('-');

                            thisDate = $('#request-offer-' + res1[2] + '-' + res1[3] + '-' + res1[4] + '-date').val().trim();
                            compareDate = $('#request-offer-' + res2[2] + '-' + res2[3] + '-date').val().trim();

                            if(thisDate !=='' && compareDate !== ''){

                                thisDate += ' '+ _this.val()+ ':00';
                                compareDate += ' '+ _compareEl.val() + ':00';
                                if (_self.validations['compareDates'](thisDate,compareDate)) {
                                    _this.removeClass('invalid').addClass('valid');
                                } else {
                                    _this.removeClass('valid').addClass('invalid');
                                }
                            }
                        } else if ($('[data-date-compare="' + id + '"]').length) {
                            _compareEl = $('[data-date-compare="' + id + '"]');
                            _compareElId = _compareEl.attr('id');

                            if(_compareEl.val().trim() !== '') {

                                res1 =  id.split('-');
                                res2 = _compareElId.split('-');
                                thisDate = $('#request-offer-' + res1[2] + '-' + res1[3] + '-date').val().trim();
                                compareDate = $('#request-offer-' + res2[2] + '-' + res2[3] + '-' + res2[4] + '-date').val().trim();
                                thisDate += ' ' + _this.val() + ':00';
                                compareDate += ' ' + _compareEl.val() + ':00';
                                if (_self.validations['compareDates'](compareDate, thisDate)) {
                                    _compareEl.removeClass('invalid').addClass('valid');
                                } else {
                                    _compareEl.removeClass('valid').addClass('invalid');
                                }
                            }else{
                                _this.removeClass('invalid').addClass('valid');
                            }
                        }

                    },
                    onCloseEnd: function(){
                        $('input.timepicker').blur()
                    }
                });
            });

            if(typeof offerData !== 'undefined'){

                getPicker($('#request-offer-departure-departure-date')).options.maxDate = new Date(offerData['departure_destination_arrival_date']+" 00:00:00");

                getPicker($('#request-offer-departure-destination-arrival-date')).options.minDate = new Date(offerData['departure_departure_date']+" 00:00:00");

                //Add min-nights
                var min = new Date(offerData['departure_destination_arrival_date']+" 00:00:00");
                min.setDate(min.getDate() + parseInt(nightsFrom));

                getPicker($('#request-offer-arrival-departure-date')).options.minDate = min;

                getPicker($('#request-offer-arrival-destination-arrival-date')).options.minDate = new Date(offerData['arrival_departure_date']+" 00:00:00");

                getPicker($('#request-offer-hotel-in')).options.minDate = new Date(offerData['departure_destination_arrival_date']+" 00:00:00");
                getPicker($('#request-offer-hotel-in')).options.maxDate = new Date(offerData['arrival_departure_date']+" 00:00:00");

                getPicker($('#request-offer-hotel-out')).options.minDate =  new Date(offerData['hotel_checkin']+" 00:00:00");
                getPicker($('#request-offer-hotel-out')).options.maxDate =  new Date(offerData['arrival_departure_date']+" 00:00:00");

            }
            //====================================================

            $('.airport-autocomplete').autocomplete({data: {}});
            //========== Google AutoCompletes Init ===============

            var citiesAutoInputs = $('.city-autocomplete');

            citiesAutoInputs.each(function(index){

                var _this = this;

                var thatId = $(_this).attr('id');

                var options = {
                    types: ['(cities)']
                };
                if(typeof _self.autocomplete['city'] === 'undefined'){
                    _self.autocomplete['city'] = {};
                }
                _self.autocomplete['city'][thatId] = new google.maps.places.Autocomplete(_this, options);
                _self.autocomplete['city'][thatId].addListener('place_changed', placeChanged);
                // FIX Placeholder BUG
                setTimeout(function(){
                    $(_this).removeAttr('placeholder');
                },300)
                function placeChanged() {
                    if($(_this).val().trim().indexOf(' ') > -1 ){
                        var place = _self.autocomplete['city'][thatId].getPlace();
                        $(_this).attr('data-value',place.name);
                    }
                    $(_this).trigger('change');
                }

            });

            var hotelAutoInputs = $('.hotel-autocomplete');

            var _this = hotelAutoInputs.get(0);
            var options = {
                types: ['establishment']
            };
            _self.autocomplete['hotel'] = new google.maps.places.Autocomplete(_this, options);

            _self.autocompleteLsr['hotel'] = _self.autocomplete['hotel'].addListener('place_changed', placeChanged);
            // FIX Placeholder BUG
            setTimeout(function(){
                $(_this).removeAttr('placeholder');
            },300)
            function placeChanged() {
                var place = _self.autocomplete['hotel'].getPlace();

                if(place.types.indexOf('lodging') === -1) {
                    $(_this).val("");
                    $('#hotel-place-id').val('');
                    _self.errorToast('Please Select Hotel.');
                }else{
                    var placeId = place.place_id;
                    $('#hotel-place-id').val(placeId);
                }
                $(_this).trigger('change');

            }
            M.updateTextFields()
        },
        drawReqOfferBlock: function (_self, offerData) {

            // if (offerData['doc_list'] && typeof offerData['doc_list'] == 'string') {
            //     offerData['doc_list'] = JSON.parse(offerData['doc_list']);
            // }
            // var docList = offerData['doc_list'];

            var docListHtml = '';


            // if (offerData['doc_list'] && offerData['doc_list'].length) {
            //
            //     docListHtml += '<tr><th>Document List</th><td class="capitalize">';
            //     for (var i = 0; i < docList.length; i++) {
            //         docListHtml += docList[i] + '<br>'
            //     }
            //     docListHtml += '</td></tr>'
            // }
            var airportHtml = "";
            if (typeof offerData['arrival'] === 'string' && offerData['arrival']) {
                airportHtml = '<tr>\
                                    <th>Flight Type</th>\
                                    <td class="capitalize">' + offerData["flight_type"] + '</td>\
                                </tr>\
                                <tr>\
                                    <th>Arrival</th>\
                                    <td>' + offerData["arrival"] + '</td>\
                                </tr>\
                                <tr>\
                                    <th>Departure</th>\
                                    <td>' + offerData["departure"] + '</td>\
                                </tr>';
            }else if(offerData['trans_type']){
                var depAirRows ='', depAirRowDestAirport ='', arrivalAirRows='', arrivAirRowDestAirport = '';
                if(offerData['trans_type'] ==='air'){
                    depAirRows = '\
                    <tr>\
                        <th>Departure Flight Type</th>\
                        <td class="capitalize">' + offerData['departure_flight_type']+ '</td>\
                    </tr>\
                    <tr>\
                        <th>Departure Air Company</th>\
                        <td class="capitalize">' + offerData['departure_departure_air_company']+ '</td>\
                    </tr>\
                    <tr>\
                        <th>Departure Airport</th>\
                        <td class="capitalize">' + offerData['departure_departure_airport']+ '</td>\
                    </tr>';

                    depAirRowDestAirport ='\
                        <tr>\
                            <th>Destination Arrival Airport</th>\
                            <td class="capitalize">' + offerData['departure_destination_arrival_airport']+ '</td>\
                        </tr>';

                    arrivalAirRows = '\
                    <tr>\
                        <th>Departure Flight Type</th>\
                        <td class="capitalize">' + offerData['arrival_flight_type']+ '</td>\
                    </tr>\
                    <tr>\
                        <th>Departure Air Company</th>\
                        <td class="capitalize">' + offerData['arrival_departure_air_company']+ '</td>\
                    </tr>\
                    <tr>\
                        <th>Departure Airport</th>\
                        <td class="capitalize">' + offerData['arrival_departure_airport']+ '</td>\
                    </tr>\
                    <tr>';

                    arrivAirRowDestAirport ='\
                        <tr>\
                            <th>Destination Arrival Airport</th>\
                            <td class="capitalize">' + offerData['arrival_destination_arrival_airport']+ '</td>\
                        </tr>';

                }


                airportHtml = '\
                            <tr>\
                                <th colspan="2" class="capitalize center-align"><strong>Departure</strong></th>\
                            </tr>\
                                ' + depAirRows +
                            '<tr>\
                                <th>Departure City</th>\
                                    <td class="capitalize">' + offerData['departure_departure_city']+ '</td>\
                            </tr>\
                            <tr>\
                                <th>Departure Date/Time</th>\
                                <td class="capitalize">' + offerData['departure_departure_date']+ ' ' + offerData['departure_departure_time'] +'</td>\
                            </tr>\
                            ' + depAirRowDestAirport + '\
                            <tr>\
                                <th>Destination Arrival City</th>\
                                <td class="capitalize">' + offerData['departure_destination_arrival_city']+ '</td>\
                            </tr>\
                            <tr>\
                                <th>Destination Arrival  Date/Time</th>\
                                <td class="capitalize">' + offerData['departure_destination_arrival_date'] + ' ' + offerData['departure_destination_arrival_time'] +'</td>\
                            </tr>\
                            <tr>\
                                <th colspan="2"  class="capitalize center-align"><strong>Arrival</strong></h5>\
                            </tr>\
                            ' + arrivalAirRows + '\
                            <tr>\
                                <th>Departure City</th>\
                                <td class="capitalize">' + offerData['arrival_departure_city']+ '</td>\
                            </tr>\
                            <tr>\
                                <th>Departure Date/Time</th>\
                                <td class="capitalize">' + offerData['arrival_departure_date']+ ' ' + offerData['arrival_departure_time'] +'</td>\
                            </tr>\
                            ' + arrivAirRowDestAirport + '\
                            <tr>\
                                <th>Destination Arrival City</th>\
                                <td class="capitalize">' + offerData['arrival_destination_arrival_city']+ '</td>\
                            </tr>\
                            <tr>\
                                <th>Destination Arrival  Date/Time</th>\
                                <td class="capitalize">' + offerData['arrival_destination_arrival_date'] + ' ' + offerData['arrival_destination_arrival_time'] +'</td>\
                            </tr>';
            }

            var editBtnHtml =  '';
            if(offerData['offer_status'] === 'active' && _self.tourId !== 'all'){
                editBtnHtml = '<tr class="tr-no-border edit-offer-btn-tr">\
                                    <th colspan="2" style="text-align:right">\
                                    <a data-requst-id="'+offerData["request_id"]+'" data-offer-id="'+offerData["offer_id"]+'" class="waves-effect edit-request-offer-btn waves-light darken-1 light-green btn">Edit Offer</a>\
                                    </th>\
                                </tr>'
            }

            var days = parseInt(offerData["nights"]) + 1;

            var agencyOfferHtml ='';

            var boardType = offerData['board_type'];
            var boardTypeRow = "";

            if(typeof boardType === 'object' && boardType && boardType.length){

                boardTypeRow += '<tr><th>Board Type</th><td class="capitalize">';
                for (var i = 0; i < boardType.length; i++) {
                    boardTypeRow += _self.labels[boardType[i]] + '<br>'
                }
                boardTypeRow += '</td></tr>'
            }else{
                boardTypeRow = '<tr>' +
                    '              <th>Board Type</th>' +
                    '              <td class="capitalize">' + (offerData["board_type"] ? _self.labels[offerData["board_type"]] : 'Not Specified') + '</td>' +
                    '           </tr>'
            }
            var nightRow = '<tr>' +
                '                            <th>Nights</th>' +
                '                            <td>' + (offerData['nights'] ? offerData['nights'] +'(Days -'+days+')' : Math.floor(((new Date(offerData['hotel_checkout']) - new Date(offerData['hotel_checkin'])) / 3600 / 24 / 1000) )  + ' Nights' )+ '</td>' +
                '                        </tr>';

            agencyOfferHtml =
                '<table class="bordered my-offer-table box-shadow" data-offer-id="'+  offerData["offer_id"] + '">\
                        <tbody>'+
                            (offerData['tour_agent'] && _self.role === 'tour-super-admin' ? "<tr><th>Tour Agency</th><td>" + offerData['tour_agent'] +"</td> </tr>": "" )
                            +'<tr>\
                                <th>Price</th>\
                                <td>' + offerData["price"] + ' AMD</td>\
                            </tr>'
                + nightRow +
                '<tr>\
                    <th>Hotel</th>\
                    <td class="capitalize">' + offerData["hotel"] + '</td>\
                </tr>\
                <tr>\
                    <th>Hotel Checkin</th>\
                    <td>' + offerData["hotel_checkin"] + '</td>\
                </tr>\
                <tr>\
                    <th>Hotel Checkout</th>\
                    <td>' + offerData["hotel_checkout"] + '</td>\
                </tr>'
                + boardTypeRow +
                '<tr>\
                   <th>Notes</th>\
                   <td>' + (_self.helpers.urlify(offerData["notes"]) )+ '</td>\
                </tr>'
                + docListHtml
                + airportHtml
                + editBtnHtml+
                '</tbody>\
                 </table>';
            return agencyOfferHtml;
        },
        deleteDocList: function (_self, docIndex) {


            _self.sendAjax({
                data: {
                    user_id: _self.userId,
                    tour_id: _self.tourId,
                    doc_index: docIndex,
                    action: 'apet_delete_doc_template'
                },
                success: function (response) {
                    if (response.success === "true") {

                        _self.docTemplates = response.data['doc_list'];
                        $('#doc-template-select').empty();
                        var docList = "<option value='' disabled selected>Choose Sample</option>";
                        if (_self.docTemplates.length) {
                            for (var i = 0; i < _self.docTemplates.length; i++) {
                                docList += '<option value="' + i + '">' + _self.docTemplates[i]['template_name'] + '</option>';
                            }
                        }
                        $('#doc-template-select').append(docList);

                        $('#popup-modal .custom-doc-row:not(.required)').remove();
                        $('#popup-modal .custom-doc-row.required input').val('');
                        $('.delete-doc-list').addClass('hide');
                        $('#doc-template-select').formSelect();
                        M.updateTextFields()
                        $('#doc-sample-name').val('');
                        _self.successToast('Document template deleted.');
                    } else {
                        _self.errorToast('Something went wrong.');
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }


                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }
            })
        },
        saveDocTemplate: function (_self, templateData) {
            templateData['user_id'] = _self.userId;
            templateData['tour_id'] = _self.tourId;
            templateData['action'] = 'apet_save_doc_template';
            _self.sendAjax({
                data: templateData,
                beforeSend: function () {
                    $('a.save-doc-sample').addClass('disabled');
                },
                complete: function () {
                    $('a.save-doc-sample').removeClass('disabled');
                },
                success: function (response) {
                    if (response.success === "true") {

                        _self.docTemplates[response['id']] = response.data;
                        $('select.doc-template-select').append('<option  value="' + response['id'] + '">' + response.data['template_name'] + '</option>');
                        $('select').formSelect();
                        M.updateTextFields()
                        $('.doc-sample-collapsible').collapsible('close', 0);
                        $('#doc-sample-name').val('');
                        _self.successToast('Document template saved.');
                    } else {
                        _self.errorToast('Something went wrong.');
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }


                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }
            })


        },
        makeOfferSubmit: function (offerData, _self) {
            var tourData = _self.agencyDetails;
            var doc_list = [];
            $.each(offerData, function (key, value) {
                if (key.indexOf('document') > -1) {
                    doc_list.push(value);
                    delete offerData[key];
                }
            });
            if(offerData['place_id'] === ''){

                $('#popup-modal #request-offer-hotel').removeClass('valid').addClass('invalid');
                _self.errorToast('Please Select Hotel From Suggestion List');
                return false;

            }

            offerData['doc_list'] = doc_list.length ? JSON.stringify(doc_list) : '';
            offerData['user_id'] = _self.userId;
            offerData['tour_id'] = _self.tourId;
            offerData['tour_agent'] = tourData && tourData['tour_agent'] ? tourData['tour_agent']:'';
            offerData['action'] = offerData['action']? offerData['action'] : 'apet_make_offer_to_request';
            _self.sendAjax({
                data: offerData,
                loader: true,
                success: function (response) {
                    if (response.success === "true") {

                        offerData['offer_id'] = response.data['offer_id'];
                        offerData['offer_status'] = 'active';
                        offerData['seen_status'] = 'new';
                        if(response.data['offer_creation_date']){
                            offerData['offer_creation_date'] = response.data['offer_creation_date'];
                        }

                        if(typeof _self.offersStorage[offerData['offer_id']] === 'undefined'){
                            _self.offersStorage[offerData['offer_id']] = {};
                        }
                        $.extend(_self.offersStorage[offerData['offer_id']], offerData);
                        var reqOfferHtml = _self.drawReqOfferBlock(_self, _self.offersStorage[offerData['offer_id']]);
                        var reqBlock = $('.single-request[data-requst-id="' + offerData['request_id'] + '"] .collapsible-single-requst');

                        if(offerData['action'] === 'apet_make_offer_to_request'){

                            /* append offerd badge */
                            var reqRow = $('.single-request[data-requst-id="' + offerData['request_id'] + '"] ');
                            var statusColumn = reqRow.find('.collapsible-header table th.status-column');
                            if( reqRow.attr('data-status') !== 'offered'){
                                statusColumn.html('<span class="new badge light-blue lighten-1" data-badge-caption="Offered"></span>');
                                reqRow.attr('data-status','offered');
                                reqBlock.find('.single-request-table tr td.request-status').html('<span class="new badge light-blue lighten-1" data-badge-caption="Offered"></span>');
                            }

                            /* append new offer */
                            if(reqBlock.find(' .offers-accordion').length === 0){
                                reqBlock.append('<h5 class="center">My Offers</h5>');
                                reqBlock.append('<ul class="collapsible offers-accordion" data-collapsible="accordion"></ul>');
                            }

                            var agencyOfferHtml ='<li>' +
                                '<div class="collapsible-header">' +
                                    '<table>' +
                                        '<tr>' +
                                            '<th>Offer: <strong>#' + offerData["offer_id"] +'</strong></th>' +
                                            '<td>Offer date: '+ offerData["offer_creation_date"] +'</td>' +
                                            '<td>Offer deadline: '+ offerData["offer_deadline"] +'</td>' +
                                            '<td>Offer Status: '+ offerData["offer_status"] +'</td>' +
                                            '<td><a class="view-request collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a></td>' +
                                        '</tr>' +
                                    '</table>' +
                                '</div>' +
                                '<div class="collapsible-body">';
                            agencyOfferHtml += reqOfferHtml;
                            agencyOfferHtml += '</div></li>';

                            reqBlock.find('.offers-accordion').append(agencyOfferHtml).collapsible();

                            _self.packageDetails.req_offers_left = parseInt(response.data['offer_left']);
                            var reqOffersLeft = _self.packageDetails.req_offers_left;

                            _self.updateReqOffersCounter(reqOffersLeft);
                            if(reqOffersLeft !== -1){
                                _self.successToast('Success. Offers left: ' +reqOffersLeft);
                            }else{
                                _self.successToast('Success');
                            }
                        }else{
                            /* replace my previos offer */
                            reqBlock.find('.my-offer-table[data-offer-id="'+ offerData["offer_id"] +'"]').replaceWith(reqOfferHtml);
                            // $('.single-request[data-requst-id="' + offerData['request_id'] + '"] .collapsible-single-requst .my-offer-table[data-offer-id="'+ offerData["offer_id"] +'"]').remove();
                            _self.successToast('Success');
                        }

                        // remove make offer button
                        // commented for multiple offer
                        // $('.single-request[data-requst-id="' + offerData['request_id'] + '"] .collapsible-single-requst .make-offer-btn-tr').remove();

                        $('#popup-modal').modal('close');
                    } else {
                        if (response.errorMessage === 'offers_max_limit_reached') {
                            _self.errorToast('Could not create offer.');
                            _self.errorToast('You have exceeded the request offers limit.');
                            _self.packageDetails.req_offers_left = 0;
                            _self.updateSpecOffersCounter(_self.packageDetails.req_offers_left);
                            return false;

                        }
                        _self.errorToast('Something went wrong.');
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }
            })

        },
        settingPageEvents: function (_self) {

            _self.settingsUploader = new ss.SimpleUpload({
                button: $('.tour-img-file').get(0), // file upload button
                url: _self.ajaxUrl,
                name: 'file',
                form: 'settings-form',
                responseType: 'json',
                overrideSubmit: false,
                allowedExtensions: ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG'],
                maxSize: 2048, // kilobytes
                onChange: function (filename, extension, uploadBtn, fileSize, file) {
                    if (_self.validations.allowedFileSize(fileSize) == false) {
                        _self.errorToast('Max file size is 2MB.');
                        return false;
                    }
                    var allowedType = ['jpg', 'jpeg', 'png'];
                    var imageType = extension.toLowerCase();
                    if (($.inArray(imageType, allowedType) > -1)) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#settings .tour-agency-img').attr('src', e.target.result);
                            $('#settings  .tour-agency-img.placeholder-image').removeClass('placeholder-image');
                            $('#settings .file-path').val(filename).removeClass('invalid');
                            _self.settingPageEvents.photoChanged = true;
                        };
                        reader.readAsDataURL(file);
                    } else {
                        _self.errorToast('Allowed types jpeg or png.');
                    }
                },
                onSubmit: function (filename, extension) {
                    $('.loader-layer').show();
                },
                onComplete: function (filename, response) {
                    $('.loader-layer').hide();
                    $.extend(_self.settingsUploader._opts.data, response.data);
                    _self.settingsSaveOnComplete(response, _self.settingsUploader._opts.data);
                }, onError: function (response) {
                    $('.loader-layer').hide();
                    _self.errorToast('Something went wrong.');
                }
            });

            //========= validating single input =========
            /*$('#settings  input').on('input change', function () {
                _self.validateInputs($(this));
            }); */
            //========== changing password ==========
            $('#settings-password-form .collapsible input').on('input', function () {

                if ($(this).val() !== "") {
                    $(this).closest('.form-container').find('.submit').removeClass('disabled')
                }
                _self.validateInputs($(this));
                if ($(this).val() === "") {
                    $(this).removeClass('valid invalid');
                }
            });
            $('#settings-retype-password').on('input', function () {
                if ($('#settings-new-password').val() !== '' && $('#settings-new-password').hasClass('valid')) {
                    if ($(this).val() === $('#settings-new-password').val()) {
                        $(this).removeClass('invalid').addClass('valid')
                    } else {
                        $(this).removeClass('valid').addClass('invalid')
                    }
                }
            });
        },
        fillSettings: function (_self) {
            $('#settings-form')[0].reset();
            var tourData = _self.agencyDetails;
            var prefCountries = "<option value=''  disabled>Choose Prefered Countries</option>";
            $.each(countriesList, function (index, value) {
                prefCountries += "<option value='" + index + "'>" + _self.helpers.firstToUpperCase(index) + "</option>";
            });

            $('#settings-pref-countries').html(prefCountries);
            if (tourData['status'] !== 'inactive') {

                $.each(tourData, function (key, val) {
                    if (key === "photo") {
                        if (!val)
                            return true;
                        var index = val.lastIndexOf("/") + 1;
                        var filename = val.substr(index);
                        $('#settings .tour-agency-img').attr('src', val + '?#' + (new Date().getTime()));
                        $('#settings  .tour-agency-img.placeholder-image').removeClass('placeholder-image');
                        $('#settings .file-path').val(filename);
                        return true;
                    }
                    $('#settings .app-input[name="' + key + '"]').val(val);
                });
            }
            $('#settings-pref-countries').formSelect();
            M.updateTextFields()
        },
        settingsSubmit: function (inputsData, _self) {

            inputsData["action"] = "apet_change_tour_settings";
            inputsData["user_id"] = _self.userId;
            inputsData["tour_id"] = _self.tourId;
            if (_self.settingPageEvents.photoChanged != true) {
                _self.sendAjax({
                    data: inputsData,
                    success: function (response) {
                        _self.settingsSaveOnComplete(response, inputsData);
                    },
                    error: function (error) {
                        _self.errorToast('Something went wrong.');
                    }, loader: true
                })
            } else {
                _self.settingsUploader._opts.data = inputsData;
                _self.settingsUploader.submit()
            }

        },
        settingsSaveOnComplete: function (response, inputsData) {
            var _self = this;
            if (response.success === "true") {
                inputsData['prefered_countries'] = inputsData['prefered_countries'] ? JSON.parse(inputsData['prefered_countries']) : null;
                $.extend(_self.agencyDetails, inputsData);
                bloch = _self.agencyDetails;
                if (response.data.status === 'inactive') {
                    _self.agencyActivated();
                } else {
                    _self.fillMenuHeaderHtml(_self);
                    _self.successToast('Settings saved.');
                }
            } else {
                _self.errorToast('Failed to save settings.');
                if (response.errorMessage == 'img_upload_failed') {
                    if (_self.agencyDetails && _self.agencyDetails.photo) {
                        $('#settings .tour-agency-img').attr('src', _self.agencyDetails.photo);
                    } else {
                        $('#settings .tour-agency-img').attr('src', apetTempLogoUrl).addClass('placeholder-image');
                        $('#settings .file-path').val('');

                    }
                } else if (response.errorMessage === 'unauthorize_access') {
                    _self.logout(_self);
                }
            }
            _self.settingPageEvents.photoChanged = false;
        },
        changePassword: function (data, _self) {

            _self.sendAjax({
                data: {
                    "action": "apet_change_tour_admin_password",
                    "current-password": data['current-password'],
                    "new-password": data['new-password'],
                    "user_id": _self.userId,
                    "tour_id": _self.tourId
                }, success: function (response) {
                    if (response.success === "true") {
                        $('#settings-password-form')[0].reset();
                        $('.collapsible').collapsible('close', 0);
                        _self.successToast('Password changed.');
                    } else {

                        _self.errorToast('Failed to change password.');
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }

                }, error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            })

        },
        addUserBonus: function(data, finalStep){
            var _self = this;

            if(typeof finalStep == 'undefined'){
                finalStep = false;
            }
            if(finalStep == false) {
                $('.popup-bonus-amount').html(data['bonus_amount']);
                $('.popup-bonus-email').html(data['user_email']);
                $('#add-user-bonus-popup').modal('open');

                $('#add-user-bonus-popup .modal-action.yes').off('click.addbonus').on('click.addbonus',function(){
                    _self.addUserBonus(data, true);
                });
                return false;
            }

            data['action'] = 'apet_add_user_bonus';
            data['user_id'] = _self.userId;

            _self.sendAjax({
                data: data,
                success: function (response) {
                    if (response.success === "true") {
                        _self.successToast('Success');

                        $('#add-user-bonus-form')[0].reset();
                        M.updateTextFields()

                    } else if(response.errorMessage === 'wrong_user_email') {
                        _self.errorToast('Wrong app user email.');
                    }else{
                        _self.errorToast('Something went wrong.');
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            })
        },
        checkBonusDetails: function (data) {
            var _self = this;


            data['action'] = 'apet_get_user_from_bonus_id';
            data['user_id'] = _self.userId;


            _self.sendAjax({
                data: data,
                success: function (response) {
                    if (response.success === "true") {
                        var userBonusDetailsHtml = '<table class="bordered">\
                                                        <caption>User Info</caption>\
                                                        <tbody>\
                                                        <tr>\
                                                           <th>Bonus amount</th>\
                                                           <td>' + response.data.bonus_amount + '</td>\
                                                         </tr>\
                                                         <tr>\
                                                           <th>First Name</th>\
                                                           <td class="capitalize">' + response.data.firstname + '</td>\
                                                         </tr>\
                                                         <tr>\
                                                           <th>Last Name</th>\
                                                           <td class="capitalize">' + response.data.lastname + '</td>\
                                                         </tr>\
                                                         <tr>\
                                                           <th>Phone</th>\
                                                           <td>' + response.data.phone + '</td>\
                                                         </tr>\
                                                         <tr>\
                                                           <th>Email</th>\
                                                           <td>' + response.data.email + '</td>\
                                                         </tr>\
                                                         </tbody>\
                                                    </table>';
                        $('#bonus-details .bonus-user-info').html(userBonusDetailsHtml);
                    } else {
                        $('#bonus-details .bonus-user-info').html('<span class="invalid-bonus-id red-text text-lighten-1">Invalid Bonus ID</span>');
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            })
        },
        getTourAgencies: function (firstLoad) {
            var _self = this;

            firstLoad = (typeof firstLoad == 'undefined') ? false : firstLoad;

            if(firstLoad){
                delete  _self.getTourAgencies.filterOptions;
                delete _self.getTourAgencies.offset;
                $('#tour-agencies-filter-form')[0].reset();
                M.updateTextFields()
            }

            var loader = false;
            if(typeof _self.getTourAgencies.offset == 'undefined'){
                _self.getTourAgencies.offset = 0;
                loader = true;
            }

            $('html').addClass('loading-start');

            var data = {
                'action': 'apet_get_admin_tour_agencies',
                'user_id': _self.userId,
                'tour_id': _self.tourId,
                'limit': _self.limit,
                'filter': JSON.stringify({'status':'active'}),
                'offset': _self.getTourAgencies.offset
            };

            if(_self.getTourAgencies.filterOptions){
                data['filter'] = JSON.stringify(_self.getTourAgencies.filterOptions);
            }

            var userId = _self.userId;
            var tourId = _self.tourId;
            _self.sendAjax({
                data: data,
                beforeSend:function(){
                    if( _self.getTourAgencies.offset == 0){
                        $('.tour-agencies-container').empty();
                        $('.agencies-empty-msg').show();
                    }
                },
                complete: function(){
                    $('html').removeClass('loading-start')
                },
                success: function (response) {
                    if (response.success === "true") {
                        _self.drawTourAgencies(_self, response.data);
                        _self.getTourAgencies.offset+= response.data.length;
                        _self.agencyStorage = {};
                        _self.getTourAgency.agencyLoaded = {};
                    } else {
                        if (response.errorMessage === 'no_agencies' && _self.getTourAgencies.offset == 0) {
                            $('.agencies-empty-msg').show();

                        }
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: loader
            })


        },
        drawTourAgencies: function (_self, data) {
            if(data.length == 0){
                return false;
            }

            if(!$('.tour-agencies-container .agencies-table').length){
                $('.tour-agencies-container').append('<ul class="collapsible agencies-table" data-collapsible="accordion"></ul>');
            }

            var agenciesHtml = '';
            $.each(data, function (key, agencyData) {
                agenciesHtml += _self.addAgencyBlock(agencyData, _self);
            });
            $('.tour-agencies-container .agencies-table').append(agenciesHtml);
            $('.collapsible').collapsible();
        },
        addAgencyBlock: function (agencyData, _self) {
            $('.agencies-empty-msg').hide();
            var statusBadge = '';
            var packageTitle = agencyData['package_plan_name'] ? apetPackagePlans[agencyData['package_plan_name']].title : "Not selected";
            if (agencyData['status'] === 'inactive') {
                statusBadge = '<span data-text=" " class="new badge grey lighten-1 deactivated" data-badge-caption="Pending"></span>'
            } else if (agencyData['status'] === 'deactivated') {
                statusBadge = '<span data-text=" " class="new badge grey lighten-1 deactivated" data-badge-caption="Deactivated"></span>';
            }
            var blockHtml = '<li  data-tour-id="' + agencyData["id"] + '"   class="single-agency ' + agencyData['status'] + '-agency">\
                                <div class="collapsible-header">\
                                    <table>\
                                        <tbody>\
                                        <tr>\
                                            <th >' + (agencyData["tour_agent"] ? agencyData["tour_agent"] : 'Not Set') + '</th>\
                                            <th >Date: ' + (agencyData["tour_creation_date"] ? agencyData["tour_creation_date"] : 'Not Set' ) + '</th>\
                                            <th class="agency-plan-name-th">' + packageTitle + '</th>\
                                            <th class="agency-status-th">' + statusBadge + '</th>\
                                            <td class="agency-view-btn">\
                                                <a class="collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a>\
                                            </td>\
                                        </tr>\
                                        </tbody>\
                                    </table>\
                                </div>\
                                <div class="collapsible-body collapsible-single-agency grey lighten-3">\
                                    <div class = "progress" >\
                                        <div class = "indeterminate" ></div>\
                                    </div>\
                                </div>\
                            </li>';
            return blockHtml;
        },
        drawAgencyInnerBlock: function (_self, agencyData) {

            var agencySwitchBtn = "";
            if (agencyData['status'] === 'active') {
                agencySwitchBtn = '<tr class="tr-no-border make-offer-btn-tr">' +
                    '<th colspan="2" style="text-align:right"><a data-tour-id="' + agencyData["id"] + '" class="deactivate-tour-agency agency-switch-btn  btn-floating waves-effect waves-light  btn red lighten-1" ><i class="material-icons">pause_circle_outline</i></a></th>' +
                    ' </tr>';
            } else if (agencyData['status'] === 'deactivated') {
                agencySwitchBtn = '<tr class="tr-no-border make-offer-btn-tr">' +
                    '<th colspan="2" style="text-align:right"><a data-tour-id="' + agencyData["id"] + '" class="activate-tour-agency agency-switch-btn btn-floating waves-effect waves-light  btn darken-1 light-green" ><i class="material-icons">play_circle_outline</i></a></th>' +
                    ' </tr>';
            }
            var preferedCountries = 'All';
            if (agencyData['prefered_countries'] && agencyData['prefered_countries'].length) {
                var preferedCountries = '';
                for (var i = 0; i < agencyData['prefered_countries'].length; i++) {
                    preferedCountries += _self.helpers.firstToUpperCase(agencyData['prefered_countries'][i]) + '<br>'
                }
            }

            var selectedPackageHtml='<div class="input-field col s12 m7"><select name="package" class="app-input selected-tour-package">';

            if(!agencyData['package_plan_name']){
                selectedPackageHtml+='<option  value="" disabled selected  >Select Plan</option>';
            }

            $.each(apetPackagePlans,function(key,value){

                var selected = agencyData['package_plan'].title === value.title && agencyData['package_plan_name'] ? "selected" : "";
                selectedPackageHtml+='<option value="'+key+'"  '+selected+' >'+value.title+'</option>';

            });


            selectedPackageHtml +='</select></div>';

            var blockHtml = '<table class="bordered single-agency-table">\
                                   <caption>Tour ID   #' + agencyData["id"] + '</caption>\
                                   <tbody>\
                                      <tr>\
                                         <th>Status</th>\
                                         <td class="status capitalize">' + agencyData["status"] + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Creation Date</th>\
                                         <td class="status capitalize">' + agencyData["tour_creation_date"] + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Name</th>\
                                         <td class="capitalize">' + (agencyData["tour_agent"] ? agencyData["tour_agent"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Country</th>\
                                         <td class="capitalize">' + (agencyData["country"] ? agencyData["country"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>City</th>\
                                         <td class="capitalize">' + (agencyData["city"] ? agencyData["city"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Address</th>\
                                         <td class="capitalize">' + (agencyData["address"] ? agencyData["address"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Phone</th>\
                                         <td>' + (agencyData["phone"] ? agencyData["phone"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Photo</th>\
                                         <td class="table-photo-container" >\
                                             <div class="photo-frame">\
                                                        <img src="' + (agencyData["photo"] ? agencyData["photo"] : apetTempLogoUrl) + '" class="booked-traveler-photo materialboxed right" />\
                                              </div>\
                                         </td>\
                                      </tr>\
                                      <tr>\
                                         <th>Email</th>\
                                         <td>' + (agencyData["email"] ? agencyData["email"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Website</th>\
                                         <td>' + (agencyData["website"] ? agencyData["website"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Average Rate</th>\
                                         <td>' + (agencyData["average_rate"] ? agencyData["average_rate"] : 'Not Set') + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Prefered Countries</th>\
                                         <td class="capitalize">' + preferedCountries + '</td>\
                                      </tr>\
                                      <tr>\
                                         <th>Tour Package</th>\
                                         <td>\
                                         <div class="change-package-plan row">\
                                         ' + selectedPackageHtml +'\
                                            <div class="input-field col s12 m5">\
                                                <button class="btn waves-effect waves-light darken-1 light-green package-plan-btn" type="submit">Save</button>\
                                            </div>\
                                        </td>\
                                      </tr>'
                + agencySwitchBtn +
                '</tbody>\
                          </table>';
            $('#tour-agencies li[data-tour-id="' + agencyData["id"] + '"] .collapsible-single-agency').append(blockHtml);
            $('#tour-agencies .materialboxed').materialbox();
            $('#tour-agencies select').formSelect();
        },
        getTourAgency: function (_self, agencyData) {
            if (_self.getTourAgency.agencyLoaded[agencyData['tour_id']] !== true) {
                agencyData['user_id'] = _self.userId;
                agencyData['action'] = "apet_get_admin_tour_agency";
                _self.sendAjax({
                    data: agencyData,
                    success: function (response) {
                        if (response.success === "true") {

                            _self.drawAgencyInnerBlock(_self, response.data);
                            _self.agencyStorage[response.data['id']] = response.data;
                            _self.getTourAgency.agencyLoaded[response.data['id']] = true;
                        } else {
                            if (response.errorMessage === 'fetching_agency_failed') {
                                _self.errorToast('Something went wrong.');
                            }
                        }
                    },
                    error: function (error) {
                        _self.errorToast('Something went wrong.');
                    },
                    complete: function () {
                        $('#tour-agencies li[data-tour-id="' + agencyData['tour_id'] + '"] .progress').hide();
                    }
                })
            } else {
                if ($('[data-tour-id="' + agencyData['tour_id'] + '"] .single-agency-table').length === 0) {
                    _self.drawAgencyInnerBlock(_self, _self.agencyStorage[agencyData['tour_id']]);
                    $('#tour-agencies li[data-tour-id="' + agencyData['tour_id'] + '"] .progress').hide();

                }
            }
        },
        createAgencyModal: function (_self) {

            var selectPackageHtml='<select name="tour-package" class="app-input" id="tour-package-plan" disabled>';

            // after WEB_1.0.55 all agencies plan must be unlimited;
            // $.each(apetPackagePlans,function(key,value){
            //
            //     selectPackageHtml+='<option value="'+key+'" '+(key === "free_plan" ? "selected": "")+'  >'+value.title+'</option>';
            //
            // });
            selectPackageHtml += '<option value="unlimited_plan" selected >'+apetPackagePlans['unlimited_plan'].title+'</option>';
            selectPackageHtml +='</select>';


            var modalHtml = '<form class="submitable-form" id="create-agency-form">\
                                <div class="modal-content">\
                                    <div>\
                                        <h5 class="make-offer-heading">Add Tour Agency</h5>\
                                        <div class="clear-float"></div>\
                                        <div class="row">\
                                            <div class="input-field col s12">\
                                                <input id="tour-admin-email" name="admin-email" type="text" class="validate-email validate-required app-input">\
                                                <label for="tour-admin-email">Tour Admin Email</label>\
                                            </div>\
                                        </div>\
                                        <div class="row">\
                                            <div class="input-field col s12">\
                                                '+selectPackageHtml+'\
                                                <label for="tour-package-plan">Tour Package Plan</label>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="modal-footer">\
                                    <a href="javascript:void(0);" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>\
                                    <button class="waves-effect modal-action-btn submit-add-agency-btn waves-light light-green darken-1 btn" type="submit" data-action="create-agency">Add Tour Agency</button>\
                                </div>\
                            </form>';

            _self.createModal(_self, modalHtml, 'create-agency');
        },
        createAgencySubmit: function (agencyData, _self) {

            agencyData['action'] = 'apet_create_tour_agency';
            agencyData['user_id'] = _self.userId;
            if(typeof agencyData['tour-package'] === 'undefined'){
                agencyData['tour-package'] = 'unlimited_plan';
            }
            _self.sendAjax({
                data: agencyData,
                loader: true,
                success: function (response) {
                    if (response.success === "true") {


                        if(_self.getTourAgencies.filterOptions && _self.getTourAgencies.filterOptions.status == 'pending'){

                            var newAgencyData = {
                                id: response.data.tour_id,
                                tour_agent: null,
                                address: null,
                                status: 'inactive',
                                //agency_plan: response.data.package_plan
                            };
                            var agencyHtml = _self.addAgencyBlock(newAgencyData, _self);

                            _self.getTourAgencies.offset++;
                            if ($('.tour-agencies-container .agencies-table').length === 0) {
                                agencyHtml = '<ul class="collapsible agencies-table" data-collapsible="accordion">' + agencyHtml + '</ul>'
                                $('.tour-agencies-container').append(agencyHtml);
                            } else {
                                $('.tour-agencies-container .agencies-table').prepend(agencyHtml);
                            }

                        }


                        $('#popup-modal').modal('close');
                        _self.successToast('Success');
                    } else {
                        if (response.errorMessage === 'email_exists') {
                            _self.errorToast('Email address already registred.');
                        } else {
                            _self.errorToast('Something went wrong.');
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }
            })

        },
        deactivateAgency: function (_self, data) {

            var agencyData = {
                'tour_id': data.tour_id,
                'user_id': _self.userId,
                'action': 'apet_deactivate_agency'
            }
            _self.sendAjax({
                data: agencyData,
                success: function (response) {
                    if (response.success === "true") {
                        _self.redrawAgency(_self, data.tour_id, 'deactivate');
                        _self.successToast('Tour agency deactivated.');
                    } else {
                        if (response.errorMessage) {
                            _self.errorToast('Something went wrong.');
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            })


        },
        activateAgency: function (_self, data) {

            var agencyData = {
                'tour_id': data.tour_id,
                'user_id': _self.userId,
                'action': 'apet_activate_agency'
            }
            _self.sendAjax({
                data: agencyData,
                success: function (response) {
                    if (response.success === "true") {

                        _self.redrawAgency(_self, data.tour_id, 'activate');
                        _self.successToast('Tour agency activated.');
                    } else {
                        if (response.errorMessage) {
                            _self.errorToast('Something went wrong.');
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }, loader: true
            })


        },
        redrawAgency: function (_self, tourId, mode) {

            $('.single-agency[data-tour-id="' + tourId + '"]').remove();
            _self.getTourAgencies.offset--;
            /*   switch (mode) {
                   case 'deactivate' :
                       $('.single-agency.active-agency').last().after($('.single-agency[data-tour-id="' + tourId + '"]'));
                       $('.single-agency[data-tour-id="' + tourId + '"]').closest('.collapsible').collapsible('close', $('.single-agency[data-tour-id="' + tourId + '"]').index());
                       $('.single-agency[data-tour-id="' + tourId + '"]').removeClass('active-agency').addClass('deactivated-agency');
                       $('.single-agency[data-tour-id="' + tourId + '"] .make-offer-btn-tr .material-icons').text('play_circle_outline');
                       $('.single-agency[data-tour-id="' + tourId + '"] .status').text('deactivated');
                       $('.single-agency[data-tour-id="' + tourId + '"] .make-offer-btn-tr .deactivate-tour-agency').removeClass('deactivate-tour-agency red lighten-1').addClass('activate-tour-agency darken-1 light-green');
                       $('.single-agency[data-tour-id="' + tourId + '"] .agency-status-th').prepend('<span data-text=" " class="new badge  grey lighten-1 deactivated" data-badge-caption="Deactivated"></span>');
                       break;
                   case 'activate' :
                       $('.single-agency.active-agency').last().after($('.single-agency[data-tour-id="' + tourId + '"]'));
                       $('.single-agency[data-tour-id="' + tourId + '"] .agency-status-th span.deactivated').remove();
                       $('.single-agency[data-tour-id="' + tourId + '"]').removeClass('deactivated-agency').addClass('active-agency');
                       $('.single-agency[data-tour-id="' + tourId + '"]').closest('.collapsible').collapsible('close', $('.single-agency[data-tour-id="' + tourId + '"]').index());
                       $('.single-agency[data-tour-id="' + tourId + '"] .make-offer-btn-tr .material-icons').text('pause_circle_outline');
                       $('.single-agency[data-tour-id="' + tourId + '"] .status').text('active');
                       $('.single-agency[data-tour-id="' + tourId + '"] .make-offer-btn-tr .activate-tour-agency').removeClass('activate-tour-agency darken-1 light-green').addClass('deactivate-tour-agency red lighten-1');
                       break;
               }
   */
        },
        tourAgenciesPageEvents: function (_self) {
            _self.getTourAgency.agencyLoaded = {};
            _self.agencyStorage = {};

            $('#tour-agencies')
                .on('click', '.single-agency', function () {
                    var data = {
                        'tour_id': $(this).attr('data-tour-id')
                    };
                    _self.getTourAgency(_self, data);
                })
                .on('click', '.add-tour-agency', function () {
                    _self.createAgencyModal(_self);
                })
                .on('click', '.agency-switch-btn', function (e) {
                    e.preventDefault();
                    var tourId = $(this).attr('data-tour-id');
                    $('#deactivate-agency-modal').data({'tour_id': tourId})
                    if ($(this).hasClass('deactivate-tour-agency')) {

                        $('#deactivate-agency-modal').modal('open');
                    }
                    else if ($(this).hasClass('activate-tour-agency')) {

                        $('#deactivate-agency-modal h4 span').text('Activate');
                        $('#deactivate-agency-modal p').text('Are you sure you want activate Tour Agency ');
                        $('#deactivate-agency-modal .yes').addClass('activate');
                        $('#deactivate-agency-modal').modal('open');
                    }
                })
                .on('click', '.package-plan-btn', function (e) {
                    e.preventDefault();
                    var tourId = $(this).closest('.single-agency').attr('data-tour-id');

                    var scope = $(this).parent().siblings('.input-field').find('select.selected-tour-package');

                    var selectedPackage = scope.val();

                    if(!selectedPackage) return false;

                    scope.find('option[value=""]').remove();
                    scope.formSelect();

                    _self.editAgencyPackage(_self, {tour_id: tourId, agency_package: selectedPackage.trim()}, this);
                });

            $('#tour-agencies select').formSelect();

            $('#tour-agencies .tour-agencies-load-more').on('click',function(){
                if( $('html').hasClass('loading-start')){
                    return true;
                }
                _self.getTourAgencies();
            });

            $('#reset-tour-agencies-filter').on('click',function(){
                _self.resetTourAgenciesFilter();
            });
            $('#deactivate-agency-modal .yes').on('click', function () {
                if ($(this).hasClass('activate')) {
                    var data = {
                        'tour_id': $('#deactivate-agency-modal').data('tour_id'),
                    }
                    _self.activateAgency(_self, data);
                    $(this).removeClass('activate');
                } else {
                    var data = {
                        'tour_id': $('#deactivate-agency-modal').data('tour_id'),
                    }
                    _self.deactivateAgency(_self, data);
                }
            })

        },
        editAgencyPackage: function (_self, data, button) {

            if(!data.agency_package)
                return false;
            _self.sendAjax({
                data: {
                    'tour_id': data.tour_id,
                    'user_id': _self.userId,
                    'agency_package': data.agency_package,
                    'action': 'apet_edit_agency_package'
                },
                success: function (response) {
                    if (response.success === "true") {
                        var packageTitle = apetPackagePlans[data.agency_package].title;
                        $(button).closest('.single-agency').find('.agency-plan-name-th').html(packageTitle);
                        _self.successToast('Package plan changed.');
                    } else {
                        if (response.errorMessage) {
                            _self.errorToast('Something went wrong.');
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                },
                loader: true
            })

        },
        resetPassword: function (data, _self) {

            _self.sendAjax({
                data: {
                    "action": "apet_app_reset_password",
                    "email": data.email,
                    "user-type": "admin-user"
                },
                success: function (response) {
                    if (response.success === "true") {
                        _self.successToast('Please check your email to set new password.');
                        $('#login').addClass('active-login');
                    } else {
                        _self.errorToast('Failed to reset password.');
                    }

                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            });
        },
        loginEvents: function (_self) {
            $('#reset-password-btn').on('click', function () {
                $('#login').removeClass('active-login');
                $('.login-title').html('Reset Password');
            });
            $('#reset-password-cancel').on('click', function () {
                $('#login').addClass('active-login');
            });
        },
        loginSubmit: function (inputsValue, _self) {
            inputsValue['action'] = 'apet_admin_login';
            _self.sendAjax({
                data: inputsValue,
                success: function (response) {
                    if (response.success === "true") {
                        if (response.data.roles[0] === 'administrator') {
                            window.location.replace("/wp-admin/");
                        } else {

                            if(window.location.search.indexOf('request_id') > -1){
                                window.location.replace("/dashboard"+window.location.search+"#requests-page");
                            }else{
                                window.location.replace("/dashboard");
                            }
                        }


                    } else {
                        if (response.errorMessage === 'tour_agency_deactivated') {
                            _self.errorToast('Tour Agency Deactivated.');
                        } else {
                            _self.errorToast('Wrong Username.');
                        }


                    }

                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            })
        },
        logout: function (_self) {
            _self.sendAjax({
                data: {
                    'action': 'apet_admin_user_log_out'
                },
                success: function (response) {
                    if (response.success === "true") {
                        window.location.replace("/login/");
                    } else {
                        _self.errorToast('Something went wrong.');
                    }

                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            })
        },
        dashboardEvents: function (_self) {

            $('.log-out').on('click', function () {
                _self.logout(_self);

            })

        },
        checkRequests: function (_self) {
            var userId = _self.userId;
            var tourId = _self.tourId;

            function check() {
                if(typeof  _self.getRequests.lastUpdateTime  == 'undefined'){
                    return false;
                }
                _self.sendAjax({
                    data: {
                        'action': 'apet_get_users_requests',
                        'user_id': userId,
                        'tour_id': tourId,
                        'get_updates': _self.getRequests.lastUpdateTime

                    },
                    success: function (response) {

                        if (response.success === "true") {
                            _self.handleUpdatedRequests(response);
                            if(tourId !== 'all'){
                                _self.handleUpdatedPlan(response.extra)
                            }
                        } else if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    },
                    error: function (error) {
                        _self.errorToast('Something went wrong.');
                    }
                })
            }

             check();
             setInterval(check, Number(apetRequestsCheckInterval))
        },
        updateSpecOffersCounter: function(count){
            if(count === -1 ){
                $('.spec-offers-count-info').hide();
            }else{
                $('.spec-offers-count').html(count);
                if(count === 0){
                    $('.spec-offers-count-info').removeClass(' light-green darken-1 text-darken-1');
                    $('.spec-offers-count-info').addClass('red-text text-lighten-1');
                }else{
                    $('.spec-offers-count-info').removeClass('red-text text-lighten-1');
                    $('.spec-offers-count-info').addClass(' light-green darken-1 text-darken-1');
                }
                $('.spec-offers-count-info').show();
            }
        },
        updateReqOffersCounter: function(count){
            if(count === -1 ){
                $('.req-offers-count-info').hide();
            }else{
                $('.req-offers-count').html(count);
                if(count === 0){
                    $('.req-offers-count-info').removeClass(' light-green darken-1 text-darken-1');
                    $('.req-offers-count-info').addClass('red-text text-lighten-1');
                }else{
                    $('.req-offers-count-info').removeClass('red-text text-lighten-1');
                    $('.req-offers-count-info').addClass(' light-green darken-1 text-darken-1');
                }
                $('.req-offers-count-info').show();
            }
        },
        handleUpdatedPlan: function (responseData){

            var _self = this;
            var newAgencyDetails = responseData['package_plan'];
            var agencyDetailsNow = _self.agencyDetails['package_plan'];

            var newSpecOffers = responseData['spec_offers_left'];
            var newReqOffers = responseData['req_offers_left'];

            if(newSpecOffers !== _self.packageDetails.spec_offers_left){
                _self.packageDetails.spec_offers_left = newSpecOffers;
                _self.updateSpecOffersCounter(newSpecOffers)
            }

            if(newReqOffers !== _self.packageDetails.req_offers_left){
                _self.packageDetails.req_offers_left = newReqOffers;
                _self.updateReqOffersCounter(newReqOffers)
            }


            if(agencyDetailsNow.title !== newAgencyDetails.title){

                _self.agencyDetails['package_plan'] = newAgencyDetails;
                var specLeft = _self.packageDetails['spec_offers_left'] = responseData['spec_offers_left'];
                var reqLeft = _self.packageDetails['req_offers_left'] = responseData['req_offers_left'];


                /* Update Special Offers Counter */
                _self.updateSpecOffersCounter(specLeft);

                /* Update Request Offers Counter */
                _self.updateReqOffersCounter(reqLeft);

                /* Update SideMenu */

                var packagePlanBlock=newAgencyDetails.title;

                if(packagePlanBlock !== "Premium Plan" && packagePlanBlock !== "Unlimited Plan"){
                    packagePlanBlock+='<a href="https://easytraveling.am/#pricing" target="_blank" class="upgrade-plan">upgrade</a>';
                }
                $('.sidenav  .nav-package-plan').html(packagePlanBlock);

            }

        },
        handleUpdatedRequests: function (requests) {

            var _self = this;

            if(_self.helpers.compareVersions(apetWebVersion,requests.extra.web_min_version) === -1){
                location.reload(true);
            }
            _self.getRequests.lastUpdateTime = requests.extra.utc_time;

            var filterMode = false;
            if(typeof _self.getRequests.filterOptions !== 'undefined' || app.currentPage !== 'requests'){
                filterMode = true;
            }

            /* new requests */
            var newRequests = requests.data.new;
            if (newRequests && newRequests.length) {
                _self.addNewRequests(newRequests, filterMode);
            }

            if(requests.data.del && requests.data.del.length){
                _self.removeRequests(requests.data.del);
            }

            var updatedRequests = requests.data.updated;


            if(updatedRequests && updatedRequests.length){
                _self.handleChangedRequests(updatedRequests, filterMode);
            }
        },
        handleChangedRequests: function (updatedRequests ,filterMode) {
            var _self = this;
            var newBookedCount = 0;

            updatedRequests.forEach(function(request){

                if(typeof filterMode === 'undefined'){
                    filterMode = false;
                }

                if(request.status === 'pre-booked'){

                    newBookedCount++;
                    delete _self.getRequest.requestLoaded[request['request_id']];
                    _self.getRequests.newTrackedBookedRequests = _self.getRequests.newTrackedBookedRequests || {};


                    _self.getRequests.newTrackedBookedRequests[request['request_id']] = true;
                    if(!filterMode){
                        var bookedRequest = $('#requests li.single-request[data-requst-id="' + request['request_id'] + '"]');
                        request['pre-booked'] = true;
                        var bookedRequestHtml = _self.addRequstBlock(request, _self);
                        bookedRequest.replaceWith(bookedRequestHtml);
                    }
                    /* new notifications badge */
                    newBookedCount += _self.getNotificationBadgeNumber('pre-booked');
                    _self.setNotificationBadgeNumber('pre-booked',newBookedCount);

                    if (newBookedCount) {
                        var message  = '',title='';
                        if(newBookedCount === 1){
                            title = 'New Pre-Book';
                            message = (_self.tourId === 'all' ? 'We': 'You')+ ' have 1 new pre-book.';
                        }else{
                            title = 'New Pre-Books';
                            message = (_self.tourId === 'all' ? 'We': 'You')+ ' have ' + newBookedCount + ' new pre-books.';
                        }
                        _self.notification({
                            'title': title,
                            'message': message,
                            'count': newBookedCount
                        });
                    }
                }
                else {

                    if(!filterMode) {
                        var existRequest = $('#requests li.single-request[data-requst-id=' + request['request_id'] + ']');
                        var opened = existRequest.hasClass('active');
                        if(existRequest.attr('data-status') !== request.status){
                            delete _self.getRequest.requestLoaded[request['request_id']];
                            var changedRequestHtml = _self.addRequstBlock(request, _self);
                            existRequest.replaceWith(changedRequestHtml);
                            if(opened){
                                var index = $(".single-request[data-requst-id="+request['request_id']+"]").index();
                                $('#requests .request-table.collapsible').collapsible('open',index);
                                $('#requests li.single-request[data-requst-id="' + request['request_id'] + '"]').click();
                            }

                        }
                    }
                }
            });

        },
        removeRequests: function (deletedRequests) {
            var _self = this;
            var newReqCount = _self.getNotificationBadgeNumber('requests');
            for (var i = 0; i < deletedRequests.length; i++) {
                var removeableRequest = $('#requests li.single-request[data-requst-id="' + deletedRequests[i]['request_id']+ '"]');

                if(removeableRequest.hasClass('new-single-request')){
                    newReqCount--;
                }
                removeableRequest.remove();

                /* cleanup request Loader tracker*/
                delete _self.getRequest.requestLoaded[deletedRequests[i]['request_id']];
            }
            _self.setNotificationBadgeNumber('requests',newReqCount);


        },
        notification:function(options){
            var _self = this;

            if(typeof options == 'string'){
                options = {
                    'title':options
                }
            }

            var defaultOptions = {
                'title':'New Notification',
                'audio':true
            };

            options  = $.extend(defaultOptions, options);



            /* browser notifications */
            if(window.Notification) {
                Notification.requestPermission().then(function (result) {
                    if(result == 'granted'){

                        if(_self.arrayNotifications){
                            for(var i = 0; i < _self.arrayNotifications.length; i++){
                                _self.arrayNotifications[i].close();
                            }
                            delete _self.arrayNotifications;
                        }


                        var newNotificatiion = new Notification(options.title, {
                            'body': (options.message ? options.message : ''),
                            'icon':_self.notificationIcon

                        });
                        newNotificatiion.onclick=function() {
                            _self.goToPage('requests');
                            window.focus();
                        };

                        _self.arrayNotifications = [];
                        _self.arrayNotifications.push(newNotificatiion);

                        setTimeout(newNotificatiion.close.bind(newNotificatiion), 5000);
                    }
                });
            }

            /* default notifications */
            _self.updateDocumentTitle(options.title);
            if(options.audio) {
                var playAudio = new Audio(apetRequestAlertSoundUrl);
                playAudio.play();
            }

        },
        addNewRequests: function (newRequests, filterMode) {
            var _self  = this;

            if(typeof filterMode == 'undefined'){
                filterMode = false;
            }

            var newRequestBlock = "";
            var newRequestsCount = newRequests.length;


            for (var i = 0; i < newRequestsCount; i++) {
                _self.getRequests.newTrackedRequests = _self.getRequests.newTrackedRequests || {};
                _self.getRequests.newTrackedRequests[newRequests[i]['request_id']] = true;

                if(!filterMode){
                    newRequests[i]['new'] = true;

                    if(_self.getRequest.requestLoaded[newRequests[i]['request_id']]){
                        $('#requests li.single-request[data-requst-id="' + request['request_id'] + '"]').remove();
                        delete _self.getRequest.requestLoaded[newRequests[i]['request_id']];
                    }
                    newRequestBlock += _self.addRequstBlock(newRequests[i], _self);
                }
            }


            if (!filterMode) {
                /* draw container if not exist */
                if (!$('#requests .request-table').length) {
                    $('#requests .requests-container').html('<ul class="collapsible request-table" data-collapsible="accordion"></ul>');
                    $('.collapsible').collapsible();
                }

                $('#requests .request-table').prepend(newRequestBlock);
            }

            _self.getRequests.offset =  typeof _self.getRequests.offset == 'undefined' ?  newRequestsCount : (_self.getRequests.offset +  newRequestsCount);


            /* new notifications badge */
            newRequestsCount += _self.getNotificationBadgeNumber('requests');
            _self.setNotificationBadgeNumber('requests',newRequestsCount);


            if (newRequestsCount) {
                var message  = '',title='';
                if(newRequestsCount == 1){
                    title = 'New Request';
                    message = (_self.tourId === 'all' ? 'We': 'You') + ' have 1 new request.';
                }else{
                    title = 'New Requests';
                    message = (_self.tourId === 'all' ? 'We': 'You') +' have ' + newRequestsCount + ' new requests.';
                }
                _self.notification({
                    'title': title,
                    'message': message,
                    'count': newRequestsCount
                });
            }
        },
        setNotificationBadgeNumber: function(type, count){
            var _self = this;
            // new requests part
            if(type === 'requests') {

                if(count <= 0){
                    $('#new-request-badge').remove();
                    return;
                }

                if (!$('#new-request-badge').length) {
                    $('.notification-badge-container ').append('<span id="new-request-badge" data-destination="requests" class="new badge red darken-1 new-request-message"></span>');
                    $('#new-request-badge').on('click',function(){
                        _self.resetFilter();
                        _self.goToPage($(this).data('destination'));
                    });
                }

                //update counts
                $('#new-request-badge').attr('data-badge-caption', (count == 1 ? '1 New Request' :  (count + ' New Requests')) ).data('count', count);
            }else if(type === 'pre-booked'){
                // do book stuff here
                if(count <= 0){
                    $('#new-book-badge').remove();
                    return;
                }


                if (!$('#new-book-badge').length) {
                    $('.notification-badge-container ').append('<span id="new-book-badge" data-destination="requests" class="new badge light-green lighten-1 new-booked-message"></span>');
                    $('#new-book-badge').on('click',function(){
                        _self.resetFilter(false);
                        _self.setFilter({'status':'pre-booked'});
                        _self.goToPage($(this).data('destination'));
                    });
                }
                //update counts
                $('#new-book-badge').attr('data-badge-caption',   (count == 1 ? '1 New Pre-Book' :  (count + ' New Pre-Books'))  ).data('count', count);

            }

        },
        getNotificationBadgeNumber: function(type){
            if(type === 'requests'){
                return $('#new-request-badge').data('count') || 0;
            }else if(type === 'pre-booked'){
                return $('#new-book-badge').data('count') || 0;
            }else {
                return 0;
            }

        },
        updateDocumentTitle: function (alertTitle) {

            var lastTitle = "***" + alertTitle + "***", tmp;
            if (document.hidden) {
                var timerTitle = setInterval(function () {
                    if (document.hidden) {
                        tmp = document.title;
                        document.title = lastTitle;
                        lastTitle = tmp;
                    } else {
                        if (document.title === "***" + alertTitle + "***") {
                            document.title = lastTitle;
                        }
                        clearInterval(timerTitle);
                    }
                }, 1000);
            }
        },
        createModal: function (_self, html, type) {
            $('#popup-modal').html(html);
            $('#popup-modal select').formSelect();
            $('#popup-modal .modal-content').scrollTop(0);
            $('#popup-modal').modal('open');
        },
        appUsersEvents: function (_self) {
            _self.usersStorage={};
            _self.getAppUser.userLoaded={};
            $('#app-users').on('click', '.single-user', function () {


                _self.getAppUser(_self, {'app_user_id': $(this).attr('data-user-id')});
            });
            $('#app-users .users-load-more').on('click',function(){
                if( $('html').hasClass('loading-start')){
                    return true;
                }
                _self.getAppUsers();
            });
            $('#app-users select').formSelect();

            $('#reset-app-users-filter').on('click',function(){
                _self.resetAppUsersFilter();
            });

        },
        getAppUser: function (_self, userData) {

            if (typeof _self.getAppUser.userLoaded[userData['app_user_id']] == 'undefined') {
                userData['action'] = 'apet_get_app_user';
                userData['user_id'] = _self.userId;

                _self.sendAjax({
                    data: userData,
                    beforeSend:function(){
                        _self.getAppUser.userLoaded[userData['app_user_id']] = 'loading-start';
                    },
                    success: function (response) {
                        if (response.success === "true") {
                            _self.drawUserInfo(_self, response.data);
                            _self.usersStorage[userData['app_user_id']] = response.data;
                            _self.getAppUser.userLoaded[userData['app_user_id']] = 'loaded';

                        } else {
                            delete  _self.getAppUser.userLoaded[userData['app_user_id']];
                            if (response.errorMessage === 'fetching_user_failed') {
                                _self.errorToast('Something went wrong.');
                            }
                            if (response.errorMessage === 'unauthorize_access') {
                                _self.logout(_self);
                            }
                        }
                    },
                    error: function (error) {
                        _self.errorToast('Something went wrong.');
                        delete  _self.getAppUser.userLoaded[userData['app_user_id']];

                    },
                    complete: function () {
                        $('#app-users li[data-user-id="' + userData['app_user_id'] + '"] .progress').hide();
                    }
                })

            }
        },
        getAppUsers: function (firstLoad) {

            var _self = this;

            firstLoad = (typeof firstLoad == 'undefined') ? false : firstLoad;

            if(firstLoad){
                delete  _self.getAppUsers.filterOptions;
                delete _self.getAppUsers.offset;
                $('#app-users-filter-form')[0].reset();
                M.updateTextFields()
            }

            var loader = false;
            if(typeof _self.getAppUsers.offset == 'undefined'){
                _self.getAppUsers.offset = 0;
                loader = true;
            }

            $('html').addClass('loading-start');

            var data = {
                'action': 'apet_get_app_users',
                'user_id': _self.userId,
                'limit': _self.limit,
                'offset': _self.getAppUsers.offset
            };
            if(_self.getAppUsers.filterOptions){
                data['filter'] = JSON.stringify(_self.getAppUsers.filterOptions);
            }


            _self.sendAjax({
                data: data,
                beforeSend:function(){
                    if( _self.getAppUsers.offset == 0){
                        $('.app-users-container').empty();
                        $('.app-users-empty-msg').show();
                    }
                },
                complete: function(){
                    $('html').removeClass('loading-start')
                },
                success: function (response) {
                    if (response.success === "true") {
                        _self.appUsers = response.data;
                        _self.drawAppUsers(_self, response.data, response.users_count);
                        _self.getAppUsers.offset+= response.data.length;
                    } else {
                        if (response.errorMessage === 'no_users' && _self.getAppUsers.offset == 0) {
                            $('.app-users-empty-msg').show();
                        }
                        if (response.errorMessage === 'unauthorize_access') {
                            _self.logout(_self);
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }, loader: loader
            })


        },
        drawUserInfo:function(_self, userData){
            var blockHtml =
                '<table class="bordered single-app-user-table">\
                <caption></caption>\
                    <tbody>\
                        <tr>\
                            <th>First Name</th>\
                            <td>' + userData["first_name"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Last Name</th>\
                            <td class="capitalize">' + userData["last_name"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Phone</th>\
                            <td >' + userData["phone"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Email</th>\
                            <td >' + userData["email"] + '</td>\
                        </tr>\
                       <tr>\
                            <th>Bonus Count</th>\
                            <td>' + userData["bonus"] + '</td>\
                        </tr>\
                        <tr>\
                            <th>Device Info</th>\
                            <td class="capitalize">' + (function(data){
                    var deviceHtml = '';
                    $.each(data,function(key,value){
                        deviceHtml += value['platform'] +' - '+ value['language']+'<br>'
                    })
                    return deviceHtml;
                })(userData["device_data"] ) + '</td>\
                        </tr>\
                       <th>Status</th>\
                        <td class="capitalize">' + userData["user_status"] + '</td>\
                        </tr>'
            '</tbody>\
                </table>'
            $('#app-users li[data-user-id="' + userData["id"] + '"] .collapsible-single-user').append(blockHtml);
            $('#requests .materialboxed').materialbox();
        },
        drawAppUsers: function (_self, data, usersCount) {

            if(data.length == 0){
                return false;
            }

            if(!$('.app-users-container .app-users-table').length){
                $('.app-users-container').append('<ul class="collapsible app-users-table" data-collapsible="accordion"</ul>');
            }


            var appUsersHtml = '';
            $.each(data, function (key, userData) {
                appUsersHtml += _self.addAppUserBlock(userData, _self);
            });

            $('.app-users-container .app-users-table').append(appUsersHtml);
            $('.collapsible').collapsible();

        },
        addAppUserBlock: function (userData, _self) {
            $('.app-users-empty-msg').hide();
            var blockHtml = '<li data-user-id="' + userData["ID"] + '" class="single-user">\
                                <div class="collapsible-header">\
                                    <table>\
                                        <tbody>\
                                            <tr>\
                                                <th >' + userData["user_email"] + '</th>\
                   <td>Date: ' + userData["user_registered"].slice(0, 10) + '</td>\
                <td>\
                <a class="collapsible-opener" href="javascript:void(0);"><i class="material-icons">&#xE313;</i></a>\
                                    </td>\
                                </tr>\
                            </tbody>\
                        </table>\
                        </div>\
                        <div class="collapsible-body collapsible-single-user grey lighten-3">\
                                 <div class = "progress" >\
                                  <div class = "indeterminate" ></div>\
                                 </div>\
                        </div>\
                    </li>';
            return blockHtml;

        },
        sendAjax: function (ajax) {
            var _self = this;
            $.ajax({
                type: "POST",
                url: _self.ajaxUrl,
                dataType: "json",
                data: ajax.data,
                beforeSend: function () {
                    if (typeof ajax['beforeSend'] === "function") {
                        ajax['beforeSend']();
                    }
                    if (ajax.loader === true) {
                        $('.loader-layer').show();
                    }

                    if(ajax.data && ajax.data.action){
                        $('html').addClass('action-'+ajax.data.action);
                    }
                },
                success: function (response) {
                    if (typeof ajax['success'] === "function") {
                        ajax['success'](response)
                    }
                },
                error: function (err) {
                    if (typeof ajax['error'] === "function") {
                        ajax['error'](err);
                    }
                },
                complete: function (response) {
                    if (typeof ajax['complete'] === "function") {
                        ajax['complete'](response);
                    }
                    if (ajax.loader === true) {
                        $('.loader-layer').hide();
                    }
                    if(ajax.data && ajax.data.action){
                        $('html').removeClass('action-'+ajax.data.action);
                    }
                    if (response.errorMessage === 'unauthorize_access') {
                        _self.logout(_self);
                    }

                }
            })

        },
        errorToast: function (message,cb) {
            cb = cb || function(){};
            M.toast({html:message,displayLength: 5000,classes:'error-toast',completeCallback:cb});
        },
        successToast: function (message) {
            M.toast({html:message,displayLength: 5000,classes: 'success-toast'});
        },
        validateEvents: function () {
            $('#main').on('click', '.validate-radio.invalid label', function () {
                $(this).closest('.validate-radio.invalid').removeClass('invalid');
            });
        },
        validateInputs: function (scope) {
            var _self = this, invalid = false;

            scope.find('.validate-radio').addBack('.validate-radio').each(function () {

                if ($(this).find("input:radio:checked").val() == undefined) {
                    $(this).removeClass('valid').addClass('invalid');
                    $(this).siblings('label').attr('data-error', ' ');
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-length').addBack('.validate-length').each(function () {
                var min = (typeof $(this).attr('data-min-length')) == 'undefined' ? Number.MIN_SAFE_INTEGER : $(this).attr('data-min-length');
                var max = (typeof $(this).attr('data-max-length')) == 'undefined' ? Number.MAX_SAFE_INTEGER : $(this).attr('data-max-length');
                if ($(this).val().length < min || $(this).val().length > max) {
                    $(this).removeClass('valid').addClass('invalid');
                    $(this).siblings('label').attr('data-error', ' ');
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-required').addBack('.validate-required').each(function () {
                if ($(this).val() === "") {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ');

                    if (!$(this).closest('.collapsible li').hasClass('active')) {
                        $(this).closest('.collapsible').collapsible('open', $(this).closest('.collapsible').index());
                    }
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('select.validate-select-required').addBack('select.validate-select-required').each(function(){
                var _this = $(this).siblings('input.select-dropdown');
                if (!$(this).val() || $(this).val() === "") {
                    _this.removeClass('valid').addClass('invalid')
                    invalid = true;
                } else {
                    if (_this.hasClass('invalid')) {
                        _this.removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-positive-int').addBack('.validate-positive-int').each(function () {
                if (!_self.validations['positive-int']($(this).val()) && $(this).val() !== "") {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid') && $(this).val() !== "") {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }

            });

            scope.find('.validate-positive-int-with-zero').addBack('.validate-positive-int-with-zero').each(function () {
                if (!_self.validations['positive-int-with-zero']($(this).val()) && $(this).val() !== "") {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid') && $(this).val() !== "") {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }

            });

            scope.find('.validate-name').addBack('.validate-name').each(function () {
                if (!_self.validations['name']($(this).val())) {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }

            });

            scope.find('.validate-email').addBack('.validate-email').each(function () {
                if (!_self.validations['email']($(this).val())) {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-password').addBack('.validate-password').each(function () {
                if (!_self.validations['password']($(this).val())) {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-phone').addBack('.validate-phone').each(function () {
                if (!_self.validations['phone']($(this).val())) {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-date').addBack('.validate-date').each(function () {
                if (!_self.validations['date']($(this).val())) {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                } else {
                    var _this = $(this);
                    var _compareEl;

                    var dataCompare = _this.attr('data-date-compare');
                    var id = _this.attr('id');

                    if (dataCompare) {
                        _compareEl = $('#' + dataCompare);
                        if (_self.validations['compareDates'](_this.val(),_compareEl.val())) {
                            _this.removeClass('invalid').addClass('valid');
                        } else {
                            _this.removeClass('valid').addClass('invalid');
                            invalid = true;
                        }
                    } else if ($('[data-date-compare="' + id + '"]').length) {
                        _compareEl = $('[data-date-compare="' + id + '"]');
                        if(_compareEl.val().trim() !== '') {
                            if (_self.validations['compareDates'](_compareEl.val(),_this.val())) {
                                _compareEl.removeClass('invalid').addClass('valid');
                            } else {
                                _compareEl.removeClass('valid').addClass('invalid');
                                invalid = true;
                            }
                        }
                    } else if ($(this).hasClass('invalid')) {
                        _this.removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-time').addBack('.validate-time').each(function(){
                if (!_self.validations['time']($(this).val())) {
                    $(this).removeClass('valid').addClass('invalid')
                    $(this).siblings('label').attr('data-error', ' ')
                    invalid = true;
                }else{
                    var _this = $(this);

                    var _compareEl,thisDate,compareDate,_compareElId,
                        dataCompare = _this.attr('data-date-compare'),
                        id = _this.attr('id');

                    if (dataCompare) {
                        _compareEl = $('#' + dataCompare);
                        var res1 =  id.split('-');
                        var res2 =  dataCompare.split('-');

                        thisDate = $('#request-offer-' + res1[2] + '-' + res1[3] + '-' + res1[4] + '-date').val().trim();
                        compareDate = $('#request-offer-' + res2[2] + '-' + res2[3] + '-date').val().trim();

                        if(thisDate !=='' && compareDate !== ''){

                            thisDate += ' '+ _this.val()+ ':00';
                            compareDate += ' '+ _compareEl.val() + ':00';
                            if (_self.validations['compareDates'](thisDate,compareDate)) {
                                _this.removeClass('invalid').addClass('valid');
                            } else {
                                _this.removeClass('valid').addClass('invalid');
                                invalid = true;
                            }
                        }
                    } else if ($('[data-date-compare="' + id + '"]').length) {
                        _compareEl = $('[data-date-compare="' + id + '"]');
                        _compareElId = _compareEl.attr('id');

                        if(_compareEl.val().trim() !== '') {

                            res1 =  id.split('-');
                            res2 = _compareElId.split('-');
                            thisDate = $('#request-offer-' + res1[2] + '-' + res1[3] + '-date').val().trim();
                            compareDate = $('#request-offer-' + res2[2] + '-' + res2[3] + '-' + res2[4] + '-date').val().trim();
                            thisDate += ' ' + _this.val() + ':00';
                            compareDate += ' ' + _compareEl.val() + ':00';
                            if (_self.validations['compareDates'](compareDate, thisDate)) {
                                _compareEl.removeClass('invalid').addClass('valid');
                                if ($(this).hasClass('invalid')) {
                                    _this.removeClass('invalid').addClass('valid');
                                }
                                _this.removeClass('invalid').addClass('valid');
                            } else {
                                _compareEl.removeClass('valid').addClass('invalid');
                                invalid = true;
                            }
                        }else{
                            _this.removeClass('invalid').addClass('valid');
                        }
                    }else if ($(this).hasClass('invalid')) {
                        _this.removeClass('invalid').addClass('valid');
                    }
                }

            });

            scope.find('.validate-country').addBack('.validate-country').each(function () {
                if (!countriesList[$(this).val().toLowerCase()]) {
                    $(this).removeClass('valid').addClass('invalid');
                    $(this).siblings('label').attr('data-error', ' ');
                    invalid = true;
                } else {
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-city').addBack('.validate-city').each(function(){
                if($(this).val() === "" || $(this).attr('data-value') === ""){
                    $(this).removeClass('valid').addClass('invalid');
                    invalid = true;
                }else{
                    if ($(this).hasClass('invalid')) {
                        $(this).removeClass('invalid').addClass('valid');
                    }
                }

            });

            scope.find('.validate-autocomplete').each(function(){
                var _this = this;
                var id = $(_this).attr('id');
                var res =  id.split('-');

                var airportInput,airportCityInput;
                var cityScope = $(_this).attr('data-country-scope');

                airportInput = $('#request-offer-' + res[2] + '-' + res[3] + '-airport-code');
                airportCityInput = $('#request-offer-' + cityScope);

                if(airportInput.val() === '' || airportCityInput.val() === '' || !airportCityInput.attr('data-value') || airportCityInput.attr('data-value') === ''){
                    $(_this).removeClass('valid').addClass('invalid');
                    invalid = true;
                }else {
                    if ($(_this).hasClass('invalid')) {
                        $(_this).removeClass('invalid').addClass('valid');
                    }
                }
            });

            scope.find('.validate-company-autocomplete').each(function(){

                var _this = this;
                var val = $(_this).val().trim();
                if(val !== ''){
                    airlinesList.forEach(function(e){
                        if(e.name === val){
                            var iata = e.code;
                            if($(_this).hasClass('departure')){
                                if($('#airlines-iata-code-departure').val() !== iata){
                                    invalid = true;
                                    $(_this).removeClass('valid').addClass('invalid');
                                }else{
                                    $(_this).removeClass('invalid').addClass('valid');
                                }
                            }else if($(_this).hasClass('arrival')){
                                if($('#airlines-iata-code-arrival').val() !== iata){
                                    $(_this).removeClass('valid').addClass('invalid');
                                    invalid = true;
                                }else{
                                    $(_this).removeClass('invalid').addClass('valid');
                                }
                            }
                            return false;
                        }
                    });
                }else{
                    $(_this).removeClass('valid').addClass('invalid');
                    invalid = true;
                }
            });


            return !invalid;
        },
        validations: {
            name: function (name) {
                if (name === '' || name.length < 2) {
                    return false;
                }
                return true;
            },
            "positive-int": function (int) {
                return /^\+?([1-9]\d*)$/.test(int);
            },
            "positive-int-with-zero": function (int) {
                return /^\+?([0|1-9]\d*)$/.test(int);
            },
            email: function (email) {
                var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return pattern.test(email);
            },
            password: function (password) {
                if (password === '') {
                    return false;
                }
                var pattern = /^(?=.*[A-Za-z])(?=.*[0-9])(?=.{6,})/;
                return pattern.test(password);
            },
            phone: function (phone) {
                if (phone === '') {
                    return false;
                }
                var pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
                if (!phone.match(pattern)) {
                    return false;
                }
                return true;
            },
            date: function (date) {
                if (date === '') {
                    return false;
                }
                var pattern = /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
                return pattern.test(date);
            },
            time: function(time) {
                if (time === '') {
                    return false;
                }
                var pattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
                return pattern.test(time);
            },
            compareDates: function (date1, date2) {
                var newDate1 = new Date(date1);
                var newDate2 = new Date(date2);
                return (newDate1 >= newDate2);
            },
            allowedFileSize: function (fileSizeInKB) {
                return fileSizeInKB <= 2048;

            }
        },
        init: function () {
            var _self = this;
            $(document).ready(function () {

                _self.bindEvents(_self);
            });
        },
        fillMenuHeaderHtml: function (_self) {
            var data = _self.agencyDetails;

            var packagePlanBlock=data['package_plan'].title;

            if(data['package_plan'].title !== "Premium Plan" && data['package_plan'].title !== "Unlimited Plan"){
                packagePlanBlock+='<a href="https://easytraveling.am/#pricing" target="_blank" class="upgrade-plan">upgrade</a>';
            }


            $('.sidenav  .nav-name-place').html(data['tour_agent']);
            $('.sidenav  .nav-address-place').html(data['address'] + ', ' + data['city']);
            $('.sidenav  .nav-package-plan').html(packagePlanBlock);
            $('.sidenav  .user-photo span').html(data['tour_agent'].charAt(0));
            $('.navbar-fixed  .agency-name').html(data['tour_agent']);

            // THIS API IS DEPRECATED
            // $.ajax({
            //     url: 'https://picasaweb.google.com/data/entry/api/user/' + data.email + '?alt=json',
            //     dataType: 'json',
            //     success: function (res) {
            //         $('.sidenav  .user-photo').addClass('avatar');
            //         $('.sidenav  .user-photo img').attr('src', res.entry.gphoto$thumbnail.$t);
            //     },
            //     error: function () {
            //         $('.sidenav  .user-photo').removeClass('avatar');
            //     }
            // });
        },
        loadAgencyDetails: function (_self) {

            var userId = _self.userId;
            var tourId = _self.tourId;
            _self.sendAjax({
                data: {
                    'action': 'apet_get_tour_settings',
                    'user_id': userId,
                    'tour_id': tourId,
                },
                success: function (response) {
                    if (response.success === "true") {

                        _self.agencyDetails = response.data;
                        _self.fillMenuHeaderHtml(_self);
                    } else {
                        if (response.errorMessage === 'failed_to_fetch') {
                            _self.errorToast('Something went wrong.');
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');

                }, loader: true
            })
        },
        loggedIn: function (_self) {
            _self.userId = $('#main').attr('data-user-id');
            _self.tourId = $('#main').attr('data-tour-id');
            _self.role = $('#main').attr('data-user-role');
            _self.status = $('#main').attr('data-tour-status');
            if (_self.tourId === 'all') {
                _self.tourAgenciesPageEvents(_self);
                _self.appUsersEvents(_self);
            } else {
                _self.settingPageEvents(_self);
            }

            _self.specOffersEvents(_self);
            _self.dashboardEvents(_self);
            _self.requestsEvents(_self);
            _self.statsEvents(_self);


            if (_self.status == 'inactive') {
                _self.inactiveAgency();
            } else {
                if (_self.role == 'tour-agent-admin') {
                    _self.loadAgencyDetails(_self);
                }

                var url = new URL(location.href);
                var requestId = url.searchParams.get('request_id');

                if (!requestId) {
                    _self.goToPage(_self.currentPage);

                }else{
                    _self.setFilter({id:requestId,status:'',country:'',from:'',to:''},false);
                    _self.getRequests.filterOptions = {id:requestId};
                    _self.goToPage(_self.currentPage,false);
                }
                _self.checkRequests(_self);

            }
        },
        inactiveAgency: function () {
            var _self = this;
            $('#settings h4').html('Activation');
            _self.goToPage('settings');

        },
        agencyActivated: function () {
            var _self = this;
            $('#main').attr('data-tour-status', 'active');
            _self.status = 'active';

            $('#settings h4').html('Settings');

            if (_self.role == 'tour-agent-admin') {
                _self.loadAgencyDetails(_self);
                _self.checkRequests(_self);
            }

            $('#menu-slide-out .request-menu-btn,#menu-slide-out .offres-menu-btn').addClass('page-redirect').removeClass('side-menu-disabled');
            _self.successToast('Tour agency activated.');
            _self.goToPage('requests');

        },
        statsEvents:function(){
            var _self = this;
            $('#stats select').formSelect();

            $('#stats-filter-type').on('change',function() {

                var value = $(this).val();
                var usersSubValue = $('#user-stats-sub-type').val();
                var agencySubValue = $('#agency-stats-sub-type').val();

                $('#stats-filter-form').removeClass(function (index, css) {
                    return (css.match(/\bfilter-type-\S+/g) || []).join(' ');
                });
                $('#stats-filter-form').addClass('filter-type-' + value);



                $('#stats-filter-form').removeClass(function (index, css) {
                    return (css.match(/\bfilter-sub-type-\S+/g) || []).join(' ');
                });



                $('#agency-stats-sub-type, #user-stats-sub-type, #requests-stats-sub-type').prop('disabled',true);
                $('#stats-filter-from, #stats-filter-to').prop('disabled',false);


                if(value == 'user'){
                    if(usersSubValue == 'types'){
                        $('#stats-filter-from, #stats-filter-to').prop('disabled',true);
                    }
                    $('#stats-filter-form').addClass('filter-sub-type-' + usersSubValue);
                    $('#user-stats-sub-type').prop('disabled',false);
                }

                if(value == 'agency'){
                    if(agencySubValue == 'types'){
                        $('#stats-filter-from, #stats-filter-to').prop('disabled',true);
                    }
                    $('#stats-filter-form').addClass('filter-sub-type-' + agencySubValue);
                    $('#agency-stats-sub-type').prop('disabled',false);
                }

                if(value == 'requests'){
                    $('#requests-stats-sub-type').prop('disabled',false);
                }
                $('#stats select').formSelect();

            });
            $('#user-stats-sub-type, #agency-stats-sub-type').on('change',function() {
                var value = $(this).val();
                $('#stats-filter-form').removeClass(function (index, css) {
                    return (css.match(/\bfilter-sub-type-\S+/g) || []).join(' ');
                });
                $('#stats-filter-form').addClass('filter-sub-type-' + $(this).val());


                if(value == 'types'){
                    $('#stats-filter-from, #stats-filter-to').prop('disabled',true);
                }else{
                    $('#stats-filter-from, #stats-filter-to').prop('disabled',false);
                }
            });

            $('#stats .datepicker').datepicker({
                selectMonths: true, // Creates a dropdown to control month
                yearRange: 2, // Creates a dropdown of 15 years to control year,
                firstDay: 1,
                format: 'yyyy-mm-dd',
                closeOnSelect: false // Close upon selecting a date,
            });

            $('#reset-stats-filter').on('click',function(){
                _self.resetStatsFilter();
            });
        },
        getStats: function (firstLoad) {
            var _self = this;

            firstLoad = (typeof firstLoad == 'undefined') ? false : firstLoad;

            if(firstLoad) {
                delete  _self.getStats.filterOptions;
                $('#stats-filter-form')[0].reset();
                M.updateTextFields();

                _self.getStats.filterOptions = {
                    'type': 'user',
                    'sub_type': 'regs'
                }
            }

            var data = {
                'action': 'apet_get_stats',
                'user_id': _self.userId,
                'tour_id': _self.tourId
            };


            if (_self.getStats.filterOptions) {
                data['filter'] = JSON.stringify(_self.getStats.filterOptions);
            }

            _self.sendAjax({
                data: data,
                beforeSend: function () {
                    if (firstLoad) {
                        $('.stats-container').hide();
                        $('.stats-empty-msg').show();
                    }
                },
                complete: function () {
                    /*$('html').removeClass('loading-start')*/
                },
                success: function (response) {
                    if (response.success === "true") {
                        $('.stats-container').show();
                        $('.stats-empty-msg').hide();
                        _self.drawStatsData(response.data);

                    } else {
                        if (response.errorMessage === 'no_stats') {
                            $('.stats-empty-msg').show();
                            $('.stats-container').hide();
                        }
                    }
                },
                error: function (error) {
                    _self.errorToast('Something went wrong.');
                }, loader: true
            });


        },
        drawStatsData: function (data, statsType) {
            var _self = this;
            // inital config
            var config = {
                options:{
                    title:{
                        text:"Stats"
                    },
                    scales:{
                        xAxes:[]
                    }
                },
                data:{
                    labels:[],
                    datasets:[]
                }
            };

            if ( _self.getStats.filterOptions.type == 'user') {
                config.options.title.text = "Users"
            }else if ( _self.getStats.filterOptions.type == 'agency') {
                config.options.title.text = "Agency"
            }else if ( _self.getStats.filterOptions.type == 'special_offers') {
                config.options.title.text = "Special Offers"
            }else if ( _self.getStats.filterOptions.type == 'requests') {
                config.options.title.text = "Requests"
            }

            var datasetsArray = [];

            if (_self.getStats.filterOptions.type == 'requests') {

                config.options.scales.xAxes.push({
                    type:'time',
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Date'
                    }
                });
                $.each(data,function(key, el){
                    var newDataSetElement = {
                        data: []
                    };
                    newDataSetElement.label = _self.helpers.capitalize( _self.helpers.replaceSymbolToWhiteSpace(key,'_'));
                    newDataSetElement.fill = false;

                    switch (key){
                        case 'requests':
                            newDataSetElement.backgroundColor = '#7cb342';
                            newDataSetElement.borderColor = '#7cb342';
                            break;
                        case 'offers':
                            newDataSetElement.backgroundColor = '#e67e22';
                            newDataSetElement.borderColor = '#e67e22';
                            break;
                        case 'books':
                            newDataSetElement.backgroundColor = '#3498db';
                            newDataSetElement.borderColor = '#3498db';
                            break;
                        case 'offered_requests':
                            newDataSetElement.backgroundColor = '#f1c40f';
                            newDataSetElement.borderColor = '#f1c40f';
                            break;
                        case 'booked_offers':
                            newDataSetElement.backgroundColor = '#9b59b6';
                            newDataSetElement.borderColor = '#9b59b6';
                            break;
                        case 'pending':
                            newDataSetElement.backgroundColor = '#ff7043';
                            newDataSetElement.borderColor = '#ff7043';
                            break;
                        case 'offered':
                            newDataSetElement.backgroundColor = '#29b6f6';
                            newDataSetElement.borderColor = '#29b6f6';
                            break;
                        case 'pre-booked':
                            newDataSetElement.backgroundColor = '#8bc34a';
                            newDataSetElement.borderColor = '#8bc34a';
                            break;
                        case 'processing':
                            newDataSetElement.backgroundColor = '#607d8b';
                            newDataSetElement.borderColor = '#607d8b';
                            break;
                        case 'confirmed':
                            newDataSetElement.backgroundColor = '#33691e';
                            newDataSetElement.borderColor = '#33691e';
                            break;
                        case 'declined':
                            newDataSetElement.backgroundColor = '#F44336';
                            newDataSetElement.borderColor = '#F44336';
                            break;
                        case 'canceled':
                            newDataSetElement.backgroundColor = '#EF5350';
                            newDataSetElement.borderColor = '#EF5350';
                            break;
                        case 'booked':
                            newDataSetElement.backgroundColor = '#1B5E20';
                            newDataSetElement.borderColor = '#1B5E20';
                            break;
                    }

                    $.each(el, function (index, element) {
                        newDataSetElement.data.push({
                            t:element.dates,
                            y:element.numbers
                        });
                    });

                    datasetsArray.push(newDataSetElement);
                });
            } else {
                if (_self.getStats.filterOptions.sub_type == 'types') {

                    config.options.title.text += ' by Types';
                    config.type = 'bar';
                    $.each(data, function (index, el) {
                        var newDataSetElement = {};
                        newDataSetElement.label = _self.helpers.capitalize( _self.helpers.replaceSymbolToWhiteSpace(el.types,'_'));
                        newDataSetElement.data = [el.numbers];


                        switch (el.types){
                            case 'active':
                                newDataSetElement.backgroundColor = '#7cb342';
                                break;
                            case 'deactivated':
                                newDataSetElement.backgroundColor = '#e74c3c';
                                break;
                            case 'unconfirmed':
                                newDataSetElement.backgroundColor = '#95a5a6';
                            case 'inactive':
                                newDataSetElement.backgroundColor = '#95a5a6';
                                break;
                        }
                        datasetsArray.push(newDataSetElement);
                    });
                } else {
                    if (_self.getStats.filterOptions.sub_type == 'regs'){
                        config.options.title.text += ' by Registrations';
                    }
                    config.options.scales.xAxes.push({
                        type:'time',
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    });

                    var newDatasetElement = {};
                    newDatasetElement = {
                        label: _self.getStats.filterOptions.sub_type == 'regs' ? 'Registrations': config.options.title.text,
                        data: []
                    };
                    $.each(data, function (index, el) {
                        newDatasetElement.data.push({
                            t:el.dates,
                            y:el.numbers
                        });
                    });
                    newDatasetElement.backgroundColor = "#7cb342";
                    newDatasetElement.borderColor = "#7cb342";
                    newDatasetElement.fill = false;
                    datasetsArray.push(newDatasetElement);
                }
            }

            if(datasetsArray){
                config.data.datasets = datasetsArray;
            }
            _self.drawChart('stats-canvas',config);
        },
        drawChart:function(elementID,config){
            var _self = this;

            var ctx = document.getElementById(elementID).getContext('2d');

            config = config || {};

            var defaultOptions = {
                type:'line',
                data: {
                    datasets: [],
                    labels: []
                },
                options:{
                    responsive: true,
                    maintainAspectRatio:false,
                    beginAtZero:true,
                    title: {
                        display: true,
                        text: 'Stats'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Date'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }};
            config =  $.extend( true, defaultOptions, config );

            if(_self.statsChart){
                _self.statsChart.destroy();
            }
            _self.statsChart = new Chart(ctx, config );
        },
        goToPage: function (pageId,firstLoad) {
            var _self = this;

            if (_self.status == 'inactive' && pageId != 'settings') {
                return false;
            }

            firstLoad = typeof firstLoad === 'undefined' ? true : firstLoad;

            if ($('#' + pageId).hasClass('active-page') || $('#' + pageId).length == 0) {
                return false;
            } else {
                $('.page').removeClass('active-page');
            }

            switch (pageId) {
                case "requests":
                    _self.getRequests(firstLoad);
                    break;
                case "settings":
                    _self.fillSettings(_self);
                    break;
                case "special-offers":
                    _self.getSpecOffers(firstLoad);
                    break;
                case "tour-agencies":
                    _self.getTourAgencies(firstLoad);
                    break;
                case "app-users":
                    _self.getAppUsers(firstLoad);
                    break;
                case "stats":
                    _self.getStats(firstLoad);
                    break;
            }

            _self.currentPage = pageId;
            $('#popup-modal').attr('data-action', pageId);
            $('body').attr('data-active', pageId);
            $(".page .wrapper").scrollTop(0);
            $('#' + pageId).addClass('active-page');
            if(pageId !== 'requests' && location.search.indexOf("request_id") > -1) {
                history.pushState({},"",location.origin + '/dashboard/#'+pageId + '-page')
            }else{
                location.hash = pageId + '-page';
            }
            //add active class to menu item
            $('.sidenav li').removeClass('active');
            $('.sidenav a[data-destination=' + pageId + ']').closest('li').addClass('active');
        },
        helpers: {
            urlify: function (text) {
                if(!text){
                    return text;
                }
                var urlRegex = /(https?:\/\/[^\s]+)/g;
                return text.replace(urlRegex, function (url) {
                    var urlFilter = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
                    if (urlFilter.test(url)) {
                        return '<a style="word-break: break-all;" href="#" onclick="window.open(\'' + url + '\', \'_system\');">' + url + '</a>';
                    } else {
                        return url;
                    }
                })
            },
            nl2br: function(text){
                if(!text){
                    return text;
                }
                return text.replace(/(?:\r\n|\r|\n)/g, '<br>');
            },
            daysLeft: function (date, lastDayIncluded) {
                if (typeof lastDayIncluded == 'undefined') {
                    lastDayIncluded = true;
                }

                var endDate = new Date(date);
                if (lastDayIncluded) {
                    endDate.setDate(endDate.getDate() + 1);
                }

                var now = new Date();
                var distance = endDate - now;
                var _second = 1000;
                var _minute = _second * 60;
                var _hour = _minute * 60;
                var _day = _hour * 24;
                if (distance < 0) {
                    return -1;
                } else {
                    var days = Math.floor(distance / _day);
                    /*var hours = Math.floor((distance % _day) / _hour);
                     var minutes = Math.floor((distance % _hour) / _minute);
                     var seconds = Math.floor((distance % _minute) / _second);*/
                }

                return days;
            },
            locationHashToPageId: function () {
                if (location.hash) {
                    return location.hash.replace('-page', '').replace('#', '');
                } else {
                    return '';
                }
            },
            arrayDifference: function (arr1, arr2) {

                function comparer(otherArray) {
                    return function (current) {
                        return otherArray.filter(function (other) {

                            return other.request_id == current.request_id;

                        }).length == 0;
                    }
                }

                return arr2.filter(comparer(arr1)).length ? arr2.filter(comparer(arr1)) : false;
            },
            firstToUpperCase: function (str) {
                return str.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1)
                });
            },
            capitalize:function(str){
                if (typeof str !== 'string') return '';


                var stringsArray = str.split(/[ ]+/);
                var str = '';
                for(var i = 0; i < stringsArray.length; i++){
                    str += ((i == 0?'':' ') + stringsArray[i].charAt(0).toUpperCase() + stringsArray[i].slice(1));
                }
                return str;
            },
            replaceSymbolToWhiteSpace: function(str,symbol){
                var regExp  = new RegExp(symbol+'+','g');
                return str.replace(regExp, ' ');
            },
            compareVersions:function(v1,v2,options){
                var lexicographical = options && options.lexicographical,
                    zeroExtend = options && options.zeroExtend,
                    v1parts = v1.split('.'),
                    v2parts = v2.split('.');

                function isValidPart(x) {
                    return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
                }

                if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
                    return NaN;
                }

                if (zeroExtend) {
                    while (v1parts.length < v2parts.length) v1parts.push("0");
                    while (v2parts.length < v1parts.length) v2parts.push("0");
                }

                if (!lexicographical) {
                    v1parts = v1parts.map(Number);
                    v2parts = v2parts.map(Number);
                }

                for (var i = 0; i < v1parts.length; ++i) {
                    if (v2parts.length == i) {
                        return 1;
                    }

                    if (v1parts[i] == v2parts[i]) {
                        continue;
                    }
                    else if (v1parts[i] > v2parts[i]) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                }

                if (v1parts.length != v2parts.length) {
                    return -1;
                }

                return 0;

            }
        },
        currentPage: 'requests',
        ajaxUrl: '/wp-admin/admin-ajax.php',
        materialazeMethods: function (_self) {

            var formatedCountriesList = {};
            $.each(countriesList, function (index, value) {
                formatedCountriesList[index] = null;
            });
            $('#settings-country, #filter-country').autocomplete({
                data: formatedCountriesList,
                limit: 20,
                minLength: 1
            });
            $('.modal').modal({
                dismissible: false,
                onOpenEnd: function () {
                    $('body').addClass('modal-opened');
                    if ($('#make-spec-offer-form').length) {
                        _self.uploadSpecOfferData(_self); //call upload when button is exist
                    }


                },
                onCloseEnd: function () {
                    _self.autocomplete = {};
                    _self.autocompleteLsr = {};
                    $('body').removeClass('modal-opened');
                    $('.pac-container').remove();
                    $('body>.datepicker-modal').remove();
                    $('body>.timepicker-modal').remove();
                }
            });

            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            $('.deadline-datepicker').datepicker({
                selectMonths: false, // Creates a dropdown to control month
                yearRange: 0, // Creates a dropdown of 15 years to control year,
                firstDay: 1,
                showClearBtn:true,
                minDate: date,
                maxDate: lastDay,
                format: 'yyyy-mm-dd',
                closeOnSelect: false // Close upon selecting a date,
            });

            if ($('.make-offer-collapsible').attr('data-disabled') === 'true') {
                $('.make-offer-collapsible').collapsible('destroy');
            }

            $('.header-menu-button').click(function(){
                var instance = M.Sidenav.getInstance($('.sidenav'));
                if(instance.isOpen){
                    instance.close();
                }else{
                    instance.open();
                }
            })
            setTimeout(function(){
                $('.sidenav').sidenav();
                $('select').formSelect();
                $('.materialboxed').materialbox();
                $('.dropdown-button').dropdown();
                $('.collapsible').collapsible();
            },0)
        },
        labels: {
            'room-only': 'RO (Room Only)',
            'breakfast': 'BB (Breakfast)',
            'half-board': 'HB (breakfast & dinner)',
            'full-board': 'FB (Breakfast, Lunch, Dinner, No drinks)',
            'all-inclusive': 'AI (Breakfast, Lunch, Dinner, Drinks))',
            'cash': 'Cash',
            'social-package':'Social Package',
            'banking-transfer': 'Bank Transfer',
            'bank-cart': 'Credit Card',
            'express': 'Express',
            'city-holiday': 'City Holiday',
            'beach-holidays': 'Beach Holiday',
            'transfer': 'Transfer',
            'pool': 'Pool',
            '1st-line': '1st Line',
            '2st-line': '2nd Line',
            'internet': 'Internet',
            'parking': 'Parking',
            'gym': 'Gym',
            'childrens-room': "Children's Room",
            'gid-service': 'Guide Service'
        },
        agencyDetails: false,
        packageDetails: {},
        userId: false,
        tourId: false,
        role: false,
        status: false,
        clearScrollTimeOut: false,
        limit: 20,
        specialOffersTimers: {},
        autocomplete: {},
        autocompleteLsr: {},
        travelPayOutUrl:'https://api.travelpayouts.com/data/en/airlines.json',
        airLinesList : [],
        notificationIcon:'/wp-content/plugins/easy-travel/assets/img/fav/fav-180x180.png',
    };

    app.init();

    function getPicker(elem){
        if(typeof elem === 'object') return M.Datepicker.getInstance(elem);
    }

})(jQuery);