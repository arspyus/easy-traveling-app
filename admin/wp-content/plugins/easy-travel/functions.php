<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * ====== ERROR MESSAGE ========
 *
 * @param str $errorMessage
 */
function response_error_message($errorMessage) {

    if (APET_ERROR_LOG) {
        $trace = debug_backtrace();
        $caller = array_shift($trace);
        $log_msg = $errorMessage . ' - ' . $caller['file'] . ':' . $caller['line'] . ' (' . date('Y-m-d H:i:s') . ')';
        file_put_contents(plugin_dir_path(__FILE__) . 'error.log', $log_msg . PHP_EOL, FILE_APPEND);
    }

    echo json_encode(array(
        'errorMessage' => $errorMessage,
        'success' => 'false',
    ));
    wp_die();
}

//========== DEBUG =============//
/*add_filter('allowed_http_origins', 'add_allowed_origins');

function add_allowed_origins($origins) {
    $origins[] = 'http://localhost:8000';
    return $origins;
} */

function apet_stripslashes_deep($value)
{
    $value = is_array($value) ?
        array_map('apet_stripslashes_deep', $value) :
        stripslashes($value);
    return $value;
}

function apet_get_user_by_email_in_fr($email){
    global $auth;

    try{
        $user = $auth->getUserByEmail($email);
        return $user->uid;
    }catch(Exception $e){
        return false;
    }

}

function apet_get_user_photo_url($firebase_data){

        $providers = $firebase_data->providerData;

        foreach ($providers as $provider){
            if($provider->providerId === 'google.com' && !is_null($provider->photoUrl)){
                return $provider->photoUrl;
            }elseif ($provider->providerId === 'facebook.com' && !is_null($provider->photoUrl)){
                return $provider->photoUrl;
            }elseif($provider->providerId === 'password' && !is_null($provider->photoUrl)){
                return $provider->photoUrl;
            }else{
                $firebase_data->photoUrl;
            }
        }
}

function apet_get_user_from_firebase($uid){
    global $auth;
    try{
        $user = $auth->getUser($uid);
        return $user;
    }catch(Exception $e){
        return false;
    }

}

function apet_verify_user($token) {

    global $auth;

    $idTokenString = $token;
    try {
        $verifiedIdToken = $auth->verifyIdToken($idTokenString,null,true);
        $uid = $verifiedIdToken->getClaim('sub');

        // ======  Put token for DEBUG =====//

        //file_put_contents(plugin_dir_path(__FILE__) . 'token.log', $token . PHP_EOL, FILE_APPEND);

        // =================================//
        return array('success'=>true,'uid'=>$uid);
    }catch (InvalidToken $e) {
        return array('success' => false, 'err_msg' => $e->getMessage());
    }catch (Exception $e){
        return array('success' => false, 'err_msg' => $e->getMessage());
    }
}

function apet_login_redirection() {

    if (!isset($_POST['action'])) {
        $role = apet_get_logged_user_role();
        if (is_page('login')) {

            if (!is_user_logged_in()) {
                load_template(dirname(__FILE__) . '/templates/login-page.php', true);
                exit();
            } else {
                if (($role === 'tour-agent-admin' || $role === 'tour-super-admin')) {
                    wp_redirect(home_url('dashboard'));
                    exit();
                } else {
                    wp_redirect(admin_url());
                    exit();
                }
            }
        } elseif (is_page('dashboard')) {

            if (is_user_logged_in()) {
                if (($role === 'tour-agent-admin')) {
                    load_template(dirname(__FILE__) . '/templates/dashboard.php', true);
                    exit();
                }elseif($role === 'tour-super-admin'){
                    load_template(dirname(__FILE__) . '/templates/dashboard-super.php', true);
                    exit();
                } else {
                    wp_redirect(admin_url());
                    exit();
                }
            } else {
                $redirect_url = home_url('login');
                if(isset($_GET['request_id'])){
                    wp_redirect($redirect_url.'?request_id='.$_GET['request_id']);
                }else{
                    wp_redirect($redirect_url);
                }
                exit();
            }
        } elseif (is_page('confirmation')) {
            load_template(dirname(__FILE__) . '/templates/confirmation.php', true);
            exit();
        } elseif (is_page('reset')) {
            if (!isset($_GET['key']) || !isset($_GET['login'])) {
                wp_redirect(home_url('confirmation'));
                exit();
            }
            load_template(dirname(__FILE__) . '/templates/password-reset.php', true);
            exit();
        } elseif (is_page('bookpics')) {
            apet_protected_book_pics();
        } elseif( is_page('action')) {
            $mode = isset($_GET['mode']) ? $_GET['mode']: null;
            if($mode === 'resetPassword'){
                load_template(dirname(__FILE__) . '/templates/password-reset.php', true);
            }elseif($mode === 'verifyEmail'){
                load_template(dirname(__FILE__) . '/templates/confirmation.php', true);
            }
            exit();

        }
    }
}

add_action('wp', 'apet_login_redirection');

function apet_protected_book_pics() {

    $user_id = get_current_user_id();
    $url = isset($_GET['url']) ? $_GET['url'] : '';

    if (apet_get_logged_user_role() == 'tour-super-admin') {
        $owns_pic = 1;
    } else {
        global $wpdb;
        $results = $wpdb->get_results("SELECT IF(COUNT(*) > 0, TRUE, FALSE) AS owns_pic FROM {$wpdb->prefix}books b INNER JOIN {$wpdb->prefix}tour_agencies t ON t.id = b.offer_author WHERE t.tour_admin = $user_id AND b.travelers_data LIKE '%$url%'", ARRAY_A);
        $owns_pic = $results[0]['owns_pic'];
    }

    $file_path = str_replace(get_site_url(), $_SERVER['DOCUMENT_ROOT'], $url);
    $file_path = preg_replace('/\/+/', DIRECTORY_SEPARATOR, $file_path);
    $file_path = preg_replace('/\\\+/', DIRECTORY_SEPARATOR, $file_path);

    if ($owns_pic && file_exists($file_path)) {
        $mime_type = mime_content_type($file_path);
        header('Content-Type: ' . $mime_type);
        $file_size = filesize($file_path);
        header('Content-Length: ' . $file_size);
        readfile($file_path);
        exit();
    } else {
        global $wp_query;
        $wp_query->set_404();
        status_header(404);
        get_template_part(404);
    }
}

function apet_get_logged_user_role() {
    $user = wp_get_current_user();
    return $user->roles ? $user->roles[0] : false;
}

function apet_get_bonus_id($userId, $bonus, $soult = 2, $baseChars = '') {
    $baseChars = empty($baseChars) ? APET_BASE_CHARS : $baseChars;
    return substr(str_shuffle($baseChars), 0, $soult) . (rand(0, 1) ? 'x' : 'X') . apet_dec_to_any($userId) . (rand(0, 1) ? 'x' : 'X') . apet_dec_to_any($bonus) . (rand(0, 1) ? 'x' : 'X') . substr(str_shuffle($baseChars), 0, $soult);
}

function get_bonus_info($bonusId) {

    $data = preg_split("/x/i", $bonusId);
    if (isset($data[1]) && isset($data[2])) {
        $userId = apet_any_to_dec($data[1]);
        $bonus = apet_any_to_dec($data[2]);
        return array(
            'user_id' => $userId,
            'bonus' => $bonus
        );
    }else{
        return false;
    }
}


function apet_dec_to_any($num, $baseChars = '', $base = 60, $index = false) {
    $baseChars = empty($baseChars) ? APET_BASE_CHARS : $baseChars;
    if (!$base) {
        $base = strlen($index);
    } else if (!$index) {
        $index = substr($baseChars, 0, $base);
    }
    $out = "";
    for ($t = floor(log10($num) / log10($base)); $t >= 0; $t--) {
        $a = floor($num / pow($base, $t));
        $out = $out . substr($index, $a, 1);
        $num = $num - ($a * pow($base, $t));
    }
    return $out;
}

function apet_any_to_dec($num, $baseChars = '', $base = 60, $index = false) {

    $baseChars = empty($baseChars) ? APET_BASE_CHARS : $baseChars;
    if (!$base) {
        $base = strlen($index);
    } else if (!$index) {
        $index = substr($baseChars, 0, $base);
    }
    $out = 0;
    $len = strlen($num) - 1;
    for ($t = 0; $t <= $len; $t++) {
        $out = $out + strpos($index, substr($num, $t, 1)) * pow($base, $len - $t);
    }
    return $out;
}

function apet_remove_device_id($device_id, $user_id) {
    $device_data = get_user_meta($user_id, 'device_data', true);
    if ($device_data) {
        unset($device_data[$device_id]['push_token_id']);
        update_user_meta($user_id, 'device_data', $device_data);
    }
}

function apet_redirect_to_custom_login() {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : null;


        if (is_user_logged_in()) {
            apet_redirect_logged_in_user($redirect_to);
            exit;
        }
// The rest are redirected to the login page
        $login_url = home_url('login');
        if (!empty($redirect_to)) {
            $login_url = add_query_arg('redirect_to', $redirect_to, $login_url);
        }


        wp_redirect($login_url);
        exit;
    }
}

add_action('login_form_login', 'apet_redirect_to_custom_login');

function apet_maybe_redirect_at_authenticate($user, $username, $password) {

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (is_wp_error($user)) {
            if (!isset($_POST['user-type'])) {

                $error_codes = join(',', $user->get_error_codes());

                $login_url = home_url('login');
                $login_url = add_query_arg('login', $error_codes, $login_url);

                wp_redirect($login_url);
                exit;
            }
        }
    }

    return $user;
}

add_filter('authenticate', 'apet_maybe_redirect_at_authenticate', 101, 3);

function apet_redirect_to_custom_lostpassword() {
    if ('GET' == $_SERVER['REQUEST_METHOD']) {
        if (is_user_logged_in()) {
            apet_redirect_logged_in_user();
            exit;
        }

        wp_redirect(home_url('login'));
        exit;
    }
}

add_action('login_form_lostpassword', 'apet_redirect_to_custom_lostpassword');

function apet_redirect_to_custom_password_reset() {
    if ('GET' == $_SERVER['REQUEST_METHOD']) {
        // Verify key / login combo
        $user = check_password_reset_key($_REQUEST['key'], $_REQUEST['login']);
        $ln = isset($_REQUEST['ln']) ? $_REQUEST['ln'] : 'en';
        if (!$user || is_wp_error($user)) {
            if ($user && $user->get_error_code() === 'expired_key') {
                wp_redirect(home_url("confirmation?login=expiredkey&ln=$ln"));
            } else {
                wp_redirect(home_url("confirmation?login=invalidkey&ln=$ln"));
            }
            exit;
        }

        $redirect_url = home_url('reset');
        $redirect_url = add_query_arg('login', esc_attr($_REQUEST['login']), $redirect_url);
        $redirect_url = add_query_arg('key', esc_attr($_REQUEST['key']), $redirect_url);
        $redirect_url = add_query_arg('ln', esc_attr($ln), $redirect_url);

        wp_redirect($redirect_url);
        exit;
    }
}

add_action('login_form_rp', 'apet_redirect_to_custom_password_reset');
add_action('login_form_resetpass', 'apet_redirect_to_custom_password_reset');

function apet_do_password_reset() {

    if ('POST' == $_SERVER['REQUEST_METHOD']) {
        $rp_key = $_REQUEST['rp_key'];
        $rp_login = $_REQUEST['rp_login'];
        $ln = $_REQUEST['language'];

        $user = check_password_reset_key($rp_key, $rp_login);

        if (!$user || is_wp_error($user)) {
            if ($user && $user->get_error_code() === 'expired_key') {
                wp_redirect(home_url("confirmation?login=expiredkey&ln=$ln"));
            } else {
                wp_redirect(home_url("confirmation?login=invalidkey&ln=$ln"));
            }
            exit;
        }

        if (isset($_POST['pass1'])) {

            if (empty($_POST['pass1'])) {
                // Password is empty
                $redirect_url = home_url('reset');

                $redirect_url = add_query_arg('key', $rp_key, $redirect_url);
                $redirect_url = add_query_arg('login', $rp_login, $redirect_url);
                $redirect_url = add_query_arg('ln', $ln, $redirect_url);
                $redirect_url = add_query_arg('error', 'password_reset_empty', $redirect_url);

                wp_redirect($redirect_url);
                exit;
            }

            // Parameter checks OK, reset password
            reset_password($user, $_POST['pass1']);
            wp_redirect(home_url("confirmation?password=changed&ln=$ln"));
        } else {
            echo "Invalid request.";
        }

        exit;
    }
}

add_action('login_form_rp', 'apet_do_password_reset');
add_action('login_form_resetpass', 'apet_do_password_reset');

function apet_get_error_message($error_code) {
    switch ($error_code) {
        case 'expiredkey':
        case 'invalidkey':
            return 'The password reset link you used is not valid anymore.';
            break;
        case '':
            return NULL;
        default:
            return 'An unknown error occurred.';
            break;
    }
}

function apet_redirect_logged_in_user($redirect_to = null) {
    $user = wp_get_current_user();
    if (user_can($user, 'manage_options')) {
        if ($redirect_to) {
            wp_safe_redirect($redirect_to);
        } else {
            wp_redirect(admin_url());
        }
    } else {
        wp_redirect(home_url('login'));
    }
}

function apet_activate_app_user($user_id) {

    update_user_meta($user_id, "user_status", 'active');
}

function apet_change_app_user_email($user_id, $email) {
    global $wpdb,$auth;
    $check_email = apet_check_user_email($email);
    if ($check_email === false) {
        update_user_meta($user_id, "email", $email);
        $user_login = $wpdb->update($wpdb->users, array('user_login' => $email, 'user_email' => $email), array('ID' => $user_id));
        update_user_meta($user_id, "pending_email", NULL);
        return true;
    } elseif($check_email && get_user_meta($check_email,'user_status',true) !== 'active'){
        /*  Delete From Firebase */
        try{
            $exist_user_fr_uid = get_user_meta($check_email,'firebase_uid',true);
            $auth->deleteUser($exist_user_fr_uid);
            wp_delete_user($check_email);

            /* Update Current User's Email */
            update_user_meta($user_id, "email", $email);
            $user_login = $wpdb->update($wpdb->users, array('user_login' => $email, 'user_email' => $email), array('ID' => $user_id));
            update_user_meta($user_id, "pending_email", NULL);

            $props = [
                'email' =>  $email,
                'emailVerified' => true
            ];
            $curent_fr_uid = get_user_meta($user_id,'firebase_uid',true);
            $auth->updateUser($curent_fr_uid,$props);
            return true;
        }catch(Exception $e){
            return false;
        }
    } else{
        update_user_meta($user_id, "pending_email", NULL);
        return false;
    }
}

function apet_days_left($date, $lastDayIncluded = true) {

    $endDate = strtotime(apet_time_zone_convert($date, 'GMT', APET_WP_TIMEZONE));
    if ($lastDayIncluded) {
        $endDate = strtotime('+1 day', strtotime($date));
    }
    $now = strtotime(apet_time_zone_convert(date('Y-m-d H:i:s'), 'GMT', APET_WP_TIMEZONE));
    $distance = $endDate - $now;


    $_minute = 60;
    $_hour = $_minute * 60;
    $_day = $_hour * 24;
    if ($distance < 0) {
        return -1;
    } else {
        $days = floor($distance / $_day);
    }
    return $days;
}

function apet_get_tour_status($user_id) {
    global $wpdb;
    $tour_table = $wpdb->prefix . 'tour_agencies';
    $tour_data = $wpdb->get_results("SELECT id,status FROM `$tour_table` WHERE tour_admin='$user_id' ", ARRAY_A);
    $tour_status = !empty($tour_data) ? $tour_data[0]['status'] : '';
    return $tour_status;
}

function apet_image_load($path) {

    $image_data = null;
    if (!extension_loaded('gd') || !function_exists('gd_info')) {
        return false;
    }
    $image_data['path'] = $path;

    $result = false;
    if (file_exists($image_data['path'])) {
        $image_data['mime'] = mime_content_type($image_data['path']);
        if ($image_data['mime'] == 'image/png') {
            $result = imagecreatefrompng($image_data['path']);
            imagealphablending($result, false);
            imagesavealpha($result, true);
        } else if ($image_data['mime'] == 'image/jpeg') {
            $result = imagecreatefromjpeg($image_data['path']);
        } else if ($image_data['mime'] == 'image/gif') {
            $result = imagecreatefromgif($image_data['path']);
        }

        if ($result != false) {
            $image_data['image'] = $result;
            $image_data['size'] = filesize($image_data['path']);
            $image_data['width'] = imagesx($image_data['image']);
            $image_data['height'] = imagesy($image_data['image']);
        }
    }
    return ($result == false) ? false : $image_data;
}

function apet_image_save($image_data = null, $path = '', $compression = null) {
    if ($image_data == null) {
        return false;
    }
    $path = ($path == '') ? $image_data['path'] : $path;
    $ext = strtolower(pathinfo($path)['extension']);
    $new_mime = 'image/' . (($ext == 'jpg') ? 'jpeg' : $ext);

    if ($new_mime != $image_data['mime']) {
        if ($new_mime == 'image/jpeg') {
            $temp = imagecreatetruecolor($image_data['width'], $image_data['height']);
            imagefill($temp, 0, 0, imagecolorallocate($temp, 255, 255, 255));
            imagealphablending($temp, true);
            if (imagecopy($temp, $image_data['image'], 0, 0, 0, 0, $image_data['width'], $image_data['height'])) {
                imagedestroy($image_data['image']);
                unset($image_data['image']);
                $image_data['image'] = $temp;
            }
        }
        $image_data['mime'] = $new_mime;
    }

    $result = false;
    if ($image_data['mime'] == 'image/png') {
        $result = is_null($compression) ? imagepng($image_data['image'], $path) : imagepng($image_data['image'], $path, intval((9 * $compression) / 100));
    } else if ($image_data['mime'] == 'image/jpeg') {
        $result = is_null($compression) ? imagejpeg($image_data['image'], $path) : imagejpeg($image_data['image'], $path, intval(100 - $compression));
    } else if ($image_data['mime'] == 'image/gif') {
        $result = imagegif($image_data['image'], $path);
    }
    imagedestroy($image_data['image']);
    return $result;
}

function apet_image_resize($image_data = null, $size = [], $keepRatio = true) {

    if ($image_data == null) {
        return false;
    }

    $size[0] = isset($size[0]) ? ($size[0] <= 0 ? $image_data['width'] : $size[0]) : (isset($size[1]) ? ($keepRatio ? PHP_INT_MAX : $image_data['width']) : $image_data['width']);
    $size[1] = isset($size[1]) ? ($size[1] <= 0 ? $image_data['height'] : $size[1]) : (isset($size[0]) ? ($keepRatio ? PHP_INT_MAX : $image_data['height']) : $image_data['height']);

    $oldSize['width'] = imagesx($image_data['image']);
    $oldSize['height'] = imagesy($image_data['image']);

    if ($keepRatio) {
        $ratio = $oldSize['width'] / $oldSize['height'];

        if ($size[0] / $size[1] > $ratio) {
            $newSize['width'] = $size[1] * $ratio;
            $newSize['height'] = $size[1];
        } else {
            $newSize['height'] = $size[0] / $ratio;
            $newSize['width'] = $size[0];
        }
    } else {
        $newSize['width'] = $size[0];
        $newSize['height'] = $size[1];
    }

    $new = imagecreatetruecolor($newSize['width'], $newSize['height']);
    imagealphablending($new, false);
    imagesavealpha($new, true);
    $result = imagecopyresampled($new, $image_data['image'], 0, 0, 0, 0, $newSize['width'], $newSize['height'], $image_data['width'], $image_data['height']);
    if ($result != false) {
        $image_data['width'] = imagesx($new);
        $image_data['height'] = imagesy($new);
        imagedestroy($image_data['image']);
        unset($image_data['image']);
        $image_data['image'] = $new;
    }
    return ($result == false) ? false : $image_data;
}

function apet_image_upload($uploaded_file = '', $new_file = '', $resize_to = array(), $allowed_mime = array('image/png', 'image/jpeg'), $max_size = 2097152/*2MB*/) {

    $result = false;
    $image_data = apet_image_load($uploaded_file);
    if ($image_data != false) {
        if (in_array($image_data['mime'], $allowed_mime) && $image_data['size'] <= 2 * 1024 * 1024) {
            if (!empty($resize_to) && ($image_data['width'] > $resize_to[0] || $image_data['height'] > $resize_to[1])) {
                $image_data = apet_image_resize($image_data, $resize_to);
            }
            if ($image_data != false) {
                if (apet_image_save($image_data, $new_file)) {
                    $result = true;
                } else {
                    $result = 'img_save_failed';
                }
            } else {
                $result = 'img_resize_failed';
            }
        } else {
            $result = 'img_validation_failed';
        }
    } else {
        $result = 'img_open_failed';
    }
    unlink($uploaded_file);
    return $result;
}

function apet_datetime_to_date($dateime) {
    $dt = strtotime($dateime);
    $date = date("Y-m-d", $dt);
    return $date;
}

function apet_time_zone_convert($fromTime, $fromTimezone, $toTimezone, $format = 'Y-m-d H:i:s') {

    $from = new DateTimeZone($fromTimezone);
    $to = new DateTimeZone($toTimezone);
    $orgTime = new DateTime($fromTime, $from);
    $toTime = new DateTime($orgTime->format("c"));
    $toTime->setTimezone($to);

    return $toTime->format($format);
}

function apet_get_timezone() {
    $min = 60 * get_option('gmt_offset');
    $sign = $min < 0 ? "-" : "+";
    $absmin = abs($min);
    $tz = sprintf("%s%02d:%02d", $sign, $absmin / 60, $absmin % 60);
    return $tz;
}

define("APET_WP_TIMEZONE", apet_get_timezone());

function apet_send_verification_mail($firebase_uid,$lang ='en',$user_id,$changed_email = false){
    global $auth;

    $encripted_user_id = apet_dec_to_any($user_id);
    if($changed_email){
        $changed_email ='true';
    }else{
        $changed_email='false';
    }
    try{
        $auth->sendEmailVerification($firebase_uid,get_site_url().'/?uid='.$encripted_user_id.'&changeEmail='.$changed_email,$lang);
        return true;
    }catch(Exception $e){
        return false;
    }
}

function apet_send_password_reset_mail($email,$lang = 'en',$additional_link){
    global $auth;

    try{
        $auth->sendPasswordResetEmail($email,get_site_url().'/?add_link='.$additional_link,$lang);
        return true;
    }catch(Exception $e){
        return false;
    }
}

function apet_send_mail($email, $subject, $message, $style = null){

    if(APET_DEBUG){
        return true;
    }
    $message = "<html><head>$style<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\"><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head><body>" . $message . "</body></html>";
    $textmessage = strip_tags(str_replace(array('<br>', '<br/>', '<br />'), "\r\n", $message));
    $array_data = array(
        'from'=> APET_EMAIL_SENDER_NAME .' <'.APET_CONFIG_EMAIL.'>',
        'h:Reply-To'=> APET_EMAIL_SENDER_NAME .' <'.APET_CONFIG_EMAIL.'>',
        'to'=> $email,
        'subject'=>$subject,
        'html'=>$message ,
        'text'=>$textmessage
        //'o:dkim'=>'yes' //DKIM per message
    );

    $session = curl_init(APET_MAILGUN_URL.'/messages');
    curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($session, CURLOPT_USERPWD, 'api:'. APET_MAILGUN_KEY);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, true);
    $response = curl_exec($session);
    $info = curl_getinfo($session);
    //$results = json_decode($response, true); //To get more info about result
    curl_close($session);

    return ($info['http_code'] != 200) ? false : true;
}

function apet_get_device_language($device_id, $user_id) {
    $device_data = get_user_meta($user_id, 'device_data', true);
    $language = 'en';
    if ($device_data) {
        $language = $device_data[$device_id]['language'];

    }
    return $language;
}
add_filter( 'send_password_change_email', '__return_false' );
add_filter( 'email_change_email', '__return_false');
if ( !function_exists( 'wp_password_change_notification' ) ) {
    function wp_password_change_notification() {}
}

add_filter( 'random_password', 'apet_my_random_password' );
function apet_my_random_password($password) {
    //Force to have at last on letter and one number to pass through our validation
    $length = strlen($password);
    $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $numbers = '0123456789';
    $letter = substr($letters, wp_rand(0, strlen($letters) - 1), 1);
    $number = substr($numbers, wp_rand(0, strlen($numbers) - 1), 1);
    if($length > 2){
        $password = substr($password, 0, $length - 2);
        $password = $letter . $password . $number;
    }elseif($length == 2){
        $password = $letter . $number;
    }
    return $password;
}

/**
 * ===== Ajax handler for getting extra data ====
 *
 */
function apet_get_extra_data() {

    $data = array(
        'app_min_version' => APET_APP_MIN_VERSION
    );

    echo json_encode(array('data' => $data, 'success' => 'true'));
    wp_die();
}

add_action('wp_ajax_nopriv_apet_get_extra_data', 'apet_get_extra_data');
add_action('wp_ajax_apet_get_extra_data', 'apet_get_extra_data');

function apet_insert_our_users_to_fb() {

    global $auth;

    set_time_limit ( 10000);

    $args = array(
        'meta_query' => array(
            array(
                'key' => 'user_type',
                'value' => 'app-user'
            )
        )
    );


    $users = get_users($args);
    foreach ($users as $user){

        $all_data = get_user_meta($user->ID);

        $firstname = $all_data['first_name'][0];
        $lastname = $all_data['last_name'][0];

        $status = $all_data['user_status'][0];
        $display_name =$firstname." ".$lastname;

        $password = wp_generate_password(12, false);
        $email_verified = false;
        $disabled = false;
        if($status === 'active'){
            $email_verified =true;
        }elseif($status === 'unconfirmed'){
            $email_verified =false;
        }elseif($status === 'deactivated'){
            $email_verified =true;
            $disabled = true;
        }

        $userProperties = [
            'email' => $user->email,
            'emailVerified' => $email_verified,
            'password' => $password,
            'displayName' => $display_name,
            'disabled' => $disabled,
        ];

        try{
            $createdUser = $auth->createUser($userProperties);
            $uid = $createdUser->uid;

            update_user_meta($user->ID,'firebase_uid',$uid);

        }catch(Exception $e){
            echo $e->getMessage();
        }

    }


    echo json_encode(array('success' => 'true'));
    wp_die();
}

add_action('wp_ajax_nopriv_apet_insert_our_users_to_fb', 'apet_insert_our_users_to_fb');
add_action('wp_ajax_apet_insert_our_users_to_fb', 'apet_insert_our_users_to_fb');

/**
 * ===== Ajax handler for send mail to User ====
 *
 */
function apet_send_email_to_user() {
    if(isset($_POST['uid'])){
        $firebase_uid = sanitize_text_field($_POST['uid']);
        $device_id =isset($_POST['device_id']) ?  sanitize_text_field($_POST['device_id']) : '';
        $args = array(
            'meta_query' => array(
                array(
                    'key' => 'user_type',
                    'value' =>'app-user'
                ),
                array(
                    'key' => 'firebase_uid',
                    'value' =>$firebase_uid
                )
            )
        );
        $user = get_users($args);

        if(!empty($user)) {
            $user_id = $user[0]->ID;
            $language= apet_get_device_language($device_id, $user_id);
            $email = $user[0]->data->user_email;
            if(empty($email)){

                $pending_email = get_user_meta($user_id,'pending_email',true);
                if(!$pending_email){
                    response_error_message('email_failed');
                }
                $pending_user = apet_check_user_email($pending_email);

                if ($pending_user){
                    $pending_user_status = get_user_meta($pending_user,'user_status',true);
                    $pending_user_type = get_user_meta($pending_user,'user_type',true);
                    if($pending_user_status === 'active' || $pending_user_type !== 'app-user'){
                        response_error_message('user_exist');
                    }
                }
                $pending_user_uid = apet_get_user_by_email_in_fr($pending_email);
                $email_success = apet_send_verification_mail($pending_user_uid,$language, $user_id,true);
            }else{
                $email_success = apet_send_verification_mail($firebase_uid,$language, $user_id);
            }
            if(!$email_success){
                response_error_message('email_failed');
            }
            echo json_encode(array('success'=> 'true'));
        }else{
            response_error_message('unauthorize_access');
        }
    }else{
        response_error_message('unauthorize_access');
    }
    wp_die();
}

add_action('wp_ajax_nopriv_apet_send_email_to_user', 'apet_send_email_to_user');
add_action('wp_ajax_apet_send_email_to_user', 'apet_send_email_to_user');

/**
 * ===== Ajax handler for activate user by user_id ====
 *
 */
function apet_activate_user() {

    if(isset($_POST['uid'])){
        global $auth;

        $email_changing = sanitize_text_field($_POST['emailChanging']);
        $user_id = sanitize_text_field($_POST['uid']);

        $pending_email = get_user_meta($user_id,'pending_email',true);
        $firebase_uid = get_user_meta($user_id,'firebase_uid',true);
        if($email_changing === 'true') {
            try{
                $mail_uid = apet_get_user_by_email_in_fr($pending_email);
                $auth->deleteUser($mail_uid);
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'user_type',
                            'value' =>'app-user'
                        ),
                        array(
                            'key' => 'firebase_uid',
                            'value' =>$mail_uid
                        )
                    )
                );
                $user = get_users($args);
                wp_delete_user($user[0]->ID);
            }catch(Exception $e){
                response_error_message($e->getMessage());
            }
            apet_change_user_email_in_fb($pending_email,$firebase_uid,$user_id);
        }else{
            update_user_meta($user_id, 'user_status', 'active');
            apet_change_app_user_email($user_id, $pending_email);
        }
    }else{
        response_error_message('unauthorize_access');
    }
    echo json_encode(array('success' => 'true'));
    wp_die();
}

add_action('wp_ajax_nopriv_apet_activate_user', 'apet_activate_user');
add_action('wp_ajax_apet_activate_user', 'apet_activate_user');

add_action( 'password_reset', 'apet_password_reset', 10, 2 );

function apet_password_reset( $user, $new_pass ) {

    $user_id =$user->ID;
    $user_status = get_user_meta($user_id,'user_status',true);
    $user_type = get_user_meta($user_id,'user_type',true);
    if($user_type === 'app-user'){
        update_user_meta($user_id,'device_data',array());
        if($user_status === 'active'){

            require_once(ABSPATH.'wp-admin/includes/user.php');
            global $auth;
            $email = $user->data->user_email;

            $args = [
                'meta_query' => [
                    [
                        'key'   => 'user_type',
                        'value' => 'app-user'
                    ],
                    [
                        'key'   => 'pending_email',
                        'value' => $email
                    ]
                ]
            ];
            $pending_users = get_users($args);

            if(!empty($pending_users)) {

                foreach ($pending_users as $pending_user) {
                    $firebase_uid = get_user_meta($pending_user->ID, 'firebase_uid', true);
                    try {
                        $auth->deleteUser($firebase_uid);
                        wp_delete_user($pending_user->ID);
                    } catch (Exception $e) {

                    }
                }

                $curent_firebase_uid = get_user_meta($user_id, 'firebase_uid', true);

                $properties = [
                    'deleteProvider' => 'facebook.com'
                ];
                $updatedUser = $auth->updateUser($curent_firebase_uid, $properties);
                $updatedUser = $auth->changeUserPassword($curent_firebase_uid, $new_pass);
            }
        }
    }
}

function apet_check_device_data($user_id,$device_id){

    $device_data = get_user_meta($user_id,'device_data',true);

    return isset($device_data[$device_id]) && !empty($device_data[$device_id]);
}

function apet_add_monthly_schedule( $schedules ) {
    // add a 'monthly' schedule to the existing set
    $schedules['monthly'] = array(
        'interval' => 2592000,
        'display' => __('Once Monthly')
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'apet_add_monthly_schedule' );

// ========================== Airlines Lists wp_cron ======================================

if ( ! wp_next_scheduled( 'apet_get_airlines_list_hook' ) ) {
    wp_schedule_event( time(), 'monthly', 'apet_get_airlines_list_hook' );
}

add_action( 'apet_get_airlines_list_hook', 'apet_get_airlines_list_function' );

function apet_get_airlines_list_function() {


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.travelpayouts.com/data/en/airlines.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "x-access-token: ".APET_TRAVELPAYOUT_TOKEN
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        $errorMessage = "cURL Error #:" . $err;

        if (APET_ERROR_LOG) {
            $trace = debug_backtrace();
            $caller = array_shift($trace);
            $log_msg = $errorMessage . ' - ' . $caller['file'] . ':' . $caller['line'] . ' (' . date('Y-m-d H:i:s') . ')';
            file_put_contents(plugin_dir_path(__FILE__) . 'error.log', $log_msg . PHP_EOL, FILE_APPEND);
        }

    } else {
        $response = "var airlinesList = " .$response;

        file_put_contents(plugin_dir_path(__FILE__) . '/assets/js/airlines-list.js', $response);
    }

}

