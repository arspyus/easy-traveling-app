<?php

function apet_send_push_notification($platform, $deviceId, $title, $message, $data = []){

    if( APET_DEBUG ){
        return true;
    }

    $api_key = 'AAAAgv5i4cQ:APA91bEgypQraAR6eeHHL9wzap-x6lBBIA5YvCXFoNEnYD5AXcpJtjuLhmDgNhcZmWN7oQVxjlT8Zmi1b64Hxo6xGrLlD9RIidjqaaXTAME4ShNxw9EdubTSBvZCmPNvE6ECZSjV_PNw';

    $fields = array(
        'to' => $deviceId,
        'notification' => array(
            'body' => $message,
            'title' => $title,
            'sound' => 'default',
            'icon' => 'push',
            'badge' => 1,
            'color' => '#7cb342'
        ),
        'data' => $data
    );

    $headers = array
    (
        'Authorization: key=' . $api_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}