<?php

function apet_get_special_offers() {
    if (isset($_POST['action'])) {
        global $wpdb;
        $user_type = sanitize_text_field($_POST['user-type']);
        $token = isset($_POST['token']) ? sanitize_text_field($_POST['token']): false;
        $limit = sanitize_text_field($_POST['limit']);
        $offset = sanitize_text_field($_POST['offset']);
        $device_id = sanitize_text_field($_POST['device_id']);
        $platform = sanitize_text_field($_POST['platform']);
        $user_id = isset($_POST['user_id']) ? sanitize_text_field($_POST['user_id']): false;

        if($token){
            $token_info = apet_verify_user($token);

            if($token_info['success'] === true) {

                $uid = $token_info['uid'];

                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'user_type',
                            'value' => $user_type
                        ),
                        array(
                            'key' => 'firebase_uid',
                            'value' => $uid
                        )
                    )
                );
                $user = get_users($args);

                if (empty($user)) {
                    apet_remove_device_id($device_id, $user_id);
                    response_error_message('invalid_token');
                } elseif(!apet_check_device_data($user_id, $device_id)){
                    response_error_message('unauthorize_access');
                }
            }
        }

        $tour_id = isset($_POST['tour-id']) ? sanitize_text_field($_POST['tour-id']) : false;
        $agency_filter = $tour_id ? 'AND agency.id ="' . $tour_id . '"' : '';
        $date_filter = "AND DATE_ADD(CONVERT_TZ(offers.offer_deadline,'+00:00','".APET_WP_TIMEZONE."'),INTERVAL 1 DAY)>=CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."')";
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $offers_table = $wpdb->prefix . 'special_offers';
        $agency_offers_table = $wpdb->prefix . 'agency_offers';

        $result = $wpdb->get_results('
            SELECT SQL_CALC_FOUND_ROWS COUNT(DISTINCT agency_offers.offer_id) AS offers_count, offers.*, agency.phone as agency_phone, agency.photo as agency_photo, agency.email as agency_email, agency.website as agency_website, agency.tour_agent as agency_tour_agent, agency.id as agency_id, agency.average_rate as agency_average_rate, agency.coordinates as agency_coordinates, agency.country as agency_country, agency.city as agency_city, agency.address as agency_address
            FROM `' . $offers_table . '` AS offers 
            LEFT JOIN `' . $tour_table . '` AS agency ON offers.offer_author=agency.id 
            LEFT JOIN `' . $agency_offers_table . '` AS agency_offers ON offers.offer_author=agency_offers.offer_author
            WHERE offers.status="active" AND agency.status="active" ' . $agency_filter . ' ' . $date_filter . '
            GROUP BY  offers.offer_id ORDER BY offers.offer_id DESC  LIMIT ' . $limit . ' OFFSET ' . $offset
        , ARRAY_A);
        $count = $wpdb->get_results('SELECT FOUND_ROWS() as count', ARRAY_A);//must be next to query to count rows of previous query

        $response = empty($result) ? false : $result;
        if (!$response && $offset === '0') {
            response_error_message('no_offres');
        }
        $count = intval($count[0]['count']);
        $badges = apet_get_agency_badges();
        foreach ($response as $index => $spec_offer) {

            $response[$index]['days_left'] = apet_days_left($spec_offer['offer_deadline']);
            $response[$index]['offer_creation_date'] = apet_datetime_to_date(apet_time_zone_convert($response[$index]['offer_creation_date'], 'GMT', APET_WP_TIMEZONE));
            $response[$index]['trip_end'] = apet_datetime_to_date($response[$index]['trip_end']);
            $response[$index]['trip_start'] = apet_datetime_to_date($response[$index]['trip_start']);

            $response[$index]['agency'] = array(
                'id'            =>$spec_offer['agency_id'],
                'country'       =>$spec_offer['agency_country'],
                'city'          =>$spec_offer['agency_city'],
                'address'       =>$spec_offer['agency_address'],
                'tour_agent'    =>$spec_offer['agency_tour_agent'],
                'phone'         =>$spec_offer['agency_phone'],
                'email'         =>$spec_offer['agency_email'],
                'coordinates'   =>$spec_offer['agency_coordinates'],
                'average_rate'  =>$spec_offer['agency_average_rate'],
                'photo'         =>$spec_offer['agency_photo'],
                'website'       =>$spec_offer['agency_website'],
                'badges'        => isset($badges[$spec_offer['agency_id']]) ? $badges[$spec_offer['agency_id']] : null
            );

            unset(
                $response[$index]['agency_id'],
                $response[$index]['agency_country'],
                $response[$index]['agency_city'],
                $response[$index]['agency_address'],
                $response[$index]['agency_tour_agent'],
                $response[$index]['agency_phone'],
                $response[$index]['agency_email'],
                $response[$index]['agency_coordinates'],
                $response[$index]['agency_average_rate'],
                $response[$index]['agency_photo'],
                $response[$index]['agency_website']
            );

        }
        echo json_encode(array('data' => $response, 'success' => 'true', 'count' => $count));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_special_offers', 'apet_get_special_offers');
add_action('wp_ajax_apet_get_special_offers', 'apet_get_special_offers');

/* ========== ADMIN  ============== */

function apet_create_special_offer() {

    if (isset($_POST['tour-id'])) {
        global $wpdb, $apet_file_path;
        $table_name = $wpdb->prefix . "special_offers";
        $tour_table_name = $wpdb->prefix . "tour_agencies";

        $title = sanitize_text_field($_POST['title']);
        $description = sanitize_textarea_field($_POST['description']);
        $trip_start = sanitize_text_field($_POST['trip_start']);
        $trip_end = sanitize_text_field($_POST['trip_end']);
        $offer_deadline = sanitize_text_field($_POST['offer_deadline']);
        $price = sanitize_text_field($_POST['price']);
        $offer_author = sanitize_text_field($_POST['tour-id']);
        $user_id = sanitize_text_field($_POST['user-id']);


        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }
        $spec_offers_data = $wpdb->get_results("SELECT COUNT(*) as count FROM $table_name WHERE MONTH(CONVERT_TZ($table_name.offer_creation_date,'+00:00','".APET_WP_TIMEZONE."')) = MONTH(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."')) AND $table_name.offer_author=$offer_author", ARRAY_A);
        $spec_offers_count = !empty($spec_offers_data) ? $spec_offers_data[0]['count'] : false;
        $package_plan_data = $wpdb->get_results( "SELECT package_plan FROM $tour_table_name WHERE $tour_table_name.id=$offer_author ",ARRAY_A);
        $offer_limit = !empty($package_plan_data) && $package_plan_data[0]['package_plan'] ? unserialize($package_plan_data[0]['package_plan'])['special_offers_limit']: APET_PACKAGE_PLANS['free_plan']['special_offers_limit'];
        $offers_left = intval($offer_limit) - (intval($spec_offers_count));
        $offers_left = $offer_limit == -1 ? INF:  $offers_left;
        if ( $offers_left <= 0) {
            response_error_message('max_limit_reached');
        }

        if (!empty($_FILES)) {
            $ext = 'jpg';//pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $temp_img = plugin_dir_path($apet_file_path) . 'uploads/temp/' . $offer_author . '_spec_offer_photo.' . $ext;

            if (apet_image_upload($_FILES['file']['tmp_name'], $temp_img, array(2000, 2000)) !== true) {
                response_error_message('img_upload_failed');
            }
        }

        $offer_data = array(
            'title' => $title,
            'description' => $description,
            'trip_end' => $trip_end,
            'trip_start' => $trip_start,
            'offer_deadline' => $offer_deadline,
            'price' => $price,
            'status' => 'active',
            'offer_author' => $offer_author,
            'offer_creation_date' => date('Y-m-d H:i:s'),
        );

        $check = $wpdb->insert(
            $table_name, $offer_data
        );

        if ($check === false) {
            response_error_message('creating_offer_faild');
        }

        $offer_id = $wpdb->insert_id;
        $photo = NULL;
        if (!empty($_FILES)) {
            $img = preg_replace('/temp\/[0-9]+_/', 'special-offers/' . $offer_id . '_', $temp_img);
            if (rename($temp_img, $img) === false) {
                response_error_message('img_moving_failed');
            }
            $file_name = substr($img, strlen(plugin_dir_path($apet_file_path) . 'uploads/special-offers/'));
            $plugin_url = plugin_dir_url($img);
            $photo = $plugin_url . "" . $file_name;
            $set_photo = $wpdb->update($table_name, array('photo' => $photo), array('offer_id' => $offer_id));
        }
        $offer_data['photo'] = $photo;
        $offer_data['offer_id'] = $offer_id;
        $offer_data['days_left'] = apet_days_left($offer_deadline);
        $offer_data['offer_deadline'] = apet_datetime_to_date($offer_deadline);
        $editable_time = intval(APET_SPEC_OFFER_EDIT_TIME) * 60 * 1000;
        $offer_data['editable_time'] = $editable_time;
        if($editable_time > 0){
            $offer_data['editable'] = 'true';
        }else{
            $offer_data['editable'] = 'false';
        }
        //apet_send_spec_offer_notification();
        echo json_encode(array('data' => array('offers-left' => ($offer_limit === -1 ? -1 : $offers_left - 1), 'offer-limit' => $offer_limit, 'offer-data' => $offer_data), 'success' => 'true'));
        wp_die();
    } else {

        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_create_special_offer', 'apet_create_special_offer');

function apet_cancel_special_offer() {

    if (isset($_POST['user-id'])) {
        global $wpdb;
        $table_name = $wpdb->prefix . "special_offers";

        $user_id = sanitize_text_field($_POST['user-id']);
        $offer_id = sanitize_text_field($_POST['offer-id']);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }

        $update_status = $wpdb->query("UPDATE `$table_name` SET status='canceled' WHERE offer_id='$offer_id' ");
        if (!$update_status) {
            response_error_message('cancelation_failed');
        }
        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {

        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_cancel_special_offer', 'apet_cancel_special_offer');

function apet_edit_special_offer() {

    if (isset($_POST['user-id'])) {
        global $wpdb, $apet_file_path;
        $table_name = $wpdb->prefix . "special_offers";

        $user_id = sanitize_text_field($_POST['user-id']);
        $offer_id = sanitize_text_field($_POST['offer-id']);
        $edited_data = isset($_POST['edited-data']) ? json_decode(stripslashes(sanitize_text_field($_POST['edited-data'])), true) : array();
        $offer_data = json_decode(stripslashes(sanitize_text_field($_POST['offer-data'])), true);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }

        $edit_data = $wpdb->get_results("SELECT (UNIX_TIMESTAMP(CONVERT_TZ(offer_creation_date,'+00:00','".APET_WP_TIMEZONE."'))+(" . intval(APET_SPEC_OFFER_EDIT_TIME) . "*60) - UNIX_TIMESTAMP(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."')))*1000 as editable_time,photo FROM $table_name WHERE CONVERT_TZ(offer_creation_date,'+00:00','".APET_WP_TIMEZONE."') < DATE_ADD(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."'), INTERVAL " . APET_SPEC_OFFER_EDIT_TIME . " MINUTE) AND offer_id='$offer_id' ", ARRAY_A);
        if (empty($edit_data)) {
            response_error_message('offer_id_invalid');
        } else {
            $editable_time = $edit_data[0]['editable_time'];
            $photo = $edit_data[0]['photo'];
            if (intval($editable_time) <= 0) {
                response_error_message('editable_time_expired');
            }
        }


        if (!empty($_FILES)) {
            $ext = 'jpg';//pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $img = plugin_dir_path($apet_file_path) . 'uploads/special-offers/' . $offer_id . '_spec_offer_photo.' . $ext;

            if (apet_image_upload($_FILES['file']['tmp_name'], $img, array(2000, 2000)) !== true) {
                response_error_message('img_upload_failed');
            }
            $file_name = substr($img, strlen(plugin_dir_path($apet_file_path) . 'uploads/special-offers/'));
            $plugin_url = plugin_dir_url($img);
            $photo = $plugin_url . "" . $file_name;
            $edited_data['photo'] =   parse_url($photo, PHP_URL_QUERY) ?   $photo."&nocache=" . uniqid() : $photo."?nocache=" . uniqid();
        }


        $edited_result = $wpdb->update(
            $table_name, $edited_data, array('offer_id' => $offer_id)
        );

        if ($edited_result === false) {
            response_error_message('editing_failed');
        }
        if ($photo) {
            $offer_data['photo'] = $photo;
        }

        $offer_data['offer_id'] = $offer_id;
        $offer_data['offer_author'] = $user_id;
        $offer_data['days_left'] = apet_days_left($offer_data['offer_deadline']);
        $offer_data['offer_deadline']  = apet_datetime_to_date($offer_data['offer_deadline']);
        $offer_data['editable_time'] = $editable_time;
        if($editable_time > 0){
            $offer_data['editable'] = 'true';
        }else{
            $offer_data['editable'] = 'false';
        }

        echo json_encode(array('success' => 'true', 'offer-data' => $offer_data));
        wp_die();
    } else {

        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_edit_special_offer', 'apet_edit_special_offer');

function apet_get_tour_special_offers() {

    if (isset($_POST['user-id'])) {
        global $wpdb;
        $table_name = $wpdb->prefix . "special_offers";
        $role = apet_get_logged_user_role();
        $user_id = sanitize_text_field($_POST['user-id']);
        $tour_id = sanitize_text_field($_POST['tour-id']);
        $limit = (isset($_POST['limit']) && isset($_POST['offset'])) ? "LIMIT " . sanitize_text_field($_POST['offset']) . ", " . sanitize_text_field($_POST['limit']) : "";

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if (($tour_status === 'deactivated' || $tour_status === '') && $role !== 'tour-super-admin') {
                response_error_message('unauthorize_access');
            }
        }

        $spec_offers_table = $wpdb->prefix . 'special_offers';
        $tour_table = $wpdb->prefix . 'tour_agencies';

        $filter_options = isset($_POST['filter']) ? json_decode(stripslashes(sanitize_text_field($_POST['filter'])), true) : false;
        $filter = '';
        if($filter_options !== false){

            if(isset($filter_options['search']) && !empty($filter_options['search'])){
                if($tour_id === 'all' && $role === 'tour-super-admin'){
                    $filter.= " AND (offers.title LIKE '%" . $filter_options['search'] . "%' 
                            OR agency.tour_agent LIKE '%" . $filter_options['search'] . "%'
                            ) ";
                }else{
                    $filter.= " AND offers.title LIKE '%" . $filter_options['search'] . "%'";
                }

            }
            if(isset($filter_options['status']) && !empty($filter_options['status'])) {

                switch ($filter_options['status']) {
                    case 'active':
                        $filter.= " AND (DATE_ADD(CONVERT_TZ(offers.offer_deadline,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')) AND offers.status = 'active' ";
                        break;
                    case 'expired':
                        $filter.= " AND (DATE_ADD(CONVERT_TZ(offers.offer_deadline,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) < CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')) AND offers.status = 'active' ";
                        break;
                    case 'deleted':
                        $filter.= " AND offers.status = 'canceled' ";
                        break;
                }

            }
        }

        if ($tour_id === 'all' && $role === 'tour-super-admin') {
            $result = $wpdb->get_results("SELECT  offers.*,agency.tour_agent,agency.phone,agency.email FROM `$spec_offers_table` as offers LEFT JOIN `$tour_table` as agency ON offers.offer_author =agency.id  
            WHERE  MONTH(CONVERT_TZ(offers.offer_creation_date,'+00:00','".APET_WP_TIMEZONE."')) = MONTH(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."')) $filter ORDER BY offers.offer_creation_date DESC $limit", ARRAY_A);

            foreach ($result as $index => $spec_offer) {
                $result[$index]['days_left'] = apet_days_left($spec_offer['offer_deadline']);
                $result[$index]['offer_deadline']  = apet_datetime_to_date($spec_offer['offer_deadline']);
                $result[$index]['offer_creation_date'] = apet_datetime_to_date(apet_time_zone_convert($result[$index]['offer_creation_date'], 'GMT', APET_WP_TIMEZONE));
                $result[$index]['trip_end'] = apet_datetime_to_date($result[$index]['trip_end']);
                $result[$index]['trip_start'] = apet_datetime_to_date($result[$index]['trip_start']);
            }
            $result = !empty($result) ? $result : array();
            echo json_encode(array('data' => array(
                'offers-left' => NULL,
                //'offer-limit' => NULL,
                'specOffers' => $result),
                'success' => 'true'));
            wp_die();
        } else {


            $tour_data = $wpdb->get_results("SELECT id,package_plan FROM $tour_table WHERE tour_admin='$user_id'", ARRAY_A);
            $tour_id = !empty($tour_data) ? $tour_data[0]['id'] : '';
            $offer_limit = !empty($tour_data) && $tour_data[0]['package_plan'] ? unserialize($tour_data[0]['package_plan'])['special_offers_limit'] : APET_PACKAGE_PLANS['free_plan']['special_offers_limit'];
            $spec_offers = false;
            $spec_offers_count = 0;
            $offers_left = 0;
            if ($tour_id) {
                $spec_offers = $wpdb->get_results("SELECT *,IF(CONVERT_TZ(offer_creation_date,'+00:00','".APET_WP_TIMEZONE."') >(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."') - INTERVAL " . APET_SPEC_OFFER_EDIT_TIME . " MINUTE),'true','false') as editable,
                (UNIX_TIMESTAMP(CONVERT_TZ(offer_creation_date,'+00:00','".APET_WP_TIMEZONE."'))+(" . intval(APET_SPEC_OFFER_EDIT_TIME) . "*60) - UNIX_TIMESTAMP(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."')))*1000 as editable_time  FROM $spec_offers_table as offers
                WHERE MONTH(CONVERT_TZ(offer_creation_date,'+00:00','".APET_WP_TIMEZONE."')) = MONTH(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."')) AND offer_author='$tour_id' $filter ORDER BY offer_creation_date DESC $limit", ARRAY_A);
                $spec_offers_count = $wpdb->get_results("SELECT COUNT(*) as count FROM $spec_offers_table as offers WHERE MONTH(offer_creation_date) = MONTH(CURDATE()) AND offer_author='$tour_id'", ARRAY_A);
                $spec_offers_count = !empty($spec_offers_count) ? $spec_offers_count[0]['count'] : 0;
                $spec_offers = !empty($spec_offers) ? $spec_offers : array();
                $offers_left = intval($offer_limit) - intval($spec_offers_count);
                $offers_left = $offers_left<=0 ? 0 : $offers_left;
                $offers_left = $offer_limit ===  -1 ? -1 : $offers_left;
            }

            if ($spec_offers === false) {
                response_error_message('failed_to_fetch');
            }
            if (!empty($spec_offers)) {
                foreach ($spec_offers as $index => $spec_offer) {
                    $spec_offers[$index]['days_left'] = apet_days_left($spec_offer['offer_deadline']);
                    $spec_offers[$index]['offer_deadline']  = apet_datetime_to_date($spec_offer['offer_deadline']);
                    $spec_offers[$index]['offer_creation_date'] = apet_datetime_to_date(apet_time_zone_convert($spec_offers[$index]['offer_creation_date'], 'GMT', APET_WP_TIMEZONE));
                    $spec_offers[$index]['trip_end'] = apet_datetime_to_date($spec_offers[$index]['trip_end']);
                    $spec_offers[$index]['trip_start'] = apet_datetime_to_date($spec_offers[$index]['trip_start']);
                }
            }
            echo json_encode(array('data' => array(
                'offers-left' => $offers_left,
                //'offer-limit' => $offer_limit,
                'specOffers' => $spec_offers),
                'success' => 'true'));
            wp_die();
        }
    } else {

        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_tour_special_offers', 'apet_get_tour_special_offers');


//apet_send_spec_offer_notification();
function apet_send_spec_offer_notification(){
    $args = array(
        'meta_query' => array(
            array(
                'key' => 'user_type',
                'value' => 'app-user'
            ),
            array(
                'key' => 'user_status',
                'value' => 'active'
            )
        )
    );

    $users = get_users($args);
    $messages = array(
        'hy' => array('title' => 'Հատուկ առաջարկ', 'message' => 'Դուք ունեք նոր հատուկ առաջարկ:'),
        'en' => array('title' => 'Special offer', 'message' => 'You have new special offer.'),
        'ru' => array('title' => 'Специальное предложение', 'message' => 'У вас есть новое специальное предложение.')
    );
    foreach ($users as $index=>$user) {
        $usermeta = get_user_meta($user->ID);
        if(isset($usermeta['device_data'])) {
            $all_device_data = $usermeta['device_data'][0];
            $device_data = unserialize($all_device_data);

            foreach ($device_data as $key => $device) {
                $ln = $device['language'];
                $push_token_id = isset($device['push_token_id']) ? $device['push_token_id'] : '';
                if ($push_token_id) {
                    apet_send_push_notification($device['platform'], $device['push_token_id'], $messages[$ln]['title'], $messages[$ln]['message'], 'special-offer');
                }
            }
        }
    }

}

