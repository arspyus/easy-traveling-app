<?php

function apet_get_agency_offer() {
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        global $wpdb;
        $offer_id = sanitize_text_field($_POST['offer-id']);
        $platform = sanitize_text_field($_POST['platform']);
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            }
        }else{
            response_error_message('invalid_token');
        }
        $offer_table = $wpdb->prefix . 'agency_offers';
        $requests_table = $wpdb->prefix . 'requests';
        $tour_table = $wpdb->prefix . 'tour_agencies';

        $request_id = $results = $wpdb->get_row("
                SELECT 
                  offers.request_id
                FROM $offer_table offers
                WHERE offers.offer_id='$offer_id'" , ARRAY_A);

        if (empty($request_id)) {
            response_error_message('getting_offer_faild');
        }else{
            $request_id = $request_id['request_id'];
        }

        $offers_result = $wpdb->get_results("
                SELECT 
                    agencies.id as agency_id,
                    agencies.country as agency_country,
                    agencies.city   as agency_city,
                    agencies.address as agency_address,
                    agencies.tour_agent as agency_tour_agent,
                    agencies.phone as agency_phone,
                    agencies.email as agency_email,
                    agencies.coordinates as agency_coordinates,
                    agencies.average_rate as agency_average_rate,
                    agencies.photo as agency_photo,
                    agencies.website as agency_website,
                    offers.*,
                    requests.country as request_country,
                    requests.city as request_city,
                    requests.children,
                    requests.adults,
                    requests.infant 
                FROM $offer_table offers
                LEFT JOIN $requests_table requests ON requests.request_id = offers.request_id 
                LEFT JOIN $tour_table agencies ON agencies.id = offers.offer_author 
                WHERE offers.request_id = '$request_id'" , ARRAY_A);

        if (empty($offers_result)) {
            response_error_message('getting_offer_faild');
        }else{

            $badges = apet_get_agency_badges();

            foreach ($offers_result as $key => $offer){
                $offers_result[$key]['days_left'] = apet_days_left($offer['offer_deadline']);
                $offers_result[$key]['doc_list'] = $offer['doc_list'] ? unserialize($offer['doc_list']) : NULL;
                $offers_result[$key]['days_left'] = apet_days_left($offer['offer_deadline']);
                $offers_result[$key]['doc_deadline'] = apet_datetime_to_date($offer['doc_deadline']);
                $offers_result[$key]['hotel_checkin'] = apet_datetime_to_date($offer['hotel_checkin']);
                $offers_result[$key]['hotel_checkout'] = apet_datetime_to_date($offer['hotel_checkout']);
                $offers_result[$key]['offer_deadline'] = apet_datetime_to_date($offer['offer_deadline']);
                $offers_result[$key]['arrival'] = $offer['arrival'] ? apet_datetime_to_date($offer['arrival']): NULL;
                $offers_result[$key]['departure'] = $offer['departure'] ?  apet_datetime_to_date($offer['departure']): NULL;
                $offers_result[$key]['offer_creation_date'] = apet_datetime_to_date(apet_time_zone_convert($offer['offer_creation_date'], 'GMT', APET_WP_TIMEZONE));

                $offers_result[$key]['country'] = stripslashes($offer['request_country']);
                $offers_result[$key]['city'] =  stripslashes($offer['request_city']);
                $offers_result[$key]['request_id'] = $offer['request_id'];

                $offers_result[$key]['agency'] = array(
                    'id'            =>$offer['agency_id'],
                    'country'       =>$offer['agency_country'],
                    'city'          =>$offer['agency_city'],
                    'address'       =>$offer['agency_address'],
                    'tour_agent'    =>$offer['agency_tour_agent'],
                    'phone'         =>$offer['agency_phone'],
                    'email'         =>$offer['agency_email'],
                    'coordinates'   =>$offer['agency_coordinates'],
                    'average_rate'  =>$offer['agency_average_rate'],
                    'photo'         =>$offer['agency_photo'],
                    'website'       =>$offer['agency_website'],
                    'badges'        => isset($badges[$offer['offer_author']]) ? $badges[$offer['offer_author']] : null
                );

                if($offer['departure_departure_city']){
                    $offers_result[$key]['transportation'] = array(
                        'type' => $offer['trans_type'],
                        'departure' => array(
                            'type' =>$offer['departure_flight_type'],
                            'departure_date'=>date('Y-m-d' ,strtotime($offer['departure_departure_date_time'])),
                            'departure_time'=>date('H:i' ,strtotime($offer['departure_departure_date_time'])),
                            'arrival_date'=>date('Y-m-d' ,strtotime($offer['departure_destination_arrival_date_time'])),
                            'arrival_time'=>date('H:i' ,strtotime($offer['departure_destination_arrival_date_time'])),
                            'departure_airport'=>$offer['departure_departure_air_code'],
                            'arrival_airport'=>$offer['departure_destination_arrival_air_code'],
                            'departure_city'=>$offer['departure_departure_city'],
                            'arrival_city'=>$offer['departure_destination_arrival_city'],
                            'air_company_logo'=> 'https://pics.avs.io/200/200/' . $offer['departure_airlines_iata'] . '.png',
                        ),
                        'arrival' => array(
                            'type' =>$offer['arrival_flight_type'],
                            'departure_date'=> date('Y-m-d' ,strtotime($offer['arrival_departure_date_time'])),
                            'departure_time'=> date('H:i' ,strtotime($offer['arrival_departure_date_time'])),
                            'arrival_date'=> date('Y-m-d' ,strtotime($offer['arrival_destination_arrival_date_time'])),
                            'arrival_time'=> date('H:i' ,strtotime($offer['arrival_destination_arrival_date_time'])),
                            'departure_airport'=>$offer['arrival_departure_air_code'],
                            'arrival_airport'=>$offer['arrival_destination_arrival_air_code'],
                            'departure_city'=>$offer['arrival_departure_city'],
                            'arrival_city'=>$offer['arrival_destination_arrival_city'],
                            'air_company_logo'=> 'https://pics.avs.io/200/200/' . $offer['arrival_airlines_iata'] . '.png',
                        ),
                    );
                }




                unset(
                    $offers_result[$key]["arrival_destination_arrival_air_code"],
                    $offers_result[$key]["arrival_departure_air_code"],
                    $offers_result[$key]["departure_destination_arrival_air_code"],
                    $offers_result[$key]["departure_departure_air_code"],
                    $offers_result[$key]["departure_departure_city"],
                    $offers_result[$key]["departure_departure_airport"],
                    $offers_result[$key]["departure_departure_air_company"],
                    $offers_result[$key]["departure_departure_date_time"],
                    $offers_result[$key]["departure_destination_arrival_city"],
                    $offers_result[$key]["departure_destination_arrival_airport"],
                    $offers_result[$key]["departure_destination_arrival_date_time"],
                    $offers_result[$key]["arrival_departure_airport"],
                    $offers_result[$key]["arrival_departure_air_company"],
                    $offers_result[$key]["arrival_departure_date_time"],
                    $offers_result[$key]["arrival_destination_arrival_date_time"],
                    $offers_result[$key]["arrival_destination_arrival_airport"],
                    $offers_result[$key]["arrival_departure_city"],
                    $offers_result[$key]["arrival_destination_arrival_city"],
                    $offers_result[$key]['agency_id'],
                    $offers_result[$key]['agency_country'],
                    $offers_result[$key]['agency_city'],
                    $offers_result[$key]['agency_address'],
                    $offers_result[$key]['agency_tour_agent'],
                    $offers_result[$key]['agency_phone'],
                    $offers_result[$key]['agency_email'],
                    $offers_result[$key]['agency_coordinates'],
                    $offers_result[$key]['agency_average_rate'],
                    $offers_result[$key]['agency_photo'],
                    $offers_result[$key]['agency_website']
                );

            }
        }

        echo json_encode(array('data' => $offers_result, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_agency_offer', 'apet_get_agency_offer');
add_action('wp_ajax_apet_get_agency_offer', 'apet_get_agency_offer');
