<?php
defined('ABSPATH') or die('No script kiddies please!');

function apet_get_bonus() {
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {

        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $platform = sanitize_text_field($_POST['platform']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            }else{
                $user_id = $user[0]->ID;
                $all_data = get_user_meta($user_id);
                $user_email = $all_data['email'][0];
                $first_name = $all_data['first_name'][0];
                $last_name = $all_data['last_name'][0];
            }
        }else{
            response_error_message('invalid_token');
        }

        $bonus = get_user_meta($user_id, 'bonus', true);
        if ($bonus === '0') {
            response_error_message('invalid_request');
        }
        $bonus_id = apet_get_bonus_id($user_id, $bonus);
        $bonus_data = array(
            'bonus_id' => $bonus_id,
            'withdrawn_bonus' => $bonus,
        );
        update_user_meta($user_id, 'bonus', '0', $bonus);
        update_user_meta($user_id, 'bonus_data', $bonus_data);

        $admin_user = get_users(array(
            'role' => 'tour-super-admin',
        ));
        $style = '<style>.wrapper{background-color:#d6d6d6!important;padding:10px!important}.wrapper>p{color:#515151!important}.table-wrapper{background-color:#f6f6f6!important;border-radius:10px!important;padding:10px!important;width:500px!important;margin:0 auto!important}.login-text{display:block!important;font-family:arial,sans-serif!important;font-size:18px!important;padding:8px!important;text-align:center!important;color:#515151!important}.login-text a{text-decoration:none!important;padding:10px!important;background-color:#7cb342!important;color:#fff!important;border-radius:10px!important;display:inline-block!important}.table-wrapper table{background-color:#f6f6f6!important;font-family:arial,sans-serif!important;border-collapse:collapse!important;width:100%!important;margin:0 auto 10px!important}.table-wrapper thead{font-size:20px!important;border-bottom:1px solid #ddd!important}.table-wrapper thead th{color:#515151!important}.table-wrapper td,p,th{text-align:left!important;padding:0 8px!important;font-size:18px!important}.table-wrapper .first{padding-top:8px!important}.table-wrapper .odd{padding-bottom:4px!important;color:#515151!important;font-size:18px!important}.table-wrapper .even{padding-bottom:8px!important;color:#909090!important;font-size:16px!important}</style>';
        if (!empty($admin_user)) {
            $admin_email = $admin_user[0]->user_email;

            $subject = 'App User Bonus Request';

            $body = "<div class='wrapper'>"
                        ."<p>Hi</p>"
                        ."<div class='table-wrapper'>"
                            ."<table>"
                                ."<thead>"
                                    ."<tr>"
                                        ."<th><Strong>Bonus details:</Strong></th>"
                                    ."</tr>"
                                ."</thead>"
                                ."<tbody>"
                                    ."<tr class=\"first odd\"><td><Strong>User Email:  ".$user_email."</Strong></td></tr>"
                                    ."<tr class=\"even\"><td>First Name:  " . $first_name . "</td></tr>"
                                    ."<tr class=\"odd\"><td><Strong>Last Name:  " . $last_name . "</Strong></td></tr>"
                                    ."<tr class=\"even\"><td>Bonus Count:  " . $bonus . "</td></tr>"
                                    ."<tr class=\"odd\"><td><Strong>Bonus ID:  " . $bonus_id . "</Strong></td></tr>"
                                ."</tbody>"
                            ."</table>"
                        ."</div>"
                        ."<div class='login-text'>Thanks,<br>EasyTraveling team.</div>"
                    ."</div>";

            $admin_email_success = apet_send_mail($admin_email, $subject, $body, $style);
        }


        $language = apet_get_device_language($device_id, $user_id);
        $to_user_subject = array(
            'hy' => 'Բոնուսի հարցում',
            'en' => 'Bonus request',
            'ru' => 'Запрос бонуса'
        );

        $to_user_body = array(
            'en' => "<div class='wrapper'>"
                        ."<p>Hi " . ucfirst($first_name) . "</p>"
                        ."<p>Your bonus request successfully completed.</p>"
                        ."<div class='table-wrapper'>"
                            ."<table>"
                                ."<thead>"
                                    ."<tr>"
                                        ."<th><Strong>Bonus details:</Strong></th>"
                                    ."</tr>"
                                ."</thead>"
                                ."<tbody>"
                                    ."<tr class=\"first odd\"><td><Strong>Amount:  ".$bonus."</Strong></td></tr>"
                                    ."<tr class=\"even\"><td>Bonus ID:  " . $bonus_id . "</td></tr>"
                                    ."<tr class=\"odd\"><td><Strong>One bonus unit equal to:  " . APET_BONUS_UNIT_PRICE . " AMD</Strong></td></tr>"
                                ."</tbody>"
                            ."</table>"
                        ."</div>"
                        ."<div class='login-text'>Thanks,<br>EasyTraveling team.</div>"
                    ."</div>",
            'hy' => "<div class='wrapper'>"
                        ."<p>Բարև " . ucfirst($first_name) . "</p>"
                        ."<p>Ձեր բոնուսային հարցումն հաջողությամբ ավարտվեց:</p>"
                        ."<div class='table-wrapper'>"
                            ."<table>"
                                ."<tbody>"
                                    ."<tr class=\"first odd\"><td><Strong>Քանակը:  ".$bonus."</Strong></td></tr>"
                                    ."<tr class=\"even\"><td>Բոնուս ID:  " . $bonus_id . "</td></tr>"
                                    ."<tr class=\"odd\"><td><Strong>Մեկ բոնուսային միավորը համարժեք է  " . APET_BONUS_UNIT_PRICE . " AMD</Strong></td></tr>"
                                ."</tbody>"
                            ."</table>"
                        ."</div>"
                        ."<div class='login-text'>Շնորհակալություն,<br>EasyTraveling թիմ:</div>"
                    ."</div>",
            'ru' => "<div class='wrapper'>"
                        ."<p>Привет " . ucfirst($first_name) . "</p>"
                        ."<p>Ваш запрос бонуса успешно завершен.</p>"
                        ."<div class='table-wrapper'>"
                            ."<table>"
                                ."<tbody>"
                                    ."<tr class=\"first odd\"><td><Strong>Количество:  ".$bonus."</Strong></td></tr>"
                                    ."<tr class=\"even\"><td>Бонус ID:  " . $bonus_id . "</td></tr>"
                                    ."<tr class=\"odd\"><td><Strong>Единица бонуса равна  " . APET_BONUS_UNIT_PRICE . " AMD</Strong></td></tr>"
                                ."</tbody>"
                            ."</table>"
                        ."</div>"
                        ."<div class='login-text'>Спасибо,<br>Команда EasyTraveling</div>"
                    ."</div>"

        );
        $user_email_success = apet_send_mail($user_email, $to_user_subject[$language], $to_user_body[$language],$style);


        $response = array('bonus_id' => $bonus_id, 'withdrawn_bonus' => $bonus, 'bonus' => '0', 'bonus_unit_price  ' => APET_BONUS_UNIT_PRICE);
        echo json_encode(array('data' => $response, 'success' => 'true'));
        wp_die();
    } else {

        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_bonus', 'apet_get_bonus');
add_action('wp_ajax_apet_get_bonus', 'apet_get_bonus');

//============ADMIN==================

function apet_get_user_from_bonus_id() {
    if (isset($_POST['user_id'])) {
        $user_id = sanitize_text_field($_POST['user_id']);
        $bonus_id = sanitize_text_field($_POST['bonus_id']);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        }

        $bonus_info = get_bonus_info($bonus_id);
        if($bonus_info===false){
            response_error_message('invalid_bonus_id');
        }
        $app_user_id  = $bonus_info['user_id'];
        $all_data = get_user_meta($app_user_id);
        if(empty($all_data)){
            response_error_message('invalid_bonus_id');
        }
        $user_data = array();
        $user_data['bonus_amount'] = $bonus_info['bonus'];
        $user_data['lastname'] = $all_data['last_name'][0];
        $user_data['firstname'] = $all_data['first_name'][0];
        $user_data['phone'] = $all_data['phone'][0];
        $user_data['email'] = $all_data['email'][0];
        echo json_encode(array('data' => $user_data, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_user_from_bonus_id', 'apet_get_user_from_bonus_id');

function apet_add_user_bonus(){
    if (isset($_POST['user_id'])) {
        $user_id = sanitize_text_field($_POST['user_id']);
        $user_email = sanitize_text_field($_POST['user_email']);
        $bonus_to_add = sanitize_text_field($_POST['bonus_amount']);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int) $user_id) {
            response_error_message('unauthorize_access');
        }

        $user = get_user_by('email', $user_email);
        if($user !== false && array_search("app-user", $user->roles) !== false){

            $bonus = get_user_meta($user->ID, 'bonus', true);
            update_user_meta($user->ID, 'bonus', $bonus + $bonus_to_add, $bonus);
            update_user_meta($user->ID, 'bonus_data', null);
        }else{

            response_error_message('wrong_user_email');
        }

        echo json_encode(array('success' => 'true'));
        wp_die();
    }else {
        response_error_message('unauthorize_access');
    }
}
add_action('wp_ajax_apet_add_user_bonus', 'apet_add_user_bonus');
