<?php

function apet_get_tour_agencies() {
    if (isset($_POST['action'])) {
        global $wpdb;
        $tours_table = $wpdb->prefix . 'tour_agencies';
        $rate_table = $wpdb->prefix . 'tour_agencies_rate';
        $agency_offers_table = $wpdb->prefix . 'agency_offers';
        $user_id = isset($_POST['user_id']) ? sanitize_text_field($_POST['user_id']): false;
        $token = isset($_POST['token']) ? sanitize_text_field($_POST['token']) : false;
        $offset = sanitize_text_field($_POST['offset']);
        $limit = sanitize_text_field($_POST['limit']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $device_id = sanitize_text_field($_POST['device_id']);

        if($token){
            $token_info = apet_verify_user($token);

            if($token_info['success'] === true){
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'user_type',
                            'value' => $user_type
                        ),
                        array(
                            'key' => 'firebase_uid',
                            'value' => $token_info['uid']
                        )
                    )
                );
                $user = get_users($args);
                if (empty($user) || !apet_check_device_data($user_id, $device_id)) {
                    response_error_message('invalid_token');
                }else{
                    $user_id = $user[0]->ID;
                }
            }else{
                response_error_message('invalid_token');
            }

            $result = $wpdb->get_results('
                SELECT
                    SQL_CALC_FOUND_ROWS COUNT(DISTINCT agency_offers.offer_id) AS offers_count,
                    tours.*,
                    SUM(DISTINCT IF(rates.user_id = "' . $user_id . '", rates.rate, NULL)) AS is_rated 
                FROM `' . $tours_table . '` AS tours 
                LEFT JOIN `' . $rate_table . '` AS rates ON tours.id=rates.tour_id 
                LEFT JOIN `' . $agency_offers_table . '` AS agency_offers ON tours.id=agency_offers.offer_author 
                WHERE tours.status="active" 
                GROUP BY tours.id 
                ORDER BY offers_count DESC, tours.id DESC LIMIT ' . $limit . ' OFFSET ' . $offset, ARRAY_A);
        }else{
            $result = $wpdb->get_results('
            SELECT SQL_CALC_FOUND_ROWS COUNT(DISTINCT agency_offers.offer_id) AS offers_count, tours.* 
            FROM `' . $tours_table . '` AS tours 
            LEFT JOIN `' . $rate_table . '` AS rates ON tours.id=rates.tour_id 
            LEFT JOIN `' . $agency_offers_table . '` AS agency_offers ON tours.id=agency_offers.offer_author 
            WHERE tours.status="active" 
            GROUP BY tours.id ORDER BY offers_count DESC, tours.id DESC LIMIT ' . $limit . ' OFFSET ' . $offset
                , ARRAY_A);
        }
        $count = $wpdb->get_results('SELECT FOUND_ROWS() as count', ARRAY_A); //must be next to query to count rows of previous query

        $response = empty($result) ? false : $result;
        if (!$response && $offset === '0') {
            response_error_message('no_tour_agencies');
        }
        $count = intval($count[0]['count']);
        $badges = apet_get_agency_badges();
        foreach ($response as $index => $agency) {
            if (isset($badges[$agency['id']])) {
                $response[$index]['badges'] = $badges[$agency['id']];
            }
        }
        echo json_encode(array('data' => $response, 'success' => 'true', 'count' => $count));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_tour_agencies', 'apet_get_tour_agencies');
add_action('wp_ajax_apet_get_tour_agencies', 'apet_get_tour_agencies');

function apet_get_single_tour_agency() {
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['tour-id']) && isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        global $wpdb;
        $rate_table = $wpdb->prefix . 'tour_agencies_rate';
        $tours_table = $wpdb->prefix . 'tour_agencies';
        $tour_id = sanitize_text_field($_POST['tour-id']);
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            }else{
                $user_id = $user[0]->ID;

                $result = $wpdb->get_results("
                    SELECT
                        tours.*,
                        SUM(DISTINCT IF(rates.user_id = $user_id, rates.rate, NULL)) AS is_rated 
                    FROM `$tours_table` AS tours 
                        LEFT JOIN `$rate_table` AS rates ON tours.id=rates.tour_id 
                        WHERE tours.status='active' AND tours.id = $tour_id", ARRAY_A);

                $badges = apet_get_agency_badges();
                if (isset($badges[$result[0]['id']])) {
                    $result[0]['badges'] = $badges[$result[0]['id']];
                }
                echo json_encode(array('success' => 'true','data'=>$result[0]));
                wp_die();
            }
        }else{
            response_error_message('invalid_token');
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_single_tour_agency', 'apet_get_single_tour_agency');
add_action('wp_ajax_apet_get_single_tour_agency', 'apet_get_single_tour_agency');

function apet_rate_tour_agency() {
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['tour-id']) && isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        global $wpdb;
        $rate_table = $wpdb->prefix . 'tour_agencies_rate';
        $tour_agency_table = $wpdb->prefix . 'tour_agencies';
        $tour_id = sanitize_text_field($_POST['tour-id']);
        $rate = sanitize_text_field($_POST['rate']);
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            }else{
                $user_id = $user[0]->ID;
            }
        }else{
            response_error_message('invalid_token');
        }

        $result = $wpdb->replace($rate_table, array(
            "tour_id" => $tour_id,
            "user_id" => $user_id,
            "rate" => $rate
        ));
        if ($result === false) {
            response_error_message('rating_failed');
        }

        $tour_agency_data = $wpdb->get_results("SELECT AVG(rate) as average_rate , COUNT(rate) as voters_number FROM `$rate_table` WHERE tour_id='$tour_id' ", ARRAY_A);
        $voters_number = intval($tour_agency_data[0]['voters_number']);
        $average_rate = floatval($tour_agency_data[0]['average_rate']);
        $update_rate = $wpdb->query("UPDATE `$tour_agency_table` SET voters_number='$voters_number',average_rate='$average_rate' WHERE id='$tour_id' ");

        if ($update_rate === false || empty($tour_agency_data)) {
            response_error_message('rating_failed');
        }
        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_rate_tour_agency', 'apet_rate_tour_agency');
add_action('wp_ajax_apet_rate_tour_agency', 'apet_rate_tour_agency');

//==================ADMIN=========================

function apet_change_tour_settings() {
    if (isset($_POST['tour_id'])) {
        global $wpdb, $apet_file_path;
        $tour_agency_table = $wpdb->prefix . 'tour_agencies';
        $address = sanitize_text_field($_POST['address']);
        $city = sanitize_text_field($_POST['city']);
        $latitude = sanitize_text_field($_POST['latitude']);
        $longitude = sanitize_text_field($_POST['longitude']);

        $country = sanitize_text_field($_POST['country']);
        $email = sanitize_text_field($_POST['email']);
        $website = sanitize_text_field($_POST['website']);
        $phone = sanitize_text_field($_POST['phone']);
        $tour_agent = sanitize_text_field($_POST['tour_agent']);
        $prefered_countries = sanitize_text_field($_POST['prefered_countries']);
        $prefered_countries = json_decode(stripslashes($prefered_countries));
        $prefered_countries = empty($prefered_countries) ? NULL : serialize($prefered_countries);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $user_id = sanitize_text_field($_POST['user_id']);


        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }
        $tour_status = $wpdb->get_results("SELECT status FROM `$tour_agency_table` WHERE tour_admin='$user_id' ", ARRAY_A);
        $tour_status = !empty($tour_status) ? $tour_status[0]['status'] : '';
        $photo = '';
        if (!empty($_FILES)) {
            $ext = 'jpg'; //pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $img = plugin_dir_path($apet_file_path) . 'uploads/tour-agency/' . $tour_id . '_tour_logo.' . $ext;

            if (apet_image_upload($_FILES['file']['tmp_name'], $img, array(500, 500)) !== true) {
                response_error_message('img_upload_failed');
            }

            $file_name = substr($img, strlen(plugin_dir_path($apet_file_path) . 'uploads/tour-agency/'));
            $plugin_url = plugin_dir_url($img);
            $photo = $plugin_url . "" . $file_name;
            $photo = parse_url($photo, PHP_URL_QUERY) ? $photo . "&nocache=" . uniqid() : $photo . "?nocache=" . uniqid();
        }
        $coordinates = $latitude . ", " . $longitude;
        $tour_data = array(
            'address' => $address,
            'city' => $city,
            'coordinates' => $coordinates,
            'country' => $country,
            'email' => $email,
            'website' => $website,
            'phone' => $phone,
            'status' => 'active',
            'tour_agent' => $tour_agent,
            'prefered_countries' => $prefered_countries
        );
        if ($photo !== '') {
            $tour_data['photo'] = $photo;
        }
        $update_tour = $wpdb->update($tour_agency_table, $tour_data, array('id' => $tour_id));

        if ($update_tour === false) {
            response_error_message('updating_failed');
        } else {
            echo json_encode(array('success' => 'true', 'data' => array('status' => $tour_status, 'photo' => $photo)));
            wp_die();
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_change_tour_settings', 'apet_change_tour_settings');

function apet_get_tour_settings() {
    if (isset($_POST['tour_id'])) {
        global $wpdb;
        $tour_agency_table = $wpdb->prefix . 'tour_agencies';
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $user_id = sanitize_text_field($_POST['user_id']);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }
        $tour_data = $wpdb->get_row("SELECT * FROM `$tour_agency_table` WHERE id='$tour_id' ", ARRAY_A);
        if (!$tour_data) {
            response_error_message('failed_to_fetch');
        } else {
            $tour_data['prefered_countries'] = $tour_data['prefered_countries'] ? unserialize($tour_data['prefered_countries']) : NULL;
            $coordinates = explode(', ', $tour_data['coordinates']);
            unset($tour_data['coordinates']);
            $tour_data['latitude'] = floatval($coordinates[0]);
            $tour_data['longitude'] = floatval($coordinates[1]);
            $tour_data['package_plan'] = $tour_data['package_plan'] ? unserialize($tour_data['package_plan']) : APET_PACKAGE_PLANS['free_plan'];
            echo json_encode(array('success' => 'true', 'data' => $tour_data));
            wp_die();
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_tour_settings', 'apet_get_tour_settings');

function apet_change_tour_admin_password() {
    if (isset($_POST['tour_id'])) {
        $validate = true;
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $user_id = sanitize_text_field($_POST['user_id']);


        $current_password = isset($_POST['current-password']) ? (strlen($_POST['current-password']) > 6 ? $_POST['current-password'] : $validate = false) : false;
        $new_password = isset($_POST['new-password']) ? (strlen($_POST['new-password']) > 6 ? $_POST['new-password'] : $validate = false) : false;
        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        }
        $userdata = get_user_by('ID', $user_id);
        $password_check = $current_password ? wp_check_password($current_password, $userdata->user_pass, $user_id) : true;


        if ($validate === false) {

            response_error_message('validation_error');
        } elseif ($password_check === false) {
            response_error_message('incorrect_password');
        }
        $userdata = (array)$userdata;
        $userdata['user_pass'] = $new_password;
        wp_update_user($userdata);

        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_change_tour_admin_password', 'apet_change_tour_admin_password');

function apet_get_admin_tour_agencies() {
    if (isset($_POST['tour_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $user_id = sanitize_text_field($_POST['user_id']);
        $limit = (isset($_POST['limit']) && isset($_POST['offset'])) ? "LIMIT " . sanitize_text_field($_POST['offset']) . ", " . sanitize_text_field($_POST['limit']) : "";
        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }

        $filter_options = isset($_POST['filter']) ? json_decode(stripslashes(sanitize_text_field($_POST['filter'])), true) : false;
        $filter = '';
        if($filter_options !== false){

            if(isset($filter_options['status']) && !empty($filter_options['status'])){

                switch ($filter_options['status']) {
                    case 'active':
                        $filter.= " AND agencies.status = 'active'";
                        break;
                    case 'pending':
                        $filter.= " AND agencies.status = 'inactive'";
                        break;
                    case 'deactivated':
                        $filter.= " AND agencies.status = 'deactivated' ";
                        break;
                }
            }
            if(isset($filter_options['search']) && !empty($filter_options['search'])){
                $filter.= " AND (agencies.tour_agent LIKE '%" . $filter_options['search'] . "%' 
                            OR agencies.address LIKE '%" . $filter_options['search'] . "%'
                            OR agencies.phone LIKE '%" . $filter_options['search'] . "%'
                            )";
            }

            if(!empty($filter)){
                $filter = 'WHERE ' . ltrim($filter, ' AND ');
            }
        }
        $tour_agencies = $wpdb->get_results("SELECT tour_agent,DATE(tour_creation_date) as tour_creation_date,id,package_plan,package_plan_name,status FROM `$tour_table` agencies $filter ORDER BY `status` ASC, `tour_creation_date` DESC $limit", ARRAY_A);

        foreach ($tour_agencies as $key => $agency){
            $tour_agencies[$key]['package_plan'] = $agency['package_plan'] ? unserialize($agency['package_plan']) : APET_PACKAGE_PLANS['free_plan'];
        }
        if (empty($tour_agencies)) {
            response_error_message('no_agencies');
        }
        echo json_encode(array('success' => 'true', 'data' => $tour_agencies));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_admin_tour_agencies', 'apet_get_admin_tour_agencies');

function apet_get_admin_tour_agency() {
    if (isset($_POST['tour_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $spec_offers_table = $wpdb->prefix . 'special_offers';
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $user_id = sanitize_text_field($_POST['user_id']);
        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }

        $tour_agency = $wpdb->get_row("SELECT * FROM `$tour_table` WHERE id='$tour_id' ", ARRAY_A);
        if (!$tour_agency) {
            response_error_message('fetching_agency_failed');
        }
        if (isset($tour_agency['prefered_countries'])) {
            $tour_agency['prefered_countries'] = unserialize($tour_agency['prefered_countries']);
        }
        $tour_agency['tour_creation_date'] = apet_datetime_to_date($tour_agency['tour_creation_date']);

        $tour_agency['package_plan'] = $tour_agency['package_plan'] ? unserialize($tour_agency['package_plan']) : APET_PACKAGE_PLANS['free_plan'] ;
        $spec_offers_count = $wpdb->get_results("SELECT COUNT(*) as count FROM $spec_offers_table WHERE MONTH(offer_creation_date) = MONTH(CURDATE()) AND offer_author='$tour_id'", ARRAY_A);
        $spec_offers_count = !empty($spec_offers_count) ? $spec_offers_count[0]['count'] : 0;
        $offers_limit = $tour_agency['package_plan']['special_offers_limit'];
        $offers_left = intval($offers_limit) - intval($spec_offers_count);
        $offers_left = $offers_left<=0 ? 0 : $offers_left;
        $offers_left = $offers_limit === -1 ? -1: $offers_left;
        $tour_agency['offers_left'] = $offers_left;

        echo json_encode(array('success' => 'true', 'data' => $tour_agency));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_admin_tour_agency', 'apet_get_admin_tour_agency');

function apet_create_tour_agency() {
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $user_id = sanitize_text_field($_POST['user_id']);
        $admin_email = sanitize_text_field($_POST['admin-email']);
        $package_plan = sanitize_text_field($_POST['tour-package']);
        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }
        if (username_exists($admin_email) === false) {
            $password = wp_generate_password(12, false);
            $admin_id = wp_create_user($admin_email, $password, $admin_email);

            wp_update_user(
                array(
                    'ID' => $admin_id,
                    'nickname' => $admin_email
                )
            );

            $user = new WP_User($admin_id);
            $user->set_role('tour-agent-admin');
        } else {
            response_error_message('email_exists');
        }
        $package_name = $package_plan;
        $package_plan = serialize(APET_PACKAGE_PLANS[$package_plan]);


        $tourdata = array(
            'tour_admin' => $admin_id,
            'package_plan_name'=> $package_name,
            'package_plan' => $package_plan,
            'status' => 'inactive',
            'tour_creation_date' => date('Y-m-d H:i:s'),
        );

        $agency_result = $wpdb->insert($tour_table, $tourdata);

        if (!$agency_result) {
            response_error_message('creating_agency_failed');
        }

        $tour_id = $wpdb->insert_id;
        $login_url = home_url('login');
        $subject = 'Welcome!';
//        $body = "Hi,<br><br>Please got to dashboard and fill in your agency details. <br>"
//            . "Dashboard: <a href='$login_url'>$login_url</a> <br>"
//            . "Login: $admin_email <br>"
//            . "Password: $password<br><br>Thanks,<br>EasyTraveling team.";

        $style = '<style>.wrapper{background-color:#d6d6d6!important;padding:10px!important}.wrapper>p{color:#515151!important}.table-wrapper{background-color:#f6f6f6!important;border-radius:10px!important;padding:10px!important;width:500px!important;margin:0 auto!important}.login-text{display:block!important;font-family:arial,sans-serif!important;font-size:18px!important;padding:8px!important;text-align:center!important;color:#515151!important}.login-text a{text-decoration:none!important;padding:10px!important;background-color:#7cb342!important;color:#fff!important;border-radius:10px!important;display:inline-block!important}.table-wrapper table{background-color:#f6f6f6!important;font-family:arial,sans-serif!important;border-collapse:collapse!important;width:100%!important;margin:0 auto 10px!important}.table-wrapper thead{font-size:20px!important;border-bottom:1px solid #ddd!important}.table-wrapper thead th{color:#515151!important}.table-wrapper td,p,th{text-align:left!important;padding:0 8px!important;font-size:18px!important}.table-wrapper .first{padding-top:8px!important}.table-wrapper .odd{padding-bottom:4px!important;color:#515151!important;font-size:18px!important}.table-wrapper .even{padding-bottom:8px!important;color:#909090!important;font-size:16px!important}</style>';

        $body = "<div class='wrapper'>"
                    ."<p>Hi</p>"
                    ."<p>Please got to dashboard and fill in your agency details. </p>"
                        ."<div class='table-wrapper'>"
                            ."<table>"
                                ."<tbody>"
                                    ."<tr class=\"first odd\"><td><Strong>Dashboard:  <a href='$login_url'>$login_url</a></Strong></td></tr>"
                                    ."<tr class=\"even\"><td>Login:  " . $admin_email . "</td></tr>"
                                    ."<tr class=\"odd\"><td><Strong>Password:  " . $password . "</Strong></td></tr>"
                                ."</tbody>"
                            ."</table>"
                        ."</div>"
                    ."<div class='login-text'>Thanks,<br>EasyTraveling team.</div>"
                ."</div>";

        $email_success = apet_send_mail($admin_email, $subject, $body, $style);


        echo json_encode(array('success' => 'true', 'data' => array('tour_id' => $tour_id, 'admin_id' => $admin_id,'package_plan_name'=> $package_name, 'package_plan' => unserialize($package_plan))));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_create_tour_agency', 'apet_create_tour_agency');

function apet_deactivate_agency() {
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }

        $deactivate_agency = $wpdb->query("UPDATE `$tour_table` SET status='deactivated' WHERE id='$tour_id' ");

        if ($deactivate_agency === false) {
            response_error_message('deactivating_failed');
        }

        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_deactivate_agency', 'apet_deactivate_agency');

function apet_activate_agency() {
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }

        $activate_agency = $wpdb->query("UPDATE `$tour_table` SET status='active' WHERE id='$tour_id' ");

        if ($activate_agency === false) {
            response_error_message('activating_failed');
        }

        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_activate_agency', 'apet_activate_agency');

function apet_edit_agency_package() {
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $agency_package = sanitize_text_field($_POST['agency_package']);


        $package_plans=APET_PACKAGE_PLANS;

        $selected_plan=serialize($package_plans[$agency_package]);

        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }

        $edit_package = $wpdb->query("UPDATE `$tour_table` SET package_plan='$selected_plan',package_plan_name='$agency_package' WHERE id='$tour_id' ");

        if ($edit_package === false) {
            response_error_message('editing_failed');
        }

        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_edit_agency_package', 'apet_edit_agency_package');

function apet_get_agency_badges(){

    $badges = array();
    global $wpdb;
    $tours_table = $wpdb->prefix . 'tour_agencies';
    $agency_offers_table = $wpdb->prefix . 'agency_offers';

    $result = $wpdb->get_results("
            SELECT COUNT(DISTINCT agency_offers.offer_id) AS offers_count, tours.id
            FROM `" . $tours_table . "` AS tours 
            LEFT JOIN `" . $agency_offers_table . "` AS agency_offers ON tours.id=agency_offers.offer_author 
            WHERE tours.status='active' AND  CONVERT_TZ(agency_offers.offer_creation_date,'+00:00','".APET_WP_TIMEZONE."')>=DATE_SUB(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','".APET_WP_TIMEZONE."'), INTERVAL 1 MONTH)
            GROUP BY tours.id ORDER BY offers_count DESC, tours.id DESC LIMIT 5 OFFSET 0"
        , ARRAY_A);

    foreach($result as $key => $agency){
        $badges[$agency['id']] = array('offers_response_'. ($key+1) .'_place');
    }
    return $badges;
}
