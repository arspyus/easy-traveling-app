<?php

defined('ABSPATH') or die('No script kiddies please!');

function apet_booking() {

    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {

        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $request_id = sanitize_text_field($_POST['request_id']);
        $offer_id = sanitize_text_field($_POST['offer_id']);
        $offer_author = sanitize_text_field($_POST['offer_author']);
        $travelers = sanitize_text_field($_POST['travelers']);
        $travelers = json_decode(stripslashes($travelers), true);
        $img_book = sanitize_text_field($_POST['img_book']);
        $platform = sanitize_text_field($_POST['platform']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            }else{
                $user_id = $user[0]->ID;
            }
        }else{
            response_error_message('invalid_token');
        }

        global $wpdb;
        global $apet_file_path;
        $requests_table = $wpdb->prefix . 'requests';
        $requests_status_table = $wpdb->prefix . 'requests_statuses';
        $offers_table = $wpdb->prefix . 'agency_offers';
        $book_table = $wpdb->prefix . 'books';
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $data = array(
            'user_id' => $user_id,
            'request_id' => $request_id,
            'offer_id' => $offer_id,
            'offer_author' => $offer_author,
            'travelers_data' => serialize($travelers),
            'book_status' => 'active',
            'book_date' => date('Y-m-d H:i:s')
        );
        $req_status = $wpdb->get_row("SELECT status FROM `$requests_table` WHERE request_id='$request_id'", ARRAY_A);

        if(empty($req_status) || $req_status['status'] === 'canceled'){
            response_error_message('booking_failed');
        }
        $result = $wpdb->insert($book_table, $data);
        $book_id = $wpdb->insert_id;
        $img_moved = true;
        $update_status = true;
        $img_path_change = true;
        if ($result) {

            $res = $wpdb->update($requests_table,
                array('status' => 'deprecated'),
                array('request_id' => $request_id)
            );
            $check = $wpdb->insert(
                $requests_status_table, array(
                    'request_id' => $request_id,
                    'status' => 'pre-booked',
                    'set_date' => date('Y-m-d H:i:s'),
                )
            );
            if ($res === false) {
                response_error_message('booking_failed');
            }

            if ($img_book === 'true') {
                foreach ($travelers as $traveler => $traveler_data) {
                    if ($traveler_data['photo'] !== "") {
                        $travelers[$traveler]['photo'] = preg_replace('/temp\/[0-9]+_/', 'books/' . $book_id . '_', $traveler_data['photo']);
                        if (rename($traveler_data['photo'], $travelers[$traveler]['photo']) === false) {
                            $img_moved = false;
                        } else {
                            $file_name = substr($travelers[$traveler]['photo'], strlen(plugin_dir_path($apet_file_path) . 'uploads/books/'));
                            $plugin_url = plugin_dir_url($travelers[$traveler]['photo']);
                            $travelers[$traveler]['photo'] = $plugin_url . "" . $file_name;
                        }
                    }
                }
                $img_path_change = false;
                if ($img_moved) {
                    $travelers = serialize($travelers);
                    $img_path_change = $wpdb->query("UPDATE $book_table SET travelers_data = '$travelers'  WHERE  book_id ='$book_id'");
                }
            }

            if ($img_path_change) {
                $update_status = $wpdb->query("UPDATE `$requests_table`, `$offers_table` SET "
                        . " $offers_table.offer_status = CASE WHEN $offers_table.offer_id<>$offer_id THEN 'rejected' ELSE 'booked' END WHERE $offers_table.request_id='$request_id' and $requests_table.request_id='$request_id' ", ARRAY_A);
            }
        }

        if (!$result || !$update_status || !$img_path_change) {
            response_error_message('booking_failed');
        }

        /* ===Add Bonuse==== */
        $user_meta = get_user_meta($user_id);
        $bonus = $user_meta['bonus'][0];
        $updated_bonus = intval($bonus) + 1;
        update_user_meta($user_id, 'bonus', $updated_bonus, $bonus);
        update_user_meta($user_id, 'bonus_data', NULL);

        $agency_email = $wpdb->get_row("SELECT email FROM `$tour_table` WHERE id='$offer_author'", ARRAY_A);
        $agency_email = $agency_email ? $agency_email['email'] : NULL;

        $request_details = $wpdb->get_row("SELECT * FROM `$requests_table` WHERE request_id='$request_id'", ARRAY_A);
        $req_country = $request_details['country'];
        $req_city = $request_details['city'];
        $req_nights = $request_details['nights'] ? $request_details['nights'] : $request_details['nights_from']. ' - '.$request_details['nights_to'];
        $req_period_from = apet_datetime_to_date($request_details['period_from']);
        $req_period_to = apet_datetime_to_date($request_details['period_to']);
        $req_adults = $request_details['adults'];
        $req_children = (int) $request_details['children'] + (int) $request_details['infants'];
        $req_travelers = (int) $req_adults + (int) $req_children;

        $style = '<style>.wrapper{background-color:#d6d6d6!important;padding:10px!important}.wrapper>p{color:#515151!important}.table-wrapper{background-color:#f6f6f6!important;border-radius:10px!important;padding:10px!important;width:500px!important;margin:0 auto!important}.login-text{display:block!important;font-family:arial,sans-serif!important;font-size:18px!important;padding:8px!important;text-align:center!important;color:#515151!important}.login-text a{text-decoration:none!important;padding:10px!important;background-color:#7cb342!important;color:#fff!important;border-radius:10px!important;display:inline-block!important}.table-wrapper table{background-color:#f6f6f6!important;font-family:arial,sans-serif!important;border-collapse:collapse!important;width:100%!important;margin:0 auto 10px!important}.table-wrapper thead{font-size:20px!important;border-bottom:1px solid #ddd!important}.table-wrapper thead th{color:#515151!important}.table-wrapper td,p,th{text-align:left!important;padding:0 8px!important;font-size:18px!important}.table-wrapper .first{padding-top:8px!important}.table-wrapper .odd{padding-bottom:4px!important;color:#515151!important;font-size:18px!important}.table-wrapper .even{padding-bottom:8px!important;color:#909090!important;font-size:16px!important}</style>';
        $subject = 'New pre-book: [' . ucfirst($req_country) . ' ' . ucfirst($req_city) . ' , From ' . $req_period_from . ' to ' . $req_period_to . ']';

        if ($agency_email) {
            $body = "<div class='wrapper'><p>Hi, You have new pre-book.</p><div class='table-wrapper'><table><thead><tr><th><Strong>Request #".$request_id."</Strong></th></tr></thead><tbody><tr class=\"first odd\"><td ><Strong>" . $req_city . "</Strong></td></tr><tr class=\"even\"><td >" . $req_country . "</td></tr><tr class=\"odd\"><td ><Strong>" . $req_nights . " Nights</Strong></td></tr><tr class=\"even\"><td >" . $req_period_from . " - " . $req_period_to . "</td></tr><tr class=\"odd\"><td ><Strong>" . $req_travelers . " Travelers . </Strong></td></tr><tr class=\"even\"><td >" . $req_adults . " Adults, " . $req_children . " Children</td></tr></tbody></table></div><div class='login-text'> Login to dashboard <a href='" . home_url() . "/dashboard?request_id=".$request_id."#requests-page'>".ucfirst(str_replace("http://","",home_url('','http'))) ."/dashboard</a></div></div>";
            $email_success = apet_send_mail($agency_email, $subject, $body,$style);
        }
        /* ======== Sending mail to super admins =========== */
        if($request_details){
            $args = array(
                'role'    => 'tour-super-admin',
            );
            $super_admins = get_users($args);
            $body = "<div class='wrapper'><p>Hi, We have new pre-book.</p><div class='table-wrapper'><table><thead><tr><th><Strong>Request #".$request_id."</Strong></th></tr></thead><tbody><tr class=\"first odd\"><td ><Strong>" . $req_city . "</Strong></td></tr><tr class=\"even\"><td >" . $req_country . "</td></tr><tr class=\"odd\"><td ><Strong>" . $req_nights . " Nights</Strong></td></tr><tr class=\"even\"><td >" . $req_period_from . " - " . $req_period_to . "</td></tr><tr class=\"odd\"><td ><Strong>" . $req_travelers . " Travelers . </Strong></td></tr><tr class=\"even\"><td >" . $req_adults . " Adults, " . $req_children . " Children</td></tr></tbody></table></div><div class='login-text'> Login to dashboard <a href='" . home_url() . "/dashboard?request_id=".$request_id."#requests-page'>".ucfirst(str_replace("http://","",home_url('','http'))) ."/dashboard</a></div></div>";

            foreach($super_admins as $admin){

                $admin_email = $admin->data->user_email;
                $email_success = apet_send_mail($admin_email, $subject, $body, $style);
            }
        }else{
            response_error_message('booking_failed');
        }

        echo json_encode(array('data' => array('bonus' => $updated_bonus, 'bonus_id' => NULL, 'withdrawn_bonus' => NULL, 'bonus_unit_price' => APET_BONUS_UNIT_PRICE), 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_booking', 'apet_booking');
add_action('wp_ajax_apet_booking', 'apet_booking');



function apet_upload_book_imgs() {
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {

        global $apet_file_path;
        $offer_id = sanitize_text_field($_POST['offer-id']);
        $photo_index = sanitize_text_field($_POST['photo_index']);
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $platform = sanitize_text_field($_POST['platform']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            }
        }else{
            response_error_message('invalid_token');
        }

        $ext = 'jpg';//pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $img = plugin_dir_path($apet_file_path) . 'uploads/temp/' . $offer_id . '_' . $photo_index . '.' . $ext;
        if (apet_image_upload($_FILES['file']['tmp_name'], $img,  array(2000,2000)) !== true) {

            response_error_message('img_booking_failed');
        } else {
            echo json_encode(array('data' => array('photo' => $img), 'success' => 'true'));
            wp_die();
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_upload_book_imgs', 'apet_upload_book_imgs');
add_action('wp_ajax_apet_upload_book_imgs', 'apet_upload_book_imgs');
