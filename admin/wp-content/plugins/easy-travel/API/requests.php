<?php

defined('ABSPATH') or die('No script kiddies please!');

function apet_make_request() {

    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id, $device_id)) {

        global $wpdb;
        $validate = true;
        $country = sanitize_text_field($_POST['country']);
        $country_code = sanitize_text_field($_POST['country_code']);
        $city = sanitize_text_field($_POST['city']);
        $period_from = sanitize_text_field($_POST['period-from']);
        $period_to = sanitize_text_field($_POST['period-to']);
//        $airticket = sanitize_text_field($_POST['airticket']);
//        $nationality = sanitize_text_field($_POST['nationality']);
        $adults = sanitize_text_field($_POST['adults']);
        $children = sanitize_text_field($_POST['children']);
        $infant = sanitize_text_field($_POST['infant']);

        $nights = sanitize_text_field($_POST['nights']);

        $explode = explode(',',$nights);

        if(isset($explode[1])){
            $nights_from = $explode[0] ;
            $nights_to = $explode[1];
            $nights = (int) $nights_to - 3;
        }

        $rooms = sanitize_text_field($_POST['rooms']);
        $hotel_category = sanitize_text_field($_POST['hotel-category']);
//        $visa_support = sanitize_text_field($_POST['visa-support']);
        $notes = sanitize_text_field($_POST['notes']);

        $extra = isset($_POST['extra']) ? sanitize_text_field($_POST['extra']) : null;
        $extra = json_decode(stripslashes($extra));
        $extra = (!is_array($extra) && !empty($extra)) ? array($extra) : $extra;
        $extra = empty($extra) ? NULL : serialize($extra);

        $children_ages = isset($_POST['children_ages']) ? sanitize_text_field($_POST['children_ages']) : null;
        $children_ages = json_decode(stripslashes($children_ages));
        $children_ages = (!is_array($children_ages) && !empty($children_ages)) ? array($children_ages) : $children_ages;
        $children_ages = empty($children_ages) ? NULL : serialize($children_ages);

//        $payment = sanitize_text_field($_POST['payment']);

        $board_type = isset($_POST['board-type']) ? sanitize_text_field($_POST['board-type']) : null;
        $board_type = json_decode(stripslashes($board_type));
        $board_type = (!is_array($board_type) && !empty($board_type)) ? array($board_type) : $board_type;
        $board_type = empty($board_type) ? NULL : serialize($board_type);

//        $chosen_tour = isset($_POST['chosen-tour']) ? sanitize_text_field($_POST['chosen-tour']) : '';

        $desired_hotels = isset($_POST['desired_hotels']) ? sanitize_text_field($_POST['desired_hotels']) : null;
        $desired_hotels = json_decode(stripslashes($desired_hotels));
        $desired_hotels = (!is_array($desired_hotels) && !empty($desired_hotels)) ? array($desired_hotels) : $desired_hotels;
        $desired_hotels = empty($desired_hotels) ? NULL : serialize($desired_hotels);

        $price_range =  sanitize_text_field($_POST['price']);

        $explode  = explode(',',$price_range);
        $price_from = $explode[0];
        $price_to = $explode[1];

        $social_package = sanitize_text_field($_POST['social_package']);


        $status = 'pending';
        $platform = sanitize_text_field($_POST['platform']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $token = sanitize_text_field($_POST['token']);
        if ($validate === false) {
            response_error_message('validation_error');
        }

        $token_info = apet_verify_user($token);

        if ($token_info['success'] === true) {
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            } else {
                $user_id = $user[0]->ID;
            }
        } else {
            response_error_message('invalid_token');
        }

        $request_author = $user_id;


        $request_table = $wpdb->prefix . "requests";
        $request_status_table = $wpdb->prefix . "requests_statuses";


        $check = $wpdb->insert(
            $request_table, array(
                'country' => $country,
                'country_code' => $country_code,
                'city' => $city,
                'period_from' => $period_from,
                'period_to' => $period_to,
                'price_from' => $price_from,
                'price_to' => $price_to,
//                'airticket' => $airticket,
//                'nationality' => $nationality,
                'adults' => $adults,
                'children' => $children,
                'infant' => $infant,
                'nights' => $nights,
//                'nights_from' => $nights_from,
//                'nights_to' => $nights_to,
                'desired_hotels'  => $desired_hotels,
                'rooms' => $rooms,
                'hotel_category' => $hotel_category,
//                'visa_support' => $visa_support,
                'notes' => $notes,
//                'payment_method' => $payment,
                'board_type' => $board_type,
                'social_package' => $social_package,
                'extra' => $extra,
                'children_ages' => $children_ages,
                'status' => 'deprecated', // For New Set Status to Deprecated
//                'chosen_tour' => $chosen_tour,
                'request_author' => $request_author,
                'request_date' => date('Y-m-d H:i:s'),
            )
        );

        if ($check === false) {
            response_error_message('request_faild');
        }

        /* Add Requests status to Status table */
        $request_id = $wpdb->insert_id;

        $check = $wpdb->insert(
            $request_status_table, array(
                'request_id' => $request_id,
                'status' => $status,
                'set_date' => date('Y-m-d H:i:s'),
            )
        );

        if ($check === false) {
            response_error_message('request_faild');
        }
        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_make_request', 'apet_make_request');
add_action('wp_ajax_apet_make_request', 'apet_make_request');

function apet_get_app_user_requests()
{
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id, $device_id)) {

        global $wpdb;
        $requests_table = $wpdb->prefix . "requests";
        $requests_statuses_table = $wpdb->prefix . "requests_statuses";
        $agency_offers_table = $wpdb->prefix . "agency_offers";
        $tour_agencies_table = $wpdb->prefix . "tour_agencies";
        $statuses_sub_query = "(SELECT * FROM `$requests_statuses_table` AS rs WHERE status_id = ( SELECT MAX( status_id ) FROM `$requests_statuses_table` WHERE request_id = rs.request_id ) GROUP BY request_id )";

        $response = array();
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $status = isset($_POST['status']) ? sanitize_text_field($_POST['status']) : "all";
        $from = isset($_POST['from']) ? sanitize_text_field($_POST['from']) : "";
        $to = isset($_POST['to']) ? sanitize_text_field($_POST['to']) : "";
        $platform = sanitize_text_field($_POST['platform']);

        $token_info = apet_verify_user($token);

        if ($token_info['success'] === true) {
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            } else {
                $user_id = $user[0]->ID;
            }
        } else {
            response_error_message('invalid_token');
        }

        $date_range = '';
        if ($from !== '' && $to !== '') {
            $date_range = "AND (CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') >=CONVERT_TZ('$from','+00:00','" . APET_WP_TIMEZONE . "') AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "')<=DATE_ADD(CONVERT_TZ('$to','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY))";
        } elseif ($to !== '') {
            $date_range = "AND (CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "')<=DATE_ADD(CONVERT_TZ('$to','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY))";
        } elseif ($from !== '') {
            $date_range = "AND (CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "')>=CONVERT_TZ('$from','+00:00','" . APET_WP_TIMEZONE . "'))";
        }
        $status_query='';
        if ($status === 'all') {
            $status_query = "
             AND (
                (DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')
				    AND (requests.status = 'active' OR (requests.status = 'deprecated' AND statuses.status != 'canceled'))
            ) OR (requests.status = 'deprecated' AND statuses.status = 'booked') AND CONVERT_TZ(agency_offers.arrival_destination_arrival_date_time,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')  )";
        } elseif ($status === 'archive') {
            $status_query = " AND ((DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) < CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "') AND requests.status='active') OR requests.status='canceled' OR statuses.status='canceled') ";
        } elseif ($status === 'booked'){
            $status_query = "AND( requests.status='booked' OR statuses.status = 'booked')";
        }

        $sql ="
            SELECT 
                requests.request_id,
                requests.adults,
                requests.children,
                requests.infant,
                requests.country,
                requests.city,
                requests.social_package,
                requests.chosen_tour,
                requests.period_from,
                requests.period_to,
                requests.price_from,
                requests.price_to,
                requests.nights,
                requests.nights_from,
                requests.nights_to,
                IF(requests.status != 'deprecated', 'true', 'false') as deprecated,
                IF(requests.status = 'deprecated', statuses.status,requests.status) as status,
                agencies.id as agency_id,
                agencies.country as agency_country,
                agencies.city as agency_city,
                agencies.address as agency_address,
                agencies.tour_agent as agency_tour_agent,
                agencies.phone as agency_phone,
                agencies.email as agency_email,
                agencies.coordinates as agency_coordinates,
                agencies.average_rate as agency_average_rate,
                agencies.photo as agency_photo,
                agencies.website as agency_website,
                agency_offers.offer_id,
                agency_offers.offer_deadline,
                agency_offers.offer_author,
                agency_offers.price,
                agency_offers.hotel,
                agency_offers.place_id,
                agency_offers.offer_status,
                agency_offers.seen_status,
                agency_offers.departure,
                agency_offers.arrival,
                agency_offers.hotel_checkin,
                agency_offers.hotel_checkout,
                agency_offers.flight_type,
                agency_offers.board_type,
                agency_offers.notes,
                agency_offers.trans_type,
                agency_offers.departure_airlines_iata,
                agency_offers.arrival_airlines_iata,
                agency_offers.departure_flight_type,
                agency_offers.arrival_flight_type,
                agency_offers.departure_departure_city,
                agency_offers.departure_departure_airport,
                agency_offers.departure_departure_air_company,
                agency_offers.departure_departure_date_time,
                agency_offers.departure_destination_arrival_city,
                agency_offers.departure_destination_arrival_airport,
                agency_offers.departure_destination_arrival_date_time,
                agency_offers.arrival_departure_airport,
                agency_offers.arrival_departure_air_company,
                agency_offers.arrival_departure_date_time,
                agency_offers.arrival_destination_arrival_date_time,
                agency_offers.arrival_destination_arrival_airport,
                agency_offers.arrival_departure_city,
                agency_offers.arrival_destination_arrival_city,
                agency_offers.departure_departure_air_code,
                agency_offers.departure_destination_arrival_air_code,
                agency_offers.arrival_departure_air_code,
                agency_offers.arrival_destination_arrival_air_code,
                agency_offers.hotel_rating,
                agency_offers.number_of_rooms
            FROM `$requests_table` requests
            LEFT JOIN `$agency_offers_table` agency_offers ON requests.request_id = agency_offers.request_id 
            LEFT JOIN `$tour_agencies_table` agencies ON agencies.id = agency_offers.offer_author
            LEFT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id 
            WHERE 
              requests.request_author = '$user_id'
               AND (agencies.status='active' OR agencies.status IS NULL)
                $status_query $date_range 
            ORDER BY requests.request_id DESC, agency_offers.price ASC";

        $results = $wpdb->get_results($sql, ARRAY_A);

        if (empty($results)) {
            response_error_message('no_requests_found');
        }

        $badges = apet_get_agency_badges();
        foreach ($results as $i => $result) {
            $result = apet_stripslashes_deep($result);
            $result_copy = $result;
            $offers = array();
            $multi_offers = false;

            if ($result["offer_id"]) {

                $multi_offers = array_search($result["request_id"], array_column($response, 'request_id'));

                    $result_copy['agency'] = array(
                        'id'            =>  $result['agency_id'],
                        'country'       =>  $result['agency_country'],
                        'city'          =>  $result['agency_city'],
                        'address'       =>  $result['agency_address'],
                        'tour_agent'    =>  $result['agency_tour_agent'],
                        'phone'         =>  $result['agency_phone'],
                        'email'         =>  $result['agency_email'],
                        'coordinates'   =>  $result['agency_coordinates'],
                        'average_rate'  =>  $result['agency_average_rate'],
                        'photo'         =>  $result['agency_photo'],
                        'website'       =>  $result['agency_website'],
                        'badges'        =>  isset($badges[$result['offer_author']]) ? $badges[$result['offer_author']] : null
                    );
                    $result_copy['hotel_checkin'] = apet_datetime_to_date($result['hotel_checkin']);
                    $result_copy['hotel_checkout'] = apet_datetime_to_date($result['hotel_checkout']);
                    $result_copy['offer_deadline'] = apet_datetime_to_date($result['offer_deadline']);
                    $result_copy['arrival'] = $result['arrival'] ? apet_datetime_to_date($result['arrival']) : NULL;
                    $result_copy['departure'] = $result['departure'] ? apet_datetime_to_date($result['departure']) : NULL;
                    $result_copy['days_left'] = apet_days_left($result['offer_deadline']);

                    if(($result['departure_departure_city'])){
                        $result_copy['transportation'] = array(
                            'type' => $result['trans_type'],
                            'departure' => array(
                                'type' =>$result['departure_flight_type'],
                                'departure_date'=>date('Y-m-d' ,strtotime($result['departure_departure_date_time'])),
                                'departure_time'=>date('H:i' ,strtotime($result['departure_departure_date_time'])),
                                'arrival_date'=>date('Y-m-d' ,strtotime($result['departure_destination_arrival_date_time'])),
                                'arrival_time'=>date('H:i' ,strtotime($result['departure_destination_arrival_date_time'])),
                                'departure_airport'=>$result['departure_departure_air_code'],
                                'arrival_airport'=>$result['departure_destination_arrival_air_code'],
                                'departure_city'=>$result['departure_departure_city'],
                                'arrival_city'=>$result['departure_destination_arrival_city'],
                                'air_company_logo'=> 'https://pics.avs.io/200/200/' . $result['departure_airlines_iata'] . '.png',
                            ),
                            'arrival' => array(
                                'type' =>$result['arrival_flight_type'],
                                'departure_date'=> date('Y-m-d' ,strtotime($result['arrival_departure_date_time'])),
                                'departure_time'=> date('H:i' ,strtotime($result['arrival_departure_date_time'])),
                                'arrival_date'=> date('Y-m-d' ,strtotime($result['arrival_destination_arrival_date_time'])),
                                'arrival_time'=> date('H:i' ,strtotime($result['arrival_destination_arrival_date_time'])),
                                'departure_airport'=>$result['arrival_departure_air_code'],
                                'arrival_airport'=>$result['arrival_destination_arrival_air_code'],
                                'departure_city'=>$result['arrival_departure_city'],
                                'arrival_city'=>$result['arrival_destination_arrival_city'],
                                'air_company_logo'=> 'https://pics.avs.io/200/200/' . $result['arrival_airlines_iata'] . '.png',
                            ),
                        );
                    }


                    unset(
                        $result_copy["departure_departure_air_code"],
                        $result_copy["departure_destination_arrival_air_code"],
                        $result_copy["arrival_departure_air_code"],
                        $result_copy["arrival_destination_arrival_air_code"],
                        $result_copy["departure_departure_city"],
                        $result_copy["departure_departure_airport"],
                        $result_copy["departure_departure_air_company"],
                        $result_copy["departure_departure_date_time"],
                        $result_copy["departure_destination_arrival_city"],
                        $result_copy["departure_destination_arrival_airport"],
                        $result_copy["departure_destination_arrival_date_time"],
                        $result_copy["arrival_departure_airport"],
                        $result_copy["arrival_departure_air_company"],
                        $result_copy["arrival_departure_date_time"],
                        $result_copy["arrival_destination_arrival_date_time"],
                        $result_copy["arrival_destination_arrival_airport"],
                        $result_copy["arrival_departure_city"],
                        $result_copy["arrival_destination_arrival_city"],
                        $result_copy["adults"],
                        $result_copy["children"],
                        $result_copy["infant"],
                        $result_copy["nights"],
                        $result_copy["nights_to"],
                        $result_copy["nights_from"],
                        $result_copy["price_from"],
                        $result_copy["price_to"],
                        $result_copy["period_from"],
                        $result_copy["period_to"],
                        $result_copy["deprecated"],
                        $result_copy["country"],
                        $result_copy["chosen_tour"],
                        $result_copy["city"],
                        $result_copy["status"],
                        $result_copy["agency_id"],
                        $result_copy["agency_country"],
                        $result_copy["agency_city"],
                        $result_copy["agency_address"],
                        $result_copy["agency_tour_agent"],
                        $result_copy["agency_phone"],
                        $result_copy["agency_email"],
                        $result_copy["agency_coordinates"],
                        $result_copy["agency_average_rate"],
                        $result_copy["agency_photo"],
                        $result_copy["agency_website"],
                        $result_copy['social_package']

                    );
                    //old  && ($response[$multi_offers]['status'] === 'active' || $response[$multi_offers]['status'] === 'pending' || $response[$multi_offers]['status'] === 'offered' )
                    if ($multi_offers !== false ) {
                        array_push($response[$multi_offers]["offers"], $result_copy);
                        continue;
                    }
                    array_push($offers, $result_copy);
            }

            if($result['deprecated'] === 'true'){
                if($result['status'] === 'active'){
                    $status ='pending';
                }elseif($result['status'] === 'booked'){
                    $status ='pre-booked';
                }
            }else{
                $status = $result['status'];
            }

            $response[] = array(
                "request_id" => $result["request_id"],
                "country" => stripslashes($result['country']),
                "city" => stripslashes($result['city']),
                "price" => $result['price_from'].",".$result['price_to'],
                "nights" => $result['nights'] ?  $result['nights'] : $result['nights_from'] . " - " .$result['nights_to'],
                "period_from" => apet_datetime_to_date($result["period_from"]),
                "period_to" => apet_datetime_to_date($result["period_to"]),
                "chosen_tour" => $result["chosen_tour"],
                "number_of_people" => (int) $result["adults"] + (int) $result["children"] +  (int) $result["infant"],
                "adults" => $result["adults"],
                "children" => $result["children"] ,
                "infant" => $result["infant"],
                "social_package" => $result['social_package'],
                "status" => $status,
                "offers" => $offers
            );
        }

        echo json_encode(array('data' => $response, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_app_user_requests', 'apet_get_app_user_requests');
add_action('wp_ajax_apet_get_app_user_requests', 'apet_get_app_user_requests');

function apet_update_offers_seen_status()
{
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id, $device_id)) {

        global $wpdb;
        $offer_id = sanitize_text_field($_POST['offer_id']);
        $seen_status = sanitize_text_field($_POST['seen_status']);
        $agency_offers_table = $wpdb->prefix . "agency_offers";

        $res = $wpdb->update($agency_offers_table, array('seen_status' => $seen_status), array('offer_id' => $offer_id));

        if ($res === false) {
            response_error_message('failed');
        }
        echo json_encode(array('success' => 'true'));
        die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_update_offers_seen_status', 'apet_update_offers_seen_status');
add_action('wp_ajax_apet_update_offers_seen_status', 'apet_update_offers_seen_status');

function apet_cancel_app_user_request()
{
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id, $device_id)) {
        global $wpdb;
        $requests_table = $wpdb->prefix . "requests";
        $requests_status_table = $wpdb->prefix . "requests_statuses";
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $request_id = sanitize_text_field($_POST['request-id']);
        $from = isset($_POST['from']) ? sanitize_text_field($_POST['from']) : "";
        $to = isset($_POST['to']) ? sanitize_text_field($_POST['to']) : "";
        $platform = sanitize_text_field($_POST['platform']);
        $token_info = apet_verify_user($token);

        if ($token_info['success'] === true) {
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            } else {
                $user_id = $user[0]->ID;
            }
        } else {
            response_error_message('invalid_token');
        }

        $date_range = '';
        if ($from !== '' && $to !== '') {
            $date_range = "AND (CONVERT_TZ($requests_table.request_date,'+00:00','" . APET_WP_TIMEZONE . "') >=CONVERT_TZ('$from','+00:00','" . APET_WP_TIMEZONE . "') AND CONVERT_TZ($requests_table.request_date,'+00:00','" . APET_WP_TIMEZONE . "')<=DATE_ADD(CONVERT_TZ('$to','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY))";
        } elseif ($to !== '') {
            $date_range = "AND (CONVERT_TZ($requests_table.request_date,'+00:00','" . APET_WP_TIMEZONE . "')<=DATE_ADD(CONVERT_TZ('$to','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY))";
        } elseif ($from !== '') {
            $date_range = "AND (CONVERT_TZ($requests_table.request_date,'+00:00','" . APET_WP_TIMEZONE . "')>=CONVERT_TZ('$from','+00:00','" . APET_WP_TIMEZONE . "'))";
        }
        $cancel_date = date('Y-m-d H:i:s');
        if ($request_id === "all") {
            $result = $wpdb->query("UPDATE $requests_table SET status='deprecated' WHERE request_author='$user_id'  $date_range");
            $check = $wpdb->query("INSERT INTO $requests_status_table (request_id, status,set_date) SELECT request_id,'canceled' as status, UTC_TIMESTAMP ( ) as set_date FROM $requests_table WHERE request_author='$user_id'  $date_range");
        } else {
            $result = $wpdb->update(
                $requests_table,
                array(
                    'status' => 'deprecated',
                ),
                array(
                    'request_author' => $user_id,
                    'request_id' => $request_id,
                )
            );

            $check = $wpdb->insert(
                $requests_status_table,
                array(
                    'request_id' => $request_id,
                    'status' => 'canceled',
                    'set_date' => $cancel_date
                )
            );
        }

        if (!$check) {
            response_error_message('cancellation_failed');
        }
        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_cancel_app_user_request', 'apet_cancel_app_user_request');
add_action('wp_ajax_apet_cancel_app_user_request', 'apet_cancel_app_user_request');

function apet_get_single_request()
{
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id, $device_id)) {
        global $wpdb;
        $agency_offers_table = $wpdb->prefix . "agency_offers";
        $requests_table = $wpdb->prefix . 'requests';
        $agencies_table = $wpdb->prefix . 'tour_agencies';
        $requests_statuses_table = $wpdb->prefix . 'requests_statuses';
        $statuses_sub_query = "(SELECT * FROM `$requests_statuses_table` AS rs WHERE status_id = ( SELECT MAX( status_id ) FROM `$requests_statuses_table` WHERE request_id = rs.request_id ) GROUP BY request_id )";
        $request_id = sanitize_text_field($_POST['request-id']);
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $platform = sanitize_text_field($_POST['platform']);

        $token_info = apet_verify_user($token);
        $user_type = sanitize_text_field($_POST['user-type']);
        if ($token_info['success'] === true) {
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' => $user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' => $token_info['uid']
                    )
                )
            );
            $user = get_users($args);
            if (empty($user)) {
                response_error_message('invalid_token');
            } else {
                $user_id = $user[0]->ID;
            }
        } else {
            response_error_message('invalid_token');
        }


        $result = $wpdb->get_row("
            SELECT
                r.adults,
                r.children,
                r.infant,
                r.airticket,
                r.board_type,
                r.chosen_tour,
                r.city,
                r.country,
                r.period_from,
                r.period_to,
                r.request_author,
                r.request_id,
                r.rooms,
                r.extra,
                r.children_ages,
                r.visa_support,
                r.hotel_category,
                r.nationality,
                r.desired_hotels,
                r.social_package,
                r.price_from,
                r.price_to,
                r.nights,
                r.nights_from,
                r.nights_to,
                r.notes,
                r.payment_method,
                r.request_date as request_date,
                IF(r.status != 'deprecated', 'true', 'false') as deprecated,
                IF(r.status = 'deprecated', statuses.status,r.status) as status
            FROM $requests_table r
            LEFT JOIN $statuses_sub_query AS statuses ON r.request_id = statuses.request_id 
            WHERE r.request_id='$request_id'
            GROUP BY r.request_id ", ARRAY_A);

        if (empty($result)) {
            response_error_message('getting_request_faild');
        }
        $offers_query="
            SELECT 
                agencies.id as agency_id,
                agencies.country as agency_country,
                agencies.city as agency_city,
                agencies.address as agency_address,
                agencies.tour_agent as agency_tour_agent,
                agencies.phone as agency_phone,
                agencies.email as agency_email,
                agencies.coordinates as agency_coordinates,
                agencies.average_rate as agency_average_rate,
                agencies.photo as agency_photo,
                agencies.website as agency_website,
                agency_offers.*
            FROM `$agency_offers_table` agency_offers
            LEFT JOIN `$agencies_table` agencies ON agency_offers.offer_author = agencies.id 
            WHERE
                agency_offers.request_id = '$request_id' 
            ORDER BY  agency_offers.price ASC";

        $offers_result = $wpdb->get_results($offers_query, ARRAY_A);

        if (!empty($offers_result)) {

            $badges = apet_get_agency_badges();

            foreach ($offers_result as $key => $offer){
                $offers_result[$key]['days_left'] = apet_days_left($offer['offer_deadline']);
                $offers_result[$key]['doc_list'] = $offer['doc_list'] ? unserialize($offer['doc_list']) : NULL;
                $offers_result[$key]['days_left'] = apet_days_left($offer['offer_deadline']);
                $offers_result[$key]['doc_deadline'] = apet_datetime_to_date($offer['doc_deadline']);
                $offers_result[$key]['hotel_checkin'] = apet_datetime_to_date($offer['hotel_checkin']);
                $offers_result[$key]['hotel_checkout'] = apet_datetime_to_date($offer['hotel_checkout']);
                $offers_result[$key]['offer_deadline'] = apet_datetime_to_date($offer['offer_deadline']);
                $offers_result[$key]['arrival'] = $offer['arrival'] ? apet_datetime_to_date($offer['arrival']): NULL;
                $offers_result[$key]['departure'] = $offer['departure'] ?  apet_datetime_to_date($offer['departure']): NULL;
                $offers_result[$key]['offer_creation_date'] = apet_datetime_to_date(apet_time_zone_convert($offer['offer_creation_date'], 'GMT', APET_WP_TIMEZONE));

                $offers_result[$key]['agency'] = array(
                    'id'            =>$offer['agency_id'],
                    'country'       =>$offer['agency_country'],
                    'city'          =>$offer['agency_city'],
                    'address'       =>$offer['agency_address'],
                    'tour_agent'    =>$offer['agency_tour_agent'],
                    'phone'         =>$offer['agency_phone'],
                    'email'         =>$offer['agency_email'],
                    'coordinates'   =>$offer['agency_coordinates'],
                    'average_rate'  =>$offer['agency_average_rate'],
                    'photo'         =>$offer['agency_photo'],
                    'website'       =>$offer['agency_website'],
                    'badges'        => isset($badges[$offer['offer_author']]) ? $badges[$offer['offer_author']] : null
                );

                if($offer['departure_departure_city']){
                    $offers_result[$key]['transportation'] = array(
                        'type' => $offer['trans_type'],
                        'departure' => array(
                            'type' =>$offer['departure_flight_type'],
                            'departure_date'=>date('Y-m-d' ,strtotime($offer['departure_departure_date_time'])),
                            'departure_time'=>date('H:i' ,strtotime($offer['departure_departure_date_time'])),
                            'arrival_date'=>date('Y-m-d' ,strtotime($offer['departure_destination_arrival_date_time'])),
                            'arrival_time'=>date('H:i' ,strtotime($offer['departure_destination_arrival_date_time'])),
                            'departure_airport'=>$offer['departure_departure_air_code'],
                            'arrival_airport'=>$offer['departure_destination_arrival_air_code'],
                            'departure_city'=>$offer['departure_departure_city'],
                            'arrival_city'=>$offer['departure_destination_arrival_city'],
                            'air_company_logo'=> 'https://pics.avs.io/200/200/' . $offer['departure_airlines_iata'] . '.png',
                        ),
                        'arrival' => array(
                            'type' =>$offer['arrival_flight_type'],
                            'departure_date'=> date('Y-m-d' ,strtotime($offer['arrival_departure_date_time'])),
                            'departure_time'=> date('H:i' ,strtotime($offer['arrival_departure_date_time'])),
                            'arrival_date'=> date('Y-m-d' ,strtotime($offer['arrival_destination_arrival_date_time'])),
                            'arrival_time'=> date('H:i' ,strtotime($offer['arrival_destination_arrival_date_time'])),
                            'departure_airport'=>$offer['arrival_departure_air_code'],
                            'arrival_airport'=>$offer['arrival_destination_arrival_air_code'],
                            'departure_city'=>$offer['arrival_departure_city'],
                            'arrival_city'=>$offer['arrival_destination_arrival_city'],
                            'air_company_logo'=> 'https://pics.avs.io/200/200/' . $offer['arrival_airlines_iata'] . '.png',
                        ),
                    );
                }




                unset(
                    $offers_result[$key]["arrival_destination_arrival_air_code"],
                    $offers_result[$key]["arrival_departure_air_code"],
                    $offers_result[$key]["departure_destination_arrival_air_code"],
                    $offers_result[$key]["departure_departure_air_code"],
                    $offers_result[$key]["departure_departure_city"],
                    $offers_result[$key]["departure_departure_airport"],
                    $offers_result[$key]["departure_departure_air_company"],
                    $offers_result[$key]["departure_departure_date_time"],
                    $offers_result[$key]["departure_destination_arrival_city"],
                    $offers_result[$key]["departure_destination_arrival_airport"],
                    $offers_result[$key]["departure_destination_arrival_date_time"],
                    $offers_result[$key]["arrival_departure_airport"],
                    $offers_result[$key]["arrival_departure_air_company"],
                    $offers_result[$key]["arrival_departure_date_time"],
                    $offers_result[$key]["arrival_destination_arrival_date_time"],
                    $offers_result[$key]["arrival_destination_arrival_airport"],
                    $offers_result[$key]["arrival_departure_city"],
                    $offers_result[$key]["arrival_destination_arrival_city"],
                    $offers_result[$key]['agency_id'],
                    $offers_result[$key]['agency_country'],
                    $offers_result[$key]['agency_city'],
                    $offers_result[$key]['agency_address'],
                    $offers_result[$key]['agency_tour_agent'],
                    $offers_result[$key]['agency_phone'],
                    $offers_result[$key]['agency_email'],
                    $offers_result[$key]['agency_coordinates'],
                    $offers_result[$key]['agency_average_rate'],
                    $offers_result[$key]['agency_photo'],
                    $offers_result[$key]['agency_website'],
                    $offers_result[$key]['number_of_rooms']
                );

            }
        }

        if($result['deprecated'] === 'true'){
            if($result['status'] === 'active'){
                $status ='pending';
            }elseif($result['status'] === 'booked'){
                $status ='pre-booked';
            }else{
                $status = 'canceled';
            }
        }else{
            $status = $result['status'];
        }
        $result['number_of_people'] = (int)  $result['adults'] + (int) $result['children'] + (int) $result['infant'];
        $result['status'] = $status;
        $result['nights'] = $result['nights'] ? $result['nights'] : $result['nights_from'] . ' - ' . $result['nights_to'] ;
        $result['price'] = (!is_null($result['price_to'])  && !is_null($result['price_from'])) ?  $result['price_from'] . ',' . $result['price_to'] : NULL;
        $result['extra'] = $result['extra'] ? unserialize($result['extra']) : NULL;
        $result['board_type'] = $result['board_type'] ? unserialize($result['board_type']) : NULL;
        $result['desired_hotels'] = $result['desired_hotels'] ? unserialize($result['desired_hotels']) : NULL;
        $result['children_ages'] = $result['children_ages'] ? unserialize($result['children_ages']) : NULL;
        $result['period_from'] = apet_datetime_to_date($result['period_from']);
        $result['period_to'] = apet_datetime_to_date($result['period_to']);
        $result['country'] = stripslashes($result['country']);
        $result['city'] = stripslashes($result['city']);
        $result['request_date'] = apet_datetime_to_date(apet_time_zone_convert($result['request_date'], 'GMT', APET_WP_TIMEZONE));
        $result['offers'] = $offers_result;

        echo json_encode(array('data' =>$result, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_single_request', 'apet_get_single_request');
add_action('wp_ajax_apet_get_single_request', 'apet_get_single_request');

//==================ADMIN=========================

function apet_get_users_requests()
{

    if (isset($_POST['user_id'])) {
        global $wpdb;
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $role = apet_get_logged_user_role();
        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if (($tour_status === 'deactivated' || $tour_status === '') && $role !== 'tour-super-admin') {
                response_error_message('unauthorize_access');
            }
        }

        $requests_table = $wpdb->prefix . 'requests';
        $requests_statuses_table = $wpdb->prefix . 'requests_statuses';
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $spec_offers_table = $wpdb->prefix . 'special_offers';
        $book_table = $wpdb->prefix . 'books';
        $agency_offers_table = $wpdb->prefix . 'agency_offers';
        $statuses_sub_query = "(SELECT * FROM `$requests_statuses_table` AS rs WHERE status_id = ( SELECT MAX( status_id ) FROM `$requests_statuses_table` WHERE request_id = rs.request_id ) GROUP BY request_id )";
        $limit = (isset($_POST['limit']) && isset($_POST['offset'])) ? "LIMIT " . sanitize_text_field($_POST['offset']) . ", " . sanitize_text_field($_POST['limit']) : "";
        $get_updates = isset($_POST['get_updates']) ? sanitize_text_field($_POST['get_updates']) : false;
        $extra = array();
        $data = array();
        $filter_options = isset($_POST['filter']) ? json_decode(stripslashes(sanitize_text_field($_POST['filter'])), true) : false;
        $filter = '';
        $allow_canceled = false;
        if ($filter_options !== false) {
            if (isset($filter_options['id']) && !empty($filter_options['id'])){
                $filter .=" AND requests.request_id = ".$filter_options['id'];
            }
            if (isset($filter_options['date'])) {
                if (isset($filter_options['date']['from']) && !empty($filter_options['date']['from'])) {
                    $filter .= " AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $filter_options['date']['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
                }
                if (isset($filter_options['date']['to']) && !empty($filter_options['date']['to'])) {
                    $filter .= " AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $filter_options['date']['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
                }
            }
            if (isset($filter_options['country']) && !empty($filter_options['country'])) {
                $filter .= " AND requests.country LIKE '%" . strtolower($filter_options['country']) . "%' ";
            }
            if (isset($filter_options['status']) && !empty($filter_options['status'])) {

                switch ($filter_options['status']) {
                    case 'pending':
                        $filter .= " AND (offers.offer_author IS NULL OR statuses.status='pending')";
                        break;
                    case 'offered':
                        $filter .= ($tour_id === 'all' && $role === 'tour-super-admin') ? "AND (statuses.status='offered' OR (requests.status !='deprecated' AND offers.offer_author IS NOT NULL AND books.offer_id IS NULL )) " : " AND offers.offer_author = '$tour_id' AND (statuses.status = 'offered' OR ( requests.status !='deprecated' AND books.offer_id IS NULL ))";
                        break;
                    case 'pre-booked':
                        $filter .= ($tour_id === 'all' && $role === 'tour-super-admin') ? "AND (statuses.status='pre-booked') " : " AND (statuses.status='pre-booked' ) AND ( books.offer_author = '$tour_id')";
                        break;
                    case 'processing':
                        $filter .= ($tour_id === 'all' && $role === 'tour-super-admin') ? "AND statuses.status='processing' " : " AND statuses.status='processing' AND books.offer_author = '$tour_id' ";
                        break;
                    case 'confirmed':
                        $filter .= ($tour_id === 'all' && $role === 'tour-super-admin') ? "AND statuses.status='confirmed' " : " AND statuses.status='confirmed' AND books.offer_author = '$tour_id' ";
                        break;
                    case 'canceled':
                        $allow_canceled = ($tour_id === 'all' && $role === 'tour-super-admin') ? true : false;
                        break;
                    case 'booked':
                        $filter .= ($tour_id === 'all' && $role === 'tour-super-admin') ? " AND (requests.status='booked' OR statuses.status='booked') ": " AND ((statuses.status='booked' OR requests.status='booked' ) AND books.offer_author = '$tour_id' )  ";
                        break;

                    /* Deprecated Cases */
                    case 'direct':
                        $filter .= " AND requests.chosen_tour!='' ";
                        break;
                    case 'toall':
                        $filter .= " AND requests.chosen_tour='' ";
                        break;
                }
            }
        }


        if ($tour_id === 'all' && $role === 'tour-super-admin') {
            if ($get_updates === false) {

                if(!$allow_canceled){
                    $condition = "   
                        (
                            DATE_ADD( CONVERT_TZ( requests.period_from, '+00:00', '" . APET_WP_TIMEZONE . "' ), INTERVAL 1 DAY ) >= CONVERT_TZ( UTC_TIMESTAMP ( ), '+00:00', '" . APET_WP_TIMEZONE . "' ) 
                            OR requests.status = 'booked' OR (requests.status = 'deprecated' AND statuses.status = 'booked')
                        )
                        AND (
                            requests.status = 'active' 
                            OR requests.status = 'booked' 
                            OR ( requests.status = 'deprecated' AND statuses.status != 'declined' AND statuses.status != 'canceled' )  
                        )";
                }else{
                    $condition = "
                        (
                            DATE_ADD( CONVERT_TZ( requests.period_from, '+00:00', '" . APET_WP_TIMEZONE . "' ), INTERVAL 1 DAY ) >= CONVERT_TZ( UTC_TIMESTAMP ( ), '+00:00', '" . APET_WP_TIMEZONE . "' ) 
                        )
                        AND(
                            requests.status = 'canceled' 
                            OR statuses.status = 'canceled'  
                        )";
                }


                $query = "SELECT
                        requests.chosen_tour,
                        requests.request_id,
                        requests.request_author,
                        requests.country,
                        requests.city,
                        requests.nights,
                        requests.nights_from,
                        requests.nights_to,
                        IF(offers.request_id = requests.request_id, 'true', 'false') as offered,
                        IF(requests.status != 'deprecated', 'true', 'false') as deprecated,
                        IF(requests.status = 'deprecated', statuses.status,requests.status) as status
                    FROM `$requests_table` requests 
                    LEFT JOIN `$book_table` books ON requests.request_id = books.request_id
                    LEFT JOIN `$agency_offers_table` offers ON ( requests.request_id = offers.request_id ) 
                    LEFT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id 
                    WHERE
                         $condition $filter
                    GROUP BY requests.request_id
                    ORDER BY requests.request_id DESC $limit ";

                $data = $wpdb->get_results($query, ARRAY_A);

                /* Replace new Status for deprecated Requests  */
                foreach ($data as $i => $request) {
                    $data[$i]['country'] = stripslashes($request['country']);
                    $data[$i]['city'] =  $data[$i]['city'] ? stripslashes($request['city']) :  'Any';
                    if(is_null($request['nights'])){
                        $data[$i]['nights'] = $request['nights_from'] . ' - ' . $request['nights_to'];
                    }

                    if ($request['deprecated'] === 'true') {
                        if ($request['status'] === 'active') {
                            $data[$i]['status'] = 'pending';
                        }
                    }
                }

            } else {
                $data_tmp = $wpdb->get_results("
                    SELECT
                        requests.chosen_tour,
                        requests.request_id,
                        requests.request_author,
                        requests.country,
                        requests.city,
                        requests.nights,
                        requests.nights_from,
                        requests.nights_to,
                        IF(requests.status != 'deprecated', 'true', 'false' ) AS deprecated,
                        IF(requests.status = 'deprecated', statuses.status, requests.status ) AS status
                    FROM `$requests_table` AS requests
                    RIGHT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id 
                    WHERE
                        DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY)  >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')
                        AND ( CONVERT_TZ( statuses.set_date, '+00:00', '" . APET_WP_TIMEZONE . "' ) > CONVERT_TZ( '$get_updates', '+00:00', '" . APET_WP_TIMEZONE . "' ) ) 
                        AND statuses.status != 'canceled' 
                        AND statuses.status != 'declined' 
                    GROUP BY
                        requests.request_id 
                    ORDER BY
                        requests.request_id DESC
                    ", ARRAY_A);
                $data['new'] = array();
                $data['updated'] = array();
                foreach ($data_tmp as $i => $request) {

                    if(is_null($request['nights'])){
                        $data_tmp[$i]['nights'] = $request['nights_from'] . ' - ' . $request['nights_to'];
                    }
                    $data_tmp[$i]['country'] = stripslashes($request['country']);
                    $data_tmp[$i]['city'] =  $data_tmp[$i]['city'] ? stripslashes($request['city']) :  'Any';

                    if ($request['status'] === 'pending') {
                        $data['new'][] = $data_tmp[$i];
                    } else {
                        $data['updated'][] = $data_tmp[$i];
                    }
                }
                $data['del'] = $wpdb->get_results("
                        SELECT
                            requests.request_id
                        FROM `$requests_table` requests
                        LEFT JOIN `$book_table` books ON requests.request_id = books.request_id
                        RIGHT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id 
                        WHERE 
                            (
                                CONVERT_TZ('$get_updates','+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY)
                                AND (DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) < CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "') AND requests.status!='booked')
                            )
                            OR (
                                (statuses.status='canceled' OR statuses.status='declined') 
                                AND CONVERT_TZ(statuses.set_date,'+00:00','" . APET_WP_TIMEZONE . "') > CONVERT_TZ('$get_updates','+00:00','" . APET_WP_TIMEZONE . "')
                            )
                        
                        GROUP BY requests.request_id
                        ORDER BY requests.request_id DESC", ARRAY_A);
            }


        }
        else {

            $agency_offers_data = $wpdb->get_results("SELECT COUNT(*) as count FROM $agency_offers_table WHERE MONTH(CONVERT_TZ($agency_offers_table.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "')) = MONTH(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')) AND $agency_offers_table.offer_author=$tour_id", ARRAY_A);
            $spec_offers_data = $wpdb->get_results("SELECT COUNT(*) as count FROM $spec_offers_table WHERE MONTH(offer_creation_date) = MONTH(CURDATE()) AND offer_author='$tour_id'", ARRAY_A);
            $package_plan_data = $wpdb->get_results("SELECT package_plan FROM $tour_table WHERE $tour_table.id=$tour_id ", ARRAY_A);

            $package_plan_data = !empty($package_plan_data) && $package_plan_data[0]['package_plan'] ? unserialize($package_plan_data[0]['package_plan']) : APET_PACKAGE_PLANS['free_plan'];
            $agency_offers_count = !empty($agency_offers_data) ? $agency_offers_data[0]['count'] : false;
            $spec_offers_count = !empty($spec_offers_data) ? $spec_offers_data[0]['count'] : false;

            $req_offer_limit = $package_plan_data['request_offers_limit'];
            $spec_offer_limit = $package_plan_data['special_offers_limit'];

            $req_offers_left = intval($req_offer_limit) - (intval($agency_offers_count));
            $req_offers_left = $req_offers_left <= 0 ? 0 : $req_offers_left;
            $req_offers_left = $req_offer_limit === -1 ? -1 : $req_offers_left;

            $spec_offers_left = intval($spec_offer_limit) - (intval($spec_offers_count));
            $spec_offers_left = $spec_offers_left <= 0 ? 0 : $spec_offers_left;
            $spec_offers_left = $spec_offer_limit === -1 ? -1 : $spec_offers_left;

            $extra['package_plan'] = $package_plan_data;
            $extra['req_offers_left'] = $req_offers_left;
            $extra['spec_offers_left'] = $spec_offers_left;

            if ($get_updates === false) {

                $sql = "
                    SELECT
                        requests.chosen_tour,
                        requests.request_id,
                        requests.request_author,
                        requests.country,
                        requests.city,
                        requests.nights,
                        requests.nights_from,
                        requests.nights_to,
                        IF(requests.status = 'deprecated', statuses.status,requests.status) as status,
                        IF(offers.offer_author = '$tour_id', 'true', 'false') as offered,
                        IF(requests.status != 'deprecated', 'true', 'false') as deprecated
                    FROM `$requests_table` requests 
                    LEFT JOIN `$tour_table` agencies ON (agencies.prefered_countries LIKE concat('%', requests.country, '%')  OR agencies.prefered_countries IS NULL ) 
                    LEFT JOIN `$book_table` books ON requests.request_id = books.request_id  
                    LEFT JOIN `$agency_offers_table` offers ON ( requests.request_id = offers.request_id AND offers.offer_author = '$tour_id' ) 
                    LEFT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id 
                    WHERE
                        agencies.id = '$tour_id' 
                        AND(
                            DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY)  >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "') 
                            OR requests.status='booked'  OR (requests.status='deprecated' AND statuses.status = 'booked') 
                        )
                        AND (
                            (requests.status='active' AND (requests.chosen_tour='$tour_id' OR requests.chosen_tour=''))
                            OR (requests.status = 'booked' AND books.offer_author = '$tour_id' )
                            OR(
                                requests.status = 'deprecated'
                                AND(
                                    statuses.status = 'pending' 
				                    OR statuses.status = 'offered'
				                    OR (
				                        statuses.status != 'pending' 
                                        AND statuses.status != 'offered' 
                                        AND statuses.status != 'canceled' 
                                        AND statuses.status != 'declined'
                                        AND books.offer_author = '$tour_id'
                                        AND offers.offer_status = 'booked'
				                    )
                                )
                            )
                        ) $filter 
                    GROUP BY requests.request_id
                    ORDER BY requests.request_id DESC $limit ";

                $data = $wpdb->get_results($sql, ARRAY_A);



                foreach ($data as $i => $request) {

                    $data[$i]['country'] = stripslashes($request['country']);
                    $data[$i]['city'] =  $data[$i]['city'] ? stripslashes($request['city']) :  'Any';


                    if(is_null($request['nights'])){
                        $data[$i]['nights'] = $request['nights_from'] . ' - ' . $request['nights_to'];
                    }

                    /* Replace new Status for deprecated Requests  */
                    if ($request['deprecated'] === 'true') {
                        if ($request['status'] === 'active') {
                            $data[$i]['status'] = 'pending';
                        }
                    }
                    /* Agency see only its Offered Requests as Offered */
                    if($request['offered'] !== 'true' && $request['status'] == 'offered'){
                        $data[$i]['status'] = 'pending';
                    }
                }

            } else {
                $query = "
                    SELECT
                        requests.chosen_tour,
                        requests.request_id,
                        requests.request_author,
                        requests.country,
                        requests.city,
                        requests.nights,
                        requests.nights_from,
                        requests.nights_to,
                        IF(offers.offer_author = '$tour_id', 'true', 'false') as offered,
                        IF(requests.status = 'deprecated', statuses.status, requests.status ) AS status,
                        IF(requests.status != 'deprecated', 'true', 'false' ) AS deprecated
                    FROM `$requests_table` requests 
                    LEFT JOIN `$tour_table` agencies ON agencies.prefered_countries LIKE concat('%', requests.country, '%')  OR agencies.prefered_countries IS NULL 
                    LEFT JOIN `$book_table` books ON requests.request_id = books.request_id
                    LEFT JOIN `$agency_offers_table` offers ON requests.request_id = offers.request_id AND offers.offer_author = '$tour_id'
                    LEFT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id  
                    WHERE 
                        agencies.id = '$tour_id'
                        AND DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY)  >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')
                        AND ( CONVERT_TZ( statuses.set_date, '+00:00', '" . APET_WP_TIMEZONE . "' ) > CONVERT_TZ( '$get_updates', '+00:00', '" . APET_WP_TIMEZONE . "' ) ) 
                        AND (
                            statuses.status = 'pending'
                            OR statuses.status = 'offered'
                            OR (
                                statuses.status != 'pending' 
                                AND statuses.status != 'offered' 
                                AND statuses.status != 'canceled' 
                                AND statuses.status != 'declined'
                                AND books.offer_author = '$tour_id'
                                AND offers.offer_status = 'booked'
                            )
                        ) 
                    GROUP BY requests.request_id
                    ORDER BY requests.request_id DESC";

                $data_tmp = $wpdb->get_results($query, ARRAY_A);

                $data['new'] = array();
                $data['updated'] = array();

                foreach ($data_tmp as $i => $request) {

                    $data_tmp[$i]['country'] = stripslashes($request['country']);
                    $data_tmp[$i]['city'] =  $data_tmp[$i]['city'] ? stripslashes($request['city']) :  'Any';
                    if(is_null($request['nights'])){
                        $data_tmp[$i]['nights'] = $request['nights_from'] . ' - ' . $request['nights_to'];
                    }

                    if ($request['status'] == 'pending') {
                        $data['new'][] = $data_tmp[$i];
                    } else {
                        if($request['status'] === 'offered' && $request['offered'] !== 'true'){
                            continue;
                        }else{
                            $data['updated'][] = $data_tmp[$i];
                        }
                    }

                }
                $data['del'] = $wpdb->get_results("
                    SELECT 
                        requests.request_id 
                    FROM `$requests_table` requests 
                    LEFT JOIN `$tour_table` agencies ON agencies.prefered_countries LIKE concat('%', requests.country, '%')  OR agencies.prefered_countries IS NULL 
                    LEFT JOIN `$book_table` books ON requests.request_id = books.request_id 
                    LEFT JOIN `$agency_offers_table` offers ON requests.request_id = offers.request_id AND offers.offer_author = '$tour_id'
                    LEFT JOIN $statuses_sub_query AS statuses ON requests.request_id = statuses.request_id  
                    WHERE
                        agencies.id = '$tour_id'
                        AND (
                            (CONVERT_TZ('$get_updates','+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) 
                            AND (DATE_ADD(CONVERT_TZ(requests.period_from,'+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) < CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "') 
                            AND ( requests.status != 'booked' OR statuses.status != 'booked' )))
                            OR (
                                ( statuses.status = 'pre-booked' OR statuses.status = 'confirmed' OR statuses.status = 'processing' ) 
                                AND (books.offer_author!='$tour_id' AND (offers.offer_status != 'booked' OR offers.offer_status IS NULL))
                                AND CONVERT_TZ(statuses.set_date,'+00:00','" . APET_WP_TIMEZONE . "') > CONVERT_TZ('$get_updates','+00:00','" . APET_WP_TIMEZONE . "')
                            )
                            OR (
                                ( statuses.status = 'canceled' OR statuses.status = 'declined' ) 
                                AND CONVERT_TZ( statuses.set_date, '+00:00', '" . APET_WP_TIMEZONE . "' ) > CONVERT_TZ( '$get_updates', '+00:00', '" . APET_WP_TIMEZONE . "' ) 
                            )
                        ) 
                    GROUP BY requests.request_id
                    ORDER BY requests.request_id DESC
                ", ARRAY_A);
            }
        }
        $extra['web_min_version'] = APET_WEB_MIN_VERSION;
        $extra['utc_time'] = date('Y-m-d H:i:s');

        if (empty($data)) {
            response_error_message('no_requests');
        } else {
            echo json_encode(array('extra' => $extra, 'data' => $data, 'success' => 'true','debug'=>$debug));
            wp_die();
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_users_requests', 'apet_get_users_requests');

function apet_get_users_single_request()
{

    if (isset($_POST['user_id'])) {

        global $wpdb;
        $requests_table = $wpdb->prefix . 'requests';
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $books_table = $wpdb->prefix . 'books';
        $agency_offers_table = $wpdb->prefix . 'agency_offers';
        $requests_statuses_table = $wpdb->prefix . 'requests_statuses';
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $request_id = sanitize_text_field($_POST['request_id']);
        $request_author = sanitize_text_field($_POST['request_author']);
        $status = sanitize_text_field($_POST['status']);
        $chosen_tour = sanitize_text_field($_POST['chosen_tour']);
        $role = apet_get_logged_user_role();
        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if (($tour_status === 'deactivated' || $tour_status === '') && $role !== 'tour-super-admin') {
                response_error_message('unauthorize_access');
            }
        }
        $user_data = array();
        if ($tour_id === 'all') {
            $agencyOffers = array();
            $doc_list = array();
            $result = $wpdb->get_results("
                SELECT
                    requests.*,
                    IF(offers.request_id = requests.request_id, 'true', 'false') as offered,
                    IF(requests.status != 'deprecated', 'true', 'false') as deprecated,
                    IF(requests.status = 'deprecated', (SELECT statuses.status FROM `$requests_statuses_table` as statuses WHERE statuses.request_id = requests.request_id ORDER BY statuses.status_id DESC LIMIT 1 ),requests.status) as status
                FROM `$requests_table` as requests LEFT JOIN $agency_offers_table as offers ON requests.request_id = offers.request_id
                WHERE requests.request_id = '$request_id'", ARRAY_A);
            $all_data = get_user_meta($request_author);
            $user_data['lastname'] = $all_data['last_name'][0];
            $user_data['firstname'] = $all_data['first_name'][0];
            $user_data['phone'] = $all_data['phone'][0];
            $user_data['email'] = $all_data['email'][0];

            /* For Deprecated Request */

            if ($result[0]['deprecated'] === 'true') {
                if ($result[0]['status'] === 'active') {
                    $result[0]['status'] = 'pending';
                }
            }

            if ($status === 'pre-booked' || $status === 'processing' || $status === 'confirmed' || $status === 'booked') {
                $agency_query = "
                    SELECT 
                        $agency_offers_table.*,
                        $tour_table.tour_agent,
                        $books_table.book_id,
                        $books_table.book_date,
                        $books_table.travelers_data
                    FROM `$agency_offers_table` JOIN `$books_table` ON `$books_table`.offer_id = `$agency_offers_table`.offer_id  JOIN `$tour_table` ON ( `$agency_offers_table`.offer_author = `$tour_table`.id )
                    WHERE $agency_offers_table.request_id = '$request_id' AND `$agency_offers_table`.offer_status = 'booked' and $books_table.request_id = '$request_id' ";

                $agencyOffers = $wpdb->get_results($agency_query, ARRAY_A);
                if (isset($agencyOffers[0]['travelers_data'])) {
                    $agencyOffers[0]['travelers_data'] = unserialize($agencyOffers[0]['travelers_data']);
                }
                if (isset($agencyOffers[0]['doc_list'])) {
                    $agencyOffers[0]['doc_list'] = unserialize($agencyOffers[0]['doc_list']);
                }
            }
            $result[0]['extra'] = $result[0]['extra'] ? unserialize($result[0]['extra']) : NULL;
            $result[0]['children_ages'] = $result[0]['children_ages'] ? unserialize($result[0]['children_ages']) : NULL;
        } else {

            $result = $wpdb->get_results("
                SELECT
                    requests.*,
                    agency.doc_list,
                    IF(offers.offer_author = '$tour_id', 'true', 'false') as offered,
                    IF(requests.status != 'deprecated', 'true', 'false') as deprecated,
                    IF(requests.status = 'deprecated', (SELECT statuses.status FROM `$requests_statuses_table` as statuses WHERE statuses.request_id = requests.request_id ORDER BY statuses.status_id DESC LIMIT 1 ),requests.status) as status
                FROM `$requests_table` as requests LEFT JOIN $agency_offers_table as offers ON requests.request_id = offers.request_id AND offers.offer_author = '$tour_id',`$tour_table` as agency 
                WHERE requests.request_id = '$request_id' and agency.id='$tour_id' ", ARRAY_A);

            /* For Deprecated Request */

            if ($result[0]['deprecated'] === 'true') {
                if ($result[0]['status'] === 'active' && $result[0]['offered'] === 'true') {
                    $result[0]['status'] = 'offered';
                } elseif ($result[0]['status'] === 'active') {
                    $result[0]['status'] = 'pending';
                }
            }

            if ($status === 'pre-booked' || $status === 'processing' || $status === 'confirmed' || $status === 'booked') {
                $agency_query = "
                    SELECT 
                        $agency_offers_table.*,
                        $books_table.book_id,
                        $books_table.book_date,
                        $books_table.travelers_data
                    FROM `$agency_offers_table` JOIN `$books_table` ON (`$books_table`.offer_id = `$agency_offers_table`.offer_id)
                    WHERE $agency_offers_table.request_id = '$request_id' and $agency_offers_table.offer_author = '$tour_id' and $books_table.offer_author = '$tour_id' and $books_table.request_id = '$request_id' AND `$agency_offers_table`.offer_status = 'booked' ";
            } else {
                $agency_query = "
                    SELECT * 
                    FROM `$agency_offers_table`
                    WHERE request_id = '$request_id' and offer_author ='$tour_id'";
            }

            $agencyOffers = $wpdb->get_results($agency_query, ARRAY_A);

            if (empty($result)) {
                response_error_message('fetching_request_failed');
            } else {
                if ($chosen_tour || $status === 'pre-booked' || $status === 'processing' || $status === 'confirmed' || $status === 'booked') {
                    $all_data = get_user_meta($request_author);
                    $user_data['lastname'] = $all_data['last_name'][0];
                    $user_data['firstname'] = $all_data['first_name'][0];
                    $user_data['phone'] = $all_data['phone'][0];
                    $user_data['email'] = $all_data['email'][0];
                }

                $doc_list = $result[0]['doc_list'] ? unserialize($result[0]['doc_list']) : array();
                unset($result[0]['doc_list']);

                foreach ($agencyOffers as $key => $agencyOffer) {

                    if (isset($agencyOffer['travelers_data'])) {
                        $agencyOffers[$key]['travelers_data'] = unserialize($agencyOffer['travelers_data']);
                    }
                    if (isset($agencyOffer['doc_list'])) {
                        $agencyOffers[$key]['doc_list'] = unserialize($agencyOffer['doc_list']);
                    }
                }
                $result[0]['extra'] = $result[0]['extra'] ? unserialize($result[0]['extra']) : NULL;
                $result[0]['children_ages'] = $result[0]['children_ages'] ? unserialize($result[0]['children_ages']) : NULL;
            }
        }
        $result[0]['country'] = stripslashes($result[0]['country']);
        $result[0]['city'] = stripslashes($result[0]['city']);
        $result[0]['desired_hotels'] = $result[0]['desired_hotels'] ? unserialize($result[0]['desired_hotels']) : NULL;
        $result[0]['board_type'] = $result[0]['board_type'] ? unserialize($result[0]['board_type']) : NULL;
        $result[0]['period_from'] = apet_datetime_to_date($result[0]['period_from']);
        $result[0]['period_to'] = apet_datetime_to_date($result[0]['period_to']);
        $result[0]['request_date'] = apet_datetime_to_date($result[0]['request_date']);
        if (!empty($agencyOffers)) {

            foreach ($agencyOffers as $key => $agencyOffer) {

                $agencyOffers[$key]['arrival'] = $agencyOffer['arrival'] ? apet_datetime_to_date($agencyOffer['arrival']) : $agencyOffer['arrival'];
                $agencyOffers[$key]['departure'] = $agencyOffer['departure'] ? apet_datetime_to_date($agencyOffer['departure']) : $agencyOffer['departure'];
                if (isset($agencyOffer['book_date'])) {
                    $agencyOffers[$key]['book_date'] = apet_datetime_to_date($agencyOffer['book_date']);
                }
                $agencyOffers[$key]['offer_creation_date'] = apet_datetime_to_date(apet_time_zone_convert($agencyOffer['offer_creation_date'], 'GMT', APET_WP_TIMEZONE));
                $agencyOffers[$key]['offer_deadline'] = apet_datetime_to_date($agencyOffer['offer_deadline']);
                $agencyOffers[$key]['doc_deadline'] = apet_datetime_to_date($agencyOffer['doc_deadline']);
                $agencyOffers[$key]['hotel_checkin'] = apet_datetime_to_date($agencyOffer['hotel_checkin']);
                $agencyOffers[$key]['hotel_checkout'] = apet_datetime_to_date($agencyOffer['hotel_checkout']);

                $agencyOffers[$key]['departure_departure_date'] = $agencyOffer['departure_departure_date_time'] ?  date('Y-m-d' ,strtotime($agencyOffer['departure_departure_date_time'])): NULL;
                $agencyOffers[$key]['departure_departure_time'] = $agencyOffer['departure_departure_date_time'] ? date('H:i' ,strtotime($agencyOffer['departure_departure_date_time'])) : NULL;
                $agencyOffers[$key]['departure_destination_arrival_date'] = $agencyOffer['departure_departure_date_time'] ? date('Y-m-d' ,strtotime($agencyOffer['departure_destination_arrival_date_time'])) : NULL;
                $agencyOffers[$key]['departure_destination_arrival_time'] = $agencyOffer['departure_departure_date_time'] ? date('H:i' ,strtotime($agencyOffer['departure_destination_arrival_date_time'])) : NULL;

                $agencyOffers[$key]['arrival_departure_date'] = $agencyOffer['departure_departure_date_time'] ? date('Y-m-d' ,strtotime($agencyOffer['arrival_departure_date_time'])) : NULL;
                $agencyOffers[$key]['arrival_departure_time'] = $agencyOffer['departure_departure_date_time'] ? date('H:i' ,strtotime($agencyOffer['arrival_departure_date_time'])) : NULL;
                $agencyOffers[$key]['arrival_destination_arrival_date'] = $agencyOffer['departure_departure_date_time'] ? date('Y-m-d' ,strtotime($agencyOffer['arrival_destination_arrival_date_time'])) : NULL;
                $agencyOffers[$key]['arrival_destination_arrival_time'] = $agencyOffer['departure_departure_date_time'] ? date('H:i' ,strtotime($agencyOffer['arrival_destination_arrival_date_time'])) : NULL;



                unset($agencyOffers[$key]['departure_departure_date_time'],$agencyOffers[$key]['arrival_departure_date_time'],$agencyOffers[$key]['arrival_destination_arrival_date_time'],$agencyOffers[$key]['departure_destination_arrival_date_time']);

            }
        }
        echo json_encode(array(
            'data' => array(
                'request' => apet_stripslashes_deep($result[0]),
                'doc_list' => $doc_list,
                'agency_offer' => apet_stripslashes_deep($agencyOffers),
                'user_data' => apet_stripslashes_deep($user_data),
            ), 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_users_single_request', 'apet_get_users_single_request');

function apet_change_request_status()
{

    if (isset($_POST['user_id'])) {

        global $wpdb;
        $requests_table = $wpdb->prefix . 'requests';
        $requests_status_table = $wpdb->prefix . 'requests_statuses';
        $offers_table = $wpdb->prefix . 'agency_offers';
        $books_table = $wpdb->prefix . 'books';
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $request_id = sanitize_text_field($_POST['request_id']);
        $status = sanitize_text_field($_POST['status']);
        $role = apet_get_logged_user_role();
        if ($role !== 'tour-super-admin' || $tour_id !== 'all') {
            response_error_message('unauthorize_access');
        }
        /* For Deprecated Request Update status column */
        $result = $wpdb->update(
            $requests_table,
            array('status' => 'deprecated', 'cancel_date' => ''),
            array('request_id' => $request_id)
        );

        if ($result === false) {
            response_error_message('changing_failed');
        }
        $check = $wpdb->insert(
            $requests_status_table, array(
                'request_id' => $request_id,
                'status' => $status,
                'set_date' => date('Y-m-d H:i:s'),
            )
        );
        /* For Declined Re-Active Request For Other Offers */
        if($status === 'declined'){

            $check = $wpdb->insert(
                $requests_status_table, array(
                    'request_id' => $request_id,
                    'status' => 'offered',
                    'set_date' => date('Y-m-d H:i:s'),
                )
            );

            $declined_offer = $wpdb->get_row("SELECT offer_id FROM `$offers_table`  WHERE `$offers_table`.request_id=".$request_id." AND `$offers_table`.offer_status='booked' ",ARRAY_A);
            $update_status = $wpdb->query("
                UPDATE
                  `$offers_table`
                SET " . " $offers_table.offer_status = 
                    CASE 
                        WHEN $offers_table.offer_status = 'booked' THEN 'rejected'
                        ELSE IF($offers_table.offer_id IN (SELECT offer_id from `$books_table` WHERE `$books_table`.request_id='$request_id'),'rejected','active')
                    END
                WHERE $offers_table.request_id='$request_id'");

            $messages = array(
                'hy' => array('title' => 'Մերժում', 'message' => 'Ձեր հարցումը մերժվել է'),
                'en' => array('title' => 'Declined', 'message' => 'Your request has been declined'),
                'ru' => array('title' => 'Не принято', 'message' => 'Ваш запрос не принято')
            );
        }

        if($status === 'confirmed'){

            $messages = array(
                'hy' => array('title' => 'Հաստատում', 'message' => 'Ձեր հարցումը հաստատված է'),
                'en' => array('title' => 'Confirmation', 'message' => 'Your request has been confirmed'),
                'ru' => array('title' => 'Подтверждение', 'message' => 'Ваш запрос подтверждён')
            );
        }

        if($status === 'declined' || $status === 'confirmed'){


            $request = $wpdb->get_row("SELECT request_author FROM `$requests_table`  WHERE `$requests_table`.request_id=$request_id",ARRAY_A);
            $offer = $status === 'confirmed' ? $wpdb->get_row("SELECT offer_id FROM `$offers_table`  WHERE `$offers_table`.request_id=".$request_id." AND `$offers_table`.offer_status='booked' ",ARRAY_A): $declined_offer;
            if(!empty($request) && $request['request_author'] !== '' && !empty($offer) && $offer['offer_id'] !== '' ) {

                $device_data = get_user_meta($request['request_author'], 'device_data', true);

                foreach ($device_data as $key => $device) {
                    $ln = $device['language'];
                    $push_token_id = isset($device['push_token_id']) ? $device['push_token_id'] : '';
                    if ($push_token_id) {
                        apet_send_push_notification($device['platform'], $device['push_token_id'], $messages[$ln]['title'], $messages[$ln]['message'], ['action' => 'offer', 'offer_id' => $offer['offer_id']]);
                    }
                }
            }
        }

        if ($check === false) {
            response_error_message('request_faild');
        }

        echo json_encode(array('data' => array('status' => ($status === 'declined' ? 'offered':$status) ), 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_change_request_status', 'apet_change_request_status');

function apet_save_doc_template()
{
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $doc_list = sanitize_text_field($_POST['doc_list']);
        $template_name = sanitize_text_field($_POST['template_name']);
        $doc_list = json_decode(stripslashes($doc_list), true);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }

        $result = $wpdb->get_row("SELECT doc_list FROM `$tour_table` WHERE id='$tour_id'", ARRAY_A);
        if (empty($result)) {
            response_error_message('failed');
        }

        $all_doc_list = $result['doc_list'] ? unserialize($result['doc_list']) : array();
        $template_data = array('documents_list' => $doc_list, 'template_name' => $template_name);
        $id = array_push($all_doc_list, $template_data);

        $res = $wpdb->update($tour_table, array('doc_list' => serialize($all_doc_list)), array('id' => $tour_id));
        if ($res === false) {
            response_error_message('failed');
        }
        echo json_encode(array('data' => array('template_name' => $template_name, 'documents_list' => $doc_list), 'id' => $id - 1, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_save_doc_template', 'apet_save_doc_template');

function apet_delete_doc_template()
{
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $tour_table = $wpdb->prefix . 'tour_agencies';
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $doc_index = sanitize_text_field($_POST['doc_index']);


        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }

        $result = $wpdb->get_row("SELECT doc_list FROM `$tour_table` WHERE id='$tour_id'", ARRAY_A);
        if (empty($result)) {
            response_error_message('failed');
        }

        $all_doc_list = $result['doc_list'] ? unserialize($result['doc_list']) : array();

        array_splice($all_doc_list, intval($doc_index), 1);

        $res = $wpdb->update($tour_table, array('doc_list' => serialize($all_doc_list)), array('id' => $tour_id));
        if ($res === false) {
            response_error_message('failed');
        }
        echo json_encode(array('data' => array('doc_list' => $all_doc_list), 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_delete_doc_template', 'apet_delete_doc_template');

function apet_make_offer_to_request()
{
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $requests_statuses_table = $wpdb->prefix . 'requests_statuses';
        $agency_offers_table = $wpdb->prefix . 'agency_offers';
        $tour_table_name = $wpdb->prefix . "tour_agencies";
        $requests_table = $wpdb->prefix . "requests";
        $requests_status_table = $wpdb->prefix . "requests_statuses";
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);
        $tour_agent = sanitize_text_field($_POST['tour_agent']);
        $request_id = sanitize_text_field($_POST['request_id']);
        $place_id = sanitize_text_field($_POST['place_id']);

        $transportation_type = sanitize_text_field($_POST['trans_type']);
        $departure_airlines_iata = sanitize_text_field($_POST['departure_airlines_iata']);
        $arrival_airlines_iata = sanitize_text_field($_POST['arrival_airlines_iata']);

        $departure_flight_type = sanitize_text_field($_POST['departure_flight_type']);
        $arrival_flight_type = sanitize_text_field($_POST['arrival_flight_type']);

        $departure_departure_air_code = sanitize_text_field($_POST['departure_departure_air_code']);
        $departure_destination_arrival_air_code = sanitize_text_field($_POST['departure_destination_arrival_air_code']);
        $arrival_departure_air_code = sanitize_text_field($_POST['arrival_departure_air_code']);
        $arrival_destination_arrival_air_code = sanitize_text_field($_POST['arrival_destination_arrival_air_code']);

        $departure_departure_city = sanitize_text_field($_POST['departure_departure_city']);
        $departure_departure_date = sanitize_text_field($_POST['departure_departure_date']);
        $departure_departure_time = sanitize_text_field($_POST['departure_departure_time']);

        $dt = strtotime($departure_departure_date .' '. $departure_departure_time);
        $departure_departure_date_time = date('Y-m-d H:i:s',$dt);

        $departure_departure_airport = sanitize_text_field($_POST['departure_departure_airport']);
        $departure_departure_air_company = sanitize_text_field($_POST['departure_departure_air_company']);
        $departure_destination_arrival_city = sanitize_text_field($_POST['departure_destination_arrival_city']);
        $departure_destination_arrival_date = sanitize_text_field($_POST['departure_destination_arrival_date']);
        $departure_destination_arrival_time = sanitize_text_field($_POST['departure_destination_arrival_time']);

        $dt = strtotime($departure_destination_arrival_date .' '. $departure_destination_arrival_time);
        $departure_destination_arrival_date_time = date('Y-m-d H:i:s',$dt);

        $departure_destination_arrival_airport = sanitize_text_field($_POST['departure_destination_arrival_airport']);


        $arrival_departure_airport = sanitize_text_field($_POST['arrival_departure_airport']);
        $arrival_departure_air_company = sanitize_text_field($_POST['arrival_departure_air_company']);
        $arrival_departure_date = sanitize_text_field($_POST['arrival_departure_date']);
        $arrival_departure_time = sanitize_text_field($_POST['arrival_departure_time']);


        $dt = strtotime($arrival_departure_date .' '. $arrival_departure_time);
        $arrival_departure_date_time = date('Y-m-d H:i:s',$dt);

        $arrival_destination_arrival_date = sanitize_text_field($_POST['arrival_destination_arrival_date']);
        $arrival_destination_arrival_time = sanitize_text_field($_POST['arrival_destination_arrival_time']);

        $dt = strtotime($arrival_destination_arrival_date .' '. $arrival_destination_arrival_time);
        $arrival_destination_arrival_date_time = date('Y-m-d H:i:s',$dt);

        $arrival_destination_arrival_airport = sanitize_text_field($_POST['arrival_destination_arrival_airport']);
        $arrival_departure_city = sanitize_text_field($_POST['arrival_departure_city']);
        $arrival_destination_arrival_city = sanitize_text_field($_POST['arrival_destination_arrival_city']);

        $doc_deadline = sanitize_text_field($_POST['doc_deadline']);
        $board_type = sanitize_text_field($_POST['board_type']);
        $hotel = sanitize_text_field($_POST['hotel']);
        $hotel_checkin = sanitize_text_field($_POST['hotel_checkin']);
        $hotel_checkout = sanitize_text_field($_POST['hotel_checkout']);
        $hotel_rating = sanitize_text_field($_POST['hotel_rating']);
        $number_of_rooms = sanitize_text_field($_POST['number_of_rooms']);

//        $nights = sanitize_text_field($_POST['nights']);
        $offer_deadline = sanitize_text_field($_POST['offer_deadline']);
        $price = sanitize_text_field($_POST['price']);
        $notes = sanitize_text_field($_POST['notes']);
        $request_author = sanitize_text_field($_POST['request_author']);

        $flight_type = isset($_POST['flight_type']) ? sanitize_text_field($_POST['flight_type']) : NULL;

        $departure = isset($_POST['departure']) ? sanitize_text_field($_POST['departure']) : NULL;
        $arrival = isset($_POST['arrival']) ? sanitize_text_field($_POST['arrival']) : NULL;

        $doc_list = sanitize_text_field($_POST['doc_list']);
        $doc_list = json_decode(stripslashes($doc_list), true);
        $doc_list = empty($doc_list) ? NULL : serialize($doc_list);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        }
        $agency_offers_data = $wpdb->get_results("SELECT COUNT(*) as count FROM $agency_offers_table WHERE MONTH(CONVERT_TZ($agency_offers_table.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "')) = MONTH(CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','" . APET_WP_TIMEZONE . "')) AND $agency_offers_table.offer_author=$tour_id", ARRAY_A);
        $agency_offers_count = !empty($agency_offers_data) ? $agency_offers_data[0]['count'] : false;

        $package_plan_data = $wpdb->get_results("SELECT package_plan FROM $tour_table_name WHERE $tour_table_name.id=$tour_id ", ARRAY_A);
        $offer_limit = !empty($package_plan_data) && $package_plan_data[0]['package_plan'] ? unserialize($package_plan_data[0]['package_plan'])['request_offers_limit'] : APET_PACKAGE_PLANS['free_plan']['request_offers_limit'];
        $offers_left = intval($offer_limit) - (intval($agency_offers_count));
        $offers_left = $offer_limit == -1 ? INF : $offers_left;

        if ($offers_left <= 0) {
            response_error_message('offers_max_limit_reached');
        }

        $creation_date = date('Y-m-d H:i:s');
        $offer_insert = $wpdb->insert(
            $agency_offers_table, array(
                'request_id' => $request_id,
                'offer_author' => $tour_id,
                'doc_list' => $doc_list,
                'doc_deadline' => null,
                'departure_flight_type' => $departure_flight_type,
                'arrival_flight_type' => $arrival_flight_type,
                'board_type' => $board_type,
                'hotel' => $hotel,
                'number_of_rooms' => $number_of_rooms,
                'hotel_checkin' => $hotel_checkin,
                'hotel_checkout' => $hotel_checkout,
                'hotel_rating' => $hotel_rating,
//                'nights' => $nights,
                'offer_deadline' => $offer_deadline,
                'price' => $price,
                'notes' => $notes,
//                'departure' => $departure,
//                'arrival' => $arrival,
                'offer_status' => 'active',
                'seen_status' => 'new',
                'offer_creation_date' => $creation_date,
                'place_id'=> $place_id,
                'trans_type'=> $transportation_type,
                'departure_airlines_iata' =>$departure_airlines_iata,
                'arrival_airlines_iata' =>$arrival_airlines_iata,
                'departure_departure_city' =>$departure_departure_city,
                'departure_departure_airport' =>$departure_departure_airport,
                'departure_departure_air_company' =>$departure_departure_air_company,
                'departure_departure_date_time' => $departure_departure_date_time,
                'departure_destination_arrival_city' =>$departure_destination_arrival_city,
                'departure_destination_arrival_airport' =>$departure_destination_arrival_airport,
                'departure_destination_arrival_date_time' =>$departure_destination_arrival_date_time,
                'arrival_departure_airport' =>$arrival_departure_airport,
                'arrival_departure_air_company' =>$arrival_departure_air_company,
                'arrival_departure_date_time' =>$arrival_departure_date_time,
                'arrival_destination_arrival_date_time' =>$arrival_destination_arrival_date_time,
                'arrival_destination_arrival_airport' =>$arrival_destination_arrival_airport,
                'arrival_departure_city' =>$arrival_departure_city,
                'arrival_destination_arrival_city' =>$arrival_destination_arrival_city,
                'departure_departure_air_code' => $departure_departure_air_code,
                'departure_destination_arrival_air_code' => $departure_destination_arrival_air_code,
                'arrival_departure_air_code' => $arrival_departure_air_code,
                'arrival_destination_arrival_air_code' => $arrival_destination_arrival_air_code,

            )
        );
        if ($offer_insert === false) {
            response_error_message('offer_failed');
        } else {
            $offer_id = $wpdb->insert_id;
            /* ========Update Request Status to 'Offered'===========*/
            $res = $wpdb->update(
                $requests_table,
                array(
                    'status' => 'deprecated'
                ),
                array(
                    'request_id' => $request_id,
                )
            );

            $status = $wpdb->get_row("SELECT status FROM `$requests_status_table` WHERE `$requests_status_table`.request_id='$request_id' ORDER BY `$requests_status_table`.status_id DESC LIMIT 1",ARRAY_A);
            if(!empty($status) && $status['status'] !== 'offered'){
                $res = $wpdb->insert(
                    $requests_status_table, array(
                        'request_id' => $request_id,
                        'status' => 'offered',
                        'set_date' => date('Y-m-d H:i:s'),
                    )
                );
                if ($res === false) {
                    response_error_message('offer_failed');
                }
            }

            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }

        $device_data = get_user_meta($request_author, 'device_data', true);

        $title['en'] = empty($tour_agent) ? 'New offer' : 'New offer from ' . $tour_agent;
        $title['ru'] = empty($tour_agent) ? 'Новое предложение' : 'Новое предложение от ' . $tour_agent;
        $title['hy'] = empty($tour_agent) ? 'Նոր առաջարկ' : 'Նոր առաջարկ ' . $tour_agent . '-ից';

        $messages = array(
            'hy' => array('title' => $title['hy'], 'message' => 'Դուք ունեք առաջարկ ձեր հարցումով:'),
            'en' => array('title' => $title['en'], 'message' => 'You have offer to your request.'),
            'ru' => array('title' => $title['ru'], 'message' => 'У вас есть предложение по вашему запросу.')
        );

        foreach ($device_data as $key => $device) {
            $ln = $device['language'];
            $push_token_id = isset($device['push_token_id']) ? $device['push_token_id'] : '';
            if ($push_token_id) {
                apet_send_push_notification($device['platform'], $device['push_token_id'], $messages[$ln]['title'], $messages[$ln]['message'], ['action' => 'offer', 'offer_id' => $offer_id]);
            }
        }

        $creation_date = apet_datetime_to_date(apet_time_zone_convert($creation_date, 'GMT', APET_WP_TIMEZONE));
        echo json_encode(array('data' => array('offer_id' => $offer_id, 'offer_creation_date' => $creation_date, 'offer_left' => (($offer_limit === -1 ? -1 : $offers_left - 1))), 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_make_offer_to_request', 'apet_make_offer_to_request');

function apet_edit_request_offer()
{
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $agency_offers_table = $wpdb->prefix . 'agency_offers';
        $offer_id = sanitize_text_field($_POST['offer_id']);
        $user_id = sanitize_text_field($_POST['user_id']);
        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        }

        $place_id = sanitize_text_field($_POST['place_id']);

        $transportation_type = sanitize_text_field($_POST['trans_type']);
        $departure_airlines_iata = sanitize_text_field($_POST['departure_airlines_iata']);
        $arrival_airlines_iata = sanitize_text_field($_POST['arrival_airlines_iata']);

        $departure_flight_type = sanitize_text_field($_POST['departure_flight_type']);
        $arrival_flight_type = sanitize_text_field($_POST['arrival_flight_type']);

        $departure_departure_air_code = sanitize_text_field($_POST['departure_departure_air_code']);
        $departure_destination_arrival_air_code = sanitize_text_field($_POST['departure_destination_arrival_air_code']);
        $arrival_departure_air_code = sanitize_text_field($_POST['arrival_departure_air_code']);
        $arrival_destination_arrival_air_code = sanitize_text_field($_POST['arrival_destination_arrival_air_code']);

        $departure_departure_city = sanitize_text_field($_POST['departure_departure_city']);
        $departure_departure_date = sanitize_text_field($_POST['departure_departure_date']);
        $departure_departure_time = sanitize_text_field($_POST['departure_departure_time']);

        $dt = strtotime($departure_departure_date .' '. $departure_departure_time);
        $departure_departure_date_time = date('Y-m-d H:i:s',$dt);

        $departure_departure_airport = sanitize_text_field($_POST['departure_departure_airport']);
        $departure_departure_air_company = sanitize_text_field($_POST['departure_departure_air_company']);
        $departure_destination_arrival_city = sanitize_text_field($_POST['departure_destination_arrival_city']);
        $departure_destination_arrival_date = sanitize_text_field($_POST['departure_destination_arrival_date']);
        $departure_destination_arrival_time = sanitize_text_field($_POST['departure_destination_arrival_time']);

        $dt = strtotime($departure_destination_arrival_date .' '. $departure_destination_arrival_time);
        $departure_destination_arrival_date_time = date('Y-m-d H:i:s',$dt);

        $departure_destination_arrival_airport = sanitize_text_field($_POST['departure_destination_arrival_airport']);


        $arrival_departure_airport = sanitize_text_field($_POST['arrival_departure_airport']);
        $arrival_departure_air_company = sanitize_text_field($_POST['arrival_departure_air_company']);
        $arrival_departure_date = sanitize_text_field($_POST['arrival_departure_date']);
        $arrival_departure_time = sanitize_text_field($_POST['arrival_departure_time']);


        $dt = strtotime($arrival_departure_date .' '. $arrival_departure_time);
        $arrival_departure_date_time = date('Y-m-d H:i:s',$dt);

        $arrival_destination_arrival_date = sanitize_text_field($_POST['arrival_destination_arrival_date']);
        $arrival_destination_arrival_time = sanitize_text_field($_POST['arrival_destination_arrival_time']);

        $dt = strtotime($arrival_destination_arrival_date .' '. $arrival_destination_arrival_time);
        $arrival_destination_arrival_date_time = date('Y-m-d H:i:s',$dt);

        $arrival_destination_arrival_airport = sanitize_text_field($_POST['arrival_destination_arrival_airport']);
        $arrival_departure_city = sanitize_text_field($_POST['arrival_departure_city']);
        $arrival_destination_arrival_city = sanitize_text_field($_POST['arrival_destination_arrival_city']);

        $board_type = sanitize_text_field($_POST['board_type']);
        $hotel = sanitize_text_field($_POST['hotel']);
        $hotel_checkin = sanitize_text_field($_POST['hotel_checkin']);
        $hotel_checkout = sanitize_text_field($_POST['hotel_checkout']);
        $hotel_rating = sanitize_text_field($_POST['hotel_rating']);
        $number_of_rooms = sanitize_text_field($_POST['number_of_rooms']);

        $offer_deadline = sanitize_text_field($_POST['offer_deadline']);
        $price = sanitize_text_field($_POST['price']);
        $notes = sanitize_text_field($_POST['notes']);
        $request_author = sanitize_text_field($_POST['request_author']);


        $res = $wpdb->update($agency_offers_table,
            array(
                'departure_flight_type' => $departure_flight_type,
                'arrival_flight_type' => $arrival_flight_type,
                'board_type' => $board_type,
                'hotel' => $hotel,
                'number_of_rooms' => $number_of_rooms,
                'hotel_checkin' => $hotel_checkin,
                'hotel_checkout' => $hotel_checkout,
                'hotel_rating' => $hotel_rating,
                'offer_deadline' => $offer_deadline,
                'price' => $price,
                'notes' => $notes,
                'seen_status' => 'new',
                'place_id'=> $place_id,
                'trans_type'=> $transportation_type,
                'departure_airlines_iata' =>$departure_airlines_iata,
                'arrival_airlines_iata' =>$arrival_airlines_iata,
                'departure_departure_city' =>$departure_departure_city,
                'departure_departure_airport' =>$departure_departure_airport,
                'departure_departure_air_company' =>$departure_departure_air_company,
                'departure_departure_date_time' => $departure_departure_date_time,
                'departure_destination_arrival_city' =>$departure_destination_arrival_city,
                'departure_destination_arrival_airport' =>$departure_destination_arrival_airport,
                'departure_destination_arrival_date_time' =>$departure_destination_arrival_date_time,
                'arrival_departure_airport' =>$arrival_departure_airport,
                'arrival_departure_air_company' =>$arrival_departure_air_company,
                'arrival_departure_date_time' =>$arrival_departure_date_time,
                'arrival_destination_arrival_date_time' =>$arrival_destination_arrival_date_time,
                'arrival_destination_arrival_airport' =>$arrival_destination_arrival_airport,
                'arrival_departure_city' =>$arrival_departure_city,
                'arrival_destination_arrival_city' =>$arrival_destination_arrival_city,
                'departure_departure_air_code' => $departure_departure_air_code,
                'departure_destination_arrival_air_code' => $departure_destination_arrival_air_code,
                'arrival_departure_air_code' => $arrival_departure_air_code,
                'arrival_destination_arrival_air_code' => $arrival_destination_arrival_air_code,
            ),
            array('offer_id' => $offer_id)
        );

        if ($res === false) {
            response_error_message('offer_failed');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if ($tour_status === 'deactivated' || $tour_status === '') {
                response_error_message('unauthorize_access');
            }
        }

        $device_data = get_user_meta($request_author, 'device_data', true);

        $title['en'] = empty($tour_agent) ? 'Offer updated' : 'Offer from ' . $tour_agent . ' updated';
        $title['ru'] = empty($tour_agent) ? 'Предложение обновлено' : 'Предложение от ' . $tour_agent . ' обновлено';
        $title['hy'] = empty($tour_agent) ? 'Առաջարկը թարմացվել է' : $tour_agent . '-ի առաջարկը թարմացվել է';

        $messages = array(
            'hy' => array('title' => $title['hy'], 'message' => 'Ձեր հարցման առաջարկը թարմացվել է:'),
            'en' => array('title' => $title['en'], 'message' => 'Offer to your request was updated.'),
            'ru' => array('title' => $title['ru'], 'message' => 'Предложение по вашему запросу было обновлено.')
        );

        foreach ($device_data as $key => $device) {
            $ln = $device['language'];
            $push_token_id = isset($device['push_token_id']) ? $device['push_token_id'] : '';
            if ($push_token_id) {
                apet_send_push_notification($device['platform'], $device['push_token_id'], $messages[$ln]['title'], $messages[$ln]['message'], ['action' => 'offer', 'offer_id' => $offer_id]);
            }
        }

        echo json_encode(array('data' => array('offer_id' => $offer_id), 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_edit_request_offer', 'apet_edit_request_offer');

function apet_airports_autocomplate()
{
    if (isset($_POST['user_id'])) {
        global $wpdb;


        $user_id = sanitize_text_field($_POST['user_id']);
        $search = sanitize_text_field($_POST['search']);
        $airports_table = $wpdb->prefix . 'airports';

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        }

        $query = "
            SELECT
                *
            FROM
            `$airports_table` AS airport
            WHERE airport.municipality IS NOT NULL AND airport.municipality != ''
            ORDER BY ((airport.name LIKE '%$search%') OR (airport.municipality LIKE '%$search%') OR (airport.iata_code LIKE '%$search%')) DESC 
            LIMIT 5";

        $data = $wpdb->get_results($query, ARRAY_A);

        $response = $data;

        echo json_encode(array('data' => $response, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_airports_autocomplate', 'apet_airports_autocomplate');
