<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * ===== Inserting new User ======
 *
 * @global type $wpdb
 * @param arr $data
 * @return boolean| int
 */
function apet_inert_user(array $data , $re_activate, $send_varify_mail = true) {
    $user_type = $data['user_type'];
    $firstname = $data['firstname'];
    $lastname = $data['lastname'];
    $email = $data['email'];
    $phone = $data['phone'];
    $password = $data['password'];
    $platform = $data['platform'];
    $device_id = $data['device_id'];
    $language = $data['language'];
    $firebase_uid = $data['firebase_uid'];
    $user_status = isset($data['user_status']) ? $data['user_status'] : 'unconfirmed';

    $login = $email;
    $pending_email = null;
    if($user_status === "unconfirmed" && $email !== $firebase_uid){
        $pending_email = $email;
    }
    if ($re_activate) {
        $userdata = array(
            'ID' => $re_activate,
            'user_login' => $login,
            'user_pass' => $password,
            'user_email' => $email,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'role' => 'app-user',
            'user_registered' => date('Y-m-d H:i:s'),
        );

        $user_id = wp_update_user($userdata);
    } else {
        $userdata = array(
            'user_login' => $login,
            'user_pass' => $password,
            'user_email' => $email,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'role' => 'app-user',
            'user_registered' => date('Y-m-d H:i:s'),
        );

        $user_id = wp_insert_user($userdata);
    }

    if (!is_wp_error($user_id)) {
        update_user_meta($user_id, "phone", $phone);
        update_user_meta($user_id, "firebase_uid", $firebase_uid);
        update_user_meta($user_id, "bonus", '0');
        update_user_meta($user_id, "bonus_data", NULL);
        update_user_meta($user_id, "email", $email);
        update_user_meta($user_id, "pending_email", $pending_email);
        update_user_meta($user_id, "user_type", $user_type);
        update_user_meta($user_id, "user_status", $user_status);
        $device_data = array($device_id => array('device_id' => $device_id, 'language' => $language, 'platform' => $platform));
        update_user_meta($user_id, "device_data", $device_data);

        if($send_varify_mail && $user_status !== 'active'){
            $email_success = apet_send_verification_mail($firebase_uid,$language,$user_id);
            if (!$email_success) {
                response_error_message('email_failed');
            }
        }
        return $user_id;
    } else {
        return false;
    }
}

/**
 * ========= Ajax handler for registring users ========
 *
 */
function apet_app_user_registration() {
    global $auth;

    if (isset($_POST['firstname'])) {
        $validate = true;
        $user_type = sanitize_text_field($_POST['user-type']);
        $firstname = sanitize_text_field($_POST['firstname']);
        $lastname = sanitize_text_field($_POST['lastname']);
        $platform = sanitize_text_field($_POST['platform']);
        $device_id = sanitize_text_field($_POST['device_id']);
        $language = sanitize_text_field($_POST['language']);
        $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? $_POST['email'] : $validate = false;
        $phone = sanitize_text_field($_POST['phone']);
        $password = strlen($_POST['password']) > 6 ? $_POST['password'] : $validate = false;
        if ($validate === false) {
            response_error_message('validation_error');
        }
        $check_email = apet_check_user_email($email);
        $re_activate = false;
        if ($check_email !== false) {
            $user_status = get_user_meta($check_email, 'user_status', true);
            $user_type = get_user_meta($check_email, 'user_type', true);
            if ($user_status === 'active' || $user_type !== 'app-user') {
                response_error_message('user_exist');
            } else {
                $firebase_uid = get_user_meta($check_email,'firebase_uid',true);
                $re_activate = $check_email;
            }
        }

        $userProperties = [
            'email' => $email,
            'emailVerified' => false,
            'password' => $password,
            'displayName' => $firstname . ' ' . $lastname,
            'disabled' => false,
        ];

        if(!$re_activate){
            try{
                $createdUser = $auth->createUser($userProperties);
                $firebase_uid = $createdUser->uid;
            }catch (Exception $e){
                response_error_message($e->getMessage());
            }
        }else {
            try{
                $uid = $firebase_uid;
                $userProperties = [
                    'email' => $email,
                    'emailVerified' => false,
                    'password' => $password,
                    'displayName' => $firstname . ' ' . $lastname,
                    'disabled' => false,
                ];
                $updatedUser = $auth->updateUser($uid,$userProperties);
                $firebase_uid = $updatedUser->uid;
            }catch (Exception $e){
                response_error_message($e->getMessage());
            }
        }

        $data = array(
            'user_type'     => $user_type,
            'firstname'     => $firstname,
            'lastname'      => $lastname,
            'email'         => $email,
            'phone'         => $phone,
            'password'      => $password,
            'platform'      => $platform,
            'language'      => $language,
            'device_id'     => $device_id,
            'firebase_uid'  => $firebase_uid
        );
        $user_insert = apet_inert_user($data, $re_activate);

        if(!$user_insert){
            response_error_message('failed_register');
        }

        $user_data = apet_check_login(array(
            'email' => $email,
            'language' => $language,
            'password' => $password,
            'user-type' => $user_type,
            'platform' => $platform,
            'device_id' => $device_id
        ));

        if(!$user_data){
            response_error_message('failed_to_login');
        }

        $response = array('success' => 'true','data' => $user_data);

        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_app_user_registration', 'apet_app_user_registration');
add_action('wp_ajax_apet_app_user_registration', 'apet_app_user_registration');

/**
 * ===== Checking  Login =====
 *
 * @global type $wpdb
 * @param array $data
 * @return bool|arr
 */
function apet_check_login( $data ) {
    $email = $data['email'];
    $password = $data['password'];
    $user_type = $data['user-type'];
    $platform = $data['platform'];
    $device_id = $data['device_id'];
    $language = $data['language'];
    $check_user = wp_authenticate($email, $password);
    $check = false;
    if (!is_wp_error($check_user)) {
        $role = $check_user->roles[0];
        if ($role !== 'app-user') {
            return false;
        }
        $result = array();
        $all_data = get_user_meta($check_user->ID);
        $result['lastname'] = $all_data['last_name'][0];
        $result['firstname'] = $all_data['first_name'][0];
        $result['phone'] = $all_data['phone'][0];
        $result['email'] = $all_data['email'][0];
        $result['bonus'] = $all_data['bonus'][0];
        $result['pending_email'] = $all_data['pending_email'][0];
        $result['bonus_unit_price'] = APET_BONUS_UNIT_PRICE;
        $bonus_data = $all_data['bonus_data'][0] ? unserialize($all_data['bonus_data'][0]) : NULL;
        $result['bonus_id'] = $bonus_data ? $bonus_data['bonus_id'] : NULL;
        $result['withdrawn_bonus'] = $bonus_data ? $bonus_data['withdrawn_bonus'] : NULL;
        $result['user_id'] = $check_user->ID;
        $result['user_status'] = $all_data['user_status'][0];
        $result['firebase_uid'] = $all_data['firebase_uid'][0];
        $device_data = isset($all_data['device_data'][0]) ? unserialize($all_data['device_data'][0]) : array();

        $device_data[$device_id] = array('device_id' => $device_id, 'language' => $language, 'platform' => $platform);


        $check = ($user_type === $all_data['user_type'][0]) ? $result : false;
        if ($check) {
            update_user_meta($check_user->ID, "device_data", $device_data);
        }
    }

    return $check;
}

/**
 * ===== Ajax handler for login ====
 *
 */
function apet_app_user_login() {
    if (isset($_POST['email'])) {
        global $auth;

        $validate = true;
        $app_version = isset($_POST['app_version']) ? sanitize_text_field($_POST['app_version']) : false;
        $user_type = sanitize_text_field($_POST['user-type']);
        $device_id = sanitize_text_field($_POST['device_id']);
        $platform = sanitize_text_field($_POST['platform']);
        $language = isset($_POST['language']) ? sanitize_text_field($_POST['language']) : 'en';
        $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? $_POST['email'] : $validate = false;
        $password = strlen($_POST['password']) > 6 ? $_POST['password'] : $validate = false;

        if ($validate === false) {
            response_error_message('validation_error');
        }

        $user_data = apet_check_login(array(
            'email' => $email,
            'language' => $language,
            'password' => $password,
            'user-type' => $user_type,
            'platform' => $platform,
            'device_id' => $device_id
        ));

        //START: Fix for apps lower then v. 1.0.21 to force update to min major app version.
        if($app_version === false){
            $user_data['firstname'] = $user_data['firstname'] . APET_OLD_APPS_FORCE_UPDATE_FIX;
        }
        //END

        if(!$user_data){
            response_error_message('invalid_user');
        }
        if($user_data['user_status'] === 'unconfirmed'){
            $user = $auth->getUser($user_data['firebase_uid']);
            if($user->emailVerified){
                update_user_meta($user_data['user_id'],'user_status','active');
                $user_data['user_status'] = 'active';
            }
        }
        $customToken = $auth->createCustomToken($user_data['firebase_uid']);
        $customTokenString = (string) $customToken;
       echo json_encode(array(
           'data' => $user_data,
           'success' => 'true',
           'customToken' => $customTokenString)
       );
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_app_user_login', 'apet_app_user_login');
add_action('wp_ajax_apet_app_user_login', 'apet_app_user_login');
/**
 * ========= Firebase Login Integration part =============
 *
 */
function apet_insert_user_from_firebase($add_data,$firebase_data){

    global $auth;

    $user_type = 'app-user';
    $user_status = 'active';
    $platform = $add_data['platform'];
    $device_id = $add_data['device_id'];
    $language = $add_data['language'];
    $firebase_uid = $firebase_data->uid;
    $firstname = explode (' ',$firebase_data->displayName)[0];
    $lastname =  explode (' ',$firebase_data->displayName)[1];
    $email = $firebase_data->email;
    $phone = $firebase_data->phoneNumber;

    if(is_null($email)){
        $email = $firebase_uid;
        $user_status = 'unconfirmed';
    }else{
        $properties = [
            'emailVerified' => true
        ];
        $updatedUser = $auth->updateUser($firebase_data->uid, $properties);
    }

    $password = wp_generate_password(12, false);

    $data = array(
        'user_type'     => $user_type,
        'firstname'     => $firstname,
        'lastname'      => $lastname,
        'email'         => $email,
        'phone'         => $phone,
        'password'      => $password,
        'platform'      => $platform,
        'language'      => $language,
        'device_id'     => $device_id,
        'firebase_uid'  => $firebase_uid,
        'user_status'   => $user_status
    );


    $user_insert = apet_inert_user($data, false,false);


    if($user_insert){
        $all_data = get_user_meta($user_insert);
        $result['lastname'] = $all_data['last_name'][0];
        $result['firstname'] = $all_data['first_name'][0];
        $result['phone'] = $all_data['phone'][0];
        $result['email'] = $all_data['email'][0];
        $result['bonus'] = $all_data['bonus'][0];
        $result['pending_email'] = $all_data['pending_email'][0];
        $result['bonus_unit_price'] = APET_BONUS_UNIT_PRICE;
        $bonus_data = $all_data['bonus_data'][0] ? unserialize($all_data['bonus_data'][0]) : NULL;
        $result['bonus_id'] = $bonus_data ? $bonus_data['bonus_id'] : NULL;
        $result['withdrawn_bonus'] = $bonus_data ? $bonus_data['withdrawn_bonus'] : NULL;
        $result['user_id'] = $user_insert;
        $result['firebase_uid'] = $all_data['firebase_uid'][0];
        $result['user_status'] = $all_data['user_status'][0];
        $device_data = isset($all_data['device_data'][0]) ? unserialize($all_data['device_data'][0]) : array();

        $device_data[$device_id] = array('device_id' => $device_id, 'language' => $language, 'platform' => $platform);

        $check = ($user_type === $all_data['user_type'][0]) ? $result : false;
        if ($check) {
            update_user_meta($user_insert, "device_data", $device_data);
        }

        return $check;
    }
    return false;
}

function apet_firebase_login(){
    if(isset($_POST['token'])){

        global $auth,$wpdb;
        $token = $_POST['token'];
        $user_type = sanitize_text_field($_POST['user-type']);
        $device_id = sanitize_text_field($_POST['device_id']);
        $platform = sanitize_text_field($_POST['platform']);
        $language = isset($_POST['language']) ? sanitize_text_field($_POST['language']) : 'en';

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){

            $uid = $token_info['uid'];
            $firebase_data = apet_get_user_from_firebase($uid);

            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' =>'app-user'
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' =>$uid
                    )
                )
            );
            $user = get_users($args);
            $user_changed = false;
            if (!empty($user)) {
                $user_id = $user[0]->ID;
                $user_data = array();
                $providers = $firebase_data->providerData;

                $exist = false;
                $social_exist = false;
                $fb_email = null;
                foreach ($providers as $prov){
                    if($prov->providerId === "password"){
                        $exist = true;
                    }elseif($prov->providerId === "facebook.com"){
                        $fb_email = $prov->email;
                        $social_exist = true;
                    }else{
                        $social_exist = true;
                    }

                }

                if(!$exist){
                    $password = wp_generate_password(12, false);
                    wp_set_password($password, $user[0]->ID);
                }

                if(!is_null($firebase_data->email) && $social_exist){
                    update_user_meta($user[0]->ID,'user_status','active');
                    $properties = [
                        'emailVerified' => true
                    ];
                    $updatedUser = $auth->updateUser($uid, $properties);
                }elseif(is_null($firebase_data->email) && $social_exist && !is_null($fb_email)){
                    if($user[0]->user_login === $firebase_data->uid){

                        $email = $fb_email;

                        $user_by_fb_email_uid = apet_get_user_by_email_in_fr($fb_email);

                        if($user_by_fb_email_uid === $firebase_data->uid) {

                            $userdata = array(
                                'user_login' => $email,
                                'user_email' => $email,
                            );
                            $user_login = $wpdb->update($wpdb->users, $userdata, array('ID' => $user_id));
                            update_user_meta($user_id, 'email', $email);
                            update_user_meta($user_id, 'user_status', 'active');

                            if ($user_login) {
                                $properties = [
                                    'email' => $email,
                                    'emailVerified' => true
                                ];
                                try {
                                    $updatedUser = $auth->updateUser($uid, $properties);
                                } catch (Exeption $e) {
                                    response_error_message($e->getMessage());
                                }
                            } else {
                                response_error_message('user_update_failed');
                            }
                        }else{
                            try{
                                $auth->deleteUser($firebase_data->uid);
                                wp_delete_user( $user_id );

                                $args = array(
                                    'meta_query' => array(
                                        array(
                                            'key' => 'user_type',
                                            'value' =>'app-user'
                                        ),
                                        array(
                                            'key' => 'firebase_uid',
                                            'value' =>$user_by_fb_email_uid
                                        )
                                    )
                                );
                                $user = get_users($args);
                                if(!empty($user)){
                                    $user_id = $user[0]->ID;
                                    $user_changed = true;
                                }else{
                                    response_error_message('invalid_user');
                                }
                            }catch(Exception $e){
                                response_error_message('invalid_user');
                            }
                        }
                    }else{
                        response_error_message('unauthorize_access');
                    }
                }



                $all_data = get_user_meta($user_id);
                $user_data['user_id'] = $user_id;
                $user_data['firebase_uid'] = $all_data['firebase_uid'][0];
                $user_data['lastname'] = $all_data['last_name'][0];
                $user_data['firstname'] = $all_data['first_name'][0];
                $user_data['phone'] = $all_data['phone'][0];
                $user_data['email'] = $all_data['email'][0];
                $user_data['bonus'] = $all_data['bonus'][0];
                $user_data['pending_email'] = $all_data['pending_email'][0];
                $user_data['bonus_unit_price'] = APET_BONUS_UNIT_PRICE;
                $bonus_data = $all_data['bonus_data'][0] ? unserialize($all_data['bonus_data'][0]) : NULL;
                $user_data['bonus_id'] = $bonus_data ? $bonus_data['bonus_id'] : NULL;
                $user_data['withdrawn_bonus'] = $bonus_data ? $bonus_data['withdrawn_bonus'] : NULL;
                $user_data['user_status'] = $all_data['user_status'][0];
                $device_data = isset($all_data['device_data'][0]) ? unserialize($all_data['device_data'][0]) : array();

                $device_data[$device_id] = array('device_id' => $device_id, 'language' => $language, 'platform' => $platform);

                $user_data = ($user_type === $all_data['user_type'][0]) ? $user_data : false;

                if($user_data['phone'] === 'temp'){

                    foreach ($providers as $prov){
                        if($prov->providerId === "facebook.com" || $prov->providerId === "google.com"){
                            $display_name = $prov->displayName;
                            $user_data['firstname'] = explode(' ',$display_name)[0];
                            $user_data['lastname'] = explode(' ',$display_name)[1];
                            $user_data['phone'] = $prov->phoneNumber;
                            update_user_meta($user_id,'phone','');
                            update_user_meta($user_id,'first_name',$user_data['firstname']);
                            update_user_meta($user_id,'last_name',$user_data['lastname']);
                        }
                    }
                    update_user_meta($user_id,'pending_email','');
                }


                if ($user_data) {
                    update_user_meta($user_id, "device_data", $device_data);
                }else{
                    response_error_message('invalid_user');
                }
            } else {

                $user_id = apet_check_user_email($firebase_data->email);

                if($user_id){
                    update_user_meta($user_id,'firebase_uid',$uid);
                    update_user_meta($user_id,'user_status','active');

                    $all_data = get_user_meta($user_id);
                    $user_data['user_id'] = $user_id;
                    $user_data['lastname'] = $all_data['last_name'][0];
                    $user_data['firstname'] = $all_data['first_name'][0];
                    $user_data['phone'] = $all_data['phone'][0];
                    $user_data['email'] = $all_data['email'][0];
                    $user_data['bonus'] = $all_data['bonus'][0];
                    $user_data['pending_email'] = $all_data['pending_email'][0];
                    $user_data['bonus_unit_price'] = APET_BONUS_UNIT_PRICE;
                    $bonus_data = $all_data['bonus_data'][0] ? unserialize($all_data['bonus_data'][0]) : NULL;
                    $user_data['bonus_id'] = $bonus_data ? $bonus_data['bonus_id'] : NULL;
                    $user_data['withdrawn_bonus'] = $bonus_data ? $bonus_data['withdrawn_bonus'] : NULL;
                    $user_data['firebase_uid'] = $all_data['firebase_uid'][0];
                    $user_data['token'] = $token;

                    echo json_encode(array(
                        'success'   => 'true',
                        'data'      => $user_data
                    ));
                    wp_die();

                }
                /*   create Account in our DB  */

                $add_data = array(
                    'user_type'     => $user_type,
                    'platform'      => $platform,
                    'device_id'     => $device_id,
                    'language'      => $language,
                    'firebase_uid'  => $uid
                );

                $user_data = apet_insert_user_from_firebase($add_data,$firebase_data);

                if(!$user_data){
                    response_error_message('faild_to_creat_new_user');
                }
            }
            $user_data['photo_url'] = apet_get_user_photo_url($firebase_data);
            $res=array(
                'success'   => 'true',
                'data'      => $user_data,
            );
            if($user_changed){
                $res['user_changed'] = 'true';
            }
        }else{
            $res=array(
                'success'       =>'false',
                'error_message' => $token_info['err_msg'],
            );
        }
        echo json_encode($res);
        wp_die();
    }else{
        response_error_message('unauthorize_access');
    }
}
add_action('wp_ajax_nopriv_apet_firebase_login', 'apet_firebase_login');
add_action('wp_ajax_apet_firebase_login', 'apet_firebase_login');
/**
 * ========= GET USER INFO ======
 *
 * @global type $wpdb
 * @param arr $data
 * @return arr | bool
 */
function apet_get_user_info($data) {
    $token = $data['token'];
    $user_type = $data['user_type'];
    $user_id = $data['user_id'];
    $device_id = $data['device_id'];
    $platform = $data['platform'];
    $result = false;

    $token_info = apet_verify_user($token);

    if($token_info['success'] === true){
        $args = array(
            'meta_query' => array(
                array(
                    'key' => 'user_type',
                    'value' => $user_type
                ),
                array(
                    'key' => 'firebase_uid',
                    'value' => $token_info['uid']
                )
            )
        );
        $user = get_users($args);
        if (!empty($user)) {
            $result = array();
            $user_id = $user[0]->ID;
            $all_data = get_user_meta($user_id);
            $result['user_id'] = $user_id;
            $result['lastname'] = $all_data['last_name'][0];
            $result['firstname'] = $all_data['first_name'][0];
            $result['phone'] = $all_data['phone'][0];
            $result['email'] = $all_data['email'][0];
            $result['user_status'] = $all_data['user_status'][0];
            $result['firebase_uid'] = $all_data['firebase_uid'][0];
            $result['bonus'] = $all_data['bonus'][0];
            $result['pending_email'] = $all_data['pending_email'][0];
            $result['bonus_unit_price'] = APET_BONUS_UNIT_PRICE;
            $bonus_data = $all_data['bonus_data'][0] ? unserialize($all_data['bonus_data'][0]) : NULL;
            $result['bonus_id'] = $bonus_data ? $bonus_data['bonus_id'] : NULL;
            $result['withdrawn_bonus'] = $bonus_data ? $bonus_data['withdrawn_bonus'] : NULL;
        }else{
            response_error_message('invalid_token');
        }
    }else{
        response_error_message('invalid_token');
    }

    return $result;
}

/**
 * ===== Ajax handler for getting user data ====
 *
 */
function apet_get_app_user_data() {

    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);

    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        $token = sanitize_text_field($_POST['token']);
        $app_version = isset($_POST['app_version']) ? sanitize_text_field($_POST['app_version']) : false;
        $platform = sanitize_text_field($_POST['platform']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $user_data = apet_get_user_info(array(
            'token'     => $token,
            'user_id'   => $user_id,
            'user_type' => $user_type,
            'platform'  => $platform,
            'device_id' => $device_id
        ));

        //START: Fix for apps lower then v. 1.0.21 to force update to min major app version.
        if($app_version === false){
            $user_data['firstname'] = $user_data['firstname'] . APET_OLD_APPS_FORCE_UPDATE_FIX;
        }
        //END

        $response = $user_data ? array('data' => $user_data, 'success' => 'true') : array('errorMessage' => 'data_failed', 'success' => 'false');
        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_get_app_user_data', 'apet_get_app_user_data');
add_action('wp_ajax_apet_get_app_user_data', 'apet_get_app_user_data');

/**
 * ========= CHANGE USER INFO ======
 *
 * @global type $wpdb
 * @param arr $data
 * @return arr | bool
 */
function apet_change_user_info($data) {
    global $wpdb;
    $token = $data['token'];
    $user_type = $data['user_type'];
    $user_id = $data['user_id'];
    $device_id = $data['device_id'];
    $platform = $data['platform'];
    $language = $data['language'];
    unset($data['token'], $data['platform']);
    $user_type = $data['user_type'];
    unset($data['user_type']);
    $result = false;
    $token_info = apet_verify_user($token);

    if($token_info['success'] === true){

        $uid = $token_info['uid'];

        $args = array(
            'meta_query' => array(
                array(
                    'key' => 'user_type',
                    'value' =>$user_type
                ),
                array(
                    'key' => 'firebase_uid',
                    'value' =>$uid
                )
            )
        );
        $user = get_users($args);

        if (!empty($user)) {
            $user_login = true;
            $user_id = $user[0]->ID;
            $userdata = array();
            $userdata['ID'] = $user_id;
            $userdata['first_name'] = isset($data['firstname']) ? $data['firstname'] : NULL;
            $userdata['last_name'] = isset($data['lastname']) ? $data['lastname'] : NULL;
            $userdata['user_email'] = isset($data['email']) ? $data['email'] : NULL;

            $device_data = get_user_meta($user_id, 'device_data', true);
            $device_data[$device_id]['language'] = $language;
            update_user_meta($user_id, 'device_data', $device_data);

            if ($userdata['user_email']) {
                $user_login = $wpdb->update($wpdb->users, array('user_login' => $userdata['user_email']), array('ID' => $user_id));
            }
            if ($user_login) {
                foreach ($userdata as $key => $value) {
                    if (is_null($value) || $value === '')
                        unset($userdata[$key]);
                }

                $result = wp_update_user($userdata);

                if (isset($data['phone'])) {
                    update_user_meta($user_id, 'phone', $data['phone']);
                }
                if (isset($data['email'])) {
                    update_user_meta($user_id, 'email', $data['email']);
                }
            }
        }else{
            apet_remove_device_id($device_id, $user_id);
            response_error_message('invalid_token');
        }
    }

    return $result;
}

function apet_change_user_email_in_fb($email,$firebase_uid,$user_id,$verified = true){
    global $auth,$wpdb;

    if($verified === true){
        $status = 'active';
    }else{
        $status = 'unconfirmed';
    }
    try{
        $props = [
            'email' => $email,
            'emailVerified' => $verified,
        ];
        $updatedUser = $auth->updateUser($firebase_uid, $props);
        $user_login = $wpdb->update($wpdb->users, array('user_login' => $email, 'user_email' => $email), array('ID' => $user_id));
        update_user_meta($user_id, "email", $email);
        update_user_meta($user_id, 'user_status', $status);
        update_user_meta($user_id, "pending_email", null);
        return true;
    }catch(Exception $e){
        return false;
    }
}
/**
 * ===== Ajax handler for changing user settings ====
 *
 */
function apet_change_app_user_settings() {

    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);

    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        global $auth;

        $validate = true;
        $user_type = sanitize_text_field($_POST['user-type']);
        $token = sanitize_text_field($_POST['token']);
        $platform = sanitize_text_field($_POST['platform']);
        $language = sanitize_text_field($_POST['language']);
        $firstname = isset($_POST['firstname']) ? sanitize_text_field($_POST['firstname']) : false;
        $lastname = isset($_POST['lastname']) ? sanitize_text_field($_POST['lastname']) : false;
        $email = isset($_POST['email']) ? (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? $_POST['email'] : $validate = false) : false;
        $phone = isset($_POST['phone']) ? sanitize_text_field($_POST['phone']) : false;
        if ($validate === false) {
            response_error_message('validation_error');
        }
        $token_info = apet_verify_user($token);
        if($token_info['success'] === true){

            $uid = $token_info['uid'];

            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' =>'app-user'
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' =>$uid
                    )
                )
            );
            $user = get_users($args);

            if (empty($user)) {
                apet_remove_device_id($device_id, $user_id);
                response_error_message('invalid_token');
            }
        }
        $data = array();
        $data['token'] = $token;
        $data['user_type'] = $user_type;
        $data['user_id'] = $user_id;
        $data['device_id'] = $device_id;
        $data['platform'] = $platform;
        $data['language'] = $language;

        if ($email) {
            $user_id = $user[0]->ID;
            $current_email = $user[0]->user_email;
            $check_email = apet_check_user_email($email);
            $user_status = get_user_meta($user_id,'user_status',true);
            if($user_status === 'active'){
                response_error_message('user_confirmed');
            }
            if ($check_email !== false && $current_email !== $email) {
                $exist_user_status =  get_user_meta($check_email,'user_status',true);
                $exist_user_type = get_user_meta($check_email,'user_type',true);
                if($exist_user_status === 'active' || $exist_user_type !== 'app-user'){
                    response_error_message('user_exist');
                }
            }
            /* Update user Pending Mail */

            $mail_user_uid = apet_get_user_by_email_in_fr($email);
            if($mail_user_uid){
                $email_success = apet_send_verification_mail($mail_user_uid,$language, $user_id,false);
            }else {
                $user_data = apet_get_user_from_firebase($token_info['uid']);
                /*  Create New <Mail> user */
                if (!is_null($user_data->email)) {
                    $changed_email = apet_change_user_email_in_fb($email, $user_data->uid, $user_id ,false);
                    $email_success = apet_send_verification_mail($user_data->uid, $language, $user_id, false);
                } else {
                    $userProperties = [
                        'email' => $email,
                        'emailVerified' => false,
                    ];
                    try {
                        $createdUser = $auth->createUser($userProperties);
                        $firebase_uid = $createdUser->uid;
                        $email_success = apet_send_verification_mail($firebase_uid, $language, $user_id, true);
                        $password = wp_generate_password(12, false);
                        $insert_data = array(
                            'user_type' => 'app-user',
                            'firstname' => 'temp',
                            'lastname' => 'temp',
                            'email' => $email,
                            'phone' => 'temp',
                            'password' => $password,
                            'platform' => $platform,
                            'language' => $language,
                            'device_id' => $device_id,
                            'firebase_uid' => $firebase_uid
                        );
                        $user_insert = apet_inert_user($insert_data, false, false);

                        if (!$user_insert) {
                            response_error_message('failed_register');
                        }
                    } catch (Exception $e) {
                        response_error_message($e->getMessage());
                    }
                }
            }
            if (!$email_success) {
                response_error_message('sending_email_confirm_faild');
            } else {
                update_user_meta($user_id, 'pending_email', $email);
            }
        }
        if ($firstname) {
            $data['firstname'] = $firstname;
        }
        if ($lastname) {
            $data['lastname'] = $lastname;
        }
        if ($phone) {
            $data['phone'] = $phone;
        }

        $user_changed_data = apet_change_user_info($data);
        if($user_changed_data){
            $response = array('success' => 'true');
        }else{
            response_error_message('faild_update_user_info');
        }

        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_change_app_user_settings', 'apet_change_app_user_settings');
add_action('wp_ajax_apet_change_app_user_settings', 'apet_change_app_user_settings');

/**
 * =========== Change User Password ==========
 *
 * @param arr $data
 */
function apet_change_user_password($data) {
    $token = $data['token'];
    $user_type = $data['user_type'];
    $password = $data['password'];
    $platform = $data['platform'];
    $device_id = $data['device_id'];
    $user_id = $data['user_id'];
    $result = false;

    $token_info = apet_verify_user($token);

    if($token_info['success'] === true) {

        $uid = $token_info['uid'];

        $args = array(
            'meta_query' => array(
                array(
                    'key' => 'user_type',
                    'value' => 'app-user'
                ),
                array(
                    'key' => 'firebase_uid',
                    'value' => $uid
                )
            )
        );
        $user = get_users($args);

        if (!empty($user)) {
            $user_id = $user[0]->ID;
            $email = $user[0]->user_email;

            // Remove from all devices //
            $device_data = get_user_meta($user_id, 'device_data', true);
            $current_device = $device_data[$device_id];
            $device_data = array($device_id => $current_device);
            update_user_meta($user_id, 'device_data', $device_data);
            wp_set_password($password, $user_id);
            $result = true;
        } else {
            apet_remove_device_id($device_id, $user_id);
            response_error_message('invalid_token');
        }
    }

    return $result;
}

/**
 * ===== Ajax handler for changing user password ====
 *
 */
function apet_app_change_password() {

    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);

    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        $validate = true;
        $user_type = sanitize_text_field($_POST['user-type']);
        $token = sanitize_text_field($_POST['token']);
        $platform = sanitize_text_field($_POST['platform']);

        $current_password = isset($_POST['current-password']) ? (strlen($_POST['current-password']) > 6 ? $_POST['current-password'] : $validate = false) : false;
        $new_password = isset($_POST['new-password']) ? (strlen($_POST['new-password']) > 6 ? $_POST['new-password'] : $validate = false) : false;
        $password_check = $current_password ? apet_check_current_password(array('password' => $current_password, 'user_id' => $user_id, 'user_type' => $user_type, 'token' => $token, 'device_id' => $device_id, 'platform' => $platform)) : true;

        if ($validate === false) {

            response_error_message('validation_error');
        } elseif ($password_check === false) {
            response_error_message('incorrect_password');
        }

        $data = array(
            'token' => $token,
            'password' => $new_password,
            'user_type' => $user_type,
            'device_id' => $device_id,
            'platform' => $platform,
            'user_id' => $user_id,
        );

        $user_changed_password = apet_change_user_password($data);
        $response = $user_changed_password ? array('data' => $user_changed_password, 'success' => 'true') : array('errorMessage' => "faild_update_user_password", 'success' => 'false');

        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_app_change_password', 'apet_app_change_password');
add_action('wp_ajax_apet_app_change_password', 'apet_app_change_password');

/**
 * ===== Checking  Email =====
 *
 * @global type $wpdb
 * @param arr $data
 * @return bool|arr
 */
function apet_check_user_email($email) {
    $exists = email_exists($email);

    return $exists;
}

function apet_check_current_password($data) {

    $token = $data['token'];
    $user_type = $data['user_type'];
    $password = $data['password'];
    $platform = $data['platform'];
    $device_id = $data['device_id'];
    $user_id = $data['user_id'];
    $result = false;
    $token_info = apet_verify_user($token);

    if($token_info['success'] === true){

        $uid = $token_info['uid'];

        $args = array(
            'meta_query' => array(
                array(
                    'key' => 'user_type',
                    'value' =>$user_type
                ),
                array(
                    'key' => 'firebase_uid',
                    'value' =>$uid
                )
            )
        );
        $user = get_users($args);

        if (!empty($user)) {
            $result = wp_check_password($password, $user[0]->data->user_pass, $user[0]->ID);
        }else{
            apet_remove_device_id($device_id, $user_id);
            response_error_message('invalid_token');
        }
    }
    return $result;
}

function apet_deactivate_app_user() {

    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);

    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        global $wpdb,$auth;
        $social =false;
        if(!isset($_POST['password']) && isset($_POST['is_social']) && $_POST['is_social'] === 'true'){
            $social =true;
        }
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $password = sanitize_text_field($_POST['password']);
        $device_id = sanitize_text_field($_POST['device_id']);
        $platform = sanitize_text_field($_POST['platform']);
        $user_id = sanitize_text_field($_POST['user_id']);
        if(!$social){
            $check_password = apet_check_current_password(array('password' => $password, 'user_id' => $user_id, 'user_type' => $user_type, 'token' => $token, 'device_id' => $device_id, 'platform' => $platform));
            if ($check_password === false) {
                response_error_message('wrong_password');
            }
        }

        $language = apet_get_device_language($device_id, $user_id);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){

            $uid = $token_info['uid'];

            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' =>$user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' =>$uid
                    )
                )
            );
            $user = get_users($args);
            $language = apet_get_device_language($device_id, $user_id);
            if (!empty($user)) {
                $user_id = $user[0]->ID;
                $email = $user[0]->user_email;
                $all_data = get_user_meta($user_id);
                $first_name = $all_data['first_name'][0];
                $firebase_uid = $all_data['firebase_uid'][0];
                update_user_meta($user_id, 'user_status', 'deactivated');
                update_user_meta($user_id, 'device_data', NULL);
                $requests_table = $wpdb->prefix . "requests";
                $cancel_date = date('Y-m-d H:i:s');
                $cancel_requests = $wpdb->query("UPDATE `$requests_table` SET status='canceled', cancel_date='$cancel_date' WHERE request_author='$user_id' ");
                if ($cancel_requests === false) {
                    response_error_message('deactivation_failed');
                }else{
                    try{
                        $uid = $firebase_uid;
                        $updatedUser = $auth->disableUser($uid);
                    }catch(Exception $e){
                        response_error_message($e->getMessage());
                    }
                }
            }else{
                apet_remove_device_id($device_id, $user_id);
                response_error_message('invalid_token');
            }
        }
        $subject = array(
            'hy' => 'Հաշիվը  կասեցվել է',
            'en' => 'Account deactivated',
            'ru' => 'Учетная запись деактивирована'
        );

        $body = array(
            'hy' => "<div class='wrapper'><p>Բարև " . ucfirst($first_name) . "!</p><div class='login-text'>Ձեր հաշիվը  հաջողությամբ կասեցվել է: Շնորհակալություն, EasyTraveling թիմ:</div></div>",
            'en' => "<div class='wrapper'><p>Hi " . ucfirst($first_name) . "!</p><div class='login-text'>Your account has been successfully deactivated. Thanks, EasyTraveling team.</div></div>",
            'ru' => "<div class='wrapper'><p>Привет " . ucfirst($first_name) . "!</p><div class='login-text'>Ваша учетная запись успешно деактивирована. Спасибо!, Команда EasyTraveling.</div></div>"
        );
        $style = '<style>.wrapper{background-color:#d6d6d6!important;padding:10px!important}.wrapper>p{color:#515151!important}.table-wrapper{background-color:#f6f6f6!important;border-radius:10px!important;padding:10px!important;width:500px!important;margin:0 auto!important}.login-text{display:block!important;font-family:arial,sans-serif!important;font-size:18px!important;padding:8px!important;text-align:center!important;color:#515151!important}.login-text a{text-decoration:none!important;padding:10px!important;background-color:#7cb342!important;color:#fff!important;border-radius:10px!important;display:inline-block!important}.table-wrapper table{background-color:#f6f6f6!important;font-family:arial,sans-serif!important;border-collapse:collapse!important;width:100%!important;margin:0 auto 10px!important}.table-wrapper thead{font-size:20px!important;border-bottom:1px solid #ddd!important}.table-wrapper thead th{color:#515151!important}.table-wrapper td,p,th{text-align:left!important;padding:0 8px!important;font-size:18px!important}.table-wrapper .first{padding-top:8px!important}.table-wrapper .odd{padding-bottom:4px!important;color:#515151!important;font-size:18px!important}.table-wrapper .even{padding-bottom:8px!important;color:#909090!important;font-size:16px!important}</style>';
        $email_success = apet_send_mail($email, $subject[$language], $body[$language],$style);
        echo json_encode(array('success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_deactivate_app_user', 'apet_deactivate_app_user');
add_action('wp_ajax_apet_deactivate_app_user', 'apet_deactivate_app_user');

function apet_app_user_log_out() {
    if (isset($_POST['token'])) {
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $user_id = sanitize_text_field($_POST['user_id']);
        $platform = sanitize_text_field($_POST['platform']);
        $device_id = sanitize_text_field($_POST['device_id']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){

            $uid = $token_info['uid'];

            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' =>$user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' =>$uid
                    )
                )
            );
            $user = get_users($args);

            if (!empty($user)) {
                $user_id = $user[0]->ID;
                apet_remove_device_id($device_id, $user_id);
            }else{
                apet_remove_device_id($device_id, $user_id);
                response_error_message('invalid_token');
            }
        }

        $response = array('success' => 'true');
        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_app_user_log_out', 'apet_app_user_log_out');
add_action('wp_ajax_apet_app_user_log_out', 'apet_app_user_log_out');

function apet_app_reset_password() {
    if (isset($_POST['email'])) {
        global $wpdb, $wp_hasher;
        $email = sanitize_text_field($_POST['email']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $device_id =isset($_POST['device_id']) ?  sanitize_text_field($_POST['device_id']) : '';

        $exist = apet_check_user_email($email);
        if (!$exist) {
            response_error_message('not_registered_email');
        }
        $user_login = sanitize_text_field($email);

        $user_data = get_user_by('email', trim($user_login));
        $user_id = $user_data->ID;
        $role = $user_data->roles[0];
        if (empty($user_data) || !$user_data || ($role!=='app-user' && $user_type ==='app-user') || ($user_type ==='admin-user'&& $role==='app-user')) {
            response_error_message('not_registered_email');
        }

        do_action('lostpassword_post');


        // redefining user_login ensures we return the right case in the email
        $user_login = $user_data->user_login;
        $user_email = $user_data->user_email;

        do_action('retreive_password', $user_login);  // Misspelled and deprecated
        do_action('retrieve_password', $user_login);

        $allow = apply_filters('allow_password_reset', true, $user_data->ID);

        if (!$allow)
            response_error_message('not_allowed_reset');
        else if (is_wp_error($allow))
            response_error_message('not_allowed_reset');

        $key = wp_generate_password(20, false);
        do_action('retrieve_password_key', $user_login, $key);

        if (empty($wp_hasher)) {
            require_once ABSPATH . 'wp-includes/class-phpass.php';
            $wp_hasher = new PasswordHash(8, true);
        }
        $hashed = $hashed = time() . ':' . $wp_hasher->HashPassword($key);
        $wpdb->update($wpdb->users, array('user_activation_key' => $hashed), array('user_login' => $user_login));
        $language= apet_get_device_language($device_id, $user_id);

        $title = array(
            'hy' => 'Գաղտնաբառի վերականգնում',
            'en' => 'Password Reset',
            'ru' => 'Восстановление пароля'
        );

        $message = array(
            'hy' => "<div class='wrapper'><p>Բարեւ</p><div class='login-text'>Ձեր գաղտնաբառը վերականգնելու համար հետեւեք հղմանը: Եթե դա այդպես չէ խնդրում ենք անտեսել այս էլ. նամակը:  <a href='" . network_site_url("reset?action=rp&key=$key&login=" . rawurlencode($user_login)."&ln=$language", 'login') . "'>".network_site_url("reset", 'login')."</a>Շնորհակալություն, EasyTraveling թիմ:</div></div>",
            'en' => "<div class='wrapper'><p>Hi</p><div class='login-text'>To reset your password follow the link. If that wasn't you please ignore this email:  <a href='" . network_site_url("reset?action=rp&key=$key&login=" . rawurlencode($user_login)."&ln=$language", 'login') . "'>".network_site_url("reset", 'login')."</a>Thanks, EasyTraveling team.</div></div>",
            'ru' => "<div class='wrapper'><p>Привет</p><div class='login-text'>Для восстановления пароля , перейдите по ссылке. Если это не так, пожалуйста, проигнорируйте это письмо: <a href='" . network_site_url("reset?action=rp&key=$key&login=" . rawurlencode($user_login)."&ln=$language", 'login') . "'>".network_site_url("reset", 'login')."</a>Спасибо!, Команда EasyTraveling.</div></div>"
        );
        $additional_link = network_site_url("reset?action=rp&key=$key&login=" . rawurlencode($user_login)."&ln=$language", 'login');

        $title = apply_filters('retrieve_password_title', $title[$language]);
        $message = apply_filters('retrieve_password_message', $message[$language], $key);

        if($user_type === 'app-user'){
            $email_success = apet_send_password_reset_mail($email,$language,$additional_link);
        }else{
            $style = '<style>.wrapper{background-color:#d6d6d6!important;padding:10px!important}.wrapper>p{color:#515151!important;text-align:left!important;padding:0 8px!important;font-size:18px!important}.login-text{display:block!important;font-family:arial,sans-serif!important;font-size:18px!important;padding:8px!important;text-align:left!important;color:#515151!important}.login-text a{text-decoration:none!important;padding:10px!important;background-color:#7cb342!important;color:#fff!important;border-radius:10px!important;display:inline-block!important;margin:10px!important;}</style>';
            $email_success = apet_send_mail($user_email, $title, $message,$style);
        }

        if ($message && !$email_success) {
            response_error_message('email_failed');
        } else {
            $response = array('success' => 'true');
        }

        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_app_reset_password', 'apet_app_reset_password');
add_action('wp_ajax_apet_app_reset_password', 'apet_app_reset_password');

function apet_update_push_token() {
    $device_id = sanitize_text_field($_POST['device_id']);
    $user_id = sanitize_text_field($_POST['user_id']);
    if (isset($_POST['token']) && apet_check_device_data($user_id,$device_id)) {
        $token = sanitize_text_field($_POST['token']);
        $user_type = sanitize_text_field($_POST['user-type']);
        $platform = sanitize_text_field($_POST['platform']);
        $push_token_id = sanitize_text_field($_POST['push_token_id']);

        $token_info = apet_verify_user($token);

        if($token_info['success'] === true){

            $uid = $token_info['uid'];

            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'user_type',
                        'value' =>$user_type
                    ),
                    array(
                        'key' => 'firebase_uid',
                        'value' =>$uid
                    )
                )
            );
            $user = get_users($args);

            if (empty($user)) {
                apet_remove_device_id($device_id, $user_id);
                response_error_message('invalid_token');
            }
        }
        $device_data = get_user_meta($user_id, 'device_data', true);
        $device_data[$device_id]['push_token_id'] = $push_token_id;
        update_user_meta($user_id, 'device_data', $device_data);
        $response = array('success' => 'true');
        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_update_push_token', 'apet_update_push_token');
add_action('wp_ajax_apet_update_push_token', 'apet_update_push_token');

//==============ADMIN===========================

function apet_admin_login() {
    if (isset($_POST['password'])) {
        $user_login = sanitize_text_field($_POST['email-username']);
        $user_password = sanitize_text_field($_POST['password']);

        $user_check = wp_authenticate($user_login, $user_password);
        if (is_wp_error($user_check)) {
            response_error_message('wrong_user');
        } else {
            if ($user_check->roles[0] === 'app-user') {
                response_error_message('wrong_user');
            } elseif ($user_check->roles[0] === 'tour-agent-admin') {

                global $wpdb;
                $tour_table = $wpdb->prefix . 'tour_agencies';
                $user_id = $user_check->ID;

                $tour_data = $wpdb->get_results("SELECT id,status FROM `$tour_table` WHERE tour_admin='$user_id' ", ARRAY_A);

                $tour_id = !empty($tour_data) ? $tour_data[0]['id'] : false;
                $tour_status = !empty($tour_data) ? $tour_data[0]['status'] : false;
                if (!$tour_id) {
                    response_error_message('no_tour_agency_linked');
                }
                if ($tour_status === 'deactivated') {
                    response_error_message('tour_agency_deactivated');
                }
            }
        }

        $creds = array(
            'user_login' => $user_login,
            'user_password' => $user_password,
            'remember' => true
        );

        if (is_ssl()) {
            $user = wp_signon($creds, true);
        } else {
            $user = wp_signon($creds, false);
        }

        if (!is_wp_error($user)) {

            $response = array('data' => $user, 'success' => 'true');
        } else {
            $response = array('errorMessage' => 'wrong_user', 'success' => 'false');
        }


        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_nopriv_apet_admin_login', 'apet_admin_login');
add_action('wp_ajax_apet_admin_login', 'apet_admin_login');

function apet_admin_user_log_out() {
    if (isset($_POST['action'])) {

        wp_logout();
        $response = array('success' => 'true');
        echo json_encode($response);
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_admin_user_log_out', 'apet_admin_user_log_out');


function apet_get_app_users() {
    if (isset($_POST['user_id'])) {
        global $wpdb;
        $user_id = sanitize_text_field($_POST['user_id']);
        $current_user_id = get_current_user_id();
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }

        $limit = isset($_POST['limit']) ? sanitize_text_field($_POST['limit']) : '';
        $offset = isset($_POST['offset']) ? sanitize_text_field($_POST['offset']) : '';
        $result = array('success' => 'true');
        $users_table = $wpdb->prefix . 'users';
        $usermeta_table = $wpdb->prefix . 'usermeta';

        $meta_query = array(
            array('key' => 'user_type', 'value' => 'app-user')
        );
        $filter_options = isset($_POST['filter']) ? json_decode(stripslashes(sanitize_text_field($_POST['filter'])), true) : false;
        if($filter_options !== false){
            if(isset($filter_options['search']) && !empty($filter_options['search'])){
                $meta_query[] = array(
                    'relation' => 'OR',
                    array('key' => 'email', 'value' => $filter_options['search'], 'compare' => 'LIKE'),
                    array('key' => 'first_name', 'value' => $filter_options['search'], 'compare' => 'LIKE'),
                    array('key' => 'last_name', 'value' => $filter_options['search'], 'compare' => 'LIKE'),
                    array('key' => 'phone', 'value' => $filter_options['search'], 'compare' => 'LIKE')
                );
            }
            if(isset($filter_options['status']) && !empty($filter_options['status'])) {
                switch ($filter_options['status']) {
                    case 'active':
                        $meta_query[] = array('key' => 'user_status', 'value' => 'active');
                        break;
                    case 'deactivated':
                        $meta_query[] = array('key' => 'user_status', 'value' => 'deactivated');
                        break;
                    case 'unconfirmed':
                        $meta_query[] = array('key' => 'user_status', 'value' => 'unconfirmed');
                        break;
                }
            }
        }

        $users_obj = new WP_User_Query(array(
            'number'=>$limit,
            'offset'=>$offset,
            'orderby'      => 'user_registered',
            'order'        => 'DESC',
            'meta_query'   => $meta_query

        ));
        $users_tmp = $users_obj->get_results();
        $users = array();
        foreach($users_tmp as $user){
            $users[]=$user->data;
        }
        $result['data'] = $users;

        if (empty($result['data'])) {
            response_error_message('no_users');
        }else{
            echo json_encode($result);
            wp_die();
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_app_users', 'apet_get_app_users');

function apet_get_app_user() {
    if (isset($_POST['user_id'])) {

        $user_id = sanitize_text_field($_POST['user_id']);
        $current_user_id = get_current_user_id();
        $app_user_id = sanitize_text_field($_POST['app_user_id']);
        $role = apet_get_logged_user_role();
        if ((!$current_user_id && $current_user_id !== (int)$user_id) || $role !== 'tour-super-admin') {
            response_error_message('unauthorize_access');
        }
        $user_meta =  get_user_meta($app_user_id);
        if(empty($user_meta)){
            response_error_message('fetching_user_failed');
        }
        $response =array();
        $response['first_name'] = $user_meta['first_name'][0];
        $response['last_name'] = $user_meta['last_name'][0];
        $response['phone'] = $user_meta['phone'][0];
        $response['email'] = $user_meta['email'][0];
        $response['user_status'] = $user_meta['user_status'][0];
        $response['bonus'] = $user_meta['bonus'][0];
        $response['device_data'] = unserialize($user_meta['device_data'][0]);
        $response['id'] = $app_user_id;
        echo json_encode(array('data' => $response, 'success' => 'true'));
        wp_die();
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_app_user', 'apet_get_app_user');
