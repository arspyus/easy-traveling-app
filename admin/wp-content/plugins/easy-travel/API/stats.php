<?php

function apet_get_stats() {

    if (isset($_POST['user_id'])) {
        global $wpdb;
        $role = apet_get_logged_user_role();
        $user_id = sanitize_text_field($_POST['user_id']);
        $tour_id = sanitize_text_field($_POST['tour_id']);

        $current_user_id = get_current_user_id();
        if (!$current_user_id && $current_user_id !== (int)$user_id) {
            response_error_message('unauthorize_access');
        } else {
            $tour_status = apet_get_tour_status($user_id);
            if (($tour_status === 'deactivated' || $tour_status === '') && $role !== 'tour-super-admin') {
                response_error_message('unauthorize_access');
            }
        }

        $response = null;
        $filter_options = isset($_POST['filter']) ? json_decode(stripslashes(sanitize_text_field($_POST['filter'])), true) : false;
        if ($filter_options !== false) {

            if (isset($filter_options['type']) && !empty($filter_options['type'])) {

                if ($filter_options['type'] == 'user') {

                    if (isset($filter_options['sub_type']) && !empty($filter_options['sub_type'])) {
                        if($filter_options['sub_type'] == 'regs'){
                            $date_range = isset($filter_options['date']) ? $filter_options['date'] : false;
                            $response = get_users_regs($date_range);
                        }elseif($filter_options['sub_type'] == 'types'){
                            $response = get_users_types();
                        }
                    }
                }elseif ($filter_options['type'] == 'agency'){

                    if (isset($filter_options['sub_type']) && !empty($filter_options['sub_type'])) {
                        if($filter_options['sub_type'] == 'regs'){
                            $date_range = isset($filter_options['date']) ? $filter_options['date'] : false;
                            $response = get_agencies_regs($date_range);
                        }elseif($filter_options['sub_type'] == 'types'){
                            $response = get_agencies_types();
                        }
                    }

                }elseif ($filter_options['type'] == 'special_offers'){

                    $date_range = isset($filter_options['date']) ? $filter_options['date'] : false;
                    $response = get_special_offers($date_range);
                }elseif ($filter_options['type'] == 'requests'){

                    $date_range = isset($filter_options['date']) ? $filter_options['date'] : false;
                    $data_types = isset($filter_options['data_types']) ? $filter_options['data_types'] : false;
                    $response = get_requests($date_range, $data_types);
                }
            }
        } else {
            response_error_message('no_stats');
        }

        //if ($tour_id === 'all' && $role === 'tour-super-admin') { } //Need this if response differse for super admin and usual agency

        if (empty($response)) {
            response_error_message('no_stats');
        } else {
            echo json_encode(array('data' => $response, 'success' => 'true'));
            wp_die();
        }
    } else {
        response_error_message('unauthorize_access');
    }
}

add_action('wp_ajax_apet_get_stats', 'apet_get_stats');

//Filter types
function get_users_regs($date_range = false) {

    global $wpdb;
    $users_table = $wpdb->prefix . 'users';
    $usermeta_table = $wpdb->prefix . 'usermeta';
    $filter = '';
    if (isset($date_range['from']) && !empty($date_range['from'])) {
        $filter .= " AND CONVERT_TZ(users.user_registered,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
    }
    if (isset($date_range['to']) && !empty($date_range['to'])) {
        $filter .= " AND CONVERT_TZ(users.user_registered,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
    }
    $result = $wpdb->get_results("
        SELECT DATE_FORMAT(users.user_registered, '%Y-%m-%d') as dates, count(DISTINCT users.ID) as numbers
        FROM $users_table users LEFT JOIN $usermeta_table usermeta ON users.ID = usermeta.user_id
        WHERE usermeta.meta_value='app-user' AND usermeta.meta_key='user_type' $filter GROUP BY DATE(users.user_registered) ORDER BY dates ASC"
        , ARRAY_A);

    return $result;
}

function get_users_types() {

    global $wpdb;
    $users_table = $wpdb->prefix . 'users';
    $usermeta_table = $wpdb->prefix . 'usermeta';
    $result = $wpdb->get_results("
                SELECT usermeta.meta_value as types, count(app_users.ID) as numbers FROM (
                    SELECT users.ID FROM $users_table users LEFT JOIN $usermeta_table usermeta ON users.ID = usermeta.user_id 
                    WHERE usermeta.meta_value='app-user' AND usermeta.meta_key='user_type'
                ) AS app_users LEFT JOIN $usermeta_table usermeta ON app_users.ID = usermeta.user_id 
                WHERE usermeta.meta_key='user_status' GROUP BY usermeta.meta_value ORDER BY usermeta.meta_value ASC"
        , ARRAY_A);

    return $result;
}

function get_agencies_regs($date_range = false) {

    global $wpdb;
    $agencies_table = $wpdb->prefix . 'tour_agencies';
    $filter = '';
    if (isset($date_range['from']) && !empty($date_range['from'])) {
        $filter .= " AND CONVERT_TZ(agencies.tour_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
    }
    if (isset($date_range['to']) && !empty($date_range['to'])) {
        $filter .= " AND CONVERT_TZ(agencies.tour_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
    }
    $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
    $result = $wpdb->get_results("
        SELECT DATE_FORMAT(agencies.tour_creation_date, '%Y-%m-%d') as dates, count(agencies.id) as numbers 
        FROM $agencies_table agencies $filter GROUP BY DATE(agencies.tour_creation_date) ORDER BY dates ASC"
        , ARRAY_A);

    return $result;
}

function get_agencies_types() {

    global $wpdb;
    $agencies_table = $wpdb->prefix . 'tour_agencies';
    $result = $wpdb->get_results("
        SELECT agencies.status as types, count(agencies.id) as numbers 
        FROM $agencies_table agencies GROUP BY agencies.status ORDER BY types ASC"
        , ARRAY_A);

    return $result;
}

function get_special_offers($date_range = false) {

    global $wpdb;
    $offers_table = $wpdb->prefix . 'special_offers';
    $filter = '';
    if (isset($date_range['from']) && !empty($date_range['from'])) {
        $filter .= " AND CONVERT_TZ(offers.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
    }
    if (isset($date_range['to']) && !empty($date_range['to'])) {
        $filter .= " AND CONVERT_TZ(offers.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
    }
    $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
    $result = $wpdb->get_results("
        SELECT DATE_FORMAT(offers.offer_creation_date, '%Y-%m-%d') as dates, count(offers.offer_id) as numbers 
        FROM $offers_table offers $filter GROUP BY DATE(offers.offer_creation_date) ORDER BY dates ASC"
        , ARRAY_A);

    return $result;
}

function get_requests($date_range = false, $data_type = false) {

    global $wpdb;
    $data_type = !is_array($data_type)?array($data_type):$data_type;
    if(array_search('requests', $data_type) !== false){
        $requests_table = $wpdb->prefix . 'requests';
        $filter = '';
        if (isset($date_range['from']) && !empty($date_range['from'])) {
            $filter .= " AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
        }
        if (isset($date_range['to']) && !empty($date_range['to'])) {
            $filter .= " AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
        }
        $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
        $temp_result = $wpdb->get_results("
        SELECT DATE_FORMAT(requests.request_date, '%Y-%m-%d') as dates, count(requests.request_id) as numbers 
        FROM $requests_table requests $filter GROUP BY DATE(requests.request_date) ORDER BY dates ASC"
            , ARRAY_A);
        $result['requests'] = $temp_result;
    }

    if(array_search('offers', $data_type) !== false){
        $offers_table = $wpdb->prefix . 'agency_offers';
        $filter = '';
        if (isset($date_range['from']) && !empty($date_range['from'])) {
            $filter .= " AND CONVERT_TZ(offers.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
        }
        if (isset($date_range['to']) && !empty($date_range['to'])) {
            $filter .= " AND CONVERT_TZ(offers.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
        }
        $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
        $temp_result = $wpdb->get_results("
        SELECT DATE_FORMAT(offers.offer_creation_date, '%Y-%m-%d') as dates, count(offers.offer_id) as numbers
        FROM $offers_table offers $filter GROUP BY DATE(offers.offer_creation_date) ORDER BY dates ASC"
            , ARRAY_A);
        $result['offers'] = $temp_result;
    }

    if(array_search('books', $data_type) !== false){
        $books_table = $wpdb->prefix . 'books';
        $filter = '';
        if (isset($date_range['from']) && !empty($date_range['from'])) {
            $filter .= " AND CONVERT_TZ(books.book_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
        }
        if (isset($date_range['to']) && !empty($date_range['to'])) {
            $filter .= " AND CONVERT_TZ(books.book_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
        }
        $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
        $temp_result = $wpdb->get_results("
        SELECT DATE_FORMAT(books.book_date, '%Y-%m-%d') as dates, count(books.book_id) as numbers
        FROM $books_table books $filter GROUP BY DATE(books.book_date) ORDER BY dates ASC"
            , ARRAY_A);
        $result['books'] = $temp_result;
    }

    if(array_search('offered_requests', $data_type) !== false){
        $requests_table = $wpdb->prefix . 'requests';
        $offers_table = $wpdb->prefix . 'agency_offers';
        $filter = '';
        if (isset($date_range['from']) && !empty($date_range['from'])) {
            $filter .= " AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
        }
        if (isset($date_range['to']) && !empty($date_range['to'])) {
            $filter .= " AND CONVERT_TZ(requests.request_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
        }
        $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
        $temp_result = $wpdb->get_results("
        SELECT DATE_FORMAT(requests.request_date, '%Y-%m-%d') as dates, count(requests.request_id) as numbers 
        FROM $requests_table requests INNER JOIN (
            SELECT request_id FROM $offers_table GROUP BY request_id
        ) AS offers ON requests.request_id=offers.request_id $filter GROUP BY DATE(requests.request_date) ORDER BY dates ASC"
            , ARRAY_A);

        $result['offered_requests'] = $temp_result;
    }

    if(array_search('booked_offers', $data_type) !== false){
        $books_table = $wpdb->prefix . 'books';
        $offers_table = $wpdb->prefix . 'agency_offers';
        $filter = '';
        if (isset($date_range['from']) && !empty($date_range['from'])) {
            $filter .= " AND CONVERT_TZ(offers.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
        }
        if (isset($date_range['to']) && !empty($date_range['to'])) {
            $filter .= " AND CONVERT_TZ(offers.offer_creation_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
        }
        $filter = empty($filter) ? $filter : " WHERE " . ltrim($filter, " AND ");
        $temp_result = $wpdb->get_results("
        SELECT DATE_FORMAT(offers.offer_creation_date, '%Y-%m-%d') as dates, count(offers.offer_id) as numbers 
        FROM $offers_table offers INNER JOIN $books_table books ON offers.offer_id=books.offer_id $filter GROUP BY DATE(offers.offer_creation_date) ORDER BY dates ASC"
            , ARRAY_A);

        $result['booked_offers'] = $temp_result;
    }

    // for new statuses
    $statuses_table = $wpdb->prefix . 'requests_statuses';
    $filter = '';

    if (isset($date_range['from']) && !empty($date_range['from'])) {
        $filter .= " AND CONVERT_TZ(statuses.set_date,'+00:00','" . APET_WP_TIMEZONE . "') >= CONVERT_TZ('" . $date_range['from'] . "','+00:00','" . APET_WP_TIMEZONE . "') ";
    }
    if (isset($date_range['to']) && !empty($date_range['to'])) {
        $filter .= " AND CONVERT_TZ(statuses.set_date,'+00:00','" . APET_WP_TIMEZONE . "') <= DATE_ADD(CONVERT_TZ('" . $date_range['to'] . "','+00:00','" . APET_WP_TIMEZONE . "'), INTERVAL 1 DAY) ";
    }
    foreach ($data_type as  $type){

        if(array_search($type, ['pending','offered','pre-booked','processing','confirmed','canceled','declined','booked']) !== false){
            $temp_result = $wpdb->get_results("
                SELECT DATE_FORMAT(statuses.set_date, '%Y-%m-%d') as dates, count(DISTINCT statuses.request_id ) as numbers 
                FROM $statuses_table statuses WHERE statuses.status='$type' $filter GROUP BY DATE(statuses.set_date) ORDER BY dates ASC"
                , ARRAY_A);
            $result[$type] = $temp_result;
        }
    }

    return $result;
}
